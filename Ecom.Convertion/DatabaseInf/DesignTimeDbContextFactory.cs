﻿using Ecom.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Ecom.Convertion.DatabaseInf
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<EcomContext>
    {
        public EcomContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<EcomContext>();

            var connectionString = configuration.GetConnectionString("DefaultConnection");
 
            builder.UseNpgsql(connectionString, o => o.MigrationsAssembly("Ecom.Database"));

            return new EcomContext(builder.Options);
        }
    }
}
