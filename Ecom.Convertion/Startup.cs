﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Ecom.Models;
using Ecom.Services;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Globalization;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using System.Reflection;
using Newtonsoft.Json.Serialization;
using Ecom.Services.Bus;
using Ecom.MvcCore;

namespace Ecom.Convertion
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true; //CheckConsentNeeded set to true, non-essential cookies aren't sent to the browser.
                options.MinimumSameSitePolicy = SameSiteMode.None;

            });

            services.AddEntityFrameworkNpgsql()
               .AddDbContext<EcomContext>()
               .BuildServiceProvider();

            services.AddDbContext<EcomContext>(options =>
            options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"), o => o.MigrationsAssembly("Ecom.Database")));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<UserService, UserService>();
            services.AddScoped<RabbitMQService, RabbitMQService>();
            services.AddScoped<DomainService, DomainService>();
            services.AddScoped<ServiceUtils, ServiceUtils>();
            services.AddScoped<DataIntegService, DataIntegService>();
            services.AddScoped<AddressService, AddressService>();
            services.AddScoped<StoreAccountService, StoreAccountService>();
            services.AddScoped<IMemberShipService, MemberShipService>();

            services.AddScoped<BrandService, BrandService>();
            services.AddScoped<SizeService, SizeService>();
            services.AddScoped<ColorService, ColorService>();
            services.AddScoped<ColorGlobalService, ColorGlobalService>();
            services.AddScoped<SeasonService, SeasonService>();
            services.AddScoped<CatalogService, CatalogService>();
            services.AddScoped<BannerService, BannerService>();
            services.AddScoped<CampaignService, CampaignService>();
            services.AddScoped<CouponService, CouponService>();
            services.AddScoped<EmailTemplateService, EmailTemplateService>();
            services.AddScoped<SendingProviderService, SendingProviderService>();
            services.AddScoped<ProductService, ProductService>();
            services.AddScoped<ProductDetailService, ProductDetailService>();
            services.AddScoped<PolicyLocalLangService, PolicyLocalLangService>();
            services.AddScoped<CatalogLocalLangService, CatalogLocalLangService>();
            services.AddScoped<PolicyService, PolicyService>();
            services.AddScoped<StockService, StockService>();
            services.AddScoped<ContentPageService, ContentPageService>();
            services.AddScoped<ContentPageLocalLangService, ContentPageLocalLangService>();
            services.AddScoped<EmailTemplateLocalLangService, EmailTemplateLocalLangService>();
            services.AddScoped<CargoService, CargoService>();
            services.AddScoped<CargoPriceService, CargoPriceService>();
            services.AddScoped<LangService, LangService>();
            services.AddScoped<PriceService, PriceService>();
            services.AddScoped<PriceCurrencyService, PriceCurrencyService>();
            services.AddScoped<CartService, CartService>();
            services.AddScoped<AddressService, AddressService>();
            services.AddScoped<PaymentService, PaymentService>();
            services.AddScoped<IMemberShipService, MemberShipService>();
            services.AddScoped<BinNumberService, BinNumberService>();
            services.AddScoped<OrderService, OrderService>();
            services.AddScoped<BankService, BankService>();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(60);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });

            services.AddDistributedMemoryCache();

            //services.AddMvc()
            //    .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver())
            //    .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc()
               .AddMvcLocalization()
               .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
               .AddJsonOptions(o =>
               {
                   if (o.SerializerSettings.ContractResolver != null)
                   {
                       var castedResolver = o.SerializerSettings.ContractResolver
                           as DefaultContractResolver;
                       castedResolver.NamingStrategy = null;
                   }
               })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
              // .AddDataAnnotationsLocalization();
              .AddDataAnnotationsLocalization(options =>
              {
                  options.DataAnnotationLocalizerProvider = (type, factory) =>
                  {
                      var assemblyName = new AssemblyName(typeof(SharedResource).GetTypeInfo().Assembly.FullName);
                      return factory.Create("SharedResource", assemblyName.Name);
                  };
              });

            var sp = services.BuildServiceProvider();
            var domainService = sp.GetService<DomainService>();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    //new CultureInfo("tr-TR"),
                    new CultureInfo("en-US"),
                };

                //options.DefaultRequestCulture = new RequestCulture(culture: "tr-TR", uiCulture: "tr-TR");

                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;

                options.RequestCultureProviders = new[]{ new RouteDataRequestCultureProvider{
                   IndexOfCulture=1,
                   IndexofUICulture=1,
                   defaultCulture="en-US",
                   domainService =domainService
                }};

            });

            services.Configure<RouteOptions>(options =>
            {
                options.ConstraintMap.Add("lang", typeof(IsLangCodeConstrait));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            HttpHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "Panel",
                  template: "{area:exists}/{lang?}/{controller=AdminHome}/{action=Index}/{id?}");

                routes.MapRoute(
                   name: "PagesWithLangCode",
                   template: "{lang}/{controller}/{action}/{id?}",
                   defaults: new { controller = "Home", action = "Index" },
                   constraints: new { lang = new IsLangCodeConstrait() }
                   );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");


            });
        }
    }
}
