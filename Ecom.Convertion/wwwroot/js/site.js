$(document).ready(function () {
    
    $(document).on('click', 'a[href^="#"]', function (event) {

        var pathname = window.location.pathname;

        if (pathname == "" || pathname == "/") {

            //event.preventDefault();

            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 75
            }, 500);


            $("h1, h2").removeClass("activeHeader");
            $($.attr(this, 'href')).find("h1:first, h2:first").addClass("activeHeader");

        } else {
            window.location.href = "/" + $.attr(this, 'href');
        }

    });

    $('#StoreName').on('keyup', function () {
        console.log($(this).val());
        $("#StoreNamePreview b").text($(this).val().replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-').toLowerCase()

        );
    });
     
    $(window).scroll(function () {
        var windowTop = $(window).scrollTop();
        if (windowTop > 400) {

            if (!$("body").hasClass("sticky")) {
                $("#Header").hide();
                $("#Header").fadeIn("slow");
                $('body').addClass('sticky');
            }


        } else {
            $('body').removeClass('sticky');
        }
    });


});

console.log("hello from home js");