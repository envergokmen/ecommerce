﻿using Ecom.Auth.Services;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.ViewModels.Domains;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace Ecom.Convertion.Areas.Panel.Controllers
{
    public class EcomAdminBaseController : Controller
    {
        public IMemberShipService memberShipService;
        public DomainService domainService;

        public EcomAdminBaseController(IMemberShipService _memberShipService, DomainService _domainService)
        {
            this.memberShipService = _memberShipService;
            this.domainService = _domainService;
        }

        public UserViewModel user;
        public DomainViewModel domain;
        public string actionName;
        public string controllerName;
        public string curUrl;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string langCode = context.RouteData.Values["lang"]?.ToString();

            curUrl = UriHelper.GetDisplayUrl(context.HttpContext.Request);
            var domainName = new Uri(curUrl).Authority?.Split(':')[0];

            domain = String.IsNullOrWhiteSpace(langCode) ? domainService.GetDomainByName(domainName) : domainService.GetDomainByNameAndLang(domainName, langCode);


            // we must always have a domain in specifed lang
            if (domain == null)
            {
                context.Result = new NotFoundResult();
                return;
            }

            if (String.IsNullOrWhiteSpace(langCode) && !String.IsNullOrWhiteSpace(domain.LangCode)) {

                context.Result = new RedirectResult($"{curUrl.TrimEnd('/')}/{domain.LangCode}");
                return;
            }
            
            actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ActionName;
            controllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ControllerName;
            user = memberShipService.TryToSignUser(Core.UserType.StoreManager);

            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {

            // do something after the action executes
            base.OnActionExecuted(context);
        }
    }
}
