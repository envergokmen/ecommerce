﻿using Ecom.Auth.Services;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Convertion.Areas.Panel.Controllers
{
    [Area("Panel")]
    public class AdminHomeController : EcomAdminBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<AdminHomeController> _localizer;

        public AdminHomeController(
           IMemberShipService _memberShipService,
           DomainService _domainService,
           IStringLocalizer<AdminHomeController> localizer,
           IHttpContextAccessor _contextAccessor) : base(_memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
        }
 
        public IActionResult Index()
        {
            HomeVM model = new HomeVM();

            model.PageTitle = $"{this.domain.DomainBrandName} {this.domain.DomainSlogan}";

            model.WelcomeMessage = _localizer["WelcomeMessage"];

            return View(model);
        }
    }
}
