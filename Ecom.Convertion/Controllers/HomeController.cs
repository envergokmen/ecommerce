﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Convertion.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;

namespace Ecom.Convertion.Controllers
{
    public class HomeController : EcomBaseController
    {
        
        private readonly IStringLocalizer<HomeController> _localizer;

        public HomeController(EcomContext db,
            IMemberShipService _memberShipService,
            DomainService _domainService,
            IStringLocalizer<HomeController> localizer,
            IHttpContextAccessor _contextAccessor) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
        }

         

        public IActionResult Index()
        {
            HomeVM model = new HomeVM();

            model.PageTitle = $"{this.domain.DomainBrandName} {this.domain.DomainSlogan}";

            model.WelcomeMessage = _localizer["WelcomeMessage"];

            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
