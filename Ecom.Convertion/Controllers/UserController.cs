﻿using Ecom.Auth.Services;
using Ecom.Convertion;
using Ecom.Core;
using Ecom.MvcCore.Extensions;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using System.Linq;

namespace Ecom.Controllers
{
    public class UserController : EcomBaseController
    {

        AddressService addressService;
        IStringLocalizer<SharedResource> localizer;

        public UserController( IHttpContextAccessor _contextAccessor, 
            IStringLocalizer<SharedResource> _localizer, 
            AddressService _addressService, 
            IMemberShipService _memberShipService, 
            DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            addressService = _addressService;
            localizer = _localizer;
        }
         
        public IActionResult Index()
        {
            return View();
        }


        [Route("Register")]
        public IActionResult Register()
        {
             
            ViewBag.CountryId = addressService.GetCountyList().Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToList().AddDefaultOption(localizer["PleaseSelectCountry"],"");
             
            return View();
        }
 
        [HttpPost] 
        [Route("Register")]
        //[AllowAnonymous]
        public IActionResult Register(RegisterStoreStoreVM registerVM)//create
        {

            registerVM.Domain = this.domain;
            OperationResult<UserViewModel> registerResult = memberShipService.RegisterStoreAccount(registerVM);

            if (!registerResult.Status)
            {
                ViewBag.RegisterErrors = registerResult.GetErrors();
                ViewBag.StoreUrl = Utils.FormatUrl(registerVM.StoreName);
                ViewBag.CountryId = addressService.GetCountyList().Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToList().AddDefaultOption(localizer["PleaseSelectCountry"], "");

                return View(registerVM);
            }
            else
            {
                ViewBag.RegisterSucceed = true;
            }

            return View(registerVM);

        }

        [Route("Login")]
        public IActionResult Login()
        {
            return View();
        }


        [HttpPost] 
        [Route("Login")]
        public IActionResult Login(LoginViewModel loginModel)
        {
            if (loginModel != null) loginModel.RememberMe = false;
            loginModel.RememberMe = true;
          
            OperationResult<UserViewModel> loginResult = memberShipService.Login(loginModel);
 
            if (!loginResult.Status)
            {
                ViewBag.LoginErrors = loginResult.GetErrors();
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

    }
}