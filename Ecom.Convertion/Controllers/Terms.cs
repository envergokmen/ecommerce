﻿using Ecom.Auth.Services;
using Ecom.Convertion;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using System.Linq;

namespace Ecom.Controllers
{
    public class TermsController : EcomBaseController
    {

        IStringLocalizer<SharedResource> localizer;

        public TermsController(IHttpContextAccessor _contextAccessor, IStringLocalizer<SharedResource> _localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            localizer = _localizer;
        }
        
        //[Route("Terms")]
        public IActionResult Index()
        {
             
            return View();
        }
  
    }
}