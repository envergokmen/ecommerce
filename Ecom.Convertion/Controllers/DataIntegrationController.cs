﻿using Ecom.Auth.Services;
using Ecom.Convertion;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecom.Controllers
{
    public class DataIntegrationController : Controller
    {

        private readonly string rootPath;
        private readonly DataIntegService dataIntegService;
        private IHostingEnvironment _env;

        public DataIntegrationController(IHostingEnvironment env, DataIntegService _dataIntegService) 
        {
            _env = env;
            rootPath = _env.ContentRootPath;
            dataIntegService = _dataIntegService;
        }
          
        public IActionResult CreateCities()//create
        {


            //dataIntegService.CreateCities(rootPath);

            return Content("OK");

        }


        public IActionResult Install()//create
        {
            dataIntegService.Install(rootPath);

            return Content("OK");

        }

        public IActionResult copyproducts()//create
        {
            dataIntegService.copyProducts();

            return Content("OK");

        }



    }
}