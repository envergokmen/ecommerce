﻿using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.ViewModels.Domains;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace Ecom.Convertion
{
    public class EcomBaseController : Controller
    {
        public IMemberShipService memberShipService;
        public DomainService domainService;
        public IHttpContextAccessor contextAccessor;

        public EcomBaseController(IMemberShipService _memberShipService, DomainService _domainService, IHttpContextAccessor _contextAccessor)
        {
            this.memberShipService = _memberShipService;
            this.domainService = _domainService;
            this.contextAccessor = _contextAccessor;
        }

        public UserViewModel user;
        public DomainViewModel domain;
        public string actionName;
        public string controllerName;
        public string curUrl;
        public int storeId => domain?.StoreAccountID ?? 0;
        public string IP;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string langCode = context.RouteData.Values["lang"]?.ToString();

            curUrl = UriHelper.GetDisplayUrl(context.HttpContext.Request);
            var domainName = new Uri(curUrl).Authority?.Split(':')[0];

            domain = String.IsNullOrWhiteSpace(langCode) ? domainService.GetDomainByName(domainName) : domainService.GetDomainByNameAndLang(domainName, langCode);
            IP = contextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            // we must always have a domain in specifed lang
            if (domain == null)
            {
                context.Result = new NotFoundResult();
                return;
            }
            else if (langCode != null && langCode.Length == 2 && domain.LangCode == langCode)
            {
                //always redirect if specified domain requires plain URL's without lang
                context.Result = new RedirectResult(curUrl.Replace($"/{langCode}", ""));
                return;
            }

            actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ActionName;
            controllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ControllerName;
            user = memberShipService.TryToSignUser(UserType.StoreManager);

            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {

            // do something after the action executes
            base.OnActionExecuted(context);
        }
    }
}
