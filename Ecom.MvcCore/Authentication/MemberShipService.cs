﻿using Ecom.Core;
using Ecom.Services;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Auth.Services
{
    public interface IMemberShipService
    {
        UserViewModel GetSignedUser();

        void SetSignedUser(UserViewModel user);

        int GetSignedUserID();
        string GetSignedUserName();

        int? GetSignedUserIDNullAble();

        UserViewModel TryToSignUser(UserType userType);

        OperationResult<UserViewModel> Login(LoginViewModel loginViewModel);
        OperationResult<UserViewModel> Register(RegisterViewModel registerViewModel);
        OperationResult<UserViewModel> RegisterStoreAccount(RegisterStoreStoreVM registerViewModel);

        void SetAuthCookieForUser(UserViewModel user, int expriesAfterMinutes = 360);
        void SetSessionForUser(UserViewModel user);

        OperationResult<UserViewModel> SetSessionFromAuthCookie(Dictionary<string, string> cookieInfo, UserType userType);
        Dictionary<string, string> TryToGetUserNamePassFromAuthCookie(bool setToSession = false, UserType userType = UserType.Normal);

        void LogOut();

        OperationResult<string> GeneratePassWordResetToken(int userID, DateTime exprirationDate);

        //for cart and order operations without login
        string GetSpecialKeyForUser();

        OperationResult<UserViewModel> ResetUserPassword(ResetPassworViewdModel resetPassWord);
        OperationResult<UserViewModel> ChangeUserPassWord(ChangePasswordViewModel _changePasswordViewModel);
        OperationResult<UserViewModel> Update(UpdateUserViewModel userUpdateViewModel);
        //string GetUserAuthKey();

        //OperationResult<UpdateUserViewModel> UpdateUserInforMation(AllUserInfosViewModel userInfos, DateTime birthDate);

    }

    public class MemberShipService : IMemberShipService
    {
        private readonly UserService userService;
        private readonly CampaignService campaignService;

        public const string AUTH_COOKIE_NAME = "EAUTH";
        public const string SPECIAL_COOIKE_NAME = "SKEY";
        public const string LOGGED_USER_SESSION_KEY = "LoggedUser";
        public const string LOGGED_USER_ID_SESSION_KEY = "LoggedUserID";
        public const int LONG_AUTH_COOKIE_IN_DAYS = 360; //1 year
        public RabbitMQService busService = new RabbitMQService();

        //private readonly IHttpContextAccessor _contextAccessor;
        private readonly HttpContext httpContext;

        //public const string USER_ID_CACHE_KEY = "AllUserByID";

        /// <summary>
        /// Consturctor injection managed by IOC container default
        /// </summary>
        /// <param name="_userService">Service based operations passed to userService</param>
        public MemberShipService(CampaignService _campaignService,UserService _userService, IHttpContextAccessor contextAccessor)
        {
            this.userService = _userService;
            this.campaignService = _campaignService;

            httpContext = contextAccessor?.HttpContext;

            //_contextAccessor.HttpContext.Session.SetString("test",  "bla bla");
            //var c = _contextAccessor.HttpContext.Session.GetString("test");

        }

        /// <summary>
        /// Generates reset token for user and saved it on user entity, doesn't send email
        /// </summary>
        /// <param name="userID">ID of user</param>
        /// <param name="exprirationDate">when the token will be expire</param>
        /// <returns>token as string, which have to send via email</returns>
        public OperationResult<string> GeneratePassWordResetToken(int userID, DateTime exprirationDate)
        {
            return userService.GeneratePassWordResetToken(userID, exprirationDate);
        }

        /// <summary>
        /// gets current user from session, if session was timeout, won't try relogin from cookie, if you need please see TryToSignUser()
        /// </summary>
        /// <returns>Isolated UserAuthViewModel from entity</returns>
        public UserViewModel GetSignedUser()
        {
            UserViewModel usr = null;

            if (httpContext.Session != null && httpContext.Session.GetString(LOGGED_USER_SESSION_KEY) != null)
            {
                var value = httpContext.Session.GetString(LOGGED_USER_SESSION_KEY);
                return value == null ? null : JsonConvert.DeserializeObject<UserViewModel>(value);
            }

            return usr;

        }

        /// <summary>
        /// gets current userid from session, if session was timeout, won't try relogin from cookie, if you need please see TryToSignUser()
        /// </summary>
        /// <returns>ID of current User, if not logged in returns -1</returns>
        public int GetSignedUserID()
        {
            int SignedUserID = -1;
            UserViewModel usr = GetSignedUser();

            if (usr != null)
            {
                SignedUserID = usr.ID;
            }

            return SignedUserID;
        }

        /// <summary>
        /// gets current userid from session, if session was timeout, won't try relogin from cookie, if you need please see TryToSignUser()
        /// </summary>
        /// <returns>ID of current User, if not logged in returns null</returns>
        public int? GetSignedUserIDNullAble()
        {
            int? SignedUserID = null;
            UserViewModel usr = GetSignedUser();

            if (usr != null)
            {
                SignedUserID = usr.ID;
            }

            return SignedUserID;
        }

        /// <summary>
        /// gets current username from session, if session was timeout, won't try relogin from cookie, if you need please see TryToSignUser()
        /// </summary>
        /// <returns>ID of current User, if not logged in returns -1</returns>
        public string GetSignedUserName()
        {
            UserViewModel usr = GetSignedUser();
            string userName = null;

            if (usr != null)
            {
                userName = usr.UserName;
            }

            return userName;
        }

        /// <summary>
        /// Gets or generate unique auth key value for user key name declared in the begining of the class
        /// </summary>
        /// <returns>value of auth key </returns>
        //public string GetUserAuthKey()
        //{
        //    string curAuthKey = "";

        //    if (httpContext.Session.GetString(SPECIAL_AUTH_KEY) != null)
        //    {
        //        curAuthKey = httpContext.Session.GetString(SPECIAL_AUTH_KEY);
        //    }
        //    else if (httpContext.Request.Cookies[SPECIAL_AUTH_KEY] != null)
        //    {
        //        curAuthKey = httpContext.Request.Cookies[SPECIAL_AUTH_KEY];
        //        curAuthKey = SecureOperations.ClearSqlInjectionHard(curAuthKey);
        //        httpContext.Session.SetString(SPECIAL_AUTH_KEY, curAuthKey);
        //    }
        //    else
        //    {
        //        curAuthKey = Guid.NewGuid().ToString();

        //        httpContext.Session.SetString(SPECIAL_AUTH_KEY, curAuthKey);
        //        httpContext.Response.Cookies.Append(SPECIAL_AUTH_KEY, curAuthKey, new CookieOptions
        //        {
        //            Expires = DateTime.Now.AddYears(20),
        //            HttpOnly = true
        //        });
        //    }

        //    return curAuthKey;
        //}


        /// <summary>
        /// try validate and login user if rememberme is checked adds cookie for long date if not adds cookie a little time
        /// </summary>
        /// <param name="loginViewModel"></param>
        /// <returns></returns>
        public OperationResult<UserViewModel> Login(LoginViewModel loginViewModel)
        {
            OperationResult<UserViewModel> loginResult = new OperationResult<UserViewModel>();

            if (loginViewModel == null)
            {
                loginResult.SetError("Username and password are required", ReturnStatusCode.BadRequest);
                return loginResult;
            }

            loginResult = userService.ValidateUser(loginViewModel);

            if (loginResult.Status)
            {
                SetSessionForUser(loginResult.Result);

                if (loginViewModel.RememberMe)
                {
                    SetAuthCookieForUser(loginResult.Result);
                }
                else
                {
                    ClearCookie();
                }

                busService.Publish(QueueItemType.UserLoggedIn, JsonConvert.SerializeObject(loginResult.Result), loginViewModel.StoreID);

            }

            return loginResult;
        }

        /// <summary>
        /// try validate and login user if rememberme is checked adds cookie for long date if not adds cookie a little time
        /// </summary>
        /// <param name="loginViewModel"></param>
        /// <returns></returns>
        public OperationResult<UserViewModel> Register(RegisterViewModel registerViewModel)
        {
            OperationResult<UserViewModel> registerResult = userService.Create(registerViewModel);

            if (registerResult.Status)
            {
                SetSessionForUser(registerResult.Result);
                SetAuthCookieForUser(registerResult.Result);
                busService.Publish(QueueItemType.UserRegistered, JsonConvert.SerializeObject(registerResult.Result), registerViewModel.StoreId);
                busService.Publish(QueueItemType.WelcomeMail, JsonConvert.SerializeObject(registerResult.Result), registerViewModel.StoreId);
            }

            return registerResult;
        }

        /// <summary>
        /// try validate and login user if rememberme is checked adds cookie for long date if not adds cookie a little time
        /// </summary>
        /// <param name="loginViewModel"></param>
        /// <returns></returns>
        public OperationResult<UserViewModel> RegisterStoreAccount(RegisterStoreStoreVM registerViewModel)
        {
            OperationResult<UserViewModel> registerResult = userService.CreateStoreUser(registerViewModel);

            if (registerResult.Status)
            {
                SetSessionForUser(registerResult.Result);
                SetAuthCookieForUser(registerResult.Result);
                busService.Publish(QueueItemType.StoreAccountCreated, JsonConvert.SerializeObject(registerResult.Result), registerViewModel.StoreId);
            }

            return registerResult;
        }


        public void LogOut()
        {
            ClearSession();
            ClearCookie();
        }

        private void ClearCookie()
        {
            httpContext.Response.Cookies.Delete(AUTH_COOKIE_NAME);
        }

        private void ClearSession()
        {
            httpContext.Session.Remove(LOGGED_USER_SESSION_KEY);
            httpContext.Session.Remove(LOGGED_USER_ID_SESSION_KEY);
        }

        public void SetAuthCookieForUser(UserViewModel user, int expriesAfterDay = 360)
        {
            if (user != null && user.UserName != null)
            {
                string UserNameAndPass = String.Concat(user.UserName, Environment.NewLine, user.HashedPassWord, Environment.NewLine, user.ID, Environment.NewLine, user.StoreAccountID);
                string HashedUserNameAndPass = SecureOperations.Encrypt(UserNameAndPass);

                httpContext.Response.Cookies.Append(AUTH_COOKIE_NAME, HashedUserNameAndPass, new CookieOptions
                {
                    Expires = DateTime.Now.AddDays(expriesAfterDay),
                    IsEssential = true
                });
            }
        }

        public string GetSpecialKeyForUser()
        {
            string sKey = httpContext.Session.GetString(SPECIAL_COOIKE_NAME);

            if (!String.IsNullOrWhiteSpace(sKey))
            {
                return sKey;
            }

            sKey = String.IsNullOrWhiteSpace(httpContext.Request.Cookies[SPECIAL_COOIKE_NAME]) ? Guid.NewGuid().ToString("N") : httpContext.Request.Cookies[SPECIAL_COOIKE_NAME];
            //TODO:consider checking cookie before accepting
            AppendCookieAndSession(sKey);

            return sKey;

        }

        private void AppendCookieAndSession(string sKey)
        {
            httpContext.Response.Cookies.Append(SPECIAL_COOIKE_NAME, sKey, new CookieOptions
            {
                Expires = DateTime.Now.AddDays(90),
                IsEssential = true
            });

            httpContext.Session.SetString(SPECIAL_COOIKE_NAME, sKey);
        }

        public void SetSessionForUser(UserViewModel user)
        {
            if (user != null && user.UserName != null && httpContext.Session != null)
            {
                httpContext.Session.SetString(LOGGED_USER_SESSION_KEY, JsonConvert.SerializeObject(user));
                httpContext.Session.SetInt32(LOGGED_USER_ID_SESSION_KEY, user.ID);
            }
        }


        public OperationResult<UserViewModel> SetSessionFromAuthCookie(Dictionary<string, string> cookieInfo, UserType userType)
        {
            OperationResult<UserViewModel> result = new OperationResult<UserViewModel>();

            if (cookieInfo != null && cookieInfo.Count() >= 2)
            {

                string userNameOrEmail = cookieInfo.First().Value;
                string hashedPassword = cookieInfo.Skip(1).FirstOrDefault().Value;
                int storeId = Convert.ToInt32(cookieInfo.Skip(3).FirstOrDefault().Value ?? "0");

                var userValidateResult = userService.ValidateUser(new LoginViewModel
                {
                    UserName = userNameOrEmail,
                    HashedPassWord = hashedPassword,
                    LoginType = userType,
                    StoreID = storeId

                });

                if (userValidateResult.Status && userValidateResult.Result != null)
                {
                    SetSessionForUser(userValidateResult.Result);
                    result.SetSuccessAndClearError(userValidateResult.Result); 
                }
                else if (userValidateResult.ReturnCode == ReturnStatusCode.NotFound)
                {
                    //if user not found remove cookie and session
                    LogOut();
                }

            }

            return result;

        }

        public void SetSignedUser(UserViewModel user)
        {
            if (user == null) return;
            httpContext.Session.SetString(LOGGED_USER_SESSION_KEY, JsonConvert.SerializeObject(user));
        }

        private void FillAuthDictFromCookie(Dictionary<string, string> values, string[] UserAndPass)
        {
            if (UserAndPass != null && UserAndPass.Count() > 0)
            {
                int i = 0;
                foreach (string item in UserAndPass)
                {
                    string key = "";

                    switch (i)
                    {
                        case 0: key = "username"; break;
                        case 1: key = "pass"; break;
                        case 2: key = "userid"; break;
                    }

                    values.Add(key, item);

                    i++;
                }

            }

        }

        public Dictionary<string, string> TryToGetUserNamePassFromAuthCookie(bool setToSession = false, UserType userType = UserType.Normal)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();

            if (httpContext.Request.Cookies[AUTH_COOKIE_NAME] != null && !String.IsNullOrWhiteSpace(httpContext.Request.Cookies[AUTH_COOKIE_NAME]))
            {
                string HashedUserNameAndPass = httpContext.Request.Cookies[AUTH_COOKIE_NAME];

                string NotHashedUserNamePass = SecureOperations.Decrypt(HashedUserNameAndPass);

                string[] UserAndPass = NotHashedUserNamePass.Replace(Environment.NewLine, "|").Split('|');

                FillAuthDictFromCookie(values, UserAndPass);

                if (setToSession)
                {
                    SetSessionFromAuthCookie(values, userType);
                }
            }

            return values;
        }

        public UserViewModel TryToSignUser(UserType userType)
        {
            UserViewModel user = GetSignedUser();

            if (user != null)
            {
                return user;
            }
            else
            {
                TryToGetUserNamePassFromAuthCookie(true, userType);
                user = GetSignedUser();
            }

            return user;

        }

        public OperationResult<UserViewModel> ResetUserPassword(ResetPassworViewdModel resetPassWord)
        {
            return userService.ResetUserPassword(resetPassWord);
        }

        public OperationResult<UserViewModel> ChangeUserPassWord(ChangePasswordViewModel _changePasswordViewModel)
        {
            return userService.ChangeUserPassWord(_changePasswordViewModel);
        }

        public OperationResult<UserViewModel> Update(UpdateUserViewModel userUpdateViewModel)
        {
            var result = userService.Update(userUpdateViewModel);

            if (result.Status && result.Result != null)
            {
                SetSessionForUser(result.Result);
            }

            return result;
        }

        //public OperationResult<UpdateUserViewModel> UpdateUserInforMation(AllUserInfosViewModel userInfos, DateTime birthDate)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
