﻿namespace Ecom.MvcCore
{
    public static class ToCultureCodeExtension
    {
        public static string ToCultureCode(this string langCode){

            var culture = "tr-TR";

            switch (langCode)
            {
                case "en": culture = "en-US"; break;
                case "tr": culture = "tr-TR"; break;
            }

            return culture;
        }
    }
}
