﻿using Ecom.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Localization;
using System;
using System.Threading.Tasks;

namespace Ecom.MvcCore
{
    public class RouteDataRequestCultureProvider : RequestCultureProvider
    {
        public int IndexOfCulture;
        public int IndexofUICulture;
        public string defaultCulture;
        public DomainService domainService;
        public CatalogService catalogService;
        public ProductService productService;
        public LangService langService;

        public override Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException(nameof(httpContext));

            string culture = defaultCulture;
            string uiCulture = defaultCulture;

            if (!IsInValidUrl(httpContext.Request.Path))
            {
                var twoLetterCultureName = httpContext.Request.Path.Value.Split('/')[IndexOfCulture]?.ToString();
                if (!String.IsNullOrWhiteSpace(httpContext.Request.Query["lang"].ToString())) twoLetterCultureName = httpContext.Request.Query["lang"].ToString();

                //Panel Area Route Lang Values
                if (!String.IsNullOrWhiteSpace(twoLetterCultureName) && twoLetterCultureName.ToLower() == "panel")
                {
                    var nextLangIndex = IndexOfCulture + 1;

                    var urlPaths = httpContext.Request.Path.Value.Split('/');
                    if (urlPaths.Length > nextLangIndex)
                    {
                        twoLetterCultureName = httpContext.Request.Path.Value.Split('/')[nextLangIndex]?.ToString();
                    }
                }

                if (twoLetterCultureName != null && twoLetterCultureName.Length == 2)
                {
                    culture = uiCulture = twoLetterCultureName.ToCultureCode();
                }

                if (catalogService!=null && (String.IsNullOrWhiteSpace(twoLetterCultureName) || twoLetterCultureName.Length != 2))
                {
                    //There is no route value we must decide default lang by making call  
                    var curUrl = UriHelper.GetDisplayUrl(httpContext.Request);
                    var domainName = new Uri(curUrl).Authority?.Split(':')[0];
                    var domain = domainService.GetDomainByName(domainName);

                    //we have to check products and catalogs details for current culture
                    if (domain != null && domain.StoreAccountID != null)
                    {
                        var path = httpContext.Request.Path.Value;

                        if (path.Length > 2)
                        {
                            var catalog = catalogService.GetCatalogForRoute(domain.StoreAccountID.Value, path, null);
                            if (catalog != null && catalog.LangID != null)
                            {
                                twoLetterCultureName = langService.GetLangById(catalog.LangID.Value, domain.StoreAccountID.Value)?.LangCode;
                                culture = uiCulture = twoLetterCultureName.ToCultureCode();
                            }
                            else
                            {
                                var langCode = productService.GetProductLangCodeForRoute(domain.StoreAccountID.Value, path, null);
                                twoLetterCultureName = langCode;
                                if (!String.IsNullOrWhiteSpace(langCode))
                                {
                                    culture = uiCulture = twoLetterCultureName.ToCultureCode();
                                }
                            }
                        }
                       
                    }

                    if (culture == null && domain != null && domain.LangCode != null && domain.LangCode.Length == 2)
                    {
                        culture = uiCulture = domain.LangCode.ToCultureCode();
                    }
                }

            }

            if (String.IsNullOrEmpty(culture))
            {
                culture = uiCulture = "en-US";
            }

            var providerResultCulture = new ProviderCultureResult(culture, uiCulture);
            return Task.FromResult(providerResultCulture);
        }

        private bool IsInValidUrl(string url)
        {
            if (String.IsNullOrWhiteSpace(url)) return true;

            var forbiddenItems = new string[] { "/js", "/sockjs", "/css", ".ico", "/ContentImages",".svg","/images", ".jpg", ".png", ".js", ".css", ".min", ".ttf", ".otf", ".oef", ".woff" };

            foreach (var item in forbiddenItems)
            {
                if (url.Contains(item, StringComparison.CurrentCultureIgnoreCase)) return true;
            }

            return false;
        }
    }
}
