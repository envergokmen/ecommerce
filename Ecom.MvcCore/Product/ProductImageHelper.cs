﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services;
using Ecom.ViewModels.Products;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Ecom.MvcCore.Product
{
    public class ProductImageHelper
    {
        private readonly ServiceUtils serviceUtils;
        private readonly EcomContext db;

        public ProductImageHelper(EcomContext _db,
            ServiceUtils _serviceUtils)
        {
            db = _db;
            serviceUtils = _serviceUtils;
        }

        public List<ProductImageVM> ParseProductImages(ProductCreateEditVM product, IFormFileCollection files)
        {
            List<ProductImageVM> images = new List<ProductImageVM>();

            if (files != null)
            {
                foreach (var file in files.OrderBy(x=>x.FileName))
                {
                    ProductImageVM img = new ProductImageVM();
                    img.ProductCode = product.ProductCode;
                    img.file = file;
                    img.ImagePath = file.FileName;

                    var fileName = file.FileName;
                    var items = fileName.Replace(product?.ProductCode ?? "", "").Replace(Path.GetExtension(fileName), "").Trim().Split('_').Where(x => x != "").ToArray();

                    //BLACK_DEEP-BLACK_1
                    SetColorAndGlobalAndAngle(product, img, items);

                    //DEEP-BLACK_1
                    SetColorAngleOnly(product, img, items);

                    images.Add(img);

                }
            }
            return images;
        }

        private static void SetColorOnly(ProductCreateEditVM product, ProductImageVM img, string[] items)
        {
            if (items.Length == 1 && Utils.IsNumeric(items[0]))
            {
                img.ColorName = items[0];
                img.Angle = Convert.ToInt16(items[1]);
            }
        }

        private static void SetColorAngleOnly(ProductCreateEditVM product, ProductImageVM img, string[] items)
        {
            if (items.Length == 2 && Utils.IsNumeric(items[1]))
            {
                img.ColorName = items[0];
                img.Angle = Convert.ToInt16(items[1]);
            }
        }
        private static void SetColorAndGlobalAndAngle(ProductCreateEditVM product, ProductImageVM img, string[] items)
        {

            if (items.Length == 3 && Utils.IsNumeric(items[2]))
            {
                img.ColorGlobalName = items[0];
                img.ColorName = items[1];
                img.Angle = Convert.ToInt16(items[2]);

            }
        }
    }
}
