﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecom.MvcCore.Extensions
{
    public static class ListGenerator
    {
        public static List<SelectListItem> AddDefaultOption(this List<SelectListItem> list, string dataTextField, string selectedValue)
        {
            list.Insert(0, new SelectListItem() { Text = dataTextField, Value = selectedValue });
            return list;
        }
    }
}
