﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.ContentPages
{
    public class ContentPageCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        public int SortNo { get; set; }

        [StringLength(100)]
        [Required]
        public string Name { get; set; }

        public Status PStatus { get; set; }

        public ContentPageType ContentPageType { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueIntVM> AllContentPageTypes { get; set; }

        public int Translations { get; set; }

    }
}
