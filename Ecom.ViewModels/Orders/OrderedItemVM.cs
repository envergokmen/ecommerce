﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Orders
{
    public class OrderedItemVM
    {
        public long OrderID { get; set; }
        public long ID { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal AraToplam { get; set; }
        public long StockID { get; set; }
        public long ProductID { get; set; }
        public int ColorID { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string URL { get; set; }
        public string Barkod { get; set; }
        public string YapCode { get; set; }
        public string ColorName { get; set; }
        public string SizeName { get; set; }
        public string ImagePath { get; set; }
        public bool OneDayCargo { get; set; }
        public Nullable<IadeStatus> ReturnStatus { get; set; }
        public DateTime CreatedOn { get; set; }

        public decimal SubTotal => Amount * Quantity;
    }

}
