﻿using Ecom.Core;
using Ecom.ViewModels.Addresses;
using Ecom.ViewModels.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Orders
{
    public class OrderDetailsVM
    {
        public OrderDetailsVM()
        {
            this.OrderedItems = new List<OrderedItemVM>();
        }
        public long ID { get; set; }

        public Status PStatus { get; set; }
        public DateTime CreatedOn { get; set; }
        public string TrackCode { get; set; }
        public decimal TotalAmount { get; set; }

        public AddressVM DeliveryAddress { get; set; }
        public AddressVM BillingAddress { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }

        public InstallmentGrpVM WebPosInstallment { get; set; }
        public int WebPosInstallmentId { get; set; }
        //public string DeliveryAdressPhone { get; set; }
        //public string DeliveryAdressPhone2 { get; set; }

        public string UserName { get; set; }
        public Nullable<decimal> CargoAmount { get; set; }
        public int? UserID { get; set; }
        public bool IsTransferedToErp { get; set; }
        public bool? IsCargoTransferred { get; set; }
        public decimal Balance { get; set; }
        public SiparisOdemeDurumu ErpOrderStatus { get; set; }
        public SiparisOdemeDurumu PaymentStatus { get; set; }
        public string PaymentStatus_Info { get; set; }
        public string Session { get; set; }
        public PaymentType PaymentType { get; set; }
        public HesapLamaTuru? CouponCalculationType { get; set; }
        public Nullable<decimal> CouponAmount { get; set; }
        public Nullable<decimal> CouponCalculationAmount { get; set; }
        public string CouponCode { get; set; }
        public long? CouponID { get; set; }

        public string CurrencySymbol { get; set; }
        public bool OneDayCargo { get; set; }
        public bool ShipAsGift { get; set; }
        public string CurrencyCode { get; set; }
        public int? CurrencyId { get; set; }
        //public string CityName { get; set; }
        //public string TownName { get; set; }
        public string CargoName { get; set; }
        public string CargoCode { get; set; }
        public string CargoLink { get; set; }

        public Nullable<long> ReturnedOrderID { get; set; }
        public bool IsReturnedToNetsis { get; set; }

        public Nullable<int> InstallmentQuantity { get; set; }
        public Nullable<DateTime> RetOrderDate { get; set; }
        public Nullable<DateTime> ChangeOrderDate { get; set; }

        public string PosName { get; set; }
        public string PaymentName { get; set; }
        public string CardNumber { get; set; }

        public Nullable<bool> IsPaid { get; set; }

        public int StoreAccountId { get; set; }
        public List<OrderedItemVM> OrderedItems { get; set; }
    }
}
