﻿using Ecom.Core;
using Ecom.ViewModels.Addresses;
using Ecom.ViewModels.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Orders
{
    public class OrderVM
    {
        public long ID { get; set; }
        public string TrackCode { get; set; }
        public PaymentType PaymentType { get; set; }
        public DateTime CreatedOn { get; set; }
        public decimal TotalAmount { get; set; }
        public SiparisOdemeDurumu PaymentStatus { get; set; }

        public int WebPosInstallmentId { get; set; }
        public InstallmentGrpVM WebPosInstallment { get; set; }
        public int PriceCurrencyId { get; set; }
        public string CurrencyCode { get; set; }

        public AddressVM DeliveryAddress { get; set; }
        public AddressVM BillingAddress { get; set; }

    }
}
