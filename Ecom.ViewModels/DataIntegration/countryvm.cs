﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.DataIntegration
{
    public class countryvm
    {
        public string id { get; set; }
        public string sortname { get; set; }
        public string name { get; set; }
        public string phoneCode { get; set; }
    }

    public class cityvm
    {
        public string id { get; set; }
        public string country_id { get; set; }
        public string name { get; set; } 
    }


    public class townvm
    {
        public string id { get; set; }
        public string state_id { get; set; }
        public string name { get; set; }
    }

}
