﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Stores
{
    public class StoreAccountViewModel
    {

        public int ID { get; set; }

        public string StoreName { get; set; }

        public string CompanyName { get; set; }
        public string CompanyTaxInfo { get; set; }
        public string CompanyTaxInfo2 { get; set; }
        public string CompanyEMail { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }

        public int? CountryId { get; set; }
        public int? CityId { get; set; }
        public int? TownId { get; set; }

        public int? RelatedUserId { get; set; }

        public Status PStatus { get; set; }
    }
}
