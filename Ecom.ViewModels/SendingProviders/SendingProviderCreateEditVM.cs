﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.SendingProviders
{
    public class SendingProviderCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        public Status PStatus { get; set; }


        public string SenderName { get; set; }
        [StringLength(90)]

        public string UserName { get; set; }

        [StringLength(20)]
        public string Password { get; set; }

        public SendingProviderType ProviderType { get; set; }
        public string ProviderTypeText => ProviderType.ToString();

        #region SMTP
        public int SmptPort { get; set; }

        [StringLength(150)]
        public string SmptHost { get; set; }

        [StringLength(150)]
        public string ApiEndPoint { get; set; }

        public bool EnableSsl { get; set; }

        #endregion

        #region EuroMSG

        [StringLength(25)]
        public string EuNameColumnName { get; set; }

        [StringLength(25)]
        public string EuEmailColumnName { get; set; }

        [StringLength(60)]
        public string EuroMessageFromName { get; set; }

        [StringLength(60)]
        public string EuroMessageFromAddress { get; set; }

        [StringLength(60)]
        public string EuroMessageReplyAddress { get; set; }

        [StringLength(25)]
        public string EuroMessageApiUser { get; set; }

        [StringLength(25)]
        public string EuroMessageApiPassword { get; set; }

        #endregion
 
        public string MailApiKey { get; set; }
        public string MailTransCode { get; set; }


        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueIntVM> AllSendingProviderTypes { get; set; }

    }
}
