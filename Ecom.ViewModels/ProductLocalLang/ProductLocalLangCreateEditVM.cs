﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.ProductLocalLangs
{
    public class ProductLocalLangCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [StringLength(150)]
        [Required]
        public string ProductName { get; set; }
         
        public string DetailHTML { get; set; }

        public int? LangID { get; set; }
        public long? ProductID { get; set; }

        public Status PStatus { get; set; }

        public string ProductCode { get; set; }
        public string LangCode { get; set; }

        [StringLength(250)]

        public string Title { get; set; }
         
        [StringLength(800)]

        public string Summary { get; set; }

        [StringLength(160)]

        public string MetaDescription { get; set; }

        [StringLength(260)]

        public string MetaKeywords { get; set; }

        [StringLength(260)]

        public string CanonicalLink { get; set; }

        [StringLength(260)]

        public string Url { get; set; }

       
        public List<NameValueStringVM> AllStatuses { get; set; } 
        public List<NameValueIntVM> AllDomains { get; set; }
        public List<NameValueIntVM> AllLangs { get; set; }
        public List<NameValueLongVM> AllProducts { get; set; }

    }
}
