﻿using Ecom.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Langs
{
    public class LangViewModel
    {
         
        public int ID { get; set; }
        public string Name { get; set; }
        public string LangCode { get; set; }
        public string FlagPath { get; set; }
        public bool IsDefault { get; set; }

    }
}
