﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels
{
    public class BaseViewModel
    {
        public string IP { get; set; }
        public UserViewModel CurUser { get; set; }
        public int StoreID => _storeId.HasValue && _storeId.Value>0? _storeId.Value : CurUser?.StoreAccountID ?? 0;

        private int? _storeId { get; set; }
        public void SetStoreId(int? Id)
        {
            _storeId = Id;
        }
    }
}
