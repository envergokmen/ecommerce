﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.PriceCurrencies
{
    public class PriceCurrencyVM
    {
        public string Name { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }
        public int PriceCurrencyID { get; set; }
        public bool IsDefault { get; set; }
    }
}
