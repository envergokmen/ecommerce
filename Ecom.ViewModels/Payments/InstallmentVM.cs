﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Payments
{
    public class InstallmentVM
    {
        public int ID { get; set; }
        public int WebPosID { get; set; }
        //INSTALLMENT
        public int Quantity { get; set; }
        public decimal Rate { get; set; }

        public decimal ExtraAmount { get; set; }
        public Nullable<decimal> UpLimit { get; set; }
        public Nullable<decimal> DownLimit { get; set; }

        //taksit erteleme
        public decimal DelayedMouth { get; set; }

        //ilave taksit
        public decimal PlusIntallmentAmount { get; set; }
        public decimal PlusInstallmentLimit { get; set; }

        public Nullable<int> SortNo { get; set; }

        public bool IsDefault { get; set; }
        public string Title { get; set; }
    }
}
