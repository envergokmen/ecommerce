﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Payments
{
    public class InstallmentGrpVM
    {
        public int ID { get; set; }
        //INSTALLMENT
        public int WebPosID { get; set; }
        public int Quantity { get; set; }
        public decimal Rate { get; set; }

        public decimal ExtraAmount { get; set; }
        public Nullable<decimal> UpLimit { get; set; }
        public Nullable<decimal> DownLimit { get; set; }

        //taksit erteleme
        public int DelayedMouth { get; set; }

        //ilave taksit
        public decimal PlusIntallmentAmount { get; set; }
        public decimal PlusInstallmentLimit { get; set; }

        public Nullable<int> SortNo { get; set; }

        public bool IsDefault { get; set; }
        public string Title { get; set; }

        //WEBPOS
        public string PosName { get; set; }
        public Nullable<int> BankCode { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string PostUrl { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string NormalPostUrl { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string MagazaNo { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string TerminalID { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string SecureKey3D { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string ApiUser { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string ApiPassword { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string ProvType { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string PostNetNo { get; set; }

        public string PosLogoPath { get; set; }

        public SanalPosOdemeTuru PosPaymentType { get; set; }

        public Nullable<int> PosSortNo { get; set; }

        public int PaymentID { get; set; }

        public int MountDelayCount { get; set; }

        public bool PosIsDefault { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string EntegHesapKodu { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string EntegHesapAdi { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public int? EntegRefKodu { get; set; }

        //PAYMENT

        public int? PaymentSortNo { get; set; }
        public string PaymentName { get; set; }
        public PaymentType PaymentType { get; set; }
        public string PaymentKeyForOtherTypes { get; set; }
        public string PaymentLogoPath { get; set; }



    }
}
