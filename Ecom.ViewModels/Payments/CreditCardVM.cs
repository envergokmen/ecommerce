﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ecom.ViewModels.Payments
{
    public class CreditCardVM
    {
        [Required]
        [StringLength(22)]
        public string CardNumber { get; set; }

        [Required]
        [StringLength(2)]
        public string ExpiresMonth { get; set; }

        [Required]
        [StringLength(4)]
        public string ExpiresYear { get; set; }

        [Required]
        [StringLength(50)]
        public string CardOwner { get; set; }

        public int CardCvc { get; set; }

        public string CardType { get; set; } // 01 VISA - 02 MASTER CARD - 03 AMERICAN EXPRESS

        public bool IsDebit { get; set; } // 01 VISA - 02 MASTER CARD - 03 AMERICAN EXPRESS

        public int WebPosInstallmentID { get; set; }
    }
}
