﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Payments
{
    public class PaymentCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        public int? SortNo { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        public string PaymentTypeStr { get; set; }

        public PaymentType PaymentType { get; set; }

        [StringLength(20)]
        public string PaymentKeyForOtherTypes { get; set; }

        public int WebPosCount { get; set; }

        public Status PStatus { get; set; }

        public string LogoPath { get; set; }

        [DataType(DataType.Upload)]
        public IFormFile file { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueIntVM> AllPaymentTypes { get; set; }

    }
}
