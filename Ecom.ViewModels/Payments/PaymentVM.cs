﻿using Ecom.Core;
using Ecom.ViewModels.Banks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Payments
{
    public class PaymentVM
    {
        public PaymentVM()
        {
            this.WebPoseses = new List<WebPosVM>();
            this.Banks = new List<BankVM>();
        }
        public int PaymentId { get; set; }
        //PAYMENT
        public int? PaymentSortNo { get; set; }
        public string PaymentName { get; set; }
        public PaymentType PaymentType { get; set; }
        public string PaymentKeyForOtherTypes { get; set; }
        public string PaymentLogoPath { get; set; }

        public List<WebPosVM> WebPoseses { get; set; }
        public List<BankVM> Banks { get; set; }
    }

 
}
