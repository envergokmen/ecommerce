﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Payments
{
    [Serializable]
    public class PaymentHistoryViewModel
    {
        public string CampaignChooseLink { get; set; } // Garanti Campaign Choose Link

        public long ID { get; set; }

        public decimal Amount { get; set; }
        public DateTime? PaymentDate { get; set; }

        public string Description { get; set; }

        public string Detail { get; set; }

        public long? OrderID { get; set; }

        public string TrackCode { get; set; }

        public int? UserID { get; set; }

        public string IP { get; set; }

        public string UserArgent { get; set; }

        public PaymentType PaymentType { get; set; }
        public PaymentLogType? PaymentLogType { get; set; }
        public PaymentComplateStaus PaymentComplateStatus { get; set; }

        public string CardNumber { get; set; }

        public int? ExpiresMonth { get; set; }
        public int? ExpiresYear { get; set; }

        public string CardOwner { get; set; }
        public int? CardCvc { get; set; }

        public int? WebPosID { get; set; }
        public int? CardType { get; set; } // 01 VISA - 02 MASTER CARD - 03 AMERICAN EXPRESS

        public int? WebPosInstallmentID { get; set; }

        public int? PriceCurrencyID { get; set; }
        public int? BankCurrencyNo { get; set; }

        public string Retval { get; set; }

        public string Err { get; set; }

        public string ErrMsg { get; set; }

        public string PayCode { get; set; }

        public string ExtraHostMsg { get; set; }

        public string PayRefNo { get; set; }
        public int StoreAccountId { get; set; }

    }
}
