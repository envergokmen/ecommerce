﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Payments
{
    public class WebPosVM
    {
        public WebPosVM()
        {
            this.Installments = new List<InstallmentVM>();
        }
        public int WebPosID { get; set; }

        //WEBPOS
        public string PosName { get; set; }
        public Nullable<int> BankCode { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string PostUrl { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string NormalPostUrl { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string MagazaNo { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string TerminalID { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string SecureKey3D { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string ApiUser { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string ApiPassword { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string ProvType { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string PostNetNo { get; set; }

        public string PosLogoPath { get; set; }

        public SanalPosOdemeTuru PosPaymentType { get; set; }

        public Nullable<int> PosSortNo { get; set; }

        public Nullable<int> PaymentID { get; set; }

        public int MountDelayCount { get; set; }

        public bool PosIsDefault { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string EntegHesapKodu { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string EntegHesapAdi { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public int? EntegRefKodu { get; set; }

        public List<InstallmentVM> Installments { get; set; }

    }


}
