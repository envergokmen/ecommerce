﻿namespace Ecom.ViewModels
{
    public class CategoryVM
    {
        public int CategoryId { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }

    }
}
