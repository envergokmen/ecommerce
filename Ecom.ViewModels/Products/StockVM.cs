﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class StockVM
    {
        public int? SizeID { get; set; }
        public string SizeName { get; set; }
        public string ColorName { get; set; }
        public bool IsActive { get; set; }
        public int? ColorID { get; set; }
        public long StockID { get; set; }
        public string Barkod { get; set; }
        public string YapCode { get; set; }
        public int Quantity { get; set; }
        public Status Status { get; set; }

        //Upload Products Transfer
        public string ProductCode { get; set; }
        public string MainColorName { get; set; }
        public string MainColorHex { get; set; }
        public string ColorHex { get; set; }

    }
}
