﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class SizeVM
    {
        public int SizeID { get; set; }
        //public string Code { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public int? ColorID { get; set; }
    }
}
