﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class ProductImageVM : BaseViewModel
    {
        public string ImagePath { get; set; }
        public string ImagePath2 { get; set; }
        public string ClassName { get; set; }

        public string ColorGlobalName { get; set; }
        public string ColorName { get; set; }
        public string ColorHex { get; set; }
        public string ColorHexPath { get; set; }
        public string SeasonName { get; set; }
        public string ProductCode { get; set; }

        public int? ColorID { get; set; }
        public int SortNo { get; set; }
        public int Angle { get; set; }
        public int ID { get; set; }
        public long? ProductID { get; set; }
        public IFormFile file { get; set; }
}
}
