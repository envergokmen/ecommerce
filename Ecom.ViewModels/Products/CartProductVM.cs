﻿using Ecom.Core;
using Ecom.ViewModels.Prices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class CartProductVM
    {
        public CartProductVM()
        {

        }

        public decimal SubTotal => (Amount ?? 0) * Quantity;
        public decimal SubTotalOld => (AmountOld ?? 0) * Quantity;
        public bool HasDiscount => SubTotalOld > SubTotal;
        public decimal DiscountRate => SubTotalOld > 0 && SubTotalOld > SubTotal ? 100 - ((SubTotal / SubTotalOld) * 100) : 0;

        public long ID { get; set; }

        public int Quantity { get; set; }

        public string Url { get; set; }
        public long ProductID { get; set; }
        public long? StockID { get; set; }
        public int? ColorID { get; set; }
        public int? SizeID { get; set; }

        public string ColorName { get; set; }
        public string SizeName { get; set; }

        public string ImagePath { get; set; }
        public string ProductName { get; set; }

        public string ProductCode { get; set; }
        public string Barkod { get; set; }
        public decimal VatRatio { get; set; }
        public int? LangID { get; set; }
        public int PriceCurrencyId { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> AmountOld { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }


    }
}
