﻿using Ecom.Core;
using Ecom.ViewModels.Prices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class ProductTransferVM : BaseViewModel
    {
        public ProductTransferVM()
        {
            this.Locals = new List<ProductLocalVM>();
            this.Images = new List<ProductImageVM>();
            this.Prices = new List<PriceVM>();
            this.Stocks = new List<StockVM>();
        }

        public string ProductCode { get; set; }
       
        public string ProductName { get; set; }
        public string SeasonName { get; set; }
        public string BrandName { get; set; }
        public string ReturnPolicyName { get; set; }
        public string CargoPolicyName { get; set; }
        public string DepartmentName { get; set; }
        public string StyleName { get; set; }
        public Status Status { get; set; }
        public int? CatalogSortNo { get; set; }
        public string Catalogs { get; set; }

        public List<PriceVM> Prices { get; set; }
        public List<ProductLocalVM> Locals { get; set; }
        public List<StockVM> Stocks { get; set; }
        public List<ProductImageVM> Images { get; set; }
           
        public Nullable<Gender> Gender { get; set; }
    }
}
