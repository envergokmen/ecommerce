﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class FilterGroup
    {
        public string Name { get; set; }
        public string FilterType { get; set; }
        public int PriceCurrencyId { get; set; }
        public int LangId { get; set; }
        public List<FilterItem> Items { get; set; }

    }
    public class FilterItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string FilterType { get; set; }
        public string ColorHex { get; set; }
        public string ColorPatternPath { get; set; }
        public int SortNo { get; set; }
        public bool IsSelected { get; set; }
    }
}
