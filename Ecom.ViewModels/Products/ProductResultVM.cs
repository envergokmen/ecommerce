﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class ProductResultVM
    {
        public int? ImageId { get; set; }
        public int? StockId { get; set; }
        public int? PriceId { get; set; }

        public List<int> ImageIds { get; set; }
        public long? ProductId { get; set; }
    }
}
