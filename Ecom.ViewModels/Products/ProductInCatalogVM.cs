﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class ProductInCatalogVMBasic
    {
        public int StoreID { get; set; }
        public long ProductID { get; set; }
        public int CatalogID { get; set; }
        public int ColorID { get; set; }
        public int ColorGlobalID { get; set; }
        public int SortNo { get; set; }
        public QueueItemType? ItemType { get; set; }

    }
}
