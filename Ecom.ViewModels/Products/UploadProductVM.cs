﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Products
{
    public class UploadProductVM : BaseViewModel
    {
        [DataType(DataType.Upload)]
        public IFormFileCollection files { get; set; }
    }
}
