﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Products
{
    public class ProductCreateEditVM : BaseViewModel
    {
        public long ID { get; set; }
         
        [StringLength(150)]
        [Required]
        public string ProductName { get; set; }

        public Status PStatus { get; set; }
         
        [StringLength(150)]
        public string SmallName { get; set; }
        
        [StringLength(40)]

        public string ProductCode { get; set; }

        public int? BrandID { get; set; }
        public int? SeasonID { get; set; }
        public int? ReturnPolicyID { get; set; }
        public int? CargoPolicyID { get; set; }
        public Nullable<int> ProductDepartmentID { get; set; }
        public Nullable<int> ProductStyleID { get; set; }
        public Nullable<Gender> Gender { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueIntVM> AllGenders { get; set; }
        public List<NameValueIntVM> AllBrands { get; set; }
        public List<NameValueIntVM> AllSeasons { get; set; }
        public List<NameValueIntVM> AllReturnPolicies { get; set; }
        public List<NameValueIntVM> AllCargoPolicies { get; set; }
        public List<NameValueIntVM> AllProductDepartments { get; set; }
        public List<NameValueIntVM> AllProductStyles { get; set; }
        public List<NameValueIntVM> AllColors { get; set; }
        public int Translations { get; set; }
        public int StockCount { get; set; }

        public List<ProductImageVM> Images { get; set; }

        [DataType(DataType.Upload)]
        public IFormFileCollection files { get; set; }
    }
}
