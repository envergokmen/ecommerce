﻿using Ecom.Core;
using Ecom.ViewModels.Prices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class ProductVM
    {
        public ProductVM()
        {
            this.Images = new List<ProductImageVM>();
        }

        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public string SmallName { get; set; }
        public string ProductCode { get; set; }
        public Nullable<Gender> Gender { get; set; }
        public int ProductLocalLangID { get; set; }
        public int? LangID { get; set; }
        public string LangCode { get; set; }
        public string Url { get; set; }
        public List<ProductInCatalogVMBasic> Categories { get; set; }
        public List<ProductImageVM> Images { get; set; }
        public List<ProductImageVM> ColorVariants { get; set; }
        public List<SizeVM> SizeVariants { get; set; }
        public List<PriceCreateEditVM> Prices { get; set; }
        public int? CatalogSortNo { get; set; }

        public int? ColorID { get; set; }
        public int? ColorGlobalID { get; set; }
        public string ImagePath => !ColorID.HasValue ? Images?.Select(c => c.ImagePath)?.FirstOrDefault() : Images?.Where(c => c.ColorID == ColorID)?.Select(c => c.ImagePath)?.FirstOrDefault();
        public string ImagePath2 => !ColorID.HasValue ? Images?.Skip(1).Select(c => c.ImagePath)?.FirstOrDefault() : Images?.Where(c => c.ColorID == ColorID)?.Skip(1).Select(c => c.ImagePath)?.FirstOrDefault();

        public decimal? Amount => PriceCurrencyId.HasValue ? Prices?.Where(c => c.PriceCurrencyID == PriceCurrencyId).Select(c => c.Amount)?.FirstOrDefault() : Prices?.Select(c => c.Amount)?.FirstOrDefault();
        public decimal? AmountOld => PriceCurrencyId.HasValue ? Prices?.Where(c => c.PriceCurrencyID == PriceCurrencyId).Select(c => c.AmountOld)?.FirstOrDefault() : Prices?.Select(c => c.AmountOld)?.FirstOrDefault();
        public string CurrencyCode => PriceCurrencyId.HasValue ? Prices?.Where(c => c.PriceCurrencyID == PriceCurrencyId).Select(c => c.CurrencyCode)?.FirstOrDefault() : Prices?.Select(c => c.CurrencyCode)?.FirstOrDefault();
        public string CurrencySymbol => PriceCurrencyId.HasValue ? Prices?.Where(c => c.PriceCurrencyID == PriceCurrencyId).Select(c => c.CurrencySymbol)?.FirstOrDefault() : Prices?.Select(c => c.CurrencySymbol)?.FirstOrDefault();

        public int? PriceCurrencyId { get; set; }
        //public string DetailHTML { get; set; }
        //public string ReturnPolicyHTML { get; set; }
        //public string CargoPolicyHTML { get; set; }
        //public string Title { get; set; }
        //public string Summary { get; set; }
        //public string MetaDescription { get; set; }
        //public string MetaKeywords { get; set; }
        //public string CanonicalLink { get; set; }
        //public int? BrandID { get; set; }
        //public int? SeasonID { get; set; }
        //public int? ReturnPolicyID { get; set; }
        //public int? CargoPolicyID { get; set; }
        public int StoreID { get; set; }
        public Nullable<int> ProductDepartmentID { get; set; }
        public Nullable<int> ProductStyleID { get; set; }

    }
}
