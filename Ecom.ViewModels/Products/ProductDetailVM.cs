﻿using Ecom.Core;
using Ecom.ViewModels.Prices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class ProductDTVM
    {
        public ProductDTVM()
        {
            this.AllImages = new List<ProductImageVM>();
            this.CurrentImages = new List<ProductImageVM>();
            this.CurrentSizes = new List<SizeVM>();
            this.AllStocks = new List<StockVM>();
        }

        public string AllStocksStr { get; set; }
        public string AllImagesStr { get; set; }

        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public string SmallName { get; set; }
        public string ProductCode { get; set; }
        public Nullable<Gender> Gender { get; set; }
        public int ProductLocalLangID { get; set; }
        public int? LangID { get; set; }
        public string LangCode { get; set; }
        public string Url { get; set; }

        public List<StockVM> AllStocks { get; set; }
        public List<SizeVM> CurrentSizes { get; set; }
        public List<ProductImageVM> AllImages { get; set; }
        public List<ProductImageVM> CurrentImages { get; set; }
        public List<ProductImageVM> ColorVariants { get; set; }
        public List<PriceCreateEditVM> Prices { get; set; }
        public int? CatalogSortNo { get; set; }

        public int? CurrentColorID { get; set; }

        public string ImagePath => !CurrentColorID.HasValue  ? AllImages?.Select(c=>c.ImagePath)?.FirstOrDefault(): AllImages.Where(c=>c.ColorID==CurrentColorID)?.Select(c => c.ImagePath)?.FirstOrDefault();
        public string ImagePath2 => !CurrentColorID.HasValue ? AllImages?.Skip(1).Select(c => c.ImagePath)?.FirstOrDefault() : AllImages.Where(c => c.ColorID == CurrentColorID)?.Skip(1).Select(c => c.ImagePath)?.FirstOrDefault();

        public decimal? Amount => Prices?.Select(c => c.Amount)?.FirstOrDefault();
        public decimal? AmountOld => Prices?.Select(c => c.AmountOld)?.FirstOrDefault();
        public string CurrencyCode => Prices?.Select(c => c.CurrencyCode)?.FirstOrDefault();
        public string CurrencySymbol => Prices?.Select(c => c.CurrencySymbol)?.FirstOrDefault();

        public string DetailHTML { get; set; }
        public string ReturnPolicyHTML { get; set; }
        public string CargoPolicyHTML { get; set; }

        public string Title { get; set; }
        public string Summary { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string CanonicalLink { get; set; }
        public int? BrandID { get; set; }
        public int? SeasonID { get; set; }
        public int? ReturnPolicyID { get; set; }
        public int? CargoPolicyID { get; set; }
        public Nullable<int> ProductDepartmentID { get; set; }
        public Nullable<int> ProductStyleID { get; set; }

    }
}
