﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Products
{
    public class ProductLocalVM
    {
        public int ProductLocalLangID { get; set; }
        public string ProductName { get; set; }
        public int? LangID { get; set; }
        public string LangCode { get; set; }
        public string Url { get; set; }
        public string DetailHTML { get; set; }
        public string ReturnPolicyHTML { get; set; }
        public string CargoPolicyHTML { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
    }
}
