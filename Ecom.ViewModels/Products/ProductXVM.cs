﻿using Ecom.Core;
using Ecom.ViewModels.Prices;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Status = Ecom.Core.Status;

namespace Ecom.ViewModels.Products
{

    public class ProductXVM
    {
        public ProductXVM()
        {
            this.AllImages = new List<ProductImageVM>();
        }


        public string ProductCode { get; set; }
        public long ProductID { get; set; }
        public string UniqueCode => StoreID.ToString() + "_" + ProductID + "_" + (ColorID ?? 0).ToString();


        public string _id { get; set; }

        public string ProductName { get; set; }
        public string SmallName { get; set; }

        public int? CatalogSortNo { get; set; }
        public int? CatalogID { get; set; }
        public int? ColorID { get; set; }
        public int? ColorGlobalID { get; set; }

        public int? PriceCurrencyId { get; set; }
        public int? LangID { get; set; }
        public string SearchItems { get; set; }
        public List<int> Sizes { get; set; }
        public List<ProductLocalVM> Locals { get; set; }

        public List<StockVM> AllStocks { get; set; }
        public List<ProductImageVM> AllImages { get; set; }


        [Ignore, JsonIgnore]
        public List<SizeVM> CurrentSizes { get; set; }

        [Ignore, JsonIgnore]
        public List<ProductImageVM> CurrentImages => ColorID.HasValue ? AllImages?.Where(c => c.ColorID == ColorID).ToList() : null;

        [Ignore, JsonIgnore]
        public ProductLocalVM CurLocal => LangID.HasValue ? Locals?.FirstOrDefault(c => c.LangID == LangID) : null;

        [Ignore, JsonIgnore]
        public int ProductLocalLangID => CurLocal?.ProductLocalLangID ?? 0;

        [Text(Ignore = true)]
        public string ProductLocalName => CurLocal?.ProductName ?? ProductName;

        [Text(Ignore = true)]
        public string LangCode => CurLocal?.LangCode ?? null;

        [Text(Ignore = true)]
        public string Url => CurLocal?.Url ?? "";

        [Text(Ignore = true)]
        public string DetailHTML => CurLocal?.DetailHTML ?? "";

        [Text(Ignore = true)]
        public string ReturnPolicyHTML => CurLocal?.ReturnPolicyHTML ?? null;

        [Text(Ignore = true)]
        public string CargoPolicyHTML => CurLocal?.CargoPolicyHTML ?? null;

        [Text(Ignore = true)]
        public string Title => CurLocal?.Title ?? null;

        [Text(Ignore = true)]
        public string Summary => CurLocal?.Summary ?? null;

        [Text(Ignore = true)]
        public string MetaDescription => CurLocal?.MetaDescription ?? null;

        [Text(Ignore = true)]
        public string MetaKeywords => CurLocal?.MetaKeywords ?? null;

        public int TotalQuantity => ColorID.HasValue ? AllStocks?.Where(c => c.ColorID == ColorID).Sum(c => c.Quantity) ?? 0 : AllStocks?.Sum(c => c.Quantity) ?? 0;

        public List<ProductImageVM> ColorVariants { get; set; }
        public List<PriceVM> Prices { get; set; }
        public List<SizeVM> SizeVariants { get; set; }

        //public int? LangID { get; set; }

        public string ImagePath => !ColorID.HasValue ? AllImages?.Select(c => c.ImagePath)?.FirstOrDefault() : AllImages.Where(c => c.ColorID == ColorID)?.Select(c => c.ImagePath)?.FirstOrDefault();
        public string ImagePath2 => !ColorID.HasValue ? AllImages?.Skip(1).Select(c => c.ImagePath)?.FirstOrDefault() : AllImages.Where(c => c.ColorID == ColorID)?.Skip(1).Select(c => c.ImagePath)?.FirstOrDefault();

        public decimal? Amount => PriceCurrencyId.HasValue ? Prices?.Where(c => c.PriceCurrencyID == PriceCurrencyId).Select(c => c.Amount)?.FirstOrDefault() : Prices?.Select(c => c.Amount)?.FirstOrDefault();
        public decimal? AmountOld => PriceCurrencyId.HasValue ? Prices?.Where(c => c.PriceCurrencyID == PriceCurrencyId).Select(c => c.AmountOld)?.FirstOrDefault() : Prices?.Select(c => c.AmountOld)?.FirstOrDefault();
        public string CurrencyCode => PriceCurrencyId.HasValue ? Prices?.Where(c => c.PriceCurrencyID == PriceCurrencyId).Select(c => c.CurrencyCode)?.FirstOrDefault() : Prices?.Select(c => c.CurrencyCode)?.FirstOrDefault();
        public string CurrencySymbol => PriceCurrencyId.HasValue ? Prices?.Where(c => c.PriceCurrencyID == PriceCurrencyId).Select(c => c.CurrencySymbol)?.FirstOrDefault() : Prices?.Select(c => c.CurrencySymbol)?.FirstOrDefault();

        public int? BrandID { get; set; }
        public int? SeasonID { get; set; }
        public int? ReturnPolicyID { get; set; }
        public int? CargoPolicyID { get; set; }
        public int StoreID { get; set; }
        public string DepartmentName { get; set; }
        public string StyleName { get; set; }
        public Nullable<int> ProductDepartmentID { get; set; }
        public Nullable<int> ProductStyleID { get; set; }


        public Nullable<Gender> Gender { get; set; }
        public Status Pstatus { get; set; }
        public string AllStocksStr { get; set; }
        public string AllImagesStr { get; set; }

    }
}
