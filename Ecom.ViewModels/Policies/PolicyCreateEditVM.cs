﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Policies
{
    public class PolicyCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        public int SortNo { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        public Status PStatus { get; set; }
        public YesNo DefaultPolicy { get; set; }
        public PoliciyType PolicyType { get; set; }
        public string PolicyTypeStr => PolicyType.ToString();

        public int Translations { get; set; }
        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueIntVM> AllIsDefaults { get; set; }
        public List<NameValueIntVM> AllPolicyTypes { get; set; }

    }
}
