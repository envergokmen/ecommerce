﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.ContentPageLocalLangs
{
    public class ContentPageLocalLangCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        [Required]
        public string DetailHTML { get; set; }

        public int LangID { get; set; }
        public int ContentPageID { get; set; }

        public Status PStatus { get; set; }

        public string ParentName { get; set; }
        public string LangCode { get; set; }

        [StringLength(260)]

        public string Title { get; set; }
         
        [StringLength(800)]

        public string Summary { get; set; }

        [StringLength(160)]

        public string MetaDescription { get; set; }

        [StringLength(260)]

        public string MetaKeywords { get; set; }

        [StringLength(260)]

        public string CanonicalLink { get; set; }

        [StringLength(260)]

        public string Link { get; set; }

        [StringLength(260)]

        public string Url { get; set; }

        [StringLength(250)]

        public string WebImageUrl { get; set; }

        [StringLength(250)]

        public string MenuImageUrl { get; set; }

        [StringLength(250)]

        public string MobileImageUrl { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; } 
        public List<NameValueIntVM> AllDomains { get; set; }
        public List<NameValueIntVM> AllLangs { get; set; }
        public List<NameValueIntVM> AllContentPages { get; set; }

    }
}
