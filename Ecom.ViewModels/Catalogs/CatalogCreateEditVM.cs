﻿using Ecom.Core;
using Ecom.ViewModels.Products;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Catalogs
{
    public class CatalogCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        public int SortNo { get; set; }

        [StringLength(100)]
        [Required]
        public string Name { get; set; }
        public string ParentName { get; set; }

        public List<FilterGroup> Filters { get; set; }
        public string FilterGroupsStr { get; set; }
        public Status PStatus { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueIntVM> AllCatalogs { get; set; }
        public List<NameValueIntVM> AllCatalogTypes { get; set; }

        public CatalogType CatalogType { get; set; }

        public int? ParentID { get; set; }
        public int? LangID { get; set; }

        public int ItemCount { get; set; }
 
        public bool ShowInFooter { get; set; }
        public bool ShowInHeader { get; set; }

        [StringLength(260)]
        public string Title { get; set; }
          
        public string Description { get; set; }

        [StringLength(800)]
        public string Summary { get; set; }

        [StringLength(160)]
        public string MetaDescription { get; set; }

        [StringLength(260)]
        public string MetaKeywords { get; set; }

        [StringLength(260)]
        public string CanonicalLink { get; set; }

        [StringLength(260)]
        public string Link { get; set; }

        [StringLength(260)]
        public string Url { get; set; }

        [StringLength(150)]
        public string WebImageUrl { get; set; }

        [StringLength(150)]
        public string MenuImageUrl { get; set; }

        [StringLength(150)]
        public string MobileImageUrl { get; set; }

        public int Translations { get; set; }

        public List<CatalogCreateEditVM> SubCatalogs { get; set; }

        public bool IsChecked { get; set; }

    }
}
