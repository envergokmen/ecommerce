﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Brands
{
    public class BrandCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "TestMessage")]
        [StringLength(150)]
        public string Name { get; set; }

        [StringLength(150)]

        public string ImagePath { get; set; }
        //public string ExistingImagePath { get; set; }

        public int SortNo { get; set; }

        public Status PStatus { get; set; }

        public int? StoreAccountID { get; set; }
        [DataType(DataType.Upload)]
        public IFormFile file { get; set; }
    }
}
