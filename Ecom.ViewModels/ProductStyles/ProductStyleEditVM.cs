﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.ProductStyles
{
    public class ProductStyleCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        public int SortNo { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        public Status PStatus { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; }

    }
}
