﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.WebPoses
{
    public class WebPosCreateEditVM : BaseViewModel
    {

        public string PaymentTypeStr { get; set; }
        public int InstallmentsCount { get; set; }

        public int ID { get; set; }

        public Status PStatus { get; set; }

        [StringLength(40)]

        public string Name { get; set; }

        public Nullable<int> BankCode { get; set; }

        [StringLength(100)]

        public string PostUrl { get; set; }

        [StringLength(100)]
        public string NormalPostUrl { get; set; }


        [StringLength(20)]
        public string MagazaNo { get; set; }

        [StringLength(20)]
        public string TerminalID { get; set; }

        [StringLength(50)]
        public string SecureKey3D { get; set; }

        [StringLength(15)]
        public string ApiUser { get; set; }

        [StringLength(20)]
        public string ApiPassword { get; set; }


        [StringLength(10)]
        public string ProvType { get; set; }

        [StringLength(10)]
        public string PostNetNo { get; set; }

        [StringLength(100)]
        public string LogoPath { get; set; }

        public SanalPosOdemeTuru PaymentType { get; set; }

        public Nullable<int> SortNo { get; set; }

        public Nullable<int> PaymentID { get; set; }

        public int MountDelayCount { get; set; }

        public bool IsDefault { get; set; }

        [StringLength(20)]
        public string EntegHesapKodu { get; set; }

        [StringLength(50)]
        public string EntegHesapAdi { get; set; }

        public int? EntegRefKodu { get; set; }


        [DataType(DataType.Upload)]
        public IFormFile file { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; } 
        public List<NameValueIntVM> AllPayments { get; set; }
        public List<NameValueIntVM> AllPosPaymentTypes { get; set; }

    }
}
