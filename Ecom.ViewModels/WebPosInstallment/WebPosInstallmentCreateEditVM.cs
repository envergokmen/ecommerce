﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.WebPosInstallments
{
    public class WebPosInstallmentCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        public int WebPosID { get; set; }
        public int Quantity { get; set; }
        public decimal Rate { get; set; }

        public decimal ExtraAmount { get; set; }

        public Nullable<decimal> UpLimit { get; set; }
        public Nullable<decimal> DownLimit { get; set; }

        //taksit erteleme
        public int DelayedMouth { get; set; }

        //ilave taksit
        public decimal PlusIntallmentAmount { get; set; }
        public decimal PlusInstallmentLimit { get; set; }

        public Nullable<int> SortNo { get; set; }
       
        public bool IsDefault { get; set; }

        [StringLength(80)]

        public string Title { get; set; }

        public Status PStatus { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; } 
        public List<NameValueIntVM> AllWebPoses { get; set; }

    }
}
