﻿using Ecom.ViewModels.Domains;
using System;

namespace Ecom.ViewModels.SiteSettings
{
    public class SiteSettingViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string BrandName { get; set; }

        public string BrandSlogan { get; set; }
        public string BrandDescription { get; set; }
        public string SiteMetaKeywords { get; set; }
        public string CompanyAdress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyPhone2 { get; set; }
        public string CompanyPhone3 { get; set; }
        public string CompanyEmail { get; set; }

        public string CompanyFax { get; set; }
        public string CompanyGoogleMap { get; set; }
        public string EmailAddress { get; set; }
        public string SliderIntervalTime { get; set; }
        public string FacebookClientID { get; set; }
        public string FacebookClientSecret { get; set; }
        public Nullable<int> LangID { get; set; }
        public string LangCode { get; set; }

        public DomainViewModel Domain { get; set; }
    }
}
