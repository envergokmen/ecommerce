﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Cargoes
{
    public class CargoCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        public int SortNo { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        public Status PStatus { get; set; }

        public string TaxNumber { get; set; }

        public bool IsUseWebServis { get; set; }

        [StringLength(25)]

        public string WebServisUserName { get; set; }

        [StringLength(25)]

        public string WebServisUserPassword { get; set; }

        [StringLength(25)]

        public string WebServisCustomerNumber { get; set; }

        [StringLength(125)]

        public string WebServisShipperName { get; set; }

        [StringLength(250)]

        public string WebServisShipperAddress { get; set; }

        [StringLength(20)]

        public string ShipperPhoneNumber { get; set; }

        public int WebServisShipperCityCode { get; set; }
        public int WebServisShipperAreaCode { get; set; }

        public CargoCompanyType CargoCompany { get; set; }
         
        public int? StoreAccountID { get; set; }
      
        public int? DomainID { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueIntVM> AllDomains { get; set; }
        public List<NameValueIntVM> AllCargoCompanies { get; set; }

        public string Notes { get; set; }

        public string LogoPath { get; set; }
        [DataType(DataType.Upload)]
        public IFormFile file { get; set; }

    }
}
