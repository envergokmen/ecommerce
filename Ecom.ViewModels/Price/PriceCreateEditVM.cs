﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Prices
{
    public class PriceCreateEditVM : BaseViewModel
    {
        public long ID { get; set; }

        public Status PStatus { get; set; }

        [Required]
        public decimal Amount { get; set; }
        public decimal AmountOld { get; set; }
        public long? ProductID { get; set; }
        public Nullable<long> StockID { get; set; }

        public string CurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }

        [Required]
        public int PriceCurrencyID { get; set; }
        public decimal VAT { get; set; }
        public int? DomainID { get; set; }
        public int? StoreAccountID { get; set; }

        [StringLength(40)]
        public string ProductCode { get; set; }

        [StringLength(40)]
        public string Barkod { get; set; }
         
        public List<NameValueStringVM> AllStatuses { get; set; } 
        public List<NameValueIntVM> AllDomains { get; set; }
        public List<NameValueIntVM> AllCurrencies { get; set; }

    }
}
