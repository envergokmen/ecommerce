﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Prices
{
    public class PriceVM
    {
        public long ID { get; set; }

        public decimal Amount { get; set; }
        public decimal AmountOld { get; set; }
        public long? ProductID { get; set; }
        public Nullable<long> StockID { get; set; }

        public string CurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }

        public int PriceCurrencyID { get; set; }
        public decimal VAT { get; set; }
        public int? DomainID { get; set; }
        public int? StoreAccountID { get; set; }

    }
}
