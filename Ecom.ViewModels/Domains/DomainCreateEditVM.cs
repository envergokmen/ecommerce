﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Domains
{
    public class DomainCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        public byte Priority { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(1, 100000)]
        public int? PriceCurrencyID { get; set; }

        [Required]
        [Range(1, 100000)]
        public int? LangID { get; set; }

        public Nullable<int> DepotID { get; set; }

        public Status PStatus { get; set; }

        //Title & Slogan
        [StringLength(50)]
        public string DomainBrandName { get; set; }

        public bool AllowChangeLang { get; set; }
        public bool AllowChangeCurrency { get; set; }

        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }

        //Listing Only
        public string LangCode { get; set; }
        public string CurrencyCode { get; set; }

        public List<NameValueIntVM> AllCurrencies { get; set; }
        public List<NameValueIntVM> AllLangs { get; set; }
        public List<NameValueStringVM> AllStatuses { get; set; }

    }
}
