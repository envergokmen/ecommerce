﻿using Ecom.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Domains
{
    public class DomainViewModel
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "İsim gereklidir")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Para birimi seçilmesi zorunludur.")]
        [Range(1, 100000)]
        public int PriceCurrencyID { get; set; }

        [Required(ErrorMessage = "Dil seçilmesi zorunlu.")]
        [Range(1, 100000)]
        public int LangID { get; set; }

        public Nullable<int> DepotID { get; set; }
        public Nullable<int> StoreAccountID { get; set; }

        public DomainTipi DomainType { get; set; }

        [StringLength(50)]
        public string DomainBrandName { get; set; }

        [StringLength(160)]
        public string DomainSlogan { get; set; }

        public string PriceCurrencyCode { get; set; }
        public string PriceCurrencySymbol { get; set; }

        public string LangCode  { get; set; }
        public bool AllowChangeLang { get; set; }
        public bool AllowChangeCurrency { get; set; }

    }
}
