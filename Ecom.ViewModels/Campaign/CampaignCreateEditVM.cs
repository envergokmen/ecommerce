﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Campaigns
{
    public class CampaignCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int? Priority { get; set; }
           
        public Status PStatus { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueStringVM> AllCampaignTypes { get; set; }
        public List<NameValueStringVM> AllCalculationTypes { get; set; }
        public List<NameValueStringVM> AllLangs { get; set; }
        public List<NameValueIntVM> AllDomains { get; set; }
        public List<NameValueIntVM> AllPriceCurrencies { get; set; }

        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }

        [Required]
        public HesapLamaTuru? CalculationType { get; set; }

        public bool AllowOnDiscountedProducts { get; set; }
        public bool AllowWithOtherCoupons { get; set; }
        public bool AllowWithOtherCampaigns { get; set; }
        public bool AllowMultipleUse { get; set; }
        public bool AllowMultipleCouponPerUser { get; set; }


        [Required]
        public decimal? Amount { get; set; } //kampanyanın vereceği çek tutarı

        [Required]
        public decimal? AmountLimit { get; set; }
        public decimal? MaxAmountLimit { get; set; }
        public DateTime? CouponEndDate { get; set; }
        public Nullable<int> CouponAutoEndDate { get; set; }
        public decimal? DiscountLimit { get; set; }

        //Kupon ne kadarlık alışverişte oluşacak
        public decimal? CreationLimit { get; set; }

        [StringLength(250)]
        public string ImagePath { get; set; }

        public KampanyaTuru CampaignType { get; set; }
        public bool ShowOnCampaigns { get; set; }

        [StringLength(4000)]
        public string CreditCardLimits { get; set; } //BIN limitleri

        public int? DomainId { get; set; }

        //[Required(ErrorMessage = "Para birimi seçilmesi zorunlu.")]
        //[Range(1, 100000)]
        public int? PriceCurrencyID { get; set; }

    }
}
