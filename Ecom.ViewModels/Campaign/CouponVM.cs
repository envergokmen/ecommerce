﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Campaign
{
    public class CouponVM
    {
        public long ID { get; set; }

        public string CouponCode { get; set; }

        public Nullable<int> CampaignID { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> FromUserID { get; set; }
        public Nullable<int> FromOrderID { get; set; }
        public bool IsUsed { get; set; }

        public Status PStatus { get; set; }

        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public HesapLamaTuru? CalculationType { get; set; }

        public bool? AllowOnDiscountedProducts { get; set; }
        public bool? AllowWithOtherCoupons { get; set; }
        public bool? AllowMultipleUse { get; set; }
        public decimal? Amount { get; set; } 
        public decimal? AmountLimit { get; set; }
        public decimal? MaxAmountLimit { get; set; }
        public decimal? DiscountLimit { get; set; }

        //Kupon ne kadarlık alışverişte oluşacak
        public decimal? CreationLimit { get; set; }
        public string ImagePath { get; set; }
        public KampanyaTuru? CampaignType { get; set; }
        public bool ShowOnCampaigns { get; set; }
        public string CreditCardLimits { get; set; } 
        public int? DomainId { get; set; }
        public int? PriceCurrencyID { get; set; }
    }
}
