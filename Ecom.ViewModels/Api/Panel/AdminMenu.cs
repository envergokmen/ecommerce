﻿using System.Collections.Generic;

namespace Ecom.ViewModels.Api.Panel
{
    public class AdminMenu
    {
        public AdminMenu(string name)
        {
            this.Name = name;
        }

        public AdminMenu(string name, int Id, List<AdminMenu> subItems = null, string linkTo = null, string title = null, bool isOpen=false, string icon="")
        {
            this.Name = name;
            this.LinkTo = linkTo;
            this.Title = title;
            this.SubItems = subItems;
            this.Id = Id;
            this.IsOpen = isOpen;
            this.Icon = icon;

        }

        public string Icon { get; set; }
        public int? Id { get; set; }
        public string Name { get; set; }
        public string LinkTo { get; set; }
        public string Title { get; set; }
        public bool IsOpen { get; set; }

        public List<AdminMenu> SubItems { get; set; }
    }
}
