﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.CargoPrices
{
    public class CargoPriceCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [Required]
        public decimal Amount { get; set; }
 
        public Nullable<decimal> FreeShippingLimit { get; set; }

        public string CurrencyCode { get; set; }
        public string CargoCompany { get; set; }
        public string CountryName { get; set; }
        public string LogoPath { get; set; }

        [Required]
        public int CargoID { get; set; }

        //Kargonun Gideceği Ülke
        public Nullable<int> CountryID { get; set; }

        [Required]
        public int PriceCurrencyID { get; set; }

        public Nullable<int> VAT { get; set; }

        public int? StoreAccountID { get; set; }
        public int? DomainID { get; set; }

        public Status PStatus { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueIntVM> AllDomains { get; set; }
        public List<NameValueIntVM> AllCountries { get; set; }
        public List<NameValueIntVM> AllCargoes { get; set; }
        public List<NameValueIntVM> AllCurrencies { get; set; }

    }
}
