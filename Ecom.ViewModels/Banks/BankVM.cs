﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Banks
{
    public class BankVM 
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string LogoPath { get; set; }
        public string CorporateName { get; set; }
        public string CityName { get; set; }
        public string DepartmentAndNo { get; set; }
        public string AccountNo { get; set; }
        public string SortCode { get; set; }
        public string IBAN { get; set; }
        public Nullable<int> PaymentID { get; set; }
        public Nullable<int> BankCode { get; set; }
        public int SortNo { get; set; }
    }
}
