﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Banks
{
    public class BankCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "TestMessage")]
        [StringLength(40)]
        public string Name { get; set; }

        [StringLength(150)]

        public string LogoPath { get; set; }

        [StringLength(60)]
        public string CorporateName { get; set; }

        [StringLength(40)]

        public string CityName { get; set; }

        [StringLength(40)]

        public string DepartmentAndNo { get; set; }

        [StringLength(25)]

        public string AccountNo { get; set; }

        [StringLength(25)]

        public string SortCode { get; set; }

        [StringLength(40)]

        public string IBAN { get; set; }

        public Nullable<int> PaymentID { get; set; }

        public Nullable<int> BankCode { get; set; }


        public int SortNo { get; set; }

        public Status PStatus { get; set; }

        public List<NameValueIntVM> AllPayments { get; set; }
        public List<NameValueStringVM> AllStatuses { get; set; }
        public int? StoreAccountID { get; set; }
        [DataType(DataType.Upload)]
        public IFormFile file { get; set; }
    }
}
