﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ecom.ViewModels.BinNumber
{
    public class BinNumberVM
    {

        public int ID { get; set; }

        public int BINNO { get; set; }

        public Nullable<int> BankCode { get; set; }
        public Nullable<int> OwnerBankCode { get; set; }

        public Nullable<int> WebPosID { get; set; }

        public Status PStatus { get; set; }

        [StringLength(50)]

        public string BankName { get; set; }

        [StringLength(50)]

        public string CardName { get; set; }

        [StringLength(25)]

        public string CardType { get; set; }

        [StringLength(25)]

        public string SubCardType { get; set; }

        [StringLength(150)]

        public string BankLogoPath { get; set; }

        [StringLength(150)]

        public string CardLogoPath { get; set; }

        [StringLength(40)]

        public string Name { get; set; }

        public bool AllowInstallment { get; set; }

        [StringLength(20)]

        public string PrizeType { get; set; }

        public bool IsCorporate { get; set; }

    }
}
