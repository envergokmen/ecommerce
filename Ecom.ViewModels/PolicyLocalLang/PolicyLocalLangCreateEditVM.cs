﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.PolicyLocalLangs
{
    public class PolicyLocalLangCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        [Required]
        public string DetailHTML { get; set; }

        public int? LangID { get; set; }
        public int PolicyID { get; set; }

        public Status PStatus { get; set; }

        public string ParentName { get; set; }
        public string LangCode { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; } 
        public List<NameValueIntVM> AllDomains { get; set; }
        public List<NameValueIntVM> AllLangs { get; set; }
        public List<NameValueIntVM> AllPolicies { get; set; }

    }
}
