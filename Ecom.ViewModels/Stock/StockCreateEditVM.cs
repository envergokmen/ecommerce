﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Stocks
{
    public class StockCreateEditVM : BaseViewModel
    {
        public long ID { get; set; }

        [StringLength(50)]
        [Required]
        public string Barkod { get; set; }
        public long? ProductID { get; set; }

        public Status PStatus { get; set; }

        public string SizeName { get; set; }
        public string ColorName { get; set; }

        public string ParentName { get; set; }
        public string ProductCode { get; set; }
        public string YapCode { get; set; }
        public int Quantity { get; set; }

        public Nullable<int> ColorID { get; set; }
        public Nullable<int> SizeID { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; } 
        public List<NameValueIntVM> AllSizes { get; set; }
        public List<NameValueIntVM> AllColors { get; set; }

    }
}
