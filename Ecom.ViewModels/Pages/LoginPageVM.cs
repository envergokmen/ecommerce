﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Ecom.Core;
using Ecom.ViewModels.Banners;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.PriceCurrencies;

namespace Ecom.ViewModels
{
    public class LoginPageVM : PageBaseVM
    {

        public RegisterViewModel registerVM;
        public LoginViewModel loginVM;

        public LoginPageVM(PriceCurrencyVM currency, LangViewModel lang, DomainViewModel domain, UserViewModel user, string controllerName = null, string actionName = null) : base(currency, lang, domain, user, controllerName, actionName)
        {

        }
    }
}
