﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Ecom.Core;
using Ecom.ViewModels.Addresses;
using Ecom.ViewModels.Banks;
using Ecom.ViewModels.Banners;
using Ecom.ViewModels.Campaign;
using Ecom.ViewModels.CargoPrices;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.Orders;
using Ecom.ViewModels.Pages;
using Ecom.ViewModels.Payments;
using Ecom.ViewModels.PriceCurrencies;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Ecom.ViewModels
{
    public class CheckoutVM : PageBaseVM
    {
        public string Step { get; set; }
        public bool CheckoutAsGuest { get; set; }

        public int? BillingAddressId { get; set; }
        public int? ShippingAddressId { get; set; }
        public int? CargoPriceId { get; set; }
        public int? WebPosIstallmentId { get; set; }
        public int? BankId { get; set; }
        public long? CreatedOrderId { get; set; }
        public long? CouponId { get; set; }

        public CargoPriceVM SelectedCargoPrice { get; set; }
        public AddressVM SelectedBillingAddress { get; set; }
        public AddressVM SelectedShippingAddress { get; set; }
        public InstallmentGrpVM SelectedWebPosInstallment { get; set; }
        public OrderDetailsVM CreatedOrder { get; set; }
        public StandartOperationResult FailResult { get; set; }
        public BankVM SelectedBank { get; set; }
        public CreditCardVM CreditCard { get; set; }
        public List<CouponVM> Coupons { get; set; }
        public CouponVM SelectedCoupon { get; set; }
        public HashSet<string> CouponWarnings { get; set; } = new HashSet<string>();
        public OperationResult<PaymentHistoryViewModel> PaymentResult { get; set; }

        public decimal CargoAmount()
        {
            decimal result = 0;

            if (this.SelectedCargoPrice != null)
            {
                result = SelectedCargoPrice.Amount;

                if (this.SelectedCargoPrice.FreeShippingLimit != null && this.SelectedCargoPrice.FreeShippingLimit < this.Cart.TotalAmount)
                {
                    result = 0;
                }

            }

            return result;
        }

        public decimal CouponAmount
        {
            get
            {
                decimal couponTotal = 0;
                if (SelectedCoupon == null || Cart == null) return couponTotal;

                if (SelectedCoupon.CalculationType == HesapLamaTuru.Yuzde && SelectedCoupon.Amount != null)
                {

                    if (this.Cart != null && this.Cart.Products.Any())
                    {
                        foreach (var item in this.Cart.Products)
                        {
                            decimal itemDiscountPercent = 0;
                            if (item.AmountOld.HasValue && item.Amount.HasValue && item.AmountOld > item.Amount)
                            {
                                itemDiscountPercent = 1 - ((item.Amount.Value / item.AmountOld.Value));

                                if ((itemDiscountPercent*100) < SelectedCoupon.Amount.Value)
                                {
                                    //discount from sale price to prevent double discount on discounted products.
                                    var newCouponRate = ((SelectedCoupon.Amount.Value / 100) - itemDiscountPercent )* 100;
                                    var itemDiscountAmount = Utils.GetPercentOfNumber(newCouponRate, item.SubTotalOld, 2);

                                    couponTotal += itemDiscountAmount;
                                    CouponWarnings.Add("BaziUrunlerIndirimliydiAma");
                                }
                                else
                                {
                                    CouponWarnings.Add("BaziUrunlerZatenIndirimliydi");
                                }
                            }
                            else
                            {
                                couponTotal += Utils.GetPercentOfNumber(SelectedCoupon.Amount ?? 0, item.SubTotal, 2);
                            }
                        }
                    }

                    //couponTotal = Utils.GetPercentOfNumber(SelectedCoupon.Amount ?? 0, Cart.TotalAmount, 2);
                }
                else
                {
                    couponTotal = SelectedCoupon.Amount ?? 0;
                }

                return couponTotal;
            }
        }

        public decimal TotalAmountWithoutInstallment()
        {
            decimal result = Cart?.TotalAmount ?? 0;
            result += CargoAmount();
            result -= CouponAmount;
            return result;
        }

        public decimal TotalAmount()
        {
            decimal result = TotalAmountWithoutInstallment();

            if (this.SelectedWebPosInstallment != null)
            {
                result += (this.SelectedWebPosInstallment.Rate > 0 ? result * (this.SelectedWebPosInstallment.Rate / 100) : 0);
            }

            return result;
        }

        public bool isPartial { get; set; }
        public CartVM Cart { get; set; }

        public List<AddressVM> Addresses { get; set; }
        public AddressVM NewAddress { get; set; }


        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> Cities { get; set; }
        public List<SelectListItem> Towns { get; set; }
        public List<CargoPriceVM> CargoPrices { get; set; }
        public List<PaymentVM> Payments { get; set; }

        public CheckoutVM(PriceCurrencyVM currency, LangViewModel lang, DomainViewModel domain, UserViewModel user, string controllerName = null, string actionName = null) : base(currency, lang, domain, user, controllerName, actionName)
        {

        }
    }
}
