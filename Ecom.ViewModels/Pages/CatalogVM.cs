﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Ecom.ViewModels.Banners;
using Ecom.ViewModels.Catalogs;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.PriceCurrencies;
using Ecom.ViewModels.Products;

namespace Ecom.ViewModels
{
    public class CatalogVM : PageBaseVM
    {
        public CatalogVM()
        {
            this.Banners = new List<BannerCreateEditVM>();
            this.Products = new List<ProductXVM>();
        }
        public CatalogCreateEditVM CurCatalog { get; set; }
        public List<ProductXVM> Products { get; set; }
        public List<FilterGroup> FilterGroups { get; set; }

        public byte Grid { get; set; }
        public int page { get; set; }
        public int pageSize { get; set; }
        public long TotalPage { get; set; }
        public long TotalItem { get; set; }
        public bool ShowPages { get; set; } = true;
        public CatalogVM(PriceCurrencyVM currency, LangViewModel lang, DomainViewModel domain, UserViewModel user, string controllerName = null, string actionName = null) : base(currency, lang, domain, user, controllerName, actionName)
        {

        }
    }
}
