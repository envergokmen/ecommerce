﻿using Ecom.ViewModels.Banners;
using Ecom.ViewModels.Catalogs;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.PriceCurrencies;
using Ecom.ViewModels.Products;
using System.Collections.Generic;

namespace Ecom.ViewModels
{
    public class PageBaseVM
    {
        public PageBaseVM()
        {
            this.Banners = new List<BannerCreateEditVM>();
        }

        public PageBaseVM(PriceCurrencyVM currency, LangViewModel lang, DomainViewModel domain, UserViewModel user, string controllerName=null, string actionName=null)
        {
            this.User = user;
            this.Domain = domain;
            this.Lang = lang;
            this.Currency = currency;
            this.ControllerName = controllerName;
            this.ActionName = actionName;
            if (this.PageTitle == null && Domain != null && Domain.DomainBrandName != null) this.PageTitle = this.Domain.DomainBrandName;
            if (this.Banners == null) this.Banners = new List<BannerCreateEditVM>();
        }
        public List<BannerCreateEditVM> Banners { get; set; }
        public List<CatalogCreateEditVM> Categories { get; set; }
        public string PageTitle { get; set; }
        public string PageKeywords { get; set; }
        public string PageDescription { get; set; }
        public string CartCount { get; set; }
        public string SearchKeyword { get; set; }
        public DomainViewModel Domain { get; set; }
        public UserViewModel User { get; set; }
        public LangViewModel Lang { get; set; }
        public PriceCurrencyVM Currency { get; set; }
        public int? UserId => this.User?.ID;
        public string SpecialKey { get; set; }

        public string LangCode => this.Lang?.LangCode ?? this.Domain?.LangCode;
        public int LangId => this.Lang?.ID ?? this.Domain?.LangID ?? 0;
        public int DomainId => this.Domain?.ID ?? 0;
        public int StoreId => this.Domain?.StoreAccountID ?? 0;

        public int CurrencyId => this.Currency!=null ? this.Currency.PriceCurrencyID: this.Domain?.PriceCurrencyID ?? 0;
        public string CurrencyCode => this.Currency != null ? this.Currency.CurrencyCode : this.Domain?.PriceCurrencyCode ?? "";
        public string CurrencySymbol => this.Currency != null ? this.Currency.CurrencySymbol : this.Domain?.PriceCurrencySymbol ?? "";

        public string IP { get; set; }
        public string Protokol { get; set; }

        public string Url { get; set; }
        public string UrlAuthority { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }

    }
}
