﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Ecom.Core;
using Ecom.ViewModels.Addresses;
using Ecom.ViewModels.Banks;
using Ecom.ViewModels.Banners;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.Orders;
using Ecom.ViewModels.PriceCurrencies;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Ecom.ViewModels
{
    public class AccountPageVM : PageBaseVM
    {
        public bool IsPartial { get; set; }
        public RegisterViewModel registerVM;
        public LoginViewModel loginVM;
        public List<OrderDetailsVM> orders;
        public List<AddressVM> addresses;
        public OrderDetailsVM orderDetail;
        public UserViewModel UserToUpdate;

        public AddressVM NewAddress { get; set; }

        public List<BankVM> Banks { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> Cities { get; set; }
        public List<SelectListItem> Towns { get; set; }
        public PaymentType? OrderType { get; set; }

        public OperationResult<UserViewModel> ChangePasswordResult { get; set; }
        public OperationResult<UserViewModel> UpdateUserResult { get; set; }

        public AccountPageVM(PriceCurrencyVM currency, LangViewModel lang, DomainViewModel domain, UserViewModel user, string controllerName = null, string actionName = null) : base(currency, lang, domain, user, controllerName, actionName)
        {

        }
    }
}
