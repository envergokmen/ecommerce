﻿using Ecom.Core;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.PriceCurrencies;
using Ecom.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecom.ViewModels.Pages
{
    public class CartVM : PageBaseVM
    {
        //public CartVM()
        //{
        //    this.Products = new List<CartProductVM>();
        //}

        public CartVM(PriceCurrencyVM currency, LangViewModel lang, DomainViewModel domain, UserViewModel user, string controllerName = null, string actionName = null) : base(currency, lang, domain, user, controllerName, actionName)
        {
            this.Products = new List<CartProductVM>();
            this.OperationResult = new StandartOperationResult();
            this.OperationResult.SetSuccessAndClearError();
        }

        public List<CartProductVM> Products { get; set; }
        public StandartOperationResult OperationResult { get; set; }

        public bool isPartial { get; set; }
        public decimal TotalAmount => Products?.Sum(c => (c.Amount ?? 0) * c.Quantity) ?? 0;
        public decimal TotalAmountWithoutDiscount => Products?.Sum(c => (c.AmountOld ?? 0) * c.Quantity) ?? 0;
        public bool HasDiscount => Products?.Any(x => x.HasDiscount) ?? false;
        public decimal MaxDiscountPercent => Products?.OrderByDescending(x => x.DiscountRate).Select(c=>c.DiscountRate).FirstOrDefault() ?? 0;

        public decimal TotalItem => Products?.Sum(c => c.Quantity) ?? 0;
        public string Key { get; set; }
    }
}
