﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Ecom.ViewModels.Banners;
using Ecom.ViewModels.Catalogs;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.PriceCurrencies;
using Ecom.ViewModels.Products;

namespace Ecom.ViewModels
{
    public class ProductDetailVM : PageBaseVM
    {
        public ProductDetailVM()
        {
            this.Banners = new List<BannerCreateEditVM>();
            this.RecommendedProducts = new List<ProductVM>();
        }
        public ProductDTVM CurProduct { get; set; }
        public List<ProductVM> RecommendedProducts { get; set; }
        public bool IsPartial { get; set; }
        public ProductDetailVM(PriceCurrencyVM currency, LangViewModel lang, DomainViewModel domain, UserViewModel user, string controllerName = null, string actionName = null) : base(currency, lang, domain, user, controllerName, actionName)
        {

        }
    }
}
