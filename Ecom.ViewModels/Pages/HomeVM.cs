﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Ecom.Core;
using Ecom.ViewModels.Banners;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.PriceCurrencies;

namespace Ecom.ViewModels
{
    public class HomeVM : PageBaseVM
    {
        public string WelcomeMessage { get; set; }
        public string FotterMessage { get; set; }

        public List<BannerCreateEditVM> MainBanners => Banners.Where(c => c.BannerShowPlace == BannerPlace.Main).ToList();
        public List<BannerCreateEditVM> MiddleBanners1 => Banners.Where(c => c.BannerShowPlace == BannerPlace.Middle).ToList();
        public List<BannerCreateEditVM> MiddleBanners2 => Banners.Where(c => c.BannerShowPlace == BannerPlace.Middle2).ToList();

        public HomeVM()
        {

        }
        public HomeVM(PriceCurrencyVM currency, LangViewModel lang, DomainViewModel domain, UserViewModel user, string controllerName = null, string actionName = null) : base(currency, lang, domain, user, controllerName, actionName)
        {

        }
    }
}
