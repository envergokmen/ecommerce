﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels
{
    public class NameValueVM<T>
    {
        public string Name { get; set; }
        public T Value { get; set; }
        public bool IsSelected { get; set; }
    }

    public class NameValueNullIntVM : NameValueVM<int?>
    {

    }

    public class NameValueIntVM : NameValueVM<int>
    {

    }
    public class NameValueNullLongVM : NameValueVM<long?>
    {

    }

    public class NameValueLongVM : NameValueVM<long>
    {

    }

    public class NameValueStringVM : NameValueVM<string>
    {

    }


    public class NameValueObjectVM : NameValueVM<object>
    {

    }

}
