﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Addresses
{
    public class TownVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CityId { get; set; }

    }
}
