﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Addresses
{
    public class CityVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { get; set; }

    }
}
