﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ecom.ViewModels.Addresses
{
    public class AddressVM
    {
        public int Id { get; set; }

        public int? UserID { get; set; }

        public string Name { get; set; }

        public string TcNo { get; set; }

        [Required]
        public int? CountryId { get; set; }
        public string CountryName { get; set; }

        public int? CityId { get; set; }

        public string CityName { get; set; }

        public int? TownId { get; set; }

        public string TownName { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [EmailAddress]
        [StringLength(100)]
        public string Email { get; set; }


        [Required]
        [StringLength(250)]
        public string Detail { get; set; }

        public int? StoreId { get; set; }
        public int? DomainId { get; set; }
        public string Session { get; set; }

    }
}
