﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Addresses
{
    public class CountryVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CountryCode { get; set; }
        public string PhoneCode { get; set; }
    }
}
