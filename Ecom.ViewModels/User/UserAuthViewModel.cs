﻿using Ecom.Core;
using Newtonsoft.Json;
using System;

namespace Ecom.ViewModels
{
    public class UserViewModel
    { 
        public int ID { get; set; }

        public Nullable<DateTime> CreatedOn { get; set; }

        public string Name { get; set; }
        public string ShortenedName()
        {
            var sname= Name;
            var splittedName = Name.Split(' ');

            if(splittedName.Length>1 && splittedName[0]!=null && splittedName[1]!=null && splittedName[0].Length>1)
            {
                return splittedName[0].Substring(0,1) + ". " + splittedName[1];
            }

            return sname;
        }

        public string UserName { get; set; } 

        [JsonIgnore]
        public string HashedPassWord { get; set; }

        public Nullable<DateTime> BirthDate { get; set; }

        public bool? AllowMail { get; set; }
        public bool? AllowSMS { get; set; }
        public string Telephone { get; set; }

        public string FbID { get; set; } 
          
        public Nullable<double> Credit { get; set; }

        public int? StoreAccountID { get; set; }
        public int? DomainId { get; set; }

        public UserType LoginType { get; set; }

        public string StoreName { get; set; }

    }
}
