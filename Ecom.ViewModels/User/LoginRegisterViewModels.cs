﻿using Ecom.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels
{ 
    public class LoginViewModel 
    {
        [Required]
        [StringLength(90)]
        public string UserName { get; set; }
         
        [StringLength(60)]
        public string PassWord { get; set; }
         
        [StringLength(60)]
        public string HashedPassWord { get; set; }

        public DateTime LastLoginDate { get; set; }

        public bool RememberMe { get; set; }

        public UserType LoginType { get; set; }

        public int StoreID { get; set; }
        public int DomainID { get; set; }
        public int PriceCurrencyID { get; set; }

    }
     
    public class ChangePasswordViewModel
    {

        //hata mesajları değişecek.
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }

        public int UserID { get; set; }
    }

    
    public class ResetPassworViewdModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }
         
        [Required]
        public string PasswordResetToken { get; set; }
         
    }

}
