﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels
{
    public class RegisterViewModel 
    { 
        [Required]
        [EmailAddress]
        [StringLength(90)]
        public string UserName { get; set; }

        [Required]
        [StringLength(90)]
        public string Name { get; set; }

        [Required] 
        [StringLength(60)]
        public string PassWord { get; set; }
          
        [StringLength(40)]
        public string IP { get; set; }

        public int ID { get; set; }
        public bool AllowTerms { get; set; }

        public int StoreId { get; set; }
        public int DomainId { get; set; }

        public string Phone { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool AllowMail { get; set; }
        public bool AllowSMS { get; set; }

    }
     
    public class UpdateUserViewModel 
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [StringLength(90)]
        public string Name { get; set; }
         
        [StringLength(40)]
        public string IP { get; set; }

        public bool AllowTerms { get; set; }
        public int StoreId { get; set; }
        public int DomainId { get; set; }

        public string Phone { get; set; }
        public DateTime? BirthDate { get; set; }

        public bool AllowMail { get; set; }
        public bool AllowSMS { get; set; }
    }
     
    public class RegisterStoreStoreVM : PageBaseVM
    {
        [Required]
        [EmailAddress]
        [StringLength(90)]
        public string UserName { get; set; }

        [Required]
        [StringLength(90)]
        public string Name { get; set; }

        [Required]
        [StringLength(60)]
        public string PassWord { get; set; }

        [StringLength(40)]
        public string IP { get; set; }
         
        public bool AllowTerms { get; set; }
         
        [Required]
        [StringLength(50)]
        public string StoreName { get; set; }

        //[Required]
        [StringLength(100)]
        public string CompanyName { get; set; }
          
        public string CompanyTaxInfo { get; set; }

        [EmailAddress]
        [StringLength(100)]
        public string CompanyEmail { get; set; }
         
        [StringLength(100)]
        public string CompanyAddress { get; set; }

        [StringLength(100)]
        public string CompanyPhone { get; set; }

        [Required]
        public int? CountryId { get; set; }

        public int? CityId { get; set; }

        public int? TownId { get; set; }


    }
}
