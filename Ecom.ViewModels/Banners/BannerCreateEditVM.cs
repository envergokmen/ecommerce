﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Banners
{
    public class BannerCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }
         
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(250)]
        public string Link { get; set; }

        [StringLength(250)]
        public string ImagePath { get; set; }

        public BannerPlace BannerShowPlace { get; set; }
        public int SortNo { get; set; }
        public int? StoreAccountID { get; set; }
        public int? LangID { get; set; }
        public int? DomainID { get; set; }
        public Status PStatus { get; set; }

        [DataType(DataType.Upload)]
        public IFormFile file { get; set; }


        public string LangCode { get; set; }
        public string DomainName { get; set; }

        public List<NameValueStringVM> AllBannerPlaces { get; set; }
        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueIntVM> AllLangs { get; set; }
        public List<NameValueIntVM> AllDomains { get; set; }


    }
}
