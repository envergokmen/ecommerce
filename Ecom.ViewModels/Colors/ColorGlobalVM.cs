﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Colors
{
    public class ColorGlobalVM
    {
        public int ID { get; set; }

        public int SortNo { get; set; }

        public string Name { get; set; }

        public string HexValue { get; set; }

    }
}
