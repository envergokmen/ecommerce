﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Colors
{
    public class ColorCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string ColorDescription { get; set; }

        [StringLength(15)]
        public string ColorCode { get; set; }

        [StringLength(150)]
        public string PatternPath { get; set; }

        [StringLength(7)]
        public string HexValue { get; set; }

        public int ColorGlobalID { get; set; }

        public int? SortNo { get; set; }

        public Status PStatus { get; set; }

        public int? StoreAccountID { get; set; }

        public List<NameValueIntVM> AllColorGlobals { get; set; }
        public List<NameValueStringVM> AllStatuses { get; set; }

        [DataType(DataType.Upload)]
        public IFormFile file { get; set; }

    }
}
