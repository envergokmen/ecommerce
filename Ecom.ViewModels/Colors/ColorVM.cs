﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.ViewModels.Colors
{
    public class ColorVM
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public string ColorDescription { get; set; }
        public string ColorCode { get; set; }
        public string PatternPath { get; set; }
        public string HexValue { get; set; }

        public int? SortNo { get; set; }

        public string ColorGlobalName { get; set; }
        public string ColorGlobalHexValue { get; set; }
        public int ColorGlobalID { get; set; }

        public int? StoreAccountID { get; set; }


    }
}
