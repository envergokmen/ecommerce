﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.EmailTemplates
{
    public class EmailTemplateCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        public Status PStatus { get; set; }

        public SendingContentType MailType { get; set; }

        public string MailTypeText => MailType.ToString();

        public int DomainID { get; set; }

        public int EmailProviderID { get; set; }
        public int Translations { get; set; }

        public List<NameValueStringVM> AllStatuses { get; set; }
        public List<NameValueStringVM> AllMailTypes { get; set; }
        public List<NameValueIntVM> AllProviders { get; set; }
        public List<NameValueIntVM> AllDomains { get; set; }

    }
}
