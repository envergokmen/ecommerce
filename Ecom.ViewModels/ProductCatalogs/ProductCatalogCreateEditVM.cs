﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Catalogs
{
    public class ProductCatalogCreateEditVM : BaseViewModel
    {
        public long ProductID { get; set; }

        public List<ProductForCatalogVM> ProductColors { get; set; }
        public List<CatalogCreateEditVM> Catalogs { get; set; }
        public List<ProductInCatalogVM> AddedCatalogs { get; set; }
    }

    public class ProductForCatalogVM
    {
        public long ProductID { get; set; }
        public string ColorName { get; set; }
        public string ImagePath { get; set; }
        public int ColorID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public bool IsChecked { get; set; }
    }
     
    public class ProductInCatalogVM
    {
        public int ID { get; set; }
        public long ProductID { get; set; }
        public int CatalogID { get; set; }
        public string ColorName { get; set; }
        public string CatalogName { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public int? ColorID { get; set; }
        public int? ColorGlobalID { get; set; }
        public int SortNo { get; set; }
        public string ImagePath { get; set; }
        public int StoreID { get; set; }
    }

    public class AddToCatalogVM : BaseViewModel
    {
        public int ProductID { get; set; }
        public int[] ColorIDs { get; set; }
        public int[] CatalogIDs { get; set; }
    }

    public class CatalogCatalogOrderVM : BaseViewModel
    {
        public int CatalogID { get; set; }
        public List<ProductInCatalogVM> Items { get; set; }
    }
}
