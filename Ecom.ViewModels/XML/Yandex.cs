﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Ecom.ViewModels.XML
{
    [XmlRoot(ElementName = "product")]
    [XmlType(TypeName = "product")]
    public class XMLProductYandex
    {
        public string producturl { get; set; }
        public string name { get; set; }
        public string productCode { get; set; }
        public string barcode { get; set; }

        public string brand { get; set; }

        public string category1 { get; set; }
        public string category2 { get; set; }
        public string category3 { get; set; }

        [XmlElement("images")]
        public YandexImages Images { get; set; }

        public string sizeName { get; set; }
        public string colorName { get; set; }
        public string colorGlobalName { get; set; }
        public double quantity { get; set; }

        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> retailprice { get; set; }

        public double discount { get; set; }

        private CDATA _description;
        [XmlElement("description", typeof(CDATA))]

        public CDATA description
        {
            get { return _description; }
            set { _description = value; }
        }
    }

    public class YandexImages
    {
        [XmlElement("image")]
        public List<string> images { get; set; }

    }
}
