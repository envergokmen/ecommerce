﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Ecom.ViewModels.XML
{
    [XmlRoot(ElementName = "product")]
    [XmlType(TypeName = "product")]
    public class XMLStockGeneral
    {
        public string producturl { get; set; }
        public string name { get; set; }
        public string productCode { get; set; }
        public string barcode { get; set; }

        public string brand { get; set; }

        public string category1 { get; set; }
        public string category2 { get; set; }
        public string category3 { get; set; }

        [XmlElement("images")]
        public XmlStockImages Images { get; set; }

        public string sizeName { get; set; }
        public string colorName { get; set; }
        public string colorGlobalName { get; set; }
        public double quantity { get; set; }

        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> retailprice { get; set; }

        public double discount { get; set; }

        private CDATA _description;
        [XmlElement("description", typeof(CDATA))]

        public CDATA description
        {
            get { return _description; }
            set { _description = value; }
        }


    }

    public class XmlStockImages
    {
        [XmlElement("image")]
        public List<string> images { get; set; }

    }

}