﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Ecom.ViewModels.XML
{
    public class CDATA : IXmlSerializable
    {

        private string text;

        public CDATA()
        { }

        public CDATA(string text)
        {
            this.text = text;
        }

        public string Text
        {
            get { return text; }
        }

        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        void IXmlSerializable.ReadXml(XmlReader reader)
        {


            this.text = reader.ReadString();
            reader.Read(); // change in .net 2.0,
            // without this line, you will lose value of all other fields
        }

        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            //writer.Settings = new XmlWriterSettings();
            //writer.Settings.Encoding = System.Text.Encoding.UTF8;

            writer.WriteCData(this.text);
        }
        public override string ToString()
        {
            return this.text;
        }
    }
}
