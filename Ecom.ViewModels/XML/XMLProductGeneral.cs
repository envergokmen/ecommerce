﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Ecom.ViewModels.XML
{

    [XmlRoot(ElementName = "products")]
    public class XMLProductGeneralList
    {
        public XMLProductGeneralList()
        {
            this.products = new List<XMLProductGeneral>();
        }

        [XmlElement("product")]
        public List<XMLProductGeneral> products { get; set; }
    }

    [XmlType("product")]
    public class XMLProductGeneral
    {
        public XMLProductGeneral()
        {
            this.Images = new List<XMlProductImage>();
            this.Stocks = new List<XmlStockItem>();
        }
        [XmlElement(IsNullable =true)]
        public string producturl { get; set; }

        [XmlElement(IsNullable =true)]
        public string name { get; set; }

        [XmlElement(IsNullable = true)]
        public string productCode { get; set; }

        [XmlElement(IsNullable = true)]
        public string brand { get; set; }

        [XmlElement(IsNullable = true)]
        public string category1 { get; set; }

        [XmlElement(IsNullable = true)]
        public string category2 { get; set; }

        [XmlElement(IsNullable = true)]
        public string category3 { get; set; }

        [XmlArray("images")]
        public List<XMlProductImage> Images { get; set; }
        //public XmlImageContainer Images { get; set; }

        [XmlArray("stocks")]
        public List<XmlStockItem> Stocks { get; set; }

        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> retailprice { get; set; }

        public Nullable<decimal> discount { get; set; }

        private CDATA _description;
        [XmlElement("description", typeof(CDATA))]

        public CDATA description
        {
            get { return _description; }
            set { _description = value; }
        }

    }

    [XmlType("stock")]
    public class XmlStockItem
    {
        public Nullable<int> quantity { get; set; }

        [XmlElement(IsNullable = true)]
        public string colorName { get; set; }

        [XmlElement(IsNullable = true)]
        public string barcode { get; set; }

        [XmlElement(IsNullable = true)]
        public string sizeName { get; set; }

        [XmlElement(IsNullable = true)]
        public string colorglobalName { get; set; }
    }

    [XmlType("image")]
    public class XMlProductImage
    {
        [XmlElement(IsNullable = true)]
        public string url { get; set; }

        [XmlElement(IsNullable = true)]
        public string colorName { get; set; }

        [XmlElement(IsNullable = true)]
        public int? angle { get; set; }
    }

}