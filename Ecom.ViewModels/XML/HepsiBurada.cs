﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Ecom.ViewModels.XML
{

    [XmlRoot(ElementName = "Urun")]
    [XmlType(TypeName = "Urun")]
    public class XMLProductHepsiBurada
    {
        public int UrunID { get; set; }
        public string Kod { get; set; }
        public string Baslik { get; set; }

        private CDATA _aciklama;

        [XmlElement("Aciklama", typeof(CDATA))]
        public CDATA Aciklama
        {
            get { return _aciklama; }
            set { _aciklama = value; }
        }

        public string mainCategory { get; set; }
        public string Marka { get; set; }
        public decimal Fiyat { get; set; }
        public decimal PiyasaFiyat { get; set; }
        public decimal KDV { get; set; }
        public string Link { get; set; }

        public string ImageName1 { get; set; }
        public string ImageName2 { get; set; }
        public string ImageName3 { get; set; }
        public string ImageName4 { get; set; }
        public string ImageName5 { get; set; }

        public string Kur { get; set; }
        public int StokAdedi { get; set; }

        [XmlElement("Stoklar")]
        public List<HepsiBuradaStok> Stoklar { get; set; }

    }

    [XmlRoot("Stok")]
    public class HepsiBuradaStok
    {
        public string Renk { get; set; }
        public string Beden { get; set; }
        public string barcode { get; set; }
        public string merchantsku { get; set; }
        public int miktar { get; set; }
    }
}
