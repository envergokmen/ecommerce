﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ecom.ViewModels.XML
{
    [XmlRoot(ElementName = "Urun")]
    [XmlType(TypeName = "Urun")]
    public class XMLProductForN11
    {
        public string GTIN { get; set; }
        private CDATA _aciklama;

        [XmlElement("Aciklama", typeof(CDATA))]
        public CDATA Aciklama
        {
            get { return _aciklama; }
            set { _aciklama = value; }
        }

        public string ProductStyleName { get; set; }

        public string DetailHTML { get; set; }

        public string SeasonName { get; set; }

        public string SeasonYear { get; set; }

        public string Barcode { get; set; }

        public string DepartmentName { get; set; }

        public string ColorName { get; set; }

        public string SizeName { get; set; }

        public string ProductUrl { get; set; }

        public string ProductCategories { get; set; }

        public int Quantity { get; set; }

        public int StockCode { get; set; }

        public Nullable<double> Amount { get; set; }

        public Nullable<double> AmountOld { get; set; }

        public Nullable<double> AmountWithoutVAT { get; set; }
        public Nullable<int> VAT { get; set; }

        public List<N11Stok> Stoklar { get; set; }

        public string ImagePath { get; set; }
        public string ImageName1 { get; set; }
        public string ImageName2 { get; set; }
        public string ImageName3 { get; set; }
        public string ImageName4 { get; set; }
        public string ImageName5 { get; set; }

        public int ColorID { get; set; }
 
        public int UrunID { get; set; }
        public string Kod { get; set; }
        public string Baslik { get; set; }

        public string mainCategory { get; set; }
        public string Marka { get; set; }
        public decimal Fiyat { get; set; }
        public decimal PiyasaFiyat { get; set; }
        public decimal KDV { get; set; }
        public string Link { get; set; }

        public string Kur { get; set; }
        public int StokAdedi { get; set; }

    }

    [XmlRoot("Stok")]
    public class N11Stok
    {
        public string Renk { get; set; }
        public string Beden { get; set; }
        public string barcode { get; set; }
        public string merchantsku { get; set; }
        public int miktar { get; set; }
    }

}
