﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Ecom.ViewModels.XML
{
    public class GoogleMerchantXML
    {
        public string title { get; set; }
        public string link { get; set; }
        public string updated { get; set; }

        public entry entries { get; set; }
    }

    public class entry
    {
        public string gid { get; set; }
        public string gtitle { get; set; }

        public string description { get; set; }

        public string glink { get; set; }
        public string gimage_link { get; set; }
        public string gcondition { get; set; }
        public string gavailability { get; set; }
        public string gprice { get; set; }

        [XmlElement("g_x003A_shipping")]
        public gshipping gshipping { get; set; }


        public string ggtin { get; set; }
        public string gbrand { get; set; }
        public string gmpn { get; set; }
        public string ggoogle_product_category { get; set; }
        public string gproduct_type { get; set; }
    }

    public class gshipping
    {
        public string gcountry { get; set; }
        public string gservice { get; set; }
        public string gprice { get; set; }
    }

}
