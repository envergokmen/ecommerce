﻿using Ecom.Core;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Ecom.ViewModels.Sizes
{
    public class SizeCreateEditVM : BaseViewModel
    {
        public int ID { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }
  
        public int SortNo { get; set; }

        public Status PStatus { get; set; }

        public int? StoreAccountID { get; set; }
        
    }
}
