﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.ViewModels.Pages;
using Ecom.Core;
using System;
using Ecom.Web.Helpers;

namespace Ecom.Web.Controllers
{
    public class CartController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly CatalogService catalogService;
        private readonly CartService cartService;
        private readonly ProductService productService;
        private readonly CheckoutStateHelper checkState;
        private readonly CouponService couponService;

        public CartController(EcomContext db,
            IMemberShipService _memberShipService,
            ProductService _productService,
            DomainService _domainService,
            CartService _cartService,
        IStringLocalizer<HomeController> localizer,
            CatalogService _catalogService,
            CheckoutStateHelper _checkState,
             CouponService _couponService,
            LangService _langService,
            PriceCurrencyService _currencyService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            catalogService = _catalogService;
            cartService = _cartService;
            productService = _productService;
            this.checkState = _checkState;
            this.couponService = _couponService;

        }

        public IActionResult Add(string lang, int productId, int? sizeId, int? colorId, int? stockId)
        {
            HttpContext.Session.Remove("CartCount");
            var specialKey = this.memberShipService.GetSpecialKeyForUser();

            CartVM model = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            model = cartService.Add(model, this.storeId, this.priceCurrencyId, productId, this.userIdNullable, this.langId, specialKey, sizeId, colorId, stockId);

            checkState.SetCouponId(null);
            HttpContext.Session.SetString("CartCount", Convert.ToString(model?.TotalItem ?? 0));

            return PartialView("Cart", model);
        }

        public IActionResult Remove(long id, CartViewType viewType = CartViewType.Cart)
        {
            HttpContext.Session.Remove("CartCount");

            var specialKey = this.memberShipService.GetSpecialKeyForUser();

            CartVM model = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

              model = cartService.Remove(model, this.storeId, id, this.userIdNullable, specialKey, this.priceCurrencyId);

            HttpContext.Session.SetString("CartCount", Convert.ToString(model?.TotalItem ?? 0));
           
            if (viewType == CartViewType.CheckOutCart)
            {
                CheckoutVM checkoutModel = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

                checkState.SetBaseItems(this.storeId, this.priceCurrencyId, this.userId, memberShipService.GetSpecialKeyForUser(), this.domainIdNotNull, this.lang, this.currency, model);
                checkState.SetCouponId(null);
                //checkoutModel.CouponId = checkState.GetCouponId();
                //checkoutModel.SelectedCoupon = couponService.GetCoupon(this.storeId, checkoutModel.CouponId ?? 0, null, this.priceCurrencyId, this.domainIdNotNull);
                checkoutModel.Coupons = couponService.GetUserCoupons(this.storeId, this.userId, this.priceCurrencyId, this.domainIdNotNull);

                checkoutModel.Cart = model;
                checkoutModel.isPartial = true;
                return PartialView("~/Views/Checkout/cart.cshtml", checkoutModel);
            }

            return PartialView("Cart", model);
        }

        public IActionResult SetQuantity(long id, byte quantity, CartViewType viewType= CartViewType.Cart)
        {
            HttpContext.Session.Remove("CartCount");
            CartVM model = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            var specialKey = this.memberShipService.GetSpecialKeyForUser();
            model = cartService.SetQuantity(model, this.storeId, id, quantity, this.userIdNullable, specialKey, this.priceCurrencyId);

            HttpContext.Session.SetString("CartCount", Convert.ToString(model?.TotalItem ?? 0));

            if(viewType== CartViewType.CheckOutCart)
            {

                CheckoutVM checkoutModel = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

                checkState.SetBaseItems(this.storeId, this.priceCurrencyId, this.userId, memberShipService.GetSpecialKeyForUser(), this.domainIdNotNull, this.lang, this.currency, model);
                checkState.SetCouponId(null);

                //checkoutModel.CouponId = checkState.GetCouponId();
                //checkoutModel.SelectedCoupon = couponService.GetCoupon(this.storeId, checkoutModel.CouponId ?? 0, null, this.priceCurrencyId, this.domainIdNotNull);
                checkoutModel.Coupons = couponService.GetUserCoupons(this.storeId, this.userId, this.priceCurrencyId, this.domainIdNotNull);

                checkoutModel.Cart = model;
                checkoutModel.isPartial = true;
                return PartialView("~/Views/Checkout/cart.cshtml", checkoutModel);
            }

            return PartialView("Cart", model);
        }

        public IActionResult Get(string lang)
        {
            var specialKey = this.memberShipService.GetSpecialKeyForUser();
            CartVM model = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model = cartService.Get(model, this.storeId, this.userIdNullable, specialKey, this.priceCurrencyId);

            return PartialView("Cart", model);
        }

        public IActionResult GetCount(string lang)
        {
            var count = HttpContext.Session.GetString("CartCount");

            if (string.IsNullOrEmpty(count))
            {
                var specialKey = this.memberShipService.GetSpecialKeyForUser();
                count = cartService.GetCount(this.storeId, this.userIdNullable, specialKey, this.priceCurrencyId).ToString();
                HttpContext.Session.SetString("CartCount", count);
            }

            return Content(count);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
