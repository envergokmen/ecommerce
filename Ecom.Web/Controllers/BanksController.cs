﻿using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecom.Controllers
{
    public class BanksController : EcomBaseController
    {
        private readonly PaymentService paymentService;
        public BanksController(
            IMemberShipService _memberShipService,
            LangService _langService,
            DomainService _domainService,
            PaymentService _paymentService,
            PriceCurrencyService _currencyService
            ) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.paymentService = _paymentService;
        }


        public IActionResult Index()
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.Banks = paymentService.GetBanks(this.storeId, this.domainIdNotNull);

            return View(model);
        }



    }
}