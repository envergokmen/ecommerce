﻿using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecom.Controllers
{
    public class AccountController : EcomBaseController
    {
        private readonly OrderService orderService;
        public AccountController(
            IMemberShipService _memberShipService,
            LangService _langService,
            DomainService _domainService,
            OrderService _orderService,
            PriceCurrencyService _currencyService
            ) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.orderService = _orderService;
        }


        public IActionResult Index()
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            return View(model);
        }


        public IActionResult ChangePassword()
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            return View(model);
        }

        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordViewModel changePasswordModel)
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            changePasswordModel.UserID = this.userId;

            var result = memberShipService.ChangeUserPassWord(changePasswordModel);
            model.ChangePasswordResult = result;

            return View(model);
        }



        public IActionResult Update()
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.UserToUpdate = memberShipService.GetSignedUser();

            return View(model);
        }

        [HttpPost]
        public IActionResult Update(UpdateUserViewModel updateUserModel)
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            updateUserModel.ID = this.userId;
            updateUserModel.StoreId = this.storeId;
            updateUserModel.DomainId = this.domain?.ID ?? 0;

            var result = memberShipService.Update(updateUserModel);
            model.UserToUpdate = memberShipService.GetSignedUser();

            model.UpdateUserResult = result;

            return View(model);
        }





        //[HttpPost]
        //[Route("{lang}/login")]
        //[Route("login")]
        //public IActionResult Login(LoginViewModel loginVM)
        //{
        //    if (loginVM != null) loginVM.RememberMe = false;
        //    loginVM.RememberMe = true;

        //    loginVM.StoreID = this.storeId;
        //    loginVM.DomainID = this.domain?.ID ?? 0;

        //    LoginPageVM model = new LoginPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

        //    OperationResult<UserViewModel> loginResult = memberShipService.Login(loginVM);

        //    if (!loginResult.Status)
        //    {
        //        ViewBag.LoginErrors = loginResult.GetErrors();
        //        model.loginVM = loginVM;

        //        return View(model);
        //    }

        //    HttpContext.Session.Remove("CartCount");
        //    return RedirectToAction("Index", "Home");
        //}



    }
}