﻿using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecom.Controllers
{
    public class OrdersController : EcomBaseController
    {
        private readonly OrderService orderService;

        public OrdersController(
            IMemberShipService _memberShipService,
            LangService _langService,
            DomainService _domainService,
            OrderService _orderService,
            PriceCurrencyService _currencyService
            ) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.orderService = _orderService;
        }


        public IActionResult Index(PaymentType? orderType)
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.OrderType = orderType;

            model.orders = orderService.GetUserOrders(this.storeId, this.userId, null, null, orderType);
            return View(model);
        }

        public IActionResult details(long id, string trackCode=null)
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.orderDetail = orderService.GetOrderDetails(this.storeId, id, trackCode, this.userId, null, null);

            return View(model);
        }



    }
}