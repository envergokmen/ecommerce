﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.MvcCore.Extensions;

namespace Ecom.Web.Controllers
{
    public class NewsLetterController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly NewsLetterService newsletterService;

        public NewsLetterController(EcomContext db,
            IMemberShipService _memberShipService,
            DomainService _domainService,
            NewsLetterService _newsLetterService,
            IStringLocalizer<HomeController> localizer,
            CatalogService _catalogService,
            LangService _langService,
            PriceCurrencyService _currencyService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            newsletterService = _newsLetterService;
        }

        public IActionResult Add(string Email)
        {
            var addResult = newsletterService.Add(Email, this.IP, this.domainIdNull, this.langId, this.storeId, this.userIdNullable);
            return Json(addResult);
        }

        public IActionResult Remove(string Email)
        {
            var addResult = newsletterService.Remove(Email, this.storeId, this.userIdNullable);
            return Json(addResult);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
