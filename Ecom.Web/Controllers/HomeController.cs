﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.MvcCore.Extensions;

namespace Ecom.Web.Controllers
{
    public class HomeController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly CatalogService catalogService;
        private readonly BannerService bannerService;

        public HomeController(EcomContext db,
            IMemberShipService _memberShipService,
            DomainService _domainService, 
            BannerService _bannerService,
            IStringLocalizer<HomeController> localizer,
            CatalogService _catalogService,
            LangService _langService,
            PriceCurrencyService _currencyService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            catalogService = _catalogService;
            bannerService = _bannerService;
        }

         

        public IActionResult Index()
        {
            HomeVM model = new HomeVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            model.CartCount = HttpContext.Session.GetString("CartCount");
            model.PageTitle = $"{this.domain.DomainBrandName} {this.domain.DomainSlogan}";
            model.Banners = bannerService.GetAllActiveBanners(this.storeId);
            model.Categories = catalogService.GetAllCatalogsForLang(this.storeId, this.langId, true, Core.CatalogType.Product);
            model.WelcomeMessage = _localizer["WelcomeMessage"];

            if (Request.IsAjaxRequest())
            {
                return PartialView(model);
            }

            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
