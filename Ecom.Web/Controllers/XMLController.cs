﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.MvcCore.Extensions;
using Microsoft.AspNetCore.Mvc.Formatters.Xml.Extensions;
using System.Collections.Generic;
using Ecom.ViewModels.XML;

namespace Ecom.Web.Controllers
{
    public class XMLController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly XMlService xmlService;

        public XMLController(EcomContext db,
            IMemberShipService _memberShipService,
            DomainService _domainService,
           XMlService _xmlService,
            IStringLocalizer<HomeController> localizer,
            LangService _langService,
            PriceCurrencyService _currencyService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            xmlService = _xmlService;
        }

        [HttpGet("xml")]
        public IActionResult Index()
        {
            XMLProductGeneralList xml = xmlService.GetGeneralXML(this.storeId, this.priceCurrencyId, langId, Core.MarketPlaceType.General);
            return new XmlResult(xml);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
