﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.MvcCore.Extensions;
using System.Linq;

namespace Ecom.Web.Controllers
{
    public class CatalogController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly CatalogService catalogService;
        private readonly BannerService bannerService;
        private readonly ProductService productService;

        public CatalogController(EcomContext db,
            IMemberShipService _memberShipService,
            ProductService _productService,
            DomainService _domainService,
            BannerService _bannerService,
        IStringLocalizer<HomeController> localizer,
            CatalogService _catalogService,
            LangService _langService,
             PriceCurrencyService _currencyService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            catalogService = _catalogService;
            bannerService = _bannerService;
            productService = _productService;
        }


        public IActionResult Index(string catalogUrl, string lang, int page=1, int pageSize = 16)
        {
            CatalogVM model = new CatalogVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.PageTitle = $"{this.domain.DomainBrandName} {this.domain.DomainSlogan}";
            model.CartCount = HttpContext.Session.GetString("CartCount");
            model.CurCatalog = catalogService.GetCatalogForRoute(this.storeId, catalogUrl, null);

            if (model.CurCatalog != null)
            {
                this.lang = model.Lang = langService.GetLangById(model.CurCatalog.LangID ?? this.langId, this.storeId);
                //model.Banners = bannerService.GetAllActiveBanners(this.storeId);

                // var prds = productService.GetFilteredProductElastic(this.storeId, this.priceCurrencyId, this.langId, model.CurCatalog.ID);

                var pageProducts = productService.GetFilteredProductElastic(this.storeId, this.priceCurrencyId, this.langId, model.CurCatalog.ID, page, pageSize);

                model.Products = pageProducts.Result;
                model.page = pageProducts.Page;
                model.pageSize = pageProducts.PageSize;
                model.TotalItem = pageProducts.TotalItem;
                model.TotalPage = pageProducts.TotalPage;


                //productService.GetFilteredProducts(this.storeId, this.priceCurrencyId, this.langId, model.CurCatalog.ID).Result;
                //model.FilterGroups = productService.GetFilterItems(this.storeId, this.priceCurrencyId, this.langId, model.CurCatalog.ID);

                GetFilters(model);

                model.Categories = catalogService.GetAllCatalogsForLang(this.storeId, this.langId, true, Core.CatalogType.Product);

            }

            SetMetaItems(model);


            if (Request.IsAjaxRequest())
            {
                return PartialView(model);
            }

            return View(model);
        }

        private void GetFilters(CatalogVM model)
        {
            if (model.CurCatalog.Filters != null && model.CurCatalog.Filters.Any())
            {
                model.FilterGroups = model.CurCatalog.Filters.Where(c => c.PriceCurrencyId == priceCurrencyId && c.LangId == this.langId).ToList();
            }
            else
            {
                model.FilterGroups = productService.GetFilterItems(this.storeId, this.priceCurrencyId, this.langId, model.CurCatalog.ID);
            }

            model.Grid = model.FilterGroups?.Where(c => c.FilterType == "g").FirstOrDefault()?.Items?.Where(c => c.IsSelected == true).Select(c => (byte)c.Id).FirstOrDefault() ?? 6;
        }

        public IActionResult Products(
         int c,
         int?[] d = null,
         int?[] s = null,
         int?[] cl = null,
         int?[] sz = null,
         byte g = 8,
         int p = 1,
         int ps = 16,
         string searchKeyWord = null)
        {
            CatalogVM model = new CatalogVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            var pageProducts = productService.GetFilteredProductElastic(

                this.storeId,
                this.priceCurrencyId,
                this.langId,
                c,
                 p,
                 ps,
                  d,
                   s,
                   cl,
                   sz,
                   searchKeyWord
                );

            model.Products = pageProducts.Result;
            model.page = pageProducts.Page;
            model.pageSize = pageProducts.PageSize;
            model.TotalItem = pageProducts.TotalItem;
            model.TotalPage = pageProducts.TotalPage;
            model.ShowPages = false;

            model.Grid = g;

            return PartialView("_Products", model);

        }

        private static void SetMetaItems(CatalogVM model)
        {
            if (model.CurCatalog != null)
            {
                if (!string.IsNullOrWhiteSpace(model.CurCatalog.Title))
                {
                    model.PageTitle = model.CurCatalog.Title;
                }
                else
                {
                    model.PageTitle = model.CurCatalog.Name;
                }

                model.PageDescription = model.CurCatalog.MetaDescription;
                model.PageKeywords = model.CurCatalog.MetaKeywords;
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
