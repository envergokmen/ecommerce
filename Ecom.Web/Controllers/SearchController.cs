﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.MvcCore.Extensions;
using System.Linq;

namespace Ecom.Web.Controllers
{
    public class SearchController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly CatalogService catalogService;
        private readonly BannerService bannerService;
        private readonly ProductService productService;

        public SearchController(EcomContext db,
            IMemberShipService _memberShipService,
            ProductService _productService,
            DomainService _domainService,
            BannerService _bannerService,
        IStringLocalizer<HomeController> localizer,
            CatalogService _catalogService,
            LangService _langService,
             PriceCurrencyService _currencyService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            catalogService = _catalogService;
            bannerService = _bannerService;
            productService = _productService;
        }


        public IActionResult Index(string catalogUrl, string lang, string q, int page = 1, int pageSize = 16)
        {
            SearchVM model = new SearchVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.PageTitle = $"\"{q}\" search results {this.domain.DomainBrandName} {this.domain.DomainSlogan}";
            model.CartCount = HttpContext.Session.GetString("CartCount");

            
            var pageProducts = productService.GetFilteredProductElastic(this.storeId, this.priceCurrencyId, this.langId, 0, page, pageSize, searchKeyWord: q);

            model.Products = pageProducts.Result;
            model.page = pageProducts.Page;
            model.pageSize = pageProducts.PageSize;
            model.TotalItem = pageProducts.TotalItem;
            model.TotalPage = pageProducts.TotalPage;
            model.ShowPages = true;
            model.SearchKeyword = q;
            model.Grid = 6;

            //GetFilters(model);
            model.Categories = catalogService.GetAllCatalogsForLang(this.storeId, this.langId, true, Core.CatalogType.Product);

            if (Request.IsAjaxRequest())
            {
                return PartialView(model);
            }

            return View(model);
        }

        //private void GetFilters(CatalogVM model)
        //{
        //   model.FilterGroups = productService.GetFilterItems(this.storeId, this.priceCurrencyId, this.langId, model.CurCatalog.ID);
        //    model.Grid = model.FilterGroups?.Where(c => c.FilterType == "g").FirstOrDefault()?.Items?.Where(c => c.IsSelected == true).Select(c => (byte)c.Id).FirstOrDefault() ?? 6;
        //}

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
