﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.ViewModels.Pages;
using Ecom.Core;
using System;
using Ecom.Web.Helpers;
using System.Linq;
using Ecom.Services.CheckoutServices;
using _PosnetDotNetTDSOOSModule;
using Ecom.ViewModels.Payments;

namespace Ecom.Web.Controllers
{
    public class PaymentController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<SharedDataResource> _localizer;
        private readonly CatalogService catalogService;
        private readonly CartService cartService;
        private readonly ProductService productService;
        private readonly AddressService addressService;
        private readonly CheckoutStateHelper checkState;
        private readonly CargoPriceService cargoPriceService;
        private readonly CouponService couponService;
        private readonly CampaignService campaignService;
        private readonly OrderService orderService;

        private readonly PaymentService paymentService;

        public PaymentController(EcomContext db,
            IMemberShipService _memberShipService,
            ProductService _productService,
            DomainService _domainService,
            CartService _cartService,
        IStringLocalizer<SharedDataResource> localizer,
            CatalogService _catalogService,
            LangService _langService,
            CheckoutStateHelper _checkState,
            AddressService _addressService,
            CargoPriceService _cargoPriceService,
            PriceCurrencyService _currencyService,
             PaymentService _paymentService,
             OrderService _orderService, 
             CouponService _couponService,
             CampaignService _campaignService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            catalogService = _catalogService;
            cartService = _cartService;
            productService = _productService;
            this.checkState = _checkState;
            this.addressService = _addressService;
            this.cargoPriceService = _cargoPriceService;
            this.paymentService = _paymentService;
            this.orderService = _orderService;
            this.couponService = _couponService;
            this.campaignService = _campaignService;

        }

        public IActionResult progress()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            SetCheckoutModelFromSession(model);

            return View(model);

        }


        /// <summary>
        /// EST Redirect Action
        /// </summary>
        /// <returns></returns>
        //[HttpToHttpsRedirectAttribute]
        public ActionResult Garanti3DSecure()
        {

            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.Protokol = this.Protokol;
            model.UrlAuthority = this.UrlAuthority;
            model.Url = this.curUrl;

            SetCheckoutModelFromSession(model);

            if (model.CreatedOrder != null)
            {
                return View(model);

            }
            else
            {
                var result = new StandartOperationResult();
                result.SetError(_localizer["OdemeIcinSiparisBulunamadi"]);
                checkState.SetFailResult(result);
                return Redirect(String.Concat("~/", model.LangCode, "/Checkout/fail"));
            }

        }

        /// <summary>
        /// EST Redirect Action
        /// </summary>
        /// <returns></returns>
        //[HttpToHttpsRedirectAttribute]
        //[ReloadOnCheckoutStateEnd(RedirectOnHttpRequest = true)]
        public ActionResult Garanti3DSecureResult()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            SetCheckoutModelFromSession(model);

            //checkState.SetIsLocked(false);

            PaymentChargeServiceFactory paymentChargeFactory = new PaymentChargeServiceFactory(model, orderService, checkState, contextAccessor);
            IPaymentChargeService chargeService = paymentChargeFactory.GetChargeService(model.SelectedWebPosInstallment.PosPaymentType);

            if (chargeService == null)
            {
                var failResult = new StandartOperationResult();
                failResult.SetError("Service cannot be found for this payment type");
                checkState.SetFailResult(failResult);

                return Redirect(String.Concat("/" + model.LangCode+ "/checkout/fail"));
            }

            var paymentResult = chargeService.MakePayment();

            //Success Payment
            if (paymentResult.PaymentResult != null && paymentResult.PaymentResult.Status)
            {
                orderService.SetSuccessOrder(model.CreatedOrder);
                return Redirect(String.Concat("/" + model.LangCode + "/checkout/success"));
            }
            else
            {
                var failResult = new StandartOperationResult();
                failResult.SetError(paymentResult.PaymentResult.GetErrors());
                checkState.SetFailResult(failResult);

                return Redirect(String.Concat("/" + model.LangCode + "/checkout/fail"));
            }

        }


        /// <summary>
        /// EST Redirect Action
        /// </summary>
        /// <returns></returns>
        //[HttpToHttpsRedirectAttribute]
        public ActionResult YapiKredi3DSecure()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            SetCheckoutModelFromSession(model);

            model.PaymentResult = new OperationResult<PaymentHistoryViewModel>();
            if (model.CreatedOrder != null)
            {
                string protokol = this.Protokol;

                C_PosnetOOSTDS posnetOOSTDSObj = new C_PosnetOOSTDS();

                posnetOOSTDSObj.SetMid(model.CreatedOrder.WebPosInstallment.MagazaNo);
                posnetOOSTDSObj.SetTid(model.CreatedOrder.WebPosInstallment.TerminalID);
                posnetOOSTDSObj.SetPosnetID(model.CreatedOrder.WebPosInstallment.PostNetNo);
                posnetOOSTDSObj.SetKey(model.CreatedOrder.WebPosInstallment.SecureKey3D);

                string islem = (!String.IsNullOrWhiteSpace(model.CreatedOrder.WebPosInstallment.ProvType)) ? model.CreatedOrder.WebPosInstallment.ProvType :"Auth";
             
                string CardNumber = model.CreditCard.CardNumber;
                if (!String.IsNullOrWhiteSpace(CardNumber)) CardNumber = CardNumber.Replace(" ", "");

                string CardOwner = model.CreditCard.CardOwner;
                string ExpiresMonth = model.CreditCard.ExpiresMonth;
                string ExpiresYear = model.CreditCard.ExpiresYear;
                string CardCvc = model.CreditCard.CardCvc.ToString();
                string CardType = model.CreditCard.CardType;

                string bitis = ExpiresYear + ExpiresMonth;
                string FormatliTutar = Convert.ToDecimal(model.CreatedOrder.TotalAmount).ToString("n2");

                FormatliTutar = FormatliTutar.Replace(".", "");
                FormatliTutar = FormatliTutar.Replace(",", "");

                string ORDid = model.CreatedOrder.TrackCode;
                int ordLength = ORDid.Length;

                string beforeStr = "";
                if (ordLength < 20)
                {
                    for (int i = ordLength; i < 20; i++)
                    {

                        beforeStr += "0";
                    }
                }

                //DateTime.Now.ToString("dd.mm.yyyy.hh.mm.ss.dd.mm.yyyy.ss").Replace(".", "");
                ORDid = beforeStr + ORDid;

                string amount = ViewBag.AmountText = FormatliTutar;
                string currencyCode = ViewBag.CurrencyCode = "YT";
              
                string instalment = ViewBag.Installment = model.CreatedOrder.InstallmentQuantity.ToString();

                string xid = ViewBag.XID = ORDid;
                string tranType = ViewBag.TranType = islem;
                string custName = ViewBag.CustName = CardOwner;
                string ccno = ViewBag.CCNO = CardNumber;
                string expdate = ViewBag.EXPDATE = bitis;
                string cvv = ViewBag.CVV = CardCvc;
                string vftCode = ViewBag.VftCode = "";

                //string custName = Request.Form.Get("custName");
                //string xid = Request.Form.Get("XID");

                //string ccno = Request.Form.Get("ccno");
                //string expdate = Request.Form.Get("expdate");
                //string cvv = Request.Form.Get("cvv");

                // string amount = Request.Form.Get("amount");
                //string currencyCode = Request.Form.Get("currency");
                //string instalment = Request.Form.Get("instalment");
                //string tranType = Request.Form.Get("tranType");
                //string vftCode = Request.Form.Get("vftCode");

                ////this.amount.Text = posnettds_util.CurrencyFormat(amount, currencyCode, true);
                //this.instNumber.Text = instalment;
                //this.XID.Text = xid;

                if ((!posnetOOSTDSObj.CreateTranRequestDatas(custName, amount, currencyCode, instalment, xid, tranType, ccno, expdate, cvv)))
                {
                    string errorText = posnetOOSTDSObj.GetResponseText();
                    string errorcode = posnetOOSTDSObj.GetResponseCode();

                    string errorMessages = "Posnet Data 'ları olusturulamadi (" + errorText + ")<br>";
                    errorMessages += "Error Code : " + errorcode;

                    Exception ex = new Exception(errorText + " " + errorcode);

                    //Repository.HataLoguOlustur(ref ex, Request.Url.AbsoluteUri, "", "YKB 3D yüklenme hatası");
                    var paymentResult = new OperationResult<PaymentHistoryViewModel>();
                    paymentResult.SetError("YKB 3D install error");

                    checkState.SetPaymentResult(paymentResult);
                   
                    return Redirect(String.Concat("~/", model.LangCode, "/checkout/fail"));

                }

                ViewBag.MID = model.CreatedOrder.WebPosInstallment.MagazaNo;
                ViewBag.PostURL = model.CreatedOrder.WebPosInstallment.PostUrl;
                ViewBag.PosnetID = model.CreatedOrder.WebPosInstallment.PostNetNo;
                ViewBag.posnetData = posnetOOSTDSObj.GetPosnetData();
                ViewBag.posnetData2 = posnetOOSTDSObj.GetPosnetData2();
                ViewBag.Digest = posnetOOSTDSObj.GetSign();
                ViewBag.MerchantReturnURL = protokol + "://" + this.UrlAuthority + $"/{model.LangCode}/payment/YapiKredi3DSecureResult";
                ViewBag.OpenANewWindow = false;

                return View(model);

            }
            else
            {
                model.PaymentResult.SetError(_localizer["OdemeIcinSiparisBulunamadi"]);
                checkState.SetPaymentResult(model.PaymentResult);

                var failResult = new StandartOperationResult();
                failResult.SetError(_localizer["OdemeIcinSiparisBulunamadi"]);

                checkState.SetFailResult(failResult);
                return Redirect(String.Concat("~/", model.LangCode, "/checkout/fail"));
            }

        }

        /// <summary>
        /// EST Redirect Action
        /// </summary>
        /// <returns></returns>
        //[HttpToHttpsRedirectAttribute]
        //[ReloadOnCheckoutStateEnd(RedirectOnHttpRequest = true)]
        public ActionResult YapiKredi3DSecureResult()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            SetCheckoutModelFromSession(model);

            PaymentChargeServiceFactory paymentChargeFactory = new PaymentChargeServiceFactory(model, orderService, checkState,  contextAccessor);
            IPaymentChargeService chargeService = paymentChargeFactory.GetChargeService(model.SelectedWebPosInstallment.PosPaymentType);

            if (chargeService == null)
            {
                model.PaymentResult.SetError(_localizer["OdemeTuruBulunamadi"]);

                var failResult = new StandartOperationResult();
                failResult.SetError(_localizer["OdemeIcinSiparisBulunamadi"]);
                checkState.SetFailResult(failResult);

                return Redirect(String.Concat("/" + model.LangCode + "/checkout/fail"));
            }

            model = chargeService.MakePayment();

            //Success Payment
            if (model.PaymentResult != null && model.PaymentResult.Status)
            {
                orderService.SetSuccessOrder(model.CreatedOrder);
                return Redirect(String.Concat("/" + model.LangCode + "/checkout/success"));
            }
            else
            {
                return Redirect(String.Concat("/" + model.LangCode + "/checkout/fail"));
            }

        }


        private void SetCheckoutModelFromSession(CheckoutVM model)
        {
            model.Step = "progress";

            model.Payments = paymentService.GetAllActivePayments(this.storeId, this.domain?.ID ?? 0);
            CartVM cart = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            checkState.SetBaseItems(this.storeId, this.priceCurrencyId, this.userId, memberShipService.GetSpecialKeyForUser(), this.domainIdNotNull, this.lang, this.currency, cart);

            model.Cart = cartService.Get(cart, this.storeId, this.userIdNullable, this.memberShipService.GetSpecialKeyForUser(), this.priceCurrencyId);

            model.CheckoutAsGuest = checkState.IsGuest();
            model.BillingAddressId = checkState.GetBillingAddressId();
            model.ShippingAddressId = checkState.GetShippingAddressId();
            model.CargoPriceId = checkState.GetCargoPriceId();
            model.WebPosIstallmentId = checkState.GetWebPosInstallmentId();
            model.CreatedOrder = checkState.GetCreatedOrder();

            model.SelectedWebPosInstallment = paymentService.GetWebPosInstallmentByID(this.storeId, checkState.GetWebPosInstallmentId() ?? 0, model.DomainId);
            model.SelectedBillingAddress = addressService.GetUserAddress(this.storeId, checkState.GetBillingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedShippingAddress = model.ShippingAddressId == model.BillingAddressId ? model.SelectedBillingAddress : addressService.GetUserAddress(this.storeId, checkState.GetShippingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedCargoPrice = cargoPriceService.GetCargoPriceById(this.storeId, this.priceCurrencyId, model.CargoPriceId ?? 0);
            model.SpecialKey = memberShipService.GetSpecialKeyForUser();
            model.BankId = checkState.GetBankId();
            model.SelectedBank = paymentService.GetBank(this.storeId, model.BankId ?? 0, this.domain?.ID ?? 0);

            model.CouponId = checkState.GetCouponId();
            model.SelectedCoupon = couponService.GetCoupon(this.storeId, model.CouponId ?? 0, null, this.priceCurrencyId, this.domainIdNotNull);

            checkState.SetIsLocked(true);

            model.PageTitle = $"Payment - Progress {this.domain.DomainBrandName} {this.domain.DomainSlogan}";
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
