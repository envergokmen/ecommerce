﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.MvcCore.Extensions;

namespace Ecom.Web.Controllers
{
    public class ProductDetailsController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly CatalogService catalogService;
        private readonly BannerService bannerService;
        private readonly ProductService productService;

        public ProductDetailsController(EcomContext db,
            IMemberShipService _memberShipService,
            ProductService _productService,
            DomainService _domainService,
            BannerService _bannerService,
        IStringLocalizer<HomeController> localizer,
            CatalogService _catalogService,
            LangService _langService,
            PriceCurrencyService _currencyService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            catalogService = _catalogService;
            bannerService = _bannerService;
            productService = _productService;
        }

        public IActionResult Index(string productUrl, string lang)
        {
            ProductDetailVM model = new ProductDetailVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            model.PageTitle = $"{this.domain.DomainBrandName} {this.domain.DomainSlogan}";

            model.CurProduct = productService.GetProductDetails(this.storeId, productId: null, null, this.priceCurrencyId, url: productUrl);

            model.CartCount = HttpContext.Session.GetString("CartCount");

            if (model.CurProduct != null)
            {
                this.lang = model.Lang = langService.GetLangById(model.CurProduct.LangID ?? this.langId, this.storeId);

                model.Categories = catalogService.GetAllCatalogsForLang(this.storeId, this.langId, true, Core.CatalogType.Product);
                SetMetaItems(model);
            }

            if (Request.IsAjaxRequest())
            {
                model.IsPartial = true;
                return PartialView(model);
            }

            return View(model);
        }

        private static void SetMetaItems(ProductDetailVM model)
        {
            if (model.CurProduct != null)
            {
                if (!string.IsNullOrWhiteSpace(model.CurProduct.Title))
                {
                    model.PageTitle = model.CurProduct.Title;
                }
                else if (!string.IsNullOrWhiteSpace(model.CurProduct.ProductName))
                {
                    model.PageTitle = model.CurProduct.ProductName;
                }

                model.PageDescription = model.CurProduct.MetaDescription;
                model.PageKeywords = model.CurProduct.MetaKeywords;
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
