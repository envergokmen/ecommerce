﻿using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.MvcCore.Extensions;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.ViewModels.Addresses;
using Ecom.Web;
using Ecom.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using System.Linq;

namespace Ecom.Controllers
{
    public class AddressController : EcomBaseController
    {
        private readonly AddressService addressService;
        private readonly CheckoutStateHelper checkState;

        private readonly IStringLocalizer<SharedDataResource> localizer;

        public AddressController(
            IMemberShipService _memberShipService,
            LangService _langService,
            AddressService _addressService,
            DomainService _domainService,
            IStringLocalizer<SharedDataResource> localizer,
            PriceCurrencyService _currencyService,
            CheckoutStateHelper _checkState
            ) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.addressService = _addressService;
            this.localizer = localizer;
            this.checkState = _checkState;
        }

        public IActionResult Index()
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            model.addresses = addressService.GetUserAddresses(this.storeId, this.userId, null);

            if(Request.IsAjaxRequest())
            {
                model.IsPartial = true;
                return PartialView(model);
            }

            return View(model);
        }


        public IActionResult GetCities(int countryId, bool addDefaultOption = true)
        {
            var citis = (from p in addressService.GetCityList(countryId) select new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
            if (addDefaultOption) citis.AddDefaultOption(localizer["PleaseSelectACity"], "").ToList();

            return Json(citis);
        }

        public IActionResult GetTowns(int cityId, bool addDefaultOption = true)
        {
            var towns = (from p in addressService.GetTownList(cityId) select new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
            if(addDefaultOption) towns.AddDefaultOption(localizer["PleaseSelectADistrict"], "").ToList();

            return Json(towns);
        }



        #region AddressActions

        public IActionResult editaddress(int id)
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            model.Countries = (from p in addressService.GetValidShippingCountryList(this.storeId, this.priceCurrencyId) select new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
            model.Countries.AddDefaultOption(localizer["PleaseSelectACountry"], "");
            if (id != 0)
            {
                model.NewAddress = addressService.GetUserAddress(this.storeId, id, this.userId, memberShipService.GetSpecialKeyForUser());
                if (model.NewAddress != null && model.NewAddress.CountryId.HasValue)
                {
                    model.Cities = (from p in addressService.GetCityList(model.NewAddress.CountryId) select new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
                }

                if (model.NewAddress != null && model.NewAddress.CityId.HasValue)
                {
                    model.Towns = (from p in addressService.GetTownList(model.NewAddress.CityId) select new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
                }
            }

            
            return PartialView("_AddressAccountPartial", model);
        }

        public IActionResult deleteaddress(int id)
        {
            AccountPageVM model = new AccountPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            var result = addressService.Delete(this.storeId, id, this.userId, memberShipService.GetSpecialKeyForUser());

            if (!result.Status)
            {
                this.HttpContext.Response.StatusCode = (int)result.ReturnCode;
                return View("_CheckoutError", result);
            }

             checkState.SetShippingAddressId(null);
             checkState.SetBillingAddressId(null);
            checkState.SetCargoPriceId(null);

            return RedirectToAction("Index", "address");
        }


        [HttpPost]
        public IActionResult updateaddress(AddressVM addres)
        {
            addres.UserID = this.userIdNullable;
            addres.Session = memberShipService.GetSpecialKeyForUser();
            addres.StoreId = this.storeId;

            var result = (addres.Id > 0) ? addressService.Update(addres) : addressService.Create(addres);

            if (!result.Status)
            {
                this.HttpContext.Response.StatusCode = (int)result.ReturnCode;
                return View("_CheckoutError", result);
            }

            checkState.SetShippingAddressId(null);
            checkState.SetBillingAddressId(null);
            checkState.SetCargoPriceId(null);

            return RedirectToAction("Index", "address");
        }


        #endregion


    }
}