﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.ViewModels.Pages;
using Ecom.Core;
using System;
using Ecom.MvcCore.Extensions;
using Ecom.Web.Helpers;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Ecom.ViewModels.Addresses;
using Ecom.Services.CheckoutServices;

namespace Ecom.Web.Controllers
{
    public class CheckOutController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<SharedDataResource> _localizer;
        private readonly CatalogService catalogService;
        private readonly CartService cartService;
        private readonly ProductService productService;
        private readonly AddressService addressService;
        private readonly CheckoutStateHelper checkState;
        private readonly CargoPriceService cargoPriceService;
        private readonly CouponService couponService;
        private readonly CampaignService campaignService;
        private readonly OrderService orderService;

        private readonly PaymentService paymentService;

        public CheckOutController(EcomContext db,
            IMemberShipService _memberShipService,
            ProductService _productService,
            DomainService _domainService,
            CartService _cartService,
        IStringLocalizer<SharedDataResource> localizer,
            CatalogService _catalogService,
            LangService _langService,
            CheckoutStateHelper _checkState,
            AddressService _addressService,
            CargoPriceService _cargoPriceService,
            PriceCurrencyService _currencyService,
             PaymentService _paymentService,
             OrderService _orderService,
             CouponService _couponService,
             CampaignService _campaignService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            catalogService = _catalogService;
            cartService = _cartService;
            productService = _productService;
            this.checkState = _checkState;
            this.addressService = _addressService;
            this.cargoPriceService = _cargoPriceService;
            this.paymentService = _paymentService;
            this.orderService = _orderService;
            this.couponService = _couponService;
            this.campaignService = _campaignService;

        }

        public IActionResult Index()
        {

            campaignService.GenerateCouponAllUsers(this.storeId, this.domainIdNull, this.userId, new KampanyaTuru[] {
                //KampanyaTuru.AcordingToCartTotal,
                //KampanyaTuru.OnlyNewRegisters,
                KampanyaTuru.ToAllUsers
            });

            checkState.SetStep("cart");
            checkState.SetIGuest(false);
            checkState.SetFailResult(null);
            checkState.SetBillingAddressId(null);
            checkState.SetShippingAddressId(null);
            checkState.SetCargoPriceId(null);
            checkState.SetBankId(null);
            checkState.SetWebPosInstallmentId(null);
            checkState.SetCouponId(null);
            checkState.SetPaymentResult(null);
            return RedirectToAction("cart");
        }

        public IActionResult cart()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.PageTitle = $"Cart - Checkout {this.domain.DomainBrandName} {this.domain.DomainSlogan}";
            model.Step = "cart";

            model.CheckoutAsGuest = checkState.IsGuest();

            if (!Request.IsAjaxRequest() && checkState.GetStep() != model.Step) return RedirectToAction(checkState.GetStep());

            CartVM cart = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            model.Cart = cartService.Get(cart, this.storeId, this.userIdNullable, this.memberShipService.GetSpecialKeyForUser(), this.priceCurrencyId);

            campaignService.GenerateCouponForCartCampaign(this.storeId, this.domainIdNull, model.Cart.TotalAmount, this.userId);

            checkState.SetBillingAddressId(null);
            checkState.SetShippingAddressId(null);
            checkState.SetCargoPriceId(null);
            checkState.SetWebPosInstallmentId(null);
            checkState.SetBankId(null);

            model.CouponId = checkState.GetCouponId();
            model.SelectedCoupon = couponService.GetCoupon(this.storeId, model.CouponId ?? 0, null, this.priceCurrencyId, this.domainIdNotNull);

            model.Coupons = couponService.GetUserCoupons(this.storeId, this.userId, this.priceCurrencyId, this.domainIdNotNull);

            return StepView(model);

        }

        public IActionResult address()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.PageTitle = $"Address - Checkout {this.domain.DomainBrandName} {this.domain.DomainSlogan}";
            model.Step = "address";
            if (!Request.IsAjaxRequest() && checkState.GetStep() != model.Step) return RedirectToAction(checkState.GetStep());

            model.CheckoutAsGuest = checkState.IsGuest();

            model.Addresses = addressService.GetUserAddresses(this.storeId, this.userIdNullable, memberShipService.GetSpecialKeyForUser());
            model.Countries = (from p in addressService.GetValidShippingCountryList(this.storeId, this.priceCurrencyId) select new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
            model.Countries.AddDefaultOption(_localizer["PleaseSelectACountry"], "");
            model.CargoPrices = cargoPriceService.GetAllCargoPrices(this.storeId, this.priceCurrencyId);

            checkState.SetBankId(null);
            checkState.SetWebPosInstallmentId(null);

            CartVM cart = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.Cart = cartService.Get(cart, this.storeId, this.userIdNullable, this.memberShipService.GetSpecialKeyForUser(), this.priceCurrencyId);

            if (model.Addresses.Count > 0)
            {
                if (checkState.GetBillingAddressId() == null) checkState.SetBillingAddressId(model.Addresses.FirstOrDefault().Id);
                if (checkState.GetShippingAddressId() == null) checkState.SetShippingAddressId(model.Addresses.FirstOrDefault().Id);

                var selectedAddress = addressService.GetUserAddress(this.storeId, checkState.GetShippingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());

                if (selectedAddress != null)
                {
                    model.CargoPrices = model.CargoPrices.Where(c => c.CountryID == selectedAddress.CountryId).ToList();

                    if (checkState.GetCargoPriceId() == null)
                    {
                        if (selectedAddress != null)
                        {
                            checkState.SetCargoPriceId(model.CargoPrices.Where(c => c.CountryID == selectedAddress.CountryId).FirstOrDefault()?.Id);
                        }
                    }


                }

            }

            model.BillingAddressId = checkState.GetBillingAddressId();
            model.ShippingAddressId = checkState.GetShippingAddressId();
            model.CargoPriceId = checkState.GetCargoPriceId();

            model.CouponId = checkState.GetCouponId();
            model.SelectedCoupon = couponService.GetCoupon(this.storeId, model.CouponId ?? 0, null, this.priceCurrencyId, this.domainIdNotNull);

            model.SelectedBillingAddress = addressService.GetUserAddress(this.storeId, checkState.GetBillingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedShippingAddress = model.ShippingAddressId == model.BillingAddressId ? model.SelectedBillingAddress : addressService.GetUserAddress(this.storeId, checkState.GetShippingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedCargoPrice = cargoPriceService.GetCargoPriceById(this.storeId, this.priceCurrencyId, model.CargoPriceId ?? 0);

            return StepView(model);
        }

        public IActionResult payment()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            //if (model.Step != null && model.Step != "Payment") return RedirectToAction(model.Step);
            model.Step = "payment";
            if (!Request.IsAjaxRequest() && checkState.GetStep() != model.Step) return RedirectToAction(checkState.GetStep());

            model.Payments = paymentService.GetAllActivePayments(this.storeId, this.domain?.ID ?? 0);

            if (checkState.GetWebPosInstallmentId() == null && model.Payments != null)
            {
                var firstInstallment = model.Payments.FirstOrDefault()?.WebPoseses?.FirstOrDefault()?.Installments?.Select(c => c.ID).FirstOrDefault();
                if (firstInstallment != null)
                {
                    checkState.SetWebPosInstallmentId(firstInstallment);
                }
            }


            if (checkState.GetBankId() == null && model.Payments != null)
            {
                var firtBank = model.Payments.Where(c => c.PaymentType == PaymentType.Havale).FirstOrDefault()?.Banks?.Select(c => c.ID).FirstOrDefault();
                if (firtBank != null)
                {
                    checkState.SetBankId(firtBank);
                }
            }


            CartVM cart = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.Cart = cartService.Get(cart, this.storeId, this.userIdNullable, this.memberShipService.GetSpecialKeyForUser(), this.priceCurrencyId);

            model.CheckoutAsGuest = checkState.IsGuest();
            model.BillingAddressId = checkState.GetBillingAddressId();
            model.ShippingAddressId = checkState.GetShippingAddressId();
            model.CargoPriceId = checkState.GetCargoPriceId();
            model.BankId = checkState.GetBankId();

            model.CouponId = checkState.GetCouponId();
            model.SelectedCoupon = couponService.GetCoupon(this.storeId, model.CouponId ?? 0, null, this.priceCurrencyId, this.domainIdNotNull);

            model.WebPosIstallmentId = checkState.GetWebPosInstallmentId();
            model.SelectedWebPosInstallment = paymentService.GetWebPosInstallmentByID(this.storeId, checkState.GetWebPosInstallmentId() ?? 0, model.DomainId);

            model.SelectedBillingAddress = addressService.GetUserAddress(this.storeId, checkState.GetBillingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedShippingAddress = model.ShippingAddressId == model.BillingAddressId ? model.SelectedBillingAddress : addressService.GetUserAddress(this.storeId, checkState.GetShippingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedCargoPrice = cargoPriceService.GetCargoPriceById(this.storeId, this.priceCurrencyId, model.CargoPriceId ?? 0);

            model.SelectedBank = paymentService.GetBank(this.storeId, model.BankId ?? 0, this.domain?.ID ?? 0);

            model.PageTitle = $"Payment - Checkout {this.domain.DomainBrandName} {this.domain.DomainSlogan}";

            return StepView(model);
        }

        public IActionResult progress()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.Step = "progress";

            model.Payments = paymentService.GetAllActivePayments(this.storeId, this.domain?.ID ?? 0);
            CartVM cart = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            checkState.SetBaseItems(this.storeId, this.priceCurrencyId, this.userId, memberShipService.GetSpecialKeyForUser(), this.domainIdNotNull, this.lang, this.currency, cart);

            model.Cart = cartService.Get(cart, this.storeId, this.userIdNullable, this.memberShipService.GetSpecialKeyForUser(), this.priceCurrencyId);

            model.CheckoutAsGuest = checkState.IsGuest();
            model.BillingAddressId = checkState.GetBillingAddressId();
            model.ShippingAddressId = checkState.GetShippingAddressId();
            model.CargoPriceId = checkState.GetCargoPriceId();
            model.WebPosIstallmentId = checkState.GetWebPosInstallmentId();

            model.SelectedWebPosInstallment = paymentService.GetWebPosInstallmentByID(this.storeId, checkState.GetWebPosInstallmentId() ?? 0, model.DomainId);
            model.SelectedBillingAddress = addressService.GetUserAddress(this.storeId, checkState.GetBillingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedShippingAddress = model.ShippingAddressId == model.BillingAddressId ? model.SelectedBillingAddress : addressService.GetUserAddress(this.storeId, checkState.GetShippingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedCargoPrice = cargoPriceService.GetCargoPriceById(this.storeId, this.priceCurrencyId, model.CargoPriceId ?? 0);
            model.SpecialKey = memberShipService.GetSpecialKeyForUser();
            model.BankId = checkState.GetBankId();
            model.SelectedBank = paymentService.GetBank(this.storeId, model.BankId ?? 0, this.domain?.ID ?? 0);

            model.CouponId = checkState.GetCouponId();
            model.SelectedCoupon = couponService.GetCoupon(this.storeId, model.CouponId ?? 0, null, this.priceCurrencyId, this.domainIdNotNull);

            var op = checkState.ValidateStepChange(model.Step);

            if (Request.IsAjaxRequest())
            {
                if (!op.Status)
                {
                    this.HttpContext.Response.StatusCode = (int)op.ReturnCode;
                    return View("_CheckoutError", op);
                }
                else
                {
                    return Content("OK");
                }

            }

            if (!op.Status)
            {
                checkState.SetFailResult(op);
                return RedirectToAction("fail");
            }

            checkState.SetStep(model.Step);
            checkState.SetIsLocked(true);

            model.PageTitle = $"Payment - Progress {this.domain.DomainBrandName} {this.domain.DomainSlogan}";

            var createResult = orderService.CreateOrderFromCheckState(model);

       
            if (createResult.Status && createResult.Result != null)
            {
                checkState.SetCreatedOrderId(createResult.Result.ID);
                checkState.SetCreatedOrder(createResult.Result);
                model.CreatedOrderId = createResult.Result.ID;


                if (createResult.IsRedirectRequired && !String.IsNullOrWhiteSpace(createResult.DirectUrl))
                {
                    return Redirect(createResult.DirectUrl);
                }
                else if(createResult.Result.PaymentType == PaymentType.KrediKarti && createResult.Result.PaymentStatus != SiparisOdemeDurumu.OdemeOnaylandi)
                {
                    PaymentChargeServiceFactory paymentChargeFactory = new PaymentChargeServiceFactory(model, orderService, checkState, contextAccessor);
                    IPaymentChargeService chargeService = paymentChargeFactory.GetChargeService(model.SelectedWebPosInstallment.PosPaymentType);

                    if (chargeService == null)
                    {
                        var failResult = new StandartOperationResult();
                        failResult.SetError("Service cannot be found for this payment type");
                        checkState.SetFailResult(failResult);

                        return Redirect(String.Concat("/" + model.LangCode + "/checkout/fail"));
                    }

                    var paymentResult = chargeService.MakePayment();

                    //Success Payment
                    if (paymentResult.PaymentResult != null && paymentResult.PaymentResult.Status)
                    {
                        orderService.SetSuccessOrder(model.CreatedOrder);
                        return Redirect(String.Concat("/" + model.LangCode + "/checkout/success"));
                    }
                    else
                    {
                        var failResult = new StandartOperationResult();
                        failResult.SetError(paymentResult.PaymentResult.GetErrors());
                        checkState.SetFailResult(failResult);

                        return Redirect(String.Concat("/" + model.LangCode + "/checkout/fail"));
                    }

                }

                return RedirectToAction("success", "checkout");
            }
            else
            {
                var failResult = new StandartOperationResult();
                failResult.ClearMessages();
                failResult.SetError(createResult.ResultDescription);
                failResult.Result = createResult.Result;
                checkState.SetFailResult(failResult);
            }

            return RedirectToAction("fail");

        }

        public IActionResult success()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.Step = "success";
            model.CreatedOrderId = checkState.GetCreatedOrderId();
            model.CreatedOrder = checkState.GetCreatedOrder();
            var order = orderService.GetOrderDetails(this.storeId, model.CreatedOrderId ?? 0, null, this.userId, null, null);

            return View(model);

        }

        public IActionResult fail()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.Step = "fail";
            model.FailResult = checkState.GetFailResult();

            return View(model);
        }

        #region ValidateAndSetCurStep

        private IActionResult StepView(CheckoutVM model)
        {

            if (Request.IsAjaxRequest())
            {
                checkState.SetBaseItems(this.storeId, this.priceCurrencyId, this.userId, memberShipService.GetSpecialKeyForUser(), this.domainIdNotNull, this.lang, this.currency, null);

                var op = checkState.ValidateStepChange(model.Step);
                if (!op.Status)
                {
                    this.HttpContext.Response.StatusCode = (int)op.ReturnCode;
                    return View("_CheckoutError", op);
                }

                model.isPartial = true;
                checkState.SetStep(model.Step);
                return PartialView(model);
            }

            return View(model);
        }
        #endregion


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
