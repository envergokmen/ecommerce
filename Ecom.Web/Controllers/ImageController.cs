﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.MvcCore.Extensions;
using Microsoft.AspNetCore.Hosting;
using SixLabors.ImageSharp;

namespace Ecom.Web.Controllers
{
    public class ImageController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly CatalogService catalogService;
        private readonly BannerService bannerService;
        private readonly IHostingEnvironment env;

        public ImageController(EcomContext db,
            IMemberShipService _memberShipService,
            DomainService _domainService, 
            BannerService _bannerService,
            IStringLocalizer<HomeController> localizer,
            CatalogService _catalogService,
            LangService _langService,
            PriceCurrencyService _currencyService,
            IHostingEnvironment _env,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            catalogService = _catalogService;
            bannerService = _bannerService;
            env = _env;
        }

        //[Route("/image/{width}/{height}/{*url}")]
        //public IActionResult ResizeImage(string url, int width, int height)
        //{
        //    if (width < 0 || height < 0) { return BadRequest(); }

        //    var imagePath = PathString.FromUriComponent("/" + url);
        //    var fileInfo = env.WebRootFileProvider.GetFileInfo(imagePath);
        //    if (!fileInfo.Exists) { return NotFound(); }

        //    var outputStream = new MemoryStream();
        //    using (var inputStream = fileInfo.CreateReadStream())
        //    using (var image = Image.Load(inputStream))
        //    {
        //        image
        //            .Resize(widthToUse, heightToUse)
        //            .SaveAsJpeg(outputStream);
        //    }

        //    outputStream.Seek(0, SeekOrigin.Begin);

        //    return File(outputStream, "image/jpg");


        //}
    }
}
