﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.ViewModels.Pages;
using Ecom.Core;
using System;
using Ecom.MvcCore.Extensions;
using Ecom.Web.Helpers;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Ecom.ViewModels.Addresses;
using Ecom.ViewModels.Payments;

namespace Ecom.Web.Controllers
{
    public class CheckOutOperationsController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<SharedDataResource> _localizer;
        private readonly CatalogService catalogService;
        private readonly CartService cartService;
        private readonly ProductService productService;
        private readonly AddressService addressService;
        private readonly CheckoutStateHelper checkState;
        private readonly CargoPriceService cargoPriceService;
        private readonly CouponService couponService;
        private readonly BinNumberService binNumberService;

        private readonly PaymentService paymentService;

        public CheckOutOperationsController(EcomContext db,
            IMemberShipService _memberShipService,
            ProductService _productService,
            DomainService _domainService,
            CartService _cartService,
        IStringLocalizer<SharedDataResource> localizer,
            CatalogService _catalogService,
            LangService _langService,
            CheckoutStateHelper _checkState,
            AddressService _addressService,
            CargoPriceService _cargoPriceService,
            PriceCurrencyService _currencyService,
            CouponService _couponService,
             PaymentService _paymentService,
             BinNumberService _binService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            _localizer = localizer;
            catalogService = _catalogService;
            cartService = _cartService;
            productService = _productService;
            this.checkState = _checkState;
            this.addressService = _addressService;
            this.cargoPriceService = _cargoPriceService;
            this.paymentService = _paymentService;
            this.binNumberService = _binService;
            this.couponService = _couponService;

        }

        public IActionResult setasguest(bool value)
        {
            checkState.SetIGuest(value);
            return RedirectToAction("cart", "checkout");
        }

        public IActionResult getbin(int binno)
        {
            return Json(binNumberService.GetBinNo(this.storeId, binno));
        }

        public IActionResult removecoupon()
        {
            checkState.SetCouponId(null);
            return Content("ok");

        }


        public IActionResult setcoupon(long id, string couponCode)
        {
            var cart = cartService.Get(new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName), this.storeId, this.userId, memberShipService.GetSpecialKeyForUser(), this.priceCurrencyId);
            checkState.SetBaseItems(this.storeId, this.priceCurrencyId, this.userId, memberShipService.GetSpecialKeyForUser(), this.domainIdNotNull, this.lang, this.currency, cart);
            var coupon = couponService.GetCoupon(this.storeId, id, couponCode, this.priceCurrencyId, this.domainIdNotNull);

            var validateResult = checkState.ValidateCouponAdd(coupon);

            if (!validateResult.Status)
            {
                this.HttpContext.Response.StatusCode = (int)validateResult.ReturnCode;
                return View("_CheckoutError", validateResult);
            }
            else
            {
                checkState.SetCouponId(coupon.ID);
                return Content("ok");
            }

        }

        public IActionResult setaddress(int id, string adrtype)
        {
            if (adrtype == "bill")
            {
                checkState.SetBillingAddressId(id);
            }

            else
            {
                checkState.SetShippingAddressId(id);

                var selectedAddress = addressService.GetUserAddress(this.storeId, checkState.GetShippingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
                if (selectedAddress != null && selectedAddress.CountryId.HasValue)
                {
                    var validCargoPrice = cargoPriceService.GetCargoPricesForCountry(this.storeId, selectedAddress.CountryId ?? 0, this.priceCurrencyId).FirstOrDefault();
                    checkState.SetCargoPriceId(validCargoPrice?.Id ?? null);
                }
                else
                {
                    checkState.SetCargoPriceId(null);
                }

            }

            return Content("ok");
        }

        public IActionResult setinstallment(int id)
        {
            var installment = paymentService.GetWebPosInstallmentByID(this.storeId, id, this.domain?.ID ?? 0);

            if (installment == null)
            {
                StandartOperationResult operationResult = new StandartOperationResult();
                operationResult.SetAsBadRequest("Payment is not found please select a valid payment type");
                this.HttpContext.Response.StatusCode = (int)operationResult.ReturnCode;
                return View("_CheckoutError", operationResult);
            }

            checkState.SetWebPosInstallmentId(id);

            return Content("ok");
        }


        public IActionResult setcargoprice(int id)
        {
            var selectedAddress = addressService.GetUserAddress(this.storeId, checkState.GetShippingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            var selectectedCargoPrice = cargoPriceService.GetCargoPriceById(this.storeId, this.priceCurrencyId, id);

            if (selectectedCargoPrice == null || selectedAddress == null || selectedAddress.CountryId != selectectedCargoPrice.CountryID)
            {
                StandartOperationResult operationResult = new StandartOperationResult();
                operationResult.SetAsBadRequest("This cargo is not valid for selected address, please select different address or cargo");
                this.HttpContext.Response.StatusCode = (int)operationResult.ReturnCode;
                return View("_CheckoutError", operationResult);
            }

            checkState.SetCargoPriceId(id);

            return Content("ok");
        }


        public IActionResult setcard(CreditCardVM card)
        {

            checkState.SetCreditCard(null);
            var validationResult = checkState.ValidateCreditCard(card);

            if (!validationResult.Status)
            {
                this.HttpContext.Response.StatusCode = (int)validationResult.ReturnCode;
                return View("_CheckoutError", validationResult);
            }

            checkState.SetCreditCard(card);
            return Content("ok");
        }

        public IActionResult setbank(int id)
        {
            var selectedBank = paymentService.GetBank(this.storeId, id, this.domain?.ID ?? 0);

            if (selectedBank == null)
            {
                StandartOperationResult operationResult = new StandartOperationResult();
                operationResult.SetAsBadRequest("Bank cannot be found, please select different bank or money transfer");
                this.HttpContext.Response.StatusCode = (int)operationResult.ReturnCode;
                return View("_CheckoutError", operationResult);
            }

            checkState.SetBankId(id);

            return Content("ok");
        }

        public IActionResult summary()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            model.BillingAddressId = checkState.GetBillingAddressId();
            model.ShippingAddressId = checkState.GetShippingAddressId();
            model.CargoPriceId = checkState.GetCargoPriceId();
            model.Step = checkState.GetStep();

            CartVM cart = new CartVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);
            model.Cart = cartService.Get(cart, this.storeId, this.userIdNullable, this.memberShipService.GetSpecialKeyForUser(), this.priceCurrencyId);
            model.SelectedBillingAddress = addressService.GetUserAddress(this.storeId, checkState.GetBillingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedShippingAddress = model.ShippingAddressId == model.BillingAddressId ? model.SelectedBillingAddress : addressService.GetUserAddress(this.storeId, checkState.GetShippingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());
            model.SelectedCargoPrice = cargoPriceService.GetCargoPriceById(this.storeId, this.priceCurrencyId, model.CargoPriceId ?? 0);
            model.BankId = checkState.GetBankId();
            model.SelectedBank = paymentService.GetBank(this.storeId, model.BankId ?? 0, this.domain?.ID ?? 0);

            model.CouponId = checkState.GetCouponId();
            model.SelectedCoupon = couponService.GetCoupon(this.storeId, model.CouponId ?? 0, null, this.priceCurrencyId, this.domainIdNotNull);

            model.WebPosIstallmentId = checkState.GetWebPosInstallmentId();
            model.SelectedWebPosInstallment = paymentService.GetWebPosInstallmentByID(this.storeId, checkState.GetWebPosInstallmentId() ?? 0, model.DomainId);

            model.isPartial = true;
            return PartialView(model);

        }

        #region AddressActions


        public IActionResult editaddress(int id)
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            model.Countries = (from p in addressService.GetValidShippingCountryList(this.storeId, this.priceCurrencyId) select new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
            model.Countries.AddDefaultOption(_localizer["PleaseSelectACountry"], "");
            if (id != 0)
            {
                model.NewAddress = addressService.GetUserAddress(this.storeId, id, this.userId, memberShipService.GetSpecialKeyForUser());
                if (model.NewAddress != null && model.NewAddress.CountryId.HasValue)
                {
                    model.Cities = (from p in addressService.GetCityList(model.NewAddress.CountryId) select new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
                }

                if (model.NewAddress != null && model.NewAddress.CityId.HasValue)
                {
                    model.Towns = (from p in addressService.GetTownList(model.NewAddress.CityId) select new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
                }
            }

            return PartialView("_AddressPartial", model);
        }

        public IActionResult deleteaddress(int id)
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            var result = addressService.Delete(this.storeId, id, this.userId, memberShipService.GetSpecialKeyForUser());

            if (!result.Status)
            {
                this.HttpContext.Response.StatusCode = (int)result.ReturnCode;
                return View("_CheckoutError", result);
            }

            if (id == checkState.GetShippingAddressId()) checkState.SetShippingAddressId(null);
            if (id == checkState.GetBillingAddressId()) checkState.SetBillingAddressId(null);
            checkState.SetCargoPriceId(null);

            return RedirectToAction("address", "checkout");
        }


        [HttpPost]
        public IActionResult updateaddress(AddressVM addres)
        {
            addres.UserID = this.userIdNullable;
            addres.Session = memberShipService.GetSpecialKeyForUser();
            addres.StoreId = this.storeId;

            var result = (addres.Id > 0) ? addressService.Update(addres) : addressService.Create(addres);

            if (!result.Status)
            {
                this.HttpContext.Response.StatusCode = (int)result.ReturnCode;
                return View("_CheckoutError", result);
            }

            if (addres.Id == 0)
            {
                checkState.SetBillingAddressId(Convert.ToInt32(result.Result));
                checkState.SetShippingAddressId(Convert.ToInt32(result.Result));
            }

            checkState.SetCargoPriceId(null);

            return RedirectToAction("address", "checkout");
        }


        #endregion

        #region CargoPartial

        public IActionResult cargo()
        {
            CheckoutVM model = new CheckoutVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            model.Addresses = addressService.GetUserAddresses(this.storeId, this.userIdNullable, memberShipService.GetSpecialKeyForUser());
            model.CargoPrices = cargoPriceService.GetAllCargoPrices(this.storeId, this.priceCurrencyId);

            if (model.Addresses.Count > 0)
            {
                var selectedAddress = addressService.GetUserAddress(this.storeId, checkState.GetShippingAddressId() ?? 0, this.userId, memberShipService.GetSpecialKeyForUser());

                if (selectedAddress != null)
                {
                    model.CargoPrices = model.CargoPrices.Where(c => c.CountryID == selectedAddress.CountryId).ToList();
                }
            }

            model.BillingAddressId = checkState.GetBillingAddressId();
            model.ShippingAddressId = checkState.GetShippingAddressId();
            model.CargoPriceId = checkState.GetCargoPriceId();

            return PartialView("_CargoPricesPartial", model);

        }
        #endregion


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
