﻿using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecom.Controllers
{
    public class UserController : EcomBaseController
    {

        public UserController(
            IMemberShipService _memberShipService,
            LangService _langService,
            DomainService _domainService,
            PriceCurrencyService _currencyService
            ) : base(_currencyService, _langService, _memberShipService, _domainService)
        {

        }


        public IActionResult Index()
        {
            LoginPageVM model = new LoginPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            return View(model);
        }


        [Route("{lang}/register")]
        [Route("register")]
        public IActionResult Register()
        {
            LoginPageVM model = new LoginPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            return View(model);
        }

        [HttpPost]
        [Route("{lang}/register")]
        [Route("register")]
        //[AllowAnonymous]
        public IActionResult Register(RegisterViewModel registerVM)//create
        {
            LoginPageVM model = new LoginPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            registerVM.StoreId = model.StoreId;
            registerVM.DomainId = model.DomainId;

            OperationResult<UserViewModel> registerResult = memberShipService.Register(registerVM);
            
            //if (Request.IsAjaxRequest())
            //{
            //    return Json(registerResult);
            //}

            if (!registerResult.Status)
            {
                ViewBag.RegisterErrors = registerResult.GetErrors();
                return View(model);
            }

            HttpContext.Session.Remove("CartCount");
            return RedirectToAction("Index", "Home");
        }

        [Route("login")]
        [Route("{lang}/login")]
        public IActionResult Login()
        {
            LoginPageVM model = new LoginPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            return View(model);
        }


        [HttpPost]
        [Route("{lang}/login")]
        [Route("login")]
        public IActionResult Login(LoginViewModel loginVM)
        {
            if (loginVM != null) loginVM.RememberMe = false;
            loginVM.RememberMe = true;

            loginVM.StoreID = this.storeId;
            loginVM.DomainID = this.domain?.ID ?? 0;

            LoginPageVM model = new LoginPageVM(this.currency, this.lang, this.domain, this.user, this.controllerName, this.actionName);

            OperationResult<UserViewModel> loginResult = memberShipService.Login(loginVM);

            if (!loginResult.Status)
            {
                ViewBag.LoginErrors = loginResult.GetErrors();
                model.loginVM = loginVM;

                return View(model);
            }

            HttpContext.Session.Remove("CartCount");
            return RedirectToAction("Index", "Home");
        }

        [Route("{lang}/logout")]
        [Route("logout")]
        //[AllowAnonymous]
        public IActionResult Logout(RegisterViewModel registerVM)//create
        {
            HttpContext.Session.Remove("CartCount");
            memberShipService.LogOut();
            return RedirectToAction("Index", "Home");
        }


    }
}