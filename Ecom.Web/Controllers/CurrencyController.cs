﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Ecom.Web.Models;
using Ecom.Models;
using Ecom.Auth.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;
using Ecom.ViewModels.Pages;
using Ecom.Core;
using System;

namespace Ecom.Web.Controllers
{
    public class CurrencyController : EcomBaseController
    {
        IHttpContextAccessor contextAccessor;

        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly PriceCurrencyService priceCurrencyService;

        public CurrencyController(EcomContext db,
            IMemberShipService _memberShipService,
            DomainService _domainService,
            PriceCurrencyService _priceCurrencyService,
            LangService _langService,
            PriceCurrencyService _currencyService,
        IHttpContextAccessor _contextAccessor) : base(_currencyService, _langService, _memberShipService, _domainService)
        {
            this.contextAccessor = _contextAccessor;
            priceCurrencyService = _priceCurrencyService;
        }


        public IActionResult Set(int id, string lang)
        {

            var currency = priceCurrencyService.GetCurrencyById(id, this.storeId);

            if (currency != null)
            {
                HttpContext.Session.SetInt32("Currency", id);

                HttpContext.Response.Cookies.Append("Currency", id.ToString(), new CookieOptions
                {
                    Expires = DateTime.Now.AddDays(360),
                    IsEssential = true
                });

                return Redirect(Request.Headers["Referer"].ToString());
            }

            return Content("Not found");

        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
