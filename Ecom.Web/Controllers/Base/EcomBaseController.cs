﻿using Ecom.Auth.Services;
using Ecom.MvcCore.Extensions;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.PriceCurrencies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace Ecom.Web
{
    public class EcomBaseController : Controller
    {
        public IMemberShipService memberShipService;
        public DomainService domainService;
        public LangService langService;
        public PriceCurrencyService currencyService;

        public EcomBaseController(PriceCurrencyService _currencyService, LangService _langService, IMemberShipService _memberShipService, DomainService _domainService)
        {
            this.memberShipService = _memberShipService;
            this.domainService = _domainService;
            this.langService = _langService;
            this.currencyService = _currencyService;
        }

        public UserViewModel user;
        public DomainViewModel domain;
        public LangViewModel lang;
        public PriceCurrencyVM currency;

        public string actionName;
        public string controllerName;
        public string IP;
        public string Protokol;
        public string curUrl;
        public string UrlAuthority;

        public int domainIdNotNull => this.domain?.ID ?? 0;
        public int? domainIdNull => this.domain?.ID;
        public int priceCurrencyId => currency!=null ? currency.PriceCurrencyID : domain?.PriceCurrencyID ?? 0;
        public int storeId => domain?.StoreAccountID ?? 0;
        public string langCode => lang?.LangCode ?? "";
        public int langId => lang?.ID ?? 0;
        public int? userIdNullable => user?.ID;
        public int userId => user?.ID ?? 0;
        public bool IsAuth => user != null;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string langCode = context.RouteData.Values["lang"]?.ToString();
            if (langCode == null && !String.IsNullOrWhiteSpace(context.HttpContext.Request.Query["lang"].ToString())) langCode = context.HttpContext.Request.Query["lang"];

            curUrl = UriHelper.GetDisplayUrl(context.HttpContext.Request);
            var urlAuth = new Uri(curUrl).Authority;
            var domainName = urlAuth?.Split(':')[0];

            //TODO:change on live
            //domain = String.IsNullOrWhiteSpace(langCode) ? domainService.GetDomainByName(domainName) : domainService.GetDomainByNameAndLang(domainName, langCode);

            domain = domainService.GetDomainByName(domainName);

            // we must always have a domain in specifed lang
            if (domain == null)
            {
                context.Result = new NotFoundResult();
                return;
            }
            else if (!context.HttpContext.Request.IsAjaxRequest() && context.HttpContext.Request.Method.ToUpper() == "GET" && langCode != null && langCode.Length == 2 && domain.LangCode == langCode)
            {
                //always redirect if specified domain requires plain URL's without lang
                context.Result = new RedirectResult(curUrl.Replace($"/{langCode}", ""));
                return;
            }

            this.lang = langService.GetLangByCode(langCode ?? domain.LangCode, this.storeId);

            //find related currency
            var currencyId = context.HttpContext.Session.GetInt32("Currency");

            if (currencyId == null && context.HttpContext.Request.Cookies["Currency"] != null)
            {
                currencyId = Convert.ToInt32(context.HttpContext.Request.Cookies["Currency"]);
                context.HttpContext.Session.SetInt32("Currency", currencyId.Value);
            }

            currencyId = (currencyId.HasValue && currencyId.Value>0) ? currencyId.Value : domain.PriceCurrencyID;
            this.currency = currencyService.GetCurrencyById(currencyId ?? 0, domain.StoreAccountID ?? 0);

            actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ActionName;
            controllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ControllerName;
            user = memberShipService.TryToSignUser(Core.UserType.Normal);

            this.IP = context.HttpContext.Connection.RemoteIpAddress.ToString();
            this.Protokol = context.HttpContext.Request.Protocol;
            this.UrlAuthority = urlAuth;

            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {

            // do something after the action executes
            base.OnActionExecuted(context);
        }
    }
}
