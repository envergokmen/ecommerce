﻿$(document).ready(function () {


    $contents = [];

    if ($("#Products").length > 0 || $("#ProductDetails").length > 0 || $("#Home").length > 0) {

        history.replaceState(window.location.href, document.title, window.location.href);
        AddToContents({ path: window.location.href, title: document.title, content: $('#MainContent').html() });
        
    }

    function AddToContents(content) {
        var curContents = $contents.filter((item) => item.path == content.path)[0];

        if (curContents == null) {
            $contents.push(content);
        }
        
        if ($contents.length > 30) $contents.shift();
    }

    function requestContent(path) {

        var curContents = $contents.filter((item) => item.path == path);

        if (curContents && curContents[0]) {

            document.title = curContents[0].title;
            $('#MainContent').html(curContents[0].content);
        }
        else {

            $('#MainContent').load(path, function () {

                document.title = $("h1:first").text();

                if ($("#Home").length > 0 || document.title == "") {
                    document.title = $("#Logo").data("pagetitle");
                }

                AddToContents({ path: path, title: document.title, content: $('#MainContent').html() });
            });

        }
    }


    $("body").on('click', "#Products .ProductItem a, #Header nav a, a#Logo:not(#Checkout #Header #Logo) ", function (e) {

        e.preventDefault();
        var href = $(this).attr("href");

        history.pushState(href, $(this).text(), href);
        requestContent(href);

    });

     
    window.addEventListener('popstate', function (e) {

        var url = e.state;

        if (url == null) {

            //this.window.location.reload;

        } else {

            requestContent(url);
        }
    });


});




function compress(data) {
    var q = {}, ret = "";
    data.replace(/([^=&]+)=([^&]*)/g, function (m, key, value) {
        q[key] = (q[key] ? q[key] + "," : "") + value;
    });
    for (var key in q)
        ret = (ret ? ret + "&" : "") + key + "=" + q[key];
    return ret;
}
function isMobile(forceResolution) {

    var isMobile = false;

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Windows Phone|Opera Mini/i.test(navigator.userAgent) || (forceResolution == true && $(window).width() < 1000)) {
        isMobile = true;
    }

    return isMobile;
}

function isAppleMobile() {

    var isAppleMob = false;

    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
        isAppleMob = true;
    }

    return isAppleMob;
}


function isIpad() {

    var is_ipad = navigator.userAgent.toLowerCase().indexOf('ipad') > -1;

    return is_ipad;
}

function IsChrome() {

    var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

    return isChrome;
}


function IsSafari() {

    var safari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
    var android = navigator.userAgent.toLowerCase().indexOf('android') > -1;

    if (safari && android) {
        safari = false;
    }

    return safari;
}


function IsAndroidSafari() {

    var safari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
    var android = navigator.userAgent.toLowerCase().indexOf('android') > -1;

    if (safari && android) {
        return true;
    }

    return false;
}

function IsAndroid() {

    var android = navigator.userAgent.toLowerCase().indexOf('android') > -1;

    if (android) {
        return true;
    }

    return false;
}

function IsFirefox() {

    var isFF = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    return isFF;
}

function fnIsAppleMobile() {
    if (navigator && navigator.userAgent && navigator.userAgent != null) {
        var strUserAgent = navigator.userAgent.toLowerCase();
        var arrMatches = strUserAgent.match(/(iphone|ipod|ipad)/);
        if (arrMatches)
            return true;
    } // End if (navigator && navigator.userAgent) 

    return false;
} // End Function fnIsAppleMobile


function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


function getQuery(url) {

    var q = (url.split('?')[1] || '').split('&'), o = {};
    for (var i = 0; i < q.length; i++) {
        var p = q[i].split('=');
        if (!!p[0]) o[unescape(p[0])] = unescape(p[1] || '');
    }
    return o;
}
