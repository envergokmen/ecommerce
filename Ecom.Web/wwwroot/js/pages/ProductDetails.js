﻿$(document).ready(function () {

    $productId = $("#ProductDetails").attr("data-id");
    $sizeId = null;
    $colorId = null;
    $stockId = null;

    if ($("#Colors div.active").length > 0) {

        $colorId = $("#Colors div.active:first").attr("data-color"); 
    }

    $('body').on('click', "#ProductDetails #Colors > div", function () {

        if (!$productId) { $productId = $("#ProductDetails").attr("data-id"); }
        $colorId = $(this).attr("data-color");
        $("div", $(this).closest("#Colors")).removeClass("active");
        $(this).addClass("active");

        $sizeId = null;
        $stockId = null;

        var filteredSizes = $stocks.filter((item) => item.ColorID == $colorId);
        var filteredImages = $images.filter((item) => item.ColorID == $colorId);

        $("> div > div", "#Thumbs , #Images").remove();

        filteredImages.forEach(function (item) {
            $("#Thumbs > div").append("<div><img src='" + item.ImagePath + "?width=" + $thumbImageWidth+ "' alt=''></div>");

            if ($("#Images > div > div").length == 0) {
                $("#Images > div").append("<div><img src='" + item.ImagePath + "?width=" + $detailImageWidth + "' alt=''></div>");
            }
        });

        $("#Sizes > div").remove();
        filteredSizes.forEach(function (item) {
            $("#Sizes").append(" <div data-id='" + item.SizeID + "' class='" + (item.IsActive ? "default" : "outofstock") + "'>" + item.SizeName + "</div>");
        });

        //console.log(filteredSizes, filteredImages);

    });

   
    $("body").on('click', "#Thumbs img", function () {

        var img1 = $(this).attr("src").split('?')[0] + "?width=" + $detailImageWidth;
        $("#Images > div > div > img:first").attr("src", img1);

    });

    $defaultText = $("#AddToCart").text();
    $("body").on('click', "#Sizes div:not(.outofstock)", function () {

        $("#Sizes > div").removeClass("active");
        $(this).addClass("active");

        $productId = $("#ProductDetails").attr("data-id");
        $colorId = $("#Colors div.active:first").attr("data-color");  
        $sizeId = $(this).attr("data-id");
        $stockId = $stocks.filter((item) => item.ColorID == $colorId && item.SizeID == $sizeId)[0].StockID;

        if ($("body").hasClass("ShowBlackBg")) {

            $("body").removeClass("ShowBlackBg");
            $("#AddToCart").text($defaultText);
            $("#AddToCart").trigger("click");
        }
    });

    $('body').on('click', '#BgBlack', function () {

        $("body").removeClass("ShowBlackBg");
        $("#AddToCart").text($defaultText);
    });

    $("body").on('click', '#AddToCart', function () {

        if (!$productId) { $productId = $("#ProductDetails").attr("data-id"); }

        //console.log($stocks, $productId, "colorid:", $colorId, "sizeid:", $sizeId, "stockid", $stockId);

        $defaultText = $(this).text();
         
        var addingText = $(this).data("addingtext");
        var selectSizeText = $(this).data("selectsizetext");
        $button = $(this);

        $button.text(addingText);

        $("body").removeClass("ShowBlackBg");

        if ($sizeId != null && $sizeId > 0) {


            $.ajax({
                url: "/" + langCode + "/Cart/Add",
                data: 'sizeID=' + $sizeId + "&colorID=" + $colorId + "&productID=" + $productId + "&langID=" + langId + "&stockId=" + $stockId,
                success: function (dt) {

                    $button.text($defaultText);
                    $("#CartModal .modal-body").html(dt);
                    $("#CartModal").modal('show');

                    $("#Total").hide();
                    $("#Total").fadeIn("slow");

                    handleCartTotalItems();


                    $("#Sizes div").removeClass("active");
                    $sizeId = null;

                }
            });
        }
        else {

            $button.text(selectSizeText);
            $("body").addClass("ShowBlackBg");
        }
    });


});

