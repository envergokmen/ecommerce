﻿function handleCartTotalItems() {

    var totalItem = $("#Cart #ItemCount").text();
    var curText = $("#CartModal h5").data("text");
    if (!totalItem) totalItem = 0;

    $("#CartModal h5").text(curText + "(" + totalItem + ")");
    $("#CartItemCount").text(" (" + totalItem + ")");

    if ($("#HiddenSummary").length > 0) { $("#Summary").html($("#HiddenSummary").html()); }

    sessionStorage.setItem("CartCount", totalItem);
}

function getCart(showModal) {
    $.ajax({
        url: "/" + langCode + "/Cart/Get",
        success: function (dt) {

            $("#CartModal .modal-body").html(dt);

            if (showModal) {
                $("#CartModal").modal('show');
            }

            handleCartTotalItems();
        }
    });
}


function getCartCount() {

    if (!sessionStorage.getItem("CartCount") || $("#LoginRegisterCon").length>0) {
        $.ajax({
            url: "/" + langCode + "/Cart/GetCount",
            success: function (totalItem) {

                $("#CartItemCount").text(" (" + totalItem + ")");
                sessionStorage.setItem("CartCount", totalItem);
            }
        });
    } else {
        $("#CartItemCount").text(" (" + sessionStorage.getItem("CartCount") + ")");
    }
}

$(document).ready(function () {

    getCartCount();

    $viewType = $("#Checkout").length <= 0 ? "Cart" : "CheckOutCart";

    $("#LogOut").on('mouseenter', function () {

        sessionStorage.removeItem("CartCount");
    });

    $("#CartButton").on('mouseenter', function () {

        if ($("#Cart #ItemCount").length <= 0) {
            getCart(false);
        }
    });

    $("#CartButton").on('click', function () {

        if ($("#Cart #ItemCount").length <= 0) {
            getCart(true);
        } else {
            $("#CartModal").modal('show');
        }
    });

    $("body").on('click', ".DeleteCartItem", function () {

        var itemId = $(this).attr("data-id");
        $item = $(this).closest(".CartItem");

        $.ajax({
            url: "/" + langCode + "/Cart/Remove",
            data: 'id=' + itemId + "&viewType=" + $viewType,
            success: function (dt) {


                $item.fadeOut('fast', function () {
                    $("#CartModal .modal-body, #Checkout #Content").html(dt);
                    handleCartTotalItems();

                    $("#Total").hide();
                    $("#Total").fadeIn("slow");
                });

            }
        });
    });

    $("body").on('click', ".quantity-btn", function () {

        var itemId = $(this).closest("div.CartItem").find(".DeleteCartItem").attr("data-id");
        $item = $(this).closest(".CartItem");
        var quantity = Number($(this).closest("div").find(".Quantity").text());

        if ($(this).hasClass("minus")) {
            quantity--;
        } else {
            quantity++;
        }

        if (quantity > 0) {

            $.ajax({
                url: "/" + langCode + "/Cart/SetQuantity",
                data: 'id=' + itemId + "&quantity=" + quantity + "&viewType=" + $viewType,
                success: function (dt) {


                    $item.fadeOut('fast', function () {
                        $("#CartModal .modal-body, #Checkout #Content").html(dt);
                        handleCartTotalItems();

                        $("#Total").hide();
                        $("#Total").fadeIn("slow");
                    });

                }
            });
        } else {
            return;
        }
    });

});