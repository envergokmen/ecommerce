﻿function refreshSummary(showCouponLine) {
    $.ajax({
        url: "/" + langCode + "/checkoutoperations/summary",
        success: function (dt) {

            $("#Summary").html(dt);

            if (showCouponLine) {

                $("#SelectedCoupon").hide();
                $("#SelectedCoupon").show("slow");
            }

        }
    });
}

function showActivePosItems() {

    var step = $("#Steps .active").data("step");
    if (step == "payment") {

        var paymentid = $("#PaymentGroups label.active").data("paymentid");

        if (paymentid == undefined) {
            paymentid = $("#PaymentGroups label:first").data("paymentid");
        }

        $("#Poses .PosItem").hide();
        $("#Poses .PosItem[data-paymentid='" + paymentid + "']").show();

        $("#PaymentGroups label").removeClass("active");
        $("#PaymentGroups label[data-paymentid='" + paymentid + "']").addClass("active");

        var paymenttype = $("#PaymentGroups label.active").data("paymenttype");


        $(".PaymentDetail").hide();
        $(".PaymentDetail[data-paymenttype='" + paymenttype + "']").show();

        $InstallmentInput = $("#" + $("#PaymentGroups label.active").attr("for"));

        $InstallmentInput.prop('checked', 'checked');
        $InstallmentInput.trigger('change');

    }
}

$(document).ready(function () {

    if ($("#Steps").length) {
        $step = $("#Steps .active").data("step");
        showActivePosItems();
    }

    $isprogress = false;

    $("#Steps").on('click', "> div > div", function (e) {

        $step = $(this).data("step");

        if ($step == "payment" && $isprogress) {
            $step = "progress";
        }

        $stepItem = $(this);

        var validCard = true;

        if ($step == "progress") {

            if ($("#CardForm:visible").length > 0) {
                $form = $("#CardForm");
                $.ajax({
                    type: 'post',
                    async: false,
                    cache: false,
                    url: "/" + langCode + "/checkoutoperations/setcard",
                    data: $form.serialize(),
                    success: function (dt) {

                    },
                    error: function (xhr, textStatus, errorThrown) {

                        $("#Errors").html(xhr.responseText);
                        validCard = false;
                    }
                });
            }
        }

        if (!validCard) {
            e.preventDefault();
            return false;
        }

        $.ajax({
            url: "/" + langCode + "/checkout/" + $step,
            success: function (dt) {


                if ($step == "progress") {
                    window.location.href = "/" + langCode + "/checkout/progress";
                    return false;
                    e.preventDefault();
                }

                $("#Steps div").removeClass("active");
                $stepItem.addClass("active");

                $("#Content").html(dt);
                history.replaceState({}, '', "/" + langCode + "/checkout/" + $step);

                $("body").removeClass();
                $("body").addClass($step);
                $("#Errors").html("");
                refreshSummary();

                showActivePosItems();

                if ($("#HiddenSummary").length > 0) { $("#Summary").html($("#HiddenSummary").html()); }

            },
            error: function (xhr, textStatus, errorThrown) {

                if (xhr.status == 400 && xhr.responseText) {
                    $("#Errors").html(xhr.responseText);
                } else if (xhr.status = 401) {

                    $("#Errors").html(xhr.responseText);
                    $("#LoginRegisterModal").modal("show");

                } else {
                    $("#Errors").html("Unknown error please try again later or let us know");
                }

            }
        });

    });


    $("#Login, #Register").on('click', function () {

        $("#LoginRegisterModal").modal("show");
        $("#" + $(this).data('tab')).trigger("click");

    });


    $("#pills-guest-tab").on('click', function () {
        $.ajax({
            url: "/" + langCode + "/checkoutoperations/setasguest?value=true",
            success: function (dt) {

                $("#Errors").html("");
                $("#Content").html(dt);
                if ($("#HiddenSummary").length > 0) { $("#Summary").html($("#HiddenSummary").html()); }

                $("#LoginRegisterModal").modal("hide");
                $("#LoginRegister").addClass("Guest");
            },
            error: function (xhr, textStatus, errorThrown) {

                $("#Errors").html(xhr.responseText);
            }
        });
    });

    $("#Nav").on('click', "#Next", function () {

        $next = $("#Steps > div > div.active").next();

        if ($("#Steps > div > div.active").data("next") == "progress") {
            $isprogress = true;
            $("#Steps > div > div.active").trigger("click");
            return;
        }

        if ($next) {
            $next.trigger("click");
        }

    });

    $("#Nav").on('click', "#Prev", function () {

        $prev = $("#Steps > div > div.active").prev();
        if ($prev) {
            $prev.trigger("click");
        }

    });

    //refreshSummary();


    function refreshUser() {
        $.ajax({
            url: "/" + langCode + "/checkout/user",
            success: function (dt) {

                $("#User").html(dt);

            }
        });
    }

});

