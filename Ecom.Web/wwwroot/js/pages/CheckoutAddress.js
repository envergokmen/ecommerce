﻿$(document).ready(function () {


    $("body").on("change", ".CountryID", function () {

        var countryId = $(this).val();

        $.ajax({
            url: '/' + langCode + '/Address/GetCities?countryId=' + countryId,
            dataType: 'json',
            success: function (dt) {

                $('.CityID option, .TownID option').remove();

                for (var i = 0; i < dt.length; i++) {

                    $('.CityID')
                        .append($("<option></option>")
                            .attr("value", dt[i].Value)
                            .text(dt[i].Text));

                }
                $('.CityID').show();
            }
        });

    });

    $("body").on("change", ".CountryID", function () {

        var countryId = $(this).val();

        $.ajax({
            url: '/' + langCode + '/Address/GetCities?countryId=' + countryId,
            dataType: 'json',
            success: function (dt) {

                $('.CityID option, .TownID option').remove();

                for (var i = 0; i < dt.length; i++) {

                    $('.CityID')
                        .append($("<option></option>")
                            .attr("value", dt[i].Value)
                            .text(dt[i].Text));

                }
                $('.CityID').show();
            }
        });

    });

    $("body").on("change", ".CityID", function () {

        var cityID = $(this).val();

        $.ajax({
            url: '/' + langCode + '/Address/GetTowns?cityid=' + cityID,
            dataType: 'json',
            success: function (dt) {

                $('.TownID option').remove();

                for (var i = 0; i < dt.length; i++) {

                    $('.TownID')
                        .append($("<option></option>")
                            .attr("value", dt[i].Value)
                            .text(dt[i].Text));

                }

                $('.TownID').show();

            }
        });
    });

    $("body").on("submit", "#CheckAddressForm", function (e) {

        $form = $(this);
        e.preventDefault();
        $(".AjaxLoader").show();

        $.ajax({
            method: "POST",
            url: $form.attr("action"),
            data: $form.serialize(),
            success: function (dt) {

                if ($("#Account").length > 0) {
                    window.location.reload();
                }

                $("#Content:first").html(dt);
                $("#Errors").html("");
                refreshSummary();

            },
            error: function (xhr, textStatus, errorThrown) {

                $("#Errors").html(xhr.responseText);

            }

        });


        return false;


    });

    $("body").on('click', "#CancelAddress", function () {
        $("#AddressFormArea").html('');
    });

    $("body").on('click', ".EditAddress, #NewAddress", function () {

        var id = $(this).data("id");
        var editAddressController = "checkoutoperations";

        if ($("#Account").length > 0) {
            editAddressController = "address";
        }

        $.ajax({
            url: "/" + langCode + "/" + editAddressController +"/editaddress?id=" + id,
            success: function (dt) {

                $("#AddressFormArea").html(dt);

            }
        });

    });


    $("#Checkout").on('change', ".AddressItem input", function () {

        var id = $(this).val();
        var type = $(this).data("adrtype");

        $.ajax({
            url: "/" + langCode + "/checkoutoperations/setaddress?id=" + id + "&adrtype=" + type,
            success: function (dt) {

                $("#Errors").html("");

                if (type == "ship") {
                    refreshCargo();
                }

                refreshSummary();
            }
        });

    });

    $("#Checkout").on('change', ".CargoItem input", function (e) {

        var id = $(this).val();

        $.ajax({
            url: "/" + langCode + "/checkoutoperations/setcargoprice?id=" + id,
            success: function (dt) {

                $("#Errors").html("");
                refreshCargo();
                refreshSummary();
            },
            error: function (xhr, textStatus, errorThrown) {
                $("#Errors").html(xhr.responseText);
            }
        });

    });



    $("body").on('click', ".DeleteAddress", function () {

        var id = $(this).data("id");

        var editAddressController = "checkoutoperations";

        if ($("#Account").length>0) {
            editAddressController = "address";
        }

        $.ajax({
            url: "/" + langCode + "/" + editAddressController+"/deleteaddress?id=" + id,
            success: function (dt) {

                if (editAddressController == "address") {
                    document.location.reload();
                }

                $("#Content:first").html(dt);
                $("#Errors").html("");

                refreshCargo();
                refreshSummary();

            }
        });

    });



    function refreshCargo() {
        $.ajax({
            url: "/" + langCode + "/checkoutoperations/cargo",
            success: function (dt) {

                $("#Cargo").html(dt);

            }
        });
    }

});