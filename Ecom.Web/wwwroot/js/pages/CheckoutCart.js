﻿$(document).ready(function () {

   

    $("#Checkout").on('click', ".CouponItem #UseCoupon", function (e) {

        var id = $(this).val();
        $couponItem = $(this).closest(".CouponItem");

        $.ajax({
            url: "/" + langCode + "/checkoutoperations/setcoupon?id=" + id + "&couponCode=" + null,
            success: function (dt) {


                $(".CouponItem").removeClass("added");
                $couponItem.addClass("added");
                $("#Errors").html("");

                refreshSummary(true);
            },
            error: function (xhr, textStatus, errorThrown) {
                $("#Errors").html(xhr.responseText);
            }
        });

    });


    $("#Checkout").on('submit', "#CouponForm", function (e) {

        e.preventDefault();
        e.stopPropagation();

        var couponCode = $("#CouponCode").val();

        $.ajax({
            url: "/" + langCode + "/checkoutoperations/setcoupon?couponCode=" + couponCode,
            success: function (dt) {

                $("#Errors").html("");
                refreshSummary(true);
            },
            error: function (xhr, textStatus, errorThrown) {
                $("#Errors").html(xhr.responseText);
            }
        });

    });


    $("#Checkout").on('click', ".CouponItem #RemoveCoupon, .RemoveCoupon", function (e) {

        $.ajax({
            url: "/" + langCode + "/checkoutoperations/removecoupon",
            success: function (dt) {

                $(".CouponItem").removeClass("added");

                $("#Errors").html("");
                refreshSummary();
            },
            error: function (xhr, textStatus, errorThrown) {
                $("#Errors").html(xhr.responseText);
            }
        });

    });

});