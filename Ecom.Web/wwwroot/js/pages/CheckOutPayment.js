﻿function formatCC(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{4,16}/g);
    var match = matches && matches[0] || ''
    var parts = []
    for (i = 0, len = match.length; i < len; i += 4) {
        parts.push(match.substring(i, i + 4))
    }
    if (parts.length) {
        return parts.join(' ')
    } else {
        return value
    }
}

function validateCardNumber(number) {
    var regex = new RegExp("^[0-9]{16}$");
    if (!regex.test(number))
        return false;

    return luhnCheck(number);
}

function luhnCheck(val) {
    var sum = 0;
    for (var i = 0; i < val.length; i++) {
        var intVal = parseInt(val.substr(i, 1));
        if (i % 2 == 0) {
            intVal *= 2;
            if (intVal > 9) {
                intVal = 1 + (intVal % 10);
            }
        }
        sum += intVal;
    }
    return (sum % 10) == 0;
}

function IsNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function ShowInstallmentByBankCode(bankcode, cardType, allowInstallment) {


    var paymentid = $("#PaymentGroups label.active").data("paymentid");

    $(".PosItem").hide();
    $posForBin = $(".PosItem[data-bankcode='" + bankcode + "'][data-paymentid='" + paymentid + "']");

    if (cardType == "creditcard") {

        $posForBin.show();
        $(".ins, .PosHeader", $posForBin).show();
        $(".ins.single input:first", $posForBin).prop('checked', 'checked');
        $(".ins.single input:first", $posForBin).trigger('change');

    } else if (cardType == "debit") {

        $(".ins.multiple", $posForBin).hide();
        $(".ins.single, .PosHeader", $posForBin).show();
        $posForBin.show();

        $(".ins.single input:first", $posForBin).prop('checked', 'checked');
        $(".ins.single input:first", $posForBin).trigger('change');

    } 

    if ($posForBin.length <= 0) {

        $posForPayType = $(".PosItem[data-paymentid='" + paymentid + "']:first");
        $posForPayType.show();

        $(".ins.single", $posForPayType).show();
        $(".ins.multiple", $posForPayType).hide();
        $(".PosHeader", $posForPayType).hide();

        $(".ins.single input:first", $posForPayType).prop('checked', 'checked');
        $(".ins.single input:first", $posForPayType).trigger('change');
    }
}

$lastBin = "";

function CheckBinNumer(bin) {

    if (bin >= 6 && $lastBin != bin.substr(0, 6)) {

        $lastBin = bin.substring(0, 6)

        $.ajax({
            url: "/" + langCode + "/CheckoutOperations/getbin?binno=" + $lastBin,
            dataType: 'json',
            success: function (jsonReturn) {

                if (jsonReturn.length !== 0) {

                    $.each(jsonReturn, function (index, element) {
                        ShowInstallmentByBankCode(element.BankCode, element.CardType, element.AllowInstallment);
                    });

                }
                else {

                    ShowInstallmentByBankCode("-1", "", true);
                }

            }


        });
    }
}

$(document).ready(function () {

    $("#Checkout").on('click', "#PaymentGroups label", function (e) {


        if ($(this).hasClass("active")) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }

        var paymentid = $(this).data("paymentid");
        var paymenttype = $(this).data("paymenttype");

        $("#Poses .PosItem").hide();
        $("#Poses .PosItem[data-paymentid='" + paymentid + "']").show();

        $(".PaymentDetail").hide();
        $(".PaymentDetail[data-paymenttype='" + paymenttype + "']").show();

        $(".PosItem[data-paymentid='" + paymentid + "'] , .PosItem[data-paymentid='" + paymentid + "'] .ins").show();

    });

    $("#Checkout").on('change', ".ins input", function (e) {

        var id = $(this).val();
        var paymentid = $(this).data("paymentid");

        $.ajax({
            url: "/" + langCode + "/checkoutoperations/setinstallment?id=" + id,
            success: function (dt) {

                $("#Errors").html("");
                $("#PaymentGroups label").removeClass("active");
                $("#PaymentGroups label[data-paymentid='" + paymentid + "']").addClass("active");


                refreshSummary();
            },
            error: function (xhr, textStatus, errorThrown) {
                $("#Errors").html(xhr.responseText);
            }
        });

    });

    $("#Checkout").on('change', ".BankItem input", function (e) {

        var id = $(this).val();
        $(".BankItem").removeClass("active");
        $(this).closest(".BankItem").addClass("active");

        $.ajax({
            url: "/" + langCode + "/checkoutoperations/setbank?id=" + id,
            success: function (dt) {

                $("#Errors").html("");
                refreshSummary();
            },
            error: function (xhr, textStatus, errorThrown) {
                $("#Errors").html(xhr.responseText);
            }
        });

    });


    $("body").on("keyup", "input#CardNumber", function (e) {

        cardNumber = formatCC($(this).val().replace(/\D/g, ''));
        $(this).val(cardNumber);

        $("#CardNumbers > input").removeClass("NotValid");

        if (cardNumber.replace(/\D/g, '').length == 16 && validateCardNumber(cardNumber.replace(/\D/g, '')) == false) {

            $("#CardNumbers > input").addClass("NotValid");

        } 

        //if (IsNumeric($(this).val().replace(/\D/g, '')) == false && $(this).val() != "") {

        //    $(this).addClass("NotValid");
        //}

        if (cardNumber.length >= 8) {
            CheckBinNumer(cardNumber.replace(/\D/g, ''));
        }

    });



});

