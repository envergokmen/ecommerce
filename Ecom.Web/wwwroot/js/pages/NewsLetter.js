﻿$(document).ready(function () {

    $("body").on("submit", "#NewsLetterForm", function (e) {

        e.preventDefault();
        $form = $(this);
        $NewsLetterResult = $(".NewsLetterResult");

        $.ajax({
            type: $form.attr("method"),
            url: $form.attr("action"),
            data: $form.serialize(),
            dataType: "json",
            success: function (dt) {

                if (dt.Status == true) {

                    $("#FormContainer").hide();
                    $NewsLetterResult.show();
                    $NewsLetterResult.html(dt.ResultDescription[0]);
                    $NewsLetterResult.addClass("alert-success");
                    $NewsLetterResult.removeClass("alert-danger");
                    $(".NewsletterFormHead").hide();

                }
                else {

                    $NewsLetterResult.show();
                    $NewsLetterResult.removeClass("alert-success");

                    if (dt.ResultDescription != undefined && dt.ResultDescription.length > 0) {
                        $NewsLetterResult.addClass("alert-danger");
                        $NewsLetterResult.html(dt.ResultDescription[0]);
                        $(".NewsletterFormHead").hide();
                    }

                }

            }

        });

        return false;


    });


});