﻿$(document).ready(function () {

    $totalPage = Number($("#totalPage").val());
    $nextpage = Number($("#p"));
    $isNewRequestAllowed = true;
    //$(".Pages").hide();
    //$lastPage = 1;

    $("body").on('change', "#FilterForm input", function (e) {

        var isScrollChange = $(this).attr("id")=="p";
        var isGridChange = $(this).attr("id") == "g";

        //if (changedId != "p" || $("#p").val() == "1") {
        //    $("#p").val("");
        //}

        if (!isScrollChange && !isGridChange) {
            $nextpage = 1;
            $("#p").val(1);
        }

        console.log("isScrollChange", isScrollChange);

        window.location.hash = compress($("form#FilterForm").serialize()).replace("&p=1","");
        var params = $("#FilterForm").serialize();
        $isNewRequestAllowed = false;

        $.ajax({
            type: "POST",
            url: "/" + langCode + "/Catalog/Products",
            data: params,
            cache: false,
            success: function (data) {

                if (!isScrollChange && !isGridChange) {
                   
                    $("#Products").html('');
                    $("#CatalogEnded").hide();
                    $isNewRequestAllowed = true;

                    setTimeout(function () {
                     $isNewRequestAllowed = true;
                    }, 1000);

                    //$lastPage = 1;
                } else {
                   $(".Pages:eq(1)").hide();
                }


                $("#Products").append(data);
                $("#p").val($nextpage);

                if ($nextpage >= $totalPage || isBlank(data)) {
                    $("#CatalogEnded").fadeIn("slow");
                }

                setTimeout(function () {
                    if ($nextpage < $totalPage && !isBlank(data)) {
                        $isNewRequestAllowed = true;
                    }
                }, 1000);

            }

        });


    });

    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }

    $(window).scroll(function () {

        if ($("#Products.Search").length <= 0 && $("#Products").length > 0 && $isNewRequestAllowed && isScrolledIntoView($("#Products"))) {

            $nextpage = $("#p").val() == "" ? 2 : Number($("#p").val()) + 1;
            $nextpage  = $nextpage <= $totalPage ? $nextpage : $totalPage

            if ($nextpage <= $totalPage) {

                $("#p").val($nextpage);
                $("#p").trigger("change");

            } 

        }

    });

    function isScrolledIntoView(elem) {

        var $elem = $(elem);
        var $window = $(window);
        if ($elem.offset() != undefined) {
            var docViewTop = $window.scrollTop();
            var docViewBottom = docViewTop + $window.height();
            //console.log($elem.offset());
            var elemTop = $elem.offset().top;
            var elemBottom = elemTop + $elem.height();
            return (((elemBottom - 750) <= (docViewBottom)));
        }

    }

    $("body").on('click', "#Products .Colors > div", function () {

        var colorID = $(this).attr("data-color");
        var img1 = $(this).attr("data-img1");
        var img2 = $(this).attr("data-img2");
        $("div", $(this).closest(".Colors")).removeClass("active");
        $(this).addClass("active");

        var productId = $(this).closest(".ProductItem").attr("data-id");
        $productDiv = $(this).closest(".ProductItem");

        $("img.ProductImage", $productDiv).attr("src", img1);

    });

    $("body").on('mouseenter', "#Products .ProductImage", function () {

        $productDiv = $(this).closest(".ProductItem");
        $activeColor = $(".Colors .active", $productDiv);

        var img1 = $activeColor.attr("data-img1");
        var img2 = $activeColor.attr("data-img2");

        $(this).attr("src", img2);

    });


    $("body").on('mouseleave', "#Products .ProductImage", function () {

        $productDiv = $(this).closest(".ProductItem");
        $activeColor = $(".Colors .active", $productDiv);

        var img1 = $activeColor.attr("data-img1");
        var img2 = $activeColor.attr("data-img2");

        $(this).attr("src", img1);

    });


});

