﻿using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.ViewModels.Campaign;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.Orders;
using Ecom.ViewModels.Pages;
using Ecom.ViewModels.Payments;
using Ecom.ViewModels.PriceCurrencies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecom.Web.Helpers
{
    public class CheckoutStateHelper
    {
        //State items
        private string Step { get; set; }

        //cart
        private bool? CheckoutAsGuest { get; set; }
        private long? CouponId { get; set; }

        //adress
        private bool? UseTheSameForBillAndShip { get; set; }
        private int? BillingAddressId { get; set; }
        private int? ShippingAddressId { get; set; }
        private int? CargoPriceId { get; set; }
        private int? BankId { get; set; }

        //payment
        //private PaymentType? PaymentType { get; set; }
        private int? WebPosInstallmentId { get; set; }
        private long CreatedOrderId { get; set; }
        private OrderDetailsVM CreatedOrder { get; set; }
        private StandartOperationResult FailResult { get; set; }
        private OperationResult<PaymentHistoryViewModel> PaymentResult { get; set; }
        private CreditCardVM CreditCard { get; set; }

        //base Items
        private int storeId { get; set; }
        private int? userId { get; set; }
        private int? domainId { get; set; }
        private string specialKey { get; set; }
        private int priceCurrencyId;
        private bool hasBaseItems = false;
        private bool IsLocked { get; set; }

        public LangViewModel Lang { get; set; }
        public PriceCurrencyVM Currency { get; set; }
        public CartVM Cart { get; set;

        }
        public void SetBaseItems(int storeId, int priceCurrencyId, int? userId, string specialKey, int domainId, LangViewModel lang, PriceCurrencyVM currency, CartVM cart)
        {
            this.storeId = storeId;
            this.priceCurrencyId = priceCurrencyId;
            this.userId = userId != null && userId > 0 ? userId : null as int?;
            this.specialKey = specialKey;
            this.Lang = lang;
            this.Currency = currency;
            this.Cart = cart;
            this.domainId = domainId;
            hasBaseItems = true;
        }

        private readonly HttpContext httpContext;
        private readonly CartService cartService;
        private readonly PaymentService paymentService;
        private readonly PriceCurrencyService priceCurrencyService;
        private readonly IMemberShipService memberShipService;
        IHttpContextAccessor _contextAccessor;
        private readonly IStringLocalizer<SharedDataResource> localizer;

        public CheckoutStateHelper(PriceCurrencyService _priceCurrencyService, IStringLocalizer<SharedDataResource> _localizer, IHttpContextAccessor contextAccessor, CartService _cartService, PaymentService _paymentService, IMemberShipService _memberShipService)
        {
            httpContext = contextAccessor?.HttpContext;
            cartService = _cartService;
            memberShipService = _memberShipService;
            localizer = _localizer;
            paymentService = _paymentService;
            this.priceCurrencyService = _priceCurrencyService;
            this.Step = GetStep();
        }

        public void SetStep(string step)
        {
            httpContext.Session.SetString("step", step);
            this.Step = step;
        }

        public void SetIGuest(bool value)
        {
            httpContext.Session.SetString("CheckoutAsGuest", value.ToString());
            this.CheckoutAsGuest = value;
        }

        public bool IsGuest()
        {
            var chkGuest = httpContext.Session.GetString("CheckoutAsGuest");
            if (chkGuest != null)
            {
                bool checkAsGuest = Convert.ToBoolean(chkGuest);
                this.CheckoutAsGuest = checkAsGuest;
                return checkAsGuest;
            }

            return false;
        }


        public void SetBillingAddressId(int? addressId)
        {
            if (addressId.HasValue)
            {
                httpContext.Session.SetInt32("BillingAddressId", addressId.Value);

            }
            else
            {
                httpContext.Session.Remove("BillingAddressId");
            }

            this.BillingAddressId = addressId;
        }

        public int? GetBillingAddressId()
        {
            var adressid = httpContext.Session.GetInt32("BillingAddressId");
            if (adressid != null)
            {

                this.BillingAddressId = adressid;
                return adressid;
            }

            return null;
        }


        public void SetShippingAddressId(int? addressId)
        {
            if (addressId.HasValue)
            {
                httpContext.Session.SetInt32("ShippingAddressId", addressId.Value);
            }
            else
            {
                httpContext.Session.Remove("ShippingAddressId");
            }
            this.ShippingAddressId = addressId;
        }

        public int? GetShippingAddressId()
        {
            var adressid = httpContext.Session.GetInt32("ShippingAddressId");
            if (adressid != null)
            {

                this.ShippingAddressId = adressid;
                return adressid;
            }

            return null;
        }



        public void SetCargoPriceId(int? cargoPriceId)
        {
            if (cargoPriceId.HasValue)
            {
                httpContext.Session.SetInt32("CargoPriceId", cargoPriceId.Value);
            }
            else
            {
                httpContext.Session.Remove("CargoPriceId");
            }
            this.CargoPriceId = cargoPriceId;
        }

        public int? GetCargoPriceId()
        {
            var cargoPriceId = httpContext.Session.GetInt32("CargoPriceId");
            if (cargoPriceId != null)
            {
                this.CargoPriceId = cargoPriceId;
                return cargoPriceId;
            }

            return null;
        }


        public void SetBankId(int? BankId)
        {
            if (BankId.HasValue)
            {
                httpContext.Session.SetInt32("BankId", BankId.Value);
            }
            else
            {
                httpContext.Session.Remove("BankId");
            }
            this.BankId = BankId;
        }

        public int? GetBankId()
        {
            var BankId = httpContext.Session.GetInt32("BankId");
            if (BankId != null)
            {
                this.BankId = BankId;
                return BankId;
            }

            return null;
        }

        public void SetCreatedOrder(OrderDetailsVM createdOrder)
        {
            if (CreatedOrder != null)
            {
                httpContext.Session.SetString("CreatedOrder", JsonConvert.SerializeObject(createdOrder));
                this.CreatedOrder = createdOrder;
            }
            else
            {
                httpContext.Session.Remove("CreatedOrder");
            }

        }

        public OrderDetailsVM GetCreatedOrder()
        {
            var createdOrder = httpContext.Session.GetString("CreatedOrder");
            if (createdOrder != null)
            {
                this.CreatedOrder = JsonConvert.DeserializeObject<OrderDetailsVM>(createdOrder);
                return this.CreatedOrder;
            }

            return null;
        }



        public void SetCreditCard(CreditCardVM CreditCard)
        {
            if (CreditCard != null)
            {
                httpContext.Session.SetString("CreditCard", JsonConvert.SerializeObject(CreditCard));
                this.CreditCard = CreditCard;
            }
            else
            {
                httpContext.Session.Remove("CreditCard");
            }

        }

        public CreditCardVM GetCreditCard()
        {
            var CreditCard = httpContext.Session.GetString("CreditCard");
            if (CreditCard != null)
            {
                this.CreditCard = JsonConvert.DeserializeObject<CreditCardVM>(CreditCard);
                return this.CreditCard;
            }

            return null;
        }


        public void SetFailResult(StandartOperationResult failResult)
        {
            if (failResult != null)
            {
                httpContext.Session.SetString("FailResult", JsonConvert.SerializeObject(failResult));
                this.FailResult = failResult;
            }
            else
            {
                httpContext.Session.Remove("FailResult");
            }

        }

        public StandartOperationResult GetFailResult()
        {
            var FailResult = httpContext.Session.GetString("FailResult");
            if (FailResult != null)
            {
                this.FailResult = JsonConvert.DeserializeObject<StandartOperationResult>(FailResult);
                return this.FailResult;
            }

            return null;
        }


        public void SetPaymentResult(OperationResult<PaymentHistoryViewModel> PaymentResult)
        {
            if (PaymentResult != null)
            {
                httpContext.Session.SetString("PaymentResult", JsonConvert.SerializeObject(PaymentResult));
                this.PaymentResult = PaymentResult;
            }
            else
            {
                httpContext.Session.Remove("PaymentResult");
            }
        }

        public OperationResult<PaymentHistoryViewModel> GetPaymentResult()
        {
            var PaymentResult = httpContext.Session.GetString("PaymentResult");
            if (PaymentResult != null)
            {
                this.PaymentResult = JsonConvert.DeserializeObject<OperationResult<PaymentHistoryViewModel>>(PaymentResult);
                return this.PaymentResult;
            }

            return null;
        }

        public void SetCreatedOrderId(long? CreatedOrderId)
        {
            if (CreatedOrderId.HasValue)
            {
                httpContext.Session.SetString("CreatedOrderId", CreatedOrderId.Value.ToString());
                this.CreatedOrderId = Convert.ToInt64(CreatedOrderId);
            }
            else
            {
                httpContext.Session.Remove("CreatedOrderId");
            }

        }

        public long? GetCreatedOrderId()
        {
            var CreatedOrderIdStr = httpContext.Session.GetString("CreatedOrderId");
            if (CreatedOrderIdStr != null)
            {
                this.CreatedOrderId = Convert.ToInt64(CreatedOrderIdStr);
                return this.CreatedOrderId;
            }

            return null;
        }


        public void SetIsLocked(bool IsLocked)
        {

            httpContext.Session.SetInt32("IsLocked", Convert.ToInt32(IsLocked));
            this.IsLocked = IsLocked;
        }

        public bool GetIsLocked()
        {
            var IsLocked = httpContext.Session.GetInt32("IsLocked");
            if (IsLocked != null)
            {
                return Convert.ToBoolean(IsLocked.Value);
            }

            return false;
        }

        public int? GetWebPosInstallmentId()
        {
            var webPosInstallmentId = httpContext.Session.GetInt32("WebPosInstallmentId");
            if (webPosInstallmentId != null)
            {

                this.WebPosInstallmentId = webPosInstallmentId;
                return webPosInstallmentId;
            }

            return null;
        }

        public void SetWebPosInstallmentId(int? webPosInstallmentId)
        {
            if (webPosInstallmentId.HasValue)
            {
                httpContext.Session.SetInt32("WebPosInstallmentId", webPosInstallmentId.Value);

            }
            else
            {
                httpContext.Session.Remove("WebPosInstallmentId");
            }

            this.WebPosInstallmentId = webPosInstallmentId;
        }

        public long? GetCouponId()
        {
            var couponId = httpContext.Session.GetString("CouponId");
            if (couponId != null)
            {
                this.CouponId = Convert.ToInt64(couponId);
                return this.CouponId;
            }

            return null;
        }

        public void SetCouponId(long? couponId)
        {
            if (couponId.HasValue)
            {
                httpContext.Session.SetString("CouponId", couponId.ToString());

            }
            else
            {
                httpContext.Session.Remove("CouponId");
            }

            this.CouponId = couponId;
        }

        public string GetStep()
        {
            string curStep = null;

            if (this.Step != null)
            {
                curStep = this.Step;
            }
            else
            {
                curStep = httpContext.Session.GetString("step");
                if (curStep == null)
                {
                    curStep = "cart";
                }

                SetStep(curStep);
            }

            return curStep;
        }

        public StandartOperationResult ValidateCouponAdd(CouponVM coupon)
        {

            StandartOperationResult op = new StandartOperationResult();
            op.SetSuccessAndClearError();

            if (coupon == null)
            {
                op.SetError(localizer["KuponBulunamadi"]);
            }
            else
            {
                var pr = this.Currency;

                var BirimliTutar = (pr != null && coupon.AmountLimit != null) ? (" " + coupon.AmountLimit.ToString() + " " + pr.CurrencyCode) : coupon.AmountLimit.ToString();
                var MaxBirimliTutar = (pr != null && coupon.MaxAmountLimit != null) ? (" " + coupon.MaxAmountLimit.ToString() + " " + pr.CurrencyCode) : coupon.MaxAmountLimit.ToString();

                if (coupon.UserID.HasValue && this.userId != coupon.UserID)
                {
                    op.SetError(localizer["KuponBulunamadi"]);
                }

                //TODO : Çok kullanım özelliği olanlar isused olmaz
                if (coupon.IsUsed == true && coupon.AllowMultipleUse != true)
                {
                    op.SetError(localizer["KuponKullanilmis"]);
                }

                if (Cart.TotalAmount < coupon.AmountLimit)
                {
                    op.SetError(String.Format(localizer["KuponMinAlisVerisTutariHatasi"], BirimliTutar));
                }

                if (coupon.MaxAmountLimit != null && coupon.MaxAmountLimit != 0 && Cart.TotalAmount > coupon.MaxAmountLimit)
                {
                    op.SetError(String.Format(localizer["KuponMaxAlisverisTutariHatasi"], MaxBirimliTutar));
                }

                if (coupon.BeginDate > DateTime.Now)
                {
                    op.SetError(localizer["KuponKampanyaTarihiBaslamadi"]);
                }

                if (coupon.EndDate < DateTime.Now)
                {
                    op.SetError(localizer["KuponKampanyaTarihiBitti"]);
                }

                if (coupon.PriceCurrencyID != this.priceCurrencyId)
                {
                    op.SetError(localizer["KuponParaBirimiFarkli"]);
                }

                if (coupon.AllowOnDiscountedProducts.HasValue && !coupon.AllowOnDiscountedProducts.Value && Cart.HasDiscount)
                {
                    op.SetError(localizer["KuponIndirimliUrunlerdeGecersiz"]);
                }


                if (coupon.DiscountLimit.HasValue && coupon.DiscountLimit.Value < Cart.MaxDiscountPercent)
                {
                    op.SetError(String.Format( localizer["KuponEnFalzIndirimOrani"], coupon.DiscountLimit) );
                }


                if (coupon.DomainId != null && coupon.DomainId != this.domainId)
                {
                    op.SetError(String.Format(localizer["KuponBuDomainDeKullanilamaz"], "this"));

                }

            }

            return op;
        }

        public StandartOperationResult ValidateCreditCard(CreditCardVM card)
        {

            StandartOperationResult op = new StandartOperationResult();
            op.SetSuccessAndClearError();

            if (card == null || !SecureOperations.IsCreditCartNumberValid(card.CardNumber))
            {
                op.SetError("KrediKartiNumaranizDogrulanmadi");
                return op;
            }

            if (String.IsNullOrWhiteSpace(card.CardOwner)) op.AddError(localizer["KrediKartiSahibiIsimGerekli"]);
            if (card.CardCvc.ToString().Length == 0 || (card.CardCvc.ToString().Length > 4)) op.AddError(localizer["KrediKartiGuvenlikKoduHatali"]);
            if (card.ExpiresMonth == null || card.ExpiresMonth.Length == 0 || (card.ExpiresMonth != null && card.ExpiresMonth.Length > 2)) op.AddError(localizer["KrediKartiAyDogrulanmadi"]);
            if (card.ExpiresYear == null || card.ExpiresYear.Length == 0 || (card.ExpiresYear != null && card.ExpiresYear.Length > 2)) op.AddError(localizer["KrediKartiYilDogrulanmadi"]);

            if (!op.Status) op.ReturnCode = ReturnStatusCode.BadRequest;

            return op;
        }

        public StandartOperationResult ValidateStepChange(string nextStep)
        {
            StandartOperationResult op = new StandartOperationResult();
            var curStep = GetStep();
            if (!hasBaseItems) throw new Exception("Base items has not been set for validation, please try again");

            op.SetSuccessAndClearError();

            if (curStep == "cart" && (nextStep == "address" || nextStep == "payment" || nextStep == "progress"))
            {
                if (!this.userId.HasValue && !IsGuest())
                {

                    op.SetAsUnauthorized(localizer["CheckoutSignInRegister"]);
                    return op;
                }

                var cartItemCount = cartService.GetCount(this.storeId, this.userId, this.specialKey, this.priceCurrencyId);
                if (cartItemCount <= 0)
                {
                    op.SetAsBadRequest(localizer["EmptyCartError"]);
                    return op;
                }
            }

            if (nextStep == "payment" || nextStep == "progress")
            {
                if (!this.GetShippingAddressId().HasValue || !this.GetShippingAddressId().HasValue)
                {
                    op.SetAsBadRequest(localizer["EmptyShippingAddressError"]);
                    return op;
                }

                if (!this.CargoPriceId.HasValue)
                {
                    op.SetAsBadRequest(localizer["EmptyCargoError"]);
                    return op;
                }
            }

            if (nextStep == "progress")
            {
                if (this.GetWebPosInstallmentId() == null)
                {
                    op.SetAsBadRequest(localizer["EmptyPaymentError"]);
                    return op;
                }

                if (this.GetBankId() == null && this.GetWebPosInstallmentId() != null)
                {
                    var selectedPayment = paymentService.GetWebPosInstallmentByID(this.storeId, this.GetWebPosInstallmentId().Value);
                    if (selectedPayment != null && selectedPayment.PaymentType == PaymentType.Havale)
                    {
                        op.SetAsBadRequest(localizer["EmptyBankError"]);
                    }
                    return op;
                }
            }

            return op;
        }

    }
}
