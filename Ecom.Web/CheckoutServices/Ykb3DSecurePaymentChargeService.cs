﻿using System;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.IO;
using System.Xml;
using _PosnetDotNetModule;
using _PosnetDotNetTDSOOSModule;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Ecom.Web.Helpers;
using Ecom.Core;

namespace Ecom.Services.CheckoutServices
{
    public class Ykb3DSecurePaymentChargeService : PaymentChargeServiceBase, IPaymentChargeService
    {
        public Ykb3DSecurePaymentChargeService(
            CheckoutVM PaymentPageVM,
            OrderService orderService,
            IHttpContextAccessor contextAccessor,
            CheckoutStateHelper checkState
            )
        {
            this.orderService = orderService;
            this.PaymentPageVM = PaymentPageVM;
            this.contextAccessor = contextAccessor;
            this.checkState = checkState;
            this.HttpContext = contextAccessor.HttpContext;
        }



        public CheckoutVM MakePayment()
        {
            //if (!Validate().Status)
            //{

            //    if (checkOutState.PaymentResult == null) checkOutState.PaymentResult = new OperationResult<PaymentHistoryViewModel>();
            //    checkOutState.PaymentResult.AddError(Validate().ResultDescription);
            //    return checkOutState;
            //}

            InitializePaymentResult();

            try
            {

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                //System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();

                // allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                string protokol = PaymentPageVM.Protokol;

                C_PosnetOOSTDS posnetOOSTDSObj = new C_PosnetOOSTDS();

                string merchantPacket = null;
                string bankPacket = null;
                string sign = null;
                string tranType = null;

                //Banka tafafından yönlendirilen işlem bilgilerini alınır
                merchantPacket = HttpContext.Request.Form["MerchantPacket"];
                bankPacket = HttpContext.Request.Form["BankPacket"];
                sign = HttpContext.Request.Form["Sign"];
                tranType = HttpContext.Request.Form["TranType"];

                posnetOOSTDSObj.SetMid(PaymentPageVM.CreatedOrder.WebPosInstallment.MagazaNo);
                posnetOOSTDSObj.SetTid(PaymentPageVM.CreatedOrder.WebPosInstallment.TerminalID);
                posnetOOSTDSObj.SetPosnetID(PaymentPageVM.CreatedOrder.WebPosInstallment.PostNetNo);
                posnetOOSTDSObj.SetKey(PaymentPageVM.CreatedOrder.WebPosInstallment.SecureKey3D);

                if (!String.IsNullOrWhiteSpace(PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl))
                {
                    posnetOOSTDSObj.SetURL(PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl);
                }
                else
                {
                    posnetOOSTDSObj.SetURL("https://www.posnet.ykb.com/PosnetWebService/XML");
                }

                if (PaymentPageVM.CreatedOrder.WebPosInstallment.SecureKey3D == "10,10,10,10,10,10,10,10" || PaymentPageVM.CreatedOrder.WebPosInstallment.PostUrl.Contains("setmpos"))
                {
                    posnetOOSTDSObj.SetURL("http://setmpos.ykb.com/PosnetWebService/XML");
                }

                bool result = posnetOOSTDSObj.CheckAndResolveMerchantData(
                 merchantPacket,
                 bankPacket,
                 sign);

                //Başarısız ise
                if (!result)
                {
                    PaymentPageVM.PaymentResult.SetError(String.Concat(
                        "YKB 3D Error : Merchant data couldn't resolve : ",
                        posnetOOSTDSObj.GetResponseText(),
                        " Error Code : ", posnetOOSTDSObj.GetResponseCode()));

                }
                //Başarılı ise
                else
                {

                    if (posnetOOSTDSObj.GetTDSMDStatus() == "1") // (HttpContext.Current.Request.IsLocal && posnetOOSTDSObj.GetTDSMDStatus() == "9")
                    {
                        bool result2 = posnetOOSTDSObj.ConnectAndDoTDSTransaction(merchantPacket, bankPacket, sign);
                        string apprCode = posnetOOSTDSObj.GetApprovedCode();

                        if (apprCode == "1")
                        {
                            PaymentPageVM.PaymentResult.SetSuccessAndClearError(String.Concat("YKB 3D Payment has succeed ", PaymentPageVM.CreatedOrder.TotalAmount.ToString("n2"), $" {PaymentPageVM.CreatedOrder.CurrencyCode} tahsil edildi"));
                            PaymentPageVM.PaymentResult.Result.PaymentComplateStatus = PaymentComplateStaus.Tamamlandi;
                            PaymentPageVM.PaymentResult.Result.PaymentLogType = PaymentLogType.SiparisArti;
                        }
                        else
                        {
                            PaymentPageVM.PaymentResult.AddError(String.Concat(posnetOOSTDSObj.GetResponseCode(), posnetOOSTDSObj.GetResponseText()));
                        }

                    }
                    else
                    {
                        PaymentPageVM.PaymentResult.SetError("MD is not valid : " + posnetOOSTDSObj.GetTDSMDStatus());
                    }

                    string posAmount = posnetOOSTDSObj.GetAmount();

                    PaymentPageVM.PaymentResult.Result.PaymentDate = DateTime.Now;
                    PaymentPageVM.PaymentResult.Result.PayCode = posnetOOSTDSObj.GetAuthcode();

                    PaymentPageVM.PaymentResult.Result.ErrMsg = String.Concat(posnetOOSTDSObj.GetResponseCode(), posnetOOSTDSObj.GetResponseText());
                    PaymentPageVM.PaymentResult.Result.ExtraHostMsg = posnetOOSTDSObj.GetHostlogkey();
                }

                if (!String.IsNullOrWhiteSpace(HttpContext.Request.Form["mderrormessage"]))
                {
                    PaymentPageVM.PaymentResult.Result.ErrMsg = HttpContext.Request.Form["mderrormessage"].ToString();
                }

            }
            catch (Exception ex)
            {
                PaymentPageVM.PaymentResult.SetError(String.Concat("An Error has occured : ", ex.Message));
            }

            if (PaymentPageVM != null && PaymentPageVM.PaymentResult != null && PaymentPageVM.PaymentResult.Result != null)
            {
                PaymentPageVM.PaymentResult.Result.Description = PaymentPageVM.PaymentResult.GetErrors();
                orderService.AddPaymentHistoryLog(PaymentPageVM.PaymentResult.Result);
            }

            checkState.SetPaymentResult(PaymentPageVM.PaymentResult);

            return PaymentPageVM;

        }



    }


}