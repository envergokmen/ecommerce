﻿using System;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.IO;
using System.Xml;
using _PosnetDotNetModule;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Ecom.Web.Helpers;
using Ecom.Core;
using Ecom.ViewModels.Payments;

namespace Ecom.Services.CheckoutServices
{

    public sealed class ControlChars
    {
        public const char Back = '\b';
        public const char Cr = '\r';
        public const string CrLf = "\r\n";
        public const char FormFeed = '\f';
        public const char Lf = '\n';
        public const string NewLine = "\r\n";
        public const char NullChar = '\0';
        public const char Quote = '"';
        public const char Tab = '\t';
        public const char VerticalTab = '\v';
    }

    public class YkbPaymentChargeService : PaymentChargeServiceBase, IPaymentChargeService
    { 

            public YkbPaymentChargeService(
                CheckoutVM PaymentPageVM,
                OrderService orderService,
                IHttpContextAccessor contextAccessor,
                CheckoutStateHelper checkState
                )
            {
                this.orderService = orderService;
                this.PaymentPageVM = PaymentPageVM;
                this.contextAccessor = contextAccessor;
                this.checkState = checkState;
                this.HttpContext = contextAccessor.HttpContext;
            }


        public CheckoutVM MakePayment()
        {
            //if (!Validate().Status)
            //{

            //    if (checkOutState.PaymentResult == null) checkOutState.PaymentResult = new OperationResult<PaymentHistoryViewModel>();
            //    checkOutState.PaymentResult.AddError(Validate().ResultDescription);
            //    return checkOutState;
            //}

            InitializePaymentResult();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            //System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();

            // allows for validation of SSL conversations
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            PaymentPageVM.PaymentResult = new OperationResult<PaymentHistoryViewModel>(); ;

            try
            {
                

                    C_Posnet posnetObj = new C_Posnet();
                    bool result = false;

                    string instNumber =(PaymentPageVM.CreatedOrder.InstallmentQuantity!=null && PaymentPageVM.CreatedOrder.InstallmentQuantity>1) ? Convert.ToDouble(PaymentPageVM.CreatedOrder.InstallmentQuantity).ToString("00") : "00";
                     
                    if (1==2)
                    {
                        //TEST işlemi
                        //posnetObj.SetURL("http://setmpos.ykb.com/PosnetWebService/XML");

                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl))
                        {
                            posnetObj.SetURL(PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl);
                        }
                        else
                        {
                            posnetObj.SetURL(PaymentPageVM.CreatedOrder.WebPosInstallment.PostUrl);
                        }

                    }

                    posnetObj.SetMid(PaymentPageVM.CreatedOrder.WebPosInstallment.MagazaNo.ToString()); //Mağaza No
                    posnetObj.SetTid(PaymentPageVM.CreatedOrder.WebPosInstallment.TerminalID.ToString()); //675 ile başlayan TerminalID - 67511295

                    string islem = "PreAuth";

                    if (!String.IsNullOrWhiteSpace(PaymentPageVM.CreatedOrder.WebPosInstallment.ProvType))
                    {
                        islem = PaymentPageVM.CreatedOrder.WebPosInstallment.ProvType;
                    }

                    string bitis = String.Concat(PaymentPageVM.CreditCard.ExpiresYear, PaymentPageVM.CreditCard.ExpiresMonth);

                    string FormatliTutar =  PaymentPageVM.CreatedOrder.TotalAmount.ToString("n2");

                    FormatliTutar = FormatliTutar.Replace(".", "");
                    FormatliTutar = FormatliTutar.Replace(",", "");

                    string ORDid = PaymentPageVM.CreatedOrder.TrackCode;
                    int ordLength = ORDid.Length;

                    string beforeStr = "";
                    if (ordLength < 24)
                    {
                        for (int i = ordLength; i < 24; i++)
                        {

                            beforeStr += "0";
                        }
                    }
                     
                    ORDid = beforeStr + ORDid;

                    if (islem == "Sale")
                    {
                        //if (_koikode.Length > 0)
                        //{
                        //    posnetObj.SetKOICode(_koikode);
                        //}
                         
                        result = posnetObj.DoSaleTran(PaymentPageVM.CreditCard.CardNumber, bitis, PaymentPageVM.CreditCard.CardCvc.ToString(), ORDid, FormatliTutar, "YT", instNumber, null, null);

                    }

                    if (islem == "PreAuth")
                    { 
                        result = posnetObj.DoAuthTran(PaymentPageVM.CreditCard.CardNumber, bitis, PaymentPageVM.CreditCard.CardCvc.ToString(), ORDid, FormatliTutar, "YT", instNumber, null, null);

                    }

                    PaymentPageVM.PaymentResult.AddMessage(String.Concat("XML Request : " , posnetObj.GetXMLRequest() , ControlChars.NewLine , ControlChars.NewLine));

                    if (result)
                    {
                        PaymentPageVM.PaymentResult.AddMessage("XML Response : " + posnetObj.GetXMLResponse() + ControlChars.NewLine + ControlChars.NewLine);
                    }

                    WriteResponseParamToEventView(posnetObj);
                 
            }
            catch (Exception ex)
            {

                PaymentPageVM.PaymentResult.SetError(String.Concat("Bir hata oluştu : ", ex.Message));
                checkState.SetPaymentResult(PaymentPageVM.PaymentResult);

            }

            if (PaymentPageVM != null && PaymentPageVM.PaymentResult != null && PaymentPageVM.PaymentResult.Result != null)
            {
                PaymentPageVM.PaymentResult.Result.Description = PaymentPageVM.PaymentResult.GetErrors();
                orderService.AddPaymentHistoryLog(PaymentPageVM.PaymentResult.Result);
            }

          //  orderService.SetCheckOutState(checkOutState); 

            return PaymentPageVM;

        }

        private void WriteResponseParamToEventView(C_Posnet posnetObj)
        {

            string APPcode = posnetObj.GetApprovedCode();

            PaymentPageVM.PaymentResult.AddMessage(String.Concat("Approved Code : " , APPcode , ControlChars.NewLine));

            PaymentPageVM.PaymentResult.Result.Description = PaymentPageVM.PaymentResult.GetErrors();
            PaymentPageVM.PaymentResult.Result.ErrMsg = String.Concat(posnetObj.GetResponseCode(), posnetObj.GetResponseText());
            PaymentPageVM.PaymentResult.Result.ExtraHostMsg = posnetObj.GetHostlogkey();
            PaymentPageVM.PaymentResult.Result.PayCode = posnetObj.GetAuthcode();

            if (APPcode == "1")
            {

                PaymentPageVM.PaymentResult.SetSuccessAndClearError(String.Concat($"payment has been completed succesfully " , PaymentPageVM.CreatedOrder.TotalAmount.ToString("n2") , $" {PaymentPageVM.CreatedOrder.CurrencyCode} çekildi."));
                PaymentPageVM.PaymentResult.Result.PaymentComplateStatus = PaymentComplateStaus.Tamamlandi;
                PaymentPageVM.PaymentResult.Result.PaymentLogType = PaymentLogType.SiparisArti; 

            }
            else
            {
                PaymentPageVM.PaymentResult.SetError(String.Concat(
                    "Ödeme işlemi banka tarafından reddedildi! : " , 
                    posnetObj.GetResponseCode() , " " , posnetObj.GetResponseText()));
                PaymentPageVM.PaymentResult.Result.PayRefNo = posnetObj.GetPoint();  
            }

            PaymentPageVM.PaymentResult.AddMessage("<span style=\"display:block\">");

            if (posnetObj.GetResponseCode().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Error Code : " , posnetObj.GetResponseCode() , ControlChars.NewLine));
            }

            if (posnetObj.GetResponseText().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Error Message : " , posnetObj.GetResponseText() , ControlChars.NewLine));
            }

            if (posnetObj.GetHostlogkey().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Hostlogkey : " , posnetObj.GetHostlogkey() , ControlChars.NewLine));
            }

            if (posnetObj.GetAuthcode().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Authcode : " , posnetObj.GetAuthcode() , ControlChars.NewLine));
            }

            if (posnetObj.GetPoint().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Point : " , posnetObj.GetPoint() , ControlChars.NewLine));
            }

            if (posnetObj.GetPointAmount().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Point Amount : " , posnetObj.GetPointAmount() , ControlChars.NewLine));
            }

            if (posnetObj.GetTotalPoint().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Total Point : " , posnetObj.GetTotalPoint() , ControlChars.NewLine));
            }

            if (posnetObj.GetTotalPointAmount().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Total Point Amount : " , posnetObj.GetTotalPointAmount() , ControlChars.NewLine));
            }

            if (posnetObj.GetInstalmentNumber().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Instalment Number : " , posnetObj.GetInstalmentNumber() , ControlChars.NewLine));
            }

            if (posnetObj.GetInstalmentAmount().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Instalment Amount : " , posnetObj.GetInstalmentAmount() , ControlChars.NewLine));
            }

            if (posnetObj.GetVFTAmount().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("VFT Amount : " , posnetObj.GetVFTAmount() , ControlChars.NewLine));
            }

            if (posnetObj.GetVFTRate().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("VFT Rate : " , posnetObj.GetVFTRate() , ControlChars.NewLine));
            }

            if (posnetObj.GetVFTDayCount().Length > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("Day Count : " , posnetObj.GetVFTDayCount() , ControlChars.NewLine));
            }

            if (posnetObj.GetCampMessageCount() > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("KOI Campaigns :" , ControlChars.NewLine));
                for (int i = 1; i <= posnetObj.GetCampMessageCount(); i++)
                {
                    PaymentPageVM.PaymentResult.AddMessage(String.Concat(i , ". [" , posnetObj.GetCampCode(i) , "] -- { " ,
                        posnetObj.GetCampMessage(i) , " }" + ControlChars.NewLine));
                }
            }

            if (posnetObj.GetWorldCampMessageCount() > 0)
            {
                PaymentPageVM.PaymentResult.AddMessage(String.Concat("World Campaigns Messages :" , ControlChars.NewLine));

                for (int i = 1; i <= posnetObj.GetWorldCampMessageCount(); i++)
                {
                    PaymentPageVM.PaymentResult.AddMessage(String.Concat(i, ". { " , posnetObj.GetWorldCampMessage(i) , " }" + ControlChars.NewLine));
                }
            }

            PaymentPageVM.PaymentResult.AddMessage("</span>");

        }

    }


}