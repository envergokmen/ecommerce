﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;
using System.IO;
using System.Xml;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Ecom.Web.Helpers;
using Ecom.ViewModels.Payments;

namespace Ecom.Services.CheckoutServices
{
    public interface IPaymentChargeService
    {
        CheckoutVM MakePayment();
    }

    public class PaymentChargeServiceBase
    {
        protected CheckoutVM PaymentPageVM;
        protected OrderService orderService;
        protected IHttpContextAccessor contextAccessor;
        protected CheckoutStateHelper checkState;
        protected HttpContext HttpContext;

        //protected StandartOperationResult Validate()
        //{
        //    StandartOperationResult result = new StandartOperationResult();
        //    result.SetSuccessAndClearError();

        //    if (PaymentPageVM == null)
        //    {
        //        checkOutState = orderService.GetCheckOutState(true);
        //        checkOutState.PaymentResult = new OperationResult<PaymentHistoryViewModel>();
        //        checkOutState.PaymentResult.ClearErrors();

        //        result.AddError("SiparisOturumZamanAsimi");
        //    }
        //    else if (PaymentPageVM == null || PaymentPageVM.WebPosInstallment == null || PaymentPageVM.WebPosInstallment.WebPos == null || PaymentPageVM.WebPosInstallment.WebPos.Payment == null)
        //    {
        //        result.SetError(Rsc.Site.OdemeTuruBulunamadi);
        //    }
        //    else if (PaymentPageVM.CreatedOrder == null)
        //    {
        //        result.SetError(Rsc.Site.OdemeIcinSiparisBulunamadi);
        //    }
        //    else if (PaymentPageVM.CreditCart == null)
        //    {
        //        result.SetError(Rsc.Site.KartNumaranizEksik);
        //    }

        //    return result;
        //}

        protected void InitializePaymentResult()
        {
            if (PaymentPageVM != null)
            {
                //PaymentPageVM.IsResultWaiting = false;
                PaymentPageVM.PaymentResult = new OperationResult<PaymentHistoryViewModel>();

                if (PaymentPageVM.PaymentResult.Result == null) PaymentPageVM.PaymentResult.Result = new PaymentHistoryViewModel();

                PaymentPageVM.PaymentResult.Result = new PaymentHistoryViewModel();
                PaymentPageVM.PaymentResult.Result.PaymentComplateStatus = PaymentComplateStaus.Tamamlanmadi;
                PaymentPageVM.PaymentResult.Result.PaymentLogType = PaymentLogType.SiparisHataLogu;
                PaymentPageVM.PaymentResult.Result.Amount = PaymentPageVM.CreatedOrder.TotalAmount;
                PaymentPageVM.PaymentResult.Result.CardNumber = Utils.MaskCreditCard(PaymentPageVM.CreditCard.CardNumber);
                PaymentPageVM.PaymentResult.Result.CardOwner = PaymentPageVM.CreditCard.CardOwner;
                PaymentPageVM.PaymentResult.Result.PaymentType = PaymentType.KrediKarti;
                PaymentPageVM.PaymentResult.Result.PriceCurrencyID = PaymentPageVM.CurrencyId;
                PaymentPageVM.PaymentResult.Result.TrackCode = PaymentPageVM.CreatedOrder.TrackCode;
                PaymentPageVM.PaymentResult.Result.WebPosInstallmentID = PaymentPageVM.SelectedWebPosInstallment?.ID ?? 0;
                PaymentPageVM.PaymentResult.Result.WebPosID = PaymentPageVM.SelectedWebPosInstallment?.WebPosID ?? 0;
                PaymentPageVM.PaymentResult.Result.UserID = PaymentPageVM.UserId;
                PaymentPageVM.PaymentResult.Result.UserArgent = contextAccessor != null && contextAccessor.HttpContext != null && contextAccessor.HttpContext.Request != null ? contextAccessor.HttpContext.Request.Headers["User-Agent"].ToString() : "";
                PaymentPageVM.PaymentResult.Result.IP = PaymentPageVM.IP;

                if (PaymentPageVM.CreatedOrderId > 0)
                    PaymentPageVM.PaymentResult.Result.OrderID = PaymentPageVM.CreatedOrderId;

                PaymentPageVM.PaymentResult.SetError(String.Concat("An Error occured on payment ( ", PaymentPageVM.SelectedWebPosInstallment.PosName, ")"));

                if (PaymentPageVM.PaymentResult == null)
                    PaymentPageVM.PaymentResult = new OperationResult<PaymentHistoryViewModel>();

            }
        }

        protected string ConGarantiPara(string st)
        {
            string gelen = st;
            gelen = gelen.Replace(",", "");
            gelen = gelen.Replace(".", "");
            return gelen;
        }

        public string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;
            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }
            return s.ToString();
        }

        public string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            return GetHexaDecimal(inputbytes);
        }
    }

    public class PaymentChargeServiceFactory
    {
        OrderService orderService;
        CheckoutVM PaymentPageVM;
        CheckoutStateHelper checkState;
        IHttpContextAccessor contextAccessor;

        public PaymentChargeServiceFactory(CheckoutVM PaymentPageVM, OrderService orderService, CheckoutStateHelper checkState, IHttpContextAccessor contextAccessor)
        {
            this.orderService = orderService;
            this.PaymentPageVM = PaymentPageVM;
            this.checkState = checkState;
            this.contextAccessor = contextAccessor;
        }

        public IPaymentChargeService GetChargeService(SanalPosOdemeTuru posType)
        {
            IPaymentChargeService chargeService = null;

            switch (posType)
            {
                //case SanalPosOdemeTuru.EstStandart:
                //    chargeService = new EstPaymentChargeService(PaymentPageVM, checkOutState, orderService);
                //    break;
                //case SanalPosOdemeTuru.Est3DPay:
                //    break;
                //case SanalPosOdemeTuru.Est3DSecure:
                //    chargeService = new Est3DModelPaymentChargeService(PaymentPageVM, checkOutState, orderService);
                //    break;

                case SanalPosOdemeTuru.GarantiStandart4:
                    chargeService = new GarantiPaymentChargeService(PaymentPageVM, orderService, contextAccessor, checkState);
                    break;

                //case SanalPosOdemeTuru.Garanti3DPay:
                //    break;
                //case SanalPosOdemeTuru.Garanti3DModel:
                //    break;
                case SanalPosOdemeTuru.Garanti3DSecure:
                    chargeService = new Garanti3DSecurePaymentChargeService(PaymentPageVM, orderService, contextAccessor, checkState);
                    break;
                case SanalPosOdemeTuru.YapiKrediStandart:
                    chargeService = new YkbPaymentChargeService(PaymentPageVM, orderService, contextAccessor, checkState);
                    break;
                //case SanalPosOdemeTuru.YapiKredi3DSecure:
                //    chargeService = new Ykb3DSecurePaymentChargeService(PaymentPageVM, checkOutState, orderService);
                //    break;
                case SanalPosOdemeTuru.YapiKredi3DPay:
                    break;
                case SanalPosOdemeTuru.BkmExpress:
                    break;
                case SanalPosOdemeTuru.InterVPos:
                    break;
                case SanalPosOdemeTuru.PayPal:
                    break;
                case SanalPosOdemeTuru.KuveytTurk:
                    break;
                case SanalPosOdemeTuru.KuveytTurk3DSecure:
                    break;
                default:

                    break;
            }

            return chargeService;
        }
    }


}