﻿using System;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.IO;
using System.Xml;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Ecom.Web.Helpers;
using Ecom.Core;

namespace Ecom.Services.CheckoutServices
{

    public class Est3DModelPaymentChargeService : PaymentChargeServiceBase, IPaymentChargeService
    {
        public Est3DModelPaymentChargeService(
            CheckoutVM PaymentPageVM,
            OrderService orderService,
            IHttpContextAccessor contextAccessor,
            CheckoutStateHelper checkState
            )
        {
            this.orderService = orderService;
            this.PaymentPageVM = PaymentPageVM;
            this.contextAccessor = contextAccessor;
            this.checkState = checkState;
            this.HttpContext = contextAccessor.HttpContext;
        }


        public CheckoutVM MakePayment()
        {
            //if (!Validate().Status)
            //{

            //    if (checkOutState.PaymentResult == null) checkOutState.PaymentResult = new OperationResult<PaymentHistoryViewModel>();
            //    checkOutState.PaymentResult.AddError(Validate().ResultDescription);
            //    return checkOutState;
            //}

            InitializePaymentResult();

            try
            {

                bool isHashParamsValid = true;
                string hashParamError = "";

                String[] odemeparametreleri = new String[] { "AuthCode", "Response", "HostRefNum", "ProcReturnCode", "TransId", "ErrMsg" };
                System.Collections.IEnumerator e = HttpContext.Request.Form.GetEnumerator();
                while (e.MoveNext())
                {
                    String xkey = (String)e.Current;
                    String xval = HttpContext.Request.Form[xkey];
                    bool ok = true;

                    for (int i = 0; i < odemeparametreleri.Length; i++)
                    {
                        if (xkey.Equals(odemeparametreleri[i]))
                        {
                            ok = false;
                            break;
                        }
                    }

                    if (ok)
                    {

                    }
                }

                string allParameters = "";
                foreach (var key in HttpContext.Request.Form)
                {
                    allParameters += " " + key.Key + ":" + key.Value;
                }

                String MDSonucu = HttpContext.Request.Form["mdStatus"]; // 3d işlemin sonucu
                String IslemSonucu = HttpContext.Request.Form["Response"];

                String ErrorMsg = HttpContext.Request.Form["mdErrorMsg"];
                String ErrorCode = HttpContext.Request.Form["mdErrorCode"];


                String hashparams = HttpContext.Request.Form["HASHPARAMS"];
                String hashparamsval = HttpContext.Request.Form["HASHPARAMSVAL"];
                String storekey = PaymentPageVM.CreatedOrder.WebPosInstallment.SecureKey3D;

                double ToplamTahsilAmount = Convert.ToDouble(PaymentPageVM.CreatedOrder.TotalAmount);

                if (1 == 2)
                {
                    ToplamTahsilAmount = 1;

                }

                String paramsval = "";

                if (hashparams == null)
                {
                    isHashParamsValid = false;
                    hashParamError = "Security warnning. Numeric sign is not valid (1)";
                    PaymentPageVM.PaymentResult.SetError(hashParamError);
                }

                int index1 = 0, index2 = 0;
                // hash hesaplamada kullanılacak değerler ayrıştırılıp değerleri birleştiriliyor.

                if (hashparams != null)
                {
                    do
                    {
                        index2 = hashparams.IndexOf(":", index1);
                        String val = !String.IsNullOrWhiteSpace(HttpContext.Request.Form[hashparams.Substring(index1, index2 - index1)]) ? HttpContext.Request.Form[hashparams.Substring(index1, index2 - index1)].ToString() : "";
                        paramsval += val;
                        index1 = index2 + 1;
                    }
                    while (index1 < hashparams.Length);
                }

                String hashval = paramsval + storekey; //elde edilecek hash değeri için paramsval e store key ekleniyor. (işyeri anahtarı)
                String hashparam = HttpContext.Request.Form["HASH"];

                System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashval);
                byte[] inputbytes = sha.ComputeHash(hashbytes);

                String hash = Convert.ToBase64String(inputbytes); //Güvenlik ve kontrol amaçlı oluşturulan hash

                if (!paramsval.Equals(hashparamsval) || !hash.Equals(hashparam)) //oluşturulan hash ile gelen hash ve hash parametreleri değerleri ile ayrıştırılıp edilen edilen aynı olmalı.
                {
                    isHashParamsValid = false;
                    hashParamError = "Security Warning. Numeric sign is not valid (2)";
                    PaymentPageVM.PaymentResult.SetError(hashParamError);

                }

                // Ödeme için gerekli parametreler
                String nameval = PaymentPageVM.CreatedOrder.WebPosInstallment.ApiUser;              //İşyeri kullanıcı adı
                String passwordval = PaymentPageVM.CreatedOrder.WebPosInstallment.ApiPassword;        //İşyeri şifresi
                String clientidval = HttpContext.Request.Form["clientid"]; // İşyeri numarası  
                String modeval = "P";                   //P olursa gerçek işlem, T olursa test işlemi yapar.

                String typeval = "PreAuth";           //Auth PreAuth PostAuth Credit Void olabilir.

                if (!String.IsNullOrWhiteSpace(PaymentPageVM.CreatedOrder.WebPosInstallment.ProvType))
                {
                    typeval = PaymentPageVM.CreatedOrder.WebPosInstallment.ProvType;
                }

                if (PaymentPageVM.CreatedOrder.PaymentType == Core.PaymentType.DebitCard)
                {
                    typeval = "Auth";
                }

                //test işlemi için
                else if (1 == 2)
                {
                    // modeval = "T"; //P olursa gerçek işlem, T olursa test işlemi yapar.
                }

                String expiresval = HttpContext.Request.Form["Ecom_Payment_Card_ExpDate_Month"] + "/" + HttpContext.Request.Form["Ecom_Payment_Card_ExpDate_Year"]; //Kredi Kartı son kullanım tarihi mm/yy formatından olmalı
                String cv2val = HttpContext.Request.Form["cv2"];    //Güvenlik Kodu
                String totalval = HttpContext.Request.Form["amount"]; //Tutar
                String numberval = HttpContext.Request.Form["md"];    //Kart numarası olarak 3d sonucu dönem md parametresi kullanılır.
                String taksitval = (PaymentPageVM.CreatedOrder.InstallmentQuantity > 1 && PaymentPageVM.CreatedOrder.PaymentType != Core.PaymentType.DebitCard) ? PaymentPageVM.CreatedOrder.InstallmentQuantity.ToString() : ""; //Taksit sayısı peşin satışlar da boş olarak gönderilmelidir. 
                String currencyval = null;           //ytl için

                switch (PaymentPageVM.CreatedOrder.CurrencyCode)
                {
                    case "TRY": currencyval = "949"; break;
                    case "TL": currencyval = "949"; break;
                    case "EUR": currencyval = "978"; break;
                    case "GBP": currencyval = "826"; break;
                    case "USD": currencyval = "998"; break;
                    default:
                        break;
                }

                String orderidval = PaymentPageVM.CreatedOrder.TrackCode;  //Sipariş numarası
                String mdStatus = HttpContext.Request.Form["mdStatus"]; // 3d işlemin sonucu 

                string AuthCode = "";
                string xResponse = "";
                string HostRefNum = "";
                string ProcReturnCode = "";
                string TransId = "";
                string ErrMsg = "";

                string posResponse = "";

                if (isHashParamsValid && mdStatus.Equals("1") || mdStatus.Equals("2") || mdStatus.Equals("3") || mdStatus.Equals("4"))
                {
                    for (int i = 0; i < odemeparametreleri.Length; i++)
                    {
                        String paramname = odemeparametreleri[i];
                        String paramval = HttpContext.Request.Form[paramname];
                    }

                    String cardholderpresentcodeval = "13";
                    String payersecuritylevelval = HttpContext.Request.Form["eci"];
                    String payertxnidval = HttpContext.Request.Form["xid"];
                    String payerauthenticationcodeval = HttpContext.Request.Form["cavv"];
                    String ipaddressval = PaymentPageVM.IP;
                    String emailval = "";
                    String useridval = "";


                    if (PaymentPageVM.User != null)
                    {
                        if (Utils.isEmail(PaymentPageVM.User.UserName))
                        {
                            emailval = PaymentPageVM.User.UserName;
                        }

                        useridval = (PaymentPageVM.CreditCard != null) ? PaymentPageVM.CreditCard.CardOwner.ToString() : PaymentPageVM.User.Name.ToString();
                    }

                    String groupidval = "";
                    String transidval = "";

                    //Fatura Bilgileri
                    String billnameval = "";      //Fatur İsmi
                    String billstreet1val = "";   //Fatura adres 1
                    String billstreet2val = "";   //Fatura adres 2
                    String billstreet3val = "";   //Fatura adres 3
                    String billcityval = "";      //Fatura şehir
                    String billstateprovval = ""; //Fatura eyalet
                    String billpostalcodeval = ""; //Fatura posta kodu

                    //Teslimat Bilgileri
                    String shipnameval = "";      //isim
                    String shipstreet1val = "";   //adres 1
                    String shipstreet2val = "";   //adres 2
                    String shipstreet3val = "";   //adres 3
                    String shipcityval = "";      //şehir
                    String shipstateprovval = ""; //eyalet
                    String shippostalcodeval = "";//posta kodu 
                    String extraval = "";

                    //Ödeme için gerekli xml yapısı oluşturuluyor 

                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                    System.Xml.XmlDeclaration dec =
                        doc.CreateXmlDeclaration("1.0", "ISO-8859-9", "yes");

                    doc.AppendChild(dec);
                    System.Xml.XmlElement cc5Request = doc.CreateElement("CC5Request");
                    doc.AppendChild(cc5Request);

                    System.Xml.XmlElement name = doc.CreateElement("Name");
                    name.AppendChild(doc.CreateTextNode(nameval));
                    cc5Request.AppendChild(name);

                    System.Xml.XmlElement password = doc.CreateElement("Password");
                    password.AppendChild(doc.CreateTextNode(passwordval));
                    cc5Request.AppendChild(password);

                    System.Xml.XmlElement clientid = doc.CreateElement("ClientId");
                    clientid.AppendChild(doc.CreateTextNode(clientidval));
                    cc5Request.AppendChild(clientid);

                    System.Xml.XmlElement ipaddress = doc.CreateElement("IPAddress");
                    ipaddress.AppendChild(doc.CreateTextNode(ipaddressval));
                    cc5Request.AppendChild(ipaddress);

                    System.Xml.XmlElement email = doc.CreateElement("Email");
                    email.AppendChild(doc.CreateTextNode(emailval));
                    cc5Request.AppendChild(email);

                    System.Xml.XmlElement mode = doc.CreateElement("Mode");
                    mode.AppendChild(doc.CreateTextNode(modeval));
                    cc5Request.AppendChild(mode);

                    System.Xml.XmlElement orderid = doc.CreateElement("OrderId");
                    orderid.AppendChild(doc.CreateTextNode(orderidval));
                    cc5Request.AppendChild(orderid);

                    System.Xml.XmlElement groupid = doc.CreateElement("GroupId");
                    groupid.AppendChild(doc.CreateTextNode(groupidval));
                    cc5Request.AppendChild(groupid);

                    System.Xml.XmlElement transid = doc.CreateElement("TransId");
                    transid.AppendChild(doc.CreateTextNode(transidval));
                    cc5Request.AppendChild(transid);

                    System.Xml.XmlElement userid = doc.CreateElement("UserId");
                    userid.AppendChild(doc.CreateTextNode(useridval));
                    cc5Request.AppendChild(userid);

                    System.Xml.XmlElement type = doc.CreateElement("Type");
                    type.AppendChild(doc.CreateTextNode(typeval));
                    cc5Request.AppendChild(type);

                    System.Xml.XmlElement number = doc.CreateElement("Number");
                    number.AppendChild(doc.CreateTextNode(numberval));
                    cc5Request.AppendChild(number);

                    System.Xml.XmlElement expires = doc.CreateElement("Expires");
                    expires.AppendChild(doc.CreateTextNode(expiresval));
                    cc5Request.AppendChild(expires);

                    System.Xml.XmlElement cvv2val = doc.CreateElement("Cvv2Val");
                    cvv2val.AppendChild(doc.CreateTextNode(cv2val));
                    cc5Request.AppendChild(cvv2val);

                    System.Xml.XmlElement total = doc.CreateElement("Total");
                    total.AppendChild(doc.CreateTextNode(totalval));
                    cc5Request.AppendChild(total);

                    System.Xml.XmlElement currency = doc.CreateElement("Currency");
                    currency.AppendChild(doc.CreateTextNode(currencyval));
                    cc5Request.AppendChild(currency);

                    System.Xml.XmlElement taksit = doc.CreateElement("Taksit");
                    taksit.AppendChild(doc.CreateTextNode(taksitval));
                    cc5Request.AppendChild(taksit);

                    System.Xml.XmlElement payertxnid = doc.CreateElement("PayerTxnId");
                    payertxnid.AppendChild(doc.CreateTextNode(payertxnidval));
                    cc5Request.AppendChild(payertxnid);

                    System.Xml.XmlElement payersecuritylevel = doc.CreateElement("PayerSecurityLevel");
                    payersecuritylevel.AppendChild(doc.CreateTextNode(payersecuritylevelval));
                    cc5Request.AppendChild(payersecuritylevel);

                    System.Xml.XmlElement payerauthenticationcode = doc.CreateElement("PayerAuthenticationCode");
                    payerauthenticationcode.AppendChild(doc.CreateTextNode(payerauthenticationcodeval));
                    cc5Request.AppendChild(payerauthenticationcode);

                    System.Xml.XmlElement cardholderpresentcode = doc.CreateElement("CardholderPresentCode");
                    cardholderpresentcode.AppendChild(doc.CreateTextNode(cardholderpresentcodeval));
                    cc5Request.AppendChild(cardholderpresentcode);

                    System.Xml.XmlElement billto = doc.CreateElement("BillTo");
                    cc5Request.AppendChild(billto);

                    System.Xml.XmlElement billname = doc.CreateElement("Name");
                    billname.AppendChild(doc.CreateTextNode(billnameval));
                    billto.AppendChild(billname);

                    System.Xml.XmlElement billstreet1 = doc.CreateElement("Street1");
                    billstreet1.AppendChild(doc.CreateTextNode(billstreet1val));
                    billto.AppendChild(billstreet1);

                    System.Xml.XmlElement billstreet2 = doc.CreateElement("Street2");
                    billstreet2.AppendChild(doc.CreateTextNode(billstreet2val));
                    billto.AppendChild(billstreet2);

                    System.Xml.XmlElement billstreet3 = doc.CreateElement("Street3");
                    billstreet3.AppendChild(doc.CreateTextNode(billstreet3val));
                    billto.AppendChild(billstreet3);

                    System.Xml.XmlElement billcity = doc.CreateElement("City");
                    billcity.AppendChild(doc.CreateTextNode(billcityval));
                    billto.AppendChild(billcity);

                    System.Xml.XmlElement billstateprov = doc.CreateElement("StateProv");
                    billstateprov.AppendChild(doc.CreateTextNode(billstateprovval));
                    billto.AppendChild(billstateprov);

                    System.Xml.XmlElement billpostalcode = doc.CreateElement("PostalCode");
                    billpostalcode.AppendChild(doc.CreateTextNode(billpostalcodeval));
                    billto.AppendChild(billpostalcode);

                    System.Xml.XmlElement shipto = doc.CreateElement("ShipTo");
                    cc5Request.AppendChild(shipto);

                    System.Xml.XmlElement shipname = doc.CreateElement("Name");
                    shipname.AppendChild(doc.CreateTextNode(shipnameval));
                    shipto.AppendChild(shipname);

                    System.Xml.XmlElement shipstreet1 = doc.CreateElement("Street1");
                    shipstreet1.AppendChild(doc.CreateTextNode(shipstreet1val));
                    shipto.AppendChild(shipstreet1);

                    System.Xml.XmlElement shipstreet2 = doc.CreateElement("Street2");
                    shipstreet2.AppendChild(doc.CreateTextNode(shipstreet2val));
                    shipto.AppendChild(shipstreet2);

                    System.Xml.XmlElement shipstreet3 = doc.CreateElement("Street3");
                    shipstreet3.AppendChild(doc.CreateTextNode(shipstreet3val));
                    shipto.AppendChild(shipstreet3);

                    System.Xml.XmlElement shipcity = doc.CreateElement("City");
                    shipcity.AppendChild(doc.CreateTextNode(shipcityval));
                    shipto.AppendChild(shipcity);

                    System.Xml.XmlElement shipstateprov = doc.CreateElement("StateProv");
                    shipstateprov.AppendChild(doc.CreateTextNode(shipstateprovval));
                    shipto.AppendChild(shipstateprov);

                    System.Xml.XmlElement shippostalcode = doc.CreateElement("PostalCode");
                    shippostalcode.AppendChild(doc.CreateTextNode(shippostalcodeval));
                    shipto.AppendChild(shippostalcode);

                    System.Xml.XmlElement extra = doc.CreateElement("Extra");
                    extra.AppendChild(doc.CreateTextNode(extraval));
                    cc5Request.AppendChild(extra);

                    String xmlval = doc.OuterXml;     //Oluşturulan xml string olarak alınıyor.

                    // Ödeme için bağlantı kuruluyor. ve post ediliyor
                    String url = (!String.IsNullOrWhiteSpace(PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl)) ? PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl : PaymentPageVM.CreatedOrder.WebPosInstallment.PostUrl;

                    System.Net.HttpWebResponse resp = null;

                    try
                    {
                        System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);

                        string postdata = "DATA=" + xmlval.ToString();
                        byte[] postdatabytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(postdata);
                        request.Method = "POST";
                        request.ContentType = "application/x-www-form-urlencoded";
                        request.ContentLength = postdatabytes.Length;
                        System.IO.Stream requeststream = request.GetRequestStream();
                        requeststream.Write(postdatabytes, 0, postdatabytes.Length);
                        requeststream.Close();

                        resp = (System.Net.HttpWebResponse)request.GetResponse();
                        System.IO.StreamReader responsereader = new System.IO.StreamReader(resp.GetResponseStream(), System.Text.Encoding.GetEncoding("ISO-8859-9"));


                        String gelenXml = responsereader.ReadToEnd(); //Gelen xml string olarak alındı.
                        posResponse = gelenXml;

                        System.Diagnostics.Debug.WriteLine(gelenXml);


                        System.Xml.XmlDocument gelen = new System.Xml.XmlDocument();
                        gelen.LoadXml(gelenXml);    //string xml dökumanına çevrildi.

                        System.Xml.XmlNodeList list = gelen.GetElementsByTagName("Response");

                        String xmlResponse = list[0].InnerText;
                        list = gelen.GetElementsByTagName("AuthCode");
                        String xmlAuthCode = list[0].InnerText;
                        list = gelen.GetElementsByTagName("HostRefNum");
                        String xmlHostRefNum = list[0].InnerText;
                        list = gelen.GetElementsByTagName("ProcReturnCode");
                        String xmlProcReturnCode = list[0].InnerText;
                        list = gelen.GetElementsByTagName("TransId");
                        String xmlTransId = list[0].InnerText;
                        list = gelen.GetElementsByTagName("ErrMsg");
                        String xmlErrMsg = list[0].InnerText;

                        AuthCode = xmlAuthCode;
                        xResponse = xmlResponse;
                        HostRefNum = xmlHostRefNum;
                        ProcReturnCode = xmlProcReturnCode;
                        TransId = xmlTransId;
                        ErrMsg = xmlErrMsg;

                        if ("Approved".Equals(xmlResponse) && !xResponse.Contains("Declined"))
                        {
                            PaymentPageVM.PaymentResult.Result.PaymentComplateStatus = PaymentComplateStaus.Tamamlandi;
                            PaymentPageVM.PaymentResult.Result.PaymentLogType = PaymentLogType.SiparisArti;
                            PaymentPageVM.PaymentResult.Result.PaymentDate = DateTime.Now;
                            PaymentPageVM.PaymentResult.SetSuccessAndClearError("Payment is succeed");

                        }
                        else
                        {
                            PaymentPageVM.PaymentResult.AddError(posResponse);
                        }

                    }
                    catch (Exception ex)
                    {
                        PaymentPageVM.PaymentResult.AddError(String.Concat(" EST 3D request error : ", posResponse, " ", ex.Message, " ", ex.InnerException));

                    }
                    finally
                    {
                        if (resp != null)
                            resp.Close();
                    }


                }
                else
                {
                    PaymentPageVM.PaymentResult.AddError("EST 3D operation has failed");
                    PaymentPageVM.PaymentResult.AddError(String.Concat("ErrorMsg:", ErrorMsg, " ErrorCode:", ErrorCode, " MdStatus : ", mdStatus));

                    if (isHashParamsValid)
                    {
                        PaymentPageVM.PaymentResult.AddError(hashParamError);
                    }

                }


                PaymentPageVM.PaymentResult.AddMessage(String.Concat(ErrMsg, " ", HttpContext.Request.Form["oid"]));
               
                string amnt = HttpContext.Request.Form["amount"];

                if (!String.IsNullOrWhiteSpace(amnt) && amnt.IndexOf('.') != -1 && amnt.IndexOf(',') == -1)
                {
                    amnt = HttpContext.Request.Form["amount"].ToString().Replace(".", ",");
                }

                PaymentPageVM.PaymentResult.Result.Amount = Convert.ToDecimal(amnt);
                PaymentPageVM.PaymentResult.Result.PaymentDate = DateTime.Now;

                if (string.IsNullOrWhiteSpace(PaymentPageVM.PaymentResult.Result.CardNumber))
                {
                    PaymentPageVM.PaymentResult.Result.CardNumber = Utils.MaskCreditCard(PaymentPageVM.CreditCard.CardNumber);
                }

                PaymentPageVM.PaymentResult.Result.ErrMsg = ErrMsg;

                if (!String.IsNullOrWhiteSpace(HttpContext.Request.Form["EXTRA.HOSTMSG"]))
                {
                    PaymentPageVM.PaymentResult.Result.ExtraHostMsg = HttpContext.Request.Form["EXTRA.HOSTMSG"];
                    PaymentPageVM.PaymentResult.AddMessage(HttpContext.Request.Form["EXTRA.HOSTMSG"]);
                }

                PaymentPageVM.PaymentResult.Result.Retval = xResponse;
                PaymentPageVM.PaymentResult.Result.PayCode = AuthCode;
                PaymentPageVM.PaymentResult.Result.PayRefNo = TransId;

                if (!string.IsNullOrWhiteSpace(mdStatus))
                {
                    PaymentPageVM.PaymentResult.Result.ErrMsg += " mdStatus:" + mdStatus;
                }

                PaymentPageVM.PaymentResult.Result.ErrMsg += " isHashParamsValid:" + isHashParamsValid;

                if (!string.IsNullOrWhiteSpace(hashparam))
                {
                    PaymentPageVM.PaymentResult.Result.ErrMsg += " hashparam:" + hashparam;
                }

                if (!string.IsNullOrWhiteSpace(ErrMsg))
                {
                    PaymentPageVM.PaymentResult.Result.ErrMsg += " xmlErrMsg:" + ErrMsg;
                }

                if (!String.IsNullOrWhiteSpace(HostRefNum))
                {
                    PaymentPageVM.PaymentResult.Result.ExtraHostMsg += HostRefNum;
                }

                if (!String.IsNullOrWhiteSpace(ProcReturnCode))
                {
                    PaymentPageVM.PaymentResult.Result.ErrMsg += " ProcReturnCode:" + ProcReturnCode;
                }


            }
            catch (Exception ex)
            {
                PaymentPageVM.PaymentResult.SetError(String.Concat(" An error has occured while requesting: ", ex.Message, " ", ex.InnerException));
            }

            if (PaymentPageVM != null && PaymentPageVM.PaymentResult != null && PaymentPageVM.PaymentResult.Result != null)
            {
                PaymentPageVM.PaymentResult.Result.Description = PaymentPageVM.PaymentResult.GetErrors();
                orderService.AddPaymentHistoryLog(PaymentPageVM.PaymentResult.Result);
            }

            checkState.SetPaymentResult(PaymentPageVM.PaymentResult);

            return PaymentPageVM;

        }
    }


}