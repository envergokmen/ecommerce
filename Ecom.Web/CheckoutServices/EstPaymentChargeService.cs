﻿using System;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.IO;
using System.Xml;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Ecom.Web.Helpers;
using Ecom.Core;

namespace Ecom.Services.CheckoutServices
{

    public class EstPaymentChargeService : PaymentChargeServiceBase, IPaymentChargeService
    {
        public EstPaymentChargeService(
            CheckoutVM PaymentPageVM,
            OrderService orderService,
            IHttpContextAccessor contextAccessor,
            CheckoutStateHelper checkState
            )
        {
            this.orderService = orderService;
            this.PaymentPageVM = PaymentPageVM;
            this.contextAccessor = contextAccessor;
            this.checkState = checkState;
            this.HttpContext = contextAccessor.HttpContext;
        }

        public CheckoutVM MakePayment()
        {
            //if (!Validate().Status)
            //{
            //    if (checkOutState.PaymentResult == null) checkOutState.PaymentResult = new OperationResult<PaymentHistoryViewModel>();
            //    checkOutState.PaymentResult.AddError(Validate().ResultDescription);
            //    return checkOutState;
            //}

            InitializePaymentResult();

            try
            {
                ePayment.cc5payment mycc5pay = new ePayment.cc5payment();

                double cekilen = Convert.ToDouble(PaymentPageVM.CreatedOrder.TotalAmount);

                // Verileri göndereceğimiz adres
                mycc5pay.host = PaymentPageVM.CreatedOrder.WebPosInstallment.PostUrl;

                if (!String.IsNullOrWhiteSpace(PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl))
                {
                    mycc5pay.host = PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl;
                }

                // Bankadan verilen kullanıcı adı
                mycc5pay.name = PaymentPageVM.CreatedOrder.WebPosInstallment.ApiUser;

                // Bankadan verilen parola
                mycc5pay.password = PaymentPageVM.CreatedOrder.WebPosInstallment.ApiPassword;

                // Bankadan verilen mağaza id
                mycc5pay.clientid = PaymentPageVM.CreatedOrder.WebPosInstallment.MagazaNo;

                mycc5pay.orderresult = 0;
                // 0 Gerçek işlem, 1 Test işlemi

                mycc5pay.cardnumber = PaymentPageVM.CreditCard.CardNumber;
                // Kredi kartı numarası

                mycc5pay.expmonth = PaymentPageVM.CreditCard.ExpiresMonth;
                // Kredi kartı son kullanım ay

                mycc5pay.expyear = PaymentPageVM.CreditCard.ExpiresYear;
                // Kredi kartı son kullanım yıl

                mycc5pay.cv2 = PaymentPageVM.CreditCard.CardCvc.ToString();
                // Kredi kartı CVC numarası

                mycc5pay.currency = null;
                // TL için 949
                 

                switch (PaymentPageVM.CreatedOrder.CurrencyCode)
                {
                    case "TRY": mycc5pay.currency = "949"; break;
                    case "TL": mycc5pay.currency = "949"; break;
                    case "EUR": mycc5pay.currency = "978"; break;
                    case "GBP": mycc5pay.currency = "826"; break;
                    case "USD": mycc5pay.currency = "998"; break;
                    default:
                        break;
                }

                mycc5pay.chargetype = "PreAuth";// 'PreAuth = Provizyon - PostAuth = İşyeri Onayı - Auth = Satış - Void = İptal

                if (!String.IsNullOrWhiteSpace(PaymentPageVM.CreatedOrder.WebPosInstallment.ProvType))
                {
                    mycc5pay.chargetype = PaymentPageVM.CreatedOrder.WebPosInstallment.ProvType;
                }

                mycc5pay.ip = PaymentPageVM.IP;
                // IP adresi

                mycc5pay.bname = PaymentPageVM.CreatedOrder.Name;

                if (PaymentPageVM.CreatedOrder.Name == null || PaymentPageVM.CreatedOrder.Name == "") mycc5pay.bname = "Noname";

                //üye adı
                string email = null;

                if (PaymentPageVM.CreditCard.CardOwner != null)
                {
                    mycc5pay.userid = PaymentPageVM.CreditCard.CardOwner;
                }

                email = PaymentPageVM.CreatedOrder.UserName;

                if (String.IsNullOrWhiteSpace(email) && PaymentPageVM.CheckoutAsGuest && PaymentPageVM.SelectedShippingAddress!=null && Utils.isEmail(PaymentPageVM.SelectedShippingAddress.Email))
                {
                    email = PaymentPageVM.SelectedShippingAddress.Email;
                }

                if (String.IsNullOrWhiteSpace(email)) email = $"info@{PaymentPageVM.UrlAuthority}";

                mycc5pay.baddr2 = email;
                 
                if (PaymentPageVM.CreatedOrder != null && PaymentPageVM.CreatedOrder.InstallmentQuantity != null && PaymentPageVM.CreatedOrder.InstallmentQuantity > 1)
                {
                    mycc5pay.taksit = PaymentPageVM.CreatedOrder.InstallmentQuantity.ToString();
                }

                mycc5pay.oid = PaymentPageVM.CreatedOrder.TrackCode;

                mycc5pay.subtotal = cekilen.ToString();
                // Çekilecek tutar

                string bankaSonuc = ""; 

                bankaSonuc = mycc5pay.processorder();

                // Fonksiyonumuzu çağırıyoruz
                string bankaHata = mycc5pay.errmsg;
                // Geri dönen hata mesajı
                //string bankaOid = mycc5pay.oid;

                string bankaOid = mycc5pay.oid; //PaymentPageVM.CreatedOrder.TrackCode;

                // Geri dönen order id
                string bankaAppr = mycc5pay.appr;
                // Geri dönen işlem sonucu
                string bankaProv = mycc5pay.code;
                // Geri dönen provizyon numarası

                // Bankadan gelen sonuçları lblSonuc labelına yazdırıyoruz.
                //uyari1.Visible = true;
             
                PaymentPageVM.PaymentResult.Result.PaymentDate = DateTime.Now;
                PaymentPageVM.PaymentResult.Result.ErrMsg = bankaHata;
                PaymentPageVM.PaymentResult.Result.ExtraHostMsg = mycc5pay.ExtraDoc.InnerXml;
                PaymentPageVM.PaymentResult.Result.Retval = mycc5pay.procreturncode;
                PaymentPageVM.PaymentResult.Result.PayCode = mycc5pay.code;
                PaymentPageVM.PaymentResult.Result.PayRefNo = mycc5pay.refno; 
                //checkOutState.PaymentResult.Result.Retval = bankaAppr; 

                if (bankaSonuc == "1")
                {
                    if (bankaAppr == "Approved")
                    {

                        PaymentPageVM.PaymentResult.SetSuccessAndClearError(
                           String.Concat("Payment is succeed ",
                           cekilen.ToString("n2"), $" {PaymentPageVM.CreatedOrder.CurrencyCode} has been charged."));

                        PaymentPageVM.PaymentResult.Result.PaymentComplateStatus = PaymentComplateStaus.Tamamlandi;
                        PaymentPageVM.PaymentResult.Result.PaymentLogType = PaymentLogType.SiparisArti;


                    }
                    else if (bankaAppr == "Declined")
                    {

                        PaymentPageVM.PaymentResult.SetError(
                         String.Concat("Payment rejected ! : ", bankaHata, " ", mycc5pay.ExtraDoc.InnerText));

                    }
                    else
                    {
                        PaymentPageVM.PaymentResult.SetError(
                         String.Concat("Could't established connection with bank !", bankaHata, " - ", mycc5pay.ExtraDoc.InnerText));

                    } 

                }
                else
                {
                    PaymentPageVM.PaymentResult.SetError(String.Concat("Connection Error with bank!", bankaHata));
                } 

            }
            catch (Exception ex)
            {
                PaymentPageVM.PaymentResult.SetError(String.Concat("An Error has occured on pyment : ", ex.Message)); 
            }

            if (PaymentPageVM != null && PaymentPageVM.PaymentResult != null && PaymentPageVM.PaymentResult.Result != null)
            {

                PaymentPageVM.PaymentResult.Result.Description = PaymentPageVM.PaymentResult.GetErrors();
                orderService.AddPaymentHistoryLog(PaymentPageVM.PaymentResult.Result);
            }

            checkState.SetPaymentResult(PaymentPageVM.PaymentResult);

            return PaymentPageVM;

        }
    }


}