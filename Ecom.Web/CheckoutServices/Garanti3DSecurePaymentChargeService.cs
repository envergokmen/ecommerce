﻿using System;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.IO;
using System.Xml;
using Ecom.ViewModels;
using Ecom.Services;
using Microsoft.AspNetCore.Http;
using Ecom.Web.Helpers;
using Ecom.Core;
using Ecom.ViewModels.Payments;

namespace Ecom.Services.CheckoutServices
{

    public class Garanti3DSecurePaymentChargeService : PaymentChargeServiceBase, IPaymentChargeService
    {
        public Garanti3DSecurePaymentChargeService(
            CheckoutVM PaymentPageVM, 
            OrderService orderService, 
            IHttpContextAccessor contextAccessor,
            CheckoutStateHelper checkState
            )
        {
            this.orderService = orderService;
            this.PaymentPageVM = PaymentPageVM;
            this.contextAccessor = contextAccessor;
            this.checkState = checkState;
            this.HttpContext = contextAccessor.HttpContext;
        }


        public CheckoutVM MakePayment()
        {
            InitializePaymentResult();
            OperationResult<PaymentHistoryViewModel> paymentResult = new OperationResult<PaymentHistoryViewModel>();
            paymentResult.Result = new PaymentHistoryViewModel();
            paymentResult.Result.StoreAccountId = PaymentPageVM.StoreId;

            try
            {
                 
                string strMode = HttpContext.Request.Form["mode"];
                string strApiVersion = HttpContext.Request.Form["apiversion"];
                string strTerminalProvUserID = HttpContext.Request.Form["terminalprovuserid"];
                string strType = HttpContext.Request.Form["txntype"];
                string strAmount = HttpContext.Request.Form["txnamount"];
                string strCurrencyCode = HttpContext.Request.Form["txncurrencycode"];
                string strInstallmentCount = HttpContext.Request.Form["txninstallmentcount"];
                string strTerminalUserID = HttpContext.Request.Form["terminaluserid"];
                string strOrderID = HttpContext.Request.Form["oid"];
                string strCustomeripaddress = HttpContext.Request.Form["customeripaddress"];
                string strcustomeremailaddress = HttpContext.Request.Form["customeremailaddress"];
                string strTerminalID = HttpContext.Request.Form["clientid"];
                string _strTerminalID = "0" + strTerminalID;
                string strTerminalMerchantID = HttpContext.Request.Form["terminalmerchantid"];
                string strStoreKey = PaymentPageVM.SelectedWebPosInstallment.SecureKey3D;
                //HASH doğrulaması için 3D Secure şifreniz
                string strProvisionPassword = PaymentPageVM.SelectedWebPosInstallment.ApiPassword;
                //HASH doğrulaması için TerminalProvUserID şifresini tekrar yazıyoruz
                string strSuccessURL = HttpContext.Request.Form["successurl"];
                string strErrorURL = HttpContext.Request.Form["errorurl"];
                string strCardholderPresentCode = "13";
                //3D Model işlemde bu değer 13 olmalı
                string strMotoInd = "N";
                string strNumber = "";
                //Kart bilgilerinin boş gitmesi gerekiyor
                string strExpireDate = "";
                //Kart bilgilerinin boş gitmesi gerekiyor
                string strCVV2 = "";
                //Kart bilgilerinin boş gitmesi gerekiyor
                string strAuthenticationCode = WebUtility.UrlDecode(HttpContext.Request.Form["cavv"]);
                string strSecurityLevel =WebUtility.UrlEncode(HttpContext.Request.Form["eci"]);
                string strTxnID =WebUtility.UrlEncode(HttpContext.Request.Form["xid"]);
                string strMD =WebUtility.UrlEncode(HttpContext.Request.Form["md"]);
                string strMDStatus = HttpContext.Request.Form["mdstatus"];
                string strMDStatusText = HttpContext.Request.Form["mderrormessage"];

                string strHostAddress = "https://sanalposprov.garanti.com.tr/VPServlet";

                string SecurityData = GetSHA1(strProvisionPassword + _strTerminalID).ToUpper();
                string HashData = GetSHA1(strOrderID + strTerminalID + strAmount + SecurityData).ToUpper();

                //Daha kısıtlı bilgileri HASH ediyoruz.

                //strMDStatus.Equals(1)
                //"Tam Doğrulama"
                //strMDStatus.Equals(2)
                //"Kart Sahibi veya bankası sisteme kayıtlı değil"
                //strMDStatus.Equals(3)
                //"Kartın bankası sisteme kayıtlı değil"
                //strMDStatus.Equals(4)
                //"Doğrulama denemesi, kart sahibi sisteme daha sonra kayıt olmayı seçmiş"
                //strMDStatus.Equals(5)
                //"Doğrulama yapılamıyor"
                //strMDStatus.Equals(6)
                //"3-D Secure Hatası"
                //strMDStatus.Equals(7)
                //"Sistem Hatası"
                //strMDStatus.Equals(8)
                //"Bilinmeyen Kart No"
                //strMDStatus.Equals(0)
                //"Doğrulama Başarısız, 3-D Secure imzası geçersiz."

                //Hashdata kontrolü için bankadan dönen secure3dhash değeri alınıyor.
                string strHashData = HttpContext.Request.Form["secure3dhash"];
                string ValidateHashData = GetSHA1(strTerminalID + strOrderID + strAmount + strSuccessURL + strErrorURL + strType + strInstallmentCount + strStoreKey + SecurityData).ToUpper();

                //İlk gönderilen ve bankadan dönen HASH değeri yeni üretilenle eşleşiyorsa;
               
                if (strHashData == ValidateHashData)
                {
                    paymentResult.AddMessage("Sayısal Imza Geçerli");

                    //Tam Doğrulama, Kart Sahibi veya bankası sisteme kayıtlı değil, Kartın bankası sisteme kayıtlı değil
                    //Doğrulama denemesi, kart sahibi sisteme daha sonra kayıt olmayı seçmiş responselarını alan
                    //işlemler için Provizyon almaya çalışıyoruz

                    if (strMDStatus == "1" | strMDStatus == "2" | strMDStatus == "3" | strMDStatus == "4")
                    {
                        //Provizyona Post edilecek XML Şablonu
                        string strXML = null;
                        strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<GVPSRequest>" + "<Mode>" + strMode + "</Mode>" + "<Version>" + strApiVersion + "</Version>" + "<ChannelCode></ChannelCode>" + "<Terminal><ProvUserID>" + strTerminalProvUserID + "</ProvUserID><HashData>" + HashData + "</HashData><UserID>" + strTerminalUserID + "</UserID><ID>" + strTerminalID + "</ID><MerchantID>" + strTerminalMerchantID + "</MerchantID></Terminal>" + "<Customer><IPAddress>" + strCustomeripaddress + "</IPAddress><EmailAddress>" + strcustomeremailaddress + "</EmailAddress></Customer>" + "<Card><Number></Number><ExpireDate></ExpireDate><CVV2></CVV2></Card>" + "<Order><OrderID>" + strOrderID + "</OrderID><GroupID></GroupID><AddressList><Address><Type>B</Type><Name></Name><LastName></LastName><Company></Company><Text></Text><District></District><City></City><PostalCode></PostalCode><Country></Country><PhoneNumber></PhoneNumber></Address></AddressList></Order>" + "<Transaction>" + "<Type>" + strType + "</Type><InstallmentCnt>" + strInstallmentCount + "</InstallmentCnt><Amount>" + strAmount + "</Amount><CurrencyCode>" + strCurrencyCode + "</CurrencyCode><CardholderPresentCode>" + strCardholderPresentCode + "</CardholderPresentCode><MotoInd>" + strMotoInd + "</MotoInd>" + "<Secure3D><AuthenticationCode>" + strAuthenticationCode + "</AuthenticationCode><SecurityLevel>" + strSecurityLevel + "</SecurityLevel><TxnID>" + strTxnID + "</TxnID><Md>" + strMD + "</Md></Secure3D>" + "</Transaction>" + "</GVPSRequest>";

                        try
                        {
                            string data = "data=" + strXML;

                            WebRequest _WebRequest = WebRequest.Create(strHostAddress);
                            _WebRequest.Method = "POST";

                            byte[] byteArray = Encoding.UTF8.GetBytes(data);
                            _WebRequest.ContentType = "application/x-www-form-urlencoded";
                            _WebRequest.ContentLength = byteArray.Length;

                            Stream dataStream = _WebRequest.GetRequestStream();
                            dataStream.Write(byteArray, 0, byteArray.Length);
                            dataStream.Close();

                            WebResponse _WebResponse = _WebRequest.GetResponse();
                            Console.WriteLine(((HttpWebResponse)_WebResponse).StatusDescription);
                            dataStream = _WebResponse.GetResponseStream();

                            StreamReader reader = new StreamReader(dataStream);
                            string responseFromServer = reader.ReadToEnd();

                            //00 ReasonCode döndüğünde işlem başarılıdır. Müşteriye başarılı veya başarısız şeklinde göstermeniz tavsiye edilir. (Fraud riski)
                            if (responseFromServer.Contains("<ReasonCode>00</ReasonCode>"))
                            {
                                paymentResult.SetSuccessAndClearError(String.Concat("Garanti 3D PAyment has been succeed ", strAmount, " ", PaymentPageVM.CurrencyCode,  "  has been charged"));

                                paymentResult.Result.PaymentComplateStatus = PaymentComplateStaus.Tamamlandi;
                                paymentResult.Result.PaymentLogType = PaymentLogType.SiparisArti;

                            }
                            else
                            {
                                paymentResult.SetError(String.Concat("Garanti 3D Payment Error : ", strMDStatusText + " ", responseFromServer)); 
                            }

                        }
                        catch (Exception ex)
                        {
                            paymentResult.SetError(String.Concat("Garanti 3D operation has failed", strMDStatusText, " ", ex.Message));

                        }

                    }
                    else
                    {
                        paymentResult.SetError(String.Concat("Garanti 3D operation has failed: " , strMDStatusText));
                    }

                }
                else
                {

                    paymentResult.SetError(String.Concat("Garanti 3D İşlem Başarısız, Güvenlik Uyarısı. Sayısal Imza Geçerli Degil : " , strMDStatusText));
                }

                paymentResult.Result.PaymentDate = DateTime.Now;
                string mdMessage = HttpContext.Request.Form["mderrormessage"];
                if (mdMessage!=null)
                {
                    paymentResult.Result.ErrMsg = HttpContext.Request.Form["mderrormessage"];
                }
 
            }
            catch (Exception ex)
            {
                paymentResult.SetError(
                    String.Concat(" Error on payment while charging : ",
                    PaymentPageVM.TotalAmount().ToString("n2"), PaymentPageVM.CurrencyCode));  
            }

            if (paymentResult.Result != null)
            {
                paymentResult.Result.Description = paymentResult.GetErrors();
                orderService.AddPaymentHistoryLog(paymentResult.Result);
            }

            //orderService.SetCheckOutState(checkOutState);
            checkState.SetPaymentResult(paymentResult);
            PaymentPageVM.PaymentResult = paymentResult;

            return PaymentPageVM;

        }
    }


}