﻿using System;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.IO;
using System.Xml;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Ecom.Web.Helpers;
using Ecom.Core;

namespace Ecom.Services.CheckoutServices
{
    //public class TrustAllCertificatePolicy : System.Net.ICertificatePolicy
    //{
    //    public TrustAllCertificatePolicy()
    //    { }

    //    public bool CheckValidationResult(ServicePoint sp,
    //     X509Certificate cert, WebRequest req, int problem)
    //    {
    //        return true;
    //    }
    //}

        public class GarantiPaymentChargeService : PaymentChargeServiceBase, IPaymentChargeService
        {
            public GarantiPaymentChargeService(
                CheckoutVM PaymentPageVM,
                OrderService orderService,
                IHttpContextAccessor contextAccessor,
                CheckoutStateHelper checkState
                )
            {
                this.orderService = orderService;
                this.PaymentPageVM = PaymentPageVM;
                this.contextAccessor = contextAccessor;
                this.checkState = checkState;
                this.HttpContext = contextAccessor.HttpContext;
            }
 
        public CheckoutVM MakePayment()
        {
        //    if (!Validate().Status)
        //    {
        //        checkOutState.PaymentResult.AddError(Validate().ResultDescription);
        //        return checkOutState;
        //    }

            InitializePaymentResult();


            string username = PaymentPageVM.User?.UserName;

            string NameOfUser = PaymentPageVM.CreditCard.CardOwner;

            if (String.IsNullOrWhiteSpace(NameOfUser) && PaymentPageVM.User != null && !String.IsNullOrWhiteSpace(PaymentPageVM.User.Name))
            {
                NameOfUser = PaymentPageVM.User.Name;
            }

            if (!String.IsNullOrEmpty(NameOfUser) && NameOfUser.Length > 65)
            {
                NameOfUser = NameOfUser.Substring(0, 65).Trim();
            }

            //System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();

            double cekilen = Convert.ToDouble(PaymentPageVM.CreatedOrder.TotalAmount);
            string GParaCekilen = ConGarantiPara(cekilen.ToString("n2"));
            decimal TPL = Convert.ToDecimal(cekilen);
            string strMode = "PROD";
            string strVersion = "v0.01";
            string strTerminalID = PaymentPageVM.CreatedOrder.WebPosInstallment.TerminalID; //8 Haneli TerminalID yazılmalı.
            string _strTerminalID = "0" + strTerminalID;
            string strProvUserID = PaymentPageVM.CreatedOrder.WebPosInstallment.ApiUser; //"PROVAUT";
            string strProvisionPassword = PaymentPageVM.CreatedOrder.WebPosInstallment.ApiPassword;  //"YNk3Djz4"; //TerminalProvUserID şifresi
            string strUserID = PaymentPageVM.CreatedOrder.WebPosInstallment.TerminalID; //"PROVAUT"; // işlemi yapan kullanıcı önemli olmamalı
            string strMerchantID = PaymentPageVM.CreatedOrder.WebPosInstallment.MagazaNo; //"1631367"; //Üye İşyeri Numarası
            string strIPAddress = PaymentPageVM.IP; //Kullanıcının IP adresini alır

            if (1==2)
            {
                strIPAddress = "94.122.155.236"; // test için valid ip silinebilir. rasgelebilir. lokalde test
                GParaCekilen = "100";
            }

            string strEmailAddress = PaymentPageVM.CreatedOrder.UserName;
            string strOrderID = PaymentPageVM.CreatedOrder.TrackCode;

            if (PaymentPageVM.CheckoutAsGuest && PaymentPageVM.SelectedShippingAddress!=null && Utils.isEmail(PaymentPageVM.SelectedShippingAddress.Email))
            {
                strEmailAddress = PaymentPageVM.SelectedShippingAddress.Email;
            }

            if (string.IsNullOrWhiteSpace(strEmailAddress))
            {
                strEmailAddress = $"nouser@{PaymentPageVM.UrlAuthority}";
            }

            if (string.IsNullOrWhiteSpace(NameOfUser))
            {
                NameOfUser = "No User";
            }

            string strNumber = PaymentPageVM.CreditCard.CardNumber;
            string strExpireDate = PaymentPageVM.CreditCard.ExpiresMonth + PaymentPageVM.CreditCard.ExpiresYear;
            string strCVV2 = PaymentPageVM.CreditCard.CardCvc.ToString();
            string strAmount = GParaCekilen; // cekilen.ToString().Replace(",", "").Replace(".", ""); //  //İşlem Tutarı 1.00 TL için 100 gönderilmeli
            string strType = "sales";

            if (!String.IsNullOrWhiteSpace(PaymentPageVM.CreatedOrder.WebPosInstallment.ProvType))
            {
                strType = PaymentPageVM.CreatedOrder.WebPosInstallment.ProvType;
            }

            //
            string strCurrencyCode = null;

            switch (PaymentPageVM.CreatedOrder.CurrencyCode)
            {
                case "TRY": strCurrencyCode = "949"; break;
                case "TL": strCurrencyCode = "949"; break;
                case "EUR": strCurrencyCode = "978"; break;
                case "GBP": strCurrencyCode = "826"; break;
                case "USD": strCurrencyCode = "998"; break;
                default:
                    break;
            }

            string strCardholderPresentCode = "0";
            string strMotoInd = "N";
            string strInstallmentCount = "";

            if (PaymentPageVM.CreatedOrder != null && PaymentPageVM.CreatedOrder.InstallmentQuantity != null && PaymentPageVM.CreatedOrder.InstallmentQuantity > 1)
            {
                strInstallmentCount = PaymentPageVM.CreatedOrder.InstallmentQuantity.ToString();
            }

            string strHostAddress = PaymentPageVM.CreatedOrder.WebPosInstallment.PostUrl;

            if (!String.IsNullOrWhiteSpace(PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl))
            {
                strHostAddress = PaymentPageVM.CreatedOrder.WebPosInstallment.NormalPostUrl;
            }

            string SecurityData = GetSHA1(strProvisionPassword + _strTerminalID).ToUpper();
            string HashData = GetSHA1(strOrderID + strTerminalID + strNumber + strAmount + SecurityData).ToUpper();

            string strXML = null;
            strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<GVPSRequest>" + "<Mode>" + strMode + "</Mode>"
                + "<Version>" + strVersion + "</Version>" + "<Terminal><ProvUserID>" + strProvUserID + "</ProvUserID><HashData>"
                + HashData + "</HashData><UserID>" + strUserID + "</UserID><ID>"
                + strTerminalID + "</ID><MerchantID>" + strMerchantID + "</MerchantID></Terminal>" + "<Customer><IPAddress>"
                + strIPAddress + "</IPAddress><EmailAddress>" + strEmailAddress + "</EmailAddress></Customer>" + "<Card><Number>"
                + strNumber + "</Number><ExpireDate>" + strExpireDate + "</ExpireDate><CVV2>"
                + strCVV2 + "</CVV2></Card>" + "<Order><OrderID>"
                + strOrderID + "</OrderID><GroupID></GroupID><AddressList><Address><Type>S</Type><Name>" + NameOfUser + "</Name><LastName></LastName><Company></Company><Text></Text><District></District><City></City><PostalCode></PostalCode><Country></Country><PhoneNumber></PhoneNumber></Address></AddressList></Order>" + "<Transaction>" + "<Type>" + strType + "</Type><InstallmentCnt>" + strInstallmentCount + "</InstallmentCnt><Amount>" + strAmount + "</Amount><CurrencyCode>" + strCurrencyCode + "</CurrencyCode><CardholderPresentCode>"
                + strCardholderPresentCode + "</CardholderPresentCode><MotoInd>" + strMotoInd + "</MotoInd>";
             
            //if (order.MountDelayCount > 0)
            //{
            //    strXML += "<DelayDayCount>" + order.MountDelayCount.ToString() + "</DelayDayCount>";
            //}
             
            strXML += "</Transaction>"; 
            strXML += "</GVPSRequest>"; 

            try
            {
                string data = "data=" + strXML;

                WebRequest _WebRequest = WebRequest.Create(strHostAddress);
                _WebRequest.Method = "POST";

                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                _WebRequest.ContentType = "application/x-www-form-urlencoded";
                _WebRequest.ContentLength = byteArray.Length;

                Stream dataStream = _WebRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse _WebResponse = _WebRequest.GetResponse();
                Console.WriteLine(((HttpWebResponse)_WebResponse).StatusDescription);
                dataStream = _WebResponse.GetResponseStream();

                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();


                //Müşteriye gösterilebilir ama Fraud riski açısından bu değerleri göstermeyelim.
                //responseFromServer

                //GVPSResponse XML'in değerlerini okuyoruz. İstediğiniz geri dönüş değerlerini gösterebilirsiniz.
                string XML = responseFromServer;
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(XML);

                //ReasonCode
                XmlElement xElement1 = xDoc.SelectSingleNode("//GVPSResponse/Transaction/Response/ReasonCode") as XmlElement;
                //lblResult2.Text = xElement1.InnerText;

                //Message
                XmlElement xElement2 = xDoc.SelectSingleNode("//GVPSResponse/Transaction/Response/Message") as XmlElement;
                //lblResult2.Text = xElement2.InnerText;

                //ErrorMsg
                XmlElement xElement3 = xDoc.SelectSingleNode("//GVPSResponse/Transaction/Response/ErrorMsg") as XmlElement;

                try
                {
                    //Kampanya seçim linki

                    XmlElement xElement4 = xDoc.SelectSingleNode("//GVPSResponse/Transaction/CampaignChooseLink/") as XmlElement;
                    if (xElement4 != null && xElement4.InnerText != null && xElement4.InnerText.Length > 0)
                    {
                        PaymentPageVM.PaymentResult.Result.CampaignChooseLink = xElement4.InnerText;
                    }

                }
                catch
                {


                }

                PaymentPageVM.PaymentResult.Result.PaymentDate = DateTime.Now;
                PaymentPageVM.PaymentResult.Result.ErrMsg = xElement1.InnerText;
                PaymentPageVM.PaymentResult.Result.Retval = xElement3.InnerText;
                PaymentPageVM.PaymentResult.Result.ExtraHostMsg = xElement2.InnerText;

                //00 ReasonCode döndüğünde işlem başarılıdır. Müşteriye başarılı veya başarısız şeklinde göstermeniz tavsiye edilir. (Fraud riski)
                if (xElement1.InnerText == "00")
                {
                    PaymentPageVM.PaymentResult.Result.PaymentLogType = PaymentLogType.SiparisArti;
                    PaymentPageVM.PaymentResult.Result.PaymentComplateStatus = PaymentComplateStaus.Tamamlandi;
                    PaymentPageVM.PaymentResult.SetSuccessAndClearError("Payment has been succed, " + cekilen.ToString("n2") + $" {PaymentPageVM.CreatedOrder.CurrencyCode} çekildi.");

                }
                else
                {
                    PaymentPageVM.PaymentResult.SetError(
                        String.Concat(" Error on payment ", cekilen.ToString("n2"), $"  {PaymentPageVM.CreatedOrder.CurrencyCode} while charcing. <br />",
                        xElement3.InnerText, " Error code : ", xElement1.InnerText, " - ", xElement2.InnerText));

                }


            }
            catch (Exception ex)
            {
                PaymentPageVM.PaymentResult.SetError(
                    String.Concat(" Error on payment request ",
                    cekilen.ToString("n2"), $" {PaymentPageVM.CreatedOrder.CurrencyCode} while charcing"));


            }
             
            if (PaymentPageVM != null && PaymentPageVM.PaymentResult != null && PaymentPageVM.PaymentResult.Result != null)
            {

                PaymentPageVM.PaymentResult.Result.Description = PaymentPageVM.PaymentResult.GetErrors();
                orderService.AddPaymentHistoryLog(PaymentPageVM.PaymentResult.Result);
            }

            checkState.SetPaymentResult(PaymentPageVM.PaymentResult);
             
            return PaymentPageVM;

        }
    }


}