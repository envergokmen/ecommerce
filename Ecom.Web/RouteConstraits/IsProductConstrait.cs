﻿using Ecom.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecom.Web
{
    public class IsProductConstrait : IRouteConstraint
    {
       
        public IsProductConstrait()
        {
            //this.catalogService = _catalogService;
        }
        private static readonly TimeSpan RegexMatchTimeout = TimeSpan.FromSeconds(10);

        public bool Match(HttpContext httpContext,
            IRouter route,
            string routeKey,
            RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            //validate input params  
            if (httpContext == null)
                throw new ArgumentNullException(nameof(httpContext));

            if (route == null)
                throw new ArgumentNullException(nameof(route));

            if (routeKey == null)
                throw new ArgumentNullException(nameof(routeKey));

            if (values == null)
                throw new ArgumentNullException(nameof(values));

            object routeValue;

            if (values.TryGetValue(routeKey, out routeValue))
            {

               
                if (routeValue != null && routeValue.ToString().Length > 2)
                {
                    var routeVal = routeValue.ToString();

                    var forbiddenItems = new string[] { "/js","/css", "/ContentImages", ".jpg", ".png", ".js", ".css", ".min", ".ttf", ".otf", ".oef", ".woff" };

                    foreach (var item in forbiddenItems)
                    {
                        if (routeVal.Contains(item, StringComparison.CurrentCultureIgnoreCase)) return false;
                    }

                    var domainService = (DomainService)httpContext.RequestServices.GetService(typeof(DomainService));
                     
                    var curUrl = UriHelper.GetDisplayUrl(httpContext.Request);
                    var domainName = new Uri(curUrl).Authority?.Split(':')[0];
                    var domain = domainService.GetDomainByName(domainName);
                     
                    var productService = (ProductService)httpContext.RequestServices.GetService(typeof(ProductService));

                    var hasProduct = productService.GetProductForRoute(domain.StoreAccountID ?? 0, routeValue.ToString(), null);

                    return hasProduct;
                }
            }

            return false;
        }
    }
}
