﻿using System;
using System.Collections.Generic;

namespace Ecom.Core
{

    /// <summary>
    /// Base Operation Object
    /// </summary>
    public class OperationResult<T>
    {
        public const string GeneralErrorMessage = "An Error Occoured";
        public const string GeneralNotFoundMessage = "Content not found";
        public const string GeneralBadRequestMessage = "Bad request";
        public const string GeneralSuccesMessage = "Your request has been completed successfully";

        public OperationResult()
        {
            Status = false;
            ReturnCode = ReturnStatusCode.GeneralError;
            ResultDescription = new List<string>();
            SetError(GeneralErrorMessage);
        }

        private bool _status;
        public bool Status
        {
            get
            {
                return _status;
            }
            set
            {
                ReturnCode = (value) ? ReturnStatusCode.Success : ReturnCode;
                _status = value;
            }
        }

        public List<string> ResultDescription { get; set; }
        public T Result { get; set; }
        public ReturnStatusCode ReturnCode { get; set; }
        /// <summary>
        /// Adds new error message to ResultDescription and marks Status to False
        /// </summary>
        /// <param name="message">ErrorMessage To Add</param>
        public virtual void AddError(string message)
        {
            this.AddMessage(message);
            Status = false;
            if (this.ReturnCode == ReturnStatusCode.Success) this.ReturnCode = ReturnStatusCode.GeneralError;
        }

        /// <summary>
        /// Adds new errors list messages to ResultDescription and marks Status to False
        /// </summary>
        /// <param name="message">ErrorMessage To Add</param>
        public void AddError(List<string> message)
        {
            this.ResultDescription.AddRange(message);
            Status = false;
        }


        /// <summary>
        /// Adds new message to ResultDescription only, status will not change
        /// </summary>
        /// <param name="message">message to add</param>
        public void AddMessage(string message)
        {
            this.ResultDescription.Add(message);
        }

        /// <summary>
        /// Clear all error messages and set passed message as error, changes status false 
        /// </summary>
        /// <param name="message">Error message to set</param>
        public void SetError(string message)
        {
            this.ClearErrors();
            this.Status = false;

            if (message != null) AddError(message);
        }

        public void ClearMessages()
        {
            this.ClearErrors();
        }

        /// <summary>
        /// Clear all error messages and set passed message as error, changes status false, set ReturnStatusCode 
        /// </summary>
        /// <param name="message">Error message to set</param>
        /// <param name="returnCode">Return reason to set</param>
        public void SetError(string message, ReturnStatusCode returnCode)
        {
            this.ReturnCode = returnCode;
            SetError(message);
        }

        public void SetAsNotFound(string message=null)
        {
            this.ReturnCode = ReturnStatusCode.NotFound;
            this.SetError(message ?? GeneralNotFoundMessage);
        }

        public void SetAsBadRequest(string message = null)
        {
            this.ReturnCode = ReturnStatusCode.BadRequest;
            this.SetError(message ?? GeneralBadRequestMessage);
        }
        public void SetAsUnauthorized(string message = null)
        {
            this.ReturnCode = ReturnStatusCode.Unauthorized;
            this.SetError(message ?? GeneralBadRequestMessage);
        }
        /// <summary>
        /// sets new errors list messages to ResultDescription and marks Status to False
        /// </summary>
        /// <param name="message">ErrorMessage To Add</param>
        public void SetError(List<string> message)
        {
            this.ClearErrors();
            this.Status = false;
            this.ResultDescription = message;
        }

        /// <summary>
        /// sets new errors list messages to ResultDescription and marks Status to False and set ReturnStatusCode 
        /// </summary>
        /// <param name="message">ErrorMessage To Add</param>
        public void SetError(List<string> message, ReturnStatusCode returnCode)
        { 
            this.ReturnCode = returnCode;
            SetError(message);
        }

        /// <summary>
        /// Clear all error messages and set passed message as error, changes status false 
        /// </summary>
        /// <param name="message">Error message to set</param>
        public void SetErrorAndMarkAsErrored(string message = null)
        {
            SetError(message);
        }

        /// <summary>
        /// Set Result Object but clear all error messages and set passed message as error, changes status false 
        /// </summary>
        /// <param name="message">Error message to set</param>
        public void SetErrorAndMarkAsErrored(T resultToSet, string message = null)
        {
            this.Result = resultToSet;
            SetErrorAndMarkAsErrored(message);
        }

        /// <summary>
        /// Clear all error messages and set general error message as error, changes status false 
        /// </summary> 
        public void SetGeneralErrorAndMarkAsErrored()
        {
            SetError(GeneralErrorMessage);
            this.Status = false;
        }

        /// <summary>
        /// Clear all error messages, and 
        /// </summary> 
        private void ClearErrors(bool SetToSucces = false)
        {
            this.ResultDescription.Clear();

            if (SetToSucces)
            {
                this.Status = true;
            }
        }

        /// <summary>
        /// Sets given object to Result and, if message has given set message to ResultDescription if not; sets General Succes Message to ResultDescription
        /// </summary>
        /// <param name="resultToSet">Object wich will be setted as Result</param>
        /// <param name="message">message to as success message</param>
        public void SetSuccessAndClearError(T resultToSet, string message = null)
        {
            this.Result = resultToSet;

            this.ClearErrors(true);

            message = message != null ? message : GeneralSuccesMessage;
            this.AddMessage(message);
        }

        /// <summary>
        /// Sets given object to Result and, if message has given set message to ResultDescription if not; sets General Succes Message to ResultDescription
        /// </summary>
        /// <param name="resultToSet">Object wich will be setted as Result</param>
        /// <param name="message">message to as success message</param>
        public void SetSuccessAndClearError(string message = null)
        {
            this.ClearErrors(true);

            if(message!=null)
            {
                this.AddMessage(message);
            }
        }

        /// <summary>
        /// Gets to erros as string
        /// </summary>
        /// <returns></returns>
        public string GetErrors()
        {
            string ErrorTotals = "";
            foreach (var item in ResultDescription)
            {
                ErrorTotals = String.Concat(ErrorTotals, item, Environment.NewLine);
            }

            return ErrorTotals;
        }
    }

    public class OperationIdResult : OperationResult<long?>
    {

    }

    public class StandartOperationResult : OperationResult<object>
    {

    }

    public class OrderCreationResult<T> : OperationResult<T>
    {
        public string DirectUrl { get; set; }
        public bool IsRedirectRequired { get; set; }
    }

    public class OperationListResult<T> : OperationResult<List<T>>
    {
        public OperationListResult()
        {
            this.Result = new List<T>();
        }
    }

    public class PagedOperationListResult<T> : OperationResult<List<T>>
    {
        public PagedOperationListResult()
        {
            this.Result = new List<T>();
        }

        public int Page { get; set; }
        public long TotalPage => PageSize==0  || TotalItem == 0 ? 0 : TotalItem % PageSize > 0 ? TotalItem / PageSize + 1 : TotalItem / PageSize;
        public long TotalItem { get; set; }
        public int PageSize { get; set; }
        public string SearchKeyword { get; set; }
    }

    public class OperationIENumResult<T> : OperationResult<IEnumerable<T>>
    {

    }

    public class OperationArrayResult<T> : OperationResult<T[]>
    {

    }

}
