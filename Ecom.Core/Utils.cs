﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Ecom.Core
{
    public class Utils
    {
        public static bool IsNumeric(string text)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(text, "^\\d+$");
        }

        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if ((attributes != null) && (attributes.Length > 0))
                return attributes[0].Description;
            else
                return value.ToString();

        }
        public static bool isEmail(string inputEmail)
        {
            if (String.IsNullOrWhiteSpace(inputEmail)) return false;
            Regex re = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$",
                          RegexOptions.IgnoreCase);
            return re.IsMatch(inputEmail);
        }

        public static string UpperCaseFirstChar(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public static int GetRandomIntNumber(int begins, int ends)
        {
            Random r = new Random();
            return r.Next(begins, ends); //for ints 
        }

        public static bool IsAllUpperCase(string input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                if (Char.IsLetter(input[i]) && !Char.IsUpper(input[i]))
                    return false;
            }
            return true;
        }

        public static bool IsFirstLetterAllUpperCase(string input)
        {
            for (int i = 0; i < 1; i++)
            {
                if (Char.IsLetter(input[i]) && !Char.IsUpper(input[i]))
                    return false;
            }
            return true;
        }


        #region FormatUrlForAllLangs

        public static string FormatUrl(string Deger)
        {
            if (String.IsNullOrWhiteSpace(Deger)) return "";

            Deger = Deger.ToLower(new System.Globalization.CultureInfo("en-Us"));

            Deger = Deger.Replace(" & ", "-");
            Deger = Deger.Replace("&", "");
            Deger = Deger.Replace("  ", " ");
            Deger = Deger.Replace(" ", "-");
            Deger = Deger.Replace(":", "");
            Deger = Deger.Replace(",", "");
            Deger = Deger.Replace("'", "-");
            Deger = Deger.Replace("%20", "-");
            Deger = Deger.Replace("20%", "-");
            Deger = Deger.Replace("ş", "s");
            Deger = Deger.Replace("İ", "i");
            Deger = Deger.Replace("ı", "i");
            Deger = Deger.Replace("ö", "o");
            Deger = Deger.Replace("Ö", "o");
            Deger = Deger.Replace("Ş", "s");
            Deger = Deger.Replace("ç", "c");
            Deger = Deger.Replace("Ç", "c");
            Deger = Deger.Replace("ğ", "g");
            Deger = Deger.Replace("Ğ", "g");
            Deger = Deger.Replace("ü", "u");
            Deger = Deger.Replace("Ü", "u");
            Deger = Deger.Replace(".", "-");
            Deger = Deger.Replace("/", "-");
            Deger = Deger.Replace("&", "");
            Deger = Deger.Replace("}", "");
            Deger = Deger.Replace("{", "");
            Deger = Deger.Replace("(", "");
            Deger = Deger.Replace(")", "");
            Deger = Deger.Replace("%", "");
            Deger = Deger.Replace("?", "");


            Deger = Deger.Replace("--", "-");

            Deger = Regex.Replace(Deger, @"\r\n?|\n", "");

            return Deger;

        }

        public static string FormatForEngBlank(string Deger)
        {
            if (String.IsNullOrWhiteSpace(Deger)) return "";
            Deger = Deger.Replace("", "");
            Deger = Deger.Replace(":", "");
            Deger = Deger.Replace(",", "");
            Deger = Deger.Replace("'", "-");
            Deger = Deger.Replace("%20", "-");
            Deger = Deger.Replace("20%", "-");
            Deger = Deger.Replace("ş", "s");
            Deger = Deger.Replace("İ", "i");
            Deger = Deger.Replace("ı", "i");
            Deger = Deger.Replace("ö", "o");
            Deger = Deger.Replace("Ö", "O");
            Deger = Deger.Replace("Ş", "S");
            Deger = Deger.Replace("ç", "c");
            Deger = Deger.Replace("Ç", "c");
            Deger = Deger.Replace("ğ", "g");
            Deger = Deger.Replace("Ğ", "G");
            Deger = Deger.Replace("ü", "u");
            Deger = Deger.Replace("Ü", "U");
            Deger = Deger.Replace(".", "-");
            Deger = Deger.Replace("/", "-");
            Deger = Deger.Replace("&", "");
            Deger = Deger.Replace("}", "");
            Deger = Deger.Replace("{", "");
            Deger = Deger.Replace("%", "");
            Deger = Deger.Replace("?", "");

            return Deger;
        }

        public static string GetLatinCodeFromCyrillic(string str)
        {

            str = str.Replace("б", "b");
            str = str.Replace("Б", "B");

            str = str.Replace("в", "v");
            str = str.Replace("В", "V");

            str = str.Replace("г", "h");
            str = str.Replace("Г", "H");

            str = str.Replace("ґ", "g");
            str = str.Replace("Ґ", "G");

            str = str.Replace("д", "d");
            str = str.Replace("Д", "D");

            str = str.Replace("є", "ye");
            str = str.Replace("Э", "Ye");

            str = str.Replace("ж", "zh");
            str = str.Replace("Ж", "Zh");

            str = str.Replace("з", "z");
            str = str.Replace("З", "Z");

            str = str.Replace("и", "y");
            str = str.Replace("И", "Y");

            str = str.Replace("ї", "yi");
            str = str.Replace("Ї", "YI");

            str = str.Replace("й", "j");
            str = str.Replace("Й", "J");

            str = str.Replace("к", "k");
            str = str.Replace("К", "K");

            str = str.Replace("л", "l");
            str = str.Replace("Л", "L");

            str = str.Replace("м", "m");
            str = str.Replace("М", "M");

            str = str.Replace("н", "n");
            str = str.Replace("Н", "N");

            str = str.Replace("п", "p");
            str = str.Replace("П", "P");

            str = str.Replace("р", "r");
            str = str.Replace("Р", "R");

            str = str.Replace("с", "s");
            str = str.Replace("С", "S");

            str = str.Replace("ч", "ch");
            str = str.Replace("Ч", "CH");

            str = str.Replace("ш", "sh");
            str = str.Replace("Щ", "SHH");

            str = str.Replace("ю", "yu");
            str = str.Replace("Ю", "YU");

            str = str.Replace("Я", "YA");
            str = str.Replace("я", "ya");

            str = str.Replace('ь', '"');
            str = str.Replace("Ь", "");

            str = str.Replace('т', 't');
            str = str.Replace("Т", "T");

            str = str.Replace('ц', 'c');
            str = str.Replace("Ц", "C");

            str = str.Replace('о', 'o');
            str = str.Replace("О", "O");

            str = str.Replace('е', 'e');
            str = str.Replace("Е", "E");

            str = str.Replace('а', 'a');
            str = str.Replace("А", "A");

            str = str.Replace('ф', 'f');
            str = str.Replace("Ф", "F");

            str = str.Replace('і', 'i');
            str = str.Replace("І", "I");

            str = str.Replace('У', 'U');
            str = str.Replace("у", "u");

            str = str.Replace('х', 'x');
            str = str.Replace("Х", "X");
            return str;
        }

        #endregion FormatUrlForAllLangs

        public static string MaskCreditCard(string str)
        {
            if (String.IsNullOrWhiteSpace(str) || str.Length <= 4) return str;

            int i = 0;
            string masked = "";

            foreach (char item in str)
            {
                masked += (i > 6 && i < 12) ? "*" : item.ToString();
                i++;
            }
            return masked;
        }
        public static string StripHTML(string text)
        {
            return Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
        }
        public static string ReplaceSpecialChars(string text)
        {
            text = text.Replace("&ouml;", "ö");
            text = text.Replace("&uuml;", "ü");
            text = text.Replace("&Uuml;", "Ü");
            text = text.Replace("&ccedil;", "ç");
            text = text.Replace("&#39;", "`");
            text = text.Replace("&nbsp;", " ");
            return text;
        }

        public static string GetUrlWithoutDomainAndProtokolNullAble(string _fullUrl, string _domainName)
        {
            string newUrl = null;
            string DomainName = (String.IsNullOrWhiteSpace(_domainName)) ? "" : _domainName;

            if (!String.IsNullOrEmpty(_fullUrl) && !String.IsNullOrWhiteSpace(DomainName))
            {
                newUrl = _fullUrl.Replace(DomainName, "").Replace("http://", "").Replace("https://", "");

            }

            return newUrl;
        }

        public static decimal GetPercentOfNumber(decimal Percent, decimal Number, int Rounding = 0)
        {
            decimal result = Number * Percent / 100;

            if (Rounding != 0)
            {
                result = Math.Round(result, Rounding);
            }

            return result;
        }

        public static double GetPercentOfNumber(double Percent, double Number, int Rounding = 0)
        {
            double result = Number * Percent / 100;

            if (Rounding != 0)
            {
                result = Math.Round(result, Rounding);
            }

            return result;
        }

        public static int GetPercentOfNumber(int Percent, int Number)
        {
            int result = Number * Percent / 100;
            return result;
        }


        public static bool IsStringArrayContains(string[] source, string[] target)
        {
            if (source == null || target == null || source.Length <= 0 || target.Length <= 0) return false;

            foreach (var curRole in source)
            {
                if (target.Contains(curRole)) return true;
            }

            return false;

        }

        public static bool IsRolesMatch(string[] RoleNames, string[] AllowedRoleNames)
        {
            return IsStringArrayContains(RoleNames, AllowedRoleNames);

        }

        public static bool IsRolesMatch(string[] RoleNames, string AllowedRoleNames)
        {
            if (RoleNames == null || RoleNames == null || String.IsNullOrWhiteSpace(AllowedRoleNames)) return false;
            string[] allowedRoles = AllowedRoleNames.Split(',');

            if (allowedRoles != null && allowedRoles.Length > 0)
            {
                allowedRoles = (from p in allowedRoles select p.Trim()).ToArray();
            }

            return IsStringArrayContains(RoleNames, allowedRoles);

        }

        public static string ValidateTC(string tcno)
        {
            var text = "";

            if (11 != tcno.Length)
            {
                text = "TC kimlik numarası için eksik veya fazla sayı girildi";

            }
            else
            {
                int toplam = 0;

                for (int i = 0; i < tcno.Length - 1; i++)
                {
                    toplam += Convert.ToInt32(tcno[i].ToString());
                }

                if (toplam.ToString().Length <= 1)
                {
                    text = "Tc kimlik Numarası Yanlış";
                }
                else
                {
                    char b = toplam.ToString()[1];
                    if (tcno != null && tcno.Length == 11 && toplam != 0 && toplam.ToString().Length > 1 && toplam.ToString()[1] == tcno[10])
                    {

                    }
                    else
                    {
                        text = "Tc kimlik Numarası Yanlış";

                    }
                }

            }

            return text;
        }
    }
}
