﻿using System.ComponentModel;

namespace Ecom.Core
{
    public enum OrderBySortType
    {
        Asc = 1,
        Desc = 2
    }

    public enum FilterReturnType
    {
        List = 1,
        MinMaxPrice = 2
    }

    public enum ShowType
    {
        Satir = 1,
        Katalog = 2
    }

    public enum SelectSecondImage
    {
        No = 0,
        Yes = 1
    }

    public enum FilterOrderByType
    {
        PriceAsc = 1,
        PriceDesc = 2,
        NewsFirst = 3
    }

    public enum Status
    {
        [Description("Active")]
        Active = 1,

        [Description("Passive")]
        Passive = 0,

        [Description("Deleted")]
        Deleted = 2
    }

    public enum LikeDislike
    {
        [Description("Like")]
        Like = 1,

        [Description("Dislike")]
        Dislike = 2,
    }

    public enum HesapLamaTuru
    {
        [Description("Cache")]
        Nakit = 0,

        [Description("Percentage")]
        Yuzde = 1
    }

    public enum MarketPlaceType
    {
        General,
        N11,
        AmazonTR,
        AmazonUK,
        EbayUK,
        GittiGidiyor,
        AmazonUsa,
        EbayUsa,
        GoogleMerchant,
        HepsiBurada
    }

    public enum BannerPlace
    {
        Main = 0,
        Main2 = 1,
        Middle = 2,
        Middle2 = 3,
        Footer = 4,
        Footer2 = 5,
        TopHeader = 6,
        TopHeader2 = 7
    }

    public enum ReturnStatusCode
    {
        Success = 200,
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        Conflict = 409,
        UnKnown = 500,
        GeneralError = 500
    }

    public enum PassWordGenerationType
    {
        Words = 1,
        Numbers = 0

    }

    public enum CategoryTypes
    {
        [Description("Kendi Sayfasında")]
        Standart = 0,

        [Description("Üst Kategori içinde Tek Sayfada")]
        TekSayfa = 1,

        [Description("Üst Kategori içinde Sol Menü ile")]
        SolMenuile = 2,

        [Description("Sık Sorulan Sorular")]
        SSS = 3,

        [Description("Uzmanına Sor")]
        UzmaninaSor = 4,

        [Description("Eğitim")]
        Egitim = 5,

        [Description("Sertifika")]
        Sertifika = 6

    }

    public enum CategoryShowPlace
    {
        [Description("Header")]
        Header = 0,

        [Description("Content")]
        Content = 1,

        [Description("Footer")]
        Footer = 2,
    }

    public enum SenderType
    {
        [Description("Email Gönderimi")]
        Email = 0,

        [Description("Sms Gönderimi")]
        SmsGonderimi = 1
    }

    public enum PoliciyType
    {
        CargoPolicy = 0,
        ReturnPolicy = 1
    }

    public enum SendingContentType
    {
        GeneralDefault = 0,
        ForgottenPassword = 1,
        MembeshipConfirmation = 2,
        BasvuruTamamlandi = 3,
        NewMembership = 4,
        InvitationToken = 5,
        CouponGained = 6,
        RefoundRequest = 7,
        SwitchRequest = 8,
        OrderCompleted = 9,
        OrderCancelled = 10,
        OrderShipped = 11,
        ForgottonCart = 12,
        StockReminder = 13
    }

    public enum CategoryImageType
    {
        [Description("MenuImageUrl")]
        MenuImageUrl = 0,

        [Description("MobileImageUrl")]
        MobileImageUrl = 1,

        [Description("WebImageUrl")]
        WebImageUrl = 2
    }

    public enum VarYok
    {
        [Description("Yok")]
        Yok = 0,

        [Description("Var")]
        Var = 1,
    }

    public enum UserType
    {
        Normal = 0,
        StoreManager = 1,
    }

    public enum Gender
    {
        NonSpecified = 0,
        Male = 1,
        Female = 2,
        Unisex = 3,
        ChildFemale = 4,
        ChildMale = 5,
        Child = 6
    }


    public enum MaterialType
    {
        Material = 0,
        OutMaterial = 1
    }


    public enum TabType
    {
        Sepet = 0,
        Adres = 1,
        Odeme = 2,
        Sucess = 3
    }

    public enum EmailSendingProtokol
    {
        [Description("SMTP")]
        SMTP = 0,

        //[Description("QUEE")]
        //QUEUE = 1,

        [Description("EuroMessage")]
        EuroMessage = 2,

        [Description("SetRow")]
        SetRow = 3

        //[Description("MailChimp")]
        //MailChimp = 4
    }

    public enum SMSSendingProtokol
    {
        //[Description("QUEE")]
        //QUEUE = 1,

        [Description("EuroMessage")]
        EuroMessage = 2,

        [Description("SetRow")]
        SetRow = 3
    }

    public enum Durum
    {
        [Description("Aktif")]
        Aktif = 1,

        [Description("Pasif")]
        Pasif = 0,

        [Description("Silinmiş")]
        Silinmis = 2
    }


    //MNG
    //    0  – Henüz işlem yapılmadı   
    //1-      Sipariş Kargoya  Verildi  
    //2-      Transfer Aşamasında 
    //3-      Gönderi Teslim Birimine Ulaştı 
    //4-      Gönderi Teslimat Adresine Yönlendirildi. 
    //5-      Teslim Edildi. (Alıcı Adı ve Soyadı – Tarihi) 
    //6-      [kod]
    //        Teslim Edilemedi(Nedeni ile) 
    //7-      Göndericiye Teslim Edildi

    //YURT_ICI

    //NOP	0	Kargo İşlem Görmemiş.
    //IND	1	Kargo Teslimattadır.
    //ISR 2	Kargo işlem görmüş, faturası henüz düzenlenmemiştir.
    //CNL 3	Kargo Çıkışı Engellendi.
    //ISC 4	Kargo daha önceden iptal edilmiştir.

    //DLV 5	Kargo teslim edilmiştir.
    //BI  6	Fatura şube tarafından iptal edilmiştir.


    public enum KargoTeslimDurum
    {
        [Description("İşlem Görmedi")]
        IslemGormedi = 0,

        [Description("İşlem görmüş, faturası düzenlenmemiş")]
        IslemGordu = 1,

        [Description("Teslimat Aşamasında")]
        TeslimatAsamasinda = 2,

        [Description("Teslim Edildi")]
        TeslimEdildi = 3,

        [Description("Teslim Edilemedi")]
        TeslimEdilemedi = 4,

        [Description("Fatura iptal (şube)")]
        SubeFaturaIptali = 5,

        [Description("Fatura iptal")]
        FaturaIptali = 6,

        [Description("Çıkışı Engellendi")]
        CikisEngellendi = 7,

        [Description("Transfer Aşamasında")]
        TransferAsamasinda = 8,

        [Description("Geri Geldi (Göndericiye)")]
        GeriGeldi = 9

    }

    public enum KpiResult
    {
        [Description("Excel")]
        Excel = 1,

        [Description("Report")]
        Report = 0,
    }


    public enum Cinsiyet
    {
        [Description("CinsiyetErkek")]
        Erkek = 1,

        [Description("CinsiyetKadin")]
        Kadin = 0,

        [Description("Belirtilmedi")]
        Belirtilmedi = 2

    }

    public enum MobileResultCode
    {
        OK = 200,
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        IzinverilmeyenMetod = 405,
        NotAcceptable = 406,
        LoginRequired = 900,
        ServerError = 950,
        Redirect = 301

    }

    public enum NetsisTahsilatBelgeTipi
    {
        Siparis = 0,
        SiparisIptal = 1,
        Iade
    }

    public enum StockCinsiyet
    {
        [Description("CinsiyetErkek")]
        Erkek = 1,

        [Description("CinsiyetKadin")]
        Kadin = 2,

        [Description("CinsiyetBelirsiz")]
        Belirsiz = 0,

        [Description("Unisex")]
        Unisex = 3,

        [Description("CocukUnisex")]
        CocukUnisex = 4,

        [Description("CocukErkek")]
        CocukErkek = 5,

        [Description("CocukKadin")]
        CocukKadin = 6,

    }

    public enum StokGuncellemeNedeni
    {
        [Description("Bilinmiyor")]
        Bilinmiyor = 0,

        [Description("Entegrasyon Update")]
        EntegrasyonUpdate = 1,

        [Description("İade İptal")]
        UrunIadeIptal = 2,

        [Description("EF Bilinmiyor")]
        BilinmiyorEF = 3,

        [Description("Panel'den Güncelleme")]
        PaneldenUpdate = 4,

        [Description("Panel'den Ekleme")]
        PaneldenInsert = 5,

        [Description("Sipariş Verilmesi")]
        Siparis = 6,

        [Description("Entegrasyon Insert")]
        EntegrasyonInsert = 7,

        [Description("Entegrasyon Saatlik Update")]
        EntegrasyonSaatlikUpdate = 8,

        [Description("Entegrasyon Saatlik Insert")]
        EntegrasyonSaatlikInsert = 9,

        [Description("Entegrasyon Saatlik Update Trigger")]
        EntegrasyonSaatlikUpdateTrigger = 10,

        [Description("Entegrasyon JoinUpdate")]
        EntegrasyonJoinUpdate = 11,

        [Description("Alıcı İstemedi İptali")]
        AliciIstemedi = 12



    }

    public enum AdresTip
    {
        [Description("Bireysel")]
        Bireysel = 0,

        [Description("Kurumsal")]
        Kurumsal = 1

    }

    public enum YesNo
    {
        Yes = 1,
        No = 0
    }

    public enum StockImageImageType
    {
        [Description("Resim")]
        Resim = 1,

        [Description("Video")]
        Video = 2
    }

    public enum StockImageAngle
    {
        [Description("Açı 1 Varasyılan")]
        Aci1 = 1,

        [Description("Açı 2")]
        Aci2 = 2,

        [Description("Açı 3")]
        Aci3 = 3,

        [Description("Açı 4")]
        Aci4 = 4,

        [Description("Açı 5")]
        Aci5 = 5,

        [Description("Açı 6")]
        Aci6 = 6,

        [Description("Açı 7")]
        Aci7 = 7,

        [Description("Açı 8")]
        Aci8 = 8

    }

    public enum BasketChargeType
    {
        UrunAlis = 0,
        KargoTutar = 1,
        HediyePaketiUcret = 2,
        HediyeCekiKullanimiNakit = 3,
        ParaPuanKullanimi = 4,
        UrunEkAlis = 5,
        HediyeCekiKullanimiYuzde = 6

    }

    public enum SanalPosOdemeTuru
    {
        //Akbank, İş Bankası, FinansBank 
        EstStandart = 1,
        Est3DPay = 2,
        Est3DSecure = 3,

        //Garanti Bankası
        GarantiStandart4 = 4,
        Garanti3DPay = 5,
        Garanti3DModel = 6,
        Garanti3DSecure = 7,

        //YapiKrediBankası
        YapiKrediStandart = 8,
        YapiKredi3DSecure = 9,
        YapiKredi3DPay = 10,

        //Bkm
        BkmExpress = 11,
        InterVPos = 12,
        PayPal = 13,

        KuveytTurk = 20,
        KuveytTurk3DSecure = 21


    }

    public enum CatalogType
    {
        Product = 0,
        Blog = 1
    }
    public enum BlogLayoutType
    {
        [Description("Resim Üstte")]
        ResimUstte = 0,

        [Description("Resim Altta")]
        ResimAltta = 1,

        [Description("Üstte 3'lü Resim")]
        Ustte3luResim = 2,

        [Description("Altta 3'lü Resim")]
        Altta3luResim = 3,

        [Description("Üstte 2'li Resim")]
        Ustte2liResim = 4,

        [Description("Altta 2'li Resim")]
        Altta2liResim = 5,

        [Description("Altta Devam Eden Full Resimler")]
        AlttaFullDevamEden = 6,

        [Description("Üstte Devam Eden Full Resimler")]
        UstteFullDevamEden = 7

    }

    public enum LookBookItemLayoutType
    {
        [Description("2li")]
        ResimUstte = 0,

        [Description("Tekli")]
        ResimAltta = 1,

        [Description("3'lü")]
        Ustte3luResim = 2
    }


    public enum BlogGosterimYeri
    {
        [Description("Anasayfa 1")]
        Anasayfa1 = 1,

        [Description("Anasayfa 2")]
        Anasayfa2 = 2,

        [Description("Anasayfa 3")]
        Anasayfa3 = 3,

        [Description("Anasayfa 4")]
        Anasayfa4 = 4,

        [Description("Sadece Kendi Kategorisi")]
        SadeceKendiKategorisi = 5

    }

    public enum BannerGosterimYeri
    {
        [Description("Anasayfa Büyük")]
        Anasayfa1 = 1,

        [Description("Anasayfa 2")]
        Anasayfa2 = 2,

        [Description("Anasayfa 3")]
        Anasayfa3 = 3,

        [Description("Anasayfa 4")]
        Anasayfa4 = 4,

        [Description("Anasayfa 5")]
        Anasayfa5 = 5,

        [Description("Anasayfa 6")]
        Anasayfa6 = 6,

        [Description("Kategori Ana Banner")]
        Kategori = 7,

        [Description("Kategori 2")]
        Kategori2 = 8,

        [Description("ÜrünDetay Ana Banner")]
        UrunDetay = 9,

        [Description("Ürün Detay 2")]
        UrunDetay2 = 10,

        [Description("Anasayfa Kargo")]
        AnasayfaKargo = 11,

        [Description("Anasayfa Text")]
        AnasayfaTextSlider = 12,

        [Description("Anasayfa Full 1")]
        AnasayfaFull1 = 13,

        [Description("Anasayfa Full 2")]
        AnasayfaFull2 = 15,

        [Description("Anasayfa Full 3")]
        AnasayfaFull3 = 16,

        [Description("Anasayfa Full 4")]
        AnasayfaFull4 = 83,

        [Description("Anasayfa Full 5")]
        AnasayfaFull5 = 84,

        [Description("Anasayfa Full 6")]
        AnasayfaFull6 = 85,

        [Description("Anasayfa Full 7")]
        AnasayfaFull7 = 86,

        [Description("Üye Giriş Ekranı")]
        UyeGirisEkrani = 17,

        [Description("Üye Kayıt Ekranı")]
        UyeKayitEkrani = 18,

        [Description("Katalog Sayfası")]
        KatalogSayfasi = 19,

        [Description("Blog Banner")]
        BlogBanner = 20,

        [Description("Anasayfa Full Video")]
        FullVideo = 21,

        [Description("Iphone IOS Uygulama Bannerı")]
        MobilAnasayfa = 22,

        [Description("Android Uygulama Bannerı")]
        MobileAndroid = 23,

        [Description("Anasayfa 7")]
        Anasayfa7 = 37,

        [Description("Anasayfa 8")]
        Anasayfa8 = 38,


        [Description("Anasayfa 9")]
        Anasayfa9 = 39,

        [Description("Anasayfa 10")]
        Anasayfa10 = 40,

        [Description("Menu 1")]
        Menu1 = 50,

        [Description("Menu 2")]
        Menu2 = 51,

        [Description("Mobil Banner Anasayfa Full")]
        AnasayfaMobileFull = 60,

        [Description("Mobil Banner Anasayfa Full 2")]
        AnasayfaMobileFull2 = 61,

        [Description("Mobil Banner Anasayfa Full 3")]
        AnasayfaMobileFull3 = 62,

        [Description("Mobil Banner Anasayfa Full 4")]
        AnasayfaMobileFull4 = 63,

        [Description("Mobil Banner Anasayfa Full 5")]
        AnasayfaMobileFull5 = 64,

        [Description("Mobil Banner Anasayfa Full 6")]
        AnasayfaMobileFull6 = 65,

        [Description("Sipariş Ödeme")]
        SiparisOdeme = 80,

        [Description("Sipariş Adres")]
        SiparisAdres = 81,

        [Description("Sipariş Sepet")]
        SiparisSepet = 82,

        [Description("Outlet 1")]
        Outlet1 = 150,

        [Description("Outlet 2")]
        Outlet2 = 151,

        [Description("Outlet 3")]
        Outlet3 = 152,

        [Description("Outlet 4")]
        Outlet4 = 153,

        [Description("Outlet 5")]
        Outlet5 = 154,
    }

    public enum BannerType
    {
        [Description("Image")]
        Image = 0,

        [Description("Video")]
        Video = 1

    }

    public enum OturumDurumu
    {
        [Description("Tümü")]
        Tumu = 0,

        [Description("Sadece Uyeler")]
        Uyeler = 1,

        [Description("Sadece Misafirler")]
        Misafir = 2
    }


    public enum PaymentComplateStaus
    {
        [Description("Tamamlanmadı")]
        Tamamlanmadi = 0,

        [Description("Tamamlandı")]
        Tamamlandi = 1,

        [Description("Diger")]
        Diger = 3,


    }

    public enum KapidaOdemeTuru
    {
        [Description("KapidaNakit")]
        KapidaNakit = 1,

        [Description("KapidaKrediKarti")]
        KapidaKredikarti = 3,
    }

    public enum PaymentType
    {
        [Description("KrediKarti")]
        KrediKarti = 0,

        [Description("Havale")]
        Havale = 1,

        [Description("KapidaOdeme")]
        Kapida = 2,

        [Description("Paypal")]
        Paypal = 3,

        [Description("BKM")]
        Bkm = 4,

        [Description("Payu")]
        Payu = 5,

        [Description("YurtDisiPos")]
        YurtDisiPos = 6,

        [Description("MobilTurkcell")]
        MobilTurkcell = 7,

        [Description("MobilAvea")]
        MobilAvea = 8,

        [Description("MobilVodafone")]
        MobilVodafone = 9,

        [Description("MobilDiger")]
        MobilDiger = 10,

        [Description("Kiosk")]
        Kiosk = 11,

        [Description("IPara")]
        IPara = 12,

        [Description("EasyPay")]
        EasyPay = 13,

        [Description("Coupon")]
        Coupon = 14,

        [Description("Ininal")]
        Ininal = 15,

        [Description("BankaKarti")]
        DebitCard = 16

    }


    public enum PaymentLogType
    {
        [Description("Sipariş Artı")]
        SiparisArti = 1,

        [Description("Siparis Eksi")]
        SiparisEksiKaydi = 2,

        [Description("Başarısız anka Denemesi")]
        SiparisHataLogu = 3,

        [Description("Finansallaştırma Başarılı")]
        FinansallastirmaBasarili = 4,

        [Description("Finansallaştırma Hata")]
        FinansallastirmaHataLogu = 5


    }


    public enum SiparisOdemeDurumu
    {
        [Description("Test")]
        Bos = 0,

        [Description("Ödeme Bekliyor")]
        OdemeBekliyor = 1,

        [Description("Telefon Onayi Alinacak")]
        TelefonOnayiAlinacak = 2,

        [Description("Ürünler Hazırlanıyor")]
        UrunlerHazirlaniyor = 3,

        [Description("Ödeme Onaylandi")]
        OdemeOnaylandi = 4,

        [Description("Ürün Kargoya Teslim Edildi")]
        UrunKargoyaTeslimEdildi = 5,

        [Description("Kredi Kartı Ödeme Hatası")]
        KrediKartiOdemeHatasi = 6,

        [Description("Müsteri Parcali Teslim Aldi")]
        MusteriParcaliTeslimAldi = 7,

        [Description("Sipariş İptal edildi")]
        SiparisIptalEdildi = 8,

        [Description("Ürunler Iade Edildi")]
        UrunlerIadeEdildi = 9,

        [Description("Paket Hazir")]
        PaketHazir = 10,

        [Description("Sipariş İceriği Değiştirildi")]
        SiparisIcerigiDegistirildi = 11,

        [Description("PayPal Ödeme Hatası")]
        PayPalOdemeHatasi = 12,

        [Description("Değişim Nedeniyle İptal")]
        DegisimNedeniyleIptal = 18,

        [Description("Ürünleriniz elimize ulaştı")]
        UrunlerinizElimizeUlasti = 19,

        [Description("3D Ödeme Bekliyor.")]
        KrediKartiOdemeBekleniyor3D = 20,

        [Description("Iade ve Değişim Nedeniyle İptal")]
        IadeDegisimNedeniyleIptal = 21,

        [Description("Mağazandan")]
        Magazadan = 100,

        [Description("Ürün Tedarik Aşamasında")]
        UrunTedarikAsamasinda = 27,

        [Description("Sipariş Başarısız")]
        SiparisBasarisiz = 25,

        [Description("Kargoya Aktarıldı")]
        KargoyaAktarildi = 26,


        [Description("Müşteriye Teslim Edildi")]
        MusteriyeTeslimEdildi = 28,


        [Description("Onaylanmadı")]
        Onaylanmadi = 30
    }

    public enum SiparisTuru
    {
        [Description("Normal")]
        Normal = 0,

        [Description("Toptan")]
        Toptan = 1
    }

    public enum SiparisKaynagi
    {
        [Description("Bilinmiyor")]
        Bilinmiyor = 0,

        [Description("Web Desktop")]
        WebDesktop = 1,

        [Description("Mobile Uygulama IOS")]
        MobileAppIOS = 3,

        [Description("Mobile Uygulama Android")]
        MobileAppAndroid = 4,

        [Description("Mobile Uygulama Windows")]
        MobileAppWindows = 5,

        [Description("Web Mobile")]
        WebMobile = 100,

        [Description("Web Mobile Android")]
        WebMobileAndroid = 101,

        [Description("Web Mobile IOS")]
        WebMobileIOS = 102,

        [Description("Web Mobile WindowsPhone")]
        WebMobileWindowsPhone = 103,
    }

    public enum OrderedItemReturnType
    {
        [Description("iade")]
        Iade = 0,
        [Description("değişim")]
        degisim = 1,
    }

    public enum IadeStatus
    {
        [Description("Stok Yok")]
        StokYok = 0,

        [Description("Müşteri İstemedi")]
        MusteriIstemedi = 1,

        [Description("Ürün Siparişte")]
        UrunSipariste = 2,

        [Description("Kargo Çıkmadan Iade")]
        KargoCikmadanIade = 3,

        [Description("Faturali Iade")]
        FaturaliIade = 5,

        [Description("Faturalı Değişim")]
        FaturaliDegisim = 6,

        [Description("Kargo Çıkmadan Değişim")]
        KargoCikmadanDegisim = 7

    }

    public enum IadeNedeni
    {
        [Description("Ürün Sitedeki Görselden farklı")]
        UrunGorseldenFarkli = 0,

        [Description("Ürün büyük geldi")]
        UrunBuyuk = 1,

        [Description("Ürün küçük geldi")]
        UrunKucuk = 2,

        [Description("Ürün kalitesini beğenmedim.")]
        UrunKalitesi = 3,

        [Description("Aynı üründen 2 beden sipariş ettim.")]
        AynisindenSiparis = 4,

        [Description("Üzerimde hayal ettiğim gibi durmadı.")]
        UzerimdeIyiDurmadi = 5,

        [Description("Yanlış ürün gönderildi.")]
        YanlisUrunGonderildi = 6,

        [Description("Kargo firmasıyla ilgili sorun yaşadım.")]
        KargoFirmasiSorun = 7,

        [Description("Siparişimin kutusu hasarlı.")]
        SiparisKutusuHasarli = 8,

        [Description("Kusurlu Ürün.")]
        KusurluUrun = 9,

        [Description("Ürünü almaktan vazgeçtim.")]
        Vazgectim = 10

    }


    public enum MusteriUrunIadeNedeni
    {
        [Description("İade/Değişim Yapmayacağım")]
        IadeDegisimYapmayacagim = 0,

        [Description("Ürünü beğenmedim")]
        UrunuBegenmedim = 1,

        [Description("Ürün Hatalı/Defolu")]
        UrunHataliDefolu = 2
    }

    public enum MusteriIadeTalebiTipi
    {
        [Description("Ücret iadesi istiyorum")]
        UcretIadesi = 0,

        [Description("Ürünlerin yenisini istiyorum")]
        UrunlerinYenisi = 1

    }

    public enum MusteriIadeStatus
    {
        [Description("Onay bekleniyor")]
        OnayBekleniyor = 0,

        [Description("Kargo bekleniyor")]
        KargoBekleniyor = 1,

        [Description("Kargo teslim alındı, İncelendi")]
        KargoTeslimAlindi = 2,

        [Description("İade Tamamlandı")]
        IadeTamamlandi = 3,

        [Description("Değişim Gönderildi")]
        DegisimGonderildi = 4,

        [Description("İptal Edildi")]
        IptalEdildi = 5
    }

    public enum KampanyaTuru
    {
        OnlyNewRegisters = 1,
        ToAllUsers = 2,
        AcordingToCartTotal = 9,
        CouponCodeOwners = 10,
        //SecondProductPercentage = 17,

    }

    public enum ContentPageType
    {
        General = 0,
        Help = 1,
        TermsAndConditions = 2,
        Privacy = 3,
        FAQ = 4,
        BankAccounts = 5,
        OrderTermsAndConditions = 16
    }

    public enum GonderimTipi
    {
        [Description("Yeni Üye")]
        YeniUye = 0,

        [Description("Confirmasyon Token")]
        ConfirmationToken = 1,

        [Description("Sifremi Unuttum")]
        SifremiUnuttum = 2,

        [Description("Invitation Token")]
        InvitationToken = 3,

        [Description("Hediye Çeki")]
        HediyeCeki = 4,

        [Description("İade Talebiniz Alınmıştır")]
        İadeTalebi = 5,

        [Description("Değişim Talebiniz Alınmıştır")]
        DegisimTalebi = 6,

        [Description("Siparişiniz Alınmıştır")]
        SiparisMaili = 7,

        [Description("Siparişiniz İptal Edilmiştir")]
        Siparisİptal = 8,

        [Description("Siparişiniz Kargoya Verilmiştir")]
        SiparisKargoyaVerildi = 9,

        [Description("Mağazaya Atılan Mağaza Rezerve Maili")]
        MagazayaAtilanMagazaRezerve = 10,

        [Description("Kullanıcıya Atılan Mağaza Rezerve Maili")]
        KullaniciyaAtilanMagazaRezerve = 11,

        [Description("Bayilik Başvurusu Tamamlandı")]
        BayilikBasvurusuTamamlandi = 12,

        [Description("Genel Mail Şablonu")]
        Genel = 13,

        [Description("Sepette Unuttun Emaili")]
        SepetteUnuttunuz = 14,

        [Description("Sepette Unuttun Emaili Ücretsiz Kargo")]
        SepetteUnuttunuzUcretsizKargo = 15,

        [Description("Stoğa Gelince Haber Ver")]
        StogaGelinceHaberVer = 16,

        [Description("Bayilik Başvurusu")]
        BayilikBasvurusu = 17

    }



    public enum CargoCompanyType
    {
        YurticiKargo = 0,
        ArasKargo = 1,
        MngKargo = 2,
        SuratKargo = 3,
        Ups = 4,
        PttKargo = 5
    }
    public enum EmailSubscribeType
    {
        Footer = 0,
        Popup = 1
    }
    public enum UyeAktiviteTuru
    {
        Giris = 0,
        Cikis = 1,
        BilgiGuncelleme = 2,
        AdresEkleme,
        ReferansDavet = 3,
        UyelikKapatma = 4,
        IletisimFormuDoldurma = 5,
        Kayit = 6,
        MailListesineKatil = 7,
        Siparis = 8,
        SifremiUnuttum = 9,
        SatisAnketDegerledirme = 10,
        FollowListEkle = 11,
        Arama = 12,
        SepeteEkleme = 13,
        SepeteKargoEkleme = 14,
        TakipListesineEkleme = 15,
        KatalogGezinme = 16,
        DilDegistirme = 17,
        ParaBirimiDegistirme = 18,
        SabitSayfa = 19,
        UrunResmiGetir = 20,
        UrunTumTiplerResimImageFollow = 21,
        BedenResmiGetir = 22,
        Yorum = 23,
        BlogBegeni = 24,
        EmaililePaylas = 25,
        StogaGelinceHaberVer = 26,
        BildirimGuncelleme = 27,
        PencereBoyutu = 28,
        SonGezilenEkleme = 29,
        EnCokSatilanlar = 30
    }

    public enum CreditCardTypeType
    {
        Visa,
        MasterCard,
        Discover,
        Amex,
        Switch,
        Solo
    }


    public enum UtmLogType
    {
        Clicked = 0,
        Showed = 1
    }

    public enum DomainTipi
    {
        InterNal = 0,
        External = 1,
        Main = 100,
        Test = 50
    }


    public enum NotificationSendingTip
    {
        [Description("Direkt Kullanıcıya")]
        DirectToUser = 0,

        [Description("Herkese Tarih Bazlı")]
        DateBased = 1
    }

    public enum NotificationTip
    {
        [Description("Mesaj")]
        Message = 0,

        [Description("Kupon")]
        Coupon = 1,

        [Description("Kampanya")]
        Campaign = 2,

        [Description("İndirim")]
        Discount = 3,

        [Description("Kargo")]
        Cargo = 4
    }

    public enum MessageType
    {

        [Description("Mesaj")]
        Message = 0,

        [Description("Mesaj-1")]
        Message1 = 1,

        [Description("Mesaj-2")]
        Message2 = 2,

        [Description("Mesaj-3")]
        Message3 = 3

    }


    public enum NotificationDurum
    {
        UnRead = 0,
        Read = 1,
        Removed = 2
    }

    public enum KullaniciAnaSegment
    {
        [Description("Bilinmiyor")]
        Y = 0,

        [Description("A")]
        A = 1,

        [Description("B")]
        B = 2,

        [Description("C")]
        C = 3,

        [Description("D")]
        D = 4,

        [Description("E")]
        E = 5
    }

    public enum KullaniciAltSegment
    {
        [Description("Bilinmiyor")]
        Bilinmiyor = 0,

        [Description("1")]
        Bir = 1,

        [Description("2")]
        Iki = 2,

        [Description("3")]
        Uc = 3
    }

    //public enum SepetStopWatchTitle
    //{
    //    [Description("Genel bilgilerin çekilmesi")]
    //    GenelSepetBilgisi = 1,

    //       [Description("Sepet Bilgisi")]
    //    SepetBilgisi = 2,

    //       [Description("Hediye Cekleri")]
    //    HediyeCekleri =3,

    //       [Description("Stokta Olmayan Ürünler")]
    //    StoktaOlmayanUrunler = 4,

    //       [Description( "Ücretsiz Kargo")]
    //    UcretsizKargo =5
    //}

    public enum MetaRuleSayfaTipi
    {
        [Description("Ürün Detay Sayfası")]
        ProductDetail = 0,

        [Description("Kategori Sayfası")]
        Category = 1,

        [Description("Renk Dahil Ürün Detay Sayfası")]
        ProductDetailColorFul = 2,

        [Description("Kombin Detay Sayfası")]
        CombineDetail = 3,
    }

    public enum DeviceType
    {
        [Description("Desktop")]
        Desktop = 0,

        [Description("Tablet")]
        Tablet = 1,

        [Description("Mobile Phone")]
        MobilePhone = 2,

        [Description("TV")]
        TV = 4,

        [Description("None")]
        None = 5
    }
    public enum SendingProviderType
    {
        Smtp = 0,
        EuroMsg = 1,
        SetRow = 2,
        MailChimp = 3,
        SetRowSMS = 4,
        OtherSMS = 5
    }

    public enum EmailSettingPriority
    {
        Main = 0,
        Backup = 1,
        Backup2 = 2
    }

    public enum CartViewType
    {
        Cart = 0,
        CheckOutCart = 1
    }
    public enum QueueItemType
    {
        UserRegistered,
        UserLoggedIn,

        StoreAccountCreated,

        WelcomeMail,

        StockUpdated,
        StockDeleted,
        StockCreated,

        //PriceUpdate,

        NewOrderCreate,
        NewMultipleProduct,
        NewMultipleStock,
        UserProfileUpdate,

        CatalogOrderHasBeenChanged,
        CatalogItemHasBeenRemoved,
        NewItemHasBeenAddedToCatalog,

        WebPosCreated,
        WebPosUpdated,
        WebPosDeleted,

        ProductStyleCreated,
        ProductStyleUpdated,
        ProductStyleDeleted,

        ProductDepartmentCreated,
        ProductDepartmentUpdated,
        ProductDepartmentDeleted,

        BankCreated,
        BankUpdated,
        BankDeleted,

        BinNumberCreated,
        BinNumberUpdated,
        BinNumberDeleted,

        BrandCreated,
        BrandUpdated,
        BrandDeleted,

        PriceCreated,
        PriceUpdated,
        PriceDeleted,

        CargoCreated,
        CargoUpdated,
        CargoDeleted,

        PaymentCreated,
        PaymentUpdated,
        PaymentDeleted,

        CargoPriceCreated,
        CargoPriceUpdated,
        CargoPriceDeleted,

        ContentPageCreated,
        ContentPageUpdated,
        ContentPageDeleted,

        PolicyTypeCreated,
        PolicyTypeUpdated,
        PolicyTypeDeleted,

        PolicyCreated,
        PolicyUpdated,
        PolicyDeleted,

        ProductCreated,
        ProductUpdated,
        ProductDeleted,
        ProductUploaded,

        CartCreated,
        CartUpdated,
        CartDeleted,
        CartAllDeleteOrder,

        NewsLetterCreated,
        NewsLetterUpdated,
        NewsLetterDeleted,

        ProductImageCreated,
        ProductImageUpdated,
        ProductImageDeleted,
        ProductRemovedFromCatalogs,

        SendingProviderCreated,
        SendingProviderUpdated,
        SendingProviderDeleted,

        EmailTemplateCreated,
        EmailTemplateUpdated,
        EmailTemplateDeleted,

        CatalogCreated,
        CatalogUpdated,
        CatalogDeleted,

        CatalogSortCreated,
        CatalogSortUpdated,
        CatalogSortDeleted,

        DomainCreated,
        DomainUpdated,
        DomainDeleted,

        StoreCreated,
        StoreUpdated,
        StoreDeleted,

        SizeCreated,
        SizeUpdated,
        SizeDeleted,

        ColorCreated,
        ColorUpdated,
        ColorDeleted,

        ColorGlobalCreated,
        ColorGlobalUpdated,
        ColorGlobalDeleted,

        SeasonCreated,
        SeasonUpdated,
        SeasonDeleted,

        BannerCreated,
        BannerUpdated,
        BannerDeleted,

        CampaignCreated,
        CampaignUpdated,
        CampaignDeleted,

        CouponCreated,
        CouponUpdated,
        CouponDeleted,

    }

}
