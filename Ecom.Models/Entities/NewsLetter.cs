﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public partial class NewsLetter : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "EmailIsRequired")]
        [StringLength(40, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax40")]
        [DataType(DataType.EmailAddress)]
        
        public string Email { get; set; }

        public Nullable<int> UserID { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "IPIsRequired")]
        [StringLength(40, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax40")]
        
        public string IP { get; set; }

        public int? DomainID { get; set; }
        public virtual Domain Domain { get; set; }

        public int? LangID { get; set; }
        public virtual Lang Lang { get; set; }

        public virtual User User { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
