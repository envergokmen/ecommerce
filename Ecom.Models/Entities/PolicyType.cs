namespace Ecom.Models
{
    using Ecom.Core;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Policy : DbBaseObject
    {
        public Policy()
        {
            this.PolicyLocalLangs = new HashSet<PolicyLocalLang>();
        }
    
        public int ID { get; set; }
        public int? SortNo { get; set; }

        [StringLength(35)]
        
        public string Name { get; set; }

        public YesNo DefaultPolicy { get; set; }

        public PoliciyType PolicyType { get; set; }
        public virtual ICollection<PolicyLocalLang> PolicyLocalLangs { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
