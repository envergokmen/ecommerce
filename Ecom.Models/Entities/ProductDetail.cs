﻿namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class ProductDetail : DbBaseObject
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Ürün adı yazılması zorunlu.")]
        [StringLength(250)]
        
        public string ProductName { get; set; }
         
        [StringLength(250)]
        public string Title { get; set; }

        [StringLength(800)]
        public string Summary { get; set; }

        [StringLength(150)]
        
        public string Url { get; set; }
        
        public Nullable<long> ProductID { get; set; }

        [StringLength(40)]
        
        public string ProductCode { get; set; }

        [StringLength(250)]
        
        public string CatalogTree { get; set; }

        public int? BreadCrumbCatalogID { get; set; }

        public string DetailHTML { get; set; }

        [StringLength(250)]
        
        public string MetaTitle { get; set; }

        //otto fields B2B entegration.

        //[StringLength(3500)]
        
        //public string B2BMarketingtextSEO { get; set; }

        //[StringLength(3500)]
        
        //public string B2BKatalogTextSEO { get; set; }

        //[StringLength(3500)]
        
        //public string SellingPointText { get; set; }


        //Otto Fields B2B entegration.

        [StringLength(200)]
        
        public string MetaDescription { get; set; }

        [StringLength(260)]
        
        public string MetaKeywords { get; set; }

        [StringLength(260)]

        public string CanonicalLink { get; set; }

        [Required(ErrorMessage = "Dil seçilmesi zorunlu.")]
        public Nullable<int> LangID { get; set; }

        public virtual Lang Lang { get; set; }
        public virtual Product Product { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
