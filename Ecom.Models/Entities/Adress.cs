﻿namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Address : DbBaseObject
    {
        public int ID { get; set; }

        public string Name { get; set; }

        [StringLength(600)]
        
        public string Detail { get; set; }

        public Nullable<int> UserID { get; set; }

        [StringLength(40)]
        public string Session { get; set; }

        [StringLength(50)]
        
        public string PostalCode { get; set; }

        [StringLength(50)]
        
        public string StateName { get; set; }

        [StringLength(50)]
        
        public string CityName { get; set; }
         
        [StringLength(50)]
        
        public string SessionID { get; set; }

        [StringLength(90)]
        
        public string Email { get; set; }

        [StringLength(40)]
        
        public string TownName { get; set; }

        [StringLength(20)]
        
        public string Phone1 { get; set; }

        [StringLength(20)] 
        
        public string Phone2 { get; set; }
        public AdresTip AdresTipi { get; set; }

        [StringLength(40)]
        public string VergiDairesi { get; set; }

        [StringLength(40)]
        public string VergiNo { get; set; }

        [StringLength(100)]
        public string Unvan { get; set; }

        [StringLength(20)]
        public string TcNo { get; set; }

        public Nullable<int> CityID { get; set; }
        public Nullable<int> StateID { get; set; }
        public Nullable<int> TownID { get; set; }
        public Nullable<int> DistrictID { get; set; }
        public Nullable<int> QuarterID { get; set; }
        public Nullable<int> CountryID { get; set; }
        public virtual User User { get; set; }
        public virtual City City { get; set; }
        public virtual Quarter Quarter { get; set; }
        public virtual District District { get; set; }
        public virtual State State { get; set; }
        public virtual Town Town { get; set; }
        public virtual Country Country { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
