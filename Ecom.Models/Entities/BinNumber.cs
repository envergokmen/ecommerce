﻿namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class BinNumber : DbBaseObject
    {
        public BinNumber()
        {
           // this.Products = new HashSet<Product>();
        }

        public int ID { get; set; }

        public int BINNO { get; set; }

        public Nullable<int> BankCode { get; set; }
        public Nullable<int> OwnerBankCode { get; set; } 

        public Nullable<int> WebPosID { get; set; }
         
        [StringLength(50)]
        
        public string BankName { get; set; }

        [StringLength(50)]
        
        public string CardName { get; set; }
         
        [StringLength(25)]
        
        public string CardType { get; set; }

        [StringLength(25)]
        
        public string SubCardType { get; set; }

        [StringLength(150)]
        
        public string BankLogoPath { get; set; }

        [StringLength(150)]
        
        public string CardLogoPath { get; set; } 
         
        [StringLength(40)]
        
        public string Name { get; set; }
         
        public bool AllowInstallment { get; set; }

        [StringLength(20)]
        
        public string PrizeType { get; set; }

        public bool IsCorporate { get; set; }


        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
