﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.Models
{
    public class ActiveLang
    {
        public int ID { get; set; }
        public int StoreAccountId { get; set; }
        public int? DomainID { get; set; }
        public int? LangID { get; set; }

        public virtual Domain Domain { get; set; }
        public virtual Lang Lang { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
