namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Order : DbBaseObject
    {
        public Order()
        {
            this.OrderedItems = new HashSet<OrderedItem>();
            this.PaymentHistories = new HashSet<PaymentHistory>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }

        [StringLength(40)]
        
        public string TrackCode { get; set; }

        [StringLength(20)]
        
        public string CariCode { get; set; }

        public Nullable<int> UserID { get; set; }

        [StringLength(50)]
        
        public string Session { get; set; }

        [StringLength(40)]
        
        public string TempSession { get; set; }

        public Nullable<int> Installment { get; set; }
        public Nullable<decimal> CommissionRatio { get; set; }

        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> TotalAmountInLocalCurrency { get; set; }
        public Nullable<double> LocalCurrencyExchangeRate { get; set; }

        public SiparisOdemeDurumu PaymentStatus { get; set; }
        public PaymentType PaymentType { get; set; }
        public SiparisTuru OrderType { get; set; }
        public SiparisKaynagi OrderPlace { get; set; }
        public KargoTeslimDurum CargoDeliveryStatus { get; set; }

        [StringLength(150)]
        public string CargoDeliveryMessage { get; set; }

        [StringLength(250)]
        public string CargoTrackingUrl { get; set; }

        [StringLength(40)]
        public string CargoTrackingCode{ get; set; }

        public Nullable<DateTime> PaymentDate { get; set; }
        public Nullable<DateTime> CargoDeliveryDate { get; set; }
        public Nullable<DateTime> LastItemChangeDate { get; set; }

        public Nullable<DateTime> LastSmsSendDate { get; set; }
        public Nullable<DateTime> LastItemRemoveDate { get; set; }
        public Nullable<decimal> CargoAmount { get; set; }
        public Nullable<decimal> CommissionAmount { get; set; }
        public Nullable<decimal> CouponAmount { get; set; }

        public decimal CampaignAmount { get; set; }
        public decimal ExtraAmount { get; set; }
        public Nullable<decimal> GiftAmount { get; set; }

        public bool ShipAsGift { get; set; }

        public Nullable<int> CampaignID { get; set; }
        public Nullable<int> CargoID { get; set; }
        public Nullable<int> CargoPriceID { get; set; }

        [StringLength(25)]
        
        public string CargoCode { get; set; }
         
        [StringLength(350)]
        
        public string CargoLink { get; set; }

        public Nullable<int> UpsAreaID { get; set; }


        public Nullable<int> BankID { get; set; }
        public Nullable<int> PriceCurrencyID { get; set; }

        public bool OneDayCargo { get; set; }
         
        [StringLength(60)]
        
        public string Name { get; set; }

        [StringLength(90)]
        
        public string Email { get; set; }

        [StringLength(200)]
        
        public string GiftNote { get; set; }

        [StringLength(300)]
        
        public string OrdNote { get; set; }

        [StringLength(25)]
        
        public string Gsm { get; set; }

        [StringLength(25)]
        
        public string Phone { get; set; }

        [StringLength(300)]
        
        public string BillingAdressDetail { get; set; }

        public Nullable<int> BillingAdressID { get; set; }
        public Nullable<int> BillingAdressCountryID { get; set; }
        public Nullable<int> BillingAdressCityID { get; set; }
        public Nullable<int> BillingAdressTownID { get; set; }
        public Nullable<int> BillingAdressDistrictID { get; set; }
        public Nullable<int> BillingAdressQuarterID { get; set; }

        //faturan�n farkl� adrese g�nderilmesi
        public Nullable<int> BillingShipAdressID { get; set; }

        [StringLength(20)]
        
        public string BillingAdressPhone { get; set; }

        [StringLength(20)]
        
        public string BillingAdressPhone2 { get; set; }

        
        public string BillingAdressTcNo { get; set; }
         
        [StringLength(300)]
        
        public string DeliveryAdressDetail { get; set; }

        [StringLength(20)]
        
        public string DeliveryAdressGSM { get; set; }

        [StringLength(20)]
        
        public string DeliveryAdressPhone { get; set; }

        [StringLength(20)]
        
        public string DeliveryAdressPhone2 { get; set; }

        
        public string DeliveryAdressTcNo { get; set; }

        public Nullable<int> DeliveryAdressID { get; set; }
        public Nullable<int> DeliveryAdressCountryID { get; set; }
        public Nullable<int> DeliveryAdressCityID { get; set; }
        public Nullable<int> DeliveryAdressTownID { get; set; }
        public Nullable<int> DeliveryAdressDistrictID { get; set; }
        public Nullable<int> DeliveryAdressQuarterID { get; set; }

        public bool IsOnlineOrder { get; set; }

        [StringLength(40)]
        
        public string IP { get; set; }

        public Nullable<long> ReturnedOrderID { get; set; } 
        public decimal ReturnAmount { get; set; }

        public Nullable<int> LangID { get; set; }
        public Nullable<int> DomainID { get; set; }
        public Nullable<decimal> PriceCurrencyAmount { get; set; }

        public Nullable<long> CouponID { get; set; }
        public string CouponCode { get; set; }

        public Nullable<int> WebPosID { get; set; }
        public Nullable<int> WebPosInstallmentID { get; set; }
        public bool InvoicesSendIntoCourier { get; set; }

        public Nullable<bool> IsCargoTransferred { get; set; }

        public virtual Cargo Cargo { get; set; }

        public virtual Campaign Campaign { get; set; }

        public virtual CargoPrice CargoPrice { get; set; }

        public virtual Bank Bank { get; set; }

        public virtual User User { get; set; }

        public virtual Coupon Coupon { get; set; }

        public virtual Domain Domain { get; set; }

        public virtual PriceCurrency PriceCurrency { get; set; }

        public virtual WebPos WebPos { get; set; }

        public virtual WebPosInstallment WebPosInstallment { get; set; }

        public virtual ICollection<OrderedItem> OrderedItems { get; set; }
        public virtual ICollection<PaymentHistory> PaymentHistories { get; set; }

        public bool IsTransferedToNetsis { get; set; }
        public bool IsReturnedToNetsis { get; set; }
        public bool IsMoneyTransferedNetsis { get; set; }

        public bool IsTransferedToNetsisTahsilat { get; set; }

        [StringLength(100)]
        
        public string BkmToken { get; set; }
         
        [StringLength(100)]
        
        public string PayPalToken { get; set; }

        [StringLength(30)]
        
        public string utm_source { get; set; }

        [StringLength(30)]
        
        public string utm_campaign { get; set; }

        [StringLength(30)]
        
        public string utm_medium { get; set; }
         
        public Nullable<bool> IsPaid { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
