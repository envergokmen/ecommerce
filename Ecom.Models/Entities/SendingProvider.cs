﻿using Ecom.Core;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{

    public partial class SendingProvider : DbBaseObject
    { 

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        
        [Required(ErrorMessage = "Tanım adı gereklidir")]
        public string Name { get; set; }

        [StringLength(90)]
        
        public string SenderName { get; set; }
        [StringLength(90)]
        
        public string UserName { get; set; }

        [StringLength(20)] 
        public string Password { get; set; }

        public SendingProviderType ProviderType { get; set; }

        #region SMTP
        public int SmptPort { get; set; }

        [StringLength(150)]
        public string SmptHost { get; set; }

        [StringLength(150)]
        public string ApiEndPoint { get; set; }

        public bool EnableSsl { get; set; }

        #endregion

        #region EuroMSG

        [StringLength(25)]
        public string EuNameColumnName { get; set; }

        [StringLength(25)]
        public string EuEmailColumnName { get; set; }

        [StringLength(60)]
        public string EuroMessageFromName { get; set; }

        [StringLength(60)]
        public string EuroMessageFromAddress { get; set; }

        [StringLength(60)]
        public string EuroMessageReplyAddress { get; set; }

        [StringLength(25)]
        public string EuroMessageApiUser { get; set; }

        [StringLength(25)]
        public string EuroMessageApiPassword { get; set; }

        #endregion
         
        public string MailApiKey { get; set; } 
        public string MailTransCode { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
