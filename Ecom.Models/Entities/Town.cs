namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Town : DbBaseObject
    {
       
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
         
        public string Name { get; set; }

        public Nullable<int> CityID { get; set; }  

        public virtual City City { get; set; }

        //public int? StoreAccountID { get; set; }
        //public virtual StoreAccount StoreAccount { get; set; }
    }
}
