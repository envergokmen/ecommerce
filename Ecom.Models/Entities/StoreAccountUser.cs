﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.Models
{
    public class StoreAccountUser
    {
        public int ID { get; set; }
        public int StoreAccountId { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
