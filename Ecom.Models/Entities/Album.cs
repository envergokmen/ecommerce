﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public class Album : DbBaseObject
    {
        public Album()
        {

        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        //[Required(ErrorMessage = null, ErrorMessageResourceName = "NameIsRequired")]
        //[StringLength(100, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax100")]
        
        public string Name { get; set; }

        public int AlbumItemCount { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }

    
}
