namespace Ecom.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Size : DbBaseObject
    {
        public Size()
        { 
            this.Stocks = new HashSet<Stock>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(30)]
        
        public string Name { get; set; }

        [StringLength(15)]
        
        public string SizeCode { get; set; }

        public int SortNo { get; set; }
     
        public virtual ICollection<Stock> Stocks { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
