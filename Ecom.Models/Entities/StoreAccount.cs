﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ecom.Models
{
    public class StoreAccount : DbBaseObject
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string StoreName { get; set; }

        //[StringLength(50)]
        //public string StoreUrl { get; set; }

        [StringLength(100)]
        public string CompanyName { get; set; }
        public string CompanyTaxInfo { get; set; }
        public string CompanyTaxInfo2 { get; set; }

        [StringLength(100)]
        public string CompanyEmail { get; set; }

        [StringLength(100)]
        public string CompanyAddress { get; set; }
         
        [StringLength(100)]
        public string CompanyPhone { get; set; }
         
        public int? CountryId { get; set; }
        public int? CityId { get; set; }
        public int? TownId { get; set; }

        public int? RelatedUserId { get; set; }

        //public virtual Town Town { get; set; }
        //public virtual City City { get; set; }
        //public virtual Country Country { get; set; }

    }
}
