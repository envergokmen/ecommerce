namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class OrderTemp : DbBaseObject
    {
        public OrderTemp()
        {
            this.OrderedItems = new HashSet<OrderedItem>();
        }

        public long ID { get; set; }

        [StringLength(40)]
        
        public string OrdNo { get; set; }

        [StringLength(20)]
        
        public string TrackCode { get; set; }
        public Nullable<int> UserID { get; set; }

        [StringLength(40)]
        
        public string Session { get; set; }

        //public Nullable<int> Status { get; set; }
        public Nullable<int> Installment { get; set; }
        public Nullable<decimal> CommissionRatio { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public SiparisOdemeDurumu PaymentStatus { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public Nullable<int> CargoID { get; set; }

        [StringLength(200)]
        
        public string GiftNote { get; set; }

        [StringLength(300)]
        
        public string OrdNote { get; set; }

        [StringLength(25)]
        
        public string Gsm { get; set; }

        [StringLength(25)]
        
        public string Phone { get; set; }

        [StringLength(200)]
        
        public string BillingAdressDetail { get; set; }

        public int BillingAdressCountryID { get; set; }
        public Nullable<int> BillingAdressCityID { get; set; }

        [StringLength(100)]
        
        public string DeliveryAdressDetail { get; set; }

        public int DeliveryAdressCountryID { get; set; }
        public int DeliveryAdressCityID { get; set; }
        public Nullable<int> LangID { get; set; }
        public Nullable<decimal> PriceCurrenyAmount { get; set; }
        public Nullable<int> PriceCurrenyID { get; set; }
        public Nullable<long> CouponID { get; set; }

        public Nullable<int> WebPosID { get; set; }
        public Nullable<int> WebPosInstallmentID { get; set; } 
        public bool InvoicesSendIntoCourier { get; set; }

        public virtual Cargo Cargo { get; set; }
        public virtual User User { get; set; }
        public virtual Coupon Coupon { get; set; }

        public virtual WebPos WebPos { get; set; }
        public virtual WebPosInstallment WebPosInstallment { get; set; }

        public virtual ICollection<OrderedItem> OrderedItems { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
