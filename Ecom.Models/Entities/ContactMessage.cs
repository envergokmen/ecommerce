﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public class ContactMessage : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        //[Required(ErrorMessage = null, ErrorMessageResourceName = "NameIsRequired")]
        [StringLength(70, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax70")]
        
        public string Name { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "EmailIsRequired")]
        [StringLength(90, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax90")]
        
        public string Email { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "SubjectNameIsRequired")]
        [StringLength(40, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax40")]
        
        public string Subject { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "MessageIsRequired")]
        [StringLength(500, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax500")]
        
        public string Message { get; set; }

        [StringLength(20)]
        public string Telephone { get; set; }

        [StringLength(40)]
        public string IP { get; set; }

        public Nullable<int> UserID { get; set; }

        public virtual User User { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
