namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Cart : DbBaseObject
    {
        public Cart()
        {
            this.Quantity = 1;
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }

        public Nullable<int> UserID { get; set; }
        public Nullable<long> ProductID { get; set; }

        [StringLength(50)]
        
        public string ProductCode { get; set; }

        [StringLength(70)]
        
        public string Barkod { get; set; }

        [StringLength(70)]
        
        public string YapCode { get; set; }
         
        public Nullable<long> StockID { get; set; }
        public Nullable<int> CargoPriceID { get; set; }

        [Required]
        [Range(1, 99)]
        public Nullable<int> Quantity { get; set; }

        public Nullable<decimal> Amount { get; set; }
        public Nullable<int> PriceCurrencyID { get; set; }
        public Nullable<decimal> AmountPsf { get; set; }

        [StringLength(50)]
        public string Session { get; set; }

        [StringLength(200)]
        
        public string Name { get; set; }

        [StringLength(200)]
        
        public string ImagePath { get; set; }
         
        [StringLength(200)]
        
        public string GiftNote { get; set; }
         
        [StringLength(40)]
        
        public string IP { get; set; }

        [StringLength(500)]
        
        public string UserAgent { get; set; }

        public Nullable<long> CouponID { get; set; }
         
        [StringLength(50)]
        public string SizeName { get; set; }


        [StringLength(100)]
        public string ColorName { get; set; }

        [StringLength(50)]
        public string Url { get; set; }

        public Nullable<int> ColorID { get; set; }
        public Nullable<int> SizeID { get; set; }
        public Nullable<int> LangID { get; set; }

        public Nullable<BasketChargeType> BasketChargeType { get; set; }
        public virtual User User { get; set; }
        public virtual Stock Stock { get; set; }
        public virtual Product Product  { get; set; }
        public virtual Coupon Coupon { get; set; }
        public virtual CargoPrice CargoPrice { get; set; }
        public virtual PriceCurrency PriceCurrency { get; set; }
        public virtual Lang Lang { get; set; }

        public Nullable<int> DomainID { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
