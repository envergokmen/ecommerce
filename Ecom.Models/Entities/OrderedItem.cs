namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class OrderedItem : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }

        public Nullable<long> StockID { get; set; }
        public Nullable<long> ProductID { get; set; }
        public Nullable<int> CargoPriceID { get; set; }

        [StringLength(40)]
        
        public string ProductCode { get; set; }

        [StringLength(70)]
        
        public string YapCode { get; set; }

        [StringLength(70)]
        
        public string Barkod { get; set; }

        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> AmountOfDiscount { get; set; }
        public Nullable<decimal> AmountPsf { get; set; }
        public Nullable<long> OrderID { get; set; }

        public Nullable<long> CouponID { get; set; }
        public Nullable<int> ReturnedOrderID { get; set; }

        public Nullable<BasketChargeType> BasketChargeType { get; set; }
         
        [StringLength(200)]
        
        public string ImagePath { get; set; }

        [StringLength(200)]

        public string SizeName { get; set; }

        [StringLength(200)]
        public string ColorName { get; set; }

        [StringLength(200)]
        public string Url { get; set; }

        [StringLength(200)]
        
        public string Name { get; set; }

        [StringLength(200)]
        
        public string GiftNote { get; set; }

        public decimal VatRatio { get; set; }

        public Nullable<IadeStatus> ReturnStatus { get; set; }
        public Nullable<IadeNedeni> ReturnReason { get; set; }
        public bool IsTransferedAsReturned { get; set; }
        
        //public Nullable<IadeStatus> ReturnStatus { get; set; }
        //public Nullable<IadeStatus> ReturnStatus { get; set; }


        public virtual Coupon Coupon { get; set; }
        public virtual CargoPrice CargoPrice { get; set; } 
        public virtual Product Product { get; set; } 
        public virtual Stock Stock { get; set; } 
        public virtual Order Order { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
