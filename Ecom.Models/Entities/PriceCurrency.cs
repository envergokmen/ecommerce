namespace Ecom.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    public partial class PriceCurrency :DbBaseObject
    {
        public PriceCurrency()
        {
            this.Prices = new HashSet<Price>();
        }


        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(20)]
        
        public string Name { get; set; }

        [StringLength(10)]
        
        public string CurrencyCode { get; set; }

        [StringLength(10)]
        
        public string CurrencySymbol { get; set; }

        [StringLength(25)]
        
        public string XMLCurrencyName { get; set; }
         
        [StringLength(25)]
        
        public string XMLCurrencyCode { get; set; }
         
        public bool IsDefault { get; set; }
         
        public virtual ICollection<Price> Prices { get; set; }
    }
}
