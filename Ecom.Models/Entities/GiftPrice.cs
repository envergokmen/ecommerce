﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ecom.Models
{
    public partial class GiftPrice : DbBaseObject
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Tutar girilmesi zorunlu.")]
        public decimal Amount { get; set; }

        public Nullable<decimal> FreeGiftLimit { get; set; }

        public int GiftID { get; set; }
        public Nullable<int> CountryID { get; set; }

        //[Required(ErrorMessage = "Para birimi secilmesi zorunlu.")]
        //[Range(1, 100000)]
        //public int PriceCurrencyID { get; set; }

        public Nullable<int> VAT { get; set; }

        //public virtual PriceCurrency PriceCurrency { get; set; }

        public virtual Country Country { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
