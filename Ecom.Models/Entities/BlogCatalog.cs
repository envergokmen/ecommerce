namespace Ecom.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    public partial class BlogCatalog :DbBaseObject
    {
        public BlogCatalog()
        {
          //  this.SubCatalogs = new HashSet<Catalog>();
          //  this.Parents = new HashSet<Catalog>();
             
        }
    
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]  
        public int ID { get; set; }

        [StringLength(50)]
         
        public string Name { get; set; }

        [StringLength(70)]
         
        public string Url { get; set; }

        public bool ShowInMenu { get; set; }

        public Nullable<int> ParentID { get; set; }

        public Nullable<int> LangID { get; set; }

        public int SortNo { get; set; }
         
        [StringLength(250)]
        
        public string MetaTitle { get; set; }

        [StringLength(160)]
        
        public string MetaDescription { get; set; }

        [StringLength(260)]
        
        public string MetaKeywords { get; set; }
           
        public virtual BlogCatalog Parent { get; set; }

        //public virtual ICollection<Catalog> Parents { get; set; } 
        public virtual ICollection<BlogCatalog> SubCatalogs { get; set; }


        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
