namespace Ecom.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class Country : DbBaseObject
    {
   
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]     
        public int ID { get; set; }

        [StringLength(100)]
         
        public string Name { get; set; } 

        [StringLength(100)]
        
        public string GlobalName { get; set; }

        public string CountryCode { get; set; }

        //public int? StoreAccountID { get; set; }
        public int? DefaultPriceCurrencyID { get; set; }
        public int? DefaultLangID { get; set; }

        public Lang DefaultLang { get; set; }
        public PriceCurrency DefaultPriceCurrency { get; set; }

        //public virtual StoreAccount StoreAccount { get; set; }
    }
}
