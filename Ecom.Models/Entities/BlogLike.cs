﻿namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class BlogLike : DbBaseObject
    {
        public BlogLike()
        {
            //this.UserMessageItems = new HashSet<UserMessageItem>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(40)]
        
        public string IP { get; set; }
      
        //[EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "GecerliMail")]
        [StringLength(90)]
        
        public string Email { get; set; }
   
        public Nullable<int> UserID { get; set; } 
        public Nullable<int> BlogPostID { get; set; }
        
        public virtual User User { get; set; }
        public virtual BlogPost BlogPost { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }

}
