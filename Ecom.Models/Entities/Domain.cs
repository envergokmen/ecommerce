﻿namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.ComponentModel.DataAnnotations;


    public partial class Domain : DbBaseObject
    {
        public int ID { get; set; }

        public byte Priority { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "İsim gereklidir")]
        public string Name { get; set; }

        //[Required(ErrorMessage = "Para birimi seçilmesi zorunludur.")]
        [Range(1, 100000)]
        public int? PriceCurrencyID { get; set; }

        [Required(ErrorMessage = "Dil seçilmesi zorunlu.")]
        [Range(1, 100000)]
        public int LangID { get; set; }
         
        public Nullable<int> DepotID { get; set; }

        public DomainTipi DomainType { get; set; }

        //Title & Slogan
        [StringLength(50)]
        public string DomainBrandName { get; set; }
         
        [StringLength(160)]
        public string DomainSlogan { get; set; }
        
        public bool AllowChangeLang { get; set; }
        public bool AllowChangeCurrency { get; set; }
       
        public virtual PriceCurrency PriceCurrency { get; set; }

        public virtual Lang Lang { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
