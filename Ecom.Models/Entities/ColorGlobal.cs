namespace Ecom.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class ColorGlobal : DbBaseObject
    {
        public ColorGlobal()
        {
            this.Colors = new HashSet<Color>();
        }
    
        public int ID { get; set; }

        [StringLength(20)]
         
        public string Name { get; set; }


        [StringLength(7)]
         
        public string HexValue { get; set; }

        public int? SortNo { get; set; }

        public virtual ICollection<Color> Colors { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
