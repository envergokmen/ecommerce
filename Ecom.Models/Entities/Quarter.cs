namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Quarter : DbBaseObject
    {
       
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
         
        public string Name { get; set; }
         
        [StringLength(50)]
        
        public string DistrictName { get; set; }
         
        public Nullable<int> DistrictID { get; set; }

        public Nullable<int> IlceID { get; set; }

        public virtual District District { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
