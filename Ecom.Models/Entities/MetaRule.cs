﻿using Ecom.Core;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public class MetaRule : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(1000)]
        
        public string MetaTitle { get; set; }

        
        public string MetaDescription { get; set; }

        [StringLength(1000)]
        
        public string Keywords { get; set; }

        public MetaRuleSayfaTipi MetaRulePageType{ get; set; }

        [Required(ErrorMessage = "Dil seçilmesi zorunlu.")]
        [Range(1, 100000)]
        public Nullable<int> LangID { get; set; }

        public virtual Lang Lang { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}