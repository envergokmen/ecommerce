﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public class Lang : DbBaseObject
    { 
        public Lang()
        {
           
            
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "NameIsRequired")]
        [StringLength(20, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax20")]
        
        public string Name { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "LangCodeIsRequired")]
        [StringLength(5, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax5")]
        
        public string LangCode { get; set; }

        [StringLength(100, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax100")]
        
        public string FlagPath { get; set; }

        public bool IsDefault { get; set; }

    }
}
