namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class OrderHistory : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }

        public Nullable<long> OrderID { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> PaymentHistoryID { get; set; }
        

        public Nullable<SiparisOdemeDurumu> OrderStatus { get; set; }
         
        [StringLength(600)]
        
        public string Note { get; set; }

        public virtual User User { get; set; }
        public virtual Order Order { get; set; }
        public virtual PaymentHistory PaymentHistory { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
