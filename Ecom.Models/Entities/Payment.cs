namespace Ecom.Models
{
    using Ecom.Core;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    public partial class Payment : DbBaseObject
    {
        public Payment()
        {
            this.WebPoses = new HashSet<WebPos>();
        }

        public int ID { get; set; }

        [StringLength(40)]
        
        public string Name { get; set; }

        public PaymentType PaymentType { get; set; }

        [StringLength(20)]
        public string PaymentKeyForOtherTypes { get; set; }

        [StringLength(100)]
        public string LogoPath { get; set; }

        public int? SortNo { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

        public virtual ICollection<WebPos> WebPoses { get; set; }

    }
}
