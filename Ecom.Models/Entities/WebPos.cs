﻿namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    
    public partial class WebPos : DbBaseObject
    {
        public WebPos()
        {
            this.Orders = new HashSet<Order>();
            this.WebPosInstallments= new HashSet<WebPosInstallment>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(40)]
        
        public string Name { get; set; }
         
        public Nullable<int> BankCode { get; set; }

        [StringLength(100)]
        
        public string PostUrl { get; set; }


        [StringLength(100)]
        
        public string NormalPostUrl { get; set; }


        [StringLength(20)]
        
        public string MagazaNo { get; set; } 

        [StringLength(20)]
        
        public string TerminalID { get; set; } 
         
        //[StringLength(20)]
        //
        //public string TerminalMerchantID { get; set; } 

        [StringLength(50)]
        
        public string SecureKey3D { get; set; } 
         
        [StringLength(15)]
        
        public string ApiUser { get; set; } 
          
        [StringLength(20)]
        
        public string ApiPassword { get; set; }


        [StringLength(10)]
        
        public string ProvType { get; set; }

        [StringLength(10)]
        
        public string PostNetNo { get; set; }
           
        //[StringLength(20)]
        //
        //public string StaticIP { get; set; } 
           
        [StringLength(100)]
        
        public string LogoPath { get; set; }
         
        public SanalPosOdemeTuru PaymentType { get; set; }

        public Nullable<int> SortNo { get; set; }

        public Nullable<int> PaymentID { get; set; }

        public int MountDelayCount { get; set; }

        public  bool IsDefault { get; set; }

        [StringLength(20)]
        
        public string EntegHesapKodu { get; set; }

        [StringLength(50)]
        
        public string EntegHesapAdi { get; set; }
         
        public int? EntegRefKodu { get; set; }
         
        public virtual ICollection<Order> Orders { get; set; }

        public virtual ICollection<WebPosInstallment> WebPosInstallments { get; set; }
          
        public virtual Payment Payment { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
