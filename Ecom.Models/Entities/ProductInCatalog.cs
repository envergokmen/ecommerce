﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ecom.Models
{
    public partial class ProductInCatalog : DbBaseObject
    {
        public int ID { get; set; }
        public Nullable<int> CatalogID { get; set; }
        public Nullable<long> ProductID { get; set; }

        [StringLength(40)]
        
        public string ProductCode { get; set; }

        [StringLength(40)]
        
        public string Barkod { get; set; }

        public bool IsAutoRemoved { get; set; }
        //public bool IsAutoAdded { get; set; }

        public Nullable<long> StockID { get; set; }
        public Nullable<int> ColorID { get; set; }
        public Nullable<int> ColorGlobalID { get; set; }
        public Nullable<int> SortNo { get; set; }

        public virtual Catalog Catalog { get; set; }
        public virtual Color Color { get; set; }
        public virtual Stock Stock { get; set; }
        public virtual Product Product { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
