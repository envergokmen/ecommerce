﻿namespace Ecom.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    
    public partial class Bank : DbBaseObject
    {
        public Bank()
        {
            this.Orders = new HashSet<Order>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)] 
        public int ID { get; set; }

        [MaxLength(40)]
        public string Name { get; set; }

        [StringLength(60)]
        public string CorporateName { get; set; }
         
        [StringLength(40)]
        
        public string CityName { get; set; }
         
        [StringLength(40)]
        
        public string DepartmentAndNo  { get; set; }

        [StringLength(25)]
        public string AccountNo { get; set; }

        [StringLength(25)]
        public string SortCode { get; set; }

        [StringLength(40)]
        
        public string IBAN { get; set; }

        [StringLength(100)]
        
        public string LogoPath { get; set; }

        public Nullable<int> PaymentID { get; set; }

        public Nullable<int> BankCode { get; set; }

        public int SortNo { get; set; }
          
        public virtual ICollection<Order> Orders { get; set; } 
         
        public virtual Payment Payment { get; set; }
         
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
