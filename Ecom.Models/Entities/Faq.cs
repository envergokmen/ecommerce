﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Ecom.Models
{
    public class Faq : DbBaseObject    {

        public Faq()
        {
            this.FaqLocalLangs = new HashSet<FaqLocalLang>(); 
        }
        
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        
        public string Question { get; set; }

        public string Answer { get; set; }


        [Required(ErrorMessage = null, ErrorMessageResourceName = "SortNoIsRequired")]
        public int SortNo { get; set; }
         
        public virtual ICollection<FaqLocalLang> FaqLocalLangs { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
