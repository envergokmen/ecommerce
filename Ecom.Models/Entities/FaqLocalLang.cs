﻿namespace Ecom.Models
{
    public class FaqLocalLang : DbBaseObject
    {
        public int ID { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public int SortNo { get; set; }

        public int LangID { get; set; }  
        public int FaqID { get; set; }
        
        public virtual Lang Lang { get; set; }
        public virtual Faq Faq { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
