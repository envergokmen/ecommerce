﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public class CatalogLocalLang : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(100)]
        
        public string Name { get; set; }

        [StringLength(260)]
        
        public string Title { get; set; }

        [Column(TypeName = "text")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [StringLength(800)]
        
        public string Summary { get; set; }

        [StringLength(160)]
        
        public string MetaDescription { get; set; }

        [StringLength(260)]
        
        public string MetaKeywords { get; set; }

        [StringLength(260)]
        
        public string CanonicalLink { get; set; }

        [StringLength(260)]
        
        public string Link { get; set; }

        [StringLength(260)]
        
        public string Url { get; set; }

        [StringLength(250)]
        
        public string WebImageUrl { get; set; }

        [StringLength(250)]
        
        public string MenuImageUrl { get; set; }

        [StringLength(250)]
        
        public string MobileImageUrl { get; set; }
         
        public int CatalogID { get; set; }
        public int LangID { get; set; }

        public virtual Lang Lang { get; set; }
        public virtual Catalog Catalog { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
