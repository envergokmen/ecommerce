﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Ecom.Models
{
    public class User  : DbBaseObject
    { 
        public User()
        { 
            //this.UserDetails = new HashSet<UserDetail>();
            this.UserInRoles = new HashSet<UserInRole>();
            this.StoreAccountUsers = new HashSet<StoreAccountUser>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(90, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax90")]
        
        public string Name { get; set; }

        [StringLength(90, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax90")]
        
        public string UserName { get; set; }
           
        [StringLength(150, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax150")]
        
        public string ShopName { get; set; }

        [StringLength(150, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax150")]
        
        public string ShopUrl { get; set; }

        [StringLength(50, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax50")]
        
        public string Password { get; set; }


        [StringLength(40, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax50")]
        public string PasswordResetToken { get; set; }
         
        public Nullable<DateTime> PasswordResetTokenExpDate { get; set; }

        public bool AllowMail { get; set; }

        public bool AllowSMS { get; set; }

        [Required(ErrorMessage = "Kayıt olabilmek için sözleşmeyi kabul etmelisiniz.")]
        public bool AllowTerms { get; set; }

        public Nullable<Gender> Gender { get; set; }

        public Nullable<DateTime> BirthDate { get; set; }

        [StringLength(100)]
        
        public string ImagePath { get; set; }

        [StringLength(40)]
        public string IP { get; set; }

        [StringLength(20)]
        public string Telephone { get; set; }
      
        public Nullable<int> CityID { get; set; }

        public DateTime? LastLoginDate { get; set; }


        public UserType UserType { get; set; }

        public virtual ICollection<UserInRole> UserInRoles { get; set; }
        public virtual ICollection<StoreAccountUser> StoreAccountUsers { get; set; }

        public virtual City City { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

        public int? DomainId { get; set; }
        public virtual Domain Domain { get; set; }
    }
     
}
