﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.Models
{
    public partial class ProductStyle : DbBaseObject
    {
        public int ID { get; set; }
        [StringLength(50)]
        
        public string Name { get; set; }

        [StringLength(50)]
        
        public string Title { get; set; }

        public Nullable<int> SortNo { get; set; }
        public bool HideOnFilter { get; set; }

        public Nullable<int> SizeGroupID { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}