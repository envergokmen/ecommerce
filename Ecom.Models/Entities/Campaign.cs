﻿namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    public partial class Campaign : DbBaseObject
    {
        public Campaign()
        {
            this.Coupons = new HashSet<Coupon>();
        }

        public int ID { get; set; }

        [StringLength(50)]
         
        public string Name { get; set; }
         
        public DateTime BeginDate { get; set; } 
        public DateTime EndDate { get; set; }
         
        public HesapLamaTuru CalculationType { get; set; }
        public bool AllowOnDiscountedProducts { get; set; }
        public bool AllowWithOtherCoupons { get; set; }
        public bool AllowWithOtherCampaigns { get; set; } 
        public bool AllowMultipleUse { get; set; }
        public bool AllowMultipleCouponPerUser { get; set; }

        public decimal Amount { get; set; } //kampanyanın vereceği çek tutarı
        public decimal AmountLimit { get; set; }

        public decimal? MaxAmountLimit { get; set; }
        public DateTime? CouponEndDate { get; set; }
        public Nullable<int> CouponAutoEndDate { get; set; }
        public decimal? DiscountLimit { get; set; }

        //Kupon ne kadarlık alışverişte oluşacak
        public decimal? CreationLimit { get; set; }
        //public string UserDomainNameLimit { get; set; }

        [StringLength(250)]
        public string ImagePath { get; set; }

        public KampanyaTuru CampaignType { get; set; }
        public bool ShowOnCampaigns { get; set; }

        [StringLength(4000)]
        
        public string CreditCardLimits { get; set; } //BIN limitleri

        //[Required(ErrorMessage = "Para birimi seçilmesi zorunlu.")]
        //[Range(1, 100000)]
        public int? PriceCurrencyID { get; set; }

        public Nullable<int> Priority { get; set; }

        public int? DomainId { get; set; }
        public int? LangId { get; set; }

        public virtual Domain Domain { get; set; }
        public virtual Lang Lang { get; set; }

        public virtual PriceCurrency PriceCurrency { get; set; }

        public virtual ICollection<Coupon> Coupons { get; set; }
        
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
