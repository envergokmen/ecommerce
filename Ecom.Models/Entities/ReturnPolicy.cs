namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class ReturnPolicy : DbBaseObject
    {

        public int ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        
        public string DetailHTML { get; set; }
          
        public Nullable<int> LangID { get; set; }  
        public virtual Lang Lang { get; set; }
        public int? StoreAccountID { get; set; }
        public int ReturnPolicyTypeID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
        public virtual ReturnPolicyType ReturnPolicyType { get; set; }

    }
}
