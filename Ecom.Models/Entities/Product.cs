﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ecom.Core;

namespace Ecom.Models
{
    public class Product : DbBaseObject    {

        public Product()
        {
            this.ProductDetails = new HashSet<ProductDetail>();
            this.ProductImages = new HashSet<ProductImage>();
            this.Stocks = new HashSet<Stock>();
            this.Prices = new HashSet<Price>();
        }
        
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }
         
        [Required(ErrorMessage = "Ürün adı yazılması zorunlu.")]
        [StringLength(150)]
        
        public string ProductName { get; set; }

        [StringLength(150)]
        public string SmallName { get; set; }

        [StringLength(40)]
        
        public string ProductCode { get; set; } 

        public int? BrandID { get; set; }
        public int? SeasonID { get; set; }
        public int? ReturnPolicyID { get; set; } 
        public int? CargoPolicyID { get; set; }
        public Nullable<int> ProductDepartmentID { get; set; }
        public Nullable<int> ProductStyleID { get; set; }
        public Nullable<Gender> Gender { get; set; } 
        public virtual Brand Brand { get; set; } 
        public virtual Season Season { get; set; }
        public virtual ProductDepartment ProductDepartment { get; set; }
        public virtual ProductStyle ProductStyle { get; set; }
        public virtual Policy ReturnPolicy { get; set; }
        public virtual Policy CargoPolicy { get; set; }
         
        public virtual ICollection<Price> Prices { get; set; }
        public virtual ICollection<Stock> Stocks { get; set; }
        public virtual ICollection<ProductDetail> ProductDetails { get; set; }
        public virtual ICollection<ProductImage> ProductImages { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
