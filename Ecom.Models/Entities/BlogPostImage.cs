namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class BlogPostImage : DbBaseObject
    {
        public int ID { get; set; }

        public int BlogPostID { get; set; }

        public Nullable<int> SortNo { get; set; }

        [StringLength(150)]
        //
        public string ImagePath { get; set; }

        [NotMapped]
        public string KucukImagePath
        {
            get
            {
                return ImagePath.Replace("_buyuk", "_kucuk");
            }

        }
         
        [NotMapped]
        public string DetayImagePath
        {
            get
            {
                return ImagePath.Replace("_buyuk", "_detay");
            }

        }

        public virtual BlogPost BlogPost { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
