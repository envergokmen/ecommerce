﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public partial class Photo : DbBaseObject
    {
        public Photo()
        {

        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
         
        [Required(ErrorMessage = null, ErrorMessageResourceName = "NameIsRequired")] 
        public int AlbumID { get; set; } 

        [Required(ErrorMessage = null, ErrorMessageResourceName = "ImagePathIsRequired")]
        [StringLength(150, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax150")]
        
        public string ImagePath { get; set; }

        public int SortNo { get; set; }

        public virtual Album Album { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }

}
