﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public partial class Season : DbBaseObject
    {
        public Season()
        {
            this.Products = new HashSet<Product>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
         
        [Required(ErrorMessage = null, ErrorMessageResourceName = "ImagePathIsRequired")]
        [StringLength(150, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax150")]
        
        public string Name { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "ImagePathIsRequired")]
        [StringLength(150, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax150")]
        
        public string ImagePath { get; set; }

        public int SortNo { get; set; }

        public virtual IEnumerable<Product> Products { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }

}
