namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;


    public partial class CargoPrice : DbBaseObject
    {
        public int ID { get; set; }

        [Required] 
        public decimal Amount { get; set; }
         
        public  Nullable<decimal> FreeShippingLimit { get; set; } 

        public int CargoID { get; set; }

        //Kargonun Bulundu�u Depo
        public Nullable<int> DepotID { get; set; }

        //Kargonun Gidece�i �lke
        [Required]
        public Nullable<int> CountryID { get; set; }

        [Required]
        public int PriceCurrencyID { get; set; }

        public Nullable<int> VAT { get; set; }

        public int? StoreAccountID { get; set; }
        public int? DomainID { get; set; }

        public virtual PriceCurrency PriceCurrency { get; set; }

        public virtual Cargo Cargo { get; set; }
         
        public virtual Country Country { get; set; }

        public virtual StoreAccount StoreAccount { get; set; }

        public virtual Domain Domain { get; set; }
    }
}
