namespace Ecom.Models
{
    using Ecom.Core;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class ReturnPolicyType :DbBaseObject
    {
        public ReturnPolicyType()
        {
            this.ReturnPolicies = new HashSet<ReturnPolicy>();
        }
    
        public int ID { get; set; }

        public int? SortNo { get; set; }

        [Required(ErrorMessage = "�sim yaz�lmas� zorunlu.")]
        [StringLength(35)]
        
        public string Name { get; set; }

        public YesNo DefaultReturnPolicy { get; set; }
    
        public virtual ICollection<ReturnPolicy> ReturnPolicies { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
