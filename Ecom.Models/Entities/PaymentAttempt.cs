﻿namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class PaymentAttempt : DbBaseObject
    {
        public PaymentAttempt()
        {
           // this.Orders = new HashSet<Order>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }

        [StringLength(40)]
        
        public string TempSession { get; set; }

        public Nullable<int> ComplateStatus { get; set; }

        public Nullable<int> UserID { get; set; }
        public virtual User User { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
