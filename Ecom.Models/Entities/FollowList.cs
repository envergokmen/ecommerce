﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public partial class FollowList : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }

        public Nullable<int> UserID { get; set; }
        public Nullable<int> ColorID { get; set; }
        public Nullable<long> ProductID { get; set; }

        [StringLength(50)]
        
        public string ProductCode { get; set; }

        [StringLength(70)]
        
        public string Barkod { get; set; }


        [StringLength(70)]
        
        public string YapCode { get; set; }

        public Nullable<long> StockID { get; set; }

        [StringLength(200)]
        
        public string Name { get; set; }

        [StringLength(200)]
        
        public string ImagePath { get; set; }

        [StringLength(50)]
        
        public string Session { get; set; }

        [StringLength(500)]
        
        public string UserAgent { get; set; }


        [StringLength(40)]
        
        public string IP { get; set; }

        //public Nullable<int> DomainID { get; set; }

        public virtual User User { get; set; }
        public virtual Stock Stock { get; set; }
        public virtual Product Product { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
