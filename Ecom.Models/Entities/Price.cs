namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Price : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }

        [Required] 
        public decimal Amount { get; set; }
        public decimal AmountOld { get; set; }
        public long ProductID { get; set; }
        public Nullable<long> StockID { get; set; }

        [Required]
        public int PriceCurrencyID { get; set; }
        public decimal VAT { get; set; }
        public int? DomainID { get; set; }
        public int? StoreAccountID { get; set; }

        [StringLength(40)]
        public string ProductCode { get; set; }

        [StringLength(40)]
        public string Barkod { get; set; }

        public virtual PriceCurrency PriceCurrency { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
        public virtual Domain Domain { get; set; }
        public virtual Product Product { get; set; }
        public virtual Stock Stock { get; set; }
    }
}
