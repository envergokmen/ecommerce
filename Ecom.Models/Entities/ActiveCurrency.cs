﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.Models
{
    public class ActiveCurrency
    {
        public int ID { get; set; }
        public int StoreAccountId { get; set; }
        public int? DomainID { get; set; }
        //public int? PriceCurrencyID { get; set; }

        public virtual Domain Domain { get; set; }
        //public virtual PriceCurrency PriceCurrency { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
