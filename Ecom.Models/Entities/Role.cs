﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public class Role : DbBaseObject
    { 
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "NameIsRequired")]
        [StringLength(50, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax50")]
        
        public string Name { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
