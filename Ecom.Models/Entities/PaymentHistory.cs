﻿using Ecom.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace Ecom.Models
{

    public class PaymentHistory : DbBaseObject
    {
        public long ID { get; set; }

        public Nullable<decimal> Amount { get; set; }
        public Nullable<DateTime> PaymentDate { get; set; }

        
        public string Description { get; set; }

        
        public string Detail { get; set; }

        public Nullable<long> OrderID { get; set; }

        [StringLength(40)]
        
        public string TrackCode { get; set; }

        public Nullable<int> UserID { get; set; }

        [StringLength(40)]
        
        public string IP { get; set; }

        
        public string UserArgent { get; set; }

        public PaymentType PaymentType { get; set; }
        public Nullable<PaymentLogType> PaymentLogType { get; set; }
        public PaymentComplateStaus PaymentComplateStatus { get; set; }

        [StringLength(20)]
        
        public string CardNumber { get; set; }

        public Nullable<int> ExpiresMonth { get; set; }
        public Nullable<int> ExpiresYear { get; set; }

        [StringLength(50)]
        
        public string CardOwner { get; set; }

        public Nullable<int> CardCvc { get; set; }

        public Nullable<int> WebPosID { get; set; }
        public Nullable<int> CardType { get; set; } // 01 VISA - 02 MASTER CARD - 03 AMERICAN EXPRESS

        public Nullable<int> WebPosInstallmentID { get; set; }
        public Nullable<int> PriceCurrencyID { get; set; }

        public virtual WebPos WebPos { get; set; }
         
        public virtual WebPosInstallment WebPosInstallment { get; set; }

        public Nullable<int> BankCurrencyNo { get; set; }
         
        public virtual Order Order { get; set; }
         
        public virtual User User { get; set; }

        public virtual PriceCurrency PriceCurrency { get; set; }

        [StringLength(900)]
        
        public string Retval { get; set; }

        [StringLength(910)]
        
        public string Err { get; set; }

        [StringLength(3500)]
        
        public string ErrMsg { get; set; }

        [StringLength(950)]
        
        public string PayCode { get; set; }

        [StringLength(4000)]
        
        public string ExtraHostMsg { get; set; }

        [StringLength(900)]
        
        public string PayRefNo { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}

