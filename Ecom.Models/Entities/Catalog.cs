﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ecom.Core;

namespace Ecom.Models
{
    public class Catalog : DbBaseObject
    {

        public Catalog()
        {
            this.CatalogLocalLangs = new HashSet<CatalogLocalLang>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
          
        public string FilterGroups { get; set; }
        public int? ParentID { get; set; }

        public int ItemCount { get; set; }

        public int SortNo { get; set; }

        public bool ShowInFooter { get; set; }
        public bool ShowInHeader { get; set; }
        public CatalogType CatalogType { get; set; }
        public string Name { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
         
        public virtual ICollection<CatalogLocalLang> CatalogLocalLangs { get; set; }
    }
}
