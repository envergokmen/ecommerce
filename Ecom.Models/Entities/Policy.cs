namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class PolicyLocalLang : DbBaseObject
    {
        public int ID { get; set; }
         
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        
        public string DetailHTML { get; set; }
    
        [Range(1, 100000)] 
        public Nullable<int> LangID { get; set; }
        public int PolicyID { get; set; }

        public int MyProperty { get; set; }
        public virtual Lang Lang { get; set; }
        public virtual Policy Policy { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
