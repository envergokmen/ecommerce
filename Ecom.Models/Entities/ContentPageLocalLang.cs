﻿using System.ComponentModel.DataAnnotations;

namespace Ecom.Models
{
    public class ContentPageLocalLang : DbBaseObject
    {
        public int ID { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "NameIsRequired")]
        [StringLength(250, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax250")]
        
        public string Name { get; set; }
 
        [StringLength(3000, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax3000")]
        
        public string DetailHTML { get; set; }
         
        public int LangID { get; set; }  
        public int ContentPageID { get; set; }

        [StringLength(260)]
        public string Title { get; set; }
         
        [StringLength(800)]

        public string Summary { get; set; }

        [StringLength(160)]

        public string MetaDescription { get; set; }

        [StringLength(260)]

        public string MetaKeywords { get; set; }

        [StringLength(260)]

        public string CanonicalLink { get; set; }

        [StringLength(260)]
        public string Url { get; set; }

        public virtual Lang Lang { get; set; }
        public virtual ContentPage ContentPage { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }


    }
}
