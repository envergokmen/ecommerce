﻿using Ecom.Core;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{

    public class WebPosInstallment :DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int WebPosID { get; set; }
        public int Quantity { get; set; }
        public decimal Rate { get; set; }

        public decimal ExtraAmount { get; set; }

        public Nullable<decimal> UpLimit { get; set; }
        public Nullable<decimal> DownLimit { get; set; }

        //taksit erteleme
        public int DelayedMouth { get; set; }

        //ilave taksit
        public decimal PlusIntallmentAmount { get; set; }
        public decimal PlusInstallmentLimit { get; set; }


        public Nullable<int> SortNo { get; set; } 
         
        public virtual WebPos WebPos { get; set; }

        public Nullable<Durum> NewPStatus { get; set; }
        public Nullable<short> RepPStatus { get; set; }
        public Nullable<int> RepSortNo { get; set; }

        public bool IsDefault { get; set; }

        [StringLength(80)]
        
        public string Title { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}