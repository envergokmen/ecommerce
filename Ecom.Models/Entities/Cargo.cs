﻿namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    public partial class Cargo : DbBaseObject
    {
        public Cargo()
        {
            this.Orders = new HashSet<Order>();
            this.CargoPrices = new HashSet<CargoPrice>();
        }
    
        public int ID { get; set; }

        [StringLength(30)]
        
        [Required(ErrorMessage="İsim gereklidir")]
        public string Name { get; set; }

        [StringLength(100)]
        
        public string LogoPath { get; set; }

        //[StringLength(200)]
        //public string Url { get; set; }

        [StringLength(2000)]
        
        public string Notes { get; set; }
         
        [StringLength(50)]
        
        public string TaxNumber { get; set; }

        public int SortNo { get; set; }
        //public Nullable<decimal> FreeShippingLimit { get; set; }
        //public Nullable<decimal> Amount { get; set; }

        public bool IsUseWebServis { get; set; }

        [StringLength(25)]
        
        public string WebServisUserName { get; set; }

        [StringLength(25)]
        
        public string WebServisUserPassword { get; set; }

        [StringLength(25)]
        
        public string WebServisCustomerNumber { get; set; }

        [StringLength(125)]
        
        public string WebServisShipperName { get; set; }

        [StringLength(250)]
        
        public string WebServisShipperAddress { get; set; }

        [StringLength(20)]
        
        public string ShipperPhoneNumber { get; set; }

        public int WebServisShipperCityCode { get; set; } 
        public int WebServisShipperAreaCode { get; set; }
        public CargoCompanyType CargoCompany { get; set; }
        public int? StoreAccountID { get; set; }
        public int? DomainID { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<CargoPrice> CargoPrices { get; set; }

        public virtual StoreAccount StoreAccount { get; set; }
        public virtual Domain Domain { get; set; }

    }
}
