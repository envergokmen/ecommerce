﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.Models
{
    public class ActivePayment
    {
        public int ID { get; set; }
        public int StoreAccountId { get; set; }
        public int? DomainID { get; set; }
        public int PaymentID { get; set; }

        public virtual Domain Domain { get; set; }
        public virtual Payment Cargo { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
