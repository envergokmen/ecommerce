﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.Models
{
    public class ActiveCargo
    {
        public int ID { get; set; }
        public int StoreAccountId { get; set; }
        public int? DomainID { get; set; }
        public int? CargoID { get; set; }

        public virtual Domain Domain { get; set; }
        public virtual Cargo Cargo { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
