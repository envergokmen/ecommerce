namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class MoneyTransferInfo : DbBaseObject
    {

        public int ID { get; set; }
        public long OrderID { get; set; } 
        public Nullable<int> UserID { get; set; }
        public int BankID { get; set; }
 
        [StringLength(200)]
        
        public string SenderName { get; set; }

        [StringLength(250)]
        
        public string Note { get; set; }

        public Nullable<decimal> Amount { get; set; }
        //public Nullable<int> PriceCurrencyID { get; set; }
        public Nullable<bool> IsApproved { get; set; } // havale bildirimi onayland� m� ?
 
        public virtual Order Order { get; set; }  
        public virtual User User { get; set; }
        public virtual Bank Bank { get; set; }
        //public virtual PriceCurrency PriceCurrency { get; set; }


        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
