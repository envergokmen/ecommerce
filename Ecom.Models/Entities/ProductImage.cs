﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public partial class ProductImage : DbBaseObject
    {
        public ProductImage()
        {

        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
         
        [Required(ErrorMessage = null, ErrorMessageResourceName = "NameIsRequired")] 
        public long ProductID { get; set; } 

        [Required(ErrorMessage = null, ErrorMessageResourceName = "ImagePathIsRequired")]
        [StringLength(150, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax150")]
        
        public string ImagePath { get; set; }

        public int SortNo { get; set; }
        public int Angle { get; set; }
        public int? ColorID { get; set; }

        public virtual Product Product { get; set; }
        public virtual Color Color { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }

}
