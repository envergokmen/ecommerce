﻿namespace Ecom.Models
{
    using Ecom.Core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class BlogPost : DbBaseObject
    {
        public BlogPost()
        {
            this.BlogPostImages = new HashSet<BlogPostImage>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }


        [Required(ErrorMessage = "Lütfen bir başlık giriniz.")]
        [StringLength(100)]
        
        public string Name { get; set; }

        [StringLength(150)]
        
        public string Url { get; set; }
         
        public string DescriptionHTML { get; set; }
         
        public string Summary { get; set; }

        [Required(ErrorMessage = "Dil seçilmesi zorunlu.")]
        [Range(1, 100000)]
        public Nullable<int> LangID { get; set; }
 
        [StringLength(100)]
        
        public string ImagePath { get; set; }

        [Required(ErrorMessage = "Lütfen bir kategori seçiniz.")]
        public int CatalogID { get; set; }
  
        public int ViewCount { get; set; }

        public BlogGosterimYeri ShowPlace { get; set; }
        public BlogLayoutType LayoutType { get; set; }
         
        [StringLength(250)]
        
        public string MetaTitle { get; set; }

        [StringLength(160)]
        
        public string MetaDescription { get; set; }

        [StringLength(260)]
        
        public string MetaKeywords { get; set; }
          
        public virtual Catalog Catalog { get; set; }
     
        public virtual Lang Lang { get; set; }
         
        public virtual ICollection<BlogPostImage> BlogPostImages { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
