﻿using Ecom.Core;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Ecom.Models
{
    public class ContentPage : DbBaseObject    {

        public ContentPage()
        {
            this.ContentPageLocalLangs = new HashSet<ContentPageLocalLang>(); 
        }
        
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }
         
        [StringLength(250)]
        public string ImagePath { get; set; }
         
        public int SortNo { get; set; }

        public ContentPageType ContentPageType { get; set; }
        public virtual ICollection<ContentPageLocalLang> ContentPageLocalLangs { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
