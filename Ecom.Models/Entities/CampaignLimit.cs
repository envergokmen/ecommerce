namespace Ecom.Models
{
    using System;

    public partial class CampaignLimit : DbBaseObject
    {
        public int ID { get; set; }
         
        public int CampaignID { get; set; }

        public Nullable<long> ProductID { get; set; }   
        public Nullable<int> CatalogID { get; set; }
        public Nullable<int> BrandID { get; set; }
        public Nullable<int> SeasonID { get; set; }

        public virtual Campaign Campaign { get; set; } 

        public virtual Catalog Catalog { get; set; } 
        public virtual Product Product { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual Season Season { get; set; }

        public bool Allow { get; set; }
        public bool DisAllow { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
