namespace Ecom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class ProductRate : DbBaseObject
    {
        public int ID { get; set; }
        public Nullable<long> ProductID { get; set; }

        [StringLength(40)]
        
        public string ProductCode { get; set; }

        [StringLength(40)]
        
        public string Barkod { get; set; }

        public Nullable<long> StockID { get; set; }
        public Nullable<int> UserID { get; set; }

        [StringLength(40)]
        
        public string IP { get; set; }
    
        public virtual Product Product { get; set; }
        public virtual Stock Stock { get; set; }
        public virtual User User { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
