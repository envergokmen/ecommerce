﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ecom.Models
{
    public class WebPosLog : DbBaseObject
    {
        public long ID { get; set; }

        public Nullable<double> TotalAmount { get; set; }

        public string Name { get; set; } 
        
        public string Description { get; set; }
        
        public string Detail { get; set; }

        public string TrackCode { get; set; }

        public long OrderID { get; set; }

        public Nullable<int> UserID { get; set; }
         
        [StringLength(40)]
        
        public string IP { get; set; }

        public virtual Order Order { get; set; }
        public virtual User User{ get; set; }
         
        [StringLength(900)]
        
        public string Retval { get; set; }

        [StringLength(910)]
        
        public string Err { get; set; }

        [StringLength(3500)]
        
        public string ErrMsg { get; set; }

        [StringLength(950)]
        
        public string PayCode { get; set; }

        [StringLength(4000)]
        
        public string ExtraHostMsg { get; set; }

        [StringLength(900)]
        
        public string PayRefNo { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}