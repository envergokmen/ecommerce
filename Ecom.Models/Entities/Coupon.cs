﻿using Ecom.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{

    public class Coupon : DbBaseObject
    {
        public Coupon()
        {
            //this.Orders = new HashSet<Order>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)] 
        public long ID { get; set; }

        [StringLength(50)]
        
        public string CouponCode { get; set; }

        [StringLength(150)]
        
        public string Description { get; set; }
         
        public Nullable<int> CampaignID { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> FromUserID { get; set; }

        public Nullable<int> FromOrderID { get; set; }

        public bool IsUsed { get; set; } 
        //Kampanya ile aynı - Aktarım İçin Kupon'daki baskın olmalı

        public Nullable<DateTime> BeginDate { get; set; }
        public Nullable<DateTime> EndDate { get; set; }
        public Nullable<DateTime> UsedDate { get; set; }

        public  Nullable<HesapLamaTuru> CalculationType { get; set; }
        public  Nullable<bool> AllowOnDiscountedProducts { get; set; }
        public  Nullable<bool> AllowMultipleUse { get; set; }
        public bool? AllowWithOtherCoupons { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> AmountLimit { get; set; }
        public Nullable<decimal> MaxAmountLimit { get; set; }
        public Nullable<decimal> DiscountLimit { get; set; }

        //Kupon ne kadarlık alışverişte oluşacak
        public Nullable<decimal> CreationLimit { get; set; }
        public string UserDomainNameLimit { get; set; }
         
        public Nullable<KampanyaTuru> CampaignType { get; set; }
        public int? DomainId { get; set; }

        [StringLength(50)]
        
        public string CreditCardLimits { get; set; }

        public int? PriceCurrencyID { get; set; }

        public virtual PriceCurrency PriceCurrency { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
         
        public virtual Campaign Campaign { get; set; }
         
        public virtual User User { get; set; }

        public virtual User FromUser { get; set; }
        public virtual Domain Domain { get; set; }

        [StringLength(200)]
        public string ReasonForGiving { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}