﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{

    public class UserDetail : DbBaseObject
    { 
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int UserID { get; set; }
        
        [StringLength(90, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax90")]
        
        public string ContactMail { get; set; }

        [StringLength(20, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax20")]
        
        public string Gsm { get; set; }

        public virtual User User { get; set; }

        public int StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
