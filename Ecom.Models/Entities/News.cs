﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public class New : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        
        public string Title { get; set; }

        [Column(TypeName = "TEXT")]
        public string Description { get; set; }

        
        public string ImagePath { get; set; }

        public DateTime Date { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
