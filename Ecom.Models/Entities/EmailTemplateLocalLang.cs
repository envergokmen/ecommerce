﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public partial class EmailTemplateLocalLang : DbBaseObject
    {

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "NameIsRequired")]
        [StringLength(80, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax80")]
        
        public string Name { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "DescriptionHTMLNameIsRequired")]
        
        public string DescriptionHTML { get; set; }
        
        [Required(ErrorMessage = null, ErrorMessageResourceName = "LanguageIsChooseRequired")]
        public int LangID { get; set; }

        public int EmailTemplateID { get; set; }

        public virtual EmailTemplate EmailTemplate { get; set; }
        public virtual Lang Lang { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }


    }
}
