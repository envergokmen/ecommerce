﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{
    public class ErrorLog : DbBaseObject
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string Session { get; set; }

        [StringLength(2000, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string RequestFormParams { get; set; }

        [StringLength(2000, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string RequestQueryStringParams { get; set; }

        [StringLength(2000, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string CookieParams { get; set; }

        [StringLength(2000, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string SessionParams { get; set; }

        [StringLength(350, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string URL { get; set; }

        [StringLength(350, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string UrlReferrer { get; set; }
         
        public int? LineNumber { get; set; }

        
        public string Message { get; set; }

        [StringLength(150, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string FileName { get; set; }

        
        public string Details { get; set; }

        
        public string InnerException { get; set; }

        [StringLength(10, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string MethodType { get; set; }

        public int? UserID { get; set; }

        public string AdSoyad { get; set; }

        public string UserName { get; set; }

        [StringLength(1000, ErrorMessage = null, ErrorMessageResourceName = "")]
        
        public string UserAgent { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "")]
        [StringLength(40)]
        public string IP { get; set; }

        
        public string ExtraMessage { get; set; }

        public virtual User User { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
