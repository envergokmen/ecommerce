﻿namespace Ecom.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    
    public partial class Stock :DbBaseObject
    {
        public Stock()
        {
            this.Prices = new HashSet<Price>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }
        
        public Nullable<long> ProductID { get; set; }

        [StringLength(40)]
        
        public string ProductCode { get; set; }

        public Nullable<int> ColorID { get; set; }
        public Nullable<int> SizeID { get; set; }

        [Required]
        [StringLength(50)]
        public string Barkod { get; set; }
         
        [StringLength(50)]
        public string YapCode { get; set; }

        public int Quantity { get; set; }
    
        public virtual Color Color { get; set; } 
      
        public virtual ICollection<Price> Prices { get; set; }
         
        public virtual Product Product { get; set; }
         
        public virtual Size Size { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
