﻿
using Ecom.Core;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Ecom.Models
{
    public partial class EmailTemplate : DbBaseObject
    {
        public EmailTemplate()
        {
            this.EmailTemplateLocalLangs = new HashSet<EmailTemplateLocalLang>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "NameIsRequired")]
        [StringLength(40, ErrorMessage = null, ErrorMessageResourceName = "LengthIsMax40")]
        
        public string Name { get; set; }
          
        public SendingContentType MailType { get; set; }

        public int DomainID { get; set; }

        public int EmailProviderID { get; set; }

        public virtual Domain Domain { get; set; }

        public virtual SendingProvider EmailProvider { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

        public virtual ICollection<EmailTemplateLocalLang> EmailTemplateLocalLangs { get; set; }

    }
      
}
