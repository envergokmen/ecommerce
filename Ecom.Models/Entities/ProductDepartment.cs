﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ecom.Models
{
    public partial class ProductDepartment : DbBaseObject
    {
        public int ID { get; set; }

        [StringLength(50)]
        
        public string Name { get; set; }

        [StringLength(50)]
        
        public string Title { get; set; }

        [StringLength(100)]
        
        public string SizeGroupName { get; set; }
         
        [StringLength(100)]
        
        public string SizeGroupValue { get; set; }
        public virtual ICollection<Product> Products { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}

