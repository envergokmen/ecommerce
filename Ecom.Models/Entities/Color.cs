namespace Ecom.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Color : DbBaseObject
    {
        public Color()
        { 
            this.Stocks = new HashSet<Stock>(); 
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        
        public string Name { get; set; }

        [StringLength(100)]
        
        public string ColorDescription { get; set; }

        [StringLength(15)]
        
        public string ColorCode { get; set; }

        [StringLength(150)]
        public string PatternPath { get; set; }

        [StringLength(7)]
        
        public string HexValue { get; set; }

        public int ColorGlobalID { get; set; }

        public int? SortNo { get; set; }

        public virtual ColorGlobal ColorGlobal { get; set; }

        public virtual ICollection<Stock> Stocks { get; set; }
        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

    }
}
