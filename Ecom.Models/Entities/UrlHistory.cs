namespace Ecom.Models
{
    using System.ComponentModel.DataAnnotations;

    public partial class UrlHistory : DbBaseObject
    {
        public UrlHistory()
        {

        }

        public int ID { get; set; }

        [StringLength(40)]
        
        public string Name { get; set; }

        [StringLength(150)]
        
        public string OldUrl { get; set; }

        [StringLength(150)]
        
        public string NewUrl { get; set; }

        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }
    }
}
