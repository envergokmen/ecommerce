﻿
using Ecom.Core;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecom.Models
{

    public class Banner : DbBaseObject
    {
 
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(250)]
        public string Link { get; set; }

        [StringLength(250)]
        public string ImagePath { get; set; }
        public int? LangID { get; set; }
        public int? DomainID { get; set; }

        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int SortNo { get; set; }

        public BannerPlace BannerShowPlace { get; set; }


        public int? StoreAccountID { get; set; }
        public virtual StoreAccount StoreAccount { get; set; }

        public virtual Lang Lang { get; set; }
        public virtual Domain Domain { get; set; }

    }

}

