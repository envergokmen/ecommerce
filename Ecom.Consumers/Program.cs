﻿using Ecom.Consumers.Parser;
using Ecom.Consumers.Tasks;
using Ecom.Core;
using Ecom.Models;
using Ecom.Services;
using Ecom.Services.Bus;
using Ecom.ViewModels.Prices;
using Ecom.ViewModels.Products;
using Ecom.ViewModels.Stocks;
using Ecom.ViewModels.Stores;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ecom.Consumers
{
    class Program
    {
        public static IServiceProvider _serviceProvider;
        public static string ElasticPath = "http://localhost:9200";
        //public static string ApplicationPath = "";
        public static string WebProjectPath = @"C:\PROJELER\ECommerce\Ecom.Web\";
        public static string WebProjectContentImagesPath = @"C:\PROJELER\ECommerce\Ecom.Web\wwwroot\ContentImages";

        static async Task Main(string[] args)
        {
            var connectionstring = "User ID=postgress;Password=123456;Server=localhost;Port=5432;Database=TESTDB;Integrated Security=true;Pooling=true;";

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            SetDependencyInjections(connectionstring);

            var storeService = _serviceProvider.GetService(typeof(StoreAccountService)) as StoreAccountService;

            Console.WriteLine("Hello this is the Receiver application!");

            var CURRENT_CLIENT = "DEFAULT_";
            var allqueueTypes = Enum.GetNames(typeof(QueueItemType));

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {

                var allQueueEnums = Enum.GetValues(typeof(QueueItemType));

                foreach (var queueType in allqueueTypes)
                {
                    channel.QueueDeclare(queue: CURRENT_CLIENT + queueType,
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                }

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var message = Encoding.UTF8.GetString(ea.Body);
                    var clientNameAndItemType = ea.RoutingKey.Split('_');
                    var StoreId = Convert.ToInt32(message.Split('_')[0] ?? "0");
                    var itemTypeStr = clientNameAndItemType[1];
                    message = message.Split('_')[1];

                    StoreAccountViewModel store = storeService.GetStoreById(Convert.ToInt32(StoreId));

                    QueueItemType itemType = Enum.Parse<QueueItemType>(itemTypeStr);

                    var messages = String.Format("[x] Received {0} {1} {2}", ea.RoutingKey, message, itemType);

                    Console.WriteLine(" [x] Received {0} {1} {2}", ea.RoutingKey, message, itemType);


                    //Start related task according to item type
                    switch (itemType)
                    {
                        case QueueItemType.NewItemHasBeenAddedToCatalog:

                            List<ProductInCatalogVMBasic> pincs = JsonConvert.DeserializeObject<List<ProductInCatalogVMBasic>>(message);

                            Task.Run(() =>
                              new ProductTask().HandleAddedProductToCatalog(pincs)
                            );

                            //var catalogID = Convert.ToInt32(message);

                            Task.Run(() =>
                              new CatalogTask().GenerateFilters(pincs)
                            );

                            break;

                        case QueueItemType.CatalogItemHasBeenRemoved:


                            List<ProductInCatalogVMBasic> pincsRemoved = JsonConvert.DeserializeObject<List<ProductInCatalogVMBasic>>(message);

                            Task.Run(() =>
                              new ProductTask().HandleAddedProductToCatalog(pincsRemoved)
                            );

                            //var catalogID = Convert.ToInt32(message);

                            Task.Run(() =>
                              new CatalogTask().GenerateFilters(pincsRemoved)
                            );


                            break;

                        case QueueItemType.ProductRemovedFromCatalogs:


                            List<ProductInCatalogVMBasic> pincsRemovedCatalogs = JsonConvert.DeserializeObject<List<ProductInCatalogVMBasic>>(message);

                            Task.Run(() =>
                              new ProductTask().HandleAddedProductToCatalog(pincsRemovedCatalogs)
                            );

                            //var catalogID = Convert.ToInt32(message);

                            Task.Run(() =>
                              new CatalogTask().GenerateFilters(pincsRemovedCatalogs)
                            );


                            break;

                        case QueueItemType.CatalogSortDeleted:


                            List<ProductInCatalogVMBasic> pincsRemovedCatalogsSort = JsonConvert.DeserializeObject<List<ProductInCatalogVMBasic>>(message);

                            Task.Run(() =>
                              new ProductTask().HandleAddedProductToCatalog(pincsRemovedCatalogsSort)
                            );

                            //var catalogID = Convert.ToInt32(message);

                            Task.Run(() =>
                              new CatalogTask().GenerateFilters(pincsRemovedCatalogsSort)
                            );


                            break;

                        case QueueItemType.ProductUploaded:


                           List<string> uploadedFiles = JsonConvert.DeserializeObject<List<string>>(message);

                            Task.Run(() =>
                              new ProductTranserTaks().HandleUploadedProducts(uploadedFiles, StoreId)
                            );

                            break;

                        case QueueItemType.CatalogCreated:
                            break;

                        case QueueItemType.UserRegistered:
                            break;
                        case QueueItemType.WelcomeMail:
                            break;
                        case QueueItemType.BrandCreated:
                            break;
                        case QueueItemType.BrandUpdated:
                            break;
                        case QueueItemType.BrandDeleted:
                            break;
                        case QueueItemType.StockUpdated:

                            var stock = JsonConvert.DeserializeObject<StockCreateEditVM>(message);

                            Task.Run(() =>
                              new ProductTask().HandleStockChange(stock.ID, StoreId)
                            );

                            break;

                        case QueueItemType.StockCreated:

                            var stockCreated = JsonConvert.DeserializeObject<StockCreateEditVM>(message);

                            Task.Run(() =>
                              new ProductTask().HandleStockChange(stockCreated.ID, StoreId)
                            );

                            break;

                        case QueueItemType.StockDeleted:

                            var stockId = JsonConvert.DeserializeObject<long>(message);
                            Task.Run(() =>
                              new ProductTask().HandleStockChange(stockId, StoreId)
                            );

                            break;
                        case QueueItemType.PriceUpdated:

                            var priceUpdate = JsonConvert.DeserializeObject<PriceCreateEditVM>(message);
                            Task.Run(() =>
                              new ProductTask().HandlePriceChange(priceUpdate.ID, StoreId)
                            );

                            break;

                        case QueueItemType.PriceCreated:

                            var priceCreatated = JsonConvert.DeserializeObject<PriceCreateEditVM>(message);
                            Task.Run(() =>
                              new ProductTask().HandlePriceChange(priceCreatated.ID, StoreId)
                            );

                            break;

                        case QueueItemType.PriceDeleted:

                            var deletedPriceId = JsonConvert.DeserializeObject<long>(message);
                            Task.Run(() =>
                              new ProductTask().HandlePriceChange(deletedPriceId, StoreId)
                            );

                            break;

                        case QueueItemType.NewOrderCreate:

                            break;

                        case QueueItemType.ProductDeleted:

                            var deletedId = JsonConvert.DeserializeObject<long>(message);

                            Task.Run(() =>
                              new ProductTask().HandleProductChange(deletedId, StoreId)
                            );

                            break;
                        case QueueItemType.ProductCreated:
                            break;
                        case QueueItemType.ProductUpdated:

                            ProductCreateEditVM product = JsonConvert.DeserializeObject<ProductCreateEditVM>(message);

                            Task.Run(() =>
                              new ProductTask().HandleProductChange(product.ID, product.StoreID)
                            );

                            break;

                        case QueueItemType.ProductImageCreated:

                            var imageCreated = JsonConvert.DeserializeObject<ProductImageVM>(message);

                            Task.Run(() =>
                              new ProductTask().HandleProductChange(imageCreated.ProductID ?? 0, StoreId)
                            );

                            break;

                        case QueueItemType.ProductImageDeleted:

                            var deletedImage = JsonConvert.DeserializeObject<ProductResultVM>(message);

                            Task.Run(() =>
                              new ProductTask().HandleProductChange(deletedImage.ProductId ?? 0, StoreId)
                            );

                            break;

                        case QueueItemType.ProductImageUpdated:

                            var imageUpdated = JsonConvert.DeserializeObject<ProductImageVM>(message);

                            Task.Run(() =>
                              new ProductTask().HandleProductChange(imageUpdated.ProductID ?? 0, StoreId)
                            );


                            break;

                        case QueueItemType.NewMultipleProduct:
                            break;
                        case QueueItemType.NewMultipleStock:
                            break;
                        case QueueItemType.UserProfileUpdate:
                            break;
                        case QueueItemType.CatalogOrderHasBeenChanged:
                            break;
                        default:
                            break;
                    }

                    Console.WriteLine(" [x] Received {0} {1} {2}", ea.RoutingKey, message, itemType);

                };

                foreach (var queueType in allqueueTypes)
                {

                    channel.BasicConsume(queue: CURRENT_CLIENT + queueType,
                                         autoAck: true,
                                         consumer: consumer);

                }

                //channel.BasicConsume(queue: CURRENT_CLIENT + "UserRegistered",
                //                     autoAck: true,
                //                     consumer: consumer);

                //channel.BasicConsume(queue: CURRENT_CLIENT + "WelcomeMail",
                //                    autoAck: true,
                //                    consumer: consumer);

                //channel.BasicConsume(queue: CURRENT_CLIENT + "BrandUpdated",
                //                autoAck: true,
                //                consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }

        private static void SetDependencyInjections(string connectionstring)
        {
            //setup our DI
            var serviceProvider = _serviceProvider = new ServiceCollection()

           .AddTransient<RabbitMQService, RabbitMQService>()
           .AddTransient<UserService, UserService>()
           .AddTransient<DomainService, DomainService>()
           .AddTransient<BrandService, BrandService>()
           .AddTransient<ServiceUtils, ServiceUtils>()
           .AddTransient<StoreAccountService, StoreAccountService>()
           .AddTransient<SizeService, SizeService>()
           .AddTransient<ColorService, ColorService>()
           .AddTransient<ColorGlobalService, ColorGlobalService>()
           .AddTransient<SeasonService, SeasonService>()
           .AddTransient<CatalogService, CatalogService>()
           .AddTransient<BannerService, BannerService>()
           .AddTransient<CampaignService, CampaignService>()
           .AddTransient<CouponService, CouponService>()
           .AddTransient<EmailTemplateService, EmailTemplateService>()
           .AddTransient<SendingProviderService, SendingProviderService>()
           .AddTransient<ProductService, ProductService>()
           .AddTransient<ProductDetailService, ProductDetailService>()
           .AddTransient<PolicyLocalLangService, PolicyLocalLangService>()
           .AddTransient<CatalogLocalLangService, CatalogLocalLangService>()
           .AddTransient<PolicyService, PolicyService>()
           .AddTransient<StockService, StockService>()
           .AddTransient<ContentPageService, ContentPageService>()
           .AddTransient<ContentPageLocalLangService, ContentPageLocalLangService>()
           .AddTransient<EmailTemplateLocalLangService, EmailTemplateLocalLangService>()
           .AddTransient<CargoService, CargoService>()
           .AddTransient<CargoPriceService, CargoPriceService>()
           .AddTransient<PriceService, PriceService>()
           .AddTransient<PaymentService, PaymentService>()
           .AddTransient<WebPosService, WebPosService>()
           .AddTransient<WebPosInstallmentService, WebPosInstallmentService>()
           .AddTransient<BankService, BankService>()
           .AddTransient<PriceCurrencyService, PriceCurrencyService>()
           .AddTransient<LangService, LangService>()
           .AddTransient<BankService, BankService>()
           .AddTransient<XLSXParser, XLSXParser>()
           .AddEntityFrameworkNpgsql().AddDbContext<EcomContext>()
           .AddLocalization(options => options.ResourcesPath = "Resources")
           .AddDbContext<EcomContext>(options =>
            options.UseNpgsql(connectionstring, o => o.MigrationsAssembly("Ecom.Database")), ServiceLifetime.Transient)

           .BuildServiceProvider();
        }
    }
}
