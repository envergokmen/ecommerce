﻿using Ecom.Core;
using Ecom.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Ecom.Consumers.Parser
{
    public class DataTableParser
    {

        public List<ProductTransferVM> ParseProducts(DataTable products, List<StockVM> allStocks)
        {
            List<ProductTransferVM> productList = new List<ProductTransferVM>();
            DataColumnCollection columns = products.Columns;

            foreach (DataRow item in products.Rows)
            {
                var genderValue = columns.Contains("GENDER") ? item["GENDER"].ToString() : "NonSpecified";
                var ProductCode = columns.Contains("PRODUCTCODE") && item["PRODUCTCODE"] != DBNull.Value ? item["PRODUCTCODE"].ToString() : "";

                if (!String.IsNullOrWhiteSpace(ProductCode))
                {
                    var curPrd = new ProductTransferVM
                    {
                        ProductCode = ProductCode,
                        ProductName = columns.Contains("PRODUCTNAME") ? item["PRODUCTNAME"].ToString() : "",
                        Status = columns.Contains("STATUS") && String.IsNullOrWhiteSpace(item["STATUS"].ToString()) ? Enum.Parse<Status>(item["STATUS"].ToString()) : Status.Active,
                        DepartmentName = columns.Contains("PRODUCT_FILTER_1") ? item["PRODUCT_FILTER_1"].ToString() : "",
                        StyleName = columns.Contains("PRODUCT_FILTER_2") ? item["PRODUCT_FILTER_2"].ToString() : "",
                        BrandName = columns.Contains("BRAND_NAME") ? item["BRAND_NAME"].ToString() : "",
                        SeasonName = columns.Contains("SEASON_NAME") ? item["SEASON_NAME"].ToString() : "",
                        CargoPolicyName = columns.Contains("CARGO_POLICY_NAME") ? item["CARGO_POLICY_NAME"].ToString() : "Default",
                        ReturnPolicyName = columns.Contains("RETURN_POLICY_NAME") ? item["RETURN_POLICY_NAME"].ToString() : "Default",
                        Gender = !String.IsNullOrWhiteSpace(genderValue) ? Enum.Parse<Gender>(genderValue) : null as Gender?
                    };

                    curPrd.Images = columns.Contains("ImapaPaths") ? item["ImapaPaths"].ToString().Split(',').Select(c => new ProductImageVM { ImagePath = c }).ToList() : new List<ProductImageVM>();
                    curPrd.Stocks = allStocks.Where(c => c.ProductCode == curPrd.ProductCode).ToList();

                    ParseLocals(columns, item, curPrd);
                    ParsePrices(columns, item, curPrd);
                    productList.Add(curPrd);
                }

            }

            return productList;
        }

        private  void ParsePrices(DataColumnCollection columns, DataRow item, ProductTransferVM curPrd)
        {
            var supportedCurrencies = new string[] { "EUR", "USD", "GBP", "TRY" };

            foreach (var cur in supportedCurrencies)
            {
                if (columns.Contains($"PRICE_{cur}"))
                {
                    var amounOld = columns.Contains($"PRICE_OLD_{cur}") ? Convert.ToDecimal(item[$"PRICE_OLD_{cur}"]) : 0;

                    curPrd.Prices.Add(new ViewModels.Prices.PriceVM
                    {
                        CurrencyCode = cur,
                        Amount = Convert.ToDecimal(item[$"PRICE_{cur}"]),
                        AmountOld = amounOld
                    });
                }
            }
        }

        private  void ParseLocals(DataColumnCollection columns, DataRow item, ProductTransferVM curPrd)
        {
            var supportedLangs = new string[] { "EN", "TR", "IT", "DE", "ES" };

            foreach (var lang in supportedLangs)
            {

                if (columns.Contains($"PRODUCTNAME_{lang}"))
                {
                    var ProductName = columns.Contains($"PRODUCTNAME_{lang}") ? Convert.ToString(item[$"PRODUCTNAME_{lang}"]) : "";
                    var MetaDescription = columns.Contains($"METADESC_{lang}") ? Convert.ToString(item[$"METADESC_{lang}"]) : "";
                    var DetailHTML = columns.Contains($"DETAIL_{lang}") ? Convert.ToString(item[$"DETAIL_{lang}"]) : "";
                    var Title = columns.Contains($"TITLE_{lang}") ? Convert.ToString(item[$"TITLE_{lang}"]) : "";
                    var Url = columns.Contains($"URL_{lang}") ? Convert.ToString(item[$"URL_{lang}"]) : "";
                    var Keywords = columns.Contains($"KEYWORDS_{lang}") ? Convert.ToString(item[$"KEYWORDS_{lang}"]) : "";

                    if (!String.IsNullOrWhiteSpace(ProductName))
                    {
                        curPrd.Locals.Add(new ProductLocalVM
                        {
                            LangCode = lang,
                            ProductName = ProductName,
                            MetaDescription = MetaDescription,
                            DetailHTML = DetailHTML,
                            Title = Title,
                            Url = Url,
                            MetaKeywords = Keywords,
                        });
                    }

                }
            }
        }

        public List<StockVM> ParseStocks(DataTable stocks)
        {
            List<StockVM> allStocks = new List<StockVM>();
            DataColumnCollection columns = stocks.Columns;
            if (!columns.Contains("BARCODE") || !columns.Contains("PRODUCT_CODE")) return allStocks;

            foreach (DataRow item in stocks.Rows)
            {

                var ProductCode = columns.Contains($"PRODUCT_CODE") ? Convert.ToString(item[$"PRODUCT_CODE"]) : "";
                var Barkod = columns.Contains($"BARCODE") ? Convert.ToString(item[$"BARCODE"]) : "";
                var YapCode = columns.Contains($"YAPCODE") ? Convert.ToString(item[$"YAPCODE"]) : "";
                var ColorName = columns.Contains($"COLOR_NAME") ? Convert.ToString(item[$"COLOR_NAME"]) : "";
                var ColorHex = columns.Contains($"COLOR_HEX") ? Convert.ToString(item[$"COLOR_HEX"]) : "";
                var MainColorName = columns.Contains($"MAIN_COLOR_NAME") ? Convert.ToString(item[$"MAIN_COLOR_NAME"]) : "";
                var MainColorHex = columns.Contains($"MAIN_COLOR_HEX") ? Convert.ToString(item[$"MAIN_COLOR_HEX"]) : "";
                var SizeName = columns.Contains($"SIZE_NAME") ? Convert.ToString(item[$"SIZE_NAME"]) : "";
                var Quantity = columns.Contains($"QUANTITY") ? Convert.ToString(item[$"QUANTITY"])?.Replace(".0","") : "0";
                if (String.IsNullOrWhiteSpace(Quantity)) Quantity = "0";

                if (!String.IsNullOrWhiteSpace(ProductCode) && !String.IsNullOrWhiteSpace(Barkod))
                {
                    allStocks.Add(new StockVM
                    {
                        ProductCode = ProductCode,
                        Barkod = Barkod,
                        YapCode = YapCode,
                        ColorName = ColorName,
                        ColorHex = ColorHex,
                        MainColorName = MainColorName,
                        MainColorHex = MainColorHex,
                        SizeName = SizeName,
                        Quantity = Convert.ToInt32(Quantity),
                    });
                }
               
            }

            return allStocks;
        }
    }
}
