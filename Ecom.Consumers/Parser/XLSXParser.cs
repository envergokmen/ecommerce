﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Ecom.Core;
using Ecom.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace Ecom.Consumers.Parser
{
    public class XLSXParser : IUploadedFileParser
    {
        DataTableParser dataTableParser = new DataTableParser();

        public List<ProductTransferVM> GetUploadedProducts(string filePath)
        {

            try
            {
                var products = ReadAsDataTable(filePath);
                var stocks = ReadAsDataTable(filePath, stocks: true);

                List<StockVM> allStocks = dataTableParser.ParseStocks(stocks);
                List<ProductTransferVM> productList = dataTableParser.ParseProducts(products, allStocks);

                return productList;

            }
            catch (Exception ex)
            {

                throw;
            }
             

        }

        public static DataTable ReadAsDataTable(string fileName, bool stocks = false)
        {
            DataTable dataTable = new DataTable();
            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = stocks ? sheets.Skip(1).First().Id.Value : sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    if (cell == null) break;
                    else
                    {
                        var cellValue = GetCellValue(spreadSheetDocument, cell);
                        if (cellValue != null && !String.IsNullOrWhiteSpace(cellValue))
                        {
                            dataTable.Columns.Add(cellValue);
                        }
                    }
                }

                foreach (Row row in rows)
                {
                    DataRow dataRow = dataTable.NewRow();
                    var total = dataTable.Columns.Count;

                    for (int i = 0; i < total; i++)
                    {
                        try
                        {
                            var curElement = row.Descendants<Cell>().ElementAt(i);
                            dataRow[i] = GetCellValue(spreadSheetDocument, curElement);
                        }
                        catch
                        {

                        }

                    }

                    dataTable.Rows.Add(dataRow);
                }

            }
            dataTable.Rows.RemoveAt(0);

            return dataTable;
        }

        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            if (cell == null || document == null || cell.CellValue == null) return null;

            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

    }
}
