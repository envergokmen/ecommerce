﻿using Ecom.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecom.Consumers.Parser
{
    public interface  IUploadedFileParser
    {
        List<ProductTransferVM> GetUploadedProducts(string filePath);
    }
}
