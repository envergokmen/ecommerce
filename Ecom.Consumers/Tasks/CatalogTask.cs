﻿using Ecom.Models;
using Ecom.Services;
using Ecom.ViewModels.Products;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecom.Consumers.Tasks
{
    public class CatalogTask
    {
        public async Task<bool> GenerateFilters(List<ProductInCatalogVMBasic> pincs)
        {
            if (pincs == null || pincs.Count == 0) return false;

            var productService = Program._serviceProvider.GetService(typeof(ProductService)) as ProductService;
            var priceCurrencyService = Program._serviceProvider.GetService(typeof(PriceCurrencyService)) as PriceCurrencyService;
            var langservice = Program._serviceProvider.GetService(typeof(LangService)) as LangService;

            var storeId = pincs.FirstOrDefault().StoreID;

            var priceCurrencies = priceCurrencyService.GetPriceCurrencyList(storeId);
            var langs = langservice.GetlangList(storeId);
            var catalogIDs = pincs.Select(c => c.CatalogID).Distinct().ToList();

            foreach (var catalogId in catalogIDs)
            {
                List<FilterGroup> filterGroups = new List<FilterGroup>();

                foreach (var cur in priceCurrencies)
                {
                    foreach (var lang in langs)
                    {
                        var filterItems = productService.GetFilterItems(storeId, cur.PriceCurrencyID, lang.ID, catalogId);
                        foreach (var fitem in filterItems)
                        {
                            fitem.LangId = lang.ID;
                            fitem.PriceCurrencyId = cur.PriceCurrencyID;
                        }

                        filterGroups.AddRange(filterItems);
                    }
                }

                using (var db = new EcomContext())
                {
                    var catalog = db.Catalogs.FirstOrDefault(c => c.ID == catalogId && c.StoreAccountID == storeId);
                    if (catalog != null)
                    {
                        catalog.FilterGroups = JsonConvert.SerializeObject(filterGroups);
                        db.SaveChanges();

                    }
                }
            }

            return await Task.FromResult(true);
        }
    }
}
