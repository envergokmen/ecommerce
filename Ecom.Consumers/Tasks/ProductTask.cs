﻿using Ecom.Models;
using Ecom.Services;
using Ecom.ViewModels.Prices;
using Ecom.ViewModels.Products;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecom.Consumers.Tasks
{
    public class ProductTask
    {
        public async Task<bool> HandleStockChange(long stockId, int storeId)
        {
            using (var db = new EcomContext())
            {
                var productID = db.Stocks.Where(c => c.ID == stockId && c.StoreAccountID == storeId).Select(w => w.ProductID).FirstOrDefault() ?? 0;
                return await HandleProductChange(productID, storeId);

            }
        }

        public async Task<bool> HandleProductImageChange(int imageId, int storeId)
        {
            using (var db = new EcomContext())
            {
                var productID = db.ProductImages.Where(c => c.ID == imageId && c.StoreAccountID == storeId).Select(w => w.ProductID).FirstOrDefault();
                return await HandleProductChange(productID, storeId);

            }
        }

        public async Task<bool> HandlePriceChange(long priceId, int storeId)
        {
            using (var db = new EcomContext())
            {
                var productID = db.Prices.Where(c => c.ID == priceId && c.StoreAccountID == storeId).Select(w => w.ProductID).FirstOrDefault();
                return await HandleProductChange(productID, storeId);

            }
        }

        public async Task<bool> HandleProductChange(long productId, int storeId)
        {
            if (productId == 0) return false;

            var productService = Program._serviceProvider.GetService(typeof(ProductService)) as ProductService;

            List<FilterGroup> filterGroups = new List<FilterGroup>();
            Dictionary<long, ProductXVM> products = new Dictionary<long, ProductXVM>();
            Dictionary<long, List<int>> sizes = new Dictionary<long, List<int>>();

            try
            {
                var pincs = productService.GetProductInCatalogBasic(productId, storeId);

                await new CatalogTask().GenerateFilters(pincs);
                ElasticClient client = CreateElasticClient();

                IndexCatalogProducts(productService, products, sizes, pincs, client);
                IndexUniqueProducts(productService, products, sizes, pincs, client);

                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private static void IndexUniqueProducts(ProductService productService, Dictionary<long, ProductXVM> products, Dictionary<long, List<int>> sizes, List<ProductInCatalogVMBasic> pincs, ElasticClient client)
        {
            var uniqueProducts = pincs.Select(c => new
            {
                ProductID = c.ProductID,
                ItemType = c.ItemType,
                StoreID = c.StoreID
            }).Distinct()
                .Select(c => new ProductInCatalogVMBasic
                {
                    ProductID = c.ProductID,
                    ItemType = c.ItemType,
                    StoreID = c.StoreID

                }).ToList();

            foreach (var pinc in uniqueProducts)
            {
                ProductXVM productToIndex = GetRelatedProduct(productService, products, sizes, pinc);

               SearchAndDeleteExistings(productToIndex, client, "uniqueproducts");

                productToIndex.CatalogID = null;
                productToIndex.ColorID = null;
                productToIndex.ColorGlobalID = null;

                if (pinc.ItemType != Core.QueueItemType.ProductRemovedFromCatalogs)
                {
                    var indexResponse = client.IndexMany(new List<ProductXVM> { productToIndex }, "uniqueproducts");
                }
            }
        }

        private static void IndexCatalogProducts(ProductService productService, Dictionary<long, ProductXVM> products, Dictionary<long, List<int>> sizes, List<ProductInCatalogVMBasic> pincs, ElasticClient client)
        {
            foreach (var pinc in pincs)
            {
                ProductXVM productToIndex = GetRelatedProduct(productService, products, sizes, pinc);

                MapProductColorsAndCatalog(pinc, productToIndex);

                SearchAndDeleteExistings(productToIndex, client, "products");

                if (productToIndex.TotalQuantity > 0)
                {
                    var indexResponse = client.IndexMany(new List<ProductXVM> { productToIndex }, "products");
                }

            }
        }

        public async Task<bool> HandleAddedProductToCatalog(List<ProductInCatalogVMBasic> pincs)
        {
            if (pincs == null || pincs.Count == 0) return false;

            var productService = Program._serviceProvider.GetService(typeof(ProductService)) as ProductService;

            List<FilterGroup> filterGroups = new List<FilterGroup>();

            Dictionary<long, ProductXVM> products = new Dictionary<long, ProductXVM>();
            Dictionary<long, List<int>> sizes = new Dictionary<long, List<int>>();

            try
            {
                ElasticClient client = CreateElasticClient();
                IndexCatalogProducts(productService, products, sizes, pincs, client);

                IndexUniqueProducts(productService, products, sizes, pincs, client);

                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private static void SearchAndDeleteExistings(ProductXVM productToIndex, ElasticClient client, string indexName)
        {
            var queries = new List<Func<SearchDescriptor<ProductXVM>, QueryContainer>>();
            ISearchResponse<ProductXVM> searchResponse = null;
            QueryContainer query = indexName == "products" ? productsQuery(productToIndex) : UniqueProductsQuery(productToIndex);

            searchResponse = client.Search<ProductXVM>(s => s
                        .Index(indexName)
                            .Query(c => query)
                        );

            if (searchResponse.Documents != null && searchResponse.Documents.Count > 0)
            {
                var items = searchResponse.Hits.Select(hit =>
                {
                    var run = hit.Source;
                    run._id = hit.Id;
                    return run;
                }).ToList();

                foreach (var item in items)
                {
                    var deleteRequest = new DeleteRequest<ProductXVM>(new DocumentPath<ProductXVM>(item._id), index: indexName);
                    var deleteResult = client.Delete(deleteRequest);
                }
             
            }
        }

        private static QueryContainer UniqueProductsQuery(ProductXVM productToIndex)
        {
            return new QueryContainerDescriptor<ProductXVM>()
                                              .Match(m => m
                                                 .Field(f => f.ProductID)
                                                 .Query(productToIndex.ProductID.ToString())
                                             );
        }

        private static QueryContainer productsQuery(ProductXVM productToIndex)
        {

            return new QueryContainerDescriptor<ProductXVM>()
                 .Match(m => m
                    .Field(f => f.UniqueCode)
                    .Query(productToIndex.UniqueCode.ToString())
                );
        }

        private static void MapProductColorsAndCatalog(ProductInCatalogVMBasic pinc, ProductXVM productToIndex)
        {
            productToIndex.ColorID = pinc.ColorID;
            productToIndex.ColorGlobalID = pinc.ColorGlobalID;
            productToIndex.CatalogID = pinc.CatalogID;
            productToIndex.CatalogSortNo = pinc.SortNo;

            if (productToIndex.Sizes != null && productToIndex.AllStocks != null)
            {
                productToIndex.Sizes = productToIndex.AllStocks.Where(c => c.ColorID == productToIndex.ColorID && c.Quantity > 0).Select(c => c.SizeID ?? 0).Distinct().ToList();
            }
        }

        private static ProductXVM GetRelatedProduct(ProductService productService, Dictionary<long, ProductXVM> products, Dictionary<long, List<int>> sizes, ProductInCatalogVMBasic pinc)
        {
            ProductXVM productToIndex = null;

            if (products.ContainsKey(pinc.ProductID))
            {
                productToIndex = products[pinc.ProductID];
                productToIndex.Sizes = sizes[pinc.ProductID];

            }
            else
            {
                productToIndex = productService.GetProductForIndex(pinc.StoreID, pinc.ProductID);
                products.Add(pinc.ProductID, productToIndex);
                sizes.Add(pinc.ProductID, productToIndex?.Sizes);
            }

            return productToIndex;
        }

        private static ElasticClient CreateElasticClient()
        {
            var client = new ElasticClient(new ConnectionSettings(new Uri(Program.ElasticPath))
            .DefaultIndex("products")
            .DefaultMappingFor<ProductXVM>(m => m.IndexName("products")));

            CreateMappings(client);
            return client;
        }

        private static void CreateMappings(ElasticClient client)
        {
            var createIndexResponse = client.CreateIndex("products", c => c
                                   .Mappings(ms => ms

                                       .Map<ProductXVM>(m => m
                                           .AutoMap<ProductLocalVM>()
                                           .AutoMap<StockVM>()
                                           .AutoMap<ProductImageVM>()
                                           .AutoMap<PriceVM>()
                                           .AutoMap<ProductInCatalogVMBasic>()
                                           .AutoMap<SizeVM>()
                                       )
                                   )
                               );
        }
    }
}
