﻿using Ecom.Consumers.Parser;
using Ecom.Core;
using Ecom.Models;
using Ecom.Services;
using Ecom.ViewModels.Prices;
using Ecom.ViewModels.Products;
using Ecom.ViewModels.Stores;
using Microsoft.EntityFrameworkCore;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecom.Consumers.Tasks
{
    public class ProductTranserTaks
    {
        private readonly string rootPath = @"C:\PROJELER\ECommerce\Ecom.Web\wwwroot\";
        private readonly string adminRootPath = @"C:\PROJELER\ECommerce\Ecom.Admin\ClientApp\public";
        private readonly IUploadedFileParser XLSXParser;
        private readonly IUploadedFileParser Excel2007Parser;
        private readonly StoreAccountService storeAccountService;


        public ProductTranserTaks()
        {
            XLSXParser = new XLSXParser();
            Excel2007Parser = new Excel2007Parser();
            storeAccountService = Program._serviceProvider.GetService(typeof(StoreAccountService)) as StoreAccountService;
        }

        public async Task<bool> HandleUploadedProducts(List<string> uploadedFiles, int storeId)
        {

            foreach (var item in uploadedFiles)
            {
                var ext = Path.GetExtension(item);
                switch (ext)
                {
                    case ".xls": HandeExcelXlsProducts(item, storeId); break;
                    case ".ods": HandeExcelXlsProducts(item, storeId); break;
                    case ".sxc": HandeExcelXlsProducts(item, storeId); break;
                    case ".xlsx": HandleExcelXlsxProducts(item, storeId); break;
                    case ".json": HandleJsonProducts(item, storeId); break;
                }
            }

            return await Task.FromResult(true);

        }



        private void HandleJsonProducts(string item, int storeId)
        {
            ProcessProducts(JsonConvert.DeserializeObject<List<ProductTransferVM>>(File.ReadAllText(item)), storeId);
        }

        private void HandleExcelXlsxProducts(string item, int storeId)
        {
            var excelProducts = XLSXParser.GetUploadedProducts(item);
            ProcessProducts(excelProducts, storeId);
        }

        private void HandeExcelXlsProducts(string item, int storeId)
        {
            var excelProducts = Excel2007Parser.GetUploadedProducts(item);
            ProcessProducts(excelProducts, storeId);
        }

        private void ProcessProducts(List<ProductTransferVM> products, int storeId)
        {
            var store = storeAccountService.GetStoreById(storeId);

            if (products == null || products.Count == 0 || store == null) return;


            try
            {
                using (var db = new EcomContext())
                {
                    foreach (var prd in products)
                    {
                        var existingPrd = db.Products
                            .Include(c => c.ProductDetails)
                            .Include(c => c.Brand)
                            .Include(c => c.ProductDepartment)
                            .Include(c => c.ProductImages)
                            .Include(c => c.ProductStyle)
                            .Include(c => c.Stocks)
                            .Where(c => c.ProductCode == prd.ProductCode && c.StoreAccountID == storeId).FirstOrDefault();

                        if (existingPrd == null)
                        {
                            existingPrd = new Product
                            {
                                ProductCode = prd.ProductCode,
                                ProductName = prd.ProductName,
                                StoreAccountID = storeId,
                            };
                        }

                        existingPrd.PStatus = prd.Status;
                        existingPrd.ProductName = prd.ProductName;
                        existingPrd.Gender = prd.Gender;


                        MapProductDepartment(storeId, db, prd, existingPrd);
                        MapProductStyle(storeId, db, prd, existingPrd);
                        MapBrand(storeId, db, prd, existingPrd);
                        MapSeason(storeId, db, prd, existingPrd);
                        MapPolicy(storeId, db, prd, existingPrd, PoliciyType.CargoPolicy);
                        MapPolicy(storeId, db, prd, existingPrd, PoliciyType.ReturnPolicy);
                        MapProductLocalLangs(db, prd, existingPrd, storeId);
                        MapStocks(db, prd, existingPrd, storeId);
                        MapImages(db, prd, existingPrd, storeId, store);
                        MapPrices(db, prd, existingPrd, storeId);

                        if (existingPrd.ID == 0) db.Products.Add(existingPrd);

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }


        }


        private static void MapPolicy(int storeId, EcomContext db, ProductTransferVM prd, Product existingPrd, PoliciyType type)
        {
            if (type == PoliciyType.ReturnPolicy && (existingPrd.ReturnPolicyID == null || (existingPrd.ReturnPolicy != null && existingPrd.ReturnPolicy.Name != prd.ReturnPolicyName))
                ||
                type == PoliciyType.CargoPolicy && (existingPrd.CargoPolicyID == null || (existingPrd.CargoPolicy != null && existingPrd.CargoPolicy.Name != prd.CargoPolicyName))
                )
            {

                Models.Policy Policy = db.Policies.FirstOrDefault(c => c.Name == prd.ReturnPolicyName && c.StoreAccountID == storeId && c.PolicyType == type);
                if (Policy == null)
                {
                    Policy = new Models.Policy
                    {
                        Name = prd.ReturnPolicyName,
                        StoreAccountID = storeId,
                        PolicyType = type
                    };

                    if (type == PoliciyType.ReturnPolicy)
                    {
                        existingPrd.ReturnPolicy = Policy;
                    }

                    if (type == PoliciyType.CargoPolicy)
                    {
                        existingPrd.CargoPolicy = Policy;
                    }

                    db.Policies.Add(Policy);
                }
                else
                {
                    if (type == PoliciyType.ReturnPolicy && existingPrd.ReturnPolicyID != Policy.ID)
                    {
                        existingPrd.ReturnPolicyID = Policy.ID;
                    }

                    if (type == PoliciyType.CargoPolicy && existingPrd.CargoPolicyID != Policy.ID)
                    {
                        existingPrd.CargoPolicyID = Policy.ID;
                    }
                }
            }
        }

        private static void MapSeason(int storeId, EcomContext db, ProductTransferVM prd, Product existingPrd)
        {
            if (existingPrd.SeasonID == null || (existingPrd.Season != null && existingPrd.Season.Name != prd.SeasonName))
            {
                var Season = db.Seasons.FirstOrDefault(c => c.Name == prd.SeasonName && c.StoreAccountID == storeId);
                if (Season == null)
                {
                    Season = new Season
                    {
                        Name = prd.SeasonName,
                        StoreAccountID = storeId,
                        ImagePath = "",
                    };

                    existingPrd.Season = Season;
                    db.Seasons.Add(Season);
                }
                else
                {
                    if (existingPrd.SeasonID != Season.ID)
                    {
                        existingPrd.SeasonID = Season.ID;
                    }
                }
            }
        }

        private static void MapBrand(int storeId, EcomContext db, ProductTransferVM prd, Product existingPrd)
        {
            if (existingPrd.BrandID == null || (existingPrd.Brand != null && existingPrd.Brand.Name != prd.BrandName))
            {
                var Brand = db.Brands.FirstOrDefault(c => c.Name == prd.BrandName && c.StoreAccountID == storeId);
                if (Brand == null)
                {
                    Brand = new Brand
                    {
                        Name = prd.BrandName,
                        StoreAccountID = storeId,
                        ImagePath = ""
                    };

                    existingPrd.Brand = Brand;
                    db.Brands.Add(Brand);
                }
                else
                {
                    if (existingPrd.BrandID != Brand.ID)
                    {
                        existingPrd.BrandID = Brand.ID;
                    }
                }
            }
        }


        private static void MapProductDepartment(int storeId, EcomContext db, ProductTransferVM prd, Product existingPrd)
        {
            if (existingPrd.ProductDepartment == null || (existingPrd.ProductDepartment != null && existingPrd.ProductDepartment.Name != prd.DepartmentName))
            {
                var department = db.ProductDepartments.FirstOrDefault(c => c.Name == prd.DepartmentName && c.StoreAccountID == storeId);
                if (department == null)
                {
                    department = new ProductDepartment
                    {

                        Name = prd.DepartmentName,
                        StoreAccountID = storeId
                    };

                    existingPrd.ProductDepartment = department;
                    db.ProductDepartments.Add(department);
                }
                else
                {
                    if (existingPrd.ProductDepartmentID != department.ID)
                    {
                        existingPrd.ProductDepartmentID = department.ID;
                    }
                }
            }
        }


        private void MapProductStyle(int storeId, EcomContext db, ProductTransferVM prd, Product existingPrd)
        {
            if (existingPrd.ProductStyle == null || existingPrd.ProductStyle.Name != prd.StyleName)
            {
                var style = db.ProductStyles.FirstOrDefault(c => c.Name == prd.StyleName && c.StoreAccountID == storeId);
                if (style == null)
                {
                    style = new ProductStyle
                    {

                        Name = prd.StyleName,
                        StoreAccountID = storeId
                    };

                    existingPrd.ProductStyle = style;
                    db.ProductStyles.Add(style);
                }
                else
                {
                    if (existingPrd.ProductStyleID != style.ID)
                    {
                        existingPrd.ProductStyleID = style.ID;
                    }
                }
            }
        }

        //TODO:delete unexisted prices?
        private void MapImages(EcomContext db, ProductTransferVM prd, Product existingPrd, int storeId, StoreAccountViewModel store)
        {
            if (prd == null || prd.Stocks == null || prd.Stocks.Count == 0 || store == null) return;

            foreach (var st in prd.Images.Where(c => c.ImagePath != null && c.ImagePath.Length > 0))
            {
                var existingImage = db.ProductImages
                    .Include(s => s.Color)
                    .Where(c => c.ProductID == existingPrd.ID && c.StoreAccountID == storeId && c.ImagePath == st.ImagePath
                    ).FirstOrDefault();

                if (existingImage == null)
                {
                    existingImage = new ProductImage
                    {
                        PStatus = Core.Status.Active,
                        Product = existingPrd,
                        ImagePath = st.ImagePath,
                        StoreAccountID = storeId,
                    };
                }

                //if there is no colorname and colorglobal name on image json
                SetImageColorAndAnglesFromFilePath(existingPrd, st);

                //TODO:Check image color name and angle from path
                existingImage.Color = db.Colors.FirstOrDefault(c => c.Name == st.ColorName && c.StoreAccountID == storeId);

                if (existingImage.Color == null)
                {
                    var globalColor = db.ColorGlobals.FirstOrDefault(c => c.Name == st.ColorGlobalName && c.StoreAccountID == storeId);

                    if (globalColor == null)
                    {
                        globalColor = new ColorGlobal
                        {
                            Name = st.ColorGlobalName,
                            PStatus = Core.Status.Active,
                            StoreAccountID = storeId
                        };
                        db.ColorGlobals.Add(globalColor);
                    }


                    var color = new Color
                    {
                        Name = st.ColorName,
                        HexValue = st.ColorHex,
                        PStatus = Core.Status.Active,
                        ColorGlobal = globalColor,
                        StoreAccountID = storeId
                    };

                    existingImage.Color = color;
                    db.Colors.Add(color);
                }


                if (existingImage.ID == 0)
                {
                    try
                    {

                        existingImage.ImagePath = (!existingImage.ImagePath.StartsWith(@"/ContentImages/")) ? ServiceUtils.DownloadAndSaveImageFromUrl(existingImage.ImagePath, rootPath, Utils.FormatUrl(store.StoreName), "Products", Utils.FormatUrl(existingPrd.ProductCode)) : existingImage.ImagePath;

                        //copy to admin area to see image
                        var fileInfo = new FileInfo(Path.Combine(rootPath, existingImage.ImagePath.TrimStart('/').Replace(@"/", @"\\")));
                        if (fileInfo.Exists)
                        {
                            var folderInfo = new DirectoryInfo(Path.Combine(adminRootPath, Path.GetDirectoryName(existingImage.ImagePath.TrimStart('/').Replace(@"/", @"\\"))));
                            if (!folderInfo.Exists) folderInfo.Create();

                            var sourceImage = Path.Combine(rootPath, existingImage.ImagePath.TrimStart('/').Replace(@"/", @"\\"));
                            var adminImage = Path.Combine(adminRootPath, existingImage.ImagePath.TrimStart('/').Replace(@"/", @"\\"));

                            File.Copy(sourceImage, adminImage);

                        }
                    }
                    catch
                    {

                    }
                    finally
                    {

                    }

                    existingPrd.ProductImages.Add(existingImage);
                }

            }
        }


        private void SetImageColorAndAnglesFromFilePath(Product existingPrd, ProductImageVM st)
        {
            var items = st.ImagePath.Replace(existingPrd?.ProductCode ?? "", "").Replace(Path.GetExtension(st.ImagePath), "").Trim().Split('_').Where(x => x != "").ToArray();

            //BLACK_DEEP-BLACK_1
            SetColorAndGlobalAndAngle(st, items);

            //DEEP-BLACK_1
            SetColorAngleOnly(st, items);
        }

        private void SetColorAngleOnly(ProductImageVM img, string[] items)
        {
            if (items.Length == 2 && Utils.IsNumeric(items[1]))
            {
                if (String.IsNullOrWhiteSpace(img.ColorName)) img.ColorName = items[0];
                if (img.Angle == 0) img.Angle = Convert.ToInt16(items[1]);
            }
        }
        private void SetColorAndGlobalAndAngle(ProductImageVM img, string[] items)
        {

            if (items.Length == 3 && Utils.IsNumeric(items[2]))
            {
                if (String.IsNullOrWhiteSpace(img.ColorGlobalName)) img.ColorGlobalName = items[0];
                if (String.IsNullOrWhiteSpace(img.ColorName)) img.ColorName = items[1];
                if (img.Angle == 0) img.Angle = Convert.ToInt16(items[2]);

            }
        }

        //TODO:delete unexisted prices?
        private void MapStocks(EcomContext db, ProductTransferVM prd, Product existingPrd, int storeId)
        {
            if (prd == null || prd.Stocks == null || prd.Stocks.Count == 0) return;

            foreach (var st in prd.Stocks)
            {

                var existingStock = db.Stocks
                 .Include(s => s.Size)
                 .Include(s => s.Color)
                 .Where(c => c.Barkod == st.Barkod && c.ProductID == existingPrd.ID && c.StoreAccountID == storeId).FirstOrDefault();

                if (existingStock == null)
                {
                    existingStock = db.Stocks
                   .Include(s => s.Size)
                   .Include(s => s.Color)
                   .Where(c => c.Color != null && c.Color.Name == st.ColorName && c.Size != null && c.Size.Name == st.SizeName && c.ProductID == existingPrd.ID && c.StoreAccountID == storeId).FirstOrDefault();
                }

                if (existingStock == null)
                {
                    existingStock = new Stock
                    {
                        PStatus = Core.Status.Active,
                        Product = existingPrd,
                        StoreAccountID = storeId,
                        Barkod = st.Barkod
                    };
                }

                existingStock.Quantity = st.Quantity;

                var globalColor = db.ColorGlobals.FirstOrDefault(c => c.Name == st.MainColorName && c.StoreAccountID == storeId);
                existingStock.Size = db.Sizes.FirstOrDefault(c => c.Name == st.SizeName && c.StoreAccountID == storeId);
                existingStock.Color = db.Colors.FirstOrDefault(c => c.Name == st.ColorName && c.StoreAccountID == storeId);

                if (globalColor == null)
                {
                    globalColor = new ColorGlobal
                    {
                        Name = st.MainColorName,
                        HexValue = st.MainColorHex,
                        PStatus = Core.Status.Active,
                        StoreAccountID = storeId
                    };
                    db.ColorGlobals.Add(globalColor);
                }

                if (existingStock.Color == null)
                {
                    var color = new Color
                    {
                        Name = st.ColorName,
                        HexValue = st.ColorHex,
                        PStatus = Core.Status.Active,
                        StoreAccountID = storeId,
                        ColorGlobal = globalColor
                    };

                    existingStock.Color = color;
                    db.Colors.Add(color);
                }


                if (existingStock.Size == null)
                {
                    var size = new Size
                    {
                        Name = st.ColorName,
                        PStatus = Core.Status.Active,
                        StoreAccountID = storeId
                    };

                    existingStock.Size = size;
                    db.Sizes.Add(size);
                }

                existingStock.StoreAccountID = storeId;

                if (existingStock.ID == 0)
                {
                    existingPrd.Stocks.Add(existingStock);
                }

            }
        }

        //TODO:delete unexisted prices?
        private void MapPrices(EcomContext db, ProductTransferVM prd, Product existingPrd, int storeId)
        {
            if (prd == null || prd.Prices == null || prd.Prices.Count == 0) return;

            foreach (var pr in prd.Prices)
            {
                var existingPrice = db.Prices.FirstOrDefault(c => c.ProductID == existingPrd.ID && c.PriceCurrency.CurrencyCode == pr.CurrencyCode);

                if (existingPrice == null)
                {
                    existingPrice = new Price();
                }

                existingPrice.Amount = pr.Amount;
                existingPrice.AmountOld = pr.AmountOld;

                existingPrice.PriceCurrency = db.PriceCurrencies.FirstOrDefault(c => c.CurrencyCode == pr.CurrencyCode);

                if (existingPrice.PriceCurrency == null)
                {
                    var currency = new PriceCurrency
                    {
                        CurrencyCode = pr.CurrencyCode,
                        XMLCurrencyCode = pr.CurrencyCode
                    };
                    existingPrice.PriceCurrency = currency;
                    db.PriceCurrencies.Add(currency);
                };

                existingPrice.VAT = pr.VAT;
                existingPrice.StoreAccountID = storeId;

                if (existingPrice.ID == 0)
                {
                    existingPrd.Prices.Add(existingPrice);
                }

            }
        }
        private void MapProductLocalLangs(EcomContext db, ProductTransferVM prd, Product existingPrd, int storeId)
        {
            if (prd == null || prd.Locals == null || prd.Locals.Count == 0) return;

            foreach (var pd in prd.Locals)
            {
                var existingDetails = db.ProductDetails.FirstOrDefault(c => c.ProductID == existingPrd.ID && c.Lang != null && c.Lang.LangCode == pd.LangCode);

                if (existingDetails == null)
                {
                    existingDetails = new ProductDetail();
                }

                existingDetails.ProductName = pd.ProductName;
                existingDetails.Title = pd.Title;
                existingDetails.Summary = pd.Summary;
                existingDetails.DetailHTML = pd.DetailHTML;
                existingDetails.MetaDescription = pd.MetaDescription;
                existingDetails.MetaKeywords = pd.MetaKeywords;
                existingDetails.Url = pd.Url;

                if (String.IsNullOrWhiteSpace(existingDetails.Url) || pd.Url == "generate" || pd.Url == "auto")
                {
                    existingDetails.Url = "/" + Utils.FormatUrl(existingDetails.ProductName);
                }

                existingDetails.StoreAccountID = storeId;

                existingDetails.Lang = db.Langs.FirstOrDefault(c => c.LangCode == pd.LangCode);

                if (existingDetails.Lang == null)
                {
                    var lang = new Lang
                    {
                        LangCode = pd.LangCode,
                        Name = pd.LangCode,
                        //TODO:important add storeaccount or langsinstore table
                        //StoreAccountID = storeId
                    };
                    existingDetails.Lang = lang;
                    db.Langs.Add(lang);
                };

                if (existingDetails.ID == 0)
                {
                    existingPrd.ProductDetails.Add(existingDetails);
                }

            }
        }
    }
}
