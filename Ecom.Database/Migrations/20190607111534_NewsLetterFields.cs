﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ecom.Database.Migrations
{
    public partial class NewsLetterFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DomainID",
                table: "NewsLetters",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LangID",
                table: "NewsLetters",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NewsLetters_DomainID",
                table: "NewsLetters",
                column: "DomainID");

            migrationBuilder.CreateIndex(
                name: "IX_NewsLetters_LangID",
                table: "NewsLetters",
                column: "LangID");

            migrationBuilder.AddForeignKey(
                name: "FK_NewsLetters_Domains_DomainID",
                table: "NewsLetters",
                column: "DomainID",
                principalTable: "Domains",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NewsLetters_Langs_LangID",
                table: "NewsLetters",
                column: "LangID",
                principalTable: "Langs",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NewsLetters_Domains_DomainID",
                table: "NewsLetters");

            migrationBuilder.DropForeignKey(
                name: "FK_NewsLetters_Langs_LangID",
                table: "NewsLetters");

            migrationBuilder.DropIndex(
                name: "IX_NewsLetters_DomainID",
                table: "NewsLetters");

            migrationBuilder.DropIndex(
                name: "IX_NewsLetters_LangID",
                table: "NewsLetters");

            migrationBuilder.DropColumn(
                name: "DomainID",
                table: "NewsLetters");

            migrationBuilder.DropColumn(
                name: "LangID",
                table: "NewsLetters");
        }
    }
}
