﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ecom.Database.Migrations
{
    public partial class AddressForm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllowCampaignSms",
                table: "Addresses");

            migrationBuilder.AddColumn<string>(
                name: "Session",
                table: "Addresses",
                maxLength: 40,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Session",
                table: "Addresses");

            migrationBuilder.AddColumn<bool>(
                name: "AllowCampaignSms",
                table: "Addresses",
                nullable: false,
                defaultValue: false);
        }
    }
}
