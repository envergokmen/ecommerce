﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Ecom.Database.Migrations
{
    public partial class basketnames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlogCatalogs");

            migrationBuilder.AddColumn<string>(
                name: "ColorName",
                table: "Carts",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ColorName",
                table: "Carts");

            migrationBuilder.CreateTable(
                name: "BlogCatalogs",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CreatedById = table.Column<int>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    LangID = table.Column<int>(nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 160, nullable: true),
                    MetaKeywords = table.Column<string>(maxLength: 260, nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 250, nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    PStatus = table.Column<int>(nullable: false),
                    ParentID = table.Column<int>(nullable: true),
                    ShowInMenu = table.Column<bool>(nullable: false),
                    SortNo = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true),
                    Url = table.Column<string>(maxLength: 70, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogCatalogs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BlogCatalogs_BlogCatalogs_ParentID",
                        column: x => x.ParentID,
                        principalTable: "BlogCatalogs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogCatalogs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlogCatalogs_ParentID",
                table: "BlogCatalogs",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogCatalogs_StoreAccountID",
                table: "BlogCatalogs",
                column: "StoreAccountID");
        }
    }
}
