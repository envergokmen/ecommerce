﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Ecom.Database.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Langs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 20, nullable: false),
                    LangCode = table.Column<string>(maxLength: 5, nullable: false),
                    FlagPath = table.Column<string>(maxLength: 100, nullable: true),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Langs", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "PriceCurrencies",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 20, nullable: true),
                    CurrencyCode = table.Column<string>(maxLength: 10, nullable: true),
                    CurrencySymbol = table.Column<string>(maxLength: 10, nullable: true),
                    XMLCurrencyName = table.Column<string>(maxLength: 25, nullable: true),
                    XMLCurrencyCode = table.Column<string>(maxLength: 25, nullable: true),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceCurrencies", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "StoreAccounts",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    StoreName = table.Column<string>(maxLength: 50, nullable: true),
                    CompanyName = table.Column<string>(maxLength: 100, nullable: true),
                    CompanyTaxInfo = table.Column<string>(nullable: true),
                    CompanyTaxInfo2 = table.Column<string>(nullable: true),
                    CompanyEmail = table.Column<string>(maxLength: 100, nullable: true),
                    CompanyAddress = table.Column<string>(maxLength: 100, nullable: true),
                    CompanyPhone = table.Column<string>(maxLength: 100, nullable: true),
                    CountryId = table.Column<int>(nullable: true),
                    CityId = table.Column<int>(nullable: true),
                    TownId = table.Column<int>(nullable: true),
                    RelatedUserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreAccounts", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    GlobalName = table.Column<string>(maxLength: 100, nullable: true),
                    CountryCode = table.Column<string>(nullable: true),
                    DefaultPriceCurrencyID = table.Column<int>(nullable: true),
                    DefaultLangID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Countries_Langs_DefaultLangID",
                        column: x => x.DefaultLangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Countries_PriceCurrencies_DefaultPriceCurrencyID",
                        column: x => x.DefaultPriceCurrencyID,
                        principalTable: "PriceCurrencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    AlbumItemCount = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Albums_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BinNumbers",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BINNO = table.Column<int>(nullable: false),
                    BankCode = table.Column<int>(nullable: true),
                    OwnerBankCode = table.Column<int>(nullable: true),
                    WebPosID = table.Column<int>(nullable: true),
                    BankName = table.Column<string>(maxLength: 50, nullable: true),
                    CardName = table.Column<string>(maxLength: 50, nullable: true),
                    CardType = table.Column<string>(maxLength: 25, nullable: true),
                    SubCardType = table.Column<string>(maxLength: 25, nullable: true),
                    BankLogoPath = table.Column<string>(maxLength: 150, nullable: true),
                    CardLogoPath = table.Column<string>(maxLength: 150, nullable: true),
                    Name = table.Column<string>(maxLength: 40, nullable: true),
                    AllowInstallment = table.Column<bool>(nullable: false),
                    PrizeType = table.Column<string>(maxLength: 20, nullable: true),
                    IsCorporate = table.Column<bool>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BinNumbers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BinNumbers_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlogCatalogs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Url = table.Column<string>(maxLength: 70, nullable: true),
                    ShowInMenu = table.Column<bool>(nullable: false),
                    ParentID = table.Column<int>(nullable: true),
                    LangID = table.Column<int>(nullable: true),
                    SortNo = table.Column<int>(nullable: false),
                    MetaTitle = table.Column<string>(maxLength: 250, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 160, nullable: true),
                    MetaKeywords = table.Column<string>(maxLength: 260, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogCatalogs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BlogCatalogs_BlogCatalogs_ParentID",
                        column: x => x.ParentID,
                        principalTable: "BlogCatalogs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogCatalogs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Brands",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 150, nullable: false),
                    ImagePath = table.Column<string>(maxLength: 150, nullable: false),
                    SortNo = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brands", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Brands_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Catalogs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ParentID = table.Column<int>(nullable: true),
                    ItemCount = table.Column<int>(nullable: false),
                    SortNo = table.Column<int>(nullable: false),
                    ShowInFooter = table.Column<bool>(nullable: false),
                    ShowInHeader = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catalogs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Catalogs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ColorGlobals",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 20, nullable: true),
                    HexValue = table.Column<string>(maxLength: 7, nullable: true),
                    SortNo = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ColorGlobals", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ColorGlobals_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentPages",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 250, nullable: false),
                    ImagePath = table.Column<string>(maxLength: 250, nullable: true),
                    SortNo = table.Column<int>(nullable: false),
                    ContentPageType = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentPages", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ContentPages_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Domains",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Priority = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    PriceCurrencyID = table.Column<int>(nullable: true),
                    LangID = table.Column<int>(nullable: false),
                    DepotID = table.Column<int>(nullable: true),
                    DomainType = table.Column<int>(nullable: false),
                    DomainBrandName = table.Column<string>(maxLength: 50, nullable: true),
                    DomainSlogan = table.Column<string>(maxLength: 160, nullable: true),
                    AllowChangeLang = table.Column<bool>(nullable: false),
                    AllowChangeCurrency = table.Column<bool>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domains", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Domains_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Domains_PriceCurrencies_PriceCurrencyID",
                        column: x => x.PriceCurrencyID,
                        principalTable: "PriceCurrencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domains_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Faqs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Question = table.Column<string>(nullable: true),
                    Answer = table.Column<string>(nullable: true),
                    SortNo = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Faqs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Faqs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MetaRules",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    MetaTitle = table.Column<string>(maxLength: 1000, nullable: true),
                    MetaDescription = table.Column<string>(nullable: true),
                    Keywords = table.Column<string>(maxLength: 1000, nullable: true),
                    MetaRulePageType = table.Column<int>(nullable: false),
                    LangID = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetaRules", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MetaRules_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MetaRules_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "News",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(type: "TEXT", nullable: true),
                    ImagePath = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_News", x => x.ID);
                    table.ForeignKey(
                        name: "FK_News_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 40, nullable: true),
                    PaymentType = table.Column<int>(nullable: false),
                    PaymentKeyForOtherTypes = table.Column<string>(maxLength: 20, nullable: true),
                    LogoPath = table.Column<string>(maxLength: 100, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Payments_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Policies",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    SortNo = table.Column<int>(nullable: true),
                    Name = table.Column<string>(maxLength: 35, nullable: true),
                    DefaultPolicy = table.Column<int>(nullable: false),
                    PolicyType = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Policies", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Policies_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductDepartments",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 50, nullable: true),
                    SizeGroupName = table.Column<string>(maxLength: 100, nullable: true),
                    SizeGroupValue = table.Column<string>(maxLength: 100, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductDepartments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductDepartments_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductStyles",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 50, nullable: true),
                    SortNo = table.Column<int>(nullable: true),
                    HideOnFilter = table.Column<bool>(nullable: false),
                    SizeGroupID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductStyles", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductStyles_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Roles_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Seasons",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 150, nullable: false),
                    ImagePath = table.Column<string>(maxLength: 150, nullable: false),
                    SortNo = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seasons", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Seasons_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SendingProviders",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    SenderName = table.Column<string>(maxLength: 90, nullable: true),
                    UserName = table.Column<string>(maxLength: 90, nullable: true),
                    Password = table.Column<string>(maxLength: 20, nullable: true),
                    ProviderType = table.Column<int>(nullable: false),
                    SmptPort = table.Column<int>(nullable: false),
                    SmptHost = table.Column<string>(maxLength: 150, nullable: true),
                    ApiEndPoint = table.Column<string>(maxLength: 150, nullable: true),
                    EnableSsl = table.Column<bool>(nullable: false),
                    EuNameColumnName = table.Column<string>(maxLength: 25, nullable: true),
                    EuEmailColumnName = table.Column<string>(maxLength: 25, nullable: true),
                    EuroMessageFromName = table.Column<string>(maxLength: 60, nullable: true),
                    EuroMessageFromAddress = table.Column<string>(maxLength: 60, nullable: true),
                    EuroMessageReplyAddress = table.Column<string>(maxLength: 60, nullable: true),
                    EuroMessageApiUser = table.Column<string>(maxLength: 25, nullable: true),
                    EuroMessageApiPassword = table.Column<string>(maxLength: 25, nullable: true),
                    MailApiKey = table.Column<string>(nullable: true),
                    MailTransCode = table.Column<string>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SendingProviders", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SendingProviders_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sizes",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 30, nullable: true),
                    SizeCode = table.Column<string>(maxLength: 15, nullable: true),
                    SortNo = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sizes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Sizes_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UrlHistories",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 40, nullable: true),
                    OldUrl = table.Column<string>(maxLength: 150, nullable: true),
                    NewUrl = table.Column<string>(maxLength: 150, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UrlHistories", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UrlHistories_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GiftPrices",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    FreeGiftLimit = table.Column<decimal>(nullable: true),
                    GiftID = table.Column<int>(nullable: false),
                    CountryID = table.Column<int>(nullable: true),
                    VAT = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GiftPrices", x => x.ID);
                    table.ForeignKey(
                        name: "FK_GiftPrices_Countries_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Countries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GiftPrices_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "States",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 30, nullable: true),
                    CountryID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_States", x => x.ID);
                    table.ForeignKey(
                        name: "FK_States_Countries_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Countries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_States_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Photos",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AlbumID = table.Column<int>(nullable: false),
                    ImagePath = table.Column<string>(maxLength: 150, nullable: false),
                    SortNo = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photos", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Photos_Albums_AlbumID",
                        column: x => x.AlbumID,
                        principalTable: "Albums",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Photos_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlogPosts",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Url = table.Column<string>(maxLength: 150, nullable: true),
                    DescriptionHTML = table.Column<string>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    LangID = table.Column<int>(nullable: false),
                    ImagePath = table.Column<string>(maxLength: 100, nullable: true),
                    BlogCatalogID = table.Column<int>(nullable: false),
                    ViewCount = table.Column<int>(nullable: false),
                    ShowPlace = table.Column<int>(nullable: false),
                    LayoutType = table.Column<int>(nullable: false),
                    MetaTitle = table.Column<string>(maxLength: 250, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 160, nullable: true),
                    MetaKeywords = table.Column<string>(maxLength: 260, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogPosts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BlogPosts_BlogCatalogs_BlogCatalogID",
                        column: x => x.BlogCatalogID,
                        principalTable: "BlogCatalogs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogPosts_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogPosts_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CatalogLocalLangs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Title = table.Column<string>(maxLength: 260, nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Summary = table.Column<string>(maxLength: 800, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 160, nullable: true),
                    MetaKeywords = table.Column<string>(maxLength: 260, nullable: true),
                    CanonicalLink = table.Column<string>(maxLength: 260, nullable: true),
                    Link = table.Column<string>(maxLength: 260, nullable: true),
                    Url = table.Column<string>(maxLength: 260, nullable: true),
                    WebImageUrl = table.Column<string>(maxLength: 250, nullable: true),
                    MenuImageUrl = table.Column<string>(maxLength: 250, nullable: true),
                    MobileImageUrl = table.Column<string>(maxLength: 250, nullable: true),
                    CatalogID = table.Column<int>(nullable: false),
                    LangID = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CatalogLocalLangs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CatalogLocalLangs_Catalogs_CatalogID",
                        column: x => x.CatalogID,
                        principalTable: "Catalogs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CatalogLocalLangs_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CatalogLocalLangs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Colors",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    ColorDescription = table.Column<string>(maxLength: 100, nullable: true),
                    ColorCode = table.Column<string>(maxLength: 15, nullable: true),
                    PatternPath = table.Column<string>(maxLength: 150, nullable: true),
                    HexValue = table.Column<string>(maxLength: 7, nullable: true),
                    ColorGlobalID = table.Column<int>(nullable: false),
                    SortNo = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Colors", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Colors_ColorGlobals_ColorGlobalID",
                        column: x => x.ColorGlobalID,
                        principalTable: "ColorGlobals",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Colors_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentPageLocalLangs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 250, nullable: false),
                    DetailHTML = table.Column<string>(maxLength: 3000, nullable: true),
                    LangID = table.Column<int>(nullable: false),
                    ContentPageID = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 260, nullable: true),
                    Summary = table.Column<string>(maxLength: 800, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 160, nullable: true),
                    MetaKeywords = table.Column<string>(maxLength: 260, nullable: true),
                    CanonicalLink = table.Column<string>(maxLength: 260, nullable: true),
                    Url = table.Column<string>(maxLength: 260, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentPageLocalLangs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ContentPageLocalLangs_ContentPages_ContentPageID",
                        column: x => x.ContentPageID,
                        principalTable: "ContentPages",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentPageLocalLangs_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentPageLocalLangs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Banners",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Link = table.Column<string>(maxLength: 250, nullable: true),
                    ImagePath = table.Column<string>(maxLength: 250, nullable: true),
                    LangID = table.Column<int>(nullable: true),
                    DomainID = table.Column<int>(nullable: true),
                    BeginDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    SortNo = table.Column<int>(nullable: false),
                    BannerShowPlace = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banners", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Banners_Domains_DomainID",
                        column: x => x.DomainID,
                        principalTable: "Domains",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Banners_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Banners_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Campaigns",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    CalculationType = table.Column<int>(nullable: false),
                    AllowOnDiscountedProducts = table.Column<bool>(nullable: false),
                    AllowWithOtherCoupons = table.Column<bool>(nullable: false),
                    AllowWithOtherCampaigns = table.Column<bool>(nullable: false),
                    AllowMultipleUse = table.Column<bool>(nullable: false),
                    AllowMultipleCouponPerUser = table.Column<bool>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    AmountLimit = table.Column<decimal>(nullable: false),
                    MaxAmountLimit = table.Column<decimal>(nullable: false),
                    CouponEndDate = table.Column<DateTime>(nullable: false),
                    CouponAutoEndDate = table.Column<int>(nullable: true),
                    DiscountLimit = table.Column<decimal>(nullable: false),
                    CreationLimit = table.Column<decimal>(nullable: false),
                    ImagePath = table.Column<string>(maxLength: 250, nullable: true),
                    CampaignType = table.Column<int>(nullable: false),
                    ShowOnCampaigns = table.Column<bool>(nullable: false),
                    CreditCardLimits = table.Column<string>(maxLength: 4000, nullable: true),
                    PriceCurrencyID = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: true),
                    DomainId = table.Column<int>(nullable: true),
                    LangId = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaigns", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Campaigns_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Campaigns_Langs_LangId",
                        column: x => x.LangId,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Campaigns_PriceCurrencies_PriceCurrencyID",
                        column: x => x.PriceCurrencyID,
                        principalTable: "PriceCurrencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Campaigns_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cargoes",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    LogoPath = table.Column<string>(maxLength: 100, nullable: true),
                    Notes = table.Column<string>(maxLength: 2000, nullable: true),
                    TaxNumber = table.Column<string>(maxLength: 50, nullable: true),
                    SortNo = table.Column<int>(nullable: false),
                    IsUseWebServis = table.Column<bool>(nullable: false),
                    WebServisUserName = table.Column<string>(maxLength: 25, nullable: true),
                    WebServisUserPassword = table.Column<string>(maxLength: 25, nullable: true),
                    WebServisCustomerNumber = table.Column<string>(maxLength: 25, nullable: true),
                    WebServisShipperName = table.Column<string>(maxLength: 125, nullable: true),
                    WebServisShipperAddress = table.Column<string>(maxLength: 250, nullable: true),
                    ShipperPhoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    WebServisShipperCityCode = table.Column<int>(nullable: false),
                    WebServisShipperAreaCode = table.Column<int>(nullable: false),
                    CargoCompany = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true),
                    DomainID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cargoes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Cargoes_Domains_DomainID",
                        column: x => x.DomainID,
                        principalTable: "Domains",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cargoes_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FaqLocalLangs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Question = table.Column<string>(nullable: true),
                    Answer = table.Column<string>(nullable: true),
                    SortNo = table.Column<int>(nullable: false),
                    LangID = table.Column<int>(nullable: false),
                    FaqID = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaqLocalLangs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FaqLocalLangs_Faqs_FaqID",
                        column: x => x.FaqID,
                        principalTable: "Faqs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FaqLocalLangs_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FaqLocalLangs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Banks",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 40, nullable: true),
                    CorporateName = table.Column<string>(maxLength: 60, nullable: true),
                    CityName = table.Column<string>(maxLength: 40, nullable: true),
                    DepartmentAndNo = table.Column<string>(maxLength: 40, nullable: true),
                    AccountNo = table.Column<string>(maxLength: 25, nullable: true),
                    IBAN = table.Column<string>(maxLength: 40, nullable: true),
                    LogoPath = table.Column<string>(maxLength: 100, nullable: true),
                    PaymentID = table.Column<int>(nullable: true),
                    BankCode = table.Column<int>(nullable: true),
                    SortNo = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banks", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Banks_Payments_PaymentID",
                        column: x => x.PaymentID,
                        principalTable: "Payments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Banks_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebPoses",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 40, nullable: true),
                    BankCode = table.Column<int>(nullable: true),
                    PostUrl = table.Column<string>(maxLength: 100, nullable: true),
                    NormalPostUrl = table.Column<string>(maxLength: 100, nullable: true),
                    MagazaNo = table.Column<string>(maxLength: 20, nullable: true),
                    TerminalID = table.Column<string>(maxLength: 20, nullable: true),
                    SecureKey3D = table.Column<string>(maxLength: 50, nullable: true),
                    ApiUser = table.Column<string>(maxLength: 15, nullable: true),
                    ApiPassword = table.Column<string>(maxLength: 20, nullable: true),
                    ProvType = table.Column<string>(maxLength: 10, nullable: true),
                    PostNetNo = table.Column<string>(maxLength: 10, nullable: true),
                    LogoPath = table.Column<string>(maxLength: 100, nullable: true),
                    PaymentType = table.Column<int>(nullable: false),
                    SortNo = table.Column<int>(nullable: true),
                    PaymentID = table.Column<int>(nullable: true),
                    MountDelayCount = table.Column<int>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    EntegHesapKodu = table.Column<string>(maxLength: 20, nullable: true),
                    EntegHesapAdi = table.Column<string>(maxLength: 50, nullable: true),
                    EntegRefKodu = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebPoses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_WebPoses_Payments_PaymentID",
                        column: x => x.PaymentID,
                        principalTable: "Payments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebPoses_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PolicyLocalLangs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    DetailHTML = table.Column<string>(nullable: false),
                    LangID = table.Column<int>(nullable: true),
                    PolicyID = table.Column<int>(nullable: false),
                    MyProperty = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PolicyLocalLangs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PolicyLocalLangs_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PolicyLocalLangs_Policies_PolicyID",
                        column: x => x.PolicyID,
                        principalTable: "Policies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PolicyLocalLangs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ProductName = table.Column<string>(maxLength: 150, nullable: false),
                    SmallName = table.Column<string>(maxLength: 150, nullable: true),
                    ProductCode = table.Column<string>(maxLength: 40, nullable: true),
                    BrandID = table.Column<int>(nullable: true),
                    SeasonID = table.Column<int>(nullable: true),
                    ReturnPolicyID = table.Column<int>(nullable: true),
                    CargoPolicyID = table.Column<int>(nullable: true),
                    ProductDepartmentID = table.Column<int>(nullable: true),
                    ProductStyleID = table.Column<int>(nullable: true),
                    Gender = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Products_Brands_BrandID",
                        column: x => x.BrandID,
                        principalTable: "Brands",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_Policies_CargoPolicyID",
                        column: x => x.CargoPolicyID,
                        principalTable: "Policies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_ProductDepartments_ProductDepartmentID",
                        column: x => x.ProductDepartmentID,
                        principalTable: "ProductDepartments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_ProductStyles_ProductStyleID",
                        column: x => x.ProductStyleID,
                        principalTable: "ProductStyles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_Policies_ReturnPolicyID",
                        column: x => x.ReturnPolicyID,
                        principalTable: "Policies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_Seasons_SeasonID",
                        column: x => x.SeasonID,
                        principalTable: "Seasons",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailTemplates",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 40, nullable: false),
                    MailType = table.Column<int>(nullable: false),
                    DomainID = table.Column<int>(nullable: false),
                    EmailProviderID = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailTemplates", x => x.ID);
                    table.ForeignKey(
                        name: "FK_EmailTemplates_Domains_DomainID",
                        column: x => x.DomainID,
                        principalTable: "Domains",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmailTemplates_SendingProviders_EmailProviderID",
                        column: x => x.EmailProviderID,
                        principalTable: "SendingProviders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmailTemplates_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    CountryID = table.Column<int>(nullable: true),
                    StateID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Cities_Countries_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Countries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cities_States_StateID",
                        column: x => x.StateID,
                        principalTable: "States",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlogPostImages",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BlogPostID = table.Column<int>(nullable: false),
                    SortNo = table.Column<int>(nullable: true),
                    ImagePath = table.Column<string>(maxLength: 150, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogPostImages", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BlogPostImages_BlogPosts_BlogPostID",
                        column: x => x.BlogPostID,
                        principalTable: "BlogPosts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogPostImages_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CargoPrices",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    FreeShippingLimit = table.Column<decimal>(nullable: true),
                    CargoID = table.Column<int>(nullable: false),
                    DepotID = table.Column<int>(nullable: true),
                    CountryID = table.Column<int>(nullable: false),
                    PriceCurrencyID = table.Column<int>(nullable: false),
                    VAT = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true),
                    DomainID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CargoPrices", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CargoPrices_Cargoes_CargoID",
                        column: x => x.CargoID,
                        principalTable: "Cargoes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CargoPrices_Countries_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Countries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CargoPrices_Domains_DomainID",
                        column: x => x.DomainID,
                        principalTable: "Domains",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CargoPrices_PriceCurrencies_PriceCurrencyID",
                        column: x => x.PriceCurrencyID,
                        principalTable: "PriceCurrencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CargoPrices_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebPosInstallments",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    WebPosID = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    Rate = table.Column<double>(nullable: false),
                    ExtraAmount = table.Column<double>(nullable: false),
                    UpLimit = table.Column<double>(nullable: true),
                    DownLimit = table.Column<double>(nullable: true),
                    DelayedMouth = table.Column<double>(nullable: false),
                    PlusIntallmentAmount = table.Column<double>(nullable: false),
                    PlusInstallmentLimit = table.Column<double>(nullable: false),
                    SortNo = table.Column<int>(nullable: true),
                    NewPStatus = table.Column<int>(nullable: true),
                    RepPStatus = table.Column<short>(nullable: true),
                    RepSortNo = table.Column<int>(nullable: true),
                    IsDefault = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(maxLength: 80, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebPosInstallments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_WebPosInstallments_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebPosInstallments_WebPoses_WebPosID",
                        column: x => x.WebPosID,
                        principalTable: "WebPoses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CampaignLimits",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CampaignID = table.Column<int>(nullable: false),
                    ProductID = table.Column<long>(nullable: true),
                    CatalogID = table.Column<int>(nullable: true),
                    BrandID = table.Column<int>(nullable: true),
                    SeasonID = table.Column<int>(nullable: true),
                    Allow = table.Column<bool>(nullable: false),
                    DisAllow = table.Column<bool>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampaignLimits", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CampaignLimits_Brands_BrandID",
                        column: x => x.BrandID,
                        principalTable: "Brands",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CampaignLimits_Campaigns_CampaignID",
                        column: x => x.CampaignID,
                        principalTable: "Campaigns",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CampaignLimits_Catalogs_CatalogID",
                        column: x => x.CatalogID,
                        principalTable: "Catalogs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CampaignLimits_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CampaignLimits_Seasons_SeasonID",
                        column: x => x.SeasonID,
                        principalTable: "Seasons",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CampaignLimits_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductDetails",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ProductName = table.Column<string>(maxLength: 250, nullable: false),
                    Title = table.Column<string>(maxLength: 250, nullable: true),
                    Summary = table.Column<string>(maxLength: 800, nullable: true),
                    Url = table.Column<string>(maxLength: 150, nullable: true),
                    ProductID = table.Column<long>(nullable: true),
                    ProductCode = table.Column<string>(maxLength: 40, nullable: true),
                    CatalogTree = table.Column<string>(maxLength: 250, nullable: true),
                    BreadCrumbCatalogID = table.Column<int>(nullable: true),
                    DetailHTML = table.Column<string>(nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 250, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 200, nullable: true),
                    MetaKeywords = table.Column<string>(maxLength: 260, nullable: true),
                    CanonicalLink = table.Column<string>(maxLength: 260, nullable: true),
                    LangID = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductDetails_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductDetails_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductDetails_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductImages",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ProductID = table.Column<long>(nullable: false),
                    ImagePath = table.Column<string>(maxLength: 150, nullable: false),
                    SortNo = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductImages", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductImages_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductImages_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Stocks",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ProductID = table.Column<long>(nullable: true),
                    ProductCode = table.Column<string>(maxLength: 40, nullable: true),
                    ColorID = table.Column<int>(nullable: true),
                    SizeID = table.Column<int>(nullable: true),
                    Barkod = table.Column<string>(maxLength: 50, nullable: false),
                    YapCode = table.Column<string>(maxLength: 50, nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stocks", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Stocks_Colors_ColorID",
                        column: x => x.ColorID,
                        principalTable: "Colors",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Stocks_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Stocks_Sizes_SizeID",
                        column: x => x.SizeID,
                        principalTable: "Sizes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Stocks_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailTemplateLocalLangs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 80, nullable: false),
                    DescriptionHTML = table.Column<string>(nullable: false),
                    LangID = table.Column<int>(nullable: false),
                    EmailTemplateID = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailTemplateLocalLangs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_EmailTemplateLocalLangs_EmailTemplates_EmailTemplateID",
                        column: x => x.EmailTemplateID,
                        principalTable: "EmailTemplates",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmailTemplateLocalLangs_Langs_LangID",
                        column: x => x.LangID,
                        principalTable: "Langs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmailTemplateLocalLangs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Towns",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    CityID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Towns", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Towns_Cities_CityID",
                        column: x => x.CityID,
                        principalTable: "Cities",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 90, nullable: true),
                    UserName = table.Column<string>(maxLength: 90, nullable: true),
                    ShopName = table.Column<string>(maxLength: 150, nullable: true),
                    ShopUrl = table.Column<string>(maxLength: 150, nullable: true),
                    Password = table.Column<string>(maxLength: 50, nullable: true),
                    PasswordResetToken = table.Column<string>(maxLength: 40, nullable: true),
                    PasswordResetTokenExpDate = table.Column<DateTime>(nullable: true),
                    AllowMail = table.Column<bool>(nullable: false),
                    AllowTerms = table.Column<bool>(nullable: false),
                    Gender = table.Column<int>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: true),
                    ImagePath = table.Column<string>(maxLength: 100, nullable: true),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    Telephone = table.Column<string>(maxLength: 20, nullable: true),
                    CityID = table.Column<int>(nullable: true),
                    LastLoginDate = table.Column<DateTime>(nullable: true),
                    UserType = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true),
                    DomainId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Users_Cities_CityID",
                        column: x => x.CityID,
                        principalTable: "Cities",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Prices",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    AmountOld = table.Column<decimal>(nullable: false),
                    ProductID = table.Column<long>(nullable: false),
                    StockID = table.Column<long>(nullable: true),
                    PriceCurrencyID = table.Column<int>(nullable: false),
                    VAT = table.Column<decimal>(nullable: false),
                    DomainID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true),
                    ProductCode = table.Column<string>(maxLength: 40, nullable: true),
                    Barkod = table.Column<string>(maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prices", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Prices_Domains_DomainID",
                        column: x => x.DomainID,
                        principalTable: "Domains",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Prices_PriceCurrencies_PriceCurrencyID",
                        column: x => x.PriceCurrencyID,
                        principalTable: "PriceCurrencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Prices_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Prices_Stocks_StockID",
                        column: x => x.StockID,
                        principalTable: "Stocks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Prices_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductInCatalogs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CatalogID = table.Column<int>(nullable: true),
                    ProductID = table.Column<long>(nullable: true),
                    ProductCode = table.Column<string>(maxLength: 40, nullable: true),
                    Barkod = table.Column<string>(maxLength: 40, nullable: true),
                    IsAutoRemoved = table.Column<bool>(nullable: false),
                    StockID = table.Column<long>(nullable: true),
                    ColorID = table.Column<int>(nullable: true),
                    SortNo = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInCatalogs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductInCatalogs_Catalogs_CatalogID",
                        column: x => x.CatalogID,
                        principalTable: "Catalogs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductInCatalogs_Colors_ColorID",
                        column: x => x.ColorID,
                        principalTable: "Colors",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductInCatalogs_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductInCatalogs_Stocks_StockID",
                        column: x => x.StockID,
                        principalTable: "Stocks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductInCatalogs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    TownID = table.Column<int>(nullable: true),
                    IlceID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Districts_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Districts_Towns_TownID",
                        column: x => x.TownID,
                        principalTable: "Towns",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlogLikes",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    Email = table.Column<string>(maxLength: 90, nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    BlogPostID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogLikes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BlogLikes_BlogPosts_BlogPostID",
                        column: x => x.BlogPostID,
                        principalTable: "BlogPosts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogLikes_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogLikes_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    Name = table.Column<string>(maxLength: 70, nullable: true),
                    Email = table.Column<string>(maxLength: 90, nullable: true),
                    Message = table.Column<string>(maxLength: 500, nullable: false),
                    UserID = table.Column<int>(nullable: true),
                    BlogPostID = table.Column<int>(nullable: true),
                    ProductID = table.Column<long>(nullable: true),
                    ReplyID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Comments_BlogPosts_BlogPostID",
                        column: x => x.BlogPostID,
                        principalTable: "BlogPosts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactMessages",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 70, nullable: true),
                    Email = table.Column<string>(maxLength: 90, nullable: false),
                    Subject = table.Column<string>(maxLength: 40, nullable: false),
                    Message = table.Column<string>(maxLength: 500, nullable: false),
                    Telephone = table.Column<string>(maxLength: 20, nullable: true),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactMessages", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ContactMessages_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContactMessages_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Coupons",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CouponCode = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 150, nullable: true),
                    CampaignID = table.Column<int>(nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    FromUserID = table.Column<int>(nullable: true),
                    FromOrderID = table.Column<int>(nullable: true),
                    IsUsed = table.Column<bool>(nullable: false),
                    BeginDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    UsedDate = table.Column<DateTime>(nullable: true),
                    CalculationType = table.Column<int>(nullable: true),
                    AllowOnDiscountedProducts = table.Column<bool>(nullable: true),
                    AllowMultipleUse = table.Column<bool>(nullable: true),
                    AllowWithOtherCoupons = table.Column<bool>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    AmountLimit = table.Column<decimal>(nullable: true),
                    MaxAmountLimit = table.Column<decimal>(nullable: true),
                    DiscountLimit = table.Column<decimal>(nullable: true),
                    CreationLimit = table.Column<decimal>(nullable: true),
                    UserDomainNameLimit = table.Column<string>(nullable: true),
                    CampaignType = table.Column<int>(nullable: true),
                    DomainId = table.Column<int>(nullable: true),
                    CreditCardLimits = table.Column<string>(maxLength: 50, nullable: true),
                    PriceCurrencyID = table.Column<int>(nullable: false),
                    ReasonForGiving = table.Column<string>(maxLength: 200, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coupons", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Coupons_Campaigns_CampaignID",
                        column: x => x.CampaignID,
                        principalTable: "Campaigns",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Coupons_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Coupons_Users_FromUserID",
                        column: x => x.FromUserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Coupons_PriceCurrencies_PriceCurrencyID",
                        column: x => x.PriceCurrencyID,
                        principalTable: "PriceCurrencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Coupons_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Coupons_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ErrorLogs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Session = table.Column<string>(maxLength: 50, nullable: true),
                    RequestFormParams = table.Column<string>(maxLength: 2000, nullable: true),
                    RequestQueryStringParams = table.Column<string>(maxLength: 2000, nullable: true),
                    CookieParams = table.Column<string>(maxLength: 2000, nullable: true),
                    SessionParams = table.Column<string>(maxLength: 2000, nullable: true),
                    URL = table.Column<string>(maxLength: 350, nullable: true),
                    UrlReferrer = table.Column<string>(maxLength: 350, nullable: true),
                    LineNumber = table.Column<int>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(maxLength: 150, nullable: true),
                    Details = table.Column<string>(nullable: true),
                    InnerException = table.Column<string>(nullable: true),
                    MethodType = table.Column<string>(maxLength: 10, nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    AdSoyad = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    UserAgent = table.Column<string>(maxLength: 1000, nullable: true),
                    IP = table.Column<string>(maxLength: 40, nullable: false),
                    ExtraMessage = table.Column<string>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ErrorLogs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ErrorLogs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ErrorLogs_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FollowLists",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    UserID = table.Column<int>(nullable: true),
                    ColorID = table.Column<int>(nullable: true),
                    ProductID = table.Column<long>(nullable: true),
                    ProductCode = table.Column<string>(maxLength: 50, nullable: true),
                    Barkod = table.Column<string>(maxLength: 70, nullable: true),
                    YapCode = table.Column<string>(maxLength: 70, nullable: true),
                    StockID = table.Column<long>(nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    ImagePath = table.Column<string>(maxLength: 200, nullable: true),
                    Session = table.Column<string>(maxLength: 50, nullable: true),
                    UserAgent = table.Column<string>(maxLength: 500, nullable: true),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowLists", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FollowLists_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FollowLists_Stocks_StockID",
                        column: x => x.StockID,
                        principalTable: "Stocks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FollowLists_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FollowLists_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NewsLetters",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Email = table.Column<string>(maxLength: 40, nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    IP = table.Column<string>(maxLength: 40, nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsLetters", x => x.ID);
                    table.ForeignKey(
                        name: "FK_NewsLetters_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NewsLetters_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductRates",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ProductID = table.Column<long>(nullable: true),
                    ProductCode = table.Column<string>(maxLength: 40, nullable: true),
                    Barkod = table.Column<string>(maxLength: 40, nullable: true),
                    StockID = table.Column<long>(nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductRates", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductRates_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductRates_Stocks_StockID",
                        column: x => x.StockID,
                        principalTable: "Stocks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductRates_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductRates_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StoreAccountUsers",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    StoreAccountId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreAccountUsers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_StoreAccountUsers_StoreAccounts_StoreAccountId",
                        column: x => x.StoreAccountId,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StoreAccountUsers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserInRoles",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    UserID = table.Column<int>(nullable: false),
                    RoleID = table.Column<int>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInRoles", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserInRoles_Roles_RoleID",
                        column: x => x.RoleID,
                        principalTable: "Roles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserInRoles_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserInRoles_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Quarters",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    DistrictName = table.Column<string>(maxLength: 50, nullable: true),
                    DistrictID = table.Column<int>(nullable: true),
                    IlceID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quarters", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Quarters_Districts_DistrictID",
                        column: x => x.DistrictID,
                        principalTable: "Districts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Quarters_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Carts",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    UserID = table.Column<int>(nullable: true),
                    ProductID = table.Column<long>(nullable: true),
                    ProductCode = table.Column<string>(maxLength: 50, nullable: true),
                    Barkod = table.Column<string>(maxLength: 70, nullable: true),
                    YapCode = table.Column<string>(maxLength: 70, nullable: true),
                    StockID = table.Column<long>(nullable: true),
                    CargoPriceID = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    PriceCurrencyID = table.Column<int>(nullable: true),
                    AmountPsf = table.Column<decimal>(nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    ImagePath = table.Column<string>(maxLength: 200, nullable: true),
                    GiftNote = table.Column<string>(maxLength: 200, nullable: true),
                    Session = table.Column<string>(maxLength: 50, nullable: true),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    UserAgent = table.Column<string>(maxLength: 500, nullable: true),
                    CouponID = table.Column<long>(nullable: true),
                    SizeName = table.Column<string>(maxLength: 50, nullable: true),
                    Url = table.Column<string>(maxLength: 50, nullable: true),
                    ColorID = table.Column<int>(nullable: true),
                    BasketChargeType = table.Column<int>(nullable: true),
                    DomainID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Carts_CargoPrices_CargoPriceID",
                        column: x => x.CargoPriceID,
                        principalTable: "CargoPrices",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Carts_Coupons_CouponID",
                        column: x => x.CouponID,
                        principalTable: "Coupons",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Carts_PriceCurrencies_PriceCurrencyID",
                        column: x => x.PriceCurrencyID,
                        principalTable: "PriceCurrencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Carts_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Carts_Stocks_StockID",
                        column: x => x.StockID,
                        principalTable: "Stocks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Carts_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Carts_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    OrdNo = table.Column<string>(maxLength: 40, nullable: true),
                    TrackCode = table.Column<string>(maxLength: 20, nullable: true),
                    CariCode = table.Column<string>(maxLength: 20, nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    Session = table.Column<string>(maxLength: 50, nullable: true),
                    TempSession = table.Column<string>(maxLength: 40, nullable: true),
                    Installment = table.Column<int>(nullable: true),
                    CommissionRatio = table.Column<decimal>(nullable: true),
                    TotalAmount = table.Column<decimal>(nullable: true),
                    TotalAmountInLocalCurrency = table.Column<decimal>(nullable: true),
                    LocalCurrencyExchangeRate = table.Column<double>(nullable: true),
                    PaymentStatus = table.Column<int>(nullable: false),
                    PaymentType = table.Column<int>(nullable: false),
                    OrderType = table.Column<int>(nullable: false),
                    OrderPlace = table.Column<int>(nullable: false),
                    CargoDeliveryStatus = table.Column<int>(nullable: false),
                    CargoDeliveryMessage = table.Column<string>(maxLength: 150, nullable: true),
                    CargoTrackingUrl = table.Column<string>(maxLength: 250, nullable: true),
                    CargoTrackingCode = table.Column<string>(maxLength: 40, nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: true),
                    CargoDeliveryDate = table.Column<DateTime>(nullable: true),
                    LastItemChangeDate = table.Column<DateTime>(nullable: true),
                    LastSmsSendDate = table.Column<DateTime>(nullable: true),
                    LastItemRemoveDate = table.Column<DateTime>(nullable: true),
                    CargoAmount = table.Column<decimal>(nullable: true),
                    CommissionAmount = table.Column<decimal>(nullable: true),
                    CouponAmount = table.Column<decimal>(nullable: true),
                    CampaignAmount = table.Column<decimal>(nullable: false),
                    ExtraAmount = table.Column<decimal>(nullable: false),
                    GiftAmount = table.Column<decimal>(nullable: true),
                    ShipAsGift = table.Column<bool>(nullable: false),
                    CampaignID = table.Column<int>(nullable: true),
                    CargoID = table.Column<int>(nullable: true),
                    CargoPriceID = table.Column<int>(nullable: true),
                    CargoCode = table.Column<string>(maxLength: 25, nullable: true),
                    CargoLink = table.Column<string>(maxLength: 350, nullable: true),
                    UpsAreaID = table.Column<int>(nullable: true),
                    BankID = table.Column<int>(nullable: true),
                    OneDayCargo = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 60, nullable: true),
                    Email = table.Column<string>(maxLength: 90, nullable: true),
                    GiftNote = table.Column<string>(maxLength: 200, nullable: true),
                    OrdNote = table.Column<string>(maxLength: 300, nullable: true),
                    Gsm = table.Column<string>(maxLength: 25, nullable: true),
                    Phone = table.Column<string>(maxLength: 25, nullable: true),
                    BillingAdressDetail = table.Column<string>(maxLength: 300, nullable: true),
                    BillingAdressID = table.Column<int>(nullable: true),
                    BillingAdressCountryID = table.Column<int>(nullable: true),
                    BillingAdressCityID = table.Column<int>(nullable: true),
                    BillingAdressTownID = table.Column<int>(nullable: true),
                    BillingAdressDistrictID = table.Column<int>(nullable: true),
                    BillingAdressQuarterID = table.Column<int>(nullable: true),
                    BillingShipAdressID = table.Column<int>(nullable: true),
                    BillingAdressPhone = table.Column<string>(maxLength: 20, nullable: true),
                    BillingAdressPhone2 = table.Column<string>(maxLength: 20, nullable: true),
                    BillingAdressTcNo = table.Column<string>(nullable: true),
                    DeliveryAdressDetail = table.Column<string>(maxLength: 300, nullable: true),
                    DeliveryAdressGSM = table.Column<string>(maxLength: 20, nullable: true),
                    DeliveryAdressPhone = table.Column<string>(maxLength: 20, nullable: true),
                    DeliveryAdressPhone2 = table.Column<string>(maxLength: 20, nullable: true),
                    DeliveryAdressTcNo = table.Column<string>(nullable: true),
                    DeliveryAdressID = table.Column<int>(nullable: true),
                    DeliveryAdressCountryID = table.Column<int>(nullable: true),
                    DeliveryAdressCityID = table.Column<int>(nullable: true),
                    DeliveryAdressTownID = table.Column<int>(nullable: true),
                    DeliveryAdressDistrictID = table.Column<int>(nullable: true),
                    DeliveryAdressQuarterID = table.Column<int>(nullable: true),
                    IsOnlineOrder = table.Column<bool>(nullable: false),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    ReturnedOrderID = table.Column<long>(nullable: true),
                    ReturnAmount = table.Column<decimal>(nullable: false),
                    LangID = table.Column<int>(nullable: true),
                    PriceCurrenyAmount = table.Column<decimal>(nullable: true),
                    CouponID = table.Column<long>(nullable: true),
                    CouponCode = table.Column<string>(nullable: true),
                    WebPosID = table.Column<int>(nullable: true),
                    WebPosInstallmentID = table.Column<int>(nullable: true),
                    InvoicesSendIntoCourier = table.Column<bool>(nullable: false),
                    IsCargoTransferred = table.Column<bool>(nullable: true),
                    IsTransferedToNetsis = table.Column<bool>(nullable: false),
                    IsReturnedToNetsis = table.Column<bool>(nullable: false),
                    IsMoneyTransferedNetsis = table.Column<bool>(nullable: false),
                    IsTransferedToNetsisTahsilat = table.Column<bool>(nullable: false),
                    BkmToken = table.Column<string>(maxLength: 100, nullable: true),
                    PayPalToken = table.Column<string>(maxLength: 100, nullable: true),
                    utm_source = table.Column<string>(maxLength: 30, nullable: true),
                    utm_campaign = table.Column<string>(maxLength: 30, nullable: true),
                    utm_medium = table.Column<string>(maxLength: 30, nullable: true),
                    IsPaid = table.Column<bool>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Orders_Banks_BankID",
                        column: x => x.BankID,
                        principalTable: "Banks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Campaigns_CampaignID",
                        column: x => x.CampaignID,
                        principalTable: "Campaigns",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Cargoes_CargoID",
                        column: x => x.CargoID,
                        principalTable: "Cargoes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_CargoPrices_CargoPriceID",
                        column: x => x.CargoPriceID,
                        principalTable: "CargoPrices",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Coupons_CouponID",
                        column: x => x.CouponID,
                        principalTable: "Coupons",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_WebPoses_WebPosID",
                        column: x => x.WebPosID,
                        principalTable: "WebPoses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_WebPosInstallments_WebPosInstallmentID",
                        column: x => x.WebPosInstallmentID,
                        principalTable: "WebPosInstallments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Detail = table.Column<string>(maxLength: 600, nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    PostalCode = table.Column<string>(maxLength: 50, nullable: true),
                    StateName = table.Column<string>(maxLength: 50, nullable: true),
                    CityName = table.Column<string>(maxLength: 50, nullable: true),
                    SessionID = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(maxLength: 90, nullable: true),
                    TownName = table.Column<string>(maxLength: 40, nullable: true),
                    Phone1 = table.Column<string>(maxLength: 20, nullable: true),
                    Phone2 = table.Column<string>(maxLength: 20, nullable: true),
                    AdresTipi = table.Column<int>(nullable: false),
                    VergiDairesi = table.Column<string>(nullable: true),
                    VergiNo = table.Column<string>(nullable: true),
                    Unvan = table.Column<string>(nullable: true),
                    TcNo = table.Column<string>(nullable: true),
                    AllowCampaignSms = table.Column<bool>(nullable: false),
                    CityID = table.Column<int>(nullable: true),
                    StateID = table.Column<int>(nullable: true),
                    TownID = table.Column<int>(nullable: true),
                    DistrictID = table.Column<int>(nullable: true),
                    QuarterID = table.Column<int>(nullable: true),
                    CountryID = table.Column<int>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Addresses_Cities_CityID",
                        column: x => x.CityID,
                        principalTable: "Cities",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Countries_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Countries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Districts_DistrictID",
                        column: x => x.DistrictID,
                        principalTable: "Districts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Quarters_QuarterID",
                        column: x => x.QuarterID,
                        principalTable: "Quarters",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_States_StateID",
                        column: x => x.StateID,
                        principalTable: "States",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Towns_TownID",
                        column: x => x.TownID,
                        principalTable: "Towns",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MoneyTransferInfos",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    OrderID = table.Column<long>(nullable: false),
                    UserID = table.Column<int>(nullable: true),
                    BankID = table.Column<int>(nullable: false),
                    SenderName = table.Column<string>(maxLength: 200, nullable: true),
                    Note = table.Column<string>(maxLength: 250, nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    IsApproved = table.Column<bool>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoneyTransferInfos", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MoneyTransferInfos_Banks_BankID",
                        column: x => x.BankID,
                        principalTable: "Banks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MoneyTransferInfos_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MoneyTransferInfos_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MoneyTransferInfos_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderedItems",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    StockID = table.Column<long>(nullable: true),
                    ProductID = table.Column<long>(nullable: true),
                    CargoPriceID = table.Column<int>(nullable: true),
                    ProductCode = table.Column<string>(maxLength: 40, nullable: true),
                    YapCode = table.Column<string>(maxLength: 70, nullable: true),
                    Barkod = table.Column<string>(maxLength: 70, nullable: true),
                    Quantity = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    AmountOfDiscount = table.Column<decimal>(nullable: true),
                    AmountPsf = table.Column<decimal>(nullable: true),
                    OrderID = table.Column<long>(nullable: true),
                    CouponID = table.Column<long>(nullable: true),
                    ReturnedOrderID = table.Column<int>(nullable: true),
                    BasketChargeType = table.Column<int>(nullable: true),
                    ImagePath = table.Column<string>(maxLength: 200, nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    GiftNote = table.Column<string>(maxLength: 200, nullable: true),
                    ReturnStatus = table.Column<int>(nullable: true),
                    ReturnReason = table.Column<int>(nullable: true),
                    IsTransferedAsReturned = table.Column<bool>(nullable: false),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderedItems", x => x.ID);
                    table.ForeignKey(
                        name: "FK_OrderedItems_CargoPrices_CargoPriceID",
                        column: x => x.CargoPriceID,
                        principalTable: "CargoPrices",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderedItems_Coupons_CouponID",
                        column: x => x.CouponID,
                        principalTable: "Coupons",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderedItems_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderedItems_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderedItems_Stocks_StockID",
                        column: x => x.StockID,
                        principalTable: "Stocks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderedItems_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PaymentHistory",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Amount = table.Column<decimal>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Detail = table.Column<string>(nullable: true),
                    OrderID = table.Column<long>(nullable: true),
                    TrackCode = table.Column<string>(maxLength: 40, nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    UserArgent = table.Column<string>(nullable: true),
                    PaymentType = table.Column<int>(nullable: false),
                    PaymentLogType = table.Column<int>(nullable: true),
                    PaymentComplateStatus = table.Column<int>(nullable: false),
                    CardNumber = table.Column<string>(maxLength: 20, nullable: true),
                    ExpiresMonth = table.Column<int>(nullable: true),
                    ExpiresYear = table.Column<int>(nullable: true),
                    CardOwner = table.Column<string>(maxLength: 50, nullable: true),
                    CardCvc = table.Column<int>(nullable: true),
                    WebPosID = table.Column<int>(nullable: true),
                    CardType = table.Column<int>(nullable: true),
                    WebPosInstallmentID = table.Column<int>(nullable: true),
                    BankCurrencyNo = table.Column<int>(nullable: true),
                    Retval = table.Column<string>(maxLength: 900, nullable: true),
                    Err = table.Column<string>(maxLength: 910, nullable: true),
                    ErrMsg = table.Column<string>(maxLength: 3500, nullable: true),
                    PayCode = table.Column<string>(maxLength: 950, nullable: true),
                    ExtraHostMsg = table.Column<string>(maxLength: 4000, nullable: true),
                    PayRefNo = table.Column<string>(maxLength: 900, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentHistory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PaymentHistory_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentHistory_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentHistory_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentHistory_WebPoses_WebPosID",
                        column: x => x.WebPosID,
                        principalTable: "WebPoses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentHistory_WebPosInstallments_WebPosInstallmentID",
                        column: x => x.WebPosInstallmentID,
                        principalTable: "WebPosInstallments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebPosLogs",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    TotalAmount = table.Column<double>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Detail = table.Column<string>(nullable: true),
                    TrackCode = table.Column<string>(nullable: true),
                    OrderID = table.Column<long>(nullable: false),
                    UserID = table.Column<int>(nullable: true),
                    IP = table.Column<string>(maxLength: 40, nullable: true),
                    Retval = table.Column<string>(maxLength: 900, nullable: true),
                    Err = table.Column<string>(maxLength: 910, nullable: true),
                    ErrMsg = table.Column<string>(maxLength: 3500, nullable: true),
                    PayCode = table.Column<string>(maxLength: 950, nullable: true),
                    ExtraHostMsg = table.Column<string>(maxLength: 4000, nullable: true),
                    PayRefNo = table.Column<string>(maxLength: 900, nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebPosLogs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_WebPosLogs_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WebPosLogs_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebPosLogs_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderHistories",
                columns: table => new
                {
                    PStatus = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedById = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    OrderID = table.Column<long>(nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    PaymentHistoryID = table.Column<int>(nullable: true),
                    OrderStatus = table.Column<int>(nullable: true),
                    Note = table.Column<string>(maxLength: 600, nullable: true),
                    PaymentHistoryID1 = table.Column<long>(nullable: true),
                    StoreAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderHistories", x => x.ID);
                    table.ForeignKey(
                        name: "FK_OrderHistories_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderHistories_PaymentHistory_PaymentHistoryID1",
                        column: x => x.PaymentHistoryID1,
                        principalTable: "PaymentHistory",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderHistories_StoreAccounts_StoreAccountID",
                        column: x => x.StoreAccountID,
                        principalTable: "StoreAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderHistories_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CityID",
                table: "Addresses",
                column: "CityID");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CountryID",
                table: "Addresses",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_DistrictID",
                table: "Addresses",
                column: "DistrictID");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_QuarterID",
                table: "Addresses",
                column: "QuarterID");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_StateID",
                table: "Addresses",
                column: "StateID");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_StoreAccountID",
                table: "Addresses",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_TownID",
                table: "Addresses",
                column: "TownID");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_UserID",
                table: "Addresses",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Albums_StoreAccountID",
                table: "Albums",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Banks_PaymentID",
                table: "Banks",
                column: "PaymentID");

            migrationBuilder.CreateIndex(
                name: "IX_Banks_StoreAccountID",
                table: "Banks",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Banners_DomainID",
                table: "Banners",
                column: "DomainID");

            migrationBuilder.CreateIndex(
                name: "IX_Banners_LangID",
                table: "Banners",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_Banners_StoreAccountID",
                table: "Banners",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_BinNumbers_StoreAccountID",
                table: "BinNumbers",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogCatalogs_ParentID",
                table: "BlogCatalogs",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogCatalogs_StoreAccountID",
                table: "BlogCatalogs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogLikes_BlogPostID",
                table: "BlogLikes",
                column: "BlogPostID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogLikes_StoreAccountID",
                table: "BlogLikes",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogLikes_UserID",
                table: "BlogLikes",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPostImages_BlogPostID",
                table: "BlogPostImages",
                column: "BlogPostID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPostImages_StoreAccountID",
                table: "BlogPostImages",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_BlogCatalogID",
                table: "BlogPosts",
                column: "BlogCatalogID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_LangID",
                table: "BlogPosts",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_StoreAccountID",
                table: "BlogPosts",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Brands_StoreAccountID",
                table: "Brands",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignLimits_BrandID",
                table: "CampaignLimits",
                column: "BrandID");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignLimits_CampaignID",
                table: "CampaignLimits",
                column: "CampaignID");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignLimits_CatalogID",
                table: "CampaignLimits",
                column: "CatalogID");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignLimits_ProductID",
                table: "CampaignLimits",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignLimits_SeasonID",
                table: "CampaignLimits",
                column: "SeasonID");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignLimits_StoreAccountID",
                table: "CampaignLimits",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_DomainId",
                table: "Campaigns",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_LangId",
                table: "Campaigns",
                column: "LangId");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_PriceCurrencyID",
                table: "Campaigns",
                column: "PriceCurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_StoreAccountID",
                table: "Campaigns",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Cargoes_DomainID",
                table: "Cargoes",
                column: "DomainID");

            migrationBuilder.CreateIndex(
                name: "IX_Cargoes_StoreAccountID",
                table: "Cargoes",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_CargoPrices_CargoID",
                table: "CargoPrices",
                column: "CargoID");

            migrationBuilder.CreateIndex(
                name: "IX_CargoPrices_CountryID",
                table: "CargoPrices",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_CargoPrices_DomainID",
                table: "CargoPrices",
                column: "DomainID");

            migrationBuilder.CreateIndex(
                name: "IX_CargoPrices_PriceCurrencyID",
                table: "CargoPrices",
                column: "PriceCurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_CargoPrices_StoreAccountID",
                table: "CargoPrices",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_CargoPriceID",
                table: "Carts",
                column: "CargoPriceID");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_CouponID",
                table: "Carts",
                column: "CouponID");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_PriceCurrencyID",
                table: "Carts",
                column: "PriceCurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_ProductID",
                table: "Carts",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_StockID",
                table: "Carts",
                column: "StockID");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_StoreAccountID",
                table: "Carts",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_UserID",
                table: "Carts",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_CatalogLocalLangs_CatalogID",
                table: "CatalogLocalLangs",
                column: "CatalogID");

            migrationBuilder.CreateIndex(
                name: "IX_CatalogLocalLangs_LangID",
                table: "CatalogLocalLangs",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_CatalogLocalLangs_StoreAccountID",
                table: "CatalogLocalLangs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Catalogs_StoreAccountID",
                table: "Catalogs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Cities_CountryID",
                table: "Cities",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_Cities_StateID",
                table: "Cities",
                column: "StateID");

            migrationBuilder.CreateIndex(
                name: "IX_ColorGlobals_StoreAccountID",
                table: "ColorGlobals",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Colors_ColorGlobalID",
                table: "Colors",
                column: "ColorGlobalID");

            migrationBuilder.CreateIndex(
                name: "IX_Colors_StoreAccountID",
                table: "Colors",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_BlogPostID",
                table: "Comments",
                column: "BlogPostID");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_StoreAccountID",
                table: "Comments",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserID",
                table: "Comments",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ContactMessages_StoreAccountID",
                table: "ContactMessages",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ContactMessages_UserID",
                table: "ContactMessages",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ContentPageLocalLangs_ContentPageID",
                table: "ContentPageLocalLangs",
                column: "ContentPageID");

            migrationBuilder.CreateIndex(
                name: "IX_ContentPageLocalLangs_LangID",
                table: "ContentPageLocalLangs",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_ContentPageLocalLangs_StoreAccountID",
                table: "ContentPageLocalLangs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ContentPages_StoreAccountID",
                table: "ContentPages",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Countries_DefaultLangID",
                table: "Countries",
                column: "DefaultLangID");

            migrationBuilder.CreateIndex(
                name: "IX_Countries_DefaultPriceCurrencyID",
                table: "Countries",
                column: "DefaultPriceCurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Coupons_CampaignID",
                table: "Coupons",
                column: "CampaignID");

            migrationBuilder.CreateIndex(
                name: "IX_Coupons_DomainId",
                table: "Coupons",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Coupons_FromUserID",
                table: "Coupons",
                column: "FromUserID");

            migrationBuilder.CreateIndex(
                name: "IX_Coupons_PriceCurrencyID",
                table: "Coupons",
                column: "PriceCurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Coupons_StoreAccountID",
                table: "Coupons",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Coupons_UserID",
                table: "Coupons",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Districts_StoreAccountID",
                table: "Districts",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Districts_TownID",
                table: "Districts",
                column: "TownID");

            migrationBuilder.CreateIndex(
                name: "IX_Domains_LangID",
                table: "Domains",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_Domains_PriceCurrencyID",
                table: "Domains",
                column: "PriceCurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Domains_StoreAccountID",
                table: "Domains",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplateLocalLangs_EmailTemplateID",
                table: "EmailTemplateLocalLangs",
                column: "EmailTemplateID");

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplateLocalLangs_LangID",
                table: "EmailTemplateLocalLangs",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplateLocalLangs_StoreAccountID",
                table: "EmailTemplateLocalLangs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplates_DomainID",
                table: "EmailTemplates",
                column: "DomainID");

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplates_EmailProviderID",
                table: "EmailTemplates",
                column: "EmailProviderID");

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplates_StoreAccountID",
                table: "EmailTemplates",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ErrorLogs_StoreAccountID",
                table: "ErrorLogs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ErrorLogs_UserID",
                table: "ErrorLogs",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_FaqLocalLangs_FaqID",
                table: "FaqLocalLangs",
                column: "FaqID");

            migrationBuilder.CreateIndex(
                name: "IX_FaqLocalLangs_LangID",
                table: "FaqLocalLangs",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_FaqLocalLangs_StoreAccountID",
                table: "FaqLocalLangs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Faqs_StoreAccountID",
                table: "Faqs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_FollowLists_ProductID",
                table: "FollowLists",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_FollowLists_StockID",
                table: "FollowLists",
                column: "StockID");

            migrationBuilder.CreateIndex(
                name: "IX_FollowLists_StoreAccountID",
                table: "FollowLists",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_FollowLists_UserID",
                table: "FollowLists",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_GiftPrices_CountryID",
                table: "GiftPrices",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_GiftPrices_StoreAccountID",
                table: "GiftPrices",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_MetaRules_LangID",
                table: "MetaRules",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_MetaRules_StoreAccountID",
                table: "MetaRules",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_MoneyTransferInfos_BankID",
                table: "MoneyTransferInfos",
                column: "BankID");

            migrationBuilder.CreateIndex(
                name: "IX_MoneyTransferInfos_OrderID",
                table: "MoneyTransferInfos",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_MoneyTransferInfos_StoreAccountID",
                table: "MoneyTransferInfos",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_MoneyTransferInfos_UserID",
                table: "MoneyTransferInfos",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_News_StoreAccountID",
                table: "News",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_NewsLetters_StoreAccountID",
                table: "NewsLetters",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_NewsLetters_UserID",
                table: "NewsLetters",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderedItems_CargoPriceID",
                table: "OrderedItems",
                column: "CargoPriceID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderedItems_CouponID",
                table: "OrderedItems",
                column: "CouponID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderedItems_OrderID",
                table: "OrderedItems",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderedItems_ProductID",
                table: "OrderedItems",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderedItems_StockID",
                table: "OrderedItems",
                column: "StockID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderedItems_StoreAccountID",
                table: "OrderedItems",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderHistories_OrderID",
                table: "OrderHistories",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderHistories_PaymentHistoryID1",
                table: "OrderHistories",
                column: "PaymentHistoryID1");

            migrationBuilder.CreateIndex(
                name: "IX_OrderHistories_StoreAccountID",
                table: "OrderHistories",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderHistories_UserID",
                table: "OrderHistories",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BankID",
                table: "Orders",
                column: "BankID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CampaignID",
                table: "Orders",
                column: "CampaignID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CargoID",
                table: "Orders",
                column: "CargoID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CargoPriceID",
                table: "Orders",
                column: "CargoPriceID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CouponID",
                table: "Orders",
                column: "CouponID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_StoreAccountID",
                table: "Orders",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_UserID",
                table: "Orders",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_WebPosID",
                table: "Orders",
                column: "WebPosID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_WebPosInstallmentID",
                table: "Orders",
                column: "WebPosInstallmentID");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentHistory_OrderID",
                table: "PaymentHistory",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentHistory_StoreAccountID",
                table: "PaymentHistory",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentHistory_UserID",
                table: "PaymentHistory",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentHistory_WebPosID",
                table: "PaymentHistory",
                column: "WebPosID");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentHistory_WebPosInstallmentID",
                table: "PaymentHistory",
                column: "WebPosInstallmentID");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_StoreAccountID",
                table: "Payments",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_AlbumID",
                table: "Photos",
                column: "AlbumID");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_StoreAccountID",
                table: "Photos",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Policies_StoreAccountID",
                table: "Policies",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_PolicyLocalLangs_LangID",
                table: "PolicyLocalLangs",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_PolicyLocalLangs_PolicyID",
                table: "PolicyLocalLangs",
                column: "PolicyID");

            migrationBuilder.CreateIndex(
                name: "IX_PolicyLocalLangs_StoreAccountID",
                table: "PolicyLocalLangs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Prices_DomainID",
                table: "Prices",
                column: "DomainID");

            migrationBuilder.CreateIndex(
                name: "IX_Prices_PriceCurrencyID",
                table: "Prices",
                column: "PriceCurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Prices_ProductID",
                table: "Prices",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_Prices_StockID",
                table: "Prices",
                column: "StockID");

            migrationBuilder.CreateIndex(
                name: "IX_Prices_StoreAccountID",
                table: "Prices",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductDepartments_StoreAccountID",
                table: "ProductDepartments",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductDetails_LangID",
                table: "ProductDetails",
                column: "LangID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductDetails_ProductID",
                table: "ProductDetails",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductDetails_StoreAccountID",
                table: "ProductDetails",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductImages_ProductID",
                table: "ProductImages",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductImages_StoreAccountID",
                table: "ProductImages",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInCatalogs_CatalogID",
                table: "ProductInCatalogs",
                column: "CatalogID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInCatalogs_ColorID",
                table: "ProductInCatalogs",
                column: "ColorID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInCatalogs_ProductID",
                table: "ProductInCatalogs",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInCatalogs_StockID",
                table: "ProductInCatalogs",
                column: "StockID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInCatalogs_StoreAccountID",
                table: "ProductInCatalogs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductRates_ProductID",
                table: "ProductRates",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductRates_StockID",
                table: "ProductRates",
                column: "StockID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductRates_StoreAccountID",
                table: "ProductRates",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductRates_UserID",
                table: "ProductRates",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_BrandID",
                table: "Products",
                column: "BrandID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CargoPolicyID",
                table: "Products",
                column: "CargoPolicyID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ProductDepartmentID",
                table: "Products",
                column: "ProductDepartmentID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ProductStyleID",
                table: "Products",
                column: "ProductStyleID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ReturnPolicyID",
                table: "Products",
                column: "ReturnPolicyID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SeasonID",
                table: "Products",
                column: "SeasonID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_StoreAccountID",
                table: "Products",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductStyles_StoreAccountID",
                table: "ProductStyles",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Quarters_DistrictID",
                table: "Quarters",
                column: "DistrictID");

            migrationBuilder.CreateIndex(
                name: "IX_Quarters_StoreAccountID",
                table: "Quarters",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_StoreAccountID",
                table: "Roles",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Seasons_StoreAccountID",
                table: "Seasons",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_SendingProviders_StoreAccountID",
                table: "SendingProviders",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Sizes_StoreAccountID",
                table: "Sizes",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_States_CountryID",
                table: "States",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_States_StoreAccountID",
                table: "States",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Stocks_ColorID",
                table: "Stocks",
                column: "ColorID");

            migrationBuilder.CreateIndex(
                name: "IX_Stocks_ProductID",
                table: "Stocks",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_Stocks_SizeID",
                table: "Stocks",
                column: "SizeID");

            migrationBuilder.CreateIndex(
                name: "IX_Stocks_StoreAccountID",
                table: "Stocks",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_StoreAccountUsers_StoreAccountId",
                table: "StoreAccountUsers",
                column: "StoreAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_StoreAccountUsers_UserId",
                table: "StoreAccountUsers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Towns_CityID",
                table: "Towns",
                column: "CityID");

            migrationBuilder.CreateIndex(
                name: "IX_UrlHistories_StoreAccountID",
                table: "UrlHistories",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_UserInRoles_RoleID",
                table: "UserInRoles",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_UserInRoles_StoreAccountID",
                table: "UserInRoles",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_UserInRoles_UserID",
                table: "UserInRoles",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CityID",
                table: "Users",
                column: "CityID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_DomainId",
                table: "Users",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_StoreAccountID",
                table: "Users",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_WebPoses_PaymentID",
                table: "WebPoses",
                column: "PaymentID");

            migrationBuilder.CreateIndex(
                name: "IX_WebPoses_StoreAccountID",
                table: "WebPoses",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_WebPosInstallments_StoreAccountID",
                table: "WebPosInstallments",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_WebPosInstallments_WebPosID",
                table: "WebPosInstallments",
                column: "WebPosID");

            migrationBuilder.CreateIndex(
                name: "IX_WebPosLogs_OrderID",
                table: "WebPosLogs",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_WebPosLogs_StoreAccountID",
                table: "WebPosLogs",
                column: "StoreAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_WebPosLogs_UserID",
                table: "WebPosLogs",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Banners");

            migrationBuilder.DropTable(
                name: "BinNumbers");

            migrationBuilder.DropTable(
                name: "BlogLikes");

            migrationBuilder.DropTable(
                name: "BlogPostImages");

            migrationBuilder.DropTable(
                name: "CampaignLimits");

            migrationBuilder.DropTable(
                name: "Carts");

            migrationBuilder.DropTable(
                name: "CatalogLocalLangs");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "ContactMessages");

            migrationBuilder.DropTable(
                name: "ContentPageLocalLangs");

            migrationBuilder.DropTable(
                name: "EmailTemplateLocalLangs");

            migrationBuilder.DropTable(
                name: "ErrorLogs");

            migrationBuilder.DropTable(
                name: "FaqLocalLangs");

            migrationBuilder.DropTable(
                name: "FollowLists");

            migrationBuilder.DropTable(
                name: "GiftPrices");

            migrationBuilder.DropTable(
                name: "MetaRules");

            migrationBuilder.DropTable(
                name: "MoneyTransferInfos");

            migrationBuilder.DropTable(
                name: "News");

            migrationBuilder.DropTable(
                name: "NewsLetters");

            migrationBuilder.DropTable(
                name: "OrderedItems");

            migrationBuilder.DropTable(
                name: "OrderHistories");

            migrationBuilder.DropTable(
                name: "Photos");

            migrationBuilder.DropTable(
                name: "PolicyLocalLangs");

            migrationBuilder.DropTable(
                name: "Prices");

            migrationBuilder.DropTable(
                name: "ProductDetails");

            migrationBuilder.DropTable(
                name: "ProductImages");

            migrationBuilder.DropTable(
                name: "ProductInCatalogs");

            migrationBuilder.DropTable(
                name: "ProductRates");

            migrationBuilder.DropTable(
                name: "StoreAccountUsers");

            migrationBuilder.DropTable(
                name: "UrlHistories");

            migrationBuilder.DropTable(
                name: "UserInRoles");

            migrationBuilder.DropTable(
                name: "WebPosLogs");

            migrationBuilder.DropTable(
                name: "Quarters");

            migrationBuilder.DropTable(
                name: "BlogPosts");

            migrationBuilder.DropTable(
                name: "ContentPages");

            migrationBuilder.DropTable(
                name: "EmailTemplates");

            migrationBuilder.DropTable(
                name: "Faqs");

            migrationBuilder.DropTable(
                name: "PaymentHistory");

            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "Catalogs");

            migrationBuilder.DropTable(
                name: "Stocks");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Districts");

            migrationBuilder.DropTable(
                name: "BlogCatalogs");

            migrationBuilder.DropTable(
                name: "SendingProviders");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Colors");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Sizes");

            migrationBuilder.DropTable(
                name: "Towns");

            migrationBuilder.DropTable(
                name: "Banks");

            migrationBuilder.DropTable(
                name: "CargoPrices");

            migrationBuilder.DropTable(
                name: "Coupons");

            migrationBuilder.DropTable(
                name: "WebPosInstallments");

            migrationBuilder.DropTable(
                name: "ColorGlobals");

            migrationBuilder.DropTable(
                name: "Brands");

            migrationBuilder.DropTable(
                name: "Policies");

            migrationBuilder.DropTable(
                name: "ProductDepartments");

            migrationBuilder.DropTable(
                name: "ProductStyles");

            migrationBuilder.DropTable(
                name: "Seasons");

            migrationBuilder.DropTable(
                name: "Cargoes");

            migrationBuilder.DropTable(
                name: "Campaigns");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "WebPoses");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Domains");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "States");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "StoreAccounts");

            migrationBuilder.DropTable(
                name: "Langs");

            migrationBuilder.DropTable(
                name: "PriceCurrencies");
        }
    }
}
