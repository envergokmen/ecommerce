﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ecom.Database.Migrations
{
    public partial class orderchanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrdNo",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "PriceCurrenyAmount",
                table: "Orders",
                newName: "PriceCurrencyAmount");

            migrationBuilder.AddColumn<int>(
                name: "PriceCurrencyID",
                table: "PaymentHistory",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TrackCode",
                table: "Orders",
                maxLength: 40,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DomainID",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PriceCurrencyID",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "VatRatio",
                table: "OrderedItems",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentHistory_PriceCurrencyID",
                table: "PaymentHistory",
                column: "PriceCurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DomainID",
                table: "Orders",
                column: "DomainID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_PriceCurrencyID",
                table: "Orders",
                column: "PriceCurrencyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Domains_DomainID",
                table: "Orders",
                column: "DomainID",
                principalTable: "Domains",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_PriceCurrencies_PriceCurrencyID",
                table: "Orders",
                column: "PriceCurrencyID",
                principalTable: "PriceCurrencies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentHistory_PriceCurrencies_PriceCurrencyID",
                table: "PaymentHistory",
                column: "PriceCurrencyID",
                principalTable: "PriceCurrencies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Domains_DomainID",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_PriceCurrencies_PriceCurrencyID",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_PaymentHistory_PriceCurrencies_PriceCurrencyID",
                table: "PaymentHistory");

            migrationBuilder.DropIndex(
                name: "IX_PaymentHistory_PriceCurrencyID",
                table: "PaymentHistory");

            migrationBuilder.DropIndex(
                name: "IX_Orders_DomainID",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_PriceCurrencyID",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "PriceCurrencyID",
                table: "PaymentHistory");

            migrationBuilder.DropColumn(
                name: "DomainID",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "PriceCurrencyID",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "VatRatio",
                table: "OrderedItems");

            migrationBuilder.RenameColumn(
                name: "PriceCurrencyAmount",
                table: "Orders",
                newName: "PriceCurrenyAmount");

            migrationBuilder.AlterColumn<string>(
                name: "TrackCode",
                table: "Orders",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 40,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrdNo",
                table: "Orders",
                maxLength: 40,
                nullable: true);
        }
    }
}
