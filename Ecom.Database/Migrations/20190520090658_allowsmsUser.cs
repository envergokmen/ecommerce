﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ecom.Database.Migrations
{
    public partial class allowsmsUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AllowSMS",
                table: "Users",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllowSMS",
                table: "Users");
        }
    }
}
