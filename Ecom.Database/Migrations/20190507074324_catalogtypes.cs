﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ecom.Database.Migrations
{
    public partial class catalogtypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlogPosts_BlogCatalogs_BlogCatalogID",
                table: "BlogPosts");

            migrationBuilder.RenameColumn(
                name: "BlogCatalogID",
                table: "BlogPosts",
                newName: "CatalogID");

            migrationBuilder.RenameIndex(
                name: "IX_BlogPosts_BlogCatalogID",
                table: "BlogPosts",
                newName: "IX_BlogPosts_CatalogID");

            migrationBuilder.AddColumn<int>(
                name: "CatalogType",
                table: "Catalogs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_BlogPosts_Catalogs_CatalogID",
                table: "BlogPosts",
                column: "CatalogID",
                principalTable: "Catalogs",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlogPosts_Catalogs_CatalogID",
                table: "BlogPosts");

            migrationBuilder.DropColumn(
                name: "CatalogType",
                table: "Catalogs");

            migrationBuilder.RenameColumn(
                name: "CatalogID",
                table: "BlogPosts",
                newName: "BlogCatalogID");

            migrationBuilder.RenameIndex(
                name: "IX_BlogPosts_CatalogID",
                table: "BlogPosts",
                newName: "IX_BlogPosts_BlogCatalogID");

            migrationBuilder.AddForeignKey(
                name: "FK_BlogPosts_BlogCatalogs_BlogCatalogID",
                table: "BlogPosts",
                column: "BlogCatalogID",
                principalTable: "BlogCatalogs",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
