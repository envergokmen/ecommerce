﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ecom.Database.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Angle",
                table: "ProductImages",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ColorID",
                table: "ProductImages",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductImages_ColorID",
                table: "ProductImages",
                column: "ColorID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductImages_Colors_ColorID",
                table: "ProductImages",
                column: "ColorID",
                principalTable: "Colors",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductImages_Colors_ColorID",
                table: "ProductImages");

            migrationBuilder.DropIndex(
                name: "IX_ProductImages_ColorID",
                table: "ProductImages");

            migrationBuilder.DropColumn(
                name: "Angle",
                table: "ProductImages");

            migrationBuilder.DropColumn(
                name: "ColorID",
                table: "ProductImages");
        }
    }
}
