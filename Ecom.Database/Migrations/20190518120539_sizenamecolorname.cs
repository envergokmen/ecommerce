﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ecom.Database.Migrations
{
    public partial class sizenamecolorname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ColorName",
                table: "OrderedItems",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SizeName",
                table: "OrderedItems",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ColorName",
                table: "OrderedItems");

            migrationBuilder.DropColumn(
                name: "SizeName",
                table: "OrderedItems");
        }
    }
}
