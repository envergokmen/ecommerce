﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ecom.Database.Migrations
{
    public partial class campaignNullableFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Campaigns_PriceCurrencies_PriceCurrencyID",
                table: "Campaigns");

            migrationBuilder.DropForeignKey(
                name: "FK_Coupons_PriceCurrencies_PriceCurrencyID",
                table: "Coupons");

            migrationBuilder.AlterColumn<int>(
                name: "PriceCurrencyID",
                table: "Coupons",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PriceCurrencyID",
                table: "Campaigns",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "MaxAmountLimit",
                table: "Campaigns",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "DiscountLimit",
                table: "Campaigns",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CreationLimit",
                table: "Campaigns",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CouponEndDate",
                table: "Campaigns",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddForeignKey(
                name: "FK_Campaigns_PriceCurrencies_PriceCurrencyID",
                table: "Campaigns",
                column: "PriceCurrencyID",
                principalTable: "PriceCurrencies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Coupons_PriceCurrencies_PriceCurrencyID",
                table: "Coupons",
                column: "PriceCurrencyID",
                principalTable: "PriceCurrencies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Campaigns_PriceCurrencies_PriceCurrencyID",
                table: "Campaigns");

            migrationBuilder.DropForeignKey(
                name: "FK_Coupons_PriceCurrencies_PriceCurrencyID",
                table: "Coupons");

            migrationBuilder.AlterColumn<int>(
                name: "PriceCurrencyID",
                table: "Coupons",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PriceCurrencyID",
                table: "Campaigns",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "MaxAmountLimit",
                table: "Campaigns",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DiscountLimit",
                table: "Campaigns",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CreationLimit",
                table: "Campaigns",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CouponEndDate",
                table: "Campaigns",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Campaigns_PriceCurrencies_PriceCurrencyID",
                table: "Campaigns",
                column: "PriceCurrencyID",
                principalTable: "PriceCurrencies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Coupons_PriceCurrencies_PriceCurrencyID",
                table: "Coupons",
                column: "PriceCurrencyID",
                principalTable: "PriceCurrencies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
