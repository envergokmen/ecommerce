﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ecom.Database.Migrations
{
    public partial class basketnames2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Quantity",
                table: "Carts",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LangID",
                table: "Carts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Carts_LangID",
                table: "Carts",
                column: "LangID");

            migrationBuilder.AddForeignKey(
                name: "FK_Carts_Langs_LangID",
                table: "Carts",
                column: "LangID",
                principalTable: "Langs",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Carts_Langs_LangID",
                table: "Carts");

            migrationBuilder.DropIndex(
                name: "IX_Carts_LangID",
                table: "Carts");

            migrationBuilder.DropColumn(
                name: "LangID",
                table: "Carts");

            migrationBuilder.AlterColumn<int>(
                name: "Quantity",
                table: "Carts",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
