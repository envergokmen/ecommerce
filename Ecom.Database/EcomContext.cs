﻿using Microsoft.EntityFrameworkCore;

namespace Ecom.Models
{
    public class EcomContext : DbContext
    {
        public EcomContext()
        {

        }

        public EcomContext(DbContextOptions<EcomContext> options) : base(options)
        {

        }

        //ForNpgsqlUseIdentityColumns

        

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseNpgsql(@"Server=localhost;Database=testdb;Username=postgres;Password=123456");
        }

        //public DbSet<ActiveLang> ActiveLang { get; set; }
        //public DbSet<ActiveCurrency> ActiveCurrencies { get; set; }
        //public DbSet<ActiveCargo> ActiveCargoes { get; set; }
        //public DbSet<ActivePayment> ActivePayment { get; set; }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Banner> Banners { get; set; }
        public DbSet<BinNumber> BinNumbers { get; set; }

        public DbSet<BlogLike> BlogLikes { get; set; }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<ContentPage> ContentPages { get; set; }
        public DbSet<BlogPostImage> BlogPostImages { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<CampaignLimit> CampaignLimits { get; set; }
        public DbSet<Catalog> Catalogs { get; set; }
        public DbSet<CatalogLocalLang> CatalogLocalLangs { get; set; }
        public DbSet<Cargo> Cargoes { get; set; }
        public DbSet<PolicyLocalLang> PolicyLocalLangs { get; set; }
        public DbSet<Policy> Policies { get; set; }
        public DbSet<CargoPrice> CargoPrices { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<ColorGlobal> ColorGlobals { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<Domain> Domains { get; set; }
        public DbSet<FollowList> FollowLists { get; set; }
        public DbSet<GiftPrice> GiftPrices { get; set; }
        public DbSet<MetaRule> MetaRules { get; set; }
        public DbSet<MoneyTransferInfo> MoneyTransferInfos { get; set; }
        public DbSet<New> News { get; set; }
        public DbSet<NewsLetter> NewsLetters { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderedItem> OrderedItems { get; set; }
        public DbSet<OrderHistory> OrderHistories { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentHistory> PaymentHistory { get; set; }
        public DbSet<ProductInCatalog> ProductInCatalogs { get; set; }
        public DbSet<ProductRate> ProductRates { get; set; }
        public DbSet<ProductDepartment> ProductDepartments { get; set; }
        public DbSet<ProductStyle> ProductStyles { get; set; }
        //public DbSet<ReturnPolicy> ReturnPolicies { get; set; }
        //public DbSet<ReturnPolicyType> ReturnPolicyTypes { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<SendingProvider> SendingProviders { get; set; }
        public DbSet<Size> Sizes { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<PriceCurrency> PriceCurrencies { get; set; }
        public DbSet<ContentPageLocalLang> ContentPageLocalLangs { get; set; }
        public DbSet<ContactMessage> ContactMessages { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<EmailTemplateLocalLang> EmailTemplateLocalLangs { get; set; }
        public DbSet<Faq> Faqs { get; set; }
        public DbSet<FaqLocalLang> FaqLocalLangs { get; set; }
        public DbSet<Lang> Langs { get; set; }
        public DbSet<UrlHistory> UrlHistories { get; set; }
        public DbSet<WebPos> WebPoses { get; set; }
        public DbSet<WebPosInstallment> WebPosInstallments { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDetail> ProductDetails { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<StoreAccount> StoreAccounts { get; set; }
        public DbSet<StoreAccountUser> StoreAccountUsers { get; set; }
        public DbSet<WebPosLog> WebPosLogs { get; set; }
        public DbSet<Quarter> Quarters { get; set; }
        public DbSet<User> Users { get; set; }
        //public DbSet<UserDetail> UserDetails { get; set; }
        public DbSet<UserInRole> UserInRoles { get; set; }
        public DbSet<Town> Towns { get; set; }
    }

}
