﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.EmailTemplates;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class EmailTemplateService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public EmailTemplateService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<EmailTemplateCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<EmailTemplateCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.EmailTemplates
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.EmailTemplates
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          select new EmailTemplateCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              EmailProviderID = p.EmailProviderID,
                                              MailType = p.MailType,
                                              Translations = p.EmailTemplateLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.EmailTemplates
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.EmailTemplates.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.ID).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new EmailTemplateCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus,
                      EmailProviderID=p.EmailProviderID,
                      MailType=p.MailType,
                      Translations = p.EmailTemplateLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<EmailTemplateCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<EmailTemplateCreateEditVM>();
             
            var allDomains = db.Domains.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allProviders = db.SendingProviders.Where(x => x.PStatus == Status.Active && x.StoreAccountID==storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);
            var allMailTypes = serviceUtils.ToSelectList<SendingContentType>(0);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new EmailTemplateCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllProviders = allProviders;
                result.Result.AllDomains = allDomains;
                result.Result.AllMailTypes = allMailTypes;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.EmailTemplates.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new EmailTemplateCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 MailType=p.MailType,
                 DomainID=p.DomainID,
                  EmailProviderID=p.EmailProviderID                 
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllProviders = allProviders;
                result.Result.AllDomains = allDomains;
                result.Result.AllMailTypes = allMailTypes;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var EmailTemplate = db.EmailTemplates.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (EmailTemplate != null)
            {
                EmailTemplate.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.EmailTemplateDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private EmailTemplate MapToDBObject(EmailTemplateCreateEditVM model, EmailTemplate dbModel)
        {
            var EmailTemplate = dbModel ?? new EmailTemplate();

            EmailTemplate.ID = model.ID;

            EmailTemplate.Name = model.Name;
            EmailTemplate.PStatus = model.PStatus;

            EmailTemplate.EmailProviderID = model.EmailProviderID;
            EmailTemplate.DomainID = model.DomainID;
            EmailTemplate.MailType = model.MailType;

            EmailTemplate.StoreAccountID = model.ID == 0 ? model.StoreID : EmailTemplate.StoreAccountID;
            EmailTemplate.ModifiedOn = DateTime.Now;
            if (EmailTemplate.ID != 0) EmailTemplate.CreatedOn = DateTime.Now;

            return EmailTemplate;

        }

        private EmailTemplateCreateEditVM MapToVM(EmailTemplate dbModel)
        {
            if (dbModel == null) return null;

            var EmailTemplate = new EmailTemplateCreateEditVM();
            EmailTemplate.ID = dbModel.ID;
            EmailTemplate.Name = dbModel.Name; 
            EmailTemplate.EmailProviderID = dbModel.EmailProviderID;
            EmailTemplate.DomainID = dbModel.DomainID;
            EmailTemplate.MailType = dbModel.MailType;

            EmailTemplate.Name = dbModel.Name;
            EmailTemplate.PStatus = dbModel.PStatus;

            return EmailTemplate;
        }

        public OperationResult<EmailTemplateCreateEditVM> Save(EmailTemplateCreateEditVM EmailTemplate)
        {
            var result = new OperationResult<EmailTemplateCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(EmailTemplate, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            EmailTemplate existing = null;

            if (EmailTemplate.ID != 0)
            {
                existing = db.EmailTemplates.Where(x => x.ID == EmailTemplate.ID).FirstOrDefault();
                existing = MapToDBObject(EmailTemplate, existing);
            }
            else
            {
                existing = MapToDBObject(EmailTemplate, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(EmailTemplate.ID == 0 ? QueueItemType.EmailTemplateCreated : QueueItemType.EmailTemplateUpdated, JsonConvert.SerializeObject(result.Result), EmailTemplate.StoreID);

            return result;

        }


    }
}
