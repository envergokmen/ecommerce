﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Stores;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class StoreAccountService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public StoreAccountService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }

        public StoreAccountViewModel GetStoreById(int StoreId)
        {
            string cacheKey = $"StoreAccount_{StoreId}";

            var Store = cache.Get<StoreAccountViewModel>(cacheKey);

            if (Store == null)
            {
                Store = db.StoreAccounts
                    .Where(x => x.ID == StoreId && x.PStatus == Status.Active)
                    .Select(x => new StoreAccountViewModel
                    {
                        CityId = x.CityId,
                        CountryId = x.CountryId,
                        TownId = x.TownId,
                        CompanyAddress = x.CompanyAddress,
                        CompanyEMail = x.CompanyEmail,
                        CompanyName = x.CompanyName,
                        CompanyPhone = x.CompanyPhone,
                        CompanyTaxInfo = x.CompanyTaxInfo,
                        CompanyTaxInfo2 = x.CompanyTaxInfo2,
                        RelatedUserId = x.RelatedUserId,
                        ID = x.ID,
                        StoreName = x.StoreName,
                        PStatus = x.PStatus

                    }).FirstOrDefault();

                if (Store != null)
                {
                    cache.Set(cacheKey, Store, DateTime.Now.AddHours(6));
                }
            }

            return Store;
        }

        public StoreAccountViewModel GetStoreByName(string name)
        {
            string cacheKey = $"StoreAccount_name_{name}";

            var Store = cache.Get<StoreAccountViewModel>(cacheKey);

            if (Store == null)
            {
                Store = db.StoreAccounts
                    .Where(x => x.StoreName == name && x.PStatus == Status.Active)
                    .Select(x => new StoreAccountViewModel
                    {
                        CityId = x.CityId,
                        CountryId = x.CountryId,
                        TownId = x.TownId,
                        CompanyAddress = x.CompanyAddress,
                        CompanyEMail = x.CompanyEmail,
                        CompanyName = x.CompanyName,
                        CompanyPhone = x.CompanyPhone,
                        CompanyTaxInfo = x.CompanyTaxInfo,
                        CompanyTaxInfo2 = x.CompanyTaxInfo2,
                        RelatedUserId = x.RelatedUserId,
                        ID = x.ID,
                        StoreName = x.StoreName,
                        PStatus = x.PStatus

                    }).FirstOrDefault();

                if (Store != null)
                {
                    cache.Set(cacheKey, Store, DateTime.Now.AddHours(6));
                }
            }

            return Store;
        }
        public PagedOperationListResult<StoreAccountCreateEditVM> GetAll(int page = 1, string searchKeyword = null, int pageSize = 10, int? userid = null)
        {
            var operationResult = new PagedOperationListResult<StoreAccountCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.StoreAccounts
                                  join u in db.StoreAccountUsers on p.ID equals u.StoreAccountId
                                  where u.ID == userid &&
                                  p.PStatus == Status.Active 
                                  && (p.StoreName.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.StoreAccounts
                                          join u in db.StoreAccountUsers on p.ID equals u.StoreAccountId
                                          where u.ID == userid && p.PStatus == Status.Active
                                          && (p.StoreName.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.ID
                                          select new StoreAccountCreateEditVM
                                          {
                                              CityId = p.CityId,
                                              CountryId = p.CountryId,
                                              TownId = p.TownId,
                                              CompanyAddress = p.CompanyAddress,
                                              CompanyEmail = p.CompanyEmail,
                                              CompanyName = p.CompanyName,
                                              CompanyPhone = p.CompanyPhone,
                                              CompanyTaxInfo = p.CompanyTaxInfo,
                                              CompanyTaxInfo2 = p.CompanyTaxInfo2,
                                              RelatedUserId = p.RelatedUserId,
                                              ID = p.ID,
                                              StoreName = p.StoreName,
                                              PStatus = p.PStatus
                                          })
                     .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.StoreAccounts
                                  join u in db.StoreAccountUsers on p.ID equals u.StoreAccountId
                                  where u.ID == userid && p.PStatus == Status.Active
                                  select p).Count();

                operationResult.Result = (from p in db.StoreAccounts
                                          join u in db.StoreAccountUsers on p.ID equals u.StoreAccountId
                                          where u.ID == userid && p.PStatus == Status.Active
                                          orderby p.ID
                                          select new StoreAccountCreateEditVM
                                          {
                                              CityId = p.CityId,
                                              CountryId = p.CountryId,
                                              TownId = p.TownId,
                                              CompanyAddress = p.CompanyAddress,
                                              CompanyEmail = p.CompanyEmail,
                                              CompanyName = p.CompanyName,
                                              CompanyPhone = p.CompanyPhone,
                                              CompanyTaxInfo = p.CompanyTaxInfo,
                                              CompanyTaxInfo2 = p.CompanyTaxInfo2,
                                              RelatedUserId = p.RelatedUserId,
                                              ID = p.ID,
                                              StoreName = p.StoreName,
                                              PStatus = p.PStatus
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<StoreAccountCreateEditVM> Get(int id)
        {
            var result = new OperationResult<StoreAccountCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new StoreAccountCreateEditVM();
                result.Result.AllStatuses = allStatuses;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.StoreAccounts.Where(x => x.ID == id).Select(p =>
             new StoreAccountCreateEditVM
             {
                 CityId = p.CityId,
                 CountryId = p.CountryId,
                 TownId = p.TownId,
                 CompanyAddress = p.CompanyAddress,
                 CompanyEmail = p.CompanyEmail,
                 CompanyName = p.CompanyName,
                 CompanyPhone = p.CompanyPhone,
                 CompanyTaxInfo = p.CompanyTaxInfo,
                 CompanyTaxInfo2 = p.CompanyTaxInfo2,
                 RelatedUserId = p.RelatedUserId,
                 ID = p.ID,
                 StoreName = p.StoreName,
                 PStatus = p.PStatus
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Store = db.StoreAccounts.Where(x => x.ID == id && x.ID == storeId).FirstOrDefault();

            if (Store != null)
            {
                Store.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.StoreDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        public OperationResult<StoreAccountCreateEditVM> Save(StoreAccountCreateEditVM Store)
        {
            var result = new OperationResult<StoreAccountCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Store, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            StoreAccount existing = null;

            if (Store.ID != 0)
            {
                existing = db.StoreAccounts.Where(x => x.ID == Store.ID).FirstOrDefault();
                existing = MapToDBObject(Store, existing);
            }
            else
            {
                existing = MapToDBObject(Store, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Store.ID == 0 ? QueueItemType.StoreCreated : QueueItemType.StoreUpdated, JsonConvert.SerializeObject(result.Result), Store.StoreID);

            return result;

        }
         
        private StoreAccount MapToDBObject(StoreAccountCreateEditVM model, StoreAccount dbModel)
        {
            var Store = dbModel ?? new StoreAccount();

            Store.ID = model.ID;

            Store.StoreName = model.StoreName;
            Store.PStatus = model.PStatus;
            Store.RelatedUserId = model.RelatedUserId;
            Store.TownId = model.TownId;
            Store.CityId = model.CityId;
            Store.CountryId = model.CountryId;
            Store.CompanyName = model.CompanyName;
            Store.CompanyAddress = model.CompanyAddress;
            Store.CompanyPhone = model.CompanyPhone;
            Store.CompanyTaxInfo = model.CompanyTaxInfo;
            Store.CompanyTaxInfo2 = model.CompanyTaxInfo2;
            Store.CompanyEmail = model.CompanyEmail;

            Store.ModifiedOn = DateTime.Now;
            if (Store.ID != 0) Store.CreatedOn = DateTime.Now;

            return Store;

        }

        private StoreAccountCreateEditVM MapToVM(StoreAccount model)
        {
            if (model == null) return null;

            var Store = new StoreAccountCreateEditVM();
            Store.StoreName = model.StoreName;
            Store.PStatus = model.PStatus;
            Store.RelatedUserId = model.RelatedUserId;
            Store.TownId = model.TownId;
            Store.CityId = model.CityId;
            Store.CountryId = model.CountryId;
            Store.CompanyName = model.CompanyName;
            Store.CompanyAddress = model.CompanyAddress;
            Store.CompanyPhone = model.CompanyPhone;
            Store.CompanyTaxInfo = model.CompanyTaxInfo;
            Store.CompanyTaxInfo2 = model.CompanyTaxInfo2;
            Store.CompanyEmail = model.CompanyEmail;

            return Store;
        }

    }
}
