﻿using Ecom.Core;
using Ecom.Models;
using Ecom.ViewModels.Addresses;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecom.Services
{
    public class AddressService
    {

        private IMemoryCache cache;
        private readonly EcomContext db;
        private readonly ServiceUtils serviceUtils;

        public AddressService(IMemoryCache _cache, EcomContext _db, ServiceUtils _serviceUtils)
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
        }


        #region CityTownCountry

        public string GetCityName(int CityID)
        {
            var city = GetCity(CityID);
            if (city != null)
            {
                return city.Name;
            }
            else
            {
                return "";
            }
        }

        public CityVM GetCityByName(string Name)
        {

            string Cachekey = String.Concat("CityDetail" + Name);

            var city = cache.Get<CityVM>(Cachekey);

            if (city == null)
            {
                city = db.Cities.Where(x => x.Name == Name)
                    .Select(x => new CityVM { CountryId = x.CountryID, Id = x.ID, Name = x.Name })
                    .FirstOrDefault();

                if (city != null)
                    cache.Set(Cachekey, city, DateTime.UtcNow.AddDays(2));
            }

            return city;
        }

        public string GetTownName(int TownID)
        {
            TownVM town = GetTown(TownID);  //db.Towns.Where(x => x.ID == TownID).FirstOrDefault();
            if (town != null)
            {
                return town.Name;
            }
            else
            {
                return "";
            }
        }

        public List<TownVM> GetTownList(int? CityID = 0)
        {
            string Cachekey = String.Concat("Towns" + CityID);

            List<TownVM> townList = cache.Get<List<TownVM>>(Cachekey);

            if (townList == null)
            {
                if (CityID == null || CityID == 0)

                    townList = db.Towns.OrderBy(x => x.Name)
                        .Select(x => new TownVM { CityId = x.CityID, Id = x.ID, Name = x.Name })
                        .ToList();
                else
                    townList = db.Towns.Where(x => x.CityID == CityID).OrderBy(x => x.Name)
                         .Select(x => new TownVM { CityId = x.CityID, Id = x.ID, Name = x.Name })
                        .ToList();


                cache.Set(Cachekey, townList, DateTime.UtcNow.AddDays(10));

            }

            return townList;
        }

        public List<CityVM> GetCityList(int? CountryID = 0)
        {

            string Cachekey = String.Concat("Cities" + CountryID);
            List<CityVM> cityList = cache.Get<List<CityVM>>(Cachekey);

            if (cityList == null)
            {
                if (CountryID == null || CountryID == 0)
                    cityList = db.Cities.OrderBy(x => x.Name)
                        .Select(x => new CityVM { Name = x.Name, CountryId = x.CountryID, Id = x.ID })
                        .ToList();
                else
                    cityList = db.Cities.Where(x => x.CountryID == CountryID).OrderBy(x => x.Name)
                        .Select(x => new CityVM { Name = x.Name, CountryId = x.CountryID, Id = x.ID })
                        .ToList();

                cache.Set(Cachekey, cityList, DateTime.UtcNow.AddDays(10));
            }

            return cityList;
        }

        public List<CountryVM> GetCountyList()
        {
            string Cachekey = String.Concat("CountryListCache");

            List<CountryVM> countryList = cache.Get<List<CountryVM>>(Cachekey);

            if (countryList == null)
            {
                countryList = db.Countries.OrderBy(x => x.Name)
                .Select(x => new CountryVM { CountryCode = x.CountryCode, Id = x.ID, Name = x.Name }).ToList();

                cache.Set(Cachekey, countryList, DateTime.UtcNow.AddDays(10));
            }

            if (countryList == null) countryList = new List<CountryVM>();

            return countryList;
        }

        //TODO:consider adding domainid
        public List<CountryVM> GetValidShippingCountryList(int storeId, int priceCurrencyId)
        {
            string Cachekey = String.Concat($"CountryListCache_Shipping_{storeId}_{priceCurrencyId}");

            List<CountryVM> countryList = cache.Get<List<CountryVM>>(Cachekey);

            if (countryList == null)
            {
                countryList = (from p in db.CargoPrices
                                     join dt in db.Countries on p.CountryID equals dt.ID
                               where p.StoreAccountID == storeId && p.PriceCurrencyID == priceCurrencyId
                               && p.PStatus == Status.Active
                               orderby p.ID
                               select new CountryVM { CountryCode = dt.CountryCode, Id = dt.ID, Name = dt.Name }).ToList();

                cache.Set(Cachekey, countryList, DateTime.UtcNow.AddHours(1));
            }

            if (countryList == null) countryList = new List<CountryVM>();

            return countryList;
        }

        public TownVM GetTown(int ID)
        {

            string Cachekey = String.Concat("TownDetail" + ID);
            TownVM town = cache.Get<TownVM>(Cachekey);

            if (town == null)
            {
                town = db.Towns.Where(x => x.ID == ID).Select(x => new TownVM
                {
                    CityId = x.CityID,
                    Id = x.ID,
                    Name = x.Name
                }).FirstOrDefault();

                if (town != null)
                    cache.Set(Cachekey, town, DateTime.UtcNow.AddDays(2));
            }

            return town;
        }

        public TownVM GetTownByName(string Name)
        {

            string Cachekey = String.Concat("TownDetail" + Name);
            TownVM town = cache.Get<TownVM>(Cachekey);

            if (town == null)
            {
                town = db.Towns.Where(x => x.Name == Name).Select(x => new TownVM
                {
                    CityId = x.CityID,
                    Id = x.ID,
                    Name = x.Name
                }).FirstOrDefault();

                if (town != null)
                    cache.Set(Cachekey, town, DateTime.UtcNow.AddDays(2));
            }

            return town;
        }

        public CountryVM GetCountry(int ID)
        {

            string Cachekey = String.Concat("CountryDetail" + ID);
            CountryVM country = cache.Get<CountryVM>(Cachekey);

            if (country == null)
            {
                country = db.Countries.Where(x => x.ID == ID)
                    .Select(x => new CountryVM
                    {

                        CountryCode = x.CountryCode,
                        Id = x.ID,
                        Name = x.Name
                    }).FirstOrDefault();

                if (country != null)
                {
                    cache.Set(Cachekey, country, DateTime.UtcNow.AddDays(2));
                }
            }

            return country;
        }

        public CityVM GetCity(int ID)
        {

            string Cachekey = String.Concat("CityDetail" + ID);
            var city = cache.Get<CityVM>(Cachekey);

            if (city == null)
            {
                city = db.Cities.Where(x => x.ID == ID)
                    .Select(x => new CityVM
                    {

                        CountryId = x.CountryID,
                        Id = x.ID,
                        Name = x.Name
                    }).FirstOrDefault();

                if (city != null)
                {
                    cache.Set(Cachekey, city, DateTime.UtcNow.AddDays(2));
                }

            }

            return city;
        }

        #endregion

        #region UserAddress
        public List<AddressVM> GetUserAddresses(int storeId, int? userId, string key)
        {
            List<AddressVM> addresses = (from p in db.Addresses
                                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                                         && (p.UserID == userId || (p.Session == key || p.UserID == null))
                                         orderby p.ID
                                         select new AddressVM
                                         {
                                             Id = p.ID,
                                             CityId = p.CityID,
                                             CountryId = p.CountryID,
                                             Name = p.Name,
                                             TownId = p.TownID,
                                             UserID = p.UserID,
                                             Phone = p.Phone1,
                                             Detail = p.Detail

                                         }).ToList();

            MapNames(addresses);

            return addresses;
        }

        public AddressVM GetUserAddress(int storeId, int addresId, int? userId, string key)
        {
            if (addresId == 0) return null;
            var address = (from p in db.Addresses
                           where p.StoreAccountID == storeId && p.PStatus == Status.Active
                           && p.ID == addresId
                           && (p.UserID == userId || (p.Session == key || p.UserID == null))
                           orderby p.ID
                           select new AddressVM
                           {
                               Id = p.ID,
                               CityId = p.CityID,
                               CountryId = p.CountryID,
                               Name = p.Name,
                               TownId = p.TownID,
                               UserID = p.UserID,
                               Detail = p.Detail,
                               Phone = p.Phone1,
                               Email = p.Email

                           }).FirstOrDefault();

            MapNames(address);

            return address;
        }


        public StandartOperationResult Create(AddressVM address)
        {
            var result = new StandartOperationResult();

            var validationResult = serviceUtils.TryToValidateModel(address, true);
            if (!validationResult.Status)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                result.Status = false;
                return result;
            }

            if ((!address.UserID.HasValue || address.UserID <= 0) && string.IsNullOrWhiteSpace(address.Session))
            {
                result.SetError("Please login or continue as guest");
                return result;
            }

            var createdAddress = new Address
            {
                CityID = address.CityId,
                CountryID = address.CountryId,
                TownID = address.TownId,
                CityName = address.CityName,
                TownName = address.TownName,
                Detail = address.Detail,
                Phone1 = address.Phone,
                Email = address.Email,
                UserID = address.UserID,
                Session = address.UserID.HasValue && address.UserID.Value > 0 ? null : address.Session,
                StoreAccountID = address.StoreId
            };

            db.Addresses.Add(createdAddress);

            db.SaveChanges();
            result.SetSuccessAndClearError(createdAddress.ID);

            return result;
        }

        public StandartOperationResult Update(AddressVM address)
        {
            var result = new StandartOperationResult();

            var validationResult = serviceUtils.TryToValidateModel(address, true);
            if (!validationResult.Status)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                result.Status = false;
                return result;
            }

            if ((!address.UserID.HasValue || address.UserID <= 0) && string.IsNullOrWhiteSpace(address.Session))
            {
                result.SetError("Please login or continue as guest");
                return result;
            }

            var existingAddress = (from p in db.Addresses
                                   where p.StoreAccountID == address.StoreId && p.PStatus == Status.Active
                                   && p.ID == address.Id
                                   && (p.UserID == address.UserID || (p.Session == address.Session || p.UserID == null))
                                   select p).FirstOrDefault();

            if (existingAddress != null)
            {
                existingAddress.CityID = address.CityId;
                existingAddress.CountryID = address.CountryId;
                existingAddress.TownID = address.TownId;
                existingAddress.CityName = address.CityName;
                existingAddress.TownName = address.TownName;
                existingAddress.Detail = address.Detail;
                existingAddress.Phone1 = address.Phone;
                existingAddress.Email = address.Email;

                db.SaveChanges();
                result.SetSuccessAndClearError(existingAddress.ID);
            }
            else
            {
                result.SetAsNotFound();
            }

            return result;
        }

        public StandartOperationResult Delete(int storeId, int addresId, int? userId, string key)
        {
            var result = new StandartOperationResult();

            var existingAddress = (from p in db.Addresses
                                   where p.StoreAccountID == storeId && p.PStatus == Status.Active
                                   && p.ID == addresId
                                   && (p.UserID == userId || (p.Session == key || p.UserID == null))
                                   select p).FirstOrDefault();

            if (existingAddress != null)
            {
                existingAddress.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(existingAddress.ID);
            }
            else
            {
                result.SetAsNotFound();
            }

            return result;
        }


        private void MapNames(List<AddressVM> addresses)
        {
            foreach (var item in addresses)
            {
                MapNames(item);
            }
        }

        private void MapNames(AddressVM item)
        {
            if (item.CityId.HasValue && item.CityName == null)
            {
                item.CityName = GetCity(item.CityId.Value)?.Name;
            }

            if (item.TownId.HasValue && item.TownName == null)
            {
                item.TownName = GetTown(item.TownId.Value)?.Name;
            }

            if (item.CountryId.HasValue && item.CountryName == null)
            {
                item.CountryName = GetCountry(item.CountryId.Value)?.Name;
            }
        }

        #endregion

    }
}
