﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.SendingProviders;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class SendingProviderService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public SendingProviderService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<SendingProviderCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<SendingProviderCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.SendingProviders
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.SendingProviders
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          select new SendingProviderCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              ApiEndPoint = p.ApiEndPoint,
                                              EnableSsl = p.EnableSsl,
                                              EuEmailColumnName = p.EuEmailColumnName,
                                              EuNameColumnName = p.EuNameColumnName,
                                              EuroMessageApiPassword = p.EuroMessageApiPassword,
                                              EuroMessageApiUser = p.EuroMessageApiUser,
                                              EuroMessageFromAddress = p.EuroMessageFromAddress,
                                              EuroMessageFromName = p.EuroMessageFromName,
                                              EuroMessageReplyAddress = p.EuroMessageReplyAddress,
                                              MailApiKey = p.MailApiKey,
                                              MailTransCode = p.MailTransCode,
                                              Password = p.Password,
                                              ProviderType = p.ProviderType,
                                              SenderName = p.SenderName,
                                              SmptHost = p.SmptHost,
                                              SmptPort = p.SmptPort,
                                              UserName = p.UserName
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.SendingProviders
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.SendingProviders.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.ID).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new SendingProviderCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus,
                      ApiEndPoint = p.ApiEndPoint,
                      EnableSsl = p.EnableSsl,
                      EuEmailColumnName = p.EuEmailColumnName,
                      EuNameColumnName = p.EuNameColumnName,
                      EuroMessageApiPassword = p.EuroMessageApiPassword,
                      EuroMessageApiUser = p.EuroMessageApiUser,
                      EuroMessageFromAddress = p.EuroMessageFromAddress,
                      EuroMessageFromName = p.EuroMessageFromName,
                      EuroMessageReplyAddress = p.EuroMessageReplyAddress,
                      MailApiKey = p.MailApiKey,
                      MailTransCode = p.MailTransCode,
                      Password = p.Password,
                      ProviderType = p.ProviderType,
                      SenderName = p.SenderName,
                      SmptHost = p.SmptHost,
                      SmptPort = p.SmptPort,
                      UserName = p.UserName
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<SendingProviderCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<SendingProviderCreateEditVM>();

            var allStatuses = serviceUtils.GetStatusValues(1);
            var allSendingProviderTypes = serviceUtils.ToSelectListInt<SendingProviderType>(0);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new SendingProviderCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllSendingProviderTypes = allSendingProviderTypes;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.SendingProviders.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new SendingProviderCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 ApiEndPoint = p.ApiEndPoint,
                 EnableSsl = p.EnableSsl,
                 EuEmailColumnName = p.EuEmailColumnName,
                 EuNameColumnName = p.EuNameColumnName,
                 EuroMessageApiPassword = p.EuroMessageApiPassword,
                 EuroMessageApiUser = p.EuroMessageApiUser,
                 EuroMessageFromAddress = p.EuroMessageFromAddress,
                 EuroMessageFromName = p.EuroMessageFromName,
                 EuroMessageReplyAddress = p.EuroMessageReplyAddress,
                 MailApiKey = p.MailApiKey,
                 MailTransCode = p.MailTransCode,
                 Password = p.Password,
                 ProviderType = p.ProviderType,
                 SenderName = p.SenderName,
                 SmptHost = p.SmptHost,
                 SmptPort = p.SmptPort,
                 UserName = p.UserName
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllSendingProviderTypes = allSendingProviderTypes;
                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var SendingProvider = db.SendingProviders.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (SendingProvider != null)
            {
                SendingProvider.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.SendingProviderDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private SendingProvider MapToDBObject(SendingProviderCreateEditVM model, SendingProvider dbModel)
        {
            var SendingProvider = dbModel ?? new SendingProvider();

            SendingProvider.ID = model.ID; 
            SendingProvider.Name = model.Name;
            SendingProvider.PStatus = model.PStatus;
            SendingProvider.ApiEndPoint = model.ApiEndPoint;
            SendingProvider.EnableSsl = model.EnableSsl;
            SendingProvider.EuEmailColumnName = model.EuEmailColumnName;
            SendingProvider.EuNameColumnName = model.EuNameColumnName;
            SendingProvider.EuroMessageApiPassword = model.EuroMessageApiPassword;
            SendingProvider.EuroMessageApiUser = model.EuroMessageApiUser;
            SendingProvider.EuroMessageFromAddress = model.EuroMessageFromAddress;
            SendingProvider.EuroMessageFromName = model.EuroMessageFromName;
            SendingProvider.EuroMessageReplyAddress = model.EuroMessageReplyAddress;
            SendingProvider.MailApiKey = model.MailApiKey;
            SendingProvider.MailTransCode = model.MailTransCode;
            SendingProvider.Password = model.Password;
            SendingProvider.ProviderType = model.ProviderType;
            SendingProvider.SenderName = model.SenderName;
            SendingProvider.SmptHost = model.SmptHost;
            SendingProvider.SmptPort = model.SmptPort;
            SendingProvider.UserName = model.UserName;
             
            SendingProvider.StoreAccountID = model.ID == 0 ? model.StoreID : SendingProvider.StoreAccountID;
            SendingProvider.ModifiedOn = DateTime.Now;
            if (SendingProvider.ID != 0) SendingProvider.CreatedOn = DateTime.Now;

            return SendingProvider;

        }

        private SendingProviderCreateEditVM MapToVM(SendingProvider dbModel)
        {
            if (dbModel == null) return null;

            var SendingProvider = new SendingProviderCreateEditVM();
            SendingProvider.ID = dbModel.ID;
            SendingProvider.Name = dbModel.Name;
            SendingProvider.PStatus = dbModel.PStatus;

            SendingProvider.ID = dbModel.ID;
            SendingProvider.Name = dbModel.Name;
            SendingProvider.PStatus = dbModel.PStatus;
            SendingProvider.ApiEndPoint = dbModel.ApiEndPoint;
            SendingProvider.EnableSsl = dbModel.EnableSsl;
            SendingProvider.EuEmailColumnName = dbModel.EuEmailColumnName;
            SendingProvider.EuNameColumnName = dbModel.EuNameColumnName;
            SendingProvider.EuroMessageApiPassword = dbModel.EuroMessageApiPassword;
            SendingProvider.EuroMessageApiUser = dbModel.EuroMessageApiUser;
            SendingProvider.EuroMessageFromAddress = dbModel.EuroMessageFromAddress;
            SendingProvider.EuroMessageFromName = dbModel.EuroMessageFromName;
            SendingProvider.EuroMessageReplyAddress = dbModel.EuroMessageReplyAddress;
            SendingProvider.MailApiKey = dbModel.MailApiKey;
            SendingProvider.MailTransCode = dbModel.MailTransCode;
            SendingProvider.Password = dbModel.Password;
            SendingProvider.ProviderType = dbModel.ProviderType;
            SendingProvider.SenderName = dbModel.SenderName;
            SendingProvider.SmptHost = dbModel.SmptHost;
            SendingProvider.SmptPort = dbModel.SmptPort;
            SendingProvider.UserName = dbModel.UserName;
             
            return SendingProvider;
        }

        public OperationResult<SendingProviderCreateEditVM> Save(SendingProviderCreateEditVM SendingProvider)
        {
            var result = new OperationResult<SendingProviderCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(SendingProvider, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            SendingProvider existing = null;

            if (SendingProvider.ID != 0)
            {
                existing = db.SendingProviders.Where(x => x.ID == SendingProvider.ID).FirstOrDefault();
                existing = MapToDBObject(SendingProvider, existing);
            }
            else
            {
                existing = MapToDBObject(SendingProvider, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(SendingProvider.ID == 0 ? QueueItemType.SendingProviderCreated : QueueItemType.SendingProviderUpdated, JsonConvert.SerializeObject(result.Result), SendingProvider.StoreID);

            return result;

        }


    }
}
