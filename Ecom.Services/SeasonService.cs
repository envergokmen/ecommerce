﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Seasons;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class SeasonService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public SeasonService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<SeasonCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<SeasonCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Seasons
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Seasons
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new SeasonCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              SortNo=p.SortNo,
                                              PStatus = p.PStatus
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Seasons
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.Seasons.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new SeasonCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      SortNo = p.SortNo,
                      PStatus = p.PStatus
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<SeasonCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<SeasonCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new SeasonCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Seasons.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new SeasonCreateEditVM
             {
                 ID = p.ID,
                 SortNo = p.SortNo,
                 Name = p.Name,
                 PStatus = p.PStatus,
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Season = db.Seasons.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Season != null)
            {
                Season.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.SeasonDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Season MapToDBObject(SeasonCreateEditVM model, Season dbModel)
        {
            var Season = dbModel ?? new Season();

            Season.ID = model.ID;

            Season.Name = model.Name;
            Season.PStatus = model.PStatus;
            Season.ImagePath = "";

            Season.StoreAccountID = model.ID == 0 ? model.StoreID : Season.StoreAccountID;
            Season.ModifiedOn = DateTime.Now;
            if (Season.ID != 0) Season.CreatedOn = DateTime.Now;

            return Season;

        }

        private SeasonCreateEditVM MapToVM(Season dbModel)
        {
            if (dbModel == null) return null;

            var Season = new SeasonCreateEditVM();
            Season.ID = dbModel.ID;
            Season.Name = dbModel.Name;
            Season.PStatus = dbModel.PStatus;
            Season.SortNo = dbModel.SortNo;

            return Season;
        }

        public OperationResult<SeasonCreateEditVM> Save(SeasonCreateEditVM Season)
        {
            var result = new OperationResult<SeasonCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Season, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Season existing = null;

            if (Season.ID != 0)
            {
                existing = db.Seasons.Where(x => x.ID == Season.ID).FirstOrDefault();
                existing = MapToDBObject(Season, existing);
            }
            else
            {
                existing = MapToDBObject(Season, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Season.ID == 0 ? QueueItemType.SeasonCreated : QueueItemType.SeasonUpdated, JsonConvert.SerializeObject(result.Result), Season.StoreID);

            return result;

        }


    }
}
