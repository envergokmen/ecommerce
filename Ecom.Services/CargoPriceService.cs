﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.CargoPrices;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class CargoPriceService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public CargoPriceService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<CargoPriceCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<CargoPriceCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.CargoPrices
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Cargo.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.CargoPrices
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Cargo.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.ID
                                          select new CargoPriceCreateEditVM
                                          {
                                              ID = p.ID,
                                              Amount = p.Amount,
                                              PStatus = p.PStatus,
                                              FreeShippingLimit = p.FreeShippingLimit,
                                              CurrencyCode = p.PriceCurrency.CurrencyCode,
                                              LogoPath = p.Cargo.LogoPath,
                                              CargoCompany = p.Cargo.Name,
                                              CountryName = p.Country.Name
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.CargoPrices
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.CargoPrices.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.ID).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new CargoPriceCreateEditVM
                  {
                      ID = p.ID,
                      Amount = p.Amount,
                      FreeShippingLimit = p.FreeShippingLimit,
                      CurrencyCode = p.PriceCurrency.CurrencyCode,
                      PStatus = p.PStatus,
                      CargoCompany = p.Cargo.Name,
                      CountryName = p.Country.Name
                  }

                ).ToList();
            }

            //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<CargoPriceCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<CargoPriceCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allCargoes = db.Cargoes.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allCountries = db.Countries.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();


            var allDomains = db.Domains.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new CargoPriceCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllCargoes = allCargoes;
                result.Result.AllCountries = allCountries;
                result.Result.AllCurrencies = allCurrencies;
                result.Result.AllDomains = allDomains;

                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.CargoPrices.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new CargoPriceCreateEditVM
             {
                 ID = p.ID,
                 Amount = p.Amount,
                 FreeShippingLimit = p.FreeShippingLimit,
                 CargoID = p.CargoID,
                 CountryID = p.CountryID,
                 StoreAccountID = p.StoreAccountID,
                 PriceCurrencyID = p.PriceCurrencyID,
                 DomainID = p.DomainID,
                 PStatus = p.PStatus,
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;

                result.Result.AllCargoes = allCargoes;
                result.Result.AllCountries = allCountries;
                result.Result.AllCurrencies = allCurrencies;
                result.Result.AllDomains = allDomains;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var CargoPrice = db.CargoPrices.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (CargoPrice != null)
            {
                CargoPrice.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.CargoPriceDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private CargoPrice MapToDBObject(CargoPriceCreateEditVM model, CargoPrice dbModel)
        {
            var CargoPrice = dbModel ?? new CargoPrice();

            CargoPrice.ID = model.ID;

            CargoPrice.Amount = model.Amount;
            CargoPrice.FreeShippingLimit = model.FreeShippingLimit;
            CargoPrice.CargoID = model.CargoID;
            CargoPrice.DomainID = model.DomainID;
            CargoPrice.PriceCurrencyID = model.PriceCurrencyID;
            CargoPrice.CountryID = model.CountryID;

            CargoPrice.PStatus = model.PStatus;

            CargoPrice.StoreAccountID = model.ID == 0 ? model.StoreID : CargoPrice.StoreAccountID;
            CargoPrice.ModifiedOn = DateTime.Now;
            if (CargoPrice.ID != 0) CargoPrice.CreatedOn = DateTime.Now;

            return CargoPrice;

        }

        private CargoPriceCreateEditVM MapToVM(CargoPrice dbModel)
        {
            if (dbModel == null) return null;

            var CargoPrice = new CargoPriceCreateEditVM();
            CargoPrice.ID = dbModel.ID;
            CargoPrice.PStatus = dbModel.PStatus;
            CargoPrice.Amount = dbModel.Amount;
            CargoPrice.FreeShippingLimit = dbModel.FreeShippingLimit;
            CargoPrice.CargoID = dbModel.CargoID;
            CargoPrice.DomainID = dbModel.DomainID;
            CargoPrice.PriceCurrencyID = dbModel.PriceCurrencyID;
            CargoPrice.CountryID = dbModel.CountryID;

            return CargoPrice;
        }

        public OperationResult<CargoPriceCreateEditVM> Save(CargoPriceCreateEditVM CargoPrice)
        {
            var result = new OperationResult<CargoPriceCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(CargoPrice, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            CargoPrice existing = null;

            if (CargoPrice.ID != 0)
            {
                existing = db.CargoPrices.Where(x => x.ID == CargoPrice.ID).FirstOrDefault();
                existing = MapToDBObject(CargoPrice, existing);
            }
            else
            {
                existing = MapToDBObject(CargoPrice, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(CargoPrice.ID == 0 ? QueueItemType.CargoPriceCreated : QueueItemType.CargoPriceUpdated, JsonConvert.SerializeObject(result.Result), CargoPrice.StoreID);

            return result;

        }

        public List<CargoPriceVM> GetCargoPricesForCountry(int storeId, int countryId, int priceCurrencyId)
        {
            string cacheKey = $"CargoPrices_By_CountryID_{countryId}-{storeId}";

            var cargoPrices = cache.Get<List<CargoPriceVM>>(cacheKey);

            //TODO:add cache
            if (cargoPrices == null || true)
            {
                cargoPrices = (from p in db.CargoPrices
                               where p.StoreAccountID == storeId && p.PriceCurrencyID == priceCurrencyId
                               && p.PStatus == Status.Active && p.CountryID == countryId
                               orderby p.ID
                               select new CargoPriceVM
                               {
                                   Id = p.ID,
                                   Amount = p.Amount,
                                   PStatus = p.PStatus,
                                   FreeShippingLimit = p.FreeShippingLimit,
                                   CurrencyCode = p.PriceCurrency.CurrencyCode,
                                   LogoPath = p.Cargo.LogoPath,
                                   CargoCompany = p.Cargo.Name,
                                   CountryName = p.Country.Name

                               }).ToList();

                if (cargoPrices != null)
                {
                    cache.Set(cacheKey, cargoPrices, DateTime.Now.AddHours(1));
                }
            }

            return cargoPrices;
        }

        //Todo:consider adding domainid
        public List<CargoPriceVM> GetAllCargoPrices(int storeId, int priceCurrencyId)
        {
            string cacheKey = $"CargoPrices_All-{storeId}";

            var cargoPrices = cache.Get<List<CargoPriceVM>>(cacheKey);

            //TODO:add cache
            if (cargoPrices == null || true)
            {
                cargoPrices = (from p in db.CargoPrices
                               where p.StoreAccountID == storeId && p.PriceCurrencyID == priceCurrencyId
                               && p.PStatus == Status.Active
                               orderby p.ID
                               select new CargoPriceVM
                               {
                                   Id = p.ID,
                                   Amount = p.Amount,
                                   PStatus = p.PStatus,
                                   CountryID = p.CountryID,
                                   FreeShippingLimit = p.FreeShippingLimit,
                                   CurrencyCode = p.PriceCurrency.CurrencyCode,
                                   LogoPath = p.Cargo.LogoPath,
                                   CargoCompany = p.Cargo.Name,
                                   CountryName = p.Country.Name

                               }).ToList();

                if (cargoPrices != null)
                {
                    cache.Set(cacheKey, cargoPrices, DateTime.Now.AddHours(1));
                }
            }

            return cargoPrices;
        }

        public CargoPriceVM GetCargoPriceById(int storeId, int priceCurrencyId, int id)
        {
            if (id == 0) return null;

            string cacheKey = $"CargoPrice_By_Store_Cur_ID-{storeId}-{priceCurrencyId}-{id}";

            var cargoPrice = cache.Get<CargoPriceVM>(cacheKey);

            //TODO:add cache
            if (cargoPrice == null || true)
            {
                cargoPrice = (from p in db.CargoPrices
                               where p.StoreAccountID == storeId && p.PriceCurrencyID == priceCurrencyId && p.ID == id
                               && p.PStatus == Status.Active
                               orderby p.ID
                               select new CargoPriceVM
                               {
                                   Id = p.ID,
                                   Amount = p.Amount,
                                   PStatus = p.PStatus,
                                   CountryID = p.CountryID,
                                   FreeShippingLimit = p.FreeShippingLimit,
                                   CurrencyCode = p.PriceCurrency.CurrencyCode,
                                   LogoPath = p.Cargo.LogoPath,
                                   CargoCompany = p.Cargo.Name,
                                   CountryName = p.Country.Name,
                                   CargoID=p.CargoID

                               }).FirstOrDefault();

                if (cargoPrice != null)
                {
                    cache.Set(cacheKey, cargoPrice, DateTime.Now.AddHours(1));
                }
            }

            return cargoPrice;
        }


    }
}
