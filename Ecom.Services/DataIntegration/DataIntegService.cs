﻿using Ecom.Core;
using Ecom.Models;
using Ecom.ViewModels.DataIntegration;
using Ecom.ViewModels.Domains;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Ecom.Services
{
    public class DataIntegService
    {
        private IMemoryCache cache;
        private UserService userService;
        private readonly EcomContext db;
        private const string DataFolder = "DataInteg";

        public DataIntegService(UserService _userService, IMemoryCache _cache, EcomContext _db)
        {
            cache = _cache;
            userService = _userService;
            db = _db;
        }

        public void copyProducts()
        {

            var pinic = db.ProductInCatalogs.Where(c => c.PStatus == Status.Active).ToList();

            for (int i = 0; i < 200; i++)
            {
                foreach (var item in pinic)
                {
                    db.ProductInCatalogs.Add(new ProductInCatalog
                    {
                        CatalogID = item.CatalogID,
                        ProductID = item.ProductID,
                        PStatus = Status.Active,
                        ColorID = item.ColorID,
                        StoreAccountID=item.StoreAccountID
                    });
                }

                db.SaveChanges();
            }
           


        }

        public void CreateCities(string roothPath)
        {

            var countriesFile = roothPath + "\\" + DataFolder + "\\countries.json";
            var citiesFile = roothPath + "\\" + DataFolder + "\\states.json";
            var townsFile = roothPath + "\\" + DataFolder + "\\cities.json";

            if (File.Exists(countriesFile))
            {

                var countires = JsonConvert.DeserializeObject<List<countryvm>>(File.ReadAllText(countriesFile));
                var cities = JsonConvert.DeserializeObject<List<cityvm>>(File.ReadAllText(citiesFile));
                var towns = JsonConvert.DeserializeObject<List<townvm>>(File.ReadAllText(townsFile));


                foreach (var country in countires)
                {

                    var countrydb = new Country();
                    countrydb.Name = country.name;
                    countrydb.CountryCode = country.sortname;
                    db.Countries.Add(countrydb);
                    //db.SaveChanges();

                    var countryCities = cities.Where(x => x.country_id == country.id).ToList();

                    foreach (var countryCity in countryCities)
                    {
                        City dbCity = new City();
                        dbCity.Name = countryCity.name;
                        dbCity.Country = countrydb;
                        db.Cities.Add(dbCity);
                        //db.SaveChanges();

                        var cityTowns = towns.Where(x => x.state_id == countryCity.id).ToList();

                        foreach (var cityTown in cityTowns)
                        {
                            Town town = new Town();
                            town.City = dbCity;
                            town.Name = cityTown.name;
                            db.Towns.Add(town);
                            //db.SaveChanges();

                        }

                    }

                    db.SaveChanges();

                }


            }
        }


        public void Install(string roothPath)
        {
            CreateCities(roothPath);

            Lang lngEN, lngTR;
            PriceCurrency curEUR, curUsd, curTRY;

            CreateCurrencyAndLangs(out lngEN, out lngTR, out curEUR, out curUsd, out curTRY);

            var userCountry = db.Countries.FirstOrDefault(x => x.CountryCode == "TR");
           
            var storeUSer = userService.CreateStoreUser(new ViewModels.RegisterStoreStoreVM
            {
                CompanyName = "Ecom",
                Name = "Enver GÖKMEN",
                StoreName = "Test",
                AllowTerms = true,
                UserName = "envergokmen@gmail.com",
                PassWord = "123456",
                CountryId = userCountry!=null ? userCountry.ID : db.Countries.FirstOrDefault()?.ID ?? null,
            });

            CreateDefaultDomainForWebsite(lngEN, curEUR, storeUSer.Result.StoreAccountID.Value);

            SetDefaultCurrenciesAndLangForCountries(lngEN, lngTR, curEUR, curUsd, curTRY);

        }

        private void SetDefaultCurrenciesAndLangForCountries(Lang lngEN, Lang lngTR, PriceCurrency curEUR, PriceCurrency curUSD, PriceCurrency curTRY)
        {
            if (curTRY == null)
            {
                throw new ArgumentNullException(nameof(curTRY));
            }

            var allcountries = db.Countries.ToList();

            foreach (var country in allcountries)
            {
                country.DefaultLangID = lngEN.ID;
                country.DefaultPriceCurrencyID = curUSD.ID;

                var euCountries = new string[]
                {
                     "AL", "AD", "AZ", "AT", "AM", "BE", "BA", "BG", "BY", "HR", "CY", "CZ", "DK", "EE", "FO", "FI", "AX", "FR", "GE", "DE", "GI", "GR", "VA", "HU", "IS", "IE", "IT", "KZ", "LV", "LI", "LT", "LU", "MT", "MC", "MD", "ME", "NL", "NO", "PL", "PT", "RO", "RU", "SM", "RS", "SK", "SI", "ES", "SJ", "SE", "CH", "TR", "UA", "MK", "GB", "GG", "JE", "IM"
                };


                if (euCountries.Contains(country.CountryCode))
                {
                    country.DefaultPriceCurrencyID = curEUR.ID;

                }

                if (country.CountryCode == "TR")
                {
                    country.DefaultPriceCurrencyID = curTRY.ID;
                    country.DefaultLangID = lngTR.ID;
                }

            }


            db.SaveChanges();
        }

        private void CreateDefaultDomainForWebsite(Lang lng, PriceCurrency cur1, int storeAccountId)
        {
            Domain defaultDomain = new Domain
            {
                AllowChangeCurrency = false,
                AllowChangeLang = false,
                DomainBrandName = "Convertion",
                DomainSlogan = "Welcome to Convertion",
                DomainType = DomainTipi.InterNal,
                Lang = lng,
                Name = "localhost",
                PriceCurrency = cur1,
                Priority = 1,
                StoreAccountID=storeAccountId
            };

            db.Domains.Add(defaultDomain);
            db.SaveChanges();
        }

        private void CreateCurrencyAndLangs(out Lang lngEN, out Lang lngTR, out PriceCurrency curEur, out PriceCurrency curUsd, out PriceCurrency curTRY)
        {

            //Create Langs
            lngEN = new Lang { PStatus = Status.Active, Name = "English", IsDefault = true, LangCode = "en", CreatedOn = DateTime.Now };
            lngTR = new Lang { PStatus = Status.Active, Name = "Turkish", IsDefault = false, LangCode = "tr", CreatedOn = DateTime.Now };

            curEur = new PriceCurrency
            {
                PStatus = Status.Active,
                CurrencyCode = "EUR",
                XMLCurrencyCode = "EUR",
                CurrencySymbol = "€",
                IsDefault = true,
                Name = "Euro",
                CreatedOn = DateTime.Now,
                XMLCurrencyName = "EUR"
            };
            curUsd = new PriceCurrency
            {
                PStatus = Status.Active,
                CurrencyCode = "USD",
                XMLCurrencyCode = "USD",
                CurrencySymbol = "$",
                IsDefault = true,
                Name = "American Dollar",
                CreatedOn = DateTime.Now,
                XMLCurrencyName = "USD"
            };
            PriceCurrency curGBP = new PriceCurrency
            {
                PStatus = Status.Active,
                CurrencyCode = "GBP",
                XMLCurrencyCode = "GBP",
                CurrencySymbol = "£",
                IsDefault = true,
                Name = "British Pound",
                CreatedOn = DateTime.Now,
                XMLCurrencyName = "GBP"
            };
            curTRY = new PriceCurrency
            {
                PStatus = Status.Active,
                CurrencyCode = "TL",
                XMLCurrencyCode = "TRY",
                CurrencySymbol = "₺",
                IsDefault = true,
                Name = "Turkish Lira",
                CreatedOn = DateTime.Now,
                XMLCurrencyName = "TRY"
            };
            db.Langs.Add(lngEN);
            db.Langs.Add(lngTR);
            db.PriceCurrencies.Add(curEur);
            db.PriceCurrencies.Add(curUsd);
            db.PriceCurrencies.Add(curGBP);
            db.PriceCurrencies.Add(curTRY);

            db.SaveChanges();
        }
    }
}
