﻿using Ecom.Core;
using Ecom.Models;
using System;
using System.Linq;
using Microsoft.Extensions.Localization;
using Ecom.Services.Bus;
using Newtonsoft.Json;
using Ecom.ViewModels.BinNumber;
using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;

namespace Ecom.Services
{
    public class BinNumberService
    {

        private readonly EcomContext db;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        private readonly IMemoryCache cache;
        public BinNumberService(
            IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            EcomContext _db,
            IMemoryCache _cache,
            RabbitMQService _rabbitMQService
            )
        {
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            this.db = _db;
            this.cache = _cache;

        }

        public PagedOperationListResult<BinNumberCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<BinNumberCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.BinNumbers
                                  where
                                   p.PStatus == Status.Active &&
                                   p.StoreAccountID == storeId &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.BinNumbers
                                          where
                                          p.PStatus == Status.Active &&
                                          p.StoreAccountID == storeId
                                          && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.ID
                                          select new BinNumberCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus

                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.BinNumbers
                                  where p.StoreAccountID == storeId && p.PStatus == Status.Active
                                  select p).Count();

                operationResult.Result = db.BinNumbers.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.ID).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new BinNumberCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus
                  }

                ).ToList();
            }

            //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<BinNumberCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<BinNumberCreateEditVM>();

            result.Result = db.BinNumbers.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p => new BinNumberCreateEditVM
            {
                ID = p.ID,
                Name = p.Name,
                PStatus = p.PStatus,
            }).FirstOrDefault();

            if (result.Result != null)
            {
                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var brand = db.BinNumbers.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (brand != null)
            {
                brand.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.BinNumberDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private BinNumber MapToDBObject(BinNumberCreateEditVM model, BinNumber dbModel)
        {
            var brand = dbModel ?? new BinNumber();

            brand.ID = model.ID;
            brand.Name = model.Name;
            brand.PStatus = model.PStatus;
            brand.StoreAccountID = model.StoreID;
            //brand.ModifiedById= //current user
            brand.ModifiedOn = DateTime.Now;
            if (brand.ID != 0) brand.CreatedOn = DateTime.Now;

            return brand;

        }

        private BinNumberCreateEditVM MapToVM(BinNumber dbModel)
        {
            if (dbModel == null) return null;

            var brand = new BinNumberCreateEditVM();
            brand.ID = dbModel.ID;
            brand.Name = dbModel.Name;
            brand.PStatus = dbModel.PStatus;
            brand.StoreAccountID = dbModel.StoreAccountID;
            return brand;

        }

        public OperationResult<BinNumberCreateEditVM> Save(BinNumberCreateEditVM brand)
        {
            var result = new OperationResult<BinNumberCreateEditVM>();

            BinNumber existing = null;

            if (brand.ID != 0)
            {
                existing = db.BinNumbers.Where(x => x.ID == brand.ID).FirstOrDefault();
                existing = MapToDBObject(brand, existing);
            }
            else
            {
                existing = MapToDBObject(brand, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(brand.ID == 0 ? QueueItemType.BinNumberCreated : QueueItemType.BinNumberUpdated, JsonConvert.SerializeObject(result.Result), brand.StoreID);

            return result;
        }

        //Todo:consider adding domainid
        public List<BinNumberVM> GetBinNo(int storeId, int binno)
        {
            string cacheKey = $"AllBinNumbers-{storeId}-{binno}";

            var binNumbers = cache.Get<List<BinNumberVM>>(cacheKey);

            //TODO:add cache
            if (binNumbers == null || true)
            {
                binNumbers = (from p in db.BinNumbers
                               where p.BINNO == binno
                               && p.PStatus == Status.Active
                               orderby p.ID
                               select new BinNumberVM
                               {
                                   ID = p.ID,
                                   AllowInstallment = p.AllowInstallment,
                                   PStatus = p.PStatus,
                                   BankCode = p.BankCode,
                                   BankLogoPath = p.BankLogoPath,
                                   CardLogoPath = p.CardLogoPath,
                                   BankName = p.BankName,
                                   OwnerBankCode = p.OwnerBankCode,
                                   BINNO = p.BINNO,
                                   CardName = p.CardName,
                                   CardType = p.CardType,
                                   IsCorporate = p.IsCorporate,
                                   Name = p.Name,
                                   PrizeType = p.PrizeType,
                                   SubCardType = p.SubCardType,
                                   WebPosID = p.WebPosID

                               }).ToList();

                if (binNumbers != null)
                {
                    cache.Set(cacheKey, binNumbers, DateTime.Now.AddDays(1));
                }
            }

            return binNumbers;
        }


    }
}
