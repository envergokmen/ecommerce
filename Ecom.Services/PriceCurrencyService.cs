﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Langs;
using Ecom.ViewModels.PriceCurrencies;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class PriceCurrencyService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public PriceCurrencyService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }

        public PriceCurrencyVM GetCurrencyByCode(string currencyCode, int storeId)
        {
            string cacheKey = $"Currency_By_Code_{currencyCode}-{storeId}";

            var Currency = cache.Get<PriceCurrencyVM>(cacheKey);

            if (Currency == null)
            {
                //TODO: add store accountID or active langs per store or domain
                Currency = db.PriceCurrencies.Where(x => x.CurrencyCode == currencyCode && x.PStatus == Status.Active).OrderBy(x => x.ID).Select(x =>
                new PriceCurrencyVM
                {
                    CurrencySymbol = x.CurrencySymbol,
                    CurrencyCode = x.CurrencyCode,
                    PriceCurrencyID = x.ID,
                    IsDefault = x.IsDefault,
                    Name = x.Name

                }).FirstOrDefault();

                if (Currency != null)
                {
                    cache.Set(cacheKey, Currency, DateTime.Now.AddHours(6));
                }
            }

            return Currency;
        }
        public PriceCurrencyVM GetCurrencyById(int priceCurrencyId, int storeId)
        {
            string cacheKey = $"Currency_By_ID_{priceCurrencyId}-{storeId}";

            var Currency = cache.Get<PriceCurrencyVM>(cacheKey);

            if (Currency == null)
            {
                //TODO: add store accountID or active langs per store or domain
                Currency = db.PriceCurrencies.Where(x => x.ID == priceCurrencyId && x.PStatus == Status.Active).OrderBy(x => x.ID).Select(x =>
                new PriceCurrencyVM
                {
                    CurrencySymbol = x.CurrencySymbol,
                    CurrencyCode = x.CurrencyCode,
                    PriceCurrencyID = x.ID,
                    IsDefault = x.IsDefault,
                    Name = x.Name

                }).FirstOrDefault();

                if (Currency != null)
                {
                    cache.Set(cacheKey, Currency, DateTime.Now.AddHours(6));
                }
            }

            return Currency;
        }

        public List<PriceCurrencyVM> GetPriceCurrencyList(int storeId)
        {
            string cacheKey = $"Currency_List_store-{storeId}";

            var Currency = cache.Get<List<PriceCurrencyVM>>(cacheKey);

            if (Currency == null)
            {
                //TODO: add store accountID or active langs per store or domain
                Currency = db.PriceCurrencies.Where(x => x.ID == storeId && x.PStatus == Status.Active).OrderBy(x => x.ID).Select(x =>
                new PriceCurrencyVM
                {
                    CurrencySymbol = x.CurrencySymbol,
                    CurrencyCode = x.CurrencyCode,
                    PriceCurrencyID = x.ID,
                    IsDefault = x.IsDefault,
                    Name = x.Name

                }).ToList();

                if (Currency != null)
                {
                    cache.Set(cacheKey, Currency, DateTime.Now.AddHours(6));
                }
            }

            return Currency;
        }




    }
}
