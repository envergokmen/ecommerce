﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Catalogs;
using Ecom.ViewModels.Products;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecom.Services
{
    public class CatalogService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        private readonly ColorService colorService;
        private readonly EcomContext db;

        public CatalogService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService,
            ColorService _colorService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            colorService = _colorService;
        }


        public PagedOperationListResult<CatalogCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<CatalogCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Catalogs
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Catalogs
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new CatalogCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              ParentID = p.ParentID,
                                              ItemCount = p.ItemCount,
                                              CatalogType = p.CatalogType,
                                              Translations = p.CatalogLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Catalogs
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.Catalogs.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new CatalogCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus,
                      ParentID = p.ParentID,
                      ItemCount = p.ItemCount,
                      Translations = p.CatalogLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                  }

                ).ToList();
            }

            if (operationResult.Result != null)
            {
                foreach (var item in operationResult.Result)
                {
                    if (item.ParentID.HasValue)
                    {
                        item.ParentName = db.Catalogs.Where(c => c.ID == item.ParentID.Value).Select(c => c.Name).FirstOrDefault();
                    }
                }
            }
           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<CatalogCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<CatalogCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allCatalogs = db.Catalogs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();


            var allStatuses = serviceUtils.GetStatusValues(1);
            var allCatalogTypes = serviceUtils.ToSelectListInt<CatalogType>(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new CatalogCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllCatalogs = allCatalogs;
                result.Result.AllCatalogTypes = allCatalogTypes;
                result.Result.PStatus = Status.Active;

                result.Result.ShowInHeader = true;
                result.Result.ShowInFooter = true;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Catalogs.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new CatalogCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 ParentID = p.ParentID,
                 ItemCount = p.ItemCount,
                 ShowInFooter = p.ShowInFooter,
                 ShowInHeader = p.ShowInHeader,
                 CatalogType = p.CatalogType

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllCatalogs = allCatalogs;
                result.Result.AllCatalogTypes = allCatalogTypes;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Catalog = db.Catalogs.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Catalog != null)
            {
                Catalog.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.CatalogDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Catalog MapToDBObject(CatalogCreateEditVM model, Catalog dbModel)
        {
            var Catalog = dbModel ?? new Catalog();

            Catalog.ID = model.ID;

            Catalog.Name = model.Name;
            Catalog.PStatus = model.PStatus;
            Catalog.SortNo = model.SortNo;
            Catalog.ParentID = model.ParentID;
            Catalog.ItemCount = model.ItemCount;
            Catalog.ShowInFooter = model.ShowInFooter;
            Catalog.ShowInHeader = model.ShowInHeader;
            Catalog.CatalogType = model.CatalogType;

            Catalog.StoreAccountID = model.ID == 0 ? model.StoreID : Catalog.StoreAccountID;
            Catalog.ModifiedOn = DateTime.Now;
            if (Catalog.ID != 0) Catalog.CreatedOn = DateTime.Now;

            return Catalog;

        }


        private List<CatalogCreateEditVM> ConvertToCategoryTree(List<CatalogCreateEditVM> list)
        {
            if (list == null) return new List<CatalogCreateEditVM>();
            List<CatalogCreateEditVM> root = list.Where(x => x.ParentID == null).ToList();
            if (root == null) return new List<CatalogCreateEditVM>();

            foreach (var item in root)
            {
                PopulateChildren(item, list);
            }

            return root;
        }

        //recursive method
        private void PopulateChildren(CatalogCreateEditVM node, List<CatalogCreateEditVM> all)
        {
            var childs = all.Where(x => x.ParentID != null && x.ParentID.Equals(node.ID)).ToList();

            foreach (var item in childs)
            {
                if (node.SubCatalogs == null || node.SubCatalogs.Where(x => x.ID == item.ID).FirstOrDefault() == null)
                {
                    if (node.SubCatalogs == null) node.SubCatalogs = new List<CatalogCreateEditVM>();

                    node.SubCatalogs.Add(item);
                }

            }

            foreach (var item in childs)
                all.Remove(item);

            foreach (var item in childs)
                PopulateChildren(item, all);
        }

        public List<CatalogCreateEditVM> GetAllCatalogsForLang(int storeId, int? langId = null, bool asTree = false, CatalogType catalogType = CatalogType.Product)
        {
            string cacheKey = String.Concat("CatalogForLang-", storeId, langId ?? 0, asTree, catalogType);

            var catalogs = cache.Get<List<CatalogCreateEditVM>>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (catalogs == null || true)
            {
                if (langId.HasValue)
                {
                    catalogs = (from p in db.Catalogs
                                join dt in db.CatalogLocalLangs on p.ID equals dt.CatalogID
                                where p.StoreAccountID == storeId && p.PStatus == Status.Active && dt.PStatus == Status.Active && dt.LangID == langId
                                && dt.StoreAccountID == storeId
                                orderby p.SortNo
                                select new CatalogCreateEditVM
                                {
                                    ID = p.ID,
                                    Name = dt.Name,
                                    PStatus = p.PStatus,
                                    ParentID = p.ParentID,
                                    ItemCount = p.ItemCount,
                                    CatalogType = p.CatalogType,
                                    Link = dt.Link,
                                    Url = dt.Url,
                                    Description = dt.Description,
                                    MetaDescription = dt.MetaDescription,
                                    MetaKeywords = dt.MetaKeywords,
                                    MenuImageUrl = dt.MenuImageUrl,
                                    ShowInHeader = p.ShowInHeader,
                                    ShowInFooter = p.ShowInFooter,
                                    SortNo = p.SortNo,
                                    LangID = dt.LangID,
                                    Title = dt.Title,
                                    MobileImageUrl = dt.MenuImageUrl,
                                    Summary = dt.Summary,
                                    WebImageUrl = dt.WebImageUrl,
                                    FilterGroupsStr = p.FilterGroups
                                }).ToList();
                }
                else
                {
                    catalogs = (from p in db.Catalogs
                                join dt in db.CatalogLocalLangs on p.ID equals dt.CatalogID
                                where p.StoreAccountID == storeId && p.PStatus == Status.Active && dt.PStatus == Status.Active
                                && dt.StoreAccountID == storeId
                                orderby p.SortNo
                                select new CatalogCreateEditVM
                                {
                                    ID = p.ID,
                                    Name = dt.Name,
                                    PStatus = p.PStatus,
                                    ParentID = p.ParentID,
                                    ItemCount = p.ItemCount,
                                    CatalogType = p.CatalogType,
                                    LangID = dt.LangID,
                                    Link = dt.Link,
                                    Url = dt.Url,
                                    Description = dt.Description,
                                    MetaDescription = dt.MetaDescription,
                                    MetaKeywords = dt.MetaKeywords,
                                    MenuImageUrl = dt.MenuImageUrl,
                                    ShowInHeader = p.ShowInHeader,
                                    ShowInFooter = p.ShowInFooter,
                                    SortNo = p.SortNo,
                                    Title = dt.Title,
                                    MobileImageUrl = dt.MenuImageUrl,
                                    Summary = dt.Summary,
                                    WebImageUrl = dt.WebImageUrl,
                                    FilterGroupsStr = p.FilterGroups
                                }).ToList();
                }

                if (asTree) catalogs = ConvertToCategoryTree(catalogs);

                if (catalogs != null)
                {
                    foreach (var item in catalogs)
                    {
                        if (!String.IsNullOrWhiteSpace(item.FilterGroupsStr))
                        {
                            item.Filters = JsonConvert.DeserializeObject<List<FilterGroup>>(item.FilterGroupsStr);
                        }
                    }

                    cache.Set(cacheKey, catalogs, DateTime.Now.AddMinutes(30));
                }
            }


            return catalogs;

        }

        public CatalogCreateEditVM GetCatalogForRoute(int storeId, string url, int? langId)
        {
            CatalogCreateEditVM result = null;
            if (String.IsNullOrWhiteSpace(url)) return result;

            string cacheKey = String.Concat("CatalogForRoute_", url, "_", storeId, "_", langId ?? 0);
            result = cache.Get<CatalogCreateEditVM>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (1==1 || result == null)
            {
                var allCatalogs = GetAllCatalogsForLang(storeId, langId);

                if (allCatalogs != null)
                {
                    url = '/' + (url.TrimStart('/'));

                    if (langId.HasValue && langId > 0)
                    {
                        result = allCatalogs.Where(c => c.Url == url && c.LangID == langId.Value).FirstOrDefault();
                    }
                    else
                    {
                        result = allCatalogs.Where(c => c.Url == url).FirstOrDefault();
                    }

                    if (result != null)
                    {
                        cache.Set(cacheKey, result, DateTime.Now.AddMinutes(30));
                    }
                }
            }

            return result;

        }
        public List<CatalogCreateEditVM> GetAllCatalogs(int storeId, bool asTree = false, CatalogType catalogType = CatalogType.Product)
        {
            string cacheKey = String.Concat("AllCatalogs-", storeId, asTree, catalogType);

            var catalogs = cache.Get<List<CatalogCreateEditVM>>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (catalogs == null || true)
            {

                catalogs = (from p in db.Catalogs
                            where p.StoreAccountID == storeId && p.PStatus == Status.Active
                            orderby p.SortNo
                            select new CatalogCreateEditVM
                            {
                                ID = p.ID,
                                Name = p.Name,
                                PStatus = p.PStatus,
                                ParentID = p.ParentID,
                                ItemCount = p.ItemCount,
                                CatalogType = p.CatalogType,
                                Translations = p.CatalogLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                            }).ToList();


                if (asTree) catalogs = ConvertToCategoryTree(catalogs);

                if (catalogs != null)
                {
                    cache.Set(cacheKey, catalogs, DateTime.Now.AddMinutes(30));
                }
            }

            return catalogs;

        }

        private CatalogCreateEditVM MapToVM(Catalog model)
        {
            if (model == null) return null;

            var Catalog = new CatalogCreateEditVM();
            Catalog.ID = model.ID;
            Catalog.Name = model.Name;
            Catalog.PStatus = model.PStatus;
            Catalog.SortNo = model.SortNo;

            Catalog.CatalogType = model.CatalogType;
            Catalog.ParentID = model.ParentID;
            Catalog.ItemCount = model.ItemCount;
            Catalog.ShowInFooter = model.ShowInFooter;
            Catalog.ShowInHeader = model.ShowInHeader;

            return Catalog;
        }


        public const string OPEN_LIST_TAG = "<ul id=\"MainMenu\">";
        public const string CLOSE_LIST_TAG = "</ul>";
        public const string OPEN_LIST_ITEM_TAG = "<li>";
        public const string CLOSE_LIST_ITEM_TAG = "</li>";

        public List<CatalogCreateEditVM> GetMenuItems(int storeId)
        {
            List<CatalogCreateEditVM> MenuItmes = (from p in db.Catalogs
                                                   where p.StoreAccountID == storeId && p.PStatus == Status.Active
                                                   orderby p.SortNo
                                                   select new CatalogCreateEditVM
                                                   {
                                                       ID = p.ID,
                                                       Name = p.Name,
                                                       PStatus = p.PStatus,
                                                       ParentID = p.ParentID,
                                                       ItemCount = p.ItemCount,
                                                       CatalogType = p.CatalogType,
                                                       Translations = p.CatalogLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                                                   }).ToList();

            return MenuItmes;

        }

        public string GenerateMenuUi(List<CatalogCreateEditVM> allMenuItems)
        {
            var strBuilder = new StringBuilder();
            List<CatalogCreateEditVM> parentItems = (from a in allMenuItems where a.ParentID == 0 select a).ToList();
            strBuilder.Append(OPEN_LIST_TAG);
            foreach (var parentcat in parentItems)
            {
                strBuilder.Append(OPEN_LIST_ITEM_TAG);
                strBuilder.Append(parentcat.Name);
                List<CatalogCreateEditVM> childItems = (from a in allMenuItems where a.ParentID == parentcat.ID select a).ToList();
                if (childItems.Count > 0)
                    AddChildItem(parentcat, strBuilder, allMenuItems);
                strBuilder.Append(CLOSE_LIST_ITEM_TAG);
            }
            strBuilder.Append(CLOSE_LIST_TAG);
            return strBuilder.ToString();
        }

        private void AddChildItem(CatalogCreateEditVM childItem, StringBuilder strBuilder, List<CatalogCreateEditVM> allMenuItems)
        {
            strBuilder.Append(OPEN_LIST_TAG);
            List<CatalogCreateEditVM> childItems = (from a in allMenuItems where a.ParentID == childItem.ID select a).ToList();
            foreach (CatalogCreateEditVM cItem in childItems)
            {
                strBuilder.Append(OPEN_LIST_ITEM_TAG);
                strBuilder.Append(cItem.Name);
                List<CatalogCreateEditVM> subChilds = (from a in allMenuItems where a.ParentID == cItem.ID select a).ToList();
                if (subChilds.Count > 0)
                {
                    AddChildItem(cItem, strBuilder, allMenuItems);
                }
                strBuilder.Append(CLOSE_LIST_ITEM_TAG);
            }
            strBuilder.Append(CLOSE_LIST_TAG);
        }


        public OperationResult<CatalogCreateEditVM> Save(CatalogCreateEditVM Catalog)
        {
            var result = new OperationResult<CatalogCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Catalog, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Catalog existing = null;

            if (Catalog.ID != 0)
            {
                existing = db.Catalogs.Where(x => x.ID == Catalog.ID).FirstOrDefault();
                existing = MapToDBObject(Catalog, existing);
            }
            else
            {
                existing = MapToDBObject(Catalog, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Catalog.ID == 0 ? QueueItemType.CatalogCreated : QueueItemType.CatalogUpdated, JsonConvert.SerializeObject(result.Result), Catalog.StoreID);

            return result;

        }

        public StandartOperationResult MergeProductWithCatalogs(int productID, int storeId, int[] CatID, int[] ColorID)
        {
            var result = new StandartOperationResult();


            Product product = db.Products.Where(c => c.ID == productID && c.StoreAccountID == storeId).FirstOrDefault();

            if (product == null)
            {

                result.SetAsBadRequest(" Product not found: " + productID);

            }

            //List<ProductInCatalog> pincs = new List<ProductInCatalog>();
            int? MinSortInCatalog = (from p in db.ProductInCatalogs where p.SortNo != null orderby p.SortNo ascending select p.SortNo).FirstOrDefault();
            if (MinSortInCatalog == null) MinSortInCatalog = 0;

            //var selectedColors = db.Colors.Where(x => x.StoreAccountID == storeId && ColorID.Contains(x.ID)).Select(c => c.ID).ToArray();
            var selectedCategories = db.Catalogs.Where(x => x.StoreAccountID == storeId && CatID.Contains(x.ID)).Select(c => c.ID).ToArray();
            List<ProductInCatalogVMBasic> pincVMs = new List<ProductInCatalogVMBasic>();

            if (ColorID != null && ColorID.Length > 0)
            {
                foreach (var citem in ColorID)
                {
                    var color = colorService.GetColorById(citem, storeId);

                    if (color != null)
                    {
                        foreach (var ct in selectedCategories)
                        {

                            var existing = db.ProductInCatalogs.FirstOrDefault(c =>
                            c.ProductID == productID && c.CatalogID == ct
                            && c.ColorID == color.ID && c.PStatus == Status.Active);

                            if (existing == null || 1==1)
                            {
                                MinSortInCatalog--;

                                ProductInCatalog pinc = new ProductInCatalog();
                                pinc.ColorID = color.ID;
                                pinc.ColorGlobalID = color.ColorGlobalID;

                                pinc.ProductID = productID;
                                pinc.ProductCode = product.ProductCode;
                                pinc.CatalogID = ct;
                                pinc.SortNo = MinSortInCatalog;
                                pinc.StoreAccountID = storeId;

                                db.ProductInCatalogs.Add(pinc);
                                db.SaveChanges();

                                ProductInCatalogVMBasic pincVM = new ProductInCatalogVMBasic();
                                pincVM.ColorID = color.ID;
                                pincVM.ProductID = productID;
                                pincVM.ColorGlobalID = color.ColorGlobalID;
                                pincVM.CatalogID = ct;
                                pincVM.SortNo = MinSortInCatalog ?? 0;
                                pincVM.StoreID = storeId;
                                pincVMs.Add(pincVM);

                            }


                        }
                    }

                }

                rabbitMQService.Publish(QueueItemType.NewItemHasBeenAddedToCatalog, JsonConvert.SerializeObject(pincVMs), storeId);

                result.SetSuccessAndClearError();
                //CatalogRepo.ResetCatalogCacheValue();
                //ProductRepo.ClearProductCatalogCache();

            }
            else
            {

                result.SetAsBadRequest("Please select a color to add catalog");

            }

            return result;

        }

        public OperationListResult<ProductInCatalogVMBasic> RemoveFromCatalog(int productInCatalogId, int storeId)
        {
            var result = new OperationListResult<ProductInCatalogVMBasic>();

            var pinc = db.ProductInCatalogs.Where(x => x.ID == productInCatalogId && x.StoreAccountID == storeId).FirstOrDefault();

            if (pinc != null)
            {
                var productID = pinc.ProductID;

                db.Remove(pinc);
                db.SaveChanges();

                List<ProductInCatalogVMBasic> pincVMs = new List<ProductInCatalogVMBasic>();
                pincVMs.Add(MapProductInCatalogVM(storeId, pinc, QueueItemType.ProductRemovedFromCatalogs));

                result.SetSuccessAndClearError(pincVMs, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.CatalogSortDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }
 
        private static ProductInCatalogVMBasic MapProductInCatalogVM(int storeId, ProductInCatalog pinc, QueueItemType action)
        {
           
            ProductInCatalogVMBasic pincVM = new ProductInCatalogVMBasic();
            pincVM.ColorID = pinc.ColorID ?? 0;
            pincVM.ProductID = pinc.ProductID ?? 0;
            pincVM.ColorGlobalID = pinc.ColorGlobalID ?? 0;
            pincVM.CatalogID = pinc.CatalogID ?? 0;
            pincVM.SortNo = pinc.SortNo ?? 0;
            pincVM.StoreID = pinc.StoreAccountID ?? storeId;
            pincVM.ItemType = action;
      
            return pincVM;
        }

        public OperationIdResult MoveToTop(int productInCatalogId, int storeId)
        {
            var result = new OperationIdResult();

            var pinc = db.ProductInCatalogs.Where(x => x.ID == productInCatalogId && x.StoreAccountID == storeId).FirstOrDefault();

            if (pinc != null)
            {
                var firstItemNo = db.ProductInCatalogs.Where(x => x.CatalogID == pinc.CatalogID && x.StoreAccountID == storeId).OrderBy(c => c.SortNo).Select(c => c.SortNo).FirstOrDefault() - 1;
                pinc.SortNo = firstItemNo;

                db.SaveChanges();

                result.SetSuccessAndClearError(productInCatalogId, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.CatalogSortUpdated, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        public OperationListResult<ProductInCatalogVMBasic> RemoveFromAllCatalogs(int productID, int storeId)
        {
            var result = new OperationListResult<ProductInCatalogVMBasic>();

            var catalogs = db.ProductInCatalogs.Where(x => x.ProductID == productID && x.StoreAccountID == storeId).ToList();

            var pincVMs = new List<ProductInCatalogVMBasic>();
           
            if (catalogs != null)
            {
                foreach (var catalog in catalogs)
                {
                    db.Remove(catalog);
                    pincVMs.Add(MapProductInCatalogVM(storeId, catalog, QueueItemType.ProductRemovedFromCatalogs));

                }

                db.SaveChanges();

                result.SetSuccessAndClearError(pincVMs, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ProductRemovedFromCatalogs, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

    }
}
