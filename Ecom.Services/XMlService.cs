﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Catalogs;
using Ecom.ViewModels.Colors;
using Ecom.ViewModels.Prices;
using Ecom.ViewModels.Products;
using Ecom.ViewModels.XML;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class XMlService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        private readonly PriceCurrencyService currencyService;
        private readonly LangService langService;
        private readonly ColorService colorService;

        private readonly EcomContext db;

        public XMlService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService,
            PriceCurrencyService _currencyService,
            LangService _langService,
            ColorService _colorService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            currencyService = _currencyService;
            langService = _langService;
            colorService = _colorService;
        }


        private ElasticClient CreateElasticClient()
        {
            var client = new ElasticClient(new ConnectionSettings(new Uri("http://localhost:9200")).DefaultIndex("products"));
            return client;
        }

        private List<ProductXVM> GetXMLProducts(
           int storeId,
           int priceCurrencyID,
           int langId,
           MarketPlaceType? marketPlaceType
       )
        {

            var result = new List<ProductXVM>();

            ElasticClient client = CreateElasticClient();
            ISearchResponse<ProductXVM> searchResponse =

            client.Search<ProductXVM>(s => s
            .Index("uniqueproducts")
               .Query(q => q
                   .Bool(b => b
                      .Must(mu => mu
                          .Match(mr => mr
                               .Field("prices.priceCurrencyID")
                               .Query(priceCurrencyID.ToString())
                           ),
                          cv => cv
                           .Match(cs => cs
                               .Field(f => f.Pstatus)
                               .Query("1")
                           ),
                           we => we
                            .Match(qa => qa
                                .Field(j => j.StoreID)
                                .Query(storeId.ToString())
                            )
                       ).MustNot((mu => mu
                           .Match(m => m
                               .Field(f => f.TotalQuantity)
                               .Query("0")
                           )))
                   )
               )
           );

            result = searchResponse.Documents?.ToList();

            if (result != null)
            {
                foreach (var item in result)
                {
                    item.PriceCurrencyId = priceCurrencyID;
                    item.LangID = langId;
                }
            }


            return result;
        }

        public XMLProductGeneralList GetGeneralXML(int storeId,
           int priceCurrencyID,
           int langId,
           MarketPlaceType? marketPlaceType)
        {
            var products = GetXMLProducts(storeId, priceCurrencyID, langId, MarketPlaceType.General);
            return MapToGeneralXML(products);
        }

        private XMLProductGeneralList MapToGeneralXML(List<ProductXVM> prds)
        {
            var xml = new XMLProductGeneralList();

            foreach (var item in prds)
            {
                var product = new XMLProductGeneral
                {
                    //TODO:Add brand name
                    brand = item.BrandID.ToString(),
                    description = new CDATA(item.DetailHTML),
                    name = item.ProductLocalName,
                    price = item.Amount,
                    retailprice = item.AmountOld,
                    productCode = item.ProductCode,
                    producturl = item.Url
                };

                foreach (var img in item.AllImages)
                {
                    product.Images.Add(new XMlProductImage
                    {
                        angle = img.Angle,
                        colorName = img.ColorName,
                        url = img.ImagePath
                    });
                }

                foreach (var stock in item.AllStocks)
                {
                    product.Stocks.Add(new XmlStockItem
                    {
                        barcode = stock.Barkod,
                        colorglobalName = stock.MainColorName,
                        colorName = stock.ColorName,
                        quantity = stock.Quantity,
                        sizeName = stock.SizeName
                    });
                }

                xml.products.Add(product);

            }

            return xml;
        }



    }
}
