﻿using Ecom.Core;
using Ecom.Models;
using Ecom.ViewModels.Colors;
using System;
using System.Linq;
using Microsoft.Extensions.Localization;
using Ecom.Services.Bus;
using Newtonsoft.Json;
using Ecom.ViewModels;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;

namespace Ecom.Services
{
    public class ColorService
    {
        private readonly IMemoryCache cache;
        private readonly EcomContext db;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        public ColorService(
            IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            EcomContext _db,
            IMemoryCache _cache,
            RabbitMQService _rabbitMQService
            )
        {
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            this.db = _db;
            cache = _cache;

        }

        public PagedOperationListResult<ColorCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<ColorCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Colors
                                  where
                                   p.PStatus == Status.Active &&
                                   p.StoreAccountID == storeId &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Colors
                                          where
                                          p.PStatus == Status.Active &&
                                          p.StoreAccountID == storeId
                                          && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new ColorCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              SortNo = p.SortNo,
                                              HexValue = p.HexValue,
                                              PatternPath = p.PatternPath,
                                              StoreAccountID = p.StoreAccountID,
                                              ColorCode = p.ColorCode,
                                              ColorDescription = p.ColorDescription,
                                              ColorGlobalID = p.ColorGlobalID,

                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Colors
                                  where p.StoreAccountID == storeId && p.PStatus == Status.Active
                                  select p).Count();

                operationResult.Result = db.Colors.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new ColorCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus,
                      SortNo = p.SortNo,
                      HexValue = p.HexValue,
                      PatternPath = p.PatternPath,
                      StoreAccountID = p.StoreAccountID,
                      ColorCode = p.ColorCode,
                      ColorDescription = p.ColorDescription,
                      ColorGlobalID = p.ColorGlobalID,
                  }

                ).ToList();
            }

            //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<ColorCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<ColorCreateEditVM>();


            var allColorGlobals = db.ColorGlobals
                .Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId)
                .Select(x => new NameValueIntVM
                {
                    Name = x.Name,
                    Value = x.ID
                }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new ColorCreateEditVM();
                result.Result.AllColorGlobals = allColorGlobals;
                result.Result.AllStatuses = allStatuses;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Colors.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p => new ColorCreateEditVM
            {
                ID = p.ID,
                Name = p.Name,
                PStatus = p.PStatus,
                SortNo = p.SortNo,
                HexValue = p.HexValue,
                PatternPath = p.PatternPath,
                StoreAccountID = p.StoreAccountID,
                ColorCode = p.ColorCode,
                ColorDescription = p.ColorDescription,
                ColorGlobalID = p.ColorGlobalID,

            }).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllColorGlobals = allColorGlobals;
                result.Result.AllStatuses = allStatuses;
                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var brand = db.Colors.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (brand != null)
            {
                brand.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ColorDeleted, JsonConvert.SerializeObject(result.Result), storeId);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Color MapToDBObject(ColorCreateEditVM model, Color dbModel)
        {
            var brand = dbModel ?? new Color();


            brand.ID = model.ID;
            brand.Name = model.Name;
            brand.PStatus = model.PStatus;
            brand.SortNo = model.SortNo;
            brand.ColorDescription = model.ColorDescription;
            brand.ColorCode = model.ColorCode;
            brand.PatternPath = !String.IsNullOrWhiteSpace(model.PatternPath) ? model.PatternPath : dbModel?.PatternPath ?? "";
            brand.HexValue = model.HexValue;
            brand.ColorGlobalID = model.ColorGlobalID;
            brand.StoreAccountID = model.StoreID;
            //brand.ModifiedById= //current user
            brand.ModifiedOn = DateTime.Now;
            if (brand.ID != 0) brand.CreatedOn = DateTime.Now;

            return brand;

        }

        private ColorCreateEditVM MapToVM(Color model)
        {
            if (model == null) return null;

            var brand = new ColorCreateEditVM();
            brand.ID = model.ID;
            brand.Name = model.Name;
            brand.PStatus = model.PStatus;
            brand.SortNo = model.SortNo;
            brand.ColorDescription = model.ColorDescription;
            brand.ColorCode = model.ColorCode;
            brand.PatternPath = model.PatternPath;
            brand.HexValue = model.HexValue;
            brand.ColorGlobalID = model.ColorGlobalID;
            brand.StoreAccountID = model.StoreAccountID;
            return brand;

        }

        public StandartOperationResult DeleteImage(int id, int storeId)
        {
            var result = new StandartOperationResult();

            var brand = db.Colors.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (brand != null)
            {
                brand.PatternPath = "";
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ColorUpdated, JsonConvert.SerializeObject(result.Result), storeId);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        public OperationResult<ColorCreateEditVM> Save(ColorCreateEditVM brand)
        {
            var result = new OperationResult<ColorCreateEditVM>();

            Color existing = null;

            if (brand.ID != 0)
            {
                existing = db.Colors.Where(x => x.ID == brand.ID).FirstOrDefault();
                existing = MapToDBObject(brand, existing);
            }
            else
            {
                existing = MapToDBObject(brand, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(brand.ID == 0 ? QueueItemType.ColorCreated : QueueItemType.ColorUpdated, JsonConvert.SerializeObject(result.Result), brand.StoreID);

            return result;

        }


        public ColorVM GetColorById(int colorId, int storeId)
        {
            string cacheKey = $"Color_By_ID_{colorId}-{storeId}";

            var color = cache.Get<ColorVM>(cacheKey);

            if (color == null)
            {
                //TODO: add store accountID or active langs per store or domain
                color = db.Colors.Where(x => x.ID == colorId && x.PStatus == Status.Active).OrderBy(x => x.ID).Select(x =>

                new ColorVM
                {
                    ID = x.ID,
                    ColorCode = x.ColorCode,
                    ColorDescription = x.ColorDescription,
                    HexValue = x.HexValue,
                    PatternPath = x.PatternPath,
                    SortNo = x.SortNo,
                    StoreAccountID = x.StoreAccountID,
                    ColorGlobalHexValue = x.ColorGlobal.HexValue,
                    ColorGlobalID = x.ColorGlobalID,
                    ColorGlobalName = x.ColorGlobal.Name,
                    Name = x.Name,


                }).FirstOrDefault();

                if (color != null)
                {
                    cache.Set(cacheKey, color, DateTime.Now.AddHours(6));
                }
            }

            return color;
        }

        public List<ColorVM> GetAllColors(int storeId)
        {
            string cacheKey = $"ALL_Colors_{storeId}";

            var colors = cache.Get<List<ColorVM>>(cacheKey);

            if (colors == null)
            {
                //TODO: add store accountID or active langs per store or domain
                colors = db.Colors.Where(x => x.PStatus == Status.Active).OrderBy(x => x.ID).Select(x =>

                new ColorVM
                {
                    ID = x.ID,
                    ColorCode = x.ColorCode,
                    ColorDescription = x.ColorDescription,
                    HexValue = x.HexValue,
                    PatternPath = x.PatternPath,
                    SortNo = x.SortNo,
                    StoreAccountID = x.StoreAccountID,
                    ColorGlobalHexValue = x.ColorGlobal.HexValue,
                    ColorGlobalID = x.ColorGlobalID,
                    ColorGlobalName = x.ColorGlobal.Name,
                    Name = x.Name,


                }).ToList();

                if (colors != null)
                {
                    cache.Set(cacheKey, colors, DateTime.Now.AddHours(6));
                }
            }

            return colors;
        }

        public ColorVM GetColorForSearchText(string searchText, int storeId)
        {
            string cacheKey = $"Color_By_text_{searchText}-{storeId}";

            var color = cache.Get<ColorVM>(cacheKey);

            if (color == null)
            {
                //TODO: add store accountID or active langs per store or domain
                var allColors = GetAllColors(storeId);

                color = allColors.FirstOrDefault(c => c.Name.Equals(searchText, StringComparison.InvariantCultureIgnoreCase) 
                || (c.ColorGlobalName!=null && c.ColorGlobalName.Equals(searchText, StringComparison.InvariantCultureIgnoreCase))
                );

                if (color == null)
                {
                    var colorTexts = searchText.Split(" ");

                    foreach (var clText in colorTexts)
                    {
                        var cacheKey2 = $"Color_By_text_{clText}-{storeId}";

                        color = cache.Get<ColorVM>(cacheKey2);

                        if (color == null)
                        {
                            color = allColors.FirstOrDefault(c => c.Name.Equals(clText, StringComparison.InvariantCultureIgnoreCase) || c.ColorGlobalName.Equals(clText, StringComparison.InvariantCultureIgnoreCase));

                            if (color != null)
                            {
                                cache.Set(cacheKey2, color, DateTime.Now.AddHours(2));
                                cache.Set(cacheKey, color, DateTime.Now.AddHours(2));
                                return color;
                            }
                        }
                        else
                        {
                            cache.Set(cacheKey, color, DateTime.Now.AddHours(2));
                            return color;
                        }



                    }
                }

                if (color != null)
                {
                    cache.Set(cacheKey, color, DateTime.Now.AddHours(6));
                }
            }

            return color;
        }

        public ColorGlobalVM GetColorGlobalById(int colorGlobalId, int storeId)
        {
            string cacheKey = $"ColorGlobal_By_ID_{colorGlobalId}-{storeId}";

            var colorglobal = cache.Get<ColorGlobalVM>(cacheKey);

            if (colorglobal == null)
            {
                //TODO: add store accountID or active langs per store or domain
                colorglobal = db.ColorGlobals.Where(x => x.ID == colorGlobalId && x.PStatus == Status.Active).OrderBy(x => x.ID).Select(x =>

                new ColorGlobalVM
                {
                    HexValue = x.HexValue,
                    ID = x.ID,
                    Name = x.Name

                }).FirstOrDefault();

                if (colorglobal != null)
                {
                    cache.Set(cacheKey, colorglobal, DateTime.Now.AddHours(6));
                }
            }

            return colorglobal;
        }

    }
}
