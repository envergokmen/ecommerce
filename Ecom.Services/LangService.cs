﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Langs;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class LangService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public LangService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }

        public List<LangViewModel> GetlangList(int storeId)
        {
            string cacheKey = $"Lang_List_{storeId}";

            var Lang = cache.Get<List<LangViewModel>>(cacheKey);

            if (Lang == null)
            {
                //TODO: add store accountID or active langs per store or domain
                Lang = db.Langs.Where(x => x.PStatus == Status.Active).OrderBy(x => x.ID).Select(x =>
                new LangViewModel
                {
                    FlagPath = x.FlagPath,
                    LangCode = x.LangCode,
                    ID = x.ID,
                    IsDefault = x.IsDefault,
                    Name = x.Name

                }).ToList();

                if (Lang != null)
                {
                    cache.Set(cacheKey, Lang, DateTime.Now.AddHours(6));
                }
            }

            return Lang;
        }


        public LangViewModel GetLangByCode(string langCode, int storeId)
        {
            string cacheKey = $"Lang_By_Name_{langCode}-{storeId}";

            var Lang = cache.Get<LangViewModel>(cacheKey);

            if (Lang == null)
            {
                //TODO: add store accountID or active langs per store or domain
                Lang = db.Langs.Where(x => x.LangCode == langCode && x.PStatus == Status.Active).OrderBy(x => x.ID).Select(x =>
                new LangViewModel
                {
                    FlagPath = x.FlagPath,
                    LangCode = x.LangCode,
                    ID = x.ID,
                    IsDefault = x.IsDefault,
                    Name = x.Name

                }).FirstOrDefault();

                if (Lang != null)
                {
                    cache.Set(cacheKey, Lang, DateTime.Now.AddHours(6));
                }
            }

            return Lang;
        }


        public LangViewModel GetLangById(int langId, int storeId)
        {
            string cacheKey = $"Lang_By_ID_{langId}-{storeId}";

            var Lang = cache.Get<LangViewModel>(cacheKey);

            if (Lang == null)
            {
                //TODO: add store accountID or active langs per store or domain
                Lang = db.Langs.Where(x => x.ID == langId && x.PStatus == Status.Active).OrderBy(x => x.ID).Select(x =>
                new LangViewModel
                {
                    FlagPath = x.FlagPath,
                    LangCode = x.LangCode,
                    ID = x.ID,
                    IsDefault = x.IsDefault,
                    Name = x.Name

                }).FirstOrDefault();

                if (Lang != null)
                {
                    cache.Set(cacheKey, Lang, DateTime.Now.AddHours(6));
                }
            }

            return Lang;
        }
         
  
    }
}
