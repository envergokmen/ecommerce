﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Ecom.Core;

namespace Ecom.Services
{
    public class SecureOperations
    {
        public const string ENCRIPTON_KEY = "SCSECOMV2X6SPBNI33277"; 

        public static string ClearSqlInjection(string text)
        {
            if (String.IsNullOrWhiteSpace(text)) return "";

            text = text.Replace(">", ""); // SQL injection temizleme fonksiyonumuz
            text = text.Replace("<", "");
            text = text.Replace("--", "");
            text = text.Replace("'", "");
            text = text.Replace("char ", "");
            text = text.Replace("delete ", "");
            text = text.Replace("insert ", "");
            text = text.Replace("update ", "");
            text = text.Replace("select ", "");
            text = text.Replace("truncate ", "");
            text = text.Replace("union ", "");
            text = text.Replace("script ", "");
            text = text.Replace("1=1", "");
            text = text.Replace("' OR ''='", "");
            text = text.Replace(";", "");
            text = text.Replace("(", "");
            text = text.Replace(")", "");
            text = text.Replace("[", "");
            text = text.Replace("]", "");
            text = text.Replace("=", "");
            text = text.Replace("delay", "");
            text = text.Replace("DELAY", "");
            text = text.Replace("WAITFOR", "");
            text = text.Replace("waitfor", "");
            text = text.Replace("++", "");
            text = text.Replace("%", "");
            return text;
        }

        public static bool IsCreditCartNumberValid(string cardNumber)
        {
            if(String.IsNullOrWhiteSpace(cardNumber)) return false;

            //Clean the card number- remove dashes and spaces
            cardNumber = cardNumber.Replace("-", "").Replace(" ", "");

            //Convert card number into digits array
            int[] digits = new int[cardNumber.Length];
            for (int len = 0; len < cardNumber.Length; len++)
            {
                digits[len] = Int32.Parse(cardNumber.Substring(len, 1));
            }


            int sum = 0;
            bool alt = false;
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int curDigit = digits[i];
                if (alt)
                {
                    curDigit *= 2;
                    if (curDigit > 9)
                    {
                        curDigit -= 9;
                    }
                }
                sum += curDigit;
                alt = !alt;
            }

            return sum % 10 == 0;
        }

        public static string ClearSqlInjectionHard(string text)
        {
            if (String.IsNullOrWhiteSpace(text)) return "";

            text = ClearSqlInjection(text);

            text = Regex.Replace(text, ",", "");
            text = Regex.Replace(text, "/", "");
            text = Regex.Replace(text, "\n", "");
            text = Regex.Replace(text, "/?", "");
            text = Regex.Replace(text, "/*", "");
            text = Regex.Replace(text, "'", "");
            text = Regex.Replace(text, "&", "");
            text = Regex.Replace(text, "<", "");
            text = Regex.Replace(text, ">", "");
            text = Regex.Replace(text, "=", "");
            text = Regex.Replace(text, "%", "[%]");
            text = Regex.Replace(text, "--", "");
            text = Regex.Replace(text, ";", "");
            text = Regex.Replace(text, "AND", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "OR", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "LIKE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "JOIN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "UNION", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "UPDATE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "SELECT", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "INSERT", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "İNSERT", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "CREATE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "DELETE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "TABLE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "DATABASE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "DROP", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "ALTER", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "HAVING", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "GROUP", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "BY", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "BETWEEN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "IN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "INNER", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "JOİN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "SUM", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "SET", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "İNNER", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "İN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "HAVİNG", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "LİKE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "UNİON", "", RegexOptions.IgnoreCase);
            return text;

        }
         

        public static string SecureStringContainsOpos(string text, bool clearNormalInjection = false)
        {
            if (String.IsNullOrWhiteSpace(text)) return text;

            if (text != null && text.IndexOf("'") != -1)
            {
                text = SecureOperations.ClearSqlInjectionHard(text);
            }
            else if (text != null && clearNormalInjection)
            {
                text = SecureOperations.ClearSqlInjection(text);
            }

            return text;

        }
        public static string GenerateCustomBase64(string plainText, int BeginCount = 2, int EndCount = 2)
        {
            string natural = Base64Encode(plainText);

            string key = GeneratePassword(BeginCount + EndCount, PassWordGenerationType.Words);

            string begins = key.Substring(0, BeginCount);
            string ends = key.Substring(BeginCount, EndCount);
            return String.Concat(begins, natural, ends);

        }

        public static string DecodeCustomBase64(string plainText, int BeginCount = 2, int EndCount = 2)
        {
            string result = "";

            if (plainText.Length > 4)
            {
                plainText = plainText.Substring(BeginCount, plainText.Length - BeginCount);
                plainText = plainText.Substring(0, plainText.Length - EndCount);
            }

            result = Base64Decode(plainText);
            return result;

        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes); ;

        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);

        }


        public static string GeneratePassword(int Length, PassWordGenerationType passwordType)
        {
            var chars = (passwordType == PassWordGenerationType.Words) ? "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" : "0123456789";

            var random = new Random();

            var result = new string(
                Enumerable.Repeat(chars, Length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result.ToString();

        }

        public static string HashWithSHA256(string text)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            byte[] message = UE.GetBytes(text);

            SHA256Managed hashString = new SHA256Managed();
            string hex = "";

            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }
         
        public static bool IsValidMailServiceName(string mailadress)
        {
            bool sonuc = true;

            int atPost = mailadress.IndexOf("@");

            if (atPost > 0)
            {
                mailadress = mailadress.Substring((atPost + 1), mailadress.Length - (atPost + 1));
            }

            string[] WrongMailList = {
                "gnmail.com", "gmal.com", "hootmail.com","hottmail.com","hptmail.com","hatmail.com","hetotmail.com","hıtmail.com","hitmail.com","otmail.com",
                "otmail.com.tr","gotmail.com","jotmail.com","kotmail.com","botmail.com","notmail.com","hhotmail.com","hottmail.com","hoymail.com","hormail.com","hogmail.com",
                "hotail.com","htmail.com","hotmial.com","homtail.com","hotmil.com","homail.com","hotöail.com","hofmail.com","hotmaiol.com","hortmal.com","hortmail.com","hotmaio.com","hohtmail.com",
                "hotnail.com","hotymail.com","hopmail.com","hotmmail.com","hotmaiil.com","hotmaıl.com","hotmeil.com","hotmsil.com","hotmqail.com","hotmqil.com","hotmzail.com","hotmai.com","hotmail.con",
                "hotmail.om","hotmail.cop","hotmail.coö","hotmail.cok","hotmail.cokm","hotmail.co","hotmal.com","hotma.com","hotmaik.com","hotmaiş.com","hotmakl.com","gnail.com","gmaiil.com","gmaill.com",
                "gmail.con","gmail.coö","con.tr","yaho.com","yaho.com.tr","mymet.com","windoslive.com","windovslive.com","windowlive.com"
                     };

            if (WrongMailList.Contains(mailadress))
            {
                sonuc = false;
            }

            return sonuc;

        }
         
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = SecureOperations.ENCRIPTON_KEY;

            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        { 
            string EncryptionKey = SecureOperations.ENCRIPTON_KEY;
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }


    }
}
