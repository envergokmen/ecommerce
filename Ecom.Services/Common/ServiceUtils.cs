﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using Ecom.Core;
using Ecom.ViewModels;
using Microsoft.Extensions.Localization;

namespace Ecom.Services
{
    //TODO:Decide to move core
    public class SwValidationResult : OperationListResult<ValidationResult>
    { 
        /// <summary>
        ///  add new validation Messages to Result and sets status to false
        /// </summary>
        /// <param name="message"></param>
        public override void AddError(string message)
        {
            if(Result==null)
            {
                Result = new List<ValidationResult>();
            }
            
            this.Result.Add(new ValidationResult(message));
            this.Status = false;
        }
    }

    public class ServiceUtils
    {
        private readonly IStringLocalizer<SharedDataResource> _sharedlocalizer;

        public ServiceUtils(IStringLocalizer<SharedDataResource> sharedlocalizer)
        {
            _sharedlocalizer = sharedlocalizer;
        }
        /// <summary>
        /// Try To validate object with Data Anotation properties also can check object is null.
        /// </summary>
        /// <param name="model">anyobject to validate by data anototion attributes</param>
        /// <param name="DontAcceptNulls">check object is null or not if you send true will be add errors for null objects</param>
        /// <returns></returns>
        public StandartOperationResult TryToValidateModel(object model, bool DontAcceptNulls = false)
        {
            var operationResult = new StandartOperationResult();

            if (DontAcceptNulls && model == null)
            {
                operationResult.SetErrorAndMarkAsErrored("InputCannotBeNull");
                return operationResult;
            }

            var validationContext = new ValidationContext(model, null, null);
            var validationResults = new List<ValidationResult>();

            Validator.TryValidateObject(model, validationContext, validationResults);

            if (validationResults.Count <= 0)
            {

                operationResult.SetSuccessAndClearError();
            }
            else
            {
                operationResult.SetErrorAndMarkAsErrored();

                foreach (var item in validationResults)
                {
                    operationResult.AddError(_sharedlocalizer[item.ErrorMessage]);
                }
            }

            return operationResult;

        }


        public List<NameValueStringVM> ToSelectList<T>(object selected)
        {
            var source = Enum.GetValues(typeof(T));

            var items = new List<NameValueStringVM>();

            var displayAttributeType = typeof(DisplayAttribute);
            if (selected == null) selected = 0;

            foreach (var value in source)
            {
                FieldInfo field = value.GetType().GetField(value.ToString());

                DisplayAttribute attrs = (DisplayAttribute)field.
                              GetCustomAttributes(displayAttributeType, false).FirstOrDefault();

                bool isSelected = (((int)value).ToString() == ((int)selected).ToString()) ? true : false;

                items.Add(new NameValueStringVM { Value = ((int)value).ToString(), Name = _sharedlocalizer[attrs != null ? attrs.GetName() : value.ToString()], IsSelected = isSelected });
            }

            return items;
        }

        public List<NameValueIntVM> ToSelectListInt<T>(object selected)
        {
            var source = Enum.GetValues(typeof(T));

            var items = new List<NameValueIntVM>();

            var displayAttributeType = typeof(DisplayAttribute);
            if (selected == null) selected = 0;

            foreach (var value in source)
            {
                FieldInfo field = value.GetType().GetField(value.ToString());

                DisplayAttribute attrs = (DisplayAttribute)field.
                              GetCustomAttributes(displayAttributeType, false).FirstOrDefault();

                bool isSelected = (((int)value).ToString() == ((int)selected).ToString()) ? true : false;

                items.Add(new NameValueIntVM { Value = ((int)value), Name = _sharedlocalizer[attrs != null ? attrs.GetName() : value.ToString()], IsSelected = isSelected });
            }

            return items;
        }

        public List<NameValueStringVM> GetStatusValues(object selected)
        {
            var items = ToSelectList<Status>(selected)?.Where(x => new string[] { "0", "1" }.Contains(x.Value)).ToList();
            items.Insert(0, new NameValueStringVM { Name = _sharedlocalizer["PleaseSelectStatus"], Value = ""});

            return items;
        }


        public static string DownloadAndSaveImageFromUrl(string remoteFileUrl, string rootPath, string domainName, string contentType, string subFolder = "")
        {

            //TODO:download new images images
            System.Net.WebClient wc = new System.Net.WebClient();

            var fileExt = Path.GetExtension(remoteFileUrl)?.Split('?')[0];

            if (new string[] { ".JPG", ".PNG", ".jpg", ".jpeg", ".JPEG", ".png", ".bmp" }.Contains(fileExt) == false)
            {
                throw new Exception("file name is not acceptable");
            }

            var curImageGuid = Guid.NewGuid().ToString();
            var formattedDomainName = Utils.FormatUrl(domainName);
            var tempFolder = Path.Combine(rootPath, "Uploads", formattedDomainName, "Temp");
            var tempfilePath = Path.Combine(tempFolder, curImageGuid + Path.GetExtension(Path.GetTempFileName()));
            var tempDirectory = new DirectoryInfo(tempFolder);

            if (!tempDirectory.Exists) Directory.CreateDirectory(tempDirectory.FullName);

            wc.DownloadFile(remoteFileUrl, tempfilePath);

            var newPathFoler = Path.Combine(rootPath,"ContentImages", formattedDomainName, contentType, subFolder);


            var newFullPath = Path.Combine(newPathFoler, curImageGuid.ToString() + fileExt);
            var newPathDirectory = new DirectoryInfo(newPathFoler);
            if (!newPathDirectory.Exists) Directory.CreateDirectory(newPathDirectory.FullName);

            var newImagePath = String.Concat("/ContentImages/", formattedDomainName, "/", contentType, (subFolder != "" ? "/" + subFolder : ""), "/", curImageGuid + fileExt);

            int i = 1;
            while (File.Exists(Path.Combine(newFullPath)))
            {
                curImageGuid = Guid.NewGuid().ToString();
                newImagePath = String.Concat("/ContentImages/", formattedDomainName, "/", contentType, (subFolder != "" ? "/" + subFolder : ""), "/", curImageGuid + fileExt);
                newFullPath = Path.Combine(newPathFoler, curImageGuid + fileExt);

                i++;
            }

            File.Copy(tempfilePath, newFullPath);

            try
            {
                File.Delete(tempfilePath);
            }
            finally { }

            return newImagePath;

        }


        //public static string GetDbValidationErrorDetails(DbEntityValidationException ex)
        //{

        //    StringBuilder sb = new StringBuilder();

        //    foreach (var failure in ex.EntityValidationErrors)
        //    {
        //        sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
        //        foreach (var error in failure.ValidationErrors)
        //        {
        //            sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
        //            sb.AppendLine();
        //        }
        //    }

        //    return sb.ToString();
        //}

    }
}
