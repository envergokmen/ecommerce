﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.ProductLocalLangs;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class ProductDetailService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public ProductDetailService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<ProductLocalLangCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10, int? catalogId = null)
        {
            var operationResult = new PagedOperationListResult<ProductLocalLangCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            var totalQuery = (from p in db.ProductDetails
                              where
                               p.StoreAccountID == storeId && p.PStatus == Status.Active
                              select p);

            var query = (from p in db.ProductDetails
                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                         orderby p.ID
                         select p).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize));

            if (catalogId.HasValue && catalogId > 0)
            {
                totalQuery = totalQuery.Where(x => x.ProductID == catalogId);
                query = query.Where(x => x.ProductID == catalogId);
            }


            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {
                totalQuery = totalQuery.Where(p => p.ProductName.ToLower().Contains(searchKeyword.ToLower()));
                query = query.Where(p => p.ProductName.ToLower().Contains(searchKeyword.ToLower()));
            }

            operationResult.Result = query.Select(p =>
                              new ProductLocalLangCreateEditVM
                              {
                                  ID = p.ID,
                                  ProductName = p.ProductName,
                                  ProductID = p.ProductID,
                                  PStatus = p.PStatus,
                                  LangCode = p.Lang.LangCode,
                                  Url = p.Url,
                                  ProductCode = p.Product.ProductCode
                              }).ToList();


            totalItemCount = totalQuery.Count();

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<ProductLocalLangCreateEditVM> Get(int id, int storeId, int typeid)
        {
            var result = new OperationResult<ProductLocalLangCreateEditVM>();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allProducts = db.Products.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueLongVM
            {
                Name = x.ProductName,
                Value = x.ID
            }).ToList();


            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new ProductLocalLangCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllProducts = allProducts;
                result.Result.ProductID = typeid;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.ProductDetails.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new ProductLocalLangCreateEditVM
             {
                 ID = p.ID,
                 ProductName = p.ProductName,
                 PStatus = p.PStatus,
                 ProductID = p.ProductID,
                 DetailHTML = p.DetailHTML,
                 LangID = p.LangID,
                 Title = p.Title,
                 Summary = p.Summary,
                 CanonicalLink = p.CanonicalLink,
                 MetaDescription = p.MetaDescription,
                 MetaKeywords = p.MetaKeywords,
                 Url = p.Url

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllProducts = allProducts;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Product = db.ProductDetails.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Product != null)
            {
                Product.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ProductDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private ProductDetail MapToDBObject(ProductLocalLangCreateEditVM model, ProductDetail dbModel)
        {
            var Product = dbModel ?? new ProductDetail();

            Product.ID = model.ID;

            Product.ProductName = model.ProductName;
            Product.PStatus = model.PStatus;
            Product.LangID = model.LangID;
            Product.ProductID = model.ProductID;
            Product.DetailHTML = model.DetailHTML;
            Product.Title = model.Title;
            Product.Summary = model.Summary;
            Product.MetaDescription = model.MetaDescription;
            Product.MetaKeywords = model.MetaKeywords;
            Product.CanonicalLink = model.CanonicalLink;
            Product.Url = model.Url;

            Product.StoreAccountID = model.ID == 0 ? model.StoreID : Product.StoreAccountID;
            Product.ModifiedOn = DateTime.Now;
            if (Product.ID != 0) Product.CreatedOn = DateTime.Now;

            return Product;

        }

        private ProductLocalLangCreateEditVM MapToVM(ProductDetail dbModel)
        {
            if (dbModel == null) return null;

            var Product = new ProductLocalLangCreateEditVM();
            Product.ID = dbModel.ID;
            Product.ProductName = dbModel.ProductName;
            Product.DetailHTML = dbModel.DetailHTML;
            Product.LangID = dbModel.LangID;
            Product.ProductID = dbModel.ProductID;
            Product.PStatus = dbModel.PStatus;
            Product.Title = dbModel.Title;
            Product.Summary = dbModel.Summary;
            Product.MetaDescription = dbModel.MetaDescription;
            Product.MetaKeywords = dbModel.MetaKeywords;
            Product.CanonicalLink = dbModel.CanonicalLink;
            Product.Url = dbModel.Url;

            return Product;
        }

        public OperationResult<ProductLocalLangCreateEditVM> Save(ProductLocalLangCreateEditVM Product)
        {
            var result = new OperationResult<ProductLocalLangCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Product, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            ProductDetail existing = null;

            if (Product.ID != 0)
            {
                existing = db.ProductDetails.Where(x => x.ID == Product.ID).FirstOrDefault();
                existing = MapToDBObject(Product, existing);
            }
            else
            {
                existing = MapToDBObject(Product, null);
                db.Add(existing);
            }

            if (String.IsNullOrWhiteSpace(existing.Url))
            {
                existing.Url = "/" + Utils.FormatUrl(Product.ProductName);
            }
            else if(!existing.Url.StartsWith('/'))
            {
                existing.Url = "/" + existing.Url;
            }            

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Product.ID == 0 ? QueueItemType.ProductCreated : QueueItemType.ProductUpdated, JsonConvert.SerializeObject(result.Result), Product.StoreID);

            return result;

        }


    }
}
