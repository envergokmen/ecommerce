﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Stocks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class StockService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public StockService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<StockCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10, int? productId = null)
        {
            var operationResult = new PagedOperationListResult<StockCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            var totalQuery = (from p in db.Stocks
                              where
                               p.StoreAccountID == storeId && p.PStatus == Status.Active
                                && (productId == null || p.ProductID == productId)
                              select p);

            var query = (from p in db.Stocks
                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                         && (productId==null || p.ProductID==productId)
                         orderby p.ID
                         select p).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize));

            //if (productId.HasValue && productId > 0)
            //{
            //    totalQuery = totalQuery.Where(x => x.ProductID == productId);
            //    query = query.Where(x => x.ProductID == productId);
            //}


            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {
                totalQuery = totalQuery.Where(p => p.Barkod.ToLower().Contains(searchKeyword.ToLower()) || p.Product.ProductCode.ToLower().Contains(searchKeyword.ToLower()));
                query = query.Where(p => p.Barkod.ToLower().Contains(searchKeyword.ToLower()));
            }

            operationResult.Result = query.Select(p =>
                              new StockCreateEditVM
                              {
                                  ID = p.ID,
                                  Barkod = p.Barkod,
                                  Quantity = p.Quantity,
                                  SizeID=p.SizeID,
                                  ColorID=p.ColorID,
                                  PStatus = p.PStatus,
                                  ProductCode = p.Product.ProductCode,
                                  ProductID = p.ProductID
                              }).ToList();

            foreach (var item in operationResult.Result)
            {
                if (item.SizeID != null)
                {

                }
            }

            MapColors(operationResult);
            MapSizes(operationResult);

            totalItemCount = totalQuery.Count();

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        private void MapSizes(PagedOperationListResult<StockCreateEditVM> operationResult)
        {
            Dictionary<int, string> sizes = new Dictionary<int, string>();

            foreach (var item in operationResult.Result)
            {
                if (item.SizeID.HasValue)
                {
                    if (sizes.ContainsKey(item.SizeID.Value))
                    {
                        item.SizeName = sizes[item.SizeID.Value];
                    }
                    else
                    {
                        item.SizeName = db.Sizes.Where(x => x.ID == item.SizeID).Select(c => c.Name).FirstOrDefault();
                        sizes.Add(item.SizeID.Value, item.SizeName);
                    }
                }
            }
        }

        private void MapColors(PagedOperationListResult<StockCreateEditVM> operationResult)
        {
            Dictionary<int, string> colors = new Dictionary<int, string>();

            foreach (var item in operationResult.Result)
            {
                if (item.ColorID.HasValue)
                {
                    if (colors.ContainsKey(item.ColorID.Value))
                    {
                        item.ColorName = colors[item.ColorID.Value];
                    }
                    else
                    {
                        item.ColorName = db.Colors.Where(x => x.ID == item.ColorID).Select(c => c.Name).FirstOrDefault();
                        colors.Add(item.ColorID.Value, item.ColorName);
                    }
                }
            }
        }

        public OperationResult<StockCreateEditVM> Get(int id, int storeId, int typeid)
        {
            var result = new OperationResult<StockCreateEditVM>();

            var allSizes = db.Sizes.Where(x => x.PStatus == Status.Active && x.StoreAccountID==storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allColors = db.Colors.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();


            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new StockCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllSizes = allSizes;
                result.Result.AllColors = allColors;
                result.Result.ProductID = typeid;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Stocks.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new StockCreateEditVM
             {
                 ID = p.ID,
                 Barkod = p.Barkod,
                 PStatus = p.PStatus,
                 ProductID = p.ProductID,
                 SizeID = p.SizeID,
                 ColorID = p.ColorID,
                 Quantity = p.Quantity,
                 YapCode = p.YapCode

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllSizes = allSizes;
                result.Result.AllColors = allColors;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Policy = db.Stocks.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Policy != null)
            {
                Policy.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.StockDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Stock MapToDBObject(StockCreateEditVM model, Stock dbModel)
        {
            var Policy = dbModel ?? new Stock();

            Policy.ID = model.ID;

            Policy.Barkod = model.Barkod;
            Policy.YapCode = model.YapCode;
            Policy.PStatus = model.PStatus;
            Policy.SizeID = model.SizeID;
            Policy.ProductID = model.ProductID;
            Policy.ColorID = model.ColorID;
            Policy.Quantity = model.Quantity;
          
            Policy.StoreAccountID = model.ID == 0 ? model.StoreID : Policy.StoreAccountID;
            Policy.ModifiedOn = DateTime.Now;
            if (Policy.ID != 0) Policy.CreatedOn = DateTime.Now;

            return Policy;

        }

        private StockCreateEditVM MapToVM(Stock dbModel)
        {
            if (dbModel == null) return null;

            var stock = new StockCreateEditVM();
            stock.ID = dbModel.ID;
            stock.Barkod = dbModel.Barkod;
            stock.ColorID = dbModel.ColorID;
            stock.SizeID = dbModel.SizeID;
            stock.Quantity = dbModel.Quantity;
            stock.YapCode = dbModel.YapCode;
            stock.ProductID = dbModel.ProductID;
            stock.PStatus = dbModel.PStatus;
            stock.SetStoreId(dbModel.StoreAccountID);

            return stock;
        }

        public OperationResult<StockCreateEditVM> Save(StockCreateEditVM stock)
        {
            var result = new OperationResult<StockCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(stock, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Stock existing = null;

            if (stock.ID != 0)
            {
                existing = db.Stocks.Where(x => x.ID == stock.ID).FirstOrDefault();
                existing = MapToDBObject(stock, existing);
            }
            else
            {
                existing = MapToDBObject(stock, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(stock.ID == 0 ? QueueItemType.StockCreated : QueueItemType.StockUpdated, JsonConvert.SerializeObject(result.Result), stock.StoreID);

            return result;

        }


    }
}
