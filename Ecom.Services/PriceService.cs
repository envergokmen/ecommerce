﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Prices;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class PriceService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public PriceService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<PriceCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10, int? catalogId = null)
        {
            var operationResult = new PagedOperationListResult<PriceCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            var totalQuery = (from p in db.Prices
                              where
                               p.StoreAccountID == storeId && p.PStatus == Status.Active
                              select p);

            var query = (from p in db.Prices
                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                         orderby p.ID
                         select p).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize));

            if (catalogId.HasValue && catalogId > 0)
            {
                totalQuery = totalQuery.Where(x => x.ProductID == catalogId);
                query = query.Where(x => x.ProductID == catalogId);
            }


            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {
                totalQuery = totalQuery.Where(p => p.Product.ProductCode.ToLower().Contains(searchKeyword.ToLower()));
                query = query.Where(p => p.Product.ProductCode.ToLower().Contains(searchKeyword.ToLower()));
            }

            operationResult.Result = query.Select(p =>
                              new PriceCreateEditVM
                              {
                                  ID = p.ID,
                                  Amount = p.Amount,
                                  AmountOld = p.AmountOld,
                                  PStatus = p.PStatus,
                                  CurrencyCode = p.PriceCurrency.CurrencyCode,
                                  ProductCode = p.Product.ProductCode,
                                  ProductID = p.ProductID,
                                  StockID = p.StockID,
                                  VAT = p.VAT
                              }).ToList();


            totalItemCount = totalQuery.Count();

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<PriceCreateEditVM> Get(int id, int storeId, int productId, int stockId)
        {
            var result = new OperationResult<PriceCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allDomains = db.Domains.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new PriceCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllDomains = allDomains;
                result.Result.AllCurrencies = allCurrencies;
                result.Result.ProductID = productId > 0 ? productId : null as long?;
                result.Result.StockID = stockId > 0 ? stockId : null as long?;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Prices.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new PriceCreateEditVM
             {
                 ID = p.ID,
                 Amount = p.Amount,
                 AmountOld = p.AmountOld,
                 PStatus = p.PStatus,
                 ProductID = p.ProductID,
                 PriceCurrencyID = p.PriceCurrencyID,
                 DomainID = p.DomainID,
                 StockID = p.StockID,
                 VAT = p.VAT,
                 ProductCode = p.ProductCode,
                 Barkod = p.Barkod

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;

                result.Result.AllStatuses = allStatuses;
                result.Result.AllDomains = allDomains;
                result.Result.AllCurrencies = allCurrencies;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var price = db.Prices.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (price != null)
            {
                price.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.PriceDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Price MapToDBObject(PriceCreateEditVM model, Price dbModel)
        {
            var Product = dbModel ?? new Price();

            Product.ID = model.ID;

            Product.Amount = model.Amount;
            Product.PStatus = model.PStatus;
            Product.AmountOld = model.AmountOld;
            Product.ProductID = model.ProductID.HasValue ? model.ProductID.Value : 0;
            Product.DomainID = model.DomainID;
            Product.VAT = model.VAT;
            Product.PriceCurrencyID = model.PriceCurrencyID;
            Product.StockID = model.StockID;
            Product.ProductCode = model.ProductCode;
            Product.Barkod = model.Barkod;

            Product.StoreAccountID = model.ID == 0 ? model.StoreID : Product.StoreAccountID;
            Product.ModifiedOn = DateTime.Now;
            if (Product.ID != 0) Product.CreatedOn = DateTime.Now;

            return Product;

        }

        private PriceCreateEditVM MapToVM(Price dbModel)
        {
            if (dbModel == null) return null;

            var Product = new PriceCreateEditVM();
            Product.ID = dbModel.ID;

            Product.Amount = dbModel.Amount;
            Product.PStatus = dbModel.PStatus;
            Product.AmountOld = dbModel.AmountOld;
            Product.ProductID = dbModel.ProductID;
            Product.DomainID = dbModel.DomainID;
            Product.VAT = dbModel.VAT;
            Product.PriceCurrencyID = dbModel.PriceCurrencyID;
            Product.StockID = dbModel.StockID;
            Product.ProductCode = dbModel.ProductCode;
            Product.Barkod = dbModel.Barkod;
            Product.SetStoreId(dbModel.StoreAccountID);

            return Product;
        }

        public OperationResult<PriceCreateEditVM> Save(PriceCreateEditVM price)
        {
            var result = new OperationResult<PriceCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(price, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Price existing = null;

            if (price.ID != 0)
            {
                existing = db.Prices.Where(x => x.ID == price.ID).FirstOrDefault();
                existing = MapToDBObject(price, existing);
            }
            else
            {
                existing = MapToDBObject(price, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(price.ID == 0 ? QueueItemType.PriceCreated : QueueItemType.PriceUpdated, JsonConvert.SerializeObject(result.Result), price.StoreID);

            return result;

        }


    }
}
