﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.EmailTemplateLocalLangs;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class EmailTemplateLocalLangService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public EmailTemplateLocalLangService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<EmailTemplateLocalLangCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10, int? emailTemplateId = null)
        {
            var operationResult = new PagedOperationListResult<EmailTemplateLocalLangCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            var totalQuery = (from p in db.EmailTemplateLocalLangs
                              where
                               p.StoreAccountID == storeId && p.PStatus == Status.Active
                              select p);

            var query = (from p in db.EmailTemplateLocalLangs
                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                         orderby p.ID
                         select p).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize));

            if (emailTemplateId.HasValue && emailTemplateId > 0)
            {
                totalQuery = totalQuery.Where(x => x.EmailTemplateID == emailTemplateId);
                query = query.Where(x => x.EmailTemplateID == emailTemplateId);
            }


            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {
                totalQuery = totalQuery.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
                query = query.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
            }

            operationResult.Result = query.Select(p =>
                              new EmailTemplateLocalLangCreateEditVM
                              {
                                  ID = p.ID,
                                  Name = p.Name,
                                  PStatus = p.PStatus,
                                  LangCode = p.Lang.LangCode,
                                  ParentName = p.EmailTemplate.Name,
                                  EmailTemplateID = p.EmailTemplateID
                              }).ToList();


            totalItemCount = totalQuery.Count();

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<EmailTemplateLocalLangCreateEditVM> Get(int id, int storeId, int typeid)
        {
            var result = new OperationResult<EmailTemplateLocalLangCreateEditVM>();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allEmailTemplates = db.EmailTemplates.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();


            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new EmailTemplateLocalLangCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllEmailTemplates = allEmailTemplates;
                result.Result.EmailTemplateID = typeid;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.EmailTemplateLocalLangs.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new EmailTemplateLocalLangCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 EmailTemplateID = p.EmailTemplateID,
                 DescriptionHTML = p.DescriptionHTML,
                 LangID = p.LangID

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllEmailTemplates = allEmailTemplates;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var EmailTemplate = db.EmailTemplateLocalLangs.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (EmailTemplate != null)
            {
                EmailTemplate.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.EmailTemplateDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private EmailTemplateLocalLang MapToDBObject(EmailTemplateLocalLangCreateEditVM model, EmailTemplateLocalLang dbModel)
        {
            var EmailTemplate = dbModel ?? new EmailTemplateLocalLang();

            EmailTemplate.ID = model.ID;

            EmailTemplate.Name = model.Name;
            EmailTemplate.PStatus = model.PStatus;
            EmailTemplate.LangID = model.LangID;
            EmailTemplate.EmailTemplateID = model.EmailTemplateID;
            EmailTemplate.DescriptionHTML = model.DescriptionHTML;
          
            EmailTemplate.StoreAccountID = model.ID == 0 ? model.StoreID : EmailTemplate.StoreAccountID;
            EmailTemplate.ModifiedOn = DateTime.Now;
            if (EmailTemplate.ID != 0) EmailTemplate.CreatedOn = DateTime.Now;

            return EmailTemplate;

        }

        private EmailTemplateLocalLangCreateEditVM MapToVM(EmailTemplateLocalLang dbModel)
        {
            if (dbModel == null) return null;

            var EmailTemplate = new EmailTemplateLocalLangCreateEditVM();
            EmailTemplate.ID = dbModel.ID;
            EmailTemplate.Name = dbModel.Name;
            EmailTemplate.DescriptionHTML = dbModel.DescriptionHTML;
            EmailTemplate.LangID = dbModel.LangID;
            EmailTemplate.EmailTemplateID = dbModel.EmailTemplateID;
            EmailTemplate.PStatus = dbModel.PStatus;
           
            return EmailTemplate;
        }

        public OperationResult<EmailTemplateLocalLangCreateEditVM> Save(EmailTemplateLocalLangCreateEditVM EmailTemplate)
        {
            var result = new OperationResult<EmailTemplateLocalLangCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(EmailTemplate, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            EmailTemplateLocalLang existing = null;

            if (EmailTemplate.ID != 0)
            {
                existing = db.EmailTemplateLocalLangs.Where(x => x.ID == EmailTemplate.ID).FirstOrDefault();
                existing = MapToDBObject(EmailTemplate, existing);
            }
            else
            {
                existing = MapToDBObject(EmailTemplate, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(EmailTemplate.ID == 0 ? QueueItemType.EmailTemplateCreated : QueueItemType.EmailTemplateUpdated, JsonConvert.SerializeObject(result.Result), EmailTemplate.StoreID);

            return result;

        }


    }
}
