﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.ProductStyles;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class ProductStyleService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public ProductStyleService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<ProductStyleCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<ProductStyleCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.ProductStyles
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.ProductStyles
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.ID
                                          select new ProductStyleCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.ProductStyles
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.ProductStyles.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.ID).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new ProductStyleCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<ProductStyleCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<ProductStyleCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new ProductStyleCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.ProductStyles.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new ProductStyleCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var ProductStyle = db.ProductStyles.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (ProductStyle != null)
            {
                ProductStyle.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ProductStyleDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private ProductStyle MapToDBObject(ProductStyleCreateEditVM model, ProductStyle dbModel)
        {
            var ProductStyle = dbModel ?? new ProductStyle();

            ProductStyle.ID = model.ID;

            ProductStyle.Name = model.Name;
            ProductStyle.PStatus = model.PStatus;

            ProductStyle.StoreAccountID = model.ID == 0 ? model.StoreID : ProductStyle.StoreAccountID;
            ProductStyle.ModifiedOn = DateTime.Now;
            if (ProductStyle.ID != 0) ProductStyle.CreatedOn = DateTime.Now;

            return ProductStyle;

        }

        private ProductStyleCreateEditVM MapToVM(ProductStyle dbModel)
        {
            if (dbModel == null) return null;

            var ProductStyle = new ProductStyleCreateEditVM();
            ProductStyle.ID = dbModel.ID;
            ProductStyle.Name = dbModel.Name;
            ProductStyle.PStatus = dbModel.PStatus;

            return ProductStyle;
        }

        public OperationResult<ProductStyleCreateEditVM> Save(ProductStyleCreateEditVM ProductStyle)
        {
            var result = new OperationResult<ProductStyleCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(ProductStyle, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            ProductStyle existing = null;

            if (ProductStyle.ID != 0)
            {
                existing = db.ProductStyles.Where(x => x.ID == ProductStyle.ID).FirstOrDefault();
                existing = MapToDBObject(ProductStyle, existing);
            }
            else
            {
                existing = MapToDBObject(ProductStyle, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(ProductStyle.ID == 0 ? QueueItemType.ProductStyleCreated : QueueItemType.ProductStyleUpdated, JsonConvert.SerializeObject(result.Result), ProductStyle.StoreID);

            return result;

        }


    }
}
