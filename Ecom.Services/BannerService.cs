﻿using Ecom.Core;
using Ecom.Models;
using Ecom.ViewModels.Banners;
using System;
using System.Linq;
using Microsoft.Extensions.Localization;
using Ecom.Services.Bus;
using Newtonsoft.Json;
using Ecom.ViewModels;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;

namespace Ecom.Services
{
    public class BannerService
    {
        private readonly IMemoryCache cache;
        private readonly EcomContext db;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        public BannerService(
            IMemoryCache _cache,
            IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            EcomContext _db,
            RabbitMQService _rabbitMQService
            )
        {
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            this.db = _db;
            cache = _cache;

        }

        public PagedOperationListResult<BannerCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<BannerCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Banners
                                  where
                                   p.PStatus == Status.Active &&
                                   p.StoreAccountID == storeId &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p)
                                             .OrderBy(c => c.SortNo).Count();

                operationResult.Result = (from p in db.Banners
                                          where
                                          p.PStatus == Status.Active &&
                                          p.StoreAccountID == storeId
                                          && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new BannerCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              SortNo = p.SortNo,
                                              Link = p.Link,
                                              ImagePath = p.ImagePath,
                                              StoreAccountID = p.StoreAccountID,
                                              BannerShowPlace = p.BannerShowPlace,
                                              LangID = p.LangID,
                                              DomainID = p.DomainID

                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Banners
                                  where p.StoreAccountID == storeId && p.PStatus == Status.Active
                                  select p).Count();

                operationResult.Result = db.Banners.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new BannerCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus,
                      SortNo = p.SortNo,
                      Link = p.Link,
                      ImagePath = p.ImagePath,
                      StoreAccountID = p.StoreAccountID,
                      BannerShowPlace = p.BannerShowPlace,
                      LangID = p.LangID,
                      DomainID = p.DomainID,
                  }

                ).ToList();
            }

            //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<BannerCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<BannerCreateEditVM>();

            var allShowPlaces = serviceUtils.ToSelectList<BannerPlace>(0);

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allDomains = db.Domains.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new BannerCreateEditVM();
                result.Result.AllBannerPlaces = allShowPlaces;
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllDomains = allDomains;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Banners.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p => new BannerCreateEditVM
            {
                ID = p.ID,
                Name = p.Name,
                PStatus = p.PStatus,
                SortNo = p.SortNo,
                Link = p.Link,
                ImagePath = p.ImagePath,
                StoreAccountID = p.StoreAccountID,
                BannerShowPlace = p.BannerShowPlace,
                LangID = p.LangID,
                DomainID = p.DomainID

            }).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllBannerPlaces = allShowPlaces;
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllDomains = allDomains;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var brand = db.Banners.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (brand != null)
            {
                brand.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.BannerDeleted, JsonConvert.SerializeObject(result.Result), storeId);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Banner MapToDBObject(BannerCreateEditVM model, Banner dbModel)
        {
            var brand = dbModel ?? new Banner();


            brand.ID = model.ID;
            brand.Name = model.Name;
            brand.PStatus = model.PStatus;
            brand.SortNo = model.SortNo;
            brand.BannerShowPlace = model.BannerShowPlace;
            brand.ImagePath = !String.IsNullOrWhiteSpace(model.ImagePath) ? model.ImagePath : dbModel?.ImagePath ?? "";
            brand.Link = model.Link;
            brand.StoreAccountID = model.StoreID;
            brand.LangID = model.LangID;
            brand.DomainID = model.DomainID;
            //brand.ModifiedById= //current user
            brand.ModifiedOn = DateTime.Now;
            if (brand.ID != 0) brand.CreatedOn = DateTime.Now;

            return brand;

        }

        private BannerCreateEditVM MapToVM(Banner model)
        {
            if (model == null) return null;

            var brand = new BannerCreateEditVM();
            brand.ID = model.ID;
            brand.Name = model.Name;
            brand.PStatus = model.PStatus;
            brand.SortNo = model.SortNo;
            brand.BannerShowPlace = model.BannerShowPlace;
            brand.ImagePath = model.ImagePath;
            brand.Link = model.Link;
            brand.LangID = model.LangID;
            brand.DomainID = model.DomainID;
            brand.StoreAccountID = model.StoreAccountID;
            return brand;

        }

        public StandartOperationResult DeleteImage(int id, int storeId)
        {
            var result = new StandartOperationResult();

            var brand = db.Banners.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (brand != null)
            {
                brand.ImagePath = "";
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.BannerUpdated, JsonConvert.SerializeObject(result.Result), storeId);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        public OperationResult<BannerCreateEditVM> Save(BannerCreateEditVM brand)
        {
            var result = new OperationResult<BannerCreateEditVM>();

            Banner existing = null;

            if (brand.ID != 0)
            {
                existing = db.Banners.Where(x => x.ID == brand.ID).FirstOrDefault();
                existing = MapToDBObject(brand, existing);
            }
            else
            {
                existing = MapToDBObject(brand, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(brand.ID == 0 ? QueueItemType.BannerCreated : QueueItemType.BannerUpdated, JsonConvert.SerializeObject(result.Result), brand.StoreID);

            return result;

        }


        public List<BannerCreateEditVM> GetAllActiveBanners(int storeId)
        {
            string cacheKey = String.Concat("AllActiveBanners-", storeId);

            var banners = cache.Get<List<BannerCreateEditVM>>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (banners == null || true)
            {
                banners = db.Banners.Where(x => x.StoreAccountID == storeId 
                && x.PStatus == Status.Active)
                .OrderBy(c => c.SortNo)
                .Select(p =>
                  new BannerCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus,
                      SortNo = p.SortNo,
                      Link = p.Link,
                      ImagePath = p.ImagePath,
                      StoreAccountID = p.StoreAccountID,
                      BannerShowPlace = p.BannerShowPlace,
                      LangID = p.LangID,
                      DomainID = p.DomainID,
                  }

                ).ToList();

                if (banners != null)
                {
                    cache.Set(cacheKey, banners, DateTime.Now.AddMinutes(30));
                }
            }

            return banners;
        }



    }
}
