﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.WebPosInstallments;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class WebPosInstallmentService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public WebPosInstallmentService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<WebPosInstallmentCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10, int? webPosId = null)
        {
            var operationResult = new PagedOperationListResult<WebPosInstallmentCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            var totalQuery = (from p in db.WebPosInstallments
                              where
                               p.StoreAccountID == storeId && p.PStatus == Status.Active
                              select p);

            var query = (from p in db.WebPosInstallments
                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                         orderby p.ID
                         select p).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize));

            if (webPosId.HasValue && webPosId > 0)
            {
                totalQuery = totalQuery.Where(x => x.WebPosID == webPosId);
                query = query.Where(x => x.WebPosID == webPosId);
            }


            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {
                totalQuery = totalQuery.Where(p => p.Title.ToLower().Contains(searchKeyword.ToLower()) || p.WebPos.Name.ToLower().Contains(searchKeyword.ToLower()));
                query = query.Where(p => p.Title.ToLower().Contains(searchKeyword.ToLower()));
            }

            operationResult.Result = query.Select(p =>
                              new WebPosInstallmentCreateEditVM
                              {
                                  ID = p.ID,
                                  Title = p.Title,
                                  Quantity = p.Quantity,
                                  DelayedMouth = p.DelayedMouth,
                                  Rate = p.Rate,
                                  ExtraAmount = p.ExtraAmount,
                                  IsDefault=p.IsDefault,
                                  PStatus = p.PStatus,
                                  WebPosID = p.WebPosID
                              }).ToList();


            totalItemCount = totalQuery.Count();

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<WebPosInstallmentCreateEditVM> Get(int id, int storeId, int typeid)
        {
            var result = new OperationResult<WebPosInstallmentCreateEditVM>();

            var allWebPoses = db.WebPoses.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();



            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new WebPosInstallmentCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllWebPoses = allWebPoses;
                result.Result.WebPosID = typeid;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.WebPosInstallments.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new WebPosInstallmentCreateEditVM
             {
                 ID = p.ID,
                 Title = p.Title,
                 PStatus = p.PStatus,
                 WebPosID = p.WebPosID,
                 DelayedMouth = p.DelayedMouth,
                 DownLimit = p.DownLimit,
                 UpLimit = p.UpLimit,
                 ExtraAmount = p.ExtraAmount,
                 IsDefault = p.IsDefault,
                 PlusInstallmentLimit = p.PlusIntallmentAmount,
                 PlusIntallmentAmount = p.PlusIntallmentAmount,
                 Quantity = p.Quantity,
                 Rate = p.Rate,
                 SortNo = p.SortNo,


             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllWebPoses = allWebPoses;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Policy = db.WebPosInstallments.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Policy != null)
            {
                Policy.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.PolicyDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private WebPosInstallment MapToDBObject(WebPosInstallmentCreateEditVM model, WebPosInstallment dbModel)
        {
            var webPosInstallment = dbModel ?? new WebPosInstallment();

            webPosInstallment.ID = model.ID;

            webPosInstallment.Title = model.Title;
            webPosInstallment.PStatus = model.PStatus;
            webPosInstallment.WebPosID = model.WebPosID;
            webPosInstallment.DelayedMouth = model.DelayedMouth;
            webPosInstallment.DownLimit = model.DownLimit;
            webPosInstallment.UpLimit = model.UpLimit;
            webPosInstallment.ExtraAmount = model.ExtraAmount;
            webPosInstallment.IsDefault = model.IsDefault;
            webPosInstallment.PlusInstallmentLimit = model.PlusIntallmentAmount;
            webPosInstallment.PlusIntallmentAmount = model.PlusIntallmentAmount;
            webPosInstallment.Quantity = model.Quantity;
            webPosInstallment.Rate = model.Rate;
            webPosInstallment.SortNo = model.SortNo;

            webPosInstallment.StoreAccountID = model.ID == 0 ? model.StoreID : webPosInstallment.StoreAccountID;
            webPosInstallment.ModifiedOn = DateTime.Now;
            if (webPosInstallment.ID != 0) webPosInstallment.CreatedOn = DateTime.Now;

            return webPosInstallment;

        }

        private WebPosInstallmentCreateEditVM MapToVM(WebPosInstallment dbModel)
        {
            if (dbModel == null) return null;

            var webPosInstallment = new WebPosInstallmentCreateEditVM();
            webPosInstallment.ID = dbModel.ID;

            webPosInstallment.Title = dbModel.Title;
            webPosInstallment.PStatus = dbModel.PStatus;
            webPosInstallment.WebPosID = dbModel.WebPosID;
            webPosInstallment.DelayedMouth = dbModel.DelayedMouth;
            webPosInstallment.DownLimit = dbModel.DownLimit;
            webPosInstallment.UpLimit = dbModel.UpLimit;
            webPosInstallment.ExtraAmount = dbModel.ExtraAmount;
            webPosInstallment.IsDefault = dbModel.IsDefault;
            webPosInstallment.PlusInstallmentLimit = dbModel.PlusIntallmentAmount;
            webPosInstallment.PlusIntallmentAmount = dbModel.PlusIntallmentAmount;
            webPosInstallment.Quantity = dbModel.Quantity;
            webPosInstallment.Rate = dbModel.Rate;
            webPosInstallment.SortNo = dbModel.SortNo;

            return webPosInstallment;
        }

        public OperationResult<WebPosInstallmentCreateEditVM> Save(WebPosInstallmentCreateEditVM webPosInstallment)
        {
            var result = new OperationResult<WebPosInstallmentCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(webPosInstallment, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            WebPosInstallment existing = null;

            if (webPosInstallment.ID != 0)
            {
                existing = db.WebPosInstallments.Where(x => x.ID == webPosInstallment.ID).FirstOrDefault();
                existing = MapToDBObject(webPosInstallment, existing);
            }
            else
            {
                existing = MapToDBObject(webPosInstallment, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(webPosInstallment.ID == 0 ? QueueItemType.PolicyCreated : QueueItemType.PolicyUpdated, JsonConvert.SerializeObject(result.Result), webPosInstallment.StoreID);

            return result;

        }


    }
}
