﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Addresses;
using Ecom.ViewModels.Catalogs;
using Ecom.ViewModels.Colors;
using Ecom.ViewModels.Orders;
using Ecom.ViewModels.Pages;
using Ecom.ViewModels.Payments;
using Ecom.ViewModels.Prices;
using Ecom.ViewModels.Products;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class OrderService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        private readonly PriceCurrencyService priceCurrencyService;
        private readonly ProductService productService;
        private readonly PaymentService paymentService;
        private readonly CargoService cargoService;
        private readonly CartService cartService;
        private readonly AddressService addressService;

        private readonly EcomContext db;

        public OrderService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            PriceCurrencyService _priceCurrencyService,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService,
            CartService _cartService,
            ProductService _productService,
            PaymentService _paymentService,
            CargoService _cargoService,
            AddressService _addressService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            this.priceCurrencyService = _priceCurrencyService;
            this.productService = _productService;
            this.paymentService = _paymentService;
            this.cargoService = _cargoService;
            this.addressService = _addressService;
            this.cartService = _cartService;
        }


        public List<OrderDetailsVM> GetUserOrders(int storeid, int? userId, string session, string email, PaymentType? orderType = null)
        {
            var orders = (from p in db.Orders
                          where
                          (p.UserID == userId || (p.Session == session && p.Email == email))
                          && (orderType == null || p.PaymentType == orderType)
                          orderby p.ID descending
                          select new OrderDetailsVM
                          {
                              TrackCode = p.TrackCode,
                              CreatedOn = p.CreatedOn.Value,
                              ID = p.ID,
                              PaymentType = p.PaymentType,
                              TotalAmount = p.TotalAmount.Value
                          }).ToList();

            return orders;
        }

        public OrderDetailsVM GetOrderDetails(int storeid, long orderid, string trackCode, int userId, string session, string email)
        {
            OrderDetailsVM orderVM = new OrderDetailsVM();
            var ord =
                db.Orders
                .Include(c => c.OrderedItems)
                .Include(c => c.Cargo)
                .Where(c => c.StoreAccountID == storeid && (c.ID == orderid || c.TrackCode == trackCode) && (c.UserID == userId || (c.Session == session && c.Email == email)))
                .FirstOrDefault();

            MapOderDetails(orderVM, ord);

            return orderVM;
        }

        public OrderDetailsVM GetOrderDetails(int storeid, long orderid)
        {
            OrderDetailsVM orderVM = new OrderDetailsVM();
            var ord =
                db.Orders
                .Include(c => c.OrderedItems)
                .Include(c => c.Cargo)
                .Where(c => c.StoreAccountID == storeid && c.ID == orderid)
                .FirstOrDefault();

            MapOderDetails(orderVM, ord);

            return orderVM;
        }

        private void MapOderDetails(OrderDetailsVM orderVM, Order ord)
        {
            if (ord != null)
            {
                var installment = paymentService.GetWebPosInstallmentByID(ord.StoreAccountID ?? 0, ord.WebPosInstallmentID ?? 0, includePassives: true);
                var priceCurrenncy = priceCurrencyService.GetCurrencyById(ord.PriceCurrencyID ?? 0, ord.StoreAccountID ?? 0);

                //order.Balance= dborder.bala
                orderVM.TrackCode = ord.TrackCode;
                orderVM.StoreAccountId = ord.StoreAccountID ?? 0;
                orderVM.CreatedOn = ord.CreatedOn ?? DateTime.Now;
                orderVM.CurrencyCode = priceCurrenncy?.CurrencyCode;
                orderVM.CurrencyId = priceCurrenncy?.PriceCurrencyID;
                orderVM.IsPaid = ord.IsPaid;
                orderVM.CurrencySymbol = priceCurrenncy?.CurrencySymbol;
                orderVM.TotalAmount = ord.TotalAmount ?? 0;
                orderVM.ChangeOrderDate = ord.LastItemChangeDate;

                //user
                orderVM.Name = ord.Name;
                orderVM.Email = ord.Email;
                orderVM.Session = ord.Session;

                //cargo
                orderVM.CargoAmount = ord.CargoAmount;
                orderVM.CargoLink = ord.CargoLink;
                orderVM.CargoName = ord.Cargo?.Name;
                orderVM.CargoCode = ord.CargoTrackingCode;
                orderVM.OneDayCargo = ord.OneDayCargo;
                orderVM.IsCargoTransferred = ord.IsCargoTransferred;
                orderVM.ShipAsGift = ord.ShipAsGift;

                //billing address
                if (ord.BillingAdressID.HasValue || ord.BillingAdressDetail != null)
                {
                    orderVM.BillingAddress = new AddressVM
                    {
                        CityName = addressService.GetCity(ord.BillingAdressCityID ?? 0)?.Name,
                        TownName = addressService.GetCity(ord.BillingAdressTownID ?? 0)?.Name,
                        CountryName = addressService.GetCity(ord.BillingAdressCountryID ?? 0)?.Name,
                        Detail = ord.BillingAdressDetail,
                        Phone = ord.BillingAdressPhone,
                        TcNo = ord.BillingAdressTcNo
                    };
                }

                //shipping address
                if (ord.DeliveryAdressID.HasValue || ord.DeliveryAdressDetail != null)
                {
                    orderVM.DeliveryAddress = new AddressVM
                    {
                        CityName = addressService.GetCity(ord.DeliveryAdressCityID ?? 0)?.Name,
                        TownName = addressService.GetCity(ord.DeliveryAdressTownID ?? 0)?.Name,
                        CountryName = addressService.GetCity(ord.DeliveryAdressCountryID ?? 0)?.Name,
                        Detail = ord.DeliveryAdressDetail,
                        Phone = ord.DeliveryAdressPhone,
                        TcNo = ord.DeliveryAdressTcNo
                    };
                }

                //payment
                orderVM.WebPosInstallment = installment;
                orderVM.PaymentName = installment?.PaymentName;
                orderVM.PaymentStatus = ord.PaymentStatus;
                orderVM.PaymentType = ord.PaymentType;
                orderVM.PosName = installment?.PosName;
                orderVM.InstallmentQuantity = installment?.Quantity;

                //coupon
                orderVM.CouponAmount = ord.CouponAmount;
                orderVM.CouponCode = ord.CouponCode;
                orderVM.CouponID = ord.CouponID;

                //return
                orderVM.ReturnedOrderID = ord.ReturnedOrderID;

                //erp  //TODO:change to ERP status instead of order status
                orderVM.IsReturnedToNetsis = ord.IsReturnedToNetsis;
                orderVM.IsTransferedToErp = ord.IsTransferedToNetsis;


                foreach (var item in ord.OrderedItems)
                {
                    OrderedItemVM oi = new OrderedItemVM();
                    oi.Amount = item.Amount ?? 0;
                    oi.AraToplam = item.Amount ?? 0 * item.Quantity ?? 0;
                    oi.ProductName = item.Name;
                    oi.Quantity = item.Quantity ?? 1;
                    oi.ReturnStatus = item.ReturnStatus;
                    oi.SizeName = item.SizeName;
                    oi.ColorName = item.ColorName;
                    oi.CreatedOn = item.CreatedOn ?? DateTime.Now;
                    oi.ID = item.ID;
                    oi.ImagePath = item.ImagePath;
                    oi.StockID = item.StockID ?? 0;
                    oi.ProductID = item.ProductID ?? 0;
                    oi.URL = item.Url;

                    orderVM.OrderedItems.Add(oi);
                }


            }
        }

        public OrderCreationResult<OrderDetailsVM> CreateOrderFromCheckState(CheckoutVM checkoutVM)
        {
            var result = new OrderCreationResult<OrderDetailsVM>();
            ValidateStateModel(checkoutVM, result);

            if (!result.Status) { return result; }

            result.SetErrorAndMarkAsErrored();

            if (checkoutVM.SelectedWebPosInstallment.PaymentType == PaymentType.Havale)
            {
                var order = new Order();

                //TrackCode and User
                SetMainOrderInfo(order, checkoutVM);

                //SetPaymentAndCoupon
                SetPaymentAndCoupon(order, checkoutVM);

                //Cargo And Shiiping
                SetCargoAndShippping(order, checkoutVM);

                //Set Delivery Address
                SetOrderDeliveryAddress(order, checkoutVM);

                //Set Billing Address
                SetOrderBillingAddress(order, checkoutVM);

                //Set User Cart 
                AddOrderedItemsFromCart(order, checkoutVM);

                //Add -(minus) Order History all orders must have - record for Balance Value
                AddMinusPaymentHistory(order, checkoutVM);

                //SetUtmParams
                //SetUtmParams(order);

                db.Orders.Add(order);
                db.SaveChanges();

                var createdOrder = GetOrderDetails(checkoutVM.StoreId, order.ID);
                result.SetSuccessAndClearError(createdOrder);
            }

            if (checkoutVM.SelectedWebPosInstallment.PaymentType == PaymentType.KrediKarti && !String.IsNullOrWhiteSpace(checkoutVM.SelectedWebPosInstallment.SecureKey3D))
            {

                result.IsRedirectRequired = true;
                result.DirectUrl = String.Concat("/", checkoutVM.LangCode, "/payment/", checkoutVM.SelectedWebPosInstallment.PosPaymentType.ToString().ToLower());
            }

            return result;

        }

        public void AddPaymentHistoryLog(PaymentHistoryViewModel ph)
        {
            try
            {
                var paymentHistory = paymentService.ConvertToPaymentHistory(ph);
                db.PaymentHistory.Add(paymentHistory);
                db.SaveChanges();

            }
            catch
            {

            }

        }

        private string GenerateTrackCode(int storeid, int? userid)
        {
            Random rnd = new Random();
            string UserIDFormat = "{0:##000000}";
            int userID = userid == null ? rnd.Next(5000000, 9000000) : (int)userid;
            return String.Format("{0}{1}{2}", storeid, DateTime.Now.ToString("yyMMddHHmmss"), String.Format(UserIDFormat, userID)).Replace("-", ""); ;
        }

        private void SetMainOrderInfo(Order order, CheckoutVM checkoutVM)
        {
            if (order == null || checkoutVM == null) return;

            order.TrackCode = GenerateTrackCode(checkoutVM.StoreId, order.UserID);
            order.UserID = (checkoutVM.UserId.HasValue && checkoutVM.UserId > 0) ? checkoutVM.UserId : null as int?;
            order.Email = (checkoutVM.User != null) ? checkoutVM.User.UserName : "";
            order.Session = checkoutVM.SpecialKey;
            order.Name = (checkoutVM.User != null) ? checkoutVM.User.Name : "";
            order.StoreAccountID = checkoutVM.StoreId;

            //order.OrdNote = checkoutVM.OrdNote;
            //order.Gsm = (checkoutVM.User != null) ? checkoutVM.User.Gsm : "";
            //order.OrderPlace = GetOrderPlace();
        }

        private void SetPaymentAndCoupon(Order order, CheckoutVM checkoutVM)
        {
            if (order == null || checkoutVM == null) return;

            //Toplam Tutarlar
            order.TotalAmount = (decimal)checkoutVM.TotalAmount();
            //order.CommissionAmount = checkoutVM.ComissionAmount;
            //order.CommissionRatio = (decimal)checkoutVM.CommissionRatio;

            //order.CampaignAmount = ? ürün kampanyası;
            //order.CouponID = (checkoutVM.Coupon != null) ? checkoutVM.Coupon.ID : null as int?;
            //order.CouponCode = (checkoutVM.Coupon != null) ? checkoutVM.Coupon.CouponCode : null as string;
            //order.CouponAmount = checkoutVM.CouponAmount;
            //order.ExtraAmount = checkoutVM.ExtraAmount;


            //order.LoyaltyDiscount = checkoutVM.MemberCardGroupDiscount;
            //order.LoyaltyDiscountRatio = (checkoutVM.MemberCardGroup != null) ? checkoutVM.MemberCardGroup.DiscountRatio : 0;
            //order.MemberCardGroupID = (checkoutVM.MemberCardGroup != null) ? checkoutVM.MemberCardGroup.ID : null as int?;

            //Currency And Lang
            order.PriceCurrencyID = checkoutVM.CurrencyId;
            order.LangID = checkoutVM.LangId;
            order.DomainID = checkoutVM.DomainId;

            //WebPos and Payment 
            order.PaymentType = checkoutVM.SelectedWebPosInstallment.PaymentType;
            order.PStatus = (order.PaymentType == PaymentType.Havale || order.PaymentType == PaymentType.Coupon || order.PaymentType == PaymentType.Kapida) ? Status.Active : Status.Passive;

            order.WebPosID = checkoutVM.SelectedWebPosInstallment.WebPosID;
            order.WebPosInstallmentID = checkoutVM.SelectedWebPosInstallment.ID;
            order.Installment = checkoutVM.SelectedWebPosInstallment.Quantity;

            if (order.PaymentType == PaymentType.Havale)
            {
                order.BankID = checkoutVM.BankId ?? null;
            }

            order.PaymentStatus = (order.PaymentType == PaymentType.Havale) ? SiparisOdemeDurumu.OdemeBekliyor : SiparisOdemeDurumu.Onaylanmadi;

        }

        private void SetOrderDeliveryAddress(Order order, CheckoutVM checkoutVM)
        {
            if (order == null || checkoutVM == null || checkoutVM.SelectedShippingAddress == null) return;

            order.DeliveryAdressID = checkoutVM.SelectedShippingAddress.Id;
            order.DeliveryAdressPhone2 = checkoutVM.SelectedShippingAddress.Phone;
            order.DeliveryAdressCountryID = checkoutVM.SelectedShippingAddress.CountryId;
            order.DeliveryAdressCityID = checkoutVM.SelectedShippingAddress.CityId;
            order.DeliveryAdressTownID = checkoutVM.SelectedShippingAddress.TownId;
            order.DeliveryAdressDetail = checkoutVM.SelectedShippingAddress.Detail;

            //UserWants to Move Without MemberShip
            if (String.IsNullOrWhiteSpace(order.Email) && !String.IsNullOrWhiteSpace(checkoutVM.SelectedShippingAddress.Email))
            {
                order.Email = checkoutVM.SelectedShippingAddress.Email;
            }

            if (String.IsNullOrWhiteSpace(order.Name) && !String.IsNullOrWhiteSpace(checkoutVM.SelectedShippingAddress.Name))
            {
                order.Name = checkoutVM.SelectedShippingAddress.Name;
            }

            if (String.IsNullOrWhiteSpace(order.Phone) && !String.IsNullOrWhiteSpace(checkoutVM.SelectedShippingAddress.Phone))
            {
                order.Phone = checkoutVM.SelectedShippingAddress.Phone;
            }

        }

        private void SetCargoAndShippping(Order order, CheckoutVM checkoutVM)
        {
            if (order == null || checkoutVM == null) return;

            //Cargo and shipping
            order.CargoAmount = checkoutVM.SelectedCargoPrice.Amount;
            order.CargoPriceID = checkoutVM.SelectedCargoPrice.Id;
            order.CargoID = checkoutVM.SelectedCargoPrice.CargoID;

            //Gift
            //order.GiftAmount = checkoutVM.GiftAmount;
            //order.GiftNote = checkoutVM.GiftNote;
            //order.ShipAsGift = checkoutVM.ShipAsGift;
        }

        private void SetOrderBillingAddress(Order order, CheckoutVM checkoutVM)
        {
            if (order == null || checkoutVM == null || checkoutVM.SelectedBillingAddress == null) return;

            order.BillingAdressID = checkoutVM.SelectedBillingAddress.Id;
            order.BillingAdressPhone2 = checkoutVM.SelectedBillingAddress.Phone;
            order.BillingAdressCountryID = checkoutVM.SelectedBillingAddress.CountryId;
            order.BillingAdressCityID = checkoutVM.SelectedBillingAddress.CountryId;
            order.BillingAdressTownID = checkoutVM.SelectedBillingAddress.CountryId;
            order.BillingAdressQuarterID = checkoutVM.SelectedBillingAddress.CountryId;
            order.BillingAdressDetail = checkoutVM.SelectedBillingAddress.Detail;

        }

        private void AddOrderedItemsFromCart(Order order, CheckoutVM checkoutVM)
        {
            if (order == null || checkoutVM.Cart == null || checkoutVM.Cart.Products == null || checkoutVM.Cart.Products.Count() <= 0) return;

            foreach (var item in checkoutVM.Cart.Products)
            {
                for (int i = 0; i < item.Quantity; i++)
                {
                    OrderedItem oi = new OrderedItem();

                    oi.Order = order;
                    oi.ProductID = item.ProductID;
                    oi.StockID = item.StockID;
                    oi.Quantity = 1; //item.Quantity;
                    oi.Amount = (decimal)item.Amount;
                    oi.AmountPsf = item.AmountOld;
                    oi.Barkod = item.Barkod;
                    oi.BasketChargeType = BasketChargeType.UrunAlis;
                    oi.ImagePath = item.ImagePath;
                    oi.Name = item.ProductName;
                    oi.SizeName = item.SizeName;
                    oi.ColorName = item.ColorName;
                    oi.ReturnStatus = IadeStatus.UrunSipariste;
                    oi.PStatus = Status.Active;
                    oi.Url = item.Url;
                    //TODO:implement on cartservice
                    oi.VatRatio = item.VatRatio;

                    order.OrderedItems.Add(oi);

                }
            }

        }

        private void AddMinusPaymentHistory(Order order, CheckoutVM checkoutVM)
        {
            order.PaymentHistories.Add(new PaymentHistory
            {
                Amount = order.TotalAmount * -1,
                IP = checkoutVM.IP,
                PaymentComplateStatus = PaymentComplateStaus.Tamamlanmadi,
                PaymentType = order.PaymentType,
                PaymentLogType = PaymentLogType.SiparisEksiKaydi,
                PriceCurrencyID = checkoutVM.CurrencyId,
                WebPosInstallmentID = order.WebPosInstallmentID,
                WebPosID = order.WebPosID,
                TrackCode = order.TrackCode,
                PStatus = Status.Active
            });
        }

        //private void SetUtmParams(Order order)
        //{
        //    if (order == null) return;

        //    //Affilate Params & Stat Params
        //    order.utm_campaign = GetUtmParameter("utm_campaign");
        //    order.utm_source = GetUtmParameter("utm_source");
        //    order.utm_medium = GetUtmParameter("utm_medium");

        //}

        public OrderDetailsVM SetSuccessOrder(OrderDetailsVM orderVM, bool isLocal = false)
        {
            if (orderVM == null) return null;

            try
            {
                Order ord = db.Orders
                    .Include(c => c.Coupon)
                    .Include(c => c.OrderedItems)
                    .Where(c => c.ID == orderVM.ID && c.StoreAccountID == orderVM.StoreAccountId).FirstOrDefault();

                ord.PStatus = orderVM.PStatus = Status.Active;
                ord.PaymentStatus = orderVM.PaymentStatus = (orderVM.PaymentType != PaymentType.Havale) ? SiparisOdemeDurumu.OdemeOnaylandi : SiparisOdemeDurumu.OdemeBekliyor;

                if (ord.Coupon != null && ord.Coupon.AllowMultipleUse != true)
                {
                    ord.Coupon.IsUsed = false;
                }

                foreach (var item in orderVM.OrderedItems)
                {
                    var stock = db.Stocks.Where(c => c.ID == item.StockID && c.StoreAccountID == orderVM.StoreAccountId).FirstOrDefault();
                    stock.Quantity -= item.Quantity;
                }

                db.SaveChanges();

                //Remove Basket Items
                if (!isLocal)
                {
                    cartService.RemoveAll(orderVM.StoreAccountId, orderVM.UserID, orderVM.Session, orderVM.CurrencyId ?? 0);
                }

            }
            catch (Exception)
            {


            }

            return orderVM;
        }
        private void ValidateStateModel(CheckoutVM checkoutVM, OrderCreationResult<OrderDetailsVM> result)
        {
            result.SetSuccessAndClearError();

            if (checkoutVM == null) { result.SetAsBadRequest("required items can not be found for order process, please start from scartch"); }
            if (checkoutVM.UserId == null && String.IsNullOrWhiteSpace(checkoutVM.SpecialKey)) { result.SetAsBadRequest("please try to login or continue as guest to complete order"); }
            if (checkoutVM.Cart == null || checkoutVM.Cart.Products == null || checkoutVM.Cart.Products.Count() == 0) { result.SetAsBadRequest("cart items can not be found for order process, please start from scartch"); }
            if (checkoutVM.BillingAddressId == null || checkoutVM.ShippingAddressId == null) { result.SetAsBadRequest("order addresses is missin please add an address to continue"); }
            if (checkoutVM.CargoPriceId == null || checkoutVM.CargoPriceId == null) { result.SetAsBadRequest("cargo is missing missing please select a valid cargo to continue"); }
            if (checkoutVM.Currency == null) { result.SetAsBadRequest("currency is missing missing please select a valid currency to continue"); }
            if (checkoutVM.SelectedWebPosInstallment == null) { result.SetAsBadRequest("payment is missing missing please select a valid payment to continue"); }
        }
    }
}
