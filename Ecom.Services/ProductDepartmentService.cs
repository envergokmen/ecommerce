﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.ProductDepartments;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class ProductDepartmentService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public ProductDepartmentService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<ProductDepartmentCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<ProductDepartmentCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.ProductDepartments
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.ProductDepartments
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.ID
                                          select new ProductDepartmentCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.ProductDepartments
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.ProductDepartments.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.ID).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new ProductDepartmentCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<ProductDepartmentCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<ProductDepartmentCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new ProductDepartmentCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.ProductDepartments.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new ProductDepartmentCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var ProductDepartment = db.ProductDepartments.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (ProductDepartment != null)
            {
                ProductDepartment.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ProductDepartmentDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private ProductDepartment MapToDBObject(ProductDepartmentCreateEditVM model, ProductDepartment dbModel)
        {
            var ProductDepartment = dbModel ?? new ProductDepartment();

            ProductDepartment.ID = model.ID;

            ProductDepartment.Name = model.Name;
            ProductDepartment.PStatus = model.PStatus;

            ProductDepartment.StoreAccountID = model.ID == 0 ? model.StoreID : ProductDepartment.StoreAccountID;
            ProductDepartment.ModifiedOn = DateTime.Now;
            if (ProductDepartment.ID != 0) ProductDepartment.CreatedOn = DateTime.Now;

            return ProductDepartment;

        }

        private ProductDepartmentCreateEditVM MapToVM(ProductDepartment dbModel)
        {
            if (dbModel == null) return null;

            var ProductDepartment = new ProductDepartmentCreateEditVM();
            ProductDepartment.ID = dbModel.ID;
            ProductDepartment.Name = dbModel.Name;
            ProductDepartment.PStatus = dbModel.PStatus;

            return ProductDepartment;
        }

        public OperationResult<ProductDepartmentCreateEditVM> Save(ProductDepartmentCreateEditVM ProductDepartment)
        {
            var result = new OperationResult<ProductDepartmentCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(ProductDepartment, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            ProductDepartment existing = null;

            if (ProductDepartment.ID != 0)
            {
                existing = db.ProductDepartments.Where(x => x.ID == ProductDepartment.ID).FirstOrDefault();
                existing = MapToDBObject(ProductDepartment, existing);
            }
            else
            {
                existing = MapToDBObject(ProductDepartment, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(ProductDepartment.ID == 0 ? QueueItemType.ProductDepartmentCreated : QueueItemType.ProductDepartmentUpdated, JsonConvert.SerializeObject(result.Result), ProductDepartment.StoreID);

            return result;

        }


    }
}
