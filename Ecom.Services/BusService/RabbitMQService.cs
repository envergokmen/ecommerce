﻿using System;
using System.Text;
using Ecom.Core;
using RabbitMQ.Client;

namespace Ecom.Services.Bus
{
    public class RabbitMQService
    {
        // localhost üzerinde kurulu olduğu için host adresi olarak bunu kullanıyorum.
        private readonly string _hostName = "localhost";
        private const string CLIENT_NAME = "DEFAULT_";
        public IConnection GetRabbitMQConnection()
        {
            ConnectionFactory connectionFactory = new ConnectionFactory()
            {
                // RabbitMQ'nun bağlantı kuracağı host'u tanımlıyoruz. Herhangi bir güvenlik önlemi koymak istersek, Management ekranından password adımlarını tanımlayıp factory içerisindeki "UserName" ve "Password" property'lerini set etmemiz yeterlidir.
                HostName = _hostName
            };

            return connectionFactory.CreateConnection();
        }

        public void Publish(QueueItemType queueItemType, string message, int storeid)
        {
            using (var connection = GetRabbitMQConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var queueNameStr = String.Concat(CLIENT_NAME, queueItemType.ToString());

                    channel.QueueDeclare(queueNameStr, false, false, false, null);

                    channel.BasicPublish("", queueNameStr, null, Encoding.UTF8.GetBytes(storeid + "_" + message));

                }
            }
        }
    }
}
