﻿using Ecom.Core;
using Ecom.Models;
using Ecom.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;

namespace Ecom.Services
{

    public class UserService
    {
        private IMemoryCache cache;
        private const string HOME_POSTS = "HOME_POSTS";
        private readonly EcomContext db;
        private readonly ServiceUtils serviceUtils;

        public UserService(IMemoryCache _cache, EcomContext _db, ServiceUtils _serviceUtils)
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
        }

        /// <summary>
        /// Validate Input and change userpassword if input is valid
        /// </summary>
        /// <param name="_changePasswordViewModel">Known as oldpassword, newpass and confirm. new and confirm should be same</param>
        /// <returns>Service Returns OperationResult object and UserAuthViewModel as it's Result. Please check to OperationResult.Status first</returns>
        public OperationResult<UserViewModel> ChangeUserPassWord(ChangePasswordViewModel _changePasswordViewModel)
        {

            OperationResult<UserViewModel> result = new OperationResult<UserViewModel>();

            result.SetErrorAndMarkAsErrored("Error on chancing password");

            if (_changePasswordViewModel == null)
            {
                result.SetError("InputCannotBeNull");
                return result;
            }

            var validationResult = serviceUtils.TryToValidateModel(_changePasswordViewModel, true);

            if (!validationResult.Status)
            {
                result.ResultDescription = validationResult.ResultDescription;
                result.Status = false;
            }
            else
            {

                if (_changePasswordViewModel.ConfirmPassword != _changePasswordViewModel.NewPassword)
                {
                    result.SetErrorAndMarkAsErrored("Passwords Do not match");
                    return result;
                }

                try
                {
                    User user = db.Users.Find(_changePasswordViewModel.UserID);

                    if (user != null)
                    {
                        user.Password = SecureOperations.Encrypt(_changePasswordViewModel.NewPassword);
                        db.SaveChanges();

                        UserViewModel usrAuth = ConvertUserToUserViewModel(user);
                        result.SetSuccessAndClearError(usrAuth);

                        //SetSessionForUser(usrAuth);
                        //SetAuthCookieForUser(usrAuth, "");

                    }
                    else
                    {
                        result.SetError("Not Found User");
                    }
                }
                catch (Exception)
                {
                    result.SetGeneralErrorAndMarkAsErrored();
                }
            }


            return result;
        }

        /// <summary>
        /// Converts entity to user authentication viewModel for rest usage at anywhere, we do not want to use entities on high level
        /// </summary>
        /// <param name="userFromDb">entity from repo or from dbcontext</param>
        /// <returns>Isoleted viewmodel from entity, without data annotations and some virtual nav props</returns>
        public UserViewModel ConvertUserToUserViewModel(User userFromDb)
        {
            return new UserViewModel
            {
                UserName = userFromDb.UserName,
                ID = userFromDb.ID,
                Name = userFromDb.Name,
                StoreAccountID = userFromDb.StoreAccountID,
                StoreName = userFromDb.StoreAccount?.StoreName ?? "",
                DomainId = userFromDb.DomainId,
                AllowMail = userFromDb.AllowMail,
                AllowSMS = userFromDb.AllowSMS,
                BirthDate = userFromDb.BirthDate,
                Telephone = userFromDb.Telephone,
                HashedPassWord = userFromDb.Password,
                LoginType = userFromDb.StoreAccountUsers == null || userFromDb.StoreAccountUsers.Count() == 0 ? UserType.Normal : UserType.StoreManager
            };
        }


        /// <summary>
        /// Creates a new User in database From UserCreateViewModel. This method doesn't send any for confirmation or welcome mail
        /// </summary>
        /// <param name="userCreateViewModel"></param>
        /// <returns>Service Returns OperationResult object and UserViewModel as it's Result. Please check to OperationResult.Status first</returns>
        public OperationResult<UserViewModel> Create(RegisterViewModel userCreateViewModel)
        {
            OperationResult<UserViewModel> result = new OperationResult<UserViewModel>();

            result.SetErrorAndMarkAsErrored("Register Failed");

            var validationResult = serviceUtils.TryToValidateModel(userCreateViewModel, true);

            if (validationResult.Status == true)
            {
                var userExist = db.Users.FirstOrDefault(x => x.UserName == userCreateViewModel.UserName && x.StoreAccountID == userCreateViewModel.StoreId);

                if (userExist != null)
                {
                    validationResult.AddError("UserName or email already exist");
                }

                if (SecureOperations.ClearSqlInjection(userCreateViewModel.Name) != userCreateViewModel.Name)
                {
                    validationResult.AddError("UserName or email already exist");
                }

                if (validationResult.Status)
                {
                    try
                    {
                        User usr = new User();

                        usr.Name = userCreateViewModel.Name;
                        usr.UserName = userCreateViewModel.UserName;
                        usr.Password = SecureOperations.Encrypt(userCreateViewModel.PassWord);
                        usr.PStatus = Status.Active;
                        usr.DomainId = userCreateViewModel.DomainId;
                        usr.StoreAccountID = userCreateViewModel.StoreId;

                        db.Users.Add(usr);
                        db.SaveChanges();

                        usr.StoreAccount = db.StoreAccounts.Find(usr.StoreAccountID);
                        UserViewModel userAuth = ConvertUserToUserViewModel(usr);
                        result.SetSuccessAndClearError(userAuth);


                    }
                    catch (Exception)
                    {
                        result.SetGeneralErrorAndMarkAsErrored();
                    }

                }
                else
                {
                    result.SetError(validationResult.ResultDescription);
                    result.Status = false;
                }

            }
            else
            {
                result.SetError(validationResult.ResultDescription);
                result.Status = false;
            }

            return result;

        }


        /// <summary>
        /// Creates a new User in database From UserCreateViewModel. This method doesn't send any for confirmation or welcome mail
        /// </summary>
        /// <param name="userCreateViewModel"></param>
        /// <returns>Service Returns OperationResult object and UserViewModel as it's Result. Please check to OperationResult.Status first</returns>
        public OperationResult<UserViewModel> CreateStoreUser(RegisterStoreStoreVM userCreateViewModel)
        {
            OperationResult<UserViewModel> result = new OperationResult<UserViewModel>();

            result.SetErrorAndMarkAsErrored("Register Failed");

            var validationResult = serviceUtils.TryToValidateModel(userCreateViewModel, true);

            if (validationResult.Status == true)
            {
                var userExist = db.Users.Include(c => c.StoreAccountUsers).Where(x => x.UserName == userCreateViewModel.UserName && x.StoreAccountUsers.Count() > 0).FirstOrDefault();

                if (userExist != null && userExist.StoreAccountUsers.Count() > 0)
                {
                    validationResult.SetError("UserName or email already exist");
                }

                if (SecureOperations.ClearSqlInjection(userCreateViewModel.Name) != userCreateViewModel.Name)
                {
                    validationResult.SetError("UserName or email already exist");
                }

                var storeUrl = Utils.FormatUrl(userCreateViewModel.StoreName);

                var storeAlreadyExists = db.Domains.Any(x => x.Name == storeUrl);

                if (storeAlreadyExists)
                {
                    validationResult.SetError("Sorry, this store name has already been taken.");
                }

                if (validationResult.Status)
                {
                    try
                    {
                        User usr = new User();

                        usr.Name = userCreateViewModel.Name;
                        usr.UserName = userCreateViewModel.UserName;
                        usr.Password = SecureOperations.Encrypt(userCreateViewModel.PassWord);
                        usr.PStatus = Status.Active;
                        //usr.DomainId = userCreateViewModel.DomainId;
                        usr.UserType = UserType.StoreManager;

                        //usr.StoreAccountID = userCreateViewModel.StoreId;

                        StoreAccount storeAccount = new StoreAccount();

                        storeAccount.StoreName = userCreateViewModel.StoreName;

                        Domain domain = new Domain();
                        domain.Name = Utils.FormatUrl(userCreateViewModel.StoreName);
                        domain.PriceCurrencyID = db.Countries.FirstOrDefault(x => x.ID == userCreateViewModel.CountryId && x.PStatus == Status.Active).DefaultPriceCurrencyID;
                        domain.LangID = db.Countries.FirstOrDefault(x => x.ID == userCreateViewModel.CountryId && x.PStatus == Status.Active).DefaultLangID ?? db.Langs.FirstOrDefault(x => x.PStatus == Status.Active && x.LangCode == "en").ID;
                        domain.DomainBrandName = userCreateViewModel.StoreName;
                        domain.StoreAccount = storeAccount;
                        usr.Domain = domain;

                        //storeAccount.StoreUrl = Utils.FormatUrl(userCreateViewModel.StoreName);

                        storeAccount.CompanyName = userCreateViewModel.CompanyName ?? userCreateViewModel.StoreName;
                        storeAccount.CompanyAddress = userCreateViewModel.CompanyAddress;
                        storeAccount.CountryId = userCreateViewModel.CountryId;
                        storeAccount.CityId = userCreateViewModel.CityId;
                        storeAccount.TownId = userCreateViewModel.TownId;
                        storeAccount.CompanyPhone = userCreateViewModel.CompanyPhone;
                        storeAccount.CompanyTaxInfo = userCreateViewModel.CompanyTaxInfo;
                        storeAccount.CompanyEmail = userCreateViewModel.CompanyEmail;

                        usr.StoreAccount = storeAccount;
                        StoreAccountUser accountUser = new StoreAccountUser();
                        accountUser.User = usr;
                        accountUser.StoreAccount = storeAccount;


                        db.Users.Add(usr);
                        db.StoreAccounts.Add(storeAccount);
                        db.StoreAccountUsers.Add(accountUser);
                        db.Domains.Add(domain);

                        db.SaveChanges();

                        //UPDATE Related User Id
                        storeAccount.RelatedUserId = usr.ID;
                        db.SaveChanges();

                        UserViewModel userAuth = ConvertUserToUserViewModel(usr);
                        result.SetSuccessAndClearError(userAuth);


                    }
                    catch (Exception)
                    {
                        result.SetGeneralErrorAndMarkAsErrored();
                    }

                }
                else
                {
                    result.SetError(validationResult.ResultDescription);
                    result.Status = false;
                }

            }
            else
            {
                result.SetError(validationResult.ResultDescription);
                result.Status = false;
            }

            return result;

        }




        /// <summary>
        /// Delete entity which has given id
        /// </summary>
        /// <param name="id">id of entity</param>
        /// <returns>Check OperationResult.Status and messages</returns>
        public StandartOperationResult Delete(int id)
        {
            StandartOperationResult deleteResult = new StandartOperationResult();

            try
            {
                User userToDelete = db.Users.Find(id);
                deleteResult = Delete(userToDelete);


            }
            catch (Exception)
            {
                deleteResult.SetGeneralErrorAndMarkAsErrored();
            }

            return deleteResult;
        }

        /// <summary>
        /// Delete entity which has given
        /// </summary>
        /// <param name="user">reference of entity</param>
        /// <returns>Check OperationResult.Status and messages</returns>
        public StandartOperationResult Delete(User user)
        {
            StandartOperationResult deleteResult = new StandartOperationResult();

            try
            {

                if (user != null)
                {

                    user.PStatus = Status.Deleted;
                    db.SaveChanges();
                    deleteResult.SetSuccessAndClearError("");
                }
                else
                {
                    deleteResult.SetGeneralErrorAndMarkAsErrored();
                }

            }
            catch (Exception)
            {
                deleteResult.SetGeneralErrorAndMarkAsErrored();
            }

            return deleteResult;

        }

        /// <summary>
        /// Generate string password reset token for given UserID
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="_exprirationDate"></param>
        /// <returns>password reset token as string</returns>
        public OperationResult<string> GeneratePassWordResetToken(int userID, DateTime _exprirationDate)
        {
            OperationResult<string> result = new OperationResult<string>();

            try
            {
                User user = db.Users.Find(userID);

                if (user != null)
                {

                    user.PasswordResetToken = Guid.NewGuid().ToString();
                    user.PasswordResetTokenExpDate = DateTime.Now.AddDays(3);
                    db.SaveChanges();

                    result.SetSuccessAndClearError(user.PasswordResetToken);

                }
                else
                {
                    result.SetErrorAndMarkAsErrored("NotFoundUserError");
                }
            }
            catch (Exception)
            {
                result.SetGeneralErrorAndMarkAsErrored();
            }

            return result;

        }

        /// <summary>
        /// Gets only one user which matches the given id
        /// </summary>
        /// <param name="id">id of user which will be get</param>
        /// <returns>Isoleted viewmodel from entity, without data annotations and some virtual nav props</returns>
        public OperationResult<UserViewModel> GetById(long id)
        {
            OperationResult<UserViewModel> result = new OperationResult<UserViewModel>();

            try
            {
                User user = db.Users.Include(x => x.StoreAccount).FirstOrDefault(x => x.ID == id);

                if (user != null)
                {
                    result.SetSuccessAndClearError(ConvertUserToUserViewModel(user));

                }
                else
                {
                    result.SetErrorAndMarkAsErrored();
                }
            }
            catch (Exception)
            {
                result.SetGeneralErrorAndMarkAsErrored();
            }

            return result;
        }

        /// <summary>
        /// Reset password from confirmation token which already sent by forgotten password form
        /// </summary>
        /// <param name="resetPassWord">contains token oldpassword and new password</param>
        /// <returns>if it succeed, returns user's authentication information</returns>
        public OperationResult<UserViewModel> ResetUserPassword(ResetPassworViewdModel resetPassWord)
        {
            OperationResult<UserViewModel> result = new OperationResult<UserViewModel>();

            User user = null;
            DateTime today = DateTime.Now;

            result.SetErrorAndMarkAsErrored("PasswordChangeFailed");
            var validationResult = serviceUtils.TryToValidateModel(resetPassWord, true);

            if (resetPassWord.ConfirmPassword != resetPassWord.NewPassword)
            {
                result.SetErrorAndMarkAsErrored("Passwords Do Not Match Error");
                return result;
            }

            try
            {
                user = db.Users.Include(x => x.StoreAccount).Where(p =>
                          p.PStatus == Status.Active
                          && p.PasswordResetToken == resetPassWord.PasswordResetToken
                          && p.PasswordResetTokenExpDate > today).FirstOrDefault();

                if (user != null)
                {

                    user.Password = SecureOperations.Encrypt(resetPassWord.NewPassword);
                    db.SaveChanges();

                    UserViewModel usrAuth = ConvertUserToUserViewModel(user);
                    result.SetSuccessAndClearError(usrAuth);

                }
                else
                {
                    result.SetErrorAndMarkAsErrored("NotFoundUserError");
                }
            }
            catch (Exception)
            {
                result.SetGeneralErrorAndMarkAsErrored();
            }


            return result;

        }

        /// <summary>
        /// Update user's name only
        /// </summary>
        /// <param name="userUpdateViewModel">update only name from model</param>
        /// <returns>Isoleted viewmodel from entity, without data annotations and some virtual nav props</returns>
        public OperationResult<UserViewModel> Update(UpdateUserViewModel userUpdateViewModel)
        {

            OperationResult<UserViewModel> result = new OperationResult<UserViewModel>();

            result.SetErrorAndMarkAsErrored("Update failed");

            var validationResult = serviceUtils.TryToValidateModel(userUpdateViewModel, true);

            if (userUpdateViewModel != null && userUpdateViewModel.ID <= 0)
            {
                validationResult.AddError("NotFoundUserError");
            }

            if (validationResult.Status == true)
            {
                try
                {

                    User usr = db.Users.Include(c => c.StoreAccount).FirstOrDefault(x => x.ID == userUpdateViewModel.ID);

                    if (usr == null)
                    {
                        validationResult.AddError("NotFoundUserError");
                    }

                    if (SecureOperations.ClearSqlInjection(userUpdateViewModel.Name) != userUpdateViewModel.Name)
                    {
                        validationResult.AddError("UserNameOrEmailExist");
                    }

                    if (validationResult.Status)
                    {

                        usr.Name = userUpdateViewModel.Name;
                        usr.AllowSMS = userUpdateViewModel.AllowSMS;
                        usr.AllowMail = userUpdateViewModel.AllowMail;
                        usr.Telephone = userUpdateViewModel.Phone;
                        usr.BirthDate = userUpdateViewModel.BirthDate;

                        usr.PStatus = Status.Active;
                        db.SaveChanges();

                        UserViewModel userToReturn = ConvertUserToUserViewModel(usr);
                        result.SetSuccessAndClearError(userToReturn);




                    }

                }
                catch (Exception)
                {
                    result.SetGeneralErrorAndMarkAsErrored();

                }
            }
            else
            {
                result.SetError(validationResult.ResultDescription);
                result.Status = false;
            }

            return result;
        }

        /// <summary>
        /// Validate user from db with given username and password, WARNING : this method will not remember from cookie it's validate only, not cached
        /// </summary>
        /// <param name="loginViewModel">Username, Password is important for validation. RememberMe must be handle on Web Layer</param>
        /// <returns>Isoleted viewmodel list from entity, without data annotations and some virtual nav props</returns>
        public OperationResult<UserViewModel> ValidateUser(LoginViewModel loginViewModel)
        {
            OperationResult<UserViewModel> result = new OperationResult<UserViewModel>();

            result.SetErrorAndMarkAsErrored("LoginFailedError");

            try
            {

                var validationResult = serviceUtils.TryToValidateModel(loginViewModel, true);

                if (String.IsNullOrWhiteSpace(loginViewModel.HashedPassWord) && String.IsNullOrWhiteSpace(loginViewModel.PassWord))
                {
                    validationResult.AddError("Password is required");
                }

                if (validationResult.Status == true)
                {

                    UserViewModel usrAuth = null;

                    string hashedPass = loginViewModel.HashedPassWord ?? SecureOperations.Encrypt(loginViewModel.PassWord);
                    User user = null;

                    if (loginViewModel.LoginType == UserType.Normal)
                    {

                        user = db.Users.Where(p => p.PStatus == Status.Active
                                    && p.UserName == loginViewModel.UserName
                                    && p.StoreAccountID == loginViewModel.StoreID
                                    && p.Password == hashedPass
                                    ).FirstOrDefault();
                    }
                    else
                    {

                        var storeUser = db.Users
                            .Include(c => c.StoreAccountUsers)
                            .Include(b => b.StoreAccount)
                            .Where(p => 
                                   // p.PStatus == Status.Active
                                   //&& p.StoreAccountUsers.Count() > 0
                                   //&& p.StoreAccountID != null
                                   //&& 
                                   p.UserName == loginViewModel.UserName
                                   //&& p.Password == hashedPass
                                   ).FirstOrDefault();

                        if (storeUser != null && storeUser.StoreAccountID.HasValue && storeUser.StoreAccountID > 0 && storeUser.StoreAccountUsers != null && storeUser.StoreAccountUsers.Count() > 0)
                        {
                            user = storeUser;
                        }
                    }

                    if (user != null)
                    {
                        usrAuth = ConvertUserToUserViewModel(user);
                        result.SetSuccessAndClearError(usrAuth);

                    }
                    else
                    {
                        result.SetError("Login Failed, please try again make sure password and username are correct", ReturnStatusCode.NotFound);
                    }

                }
                else
                {
                    result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                    result.Status = false;
                }
            }
            catch (Exception)
            {
                result.ReturnCode = ReturnStatusCode.Conflict;
                result.SetGeneralErrorAndMarkAsErrored();
            }

            return result;

        }


    }
}
