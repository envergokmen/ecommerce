﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Banks;
using Ecom.ViewModels.Payments;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class PaymentService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public PaymentService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }

        public PaymentHistoryViewModel ConvertPaymentHistoryToPhViewModel(PaymentHistory phReturn)
        {
            PaymentHistoryViewModel phvm = new PaymentHistoryViewModel();

            phvm.IP = phReturn.IP;
            phvm.UserArgent = phReturn.UserArgent;
            phvm.WebPosInstallmentID = phReturn.WebPosInstallmentID;
            phvm.WebPosID = phReturn.WebPosID;
            phvm.CardCvc = null;
            phvm.PaymentDate = phReturn.PaymentDate;
            phvm.CardNumber = Utils.MaskCreditCard(phReturn.CardNumber);
            phvm.CardOwner = phReturn.CardOwner;

            phvm.Description = phReturn.Description;
            phvm.PaymentType = phReturn.PaymentType;
            phvm.TrackCode = phReturn.TrackCode;

            phvm.ErrMsg = phReturn.ErrMsg;
            phvm.ExtraHostMsg = phReturn.ExtraHostMsg;
            phvm.Retval = phReturn.Retval;
            phvm.PayCode = phReturn.PayCode;
            phvm.StoreAccountId = phReturn.StoreAccountID ?? 0;

            phvm.PayRefNo = phReturn.PayRefNo;

            phvm.PriceCurrencyID = phReturn.PriceCurrencyID;
            phvm.Retval = phReturn.Retval;
            phvm.WebPosInstallmentID = phReturn.WebPosInstallmentID;
            phvm.WebPosID = phReturn.WebPosID;
            phvm.UserID = phReturn.UserID;
            phvm.IP = phReturn.IP;
            phvm.UserArgent = phReturn.UserArgent;

            phvm.PaymentComplateStatus = PaymentComplateStaus.Tamamlanmadi;
            phvm.Amount = phReturn.Amount ?? 0;
            phvm.OrderID = phReturn.OrderID;
            phvm.PaymentLogType = PaymentLogType.SiparisEksiKaydi;


            return phvm;
        }


        public PaymentHistory ConvertToPaymentHistory(PaymentHistoryViewModel phReturn, PaymentComplateStaus? complsateStatus = null, PaymentLogType? paymentLogType = null)
        {
            PaymentHistory phvm = new PaymentHistory();
            if (phReturn == null) return phvm;
            phvm.IP = phReturn.IP;
            phvm.UserArgent = phReturn.UserArgent;
            phvm.CardCvc = null;

            phvm.StoreAccountID = phReturn.StoreAccountId;
            phvm.PaymentDate = phReturn.PaymentDate;
            phvm.CardNumber = Utils.MaskCreditCard(phReturn.CardNumber);
            phvm.CardOwner = phReturn.CardOwner;
            phvm.Description = phReturn.Description;
            phvm.PaymentType = phReturn.PaymentType;
            phvm.TrackCode = phReturn.TrackCode;
            phvm.ErrMsg = phReturn.ErrMsg;
            phvm.ExtraHostMsg = phReturn.ExtraHostMsg;
            phvm.Retval = phReturn.Retval;
            phvm.PayCode = phReturn.PayCode;
            phvm.PayRefNo = phReturn.PayRefNo;
            phvm.PriceCurrencyID = (phReturn.PriceCurrencyID != null && phReturn.PriceCurrencyID > 0) ? phReturn.PriceCurrencyID : null as int?;
            phvm.WebPosInstallmentID = (phReturn.WebPosInstallmentID != null && phReturn.WebPosInstallmentID > 0) ? phReturn.WebPosInstallmentID : null as int?;
            phvm.WebPosID = (phReturn.WebPosID != null && phReturn.WebPosID > 0) ? phReturn.WebPosID : null as int?;
            phvm.UserID = (phReturn.UserID != null && phReturn.UserID > 0) ? phReturn.UserID : null as int?;
            phvm.OrderID = (phReturn.OrderID != null && phReturn.OrderID > 0) ? phReturn.OrderID : null as int?;

            phvm.PaymentComplateStatus = (complsateStatus != null) ? (PaymentComplateStaus)complsateStatus : phReturn.PaymentComplateStatus;
            phvm.PaymentLogType = (paymentLogType != null) ? (PaymentLogType)paymentLogType : phReturn.PaymentLogType;

            phvm.Amount = phReturn.Amount;

            return phvm;
        }


        public PagedOperationListResult<PaymentCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<PaymentCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Payments
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Payments
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new PaymentCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              SortNo = p.SortNo,
                                              PStatus = p.PStatus,
                                              LogoPath = p.LogoPath,
                                              PaymentTypeStr = p.PaymentType.ToString(),
                                              WebPosCount = p.WebPoses.Where(c => c.PStatus == Status.Active).Count()

                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Payments
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.Payments.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new PaymentCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      SortNo = p.SortNo,
                      PStatus = p.PStatus,
                      PaymentType = p.PaymentType,
                      LogoPath = p.LogoPath,
                      PaymentTypeStr = p.PaymentType.ToString(),
                      WebPosCount = p.WebPoses.Where(c => c.PStatus == Status.Active).Count()
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<PaymentCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<PaymentCreateEditVM>();

            var allPaymentTypes = serviceUtils.ToSelectListInt<PaymentType>(-1);
            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new PaymentCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllPaymentTypes = allPaymentTypes;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Payments.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new PaymentCreateEditVM
             {
                 ID = p.ID,
                 SortNo = p.SortNo,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 PaymentType = p.PaymentType,
                 LogoPath = p.LogoPath,
                 PaymentKeyForOtherTypes = p.PaymentKeyForOtherTypes,

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllPaymentTypes = allPaymentTypes;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Payment = db.Payments.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Payment != null)
            {
                Payment.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.PaymentDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Payment MapToDBObject(PaymentCreateEditVM model, Payment dbModel)
        {
            var Payment = dbModel ?? new Payment();

            Payment.ID = model.ID;

            Payment.SortNo = model.SortNo;
            Payment.Name = model.Name;
            Payment.PStatus = model.PStatus;
            Payment.PaymentType = model.PaymentType;
            Payment.PaymentKeyForOtherTypes = model.PaymentKeyForOtherTypes;

            Payment.LogoPath = !String.IsNullOrWhiteSpace(model.LogoPath) ? model.LogoPath : dbModel?.LogoPath ?? "";

            Payment.StoreAccountID = model.ID == 0 ? model.StoreID : Payment.StoreAccountID;
            Payment.ModifiedOn = DateTime.Now;
            if (Payment.ID != 0) Payment.CreatedOn = DateTime.Now;

            return Payment;

        }

        private PaymentCreateEditVM MapToVM(Payment dbModel)
        {
            if (dbModel == null) return null;

            var Payment = new PaymentCreateEditVM();
            Payment.ID = dbModel.ID;
            Payment.Name = dbModel.Name;
            Payment.PStatus = dbModel.PStatus;
            Payment.SortNo = dbModel.SortNo;
            Payment.LogoPath = dbModel.LogoPath;
            Payment.PaymentType = dbModel.PaymentType;
            Payment.PaymentKeyForOtherTypes = dbModel.PaymentKeyForOtherTypes;

            return Payment;
        }

        public OperationResult<PaymentCreateEditVM> Save(PaymentCreateEditVM Payment)
        {
            var result = new OperationResult<PaymentCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Payment, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Payment existing = null;

            if (Payment.ID != 0)
            {
                existing = db.Payments.Where(x => x.ID == Payment.ID).FirstOrDefault();
                existing = MapToDBObject(Payment, existing);
            }
            else
            {
                existing = MapToDBObject(Payment, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Payment.ID == 0 ? QueueItemType.PaymentCreated : QueueItemType.PaymentUpdated, JsonConvert.SerializeObject(result.Result),Payment.StoreID);

            return result;

        }

        public List<BankVM> GetBanks(int storeId, int domainId = 0, bool includePassives = false)
        {
            string cacheKey = String.Concat("GetBanks-", storeId, "_domain_" + domainId + "_passives_" + includePassives);

            var installments = cache.Get<List<BankVM>>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (installments == null || true)
            {
                installments = (from p in db.Banks
                                where p.StoreAccountID == storeId && p.StoreAccountID == storeId &&
                                (includePassives || p.PStatus == Status.Active)
                                orderby p.SortNo
                                select new BankVM
                                {
                                    //Installments
                                    ID = p.ID,
                                    PaymentID = p.PaymentID,
                                    AccountNo = p.AccountNo,
                                    BankCode = p.BankCode,
                                    LogoPath = p.LogoPath,
                                    CityName = p.CityName,
                                    CorporateName = p.CorporateName,
                                    DepartmentAndNo = p.DepartmentAndNo,
                                    IBAN = p.IBAN,
                                    Name = p.Name,
                                    SortCode = p.SortCode,
                                    SortNo = p.SortNo
                                }
                                ).ToList();

                if (installments != null)
                {
                    cache.Set(cacheKey, installments, DateTime.Now.AddMinutes(60));
                }
            }

            if (installments == null) installments = new List<BankVM>();
            return installments;
        }

        public BankVM GetBank(int storeId, int bankId, int domainId = 0, bool includePassives = false)
        {
            if (bankId == 0) return null;
            return GetBanks(storeId, domainId, includePassives).Where(c => c.ID == bankId).FirstOrDefault();
        }

        //TODO:handle domain id
        public List<InstallmentGrpVM> GetAllInstallmentsAsMerged(int storeId, int domainId = 0, bool includePassives = false)
        {
            string cacheKey = String.Concat("AllInstallments-", storeId, "_domain_" + domainId + "_passives_" + includePassives);

            var installments = cache.Get<List<InstallmentGrpVM>>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (installments == null || true)
            {
                installments = (from i in db.WebPosInstallments
                                join wp in db.WebPoses on i.WebPosID equals wp.ID
                                join p in db.Payments on wp.PaymentID equals p.ID
                                where i.StoreAccountID == storeId && p.StoreAccountID == storeId && wp.StoreAccountID == storeId
                                && wp.PaymentID != null &&
                                (includePassives || (i.PStatus == Status.Active && p.PStatus == Status.Active && wp.PStatus == Status.Active))
                                orderby i.SortNo
                                select new InstallmentGrpVM
                                {
                                    //Installments
                                    ID = i.ID,
                                    Title = i.Title,
                                    WebPosID = i.WebPosID,
                                    DelayedMouth = i.DelayedMouth,
                                    DownLimit = i.DownLimit,
                                    UpLimit = i.UpLimit,
                                    ExtraAmount = i.ExtraAmount,
                                    IsDefault = i.IsDefault,
                                    PlusInstallmentLimit = i.PlusIntallmentAmount,
                                    PlusIntallmentAmount = i.PlusIntallmentAmount,
                                    Quantity = i.Quantity,
                                    Rate = i.Rate,
                                    SortNo = i.SortNo,

                                    //webposses
                                    ApiPassword = wp.ApiPassword,
                                    ApiUser = wp.ApiUser,
                                    BankCode = wp.BankCode,
                                    EntegHesapKodu = wp.EntegHesapKodu,
                                    EntegHesapAdi = wp.EntegHesapAdi,
                                    EntegRefKodu = wp.EntegRefKodu,
                                    MagazaNo = wp.MagazaNo,
                                    NormalPostUrl = wp.NormalPostUrl,
                                    PostUrl = wp.PostUrl,
                                    PosPaymentType = wp.PaymentType,
                                    PosIsDefault = wp.IsDefault,
                                    PosLogoPath = wp.LogoPath,
                                    PosName = wp.Name,
                                    PosSortNo = wp.SortNo,
                                    PostNetNo = wp.PostNetNo,
                                    ProvType = wp.ProvType,
                                    SecureKey3D = wp.SecureKey3D,
                                    TerminalID = wp.TerminalID,
                                    MountDelayCount = wp.MountDelayCount,

                                    //Paymenbt
                                    PaymentLogoPath = p.LogoPath,
                                    PaymentName = p.Name,
                                    PaymentSortNo = p.SortNo,
                                    PaymentType = p.PaymentType,
                                    PaymentKeyForOtherTypes = p.PaymentKeyForOtherTypes,
                                    PaymentID = p.ID
                                }

                                ).ToList();

                if (installments != null)
                {
                    cache.Set(cacheKey, installments, DateTime.Now.AddMinutes(60));
                }
            }

            if (installments == null) installments = new List<InstallmentGrpVM>();
            return installments;
        }

        public InstallmentGrpVM GetWebPosInstallmentByID(int storeId, int webPosInstallmentID, int domainId = 0, bool includePassives = false)
        {
            if (webPosInstallmentID == 0) return null;
            return GetAllInstallmentsAsMerged(storeId, domainId, includePassives).Where(c => c.ID == webPosInstallmentID).FirstOrDefault();
        }

        //TODO:handle domain id
        public List<PaymentVM> GetAllActivePayments(int storeId, int domainId = 0)
        {
            string cacheKey = String.Concat("AllActivePayments-", storeId, "_domain_" + domainId);

            List<PaymentVM> payments = cache.Get<List<PaymentVM>>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (payments == null || true)
            {
                var allInstallments = GetAllInstallmentsAsMerged(storeId, domainId);
                var allBanks = GetBanks(storeId, domainId);

                if (allInstallments != null && allInstallments.Any())
                {

                    var installments =
                        (from i in allInstallments
                         group i by new
                         {
                             i.ID,
                             i.Title,
                             i.WebPosID,
                             i.DelayedMouth,
                             i.DownLimit,
                             i.UpLimit,
                             i.ExtraAmount,
                             i.IsDefault,
                             PlusInstallmentLimit = i.PlusIntallmentAmount,
                             i.PlusIntallmentAmount,
                             i.Quantity,
                             i.Rate,
                             i.SortNo,
                             i.PaymentID


                         } into gcs
                         select new InstallmentVM
                         {
                             ID = gcs.Key.ID,
                             Title = gcs.Key.Title,
                             WebPosID = gcs.Key.WebPosID,
                             DelayedMouth = gcs.Key.DelayedMouth,
                             DownLimit = gcs.Key.DownLimit,
                             UpLimit = gcs.Key.UpLimit,
                             ExtraAmount = gcs.Key.ExtraAmount,
                             IsDefault = gcs.Key.IsDefault,
                             PlusInstallmentLimit = gcs.Key.PlusIntallmentAmount,
                             PlusIntallmentAmount = gcs.Key.PlusIntallmentAmount,
                             Quantity = gcs.Key.Quantity,
                             Rate = gcs.Key.Rate,
                             SortNo = gcs.Key.SortNo,

                         }).OrderBy(c => c.SortNo).ToList();


                    var poses =
                        (from wp in allInstallments
                         group wp by new
                         {
                             wp.ApiPassword,
                             wp.ApiUser,
                             wp.BankCode,
                             wp.EntegHesapKodu,
                             wp.EntegHesapAdi,
                             wp.EntegRefKodu,
                             wp.MagazaNo,
                             wp.NormalPostUrl,
                             wp.PostUrl,
                             wp.PosPaymentType,
                             wp.PosIsDefault,
                             wp.PosLogoPath,
                             wp.PosName,
                             wp.PosSortNo,
                             wp.PostNetNo,
                             wp.ProvType,
                             wp.SecureKey3D,
                             wp.TerminalID,
                             wp.MountDelayCount,
                             wp.WebPosID,
                             wp.PaymentID

                         } into gcs
                         select new WebPosVM
                         {
                             ApiPassword = gcs.Key.ApiPassword,
                             PaymentID = gcs.Key.PaymentID,
                             ApiUser = gcs.Key.ApiUser,
                             BankCode = gcs.Key.BankCode,
                             EntegHesapKodu = gcs.Key.EntegHesapKodu,
                             EntegHesapAdi = gcs.Key.EntegHesapAdi,
                             EntegRefKodu = gcs.Key.EntegRefKodu,
                             MagazaNo = gcs.Key.MagazaNo,
                             NormalPostUrl = gcs.Key.NormalPostUrl,
                             PostUrl = gcs.Key.PostUrl,
                             PosPaymentType = gcs.Key.PosPaymentType,
                             PosIsDefault = gcs.Key.PosIsDefault,
                             PosLogoPath = gcs.Key.PosLogoPath,
                             PosName = gcs.Key.PosName,
                             PosSortNo = gcs.Key.PosSortNo,
                             PostNetNo = gcs.Key.PostNetNo,
                             ProvType = gcs.Key.ProvType,
                             SecureKey3D = gcs.Key.SecureKey3D,
                             TerminalID = gcs.Key.TerminalID,
                             MountDelayCount = gcs.Key.MountDelayCount,
                             WebPosID = gcs.Key.WebPosID,
                             Installments = installments.Where(c => c.WebPosID == gcs.Key.WebPosID).OrderBy(c => c.SortNo).ToList()

                         }).OrderBy(c => c.PosSortNo).ToList();


                    payments =
                       (from p in allInstallments
                        group p by new
                        {
                            PaymentId = p.PaymentID,
                            p.PaymentSortNo,
                            p.PaymentName,
                            p.PaymentType,
                            p.PaymentKeyForOtherTypes,
                            p.PaymentLogoPath,

                        } into gcs
                        select new PaymentVM
                        {
                            PaymentId = gcs.Key.PaymentId,
                            PaymentSortNo = gcs.Key.PaymentSortNo,
                            PaymentName = gcs.Key.PaymentName,
                            PaymentType = gcs.Key.PaymentType,
                            PaymentKeyForOtherTypes = gcs.Key.PaymentKeyForOtherTypes,
                            PaymentLogoPath = gcs.Key.PaymentLogoPath,
                            WebPoseses = poses.Where(c => c.PaymentID == gcs.Key.PaymentId).OrderBy(c => c.PosSortNo).ToList(),
                            Banks = allBanks.Where(c => c.PaymentID == gcs.Key.PaymentId).OrderBy(c => c.SortNo).ToList(),

                        }).OrderBy(c => c.PaymentSortNo).ToList();


                }
            }

            if (payments == null) payments = new List<PaymentVM>();

            return payments;
        }


    }
}
