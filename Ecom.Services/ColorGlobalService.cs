﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.ColorGlobals;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class ColorGlobalService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public ColorGlobalService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<ColorGlobalCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<ColorGlobalCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.ColorGlobals
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.ColorGlobals
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new ColorGlobalCreateEditVM
                                          {
                                              ID = p.ID,
                                              HexValue = p.HexValue,
                                              Name = p.Name,
                                              PStatus = p.PStatus
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.ColorGlobals
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.ColorGlobals.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new ColorGlobalCreateEditVM
                  {
                      ID = p.ID,
                      HexValue = p.HexValue,
                      Name = p.Name,
                      PStatus = p.PStatus
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<ColorGlobalCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<ColorGlobalCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new ColorGlobalCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.ColorGlobals.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new ColorGlobalCreateEditVM
             {
                 ID = p.ID,
                 HexValue = p.HexValue,
                 SortNo = p.SortNo ?? 0,
                 Name = p.Name,
                 PStatus = p.PStatus,
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var ColorGlobal = db.ColorGlobals.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (ColorGlobal != null)
            {
                ColorGlobal.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ColorGlobalDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private ColorGlobal MapToDBObject(ColorGlobalCreateEditVM model, ColorGlobal dbModel)
        {
            var ColorGlobal = dbModel ?? new ColorGlobal();

            ColorGlobal.ID = model.ID;

            ColorGlobal.Name = model.Name;
            ColorGlobal.PStatus = model.PStatus;
            ColorGlobal.SortNo = model.SortNo;
            ColorGlobal.HexValue = model.HexValue;
            ColorGlobal.SortNo = model.SortNo;
            ColorGlobal.SortNo = model.SortNo;

            ColorGlobal.StoreAccountID = model.ID == 0 ? model.StoreID : ColorGlobal.StoreAccountID;
            ColorGlobal.ModifiedOn = DateTime.Now;
            if (ColorGlobal.ID != 0) ColorGlobal.CreatedOn = DateTime.Now;

            return ColorGlobal;

        }

        private ColorGlobalCreateEditVM MapToVM(ColorGlobal dbModel)
        {
            if (dbModel == null) return null;

            var ColorGlobal = new ColorGlobalCreateEditVM();
            ColorGlobal.ID = dbModel.ID;
            ColorGlobal.Name = dbModel.Name;
            ColorGlobal.PStatus = dbModel.PStatus;
            ColorGlobal.SortNo = dbModel.SortNo ?? 0;
            ColorGlobal.HexValue = dbModel.HexValue;

            return ColorGlobal;
        }

        public OperationResult<ColorGlobalCreateEditVM> Save(ColorGlobalCreateEditVM ColorGlobal)
        {
            var result = new OperationResult<ColorGlobalCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(ColorGlobal, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            ColorGlobal existing = null;

            if (ColorGlobal.ID != 0)
            {
                existing = db.ColorGlobals.Where(x => x.ID == ColorGlobal.ID).FirstOrDefault();
                existing = MapToDBObject(ColorGlobal, existing);
            }
            else
            {
                existing = MapToDBObject(ColorGlobal, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(ColorGlobal.ID == 0 ? QueueItemType.ColorGlobalCreated : QueueItemType.ColorGlobalUpdated, JsonConvert.SerializeObject(result.Result), ColorGlobal.StoreID);

            return result;

        }


    }
}
