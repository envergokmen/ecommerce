﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.CatalogLocalLangs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class CatalogLocalLangService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public CatalogLocalLangService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<CatalogLocalLangCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10, int? catalogId = null)
        {
            var operationResult = new PagedOperationListResult<CatalogLocalLangCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            var totalQuery = (from p in db.CatalogLocalLangs
                              where
                               p.StoreAccountID == storeId && p.PStatus == Status.Active
                              select p);

            var query = (from p in db.CatalogLocalLangs
                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                         orderby p.ID
                         select p).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize));

            if (catalogId.HasValue && catalogId > 0)
            {
                totalQuery = totalQuery.Where(x => x.CatalogID == catalogId);
                query = query.Where(x => x.CatalogID == catalogId);
            }


            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {
                totalQuery = totalQuery.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
                query = query.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
            }

            operationResult.Result = query.Select(p =>
                              new CatalogLocalLangCreateEditVM
                              {
                                  ID = p.ID,
                                  Name = p.Name,
                                  PStatus = p.PStatus,
                                  LangCode = p.Lang.LangCode,
                                  ParentName = p.Catalog.Name,
                                  Url=p.Url
                              }).ToList();


            totalItemCount = totalQuery.Count();

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<CatalogLocalLangCreateEditVM> Get(int id, int storeId, int typeid)
        {
            var result = new OperationResult<CatalogLocalLangCreateEditVM>();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allCatalogs = db.Catalogs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();


            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new CatalogLocalLangCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllCatalogs = allCatalogs;
                result.Result.CatalogID = typeid;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.CatalogLocalLangs.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new CatalogLocalLangCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 CatalogID = p.CatalogID,
                 Description = p.Description,
                 LangID = p.LangID,
                 Title = p.Title,
                 Summary = p.Summary,
                 CanonicalLink = p.CanonicalLink,
                 Link = p.Link,
                 MenuImageUrl = p.MenuImageUrl,
                 MetaDescription = p.MetaDescription,
                 MetaKeywords = p.MetaKeywords,
                 MobileImageUrl = p.MobileImageUrl,
                 Url = p.Url,
                 WebImageUrl = p.WebImageUrl,

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllCatalogs = allCatalogs;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Catalog = db.CatalogLocalLangs.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Catalog != null)
            {
                Catalog.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.CatalogDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private CatalogLocalLang MapToDBObject(CatalogLocalLangCreateEditVM model, CatalogLocalLang dbModel)
        {
            var Catalog = dbModel ?? new CatalogLocalLang();

            Catalog.ID = model.ID;

            Catalog.Name = model.Name;
            Catalog.PStatus = model.PStatus;
            Catalog.LangID = model.LangID;
            Catalog.CatalogID = model.CatalogID;
            Catalog.Description = model.Description;
            Catalog.Title = model.Title;
            Catalog.Summary = model.Summary;
            Catalog.MetaDescription = model.MetaDescription;
            Catalog.MetaKeywords = model.MetaKeywords;
            Catalog.CanonicalLink = model.CanonicalLink;
            Catalog.Link = model.Link;
            Catalog.Url = model.Url;
            Catalog.WebImageUrl = model.WebImageUrl;
            Catalog.MenuImageUrl = model.MenuImageUrl;
            Catalog.MobileImageUrl = model.MobileImageUrl;

            Catalog.StoreAccountID = model.ID == 0 ? model.StoreID : Catalog.StoreAccountID;
            Catalog.ModifiedOn = DateTime.Now;
            if (Catalog.ID != 0) Catalog.CreatedOn = DateTime.Now;

            return Catalog;

        }

        private CatalogLocalLangCreateEditVM MapToVM(CatalogLocalLang dbModel)
        {
            if (dbModel == null) return null;

            var Catalog = new CatalogLocalLangCreateEditVM();
            Catalog.ID = dbModel.ID;
            Catalog.Name = dbModel.Name;
            Catalog.Description = dbModel.Description;
            Catalog.LangID = dbModel.LangID;
            Catalog.CatalogID = dbModel.CatalogID;
            Catalog.PStatus = dbModel.PStatus;
            Catalog.Title = dbModel.Title;
            Catalog.Summary = dbModel.Summary;
            Catalog.MetaDescription = dbModel.MetaDescription;
            Catalog.MetaKeywords = dbModel.MetaKeywords;
            Catalog.CanonicalLink = dbModel.CanonicalLink;
            Catalog.Link = dbModel.Link;
            Catalog.Url = dbModel.Url;
            Catalog.WebImageUrl = dbModel.WebImageUrl;
            Catalog.MenuImageUrl = dbModel.MenuImageUrl;
            Catalog.MobileImageUrl = dbModel.MobileImageUrl;

            return Catalog;
        }

        public OperationResult<CatalogLocalLangCreateEditVM> Save(CatalogLocalLangCreateEditVM Catalog)
        {
            var result = new OperationResult<CatalogLocalLangCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Catalog, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            CatalogLocalLang existing = null;

            if (Catalog.ID != 0)
            {
                existing = db.CatalogLocalLangs.Include(c => c.Catalog).Where(x => x.ID == Catalog.ID).FirstOrDefault();
                existing = MapToDBObject(Catalog, existing);
                GenerateRecurSiveUrl(Catalog, existing);


            }
            else
            {
                existing = MapToDBObject(Catalog, null);
                GenerateRecurSiveUrl(Catalog, existing);

                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Catalog.ID == 0 ? QueueItemType.CatalogCreated : QueueItemType.CatalogUpdated, JsonConvert.SerializeObject(result.Result), Catalog.StoreID);

            return result;

        }

        private void GenerateRecurSiveUrl(CatalogLocalLangCreateEditVM Catalog, CatalogLocalLang existing)
        {
            if (String.IsNullOrWhiteSpace(Catalog.Url))
            {
                var existedCatParentID = existing.Catalog !=null ? existing.Catalog.ParentID : db.Catalogs.Where(c => c.ID == Catalog.CatalogID).Select(c => c.ParentID).FirstOrDefault();

                if (existedCatParentID != null && existedCatParentID != 0)
                {

                    Stack<string> allUrls = new Stack<string>();
                    allUrls.Push(Utils.FormatUrl(existing.Name));

                    var catalog = db.Catalogs.Include(c => c.CatalogLocalLangs).Where(c => c.ID == existedCatParentID).FirstOrDefault();

                    while (catalog != null)
                    {
                        var localCat = catalog.CatalogLocalLangs.FirstOrDefault(c => c.LangID == Catalog.LangID);

                        var url = Utils.FormatUrl(catalog.Name);

                        if (localCat != null)
                        {
                            url = (!String.IsNullOrWhiteSpace(localCat.Url) && !localCat.Url.TrimStart('/').Contains("/")) ? localCat.Url.TrimStart('/') : Utils.FormatUrl(localCat.Name);
                        }

                        allUrls.Push(url);
                        catalog = catalog.ParentID.HasValue ? db.Catalogs.Include(c => c.CatalogLocalLangs).Where(c => c.ID == catalog.ParentID.Value).FirstOrDefault() : null;
                    }

                    existing.Url = "/"+ allUrls.Aggregate((current, next) => current + "/" + next);
                }
                else
                {
                    existing.Url = "/" + Utils.FormatUrl(Catalog.Name);
                }

            }
        }

    }
}
