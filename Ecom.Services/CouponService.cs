﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Campaign;
using Ecom.ViewModels.Campaigns;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class CouponService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public CouponService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<CouponCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<CouponCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Coupons
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.CouponCode.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Coupons
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.CouponCode.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.ID
                                          select new CouponCreateEditVM
                                          {
                                              ID = p.ID,
                                              CouponCode = p.CouponCode,
                                              PStatus = p.PStatus,
                                              AllowWithOtherCoupons = p.AllowWithOtherCoupons,
                                              CampaignType = p.CampaignType,
                                              BeginDate = p.BeginDate,
                                              EndDate = p.EndDate,
                                              CalculationType = p.CalculationType,
                                              AllowOnDiscountedProducts = p.AllowOnDiscountedProducts,
                                              AllowMultipleUse = p.AllowMultipleUse,
                                              Amount = p.Amount,
                                              AmountLimit = p.AmountLimit,
                                              MaxAmountLimit = p.MaxAmountLimit,
                                              DiscountLimit = p.DiscountLimit,
                                              DomainId = p.DomainId,
                                              CreditCardLimits = p.CreditCardLimits,
                                              PriceCurrencyID = p.PriceCurrencyID,
                                              IsUsed = p.IsUsed,
                                              FromOrderID = p.FromOrderID,
                                              FromUserID = p.FromUserID,
                                              UserID = p.UserID,
                                              CampaignID = p.CampaignID
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Coupons
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.Coupons.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.ID).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new CouponCreateEditVM
                  {
                      ID = p.ID,
                      CouponCode = p.CouponCode,
                      PStatus = p.PStatus,
                      AllowWithOtherCoupons = p.AllowWithOtherCoupons,
                      CampaignType = p.CampaignType,
                      BeginDate = p.BeginDate,
                      EndDate = p.EndDate,
                      CalculationType = p.CalculationType,
                      AllowOnDiscountedProducts = p.AllowOnDiscountedProducts,
                      AllowMultipleUse = p.AllowMultipleUse,
                      Amount = p.Amount,
                      AmountLimit = p.AmountLimit,
                      MaxAmountLimit = p.MaxAmountLimit,
                      DiscountLimit = p.DiscountLimit,
                      DomainId = p.DomainId,
                      CreditCardLimits = p.CreditCardLimits,
                      PriceCurrencyID = p.PriceCurrencyID,
                      IsUsed = p.IsUsed,
                      FromOrderID = p.FromOrderID,
                      FromUserID = p.FromUserID,
                      UserID = p.UserID,
                      CampaignID = p.CampaignID
                  }

                ).ToList();
            }

           // operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<CouponCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<CouponCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allDomains = db.Domains.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allCampaigns = db.Campaigns.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();


            var allCalculations = serviceUtils.ToSelectList<HesapLamaTuru>(-1);
            var allCampaignTypes = serviceUtils.ToSelectList<KampanyaTuru>(-1);
            var allStatuses = serviceUtils.GetStatusValues(1);


            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new CouponCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.PStatus = Status.Active;
                result.Result.AllCampaignTypes = allCampaignTypes;
                result.Result.AllCalculationTypes = allCalculations;
                result.Result.AllPriceCurrencies = allCurrencies;
                result.Result.AllDomains = allDomains;
                result.Result.AllCampaigns = allCampaigns;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Coupons.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new CouponCreateEditVM
             {
                 ID = p.ID,
                 CouponCode = p.CouponCode,
                 PStatus = p.PStatus,
                 AllowWithOtherCoupons = p.AllowWithOtherCoupons,
                 CampaignType = p.CampaignType,
                 BeginDate = p.BeginDate,
                 EndDate = p.EndDate,
                 CalculationType = p.CalculationType,
                 AllowOnDiscountedProducts = p.AllowOnDiscountedProducts,
                 AllowMultipleUse = p.AllowMultipleUse,
                 Amount = p.Amount,
                 AmountLimit = p.AmountLimit,
                 MaxAmountLimit = p.MaxAmountLimit,
                 DiscountLimit = p.DiscountLimit,
                 DomainId = p.DomainId,
                 CreditCardLimits = p.CreditCardLimits,
                 PriceCurrencyID = p.PriceCurrencyID,
                 IsUsed = p.IsUsed,
                 FromOrderID = p.FromOrderID,
                 FromUserID = p.FromUserID,
                 UserID = p.UserID,
                 CampaignID = p.CampaignID

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {

                result.Result.AllStatuses = allStatuses;
                result.Result.PStatus = Status.Active;
                result.Result.AllCampaignTypes = allCampaignTypes;
                result.Result.AllCalculationTypes = allCalculations;
                result.Result.AllPriceCurrencies = allCurrencies;
                result.Result.AllDomains = allDomains;
                result.Result.AllCampaigns = allCampaigns;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Coupon = db.Coupons.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Coupon != null)
            {
                Coupon.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.CouponDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Coupon MapToDBObject(CouponCreateEditVM model, Coupon dbModel)
        {
            var coupon = dbModel ?? new Coupon();

            coupon.ID = model.ID;
            coupon.CouponCode = model.CouponCode;
            coupon.PStatus = model.PStatus;

            coupon.StoreAccountID = model.ID == 0 ? model.StoreID : coupon.StoreAccountID;
            coupon.ModifiedOn = DateTime.Now;
            if (coupon.ID != 0) coupon.CreatedOn = DateTime.Now;

            coupon.BeginDate = model.BeginDate;
            coupon.EndDate = model.EndDate;
            coupon.CalculationType = model.CalculationType;
            coupon.AllowOnDiscountedProducts = model.AllowOnDiscountedProducts;
            coupon.AllowWithOtherCoupons = model.AllowWithOtherCoupons;
            coupon.AllowMultipleUse = model.AllowMultipleUse;
            coupon.Amount = model.Amount;
            coupon.AmountLimit = model.AmountLimit;
            coupon.MaxAmountLimit = model.MaxAmountLimit;
            coupon.DiscountLimit = model.DiscountLimit;
            coupon.CreationLimit = model.CreationLimit;
            coupon.DomainId = model.DomainId;
            coupon.CampaignID = model.CampaignID;

            coupon.CampaignType = model.CampaignType;
            coupon.CreditCardLimits = model.CreditCardLimits;
            coupon.PriceCurrencyID = model.PriceCurrencyID;
            coupon.StoreAccountID = model.ID == 0 ? model.StoreID : coupon.StoreAccountID;
            coupon.ModifiedOn = DateTime.Now;

            if (coupon.ID != 0) coupon.CreatedOn = DateTime.Now;

            return coupon;

        }

        private CouponCreateEditVM MapToVM(Coupon model)
        {
            if (model == null) return null;

            var coupon = new CouponCreateEditVM();
            coupon.ID = model.ID;
            coupon.CouponCode = model.CouponCode;
            coupon.PStatus = model.PStatus;

            coupon.BeginDate = model.BeginDate;
            coupon.EndDate = model.EndDate;
            coupon.CalculationType = model.CalculationType;
            coupon.AllowOnDiscountedProducts = model.AllowOnDiscountedProducts;
            coupon.AllowWithOtherCoupons = model.AllowWithOtherCoupons;
            coupon.AllowMultipleUse = model.AllowMultipleUse;
            coupon.Amount = model.Amount;
            coupon.AmountLimit = model.AmountLimit;
            coupon.MaxAmountLimit = model.MaxAmountLimit;
            coupon.CampaignID = model.CampaignID;
            coupon.DiscountLimit = model.DiscountLimit;
            coupon.CreationLimit = model.CreationLimit;
            coupon.DomainId = model.DomainId;
            coupon.CampaignType = model.CampaignType;
            coupon.CreditCardLimits = model.CreditCardLimits;
            coupon.PriceCurrencyID = model.PriceCurrencyID;

            return coupon;
        }

        public OperationResult<CouponCreateEditVM> Save(CouponCreateEditVM Coupon)
        {
            var result = new OperationResult<CouponCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Coupon, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Coupon existing = null;

            if (Coupon.ID != 0)
            {
                existing = db.Coupons.Where(x => x.ID == Coupon.ID).FirstOrDefault();
                existing = MapToDBObject(Coupon, existing);
            }
            else
            {
                existing = MapToDBObject(Coupon, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Coupon.ID == 0 ? QueueItemType.CouponCreated : QueueItemType.CouponUpdated, JsonConvert.SerializeObject(result.Result), Coupon.StoreID);

            return result;

        }

        public List<CouponVM> GetUserCoupons(int storeId, int? userID, int priceCurrencyID, int domainID)
        {

            List<CouponVM> coupons = (from p in db.Coupons.AsNoTracking()
                                      where
                                      p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                      (p.CampaignID == null || (p.CampaignID != null
                                      && (p.DomainId == null || p.DomainId == domainID)))
                                      && p.PriceCurrencyID == priceCurrencyID && (p.UserID == userID && p.IsUsed == false && p.EndDate > DateTime.Now)
                                      select new CouponVM
                                      {
                                          AllowMultipleUse = p.AllowMultipleUse,
                                          Amount = p.Amount,
                                          BeginDate = p.BeginDate,
                                          AmountLimit = p.AmountLimit,
                                          CalculationType = p.CalculationType,
                                          CampaignID = p.CampaignID,
                                          CouponCode = p.CouponCode,
                                          EndDate = p.EndDate,
                                          ID = p.ID,
                                          IsUsed = p.IsUsed,
                                          UserID = p.UserID,
                                          MaxAmountLimit = p.MaxAmountLimit

                                      }).ToList();
            return coupons;

        }


        public CouponVM GetUserCoupon(int storeId, int? userID, long couponId, string couponCode, int priceCurrencyID, int domainID)
        {
            if (userID==null || ( String.IsNullOrWhiteSpace(couponCode) && couponId == 0)) return null;

            CouponVM coupons = (from p in db.Coupons.AsNoTracking()
                                where
                                p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                (p.ID == couponId || p.CouponCode == couponCode) &&
                                (p.CampaignID == null || (p.CampaignID != null
                                && (p.DomainId == null || p.DomainId == domainID)))
                                && p.PriceCurrencyID == priceCurrencyID && (p.UserID == userID && p.IsUsed == false && p.EndDate > DateTime.Now)
                                select new CouponVM
                                {
                                    AllowMultipleUse = p.AllowMultipleUse,
                                    Amount = p.Amount,
                                    BeginDate = p.BeginDate,
                                    AmountLimit = p.AmountLimit,
                                    CalculationType = p.CalculationType,
                                    CampaignID = p.CampaignID,
                                    CouponCode = p.CouponCode,
                                    EndDate = p.EndDate,
                                    ID = p.ID,
                                    IsUsed = p.IsUsed,
                                    UserID = p.UserID,
                                    AllowOnDiscountedProducts = p.AllowOnDiscountedProducts,
                                    AllowWithOtherCoupons = p.AllowWithOtherCoupons,
                                    MaxAmountLimit = p.MaxAmountLimit,
                                    CampaignType = p.CampaignType,
                                    CreationLimit = p.CreationLimit,
                                    CreditCardLimits = p.CreditCardLimits,
                                    DiscountLimit = p.DiscountLimit,
                                    DomainId = p.DomainId,
                                    FromUserID = p.FromUserID,
                                    FromOrderID = p.FromOrderID,
                                    PriceCurrencyID = p.PriceCurrencyID,
                                    PStatus = p.PStatus,

                                }).FirstOrDefault();
            return coupons;

        }

        public CouponVM GetCoupon(int storeId, long couponId, string couponCode, int priceCurrencyID, int domainID)
        {
            if (String.IsNullOrWhiteSpace(couponCode) && couponId == 0) return null;

            CouponVM coupons = (from p in db.Coupons.AsNoTracking()
                                where
                                p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                (p.ID == couponId || p.CouponCode == couponCode) &&
                                (p.CampaignID == null || (p.CampaignID != null
                                && (p.DomainId == null || p.DomainId == domainID)))
                                && p.PriceCurrencyID == priceCurrencyID && (p.EndDate > DateTime.Now)
                                select new CouponVM
                                {
                                    AllowMultipleUse = p.AllowMultipleUse,
                                    Amount = p.Amount,
                                    BeginDate = p.BeginDate,
                                    AmountLimit = p.AmountLimit,
                                    CalculationType = p.CalculationType,
                                    CampaignID = p.CampaignID,
                                    CouponCode = p.CouponCode,
                                    EndDate = p.EndDate,
                                    ID = p.ID,
                                    IsUsed = p.IsUsed,
                                    UserID = p.UserID,
                                    AllowOnDiscountedProducts = p.AllowOnDiscountedProducts,
                                    AllowWithOtherCoupons = p.AllowWithOtherCoupons,
                                    MaxAmountLimit = p.MaxAmountLimit,
                                    CampaignType = p.CampaignType,
                                    CreationLimit = p.CreationLimit,
                                    CreditCardLimits = p.CreditCardLimits,
                                    DiscountLimit = p.DiscountLimit,
                                    DomainId = p.DomainId,
                                    FromUserID = p.FromUserID,
                                    FromOrderID = p.FromOrderID,
                                    PriceCurrencyID = p.PriceCurrencyID,
                                    PStatus = p.PStatus,

                                }).FirstOrDefault();
            return coupons;

        }

    }
}
