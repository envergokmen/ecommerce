﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.ContentPageLocalLangs;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class ContentPageLocalLangService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public ContentPageLocalLangService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<ContentPageLocalLangCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10, int? contentPageId = null)
        {
            var operationResult = new PagedOperationListResult<ContentPageLocalLangCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            var totalQuery = (from p in db.ContentPageLocalLangs
                              where
                               p.StoreAccountID == storeId && p.PStatus == Status.Active
                              select p);

            var query = (from p in db.ContentPageLocalLangs
                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                         orderby p.ID
                         select p).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize));

            if (contentPageId.HasValue && contentPageId > 0)
            {
                totalQuery = totalQuery.Where(x => x.ContentPageID == contentPageId);
                query = query.Where(x => x.ContentPageID == contentPageId);
            }


            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {
                totalQuery = totalQuery.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
                query = query.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
            }

            operationResult.Result = query.Select(p =>
                              new ContentPageLocalLangCreateEditVM
                              {
                                  ID = p.ID,
                                  Name = p.Name,
                                  PStatus = p.PStatus,
                                  LangCode = p.Lang.LangCode,
                                  ParentName = p.ContentPage.Name,
                                  ContentPageID = p.ContentPageID
                              }).ToList();


            totalItemCount = totalQuery.Count();

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<ContentPageLocalLangCreateEditVM> Get(int id, int storeId, int typeid)
        {
            var result = new OperationResult<ContentPageLocalLangCreateEditVM>();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allContentPages = db.ContentPages.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();


            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new ContentPageLocalLangCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllContentPages = allContentPages;
                result.Result.ContentPageID = typeid;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.ContentPageLocalLangs.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new ContentPageLocalLangCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 ContentPageID = p.ContentPageID,
                 DetailHTML = p.DetailHTML,
                 LangID = p.LangID,
                 Title = p.Title,
                 Summary = p.Summary,
                 CanonicalLink = p.CanonicalLink,
                 MetaDescription = p.MetaDescription,
                 MetaKeywords = p.MetaKeywords,
                 Url = p.Url,

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllContentPages = allContentPages;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var ContentPage = db.ContentPageLocalLangs.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (ContentPage != null)
            {
                ContentPage.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ContentPageDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private ContentPageLocalLang MapToDBObject(ContentPageLocalLangCreateEditVM model, ContentPageLocalLang dbModel)
        {
            var ContentPage = dbModel ?? new ContentPageLocalLang();

            ContentPage.ID = model.ID;

            ContentPage.Name = model.Name;
            ContentPage.PStatus = model.PStatus;
            ContentPage.LangID = model.LangID;
            ContentPage.ContentPageID = model.ContentPageID;
            ContentPage.DetailHTML = model.DetailHTML;
            ContentPage.Title = model.Title;
            ContentPage.Summary = model.Summary;
            ContentPage.MetaDescription = model.MetaDescription;
            ContentPage.MetaKeywords = model.MetaKeywords;
            ContentPage.CanonicalLink = model.CanonicalLink;
            ContentPage.Url = model.Url;

            ContentPage.StoreAccountID = model.ID == 0 ? model.StoreID : ContentPage.StoreAccountID;
            ContentPage.ModifiedOn = DateTime.Now;
            if (ContentPage.ID != 0) ContentPage.CreatedOn = DateTime.Now;

            return ContentPage;

        }

        private ContentPageLocalLangCreateEditVM MapToVM(ContentPageLocalLang dbModel)
        {
            if (dbModel == null) return null;

            var ContentPage = new ContentPageLocalLangCreateEditVM();
            ContentPage.ID = dbModel.ID;
            ContentPage.Name = dbModel.Name;
            ContentPage.DetailHTML = dbModel.DetailHTML;
            ContentPage.LangID = dbModel.LangID;
            ContentPage.ContentPageID = dbModel.ContentPageID;
            ContentPage.PStatus = dbModel.PStatus;
            ContentPage.Title = dbModel.Title;
            ContentPage.Summary = dbModel.Summary;
            ContentPage.MetaDescription = dbModel.MetaDescription;
            ContentPage.MetaKeywords = dbModel.MetaKeywords;
            ContentPage.CanonicalLink = dbModel.CanonicalLink;
            ContentPage.Url = dbModel.Url;

            return ContentPage;
        }

        public OperationResult<ContentPageLocalLangCreateEditVM> Save(ContentPageLocalLangCreateEditVM ContentPage)
        {
            var result = new OperationResult<ContentPageLocalLangCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(ContentPage, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            ContentPageLocalLang existing = null;

            if (ContentPage.ID != 0)
            {
                existing = db.ContentPageLocalLangs.Where(x => x.ID == ContentPage.ID).FirstOrDefault();
                existing = MapToDBObject(ContentPage, existing);
            }
            else
            {
                existing = MapToDBObject(ContentPage, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(ContentPage.ID == 0 ? QueueItemType.ContentPageCreated : QueueItemType.ContentPageUpdated, JsonConvert.SerializeObject(result.Result), ContentPage.StoreID);

            return result;

        }


    }
}
