﻿using Ecom.Core;
using Ecom.Models;
using Ecom.ViewModels.Banks;
using System;
using System.Linq;
using Microsoft.Extensions.Localization;
using Ecom.Services.Bus;
using Newtonsoft.Json;
using Ecom.ViewModels;

namespace Ecom.Services
{
    public class BankService
    {

        private readonly EcomContext db;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        public BankService(
            IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            EcomContext _db,
            RabbitMQService _rabbitMQService
            )
        {
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            this.db = _db;

        }

        public PagedOperationListResult<BankCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<BankCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Banks
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p)
                                             .OrderBy(c => c.SortNo).Count();

                operationResult.Result = (from p in db.Banks
                                          where p.StoreAccountID == storeId && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          && p.PStatus == Status.Active
                                          orderby p.SortNo
                                          select new BankCreateEditVM
                                          {
                                              ID = p.ID,
                                              LogoPath = p.LogoPath,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              SortNo = p.SortNo

                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Banks
                                  where p.StoreAccountID == storeId && p.PStatus == Status.Active
                                  select p).Count();

                operationResult.Result = db.Banks.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new BankCreateEditVM
                  {
                      ID = p.ID,
                      LogoPath = p.LogoPath,
                      Name = p.Name,
                      IBAN = p.IBAN,
                      AccountNo = p.AccountNo,
                      SortCode = p.SortCode,
                      PStatus = p.PStatus,
                      SortNo = p.SortNo
                  }

                ).ToList();
            }

            //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<BankCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<BankCreateEditVM>();


            var allPayments = db.Payments.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            if (id == 0)
            {
                result.Result = new BankCreateEditVM();

                result.Result.AllStatuses = allStatuses;
                result.Result.AllPayments = allPayments;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }
            else
            {
                result.Result = db.Banks.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p => new BankCreateEditVM
                {
                    ID = p.ID,
                    LogoPath = p.LogoPath,
                    Name = p.Name,
                    PStatus = p.PStatus,
                    SortNo = p.SortNo,
                    AccountNo = p.AccountNo,
                    BankCode = p.BankCode,
                    PaymentID = p.PaymentID,
                    CityName = p.CityName,
                    CorporateName = p.CorporateName,
                    DepartmentAndNo = p.DepartmentAndNo,
                    IBAN = p.IBAN,
                    SortCode = p.SortCode
                }).FirstOrDefault();
            }


            if (result.Result != null)
            {
                result.Result.AllPayments = allPayments;
                result.Result.AllStatuses = allStatuses;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var bank = db.Banks.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (bank != null)
            {
                bank.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.BankDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        public StandartOperationResult DeleteImage(int id, int storeId)
        {
            var result = new StandartOperationResult();

            var bank = db.Banks.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (bank != null)
            {
                bank.LogoPath = "";
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.BankUpdated, JsonConvert.SerializeObject(result.Result), storeId);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Bank MapToDBObject(BankCreateEditVM model, Bank dbModel)
        {
            var bank = dbModel ?? new Bank();

            bank.ID = model.ID;
            bank.LogoPath = !String.IsNullOrWhiteSpace(model.LogoPath) ? model.LogoPath : dbModel?.LogoPath ?? "";
            bank.Name = model.Name;
            bank.PStatus = model.PStatus;
            bank.SortNo = model.SortNo;
            bank.StoreAccountID = model.StoreID;

            bank.CorporateName = model.CorporateName;
            bank.CityName = model.CityName;
            bank.DepartmentAndNo = model.DepartmentAndNo;
            bank.AccountNo = model.AccountNo;
            bank.SortCode = model.SortCode;
            bank.IBAN = model.IBAN;
            bank.PaymentID = model.PaymentID;
            bank.BankCode = model.BankCode;

            bank.ModifiedOn = DateTime.Now;
            if (bank.ID != 0) bank.CreatedOn = DateTime.Now;

            return bank;

        }

        private BankCreateEditVM MapToVM(Bank dbModel)
        {
            if (dbModel == null) return null;

            var bank = new BankCreateEditVM();
            bank.ID = dbModel.ID;
            bank.LogoPath = dbModel.LogoPath;
            bank.Name = dbModel.Name;
            bank.PStatus = dbModel.PStatus;
            bank.SortNo = dbModel.SortNo;
            bank.StoreAccountID = dbModel.StoreAccountID;
            bank.CorporateName = dbModel.CorporateName;
            bank.CityName = dbModel.CityName;
            bank.DepartmentAndNo = dbModel.DepartmentAndNo;
            bank.AccountNo = dbModel.AccountNo;
            bank.SortCode = dbModel.SortCode;
            bank.IBAN = dbModel.IBAN;
            bank.PaymentID = dbModel.PaymentID;
            bank.BankCode = dbModel.BankCode;

            return bank;

        }

        public OperationResult<BankCreateEditVM> Save(BankCreateEditVM bank)
        {
            var result = new OperationResult<BankCreateEditVM>();

            Bank existing = null;

            if (bank.ID != 0)
            {
                existing = db.Banks.Where(x => x.ID == bank.ID).FirstOrDefault();
                existing = MapToDBObject(bank, existing);
            }
            else
            {
                existing = MapToDBObject(bank, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(bank.ID == 0 ? QueueItemType.BankCreated : QueueItemType.BankUpdated, JsonConvert.SerializeObject(result.Result), bank.StoreID);

            return result;

        }


    }
}
