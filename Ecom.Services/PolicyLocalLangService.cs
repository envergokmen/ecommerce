﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.PolicyLocalLangs;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class PolicyLocalLangService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public PolicyLocalLangService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<PolicyLocalLangCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10, int? policyId = null)
        {
            var operationResult = new PagedOperationListResult<PolicyLocalLangCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            var totalQuery = (from p in db.PolicyLocalLangs
                              where
                               p.StoreAccountID == storeId && p.PStatus == Status.Active
                              select p);

            var query = (from p in db.PolicyLocalLangs
                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                         orderby p.ID
                         select p).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize));

            if (policyId.HasValue && policyId > 0)
            {
                totalQuery = totalQuery.Where(x => x.PolicyID == policyId);
                query = query.Where(x => x.PolicyID == policyId);
            }


            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {
                totalQuery = totalQuery.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
                query = query.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
            }

            operationResult.Result = query.Select(p =>
                              new PolicyLocalLangCreateEditVM
                              {
                                  ID = p.ID,
                                  Name = p.Name,
                                  PStatus = p.PStatus,
                                  LangCode = p.Lang.LangCode,
                                  ParentName = p.Policy.Name,
                                  PolicyID = p.PolicyID
                              }).ToList();


            totalItemCount = totalQuery.Count();

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<PolicyLocalLangCreateEditVM> Get(int id, int storeId, int typeid)
        {
            var result = new OperationResult<PolicyLocalLangCreateEditVM>();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allPolicies = db.Policies.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();


            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new PolicyLocalLangCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllPolicies = allPolicies;
                result.Result.PolicyID = typeid;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.PolicyLocalLangs.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new PolicyLocalLangCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 PolicyID = p.PolicyID,
                 DetailHTML = p.DetailHTML,
                 LangID = p.LangID
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllLangs = allLangs;
                result.Result.AllPolicies = allPolicies;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Policy = db.PolicyLocalLangs.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Policy != null)
            {
                Policy.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.PolicyDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private PolicyLocalLang MapToDBObject(PolicyLocalLangCreateEditVM model, PolicyLocalLang dbModel)
        {
            var Policy = dbModel ?? new PolicyLocalLang();

            Policy.ID = model.ID;

            Policy.Name = model.Name;
            Policy.PStatus = model.PStatus;
            Policy.LangID = model.LangID;
            Policy.PolicyID = model.PolicyID;
            Policy.DetailHTML = model.DetailHTML;
            Policy.StoreAccountID = model.ID == 0 ? model.StoreID : Policy.StoreAccountID;
            Policy.ModifiedOn = DateTime.Now;
            if (Policy.ID != 0) Policy.CreatedOn = DateTime.Now;

            return Policy;

        }

        private PolicyLocalLangCreateEditVM MapToVM(PolicyLocalLang dbModel)
        {
            if (dbModel == null) return null;

            var Policy = new PolicyLocalLangCreateEditVM();
            Policy.ID = dbModel.ID;
            Policy.Name = dbModel.Name;
            Policy.DetailHTML = dbModel.DetailHTML;
            Policy.LangID = dbModel.LangID;
            Policy.PolicyID = dbModel.PolicyID;
            Policy.PStatus = dbModel.PStatus;

            return Policy;
        }

        public OperationResult<PolicyLocalLangCreateEditVM> Save(PolicyLocalLangCreateEditVM Policy)
        {
            var result = new OperationResult<PolicyLocalLangCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Policy, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            PolicyLocalLang existing = null;

            if (Policy.ID != 0)
            {
                existing = db.PolicyLocalLangs.Where(x => x.ID == Policy.ID).FirstOrDefault();
                existing = MapToDBObject(Policy, existing);
            }
            else
            {
                existing = MapToDBObject(Policy, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Policy.ID == 0 ? QueueItemType.PolicyCreated : QueueItemType.PolicyUpdated, JsonConvert.SerializeObject(result.Result), Policy.StoreID);

            return result;

        }


    }
}
