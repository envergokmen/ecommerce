﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Catalogs;
using Ecom.ViewModels.Colors;
using Ecom.ViewModels.Prices;
using Ecom.ViewModels.Products;
using Ecom.ViewModels.XML;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class ProductService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        private readonly PriceCurrencyService currencyService;
        private readonly LangService langService;
        private readonly ColorService colorService;

        private readonly EcomContext db;

        public ProductService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService,
            PriceCurrencyService _currencyService,
            LangService _langService,
            ColorService _colorService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            currencyService = _currencyService;
            langService = _langService;
            colorService = _colorService;
        }

        public PagedOperationListResult<ProductCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<ProductCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Products
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Ecom.Core.Status.Active &&
                                     (p.ProductName.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Products
                                          where p.StoreAccountID == storeId && p.PStatus == Ecom.Core.Status.Active
                                          && ((p.ProductName.ToLower().Contains(searchKeyword.ToLower())) || (p.ProductCode.ToLower().Contains(searchKeyword.ToLower())))
                                          orderby p.ID
                                          select new ProductCreateEditVM
                                          {
                                              ID = p.ID,
                                              ProductName = p.ProductName,
                                              SmallName = p.SmallName,
                                              ProductCode = p.ProductCode,
                                              PStatus = p.PStatus,
                                              Translations = p.ProductDetails.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId).Count(),
                                              StockCount = p.Stocks.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId).Count()
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Products
                                  where p.PStatus == Ecom.Core.Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.Products.Where(x => x.StoreAccountID == storeId && x.PStatus == Ecom.Core.Status.Active).OrderBy(c => c.ID).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new ProductCreateEditVM
                  {
                      ID = p.ID,
                      ProductName = p.ProductName,
                      SmallName = p.SmallName,
                      ProductCode = p.ProductCode,
                      PStatus = p.PStatus,
                      Translations = p.ProductDetails.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId).Count(),
                      StockCount = p.Stocks.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId).Count()
                  }

                ).ToList();
            }

            //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<ProductCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<ProductCreateEditVM>();

            var allReturnPolicies = db.Policies.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId && x.PolicyType == PoliciyType.ReturnPolicy).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID,

            }).ToList();

            var allCargoPolicies = db.Policies.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId && x.PolicyType == PoliciyType.CargoPolicy).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID,

            }).ToList();

            var allColors = db.Colors.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID,

            }).ToList();


            var allBrands = db.Brands.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID,

            }).ToList();

            var allSeasons = db.Seasons.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID,

            }).ToList();


            var allDepartments = db.ProductDepartments.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID,

            }).ToList();

            var allStyles = db.ProductStyles.Where(x => x.PStatus == Ecom.Core.Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID,

            }).ToList();


            var allStatuses = serviceUtils.GetStatusValues(1);
            var allGenders = serviceUtils.ToSelectListInt<Gender>(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new ProductCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllProductDepartments = allDepartments;
                result.Result.AllProductStyles = allStyles;
                result.Result.AllColors = allColors;
                result.Result.AllBrands = allBrands;
                result.Result.AllSeasons = allSeasons;
                result.Result.AllCargoPolicies = allCargoPolicies;
                result.Result.AllReturnPolicies = allReturnPolicies;
                result.Result.AllGenders = allGenders;

                result.Result.PStatus = Ecom.Core.Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Products.Include(v => v.ProductImages).Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
               new ProductCreateEditVM
               {
                   ID = p.ID,
                   ProductName = p.ProductName,
                   SeasonID = p.SeasonID,
                   Gender = p.Gender,
                   BrandID = p.BrandID,
                   SmallName = p.SmallName,
                   ProductCode = p.ProductCode,
                   CargoPolicyID = p.CargoPolicyID,
                   ReturnPolicyID = p.ReturnPolicyID,
                   ProductDepartmentID = p.ProductDepartmentID,
                   ProductStyleID = p.ProductStyleID,
                   PStatus = p.PStatus,
                   Images = p.ProductImages.Where(x => x.PStatus == Ecom.Core.Status.Active).OrderBy(x => x.SortNo).Select(x => new ProductImageVM
                   {
                       ColorID = x.ColorID,
                       Angle = x.Angle,
                       ImagePath = x.ImagePath,
                       ID = x.ID,
                       ProductID = x.ProductID,
                       SortNo = x.SortNo
                   }).ToList()
               }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllProductDepartments = allDepartments;
                result.Result.AllProductStyles = allStyles;
                result.Result.AllBrands = allBrands;
                result.Result.AllSeasons = allSeasons;
                result.Result.AllCargoPolicies = allCargoPolicies;
                result.Result.AllReturnPolicies = allReturnPolicies;
                result.Result.AllGenders = allGenders;
                result.Result.AllColors = allColors;

                if (result.Result.Images != null)
                {
                    MapColorImagesNames(result.Result.Images);
                }

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        private void MapColorImagesNames(List<ProductImageVM> images)
        {
            Dictionary<int, string> colors = new Dictionary<int, string>();

            foreach (var item in images)
            {
                if (item.ColorID.HasValue)
                {
                    if (colors.ContainsKey(item.ColorID.Value))
                    {
                        item.ColorName = colors[item.ColorID.Value];
                    }
                    else
                    {
                        item.ColorName = db.Colors.Where(x => x.ID == item.ColorID).Select(c => c.Name).FirstOrDefault();
                        colors.Add(item.ColorID.Value, item.ColorName);
                    }


                }
            }
        }

        public OperationListResult<ProductForCatalogVM> GetProductForCatalog(long productId, int storeId)
        {
            var result = new OperationListResult<ProductForCatalogVM>();

            var productForCat =
                (from p in db.Products
                 join s in db.ProductImages on p.ID equals s.ProductID
                 join c in db.Colors on s.ColorID equals c.ID
                 where p.ID == productId && p.StoreAccountID == storeId
                 group p by new
                 {
                     ColorName = c.Name,
                     ProductCode = p.ProductCode,
                     ProductID = p.ID,
                     ColorID = c.ID,
                 } into gcs

                 select new ProductForCatalogVM { ProductCode = gcs.Key.ProductCode, ColorName = gcs.Key.ColorName, ColorID = gcs.Key.ColorID, ProductID = gcs.Key.ProductID }
                 ).ToList();

            var allProductImages = db.ProductImages.Where(c => c.ProductID == productId).OrderBy(c => c.SortNo).Select(c => new { c.ColorID, c.ImagePath }).ToList();

            foreach (var item in productForCat)
            {
                item.ImagePath = allProductImages.Where(c => c.ColorID == item.ColorID).Select(c => c.ImagePath).FirstOrDefault();
            }

            result.SetSuccessAndClearError(productForCat, sharedDatalocalizer["GeneralSuccessMessage"]);

            return result;

        }

        public List<ProductInCatalogVM> GetProductInCatalog(long productId, int storeId)
        {
            var result =
                (from p in db.Products
                 join s in db.ProductInCatalogs on p.ID equals s.ProductID
                 where p.ID == productId && p.StoreAccountID == storeId && s.StoreAccountID == storeId
                 && s.CatalogID != null
                 select new ProductInCatalogVM
                 {
                     ColorID = s.ColorID,
                     CatalogID = s.CatalogID.Value,
                     CatalogName = s.Catalog.Name,
                     ColorGlobalID = s.ColorGlobalID,
                     ProductID = s.ProductID ?? 0,
                     ID = s.ID,
                     SortNo = s.SortNo ?? 0,
                     StoreID = s.StoreAccountID ?? 0

                 }).ToList();

            var allColors = db.Colors.Where(c => c.StoreAccountID == storeId).OrderBy(c => c.SortNo).Select(c => new { c.ID, c.Name }).ToList();

            foreach (var item in result)
            {
                if (item.ColorID != null)
                    item.ColorName = allColors.Where(c => c.ID == item.ColorID).Select(c => c.Name).FirstOrDefault();
            }


            return result;

        }


        public List<ProductInCatalogVMBasic> GetProductInCatalogBasic(long productId, int storeId)
        {
            var result = GetProductInCatalog(productId, storeId)?.Select(c => new ProductInCatalogVMBasic
            {

                CatalogID = c.CatalogID,
                ColorGlobalID = c.ColorGlobalID ?? 0,
                ColorID = c.ColorID ?? 0,
                ProductID = c.ProductID,
                SortNo = c.SortNo,
                StoreID = c.StoreID
            }).ToList();
            if (result == null) result = new List<ProductInCatalogVMBasic>();
            return result;

        }

        public StandartOperationResult UpdateCatalogSort(long catalogId, int storeId, List<ProductInCatalogVM> items)
        {
            var result = new StandartOperationResult();
            if (items == null || catalogId == 0 || storeId == 0) return result;

            var allIDs = items.Select(c => c.ID).ToArray();

            var addedProductCatalogs =
               (from p in db.ProductInCatalogs
                where p.CatalogID == catalogId && p.StoreAccountID == storeId
                select p).ToList();

            int sortNo = 0;
            foreach (var item in items)
            {
                var curItem = addedProductCatalogs.FirstOrDefault(c => c.ID == item.ID);
                curItem.SortNo = sortNo;
                sortNo++;
            }

            db.SaveChanges();

            return result;

        }
        public OperationListResult<ProductInCatalogVM> GetCatalogProductsForSort(long catalogId, int storeId)
        {
            var result = new OperationListResult<ProductInCatalogVM>();

            var productnCats =
                (from p in db.Products
                 join s in db.ProductInCatalogs on p.ID equals s.ProductID
                 where s.CatalogID == catalogId && p.StoreAccountID == storeId && s.StoreAccountID == storeId
                 && s.CatalogID != null
                 orderby s.SortNo
                 select new ProductInCatalogVM
                 {
                     ColorID = s.ColorID,
                     CatalogID = s.CatalogID.Value,
                     CatalogName = s.Catalog.Name,
                     ProductName = p.ProductName,
                     ProductCode = p.ProductCode,
                     ID = s.ID,
                     ImagePath = p.ProductImages.Where(c => c.ColorID == s.ColorID).Select(i => i.ImagePath).FirstOrDefault()
                 }).ToList();

            var allColors = db.Colors.Where(c => c.StoreAccountID == storeId).OrderBy(c => c.SortNo).Select(c => new { c.ID, c.Name }).ToList();

            foreach (var item in productnCats)
            {
                if (item.ColorID != null)
                    item.ColorName = allColors.Where(c => c.ID == item.ColorID).Select(c => c.Name).FirstOrDefault();
            }


            result.SetSuccessAndClearError(productnCats, sharedDatalocalizer["GeneralSuccessMessage"]);

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Product = db.Products.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Product != null)
            {
                Product.PStatus = Ecom.Core.Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ProductDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        public OperationResult<ProductResultVM> DeleteImage(int id, int storeId)
        {
            var result = new OperationResult<ProductResultVM>();

            var image = db.ProductImages.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (image != null)
            {
                db.Remove(image);
                db.SaveChanges();
                result.SetSuccessAndClearError(new ProductResultVM { ProductId = image.ProductID, ImageId = image.ID }, sharedDatalocalizer["GeneralSuccessMessage"]);

                rabbitMQService.Publish(QueueItemType.ProductImageDeleted, JsonConvert.SerializeObject(result.Result), storeId);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        public OperationResult<ProductResultVM> DeleteAllImages(long id, int storeId)
        {
            var result = new OperationResult<ProductResultVM>();

            var images = db.ProductImages.Where(x => x.ProductID == id && x.StoreAccountID == storeId).ToList();

            if (images != null)
            {
                foreach (var item in images)
                {
                    db.Remove(item);
                }

                db.SaveChanges();
                result.SetSuccessAndClearError(new ProductResultVM { ProductId = id, ImageIds = images.Select(c => c.ID).ToList() }, sharedDatalocalizer["GeneralSuccessMessage"]);

                rabbitMQService.Publish(QueueItemType.ProductImageUpdated, JsonConvert.SerializeObject(result.Result), storeId);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Product MapToDBObject(ProductCreateEditVM model, Product dbModel)
        {
            var Product = dbModel ?? new Product();

            Product.ID = model.ID;

            Product.SeasonID = model.SeasonID;
            Product.Gender = model.Gender;
            Product.BrandID = model.BrandID;
            Product.ProductCode = model.ProductCode;
            Product.CargoPolicyID = model.CargoPolicyID;
            Product.ReturnPolicyID = model.ReturnPolicyID;
            Product.ProductDepartmentID = model.ProductDepartmentID;
            Product.ProductStyleID = model.ProductStyleID;
            Product.SmallName = model.SmallName;
            Product.ProductName = model.ProductName;
            Product.PStatus = model.PStatus;

            Product.StoreAccountID = model.ID == 0 ? model.StoreID : Product.StoreAccountID;
            Product.ModifiedOn = DateTime.Now;
            if (Product.ID != 0) Product.CreatedOn = DateTime.Now;

            var lastImageSort = db.ProductImages.Where(x => x.ProductID == Product.ID && x.PStatus == Ecom.Core.Status.Active).OrderByDescending(c => c.SortNo).Select(x => x.SortNo).FirstOrDefault();

            MapProductImages(model, Product, lastImageSort);

            return Product;
        }

        private void MapProductImages(ProductCreateEditVM model, Product Product, int lastSort)
        {
            var SortNo = lastSort + 1;

            if (model.Images != null)
            {
                foreach (var img in model.Images)
                {
                    ProductImage prdImg = new ProductImage();
                    prdImg.ImagePath = img.ImagePath;
                    prdImg.PStatus = Ecom.Core.Status.Active;
                    prdImg.Angle = img.Angle;
                    prdImg.SortNo = SortNo;
                    prdImg.StoreAccountID = model.StoreID;

                    var curColorGlobal = db.ColorGlobals.FirstOrDefault(x => x.Name == img.ColorGlobalName && x.StoreAccountID == model.StoreID);

                    if (curColorGlobal == null)
                    {
                        curColorGlobal = new ColorGlobal
                        {
                            Name = img.ColorGlobalName,
                            StoreAccountID = model.StoreID,
                            HexValue = img.ColorGlobalName,
                            PStatus = Ecom.Core.Status.Active
                        };

                        db.ColorGlobals.Add(curColorGlobal);
                        db.SaveChanges();

                    }


                    if (img.ColorName != null)
                    {
                        var curColor = db.Colors.FirstOrDefault(x => x.Name == img.ColorName && x.StoreAccountID == model.StoreID);

                        if (curColor == null)
                        {
                            curColor = new Color();
                            curColor.ColorGlobal = curColorGlobal;
                            curColor.StoreAccountID = model.StoreID;
                            curColor.Name = img.ColorName;
                            curColor.PStatus = Ecom.Core.Status.Active;
                            db.Colors.Add(curColor);
                            db.SaveChanges();
                        }

                        prdImg.Color = curColor;
                    }

                    Product.ProductImages.Add(prdImg);
                    SortNo++;
                }
                // Product.ProductImages.Add()
            }
        }

        private ProductCreateEditVM MapToVM(Product dbModel)
        {
            if (dbModel == null) return null;

            var Product = new ProductCreateEditVM();
            Product.ID = dbModel.ID;
            Product.ProductName = dbModel.ProductName;
            Product.PStatus = dbModel.PStatus;
            Product.SmallName = dbModel.SmallName;
            Product.SeasonID = dbModel.SeasonID;
            Product.Gender = dbModel.Gender;
            Product.BrandID = dbModel.BrandID;
            Product.ProductCode = dbModel.ProductCode;
            Product.CargoPolicyID = dbModel.CargoPolicyID;
            Product.ReturnPolicyID = dbModel.ReturnPolicyID;
            Product.ProductDepartmentID = dbModel.ProductDepartmentID;
            Product.ProductStyleID = dbModel.ProductStyleID;
            Product.SetStoreId(dbModel.StoreAccountID);

            return Product;
        }

        public OperationResult<ProductCreateEditVM> Save(ProductCreateEditVM Product)
        {
            var result = new OperationResult<ProductCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Product, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Product existing = null;

            if (Product.ID != 0)
            {
                existing = db.Products.Where(x => x.ID == Product.ID).FirstOrDefault();
                existing = MapToDBObject(Product, existing);
            }
            else
            {
                existing = MapToDBObject(Product, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Product.ID == 0 ? QueueItemType.ProductCreated : QueueItemType.ProductUpdated, JsonConvert.SerializeObject(result.Result), Product.StoreID);

            return result;

        }


        public OperationListResult<ProductImageVM> UpdateImage(ProductImageVM image)
        {
            var result = new OperationListResult<ProductImageVM>();
            var validationResult = serviceUtils.TryToValidateModel(image, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            ProductImage existing = null;


            existing = db.ProductImages.Where(x => x.ID == image.ID && x.StoreAccountID == image.StoreID).FirstOrDefault();

            if (existing != null)
            {
                existing.Angle = image.Angle;
                existing.ColorID = image.ColorID;
                existing.SortNo = image.SortNo;

                db.SaveChanges();

                var images = db.ProductImages.Where(x => x.ProductID == existing.ProductID && x.StoreAccountID == image.StoreID && x.PStatus == Ecom.Core.Status.Active)
                    .OrderBy(x => x.SortNo).Select(x => new ProductImageVM
                    {
                        ColorID = x.ColorID,
                        Angle = x.Angle,
                        ImagePath = x.ImagePath,
                        ID = x.ID,
                        ProductID = x.ProductID,
                        SortNo = x.SortNo
                    }).ToList();

                result.SetSuccessAndClearError(images, sharedDatalocalizer["GeneralSuccessMessage"]);
                MapColorImagesNames(images);

                rabbitMQService.Publish(image.ID == 0 ? QueueItemType.ProductImageCreated : QueueItemType.ProductImageUpdated, JsonConvert.SerializeObject(result.Result), image.StoreID);
            }
            return result;

        }

        public PagedOperationListResult<ProductVM> GetFilteredProductsInMemory(
         int storeId,
         int priceCurrencyID,
         int langId,
         int catalogId,
         int page = 1,
         int pageSize = 48,
         int?[] Department = null,
         int?[] Style = null,
         int?[] Color = null,
         int?[] Size = null,
         string searchKeyWord = null
         )
        {
            var operationResult = new PagedOperationListResult<ProductVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            var depString = Department != null ? string.Join<int?>("-", Department) : null;
            var styleString = Style != null ? string.Join<int?>("-", Style) : null;
            var colorString = Color != null ? string.Join<int?>("-", Color) : null;
            var sizeString = Size != null ? string.Join<int?>("-", Size) : null;

            string cacheKey = String.Concat("Products_", storeId, "_price_", priceCurrencyID, "_lang_", langId, "_catalog_", catalogId, "_page_", page, "_size_", pageSize,
                 "_dep_", depString, "_style_", styleString, "_color_", colorString, "_", "_size_", sizeString
                );


            List<ProductVM> result = cache.Get<List<ProductVM>>(cacheKey);

            if (result == null || 1 == 1)
            {

                var products = GetAllProductsForCategory(storeId, priceCurrencyID, langId, catalogId);

                var query = from p in products select p;

                if (Department != null && Department.Any())
                {
                    query = query.Where(c => Department.Contains(c.ProductDepartmentID));
                }


                if (Style != null && Style.Any())
                {
                    query = query.Where(c => Style.Contains(c.ProductStyleID));
                }

                if (Size != null && Size.Any())
                {
                    query = query.Where(c => c.SizeVariants.Any(x => Size.Contains(x.SizeID)));
                }

                if (Color != null && Color.Any())
                {
                    query = query.Where(c => Color.Contains(c.ColorGlobalID));
                }

                if (!String.IsNullOrWhiteSpace(searchKeyWord))
                {
                    query = query.Where(c => c.ProductName.Contains(searchKeyWord, StringComparison.InvariantCultureIgnoreCase));
                }

                query = query.Skip(Convert.ToInt32((page - 1) * pageSize))
                                .Take(Convert.ToInt32(pageSize)).ToList();

                operationResult.Result = query.ToList();
                cache.Set(cacheKey, operationResult.Result, DateTime.Now.AddHours(1));

            }
            else
            {
                operationResult.Result = result;
            }

            operationResult.PageSize = pageSize;
            operationResult.Page = page;

            return operationResult;
        }


        private ElasticClient CreateElasticClient()
        {
            var client = new ElasticClient(new ConnectionSettings(new Uri("http://localhost:9200")).DefaultIndex("products"));
            //CreateMappings(client);
            return client;
        }

        private void CreateMappings(ElasticClient client)
        {
            var createIndexResponse = client.CreateIndex("products", c => c
                                   .Mappings(ms => ms

                                       .Map<ProductXVM>(m => m
                                           .AutoMap<ProductLocalVM>()
                                           .AutoMap<StockVM>()
                                           .AutoMap<ProductImageVM>()
                                           .AutoMap<PriceVM>()
                                           .AutoMap<ProductInCatalogVMBasic>()
                                           .AutoMap<SizeVM>()
                                       )
                                   )
                               );
        }


        public PagedOperationListResult<ProductXVM> GetFilteredProductElastic(
     int storeId,
     int priceCurrencyID,
     int langId,
     int catalogId,
     int page = 1,
     int pageSize = 16,
     int?[] Department = null,
     int?[] Style = null,
     int?[] Color = null,
     int?[] Size = null,
     string searchKeyWord = null
     )
        {

            var depString = Department != null ? string.Join<int?>("-", Department) : null;
            var styleString = Style != null ? string.Join<int?>("-", Style) : null;
            var colorString = Color != null ? string.Join<int?>("-", Color) : null;
            var sizeString = Size != null ? string.Join<int?>("-", Size) : null;

            string cacheKey = String.Concat("ProductsEL_", storeId, "_price_", priceCurrencyID, "_lang_", langId, "_catalog_", catalogId, "_page_", page, "_size_", pageSize,
                 "_dep_", depString, "_style_", styleString, "_color_", colorString, "_", "_size_", sizeString
                );

            var operationResult = cache.Get<PagedOperationListResult<ProductXVM>>(cacheKey);
            if (operationResult != null && 1 == 2) return operationResult;

            if (operationResult == null || 1 == 1)
            {
                operationResult = new PagedOperationListResult<ProductXVM>();
                operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                ElasticClient client = CreateElasticClient();

                var filters = new List<Func<QueryContainerDescriptor<ProductXVM>, QueryContainer>>();

                // filters.Add(fq => fq.Terms(t => t.Field("Prices.PriceCurrencyId").Terms(priceCurrencyID)));

                if (Color != null && Color.Any())
                {
                    filters.Add(fq => fq.Terms(t => t.Field(f => f.ColorGlobalID).Terms(Color)));
                }

                if (Department != null && Department.Any())
                {
                    filters.Add(fq => fq.Terms(t => t.Field(f => f.ProductDepartmentID).Terms(Department)));
                }

                if (Style != null && Style.Any())
                {
                    filters.Add(fq => fq.Terms(t => t.Field(f => f.ProductStyleID).Terms(Style)));
                }

                if (Size != null && Size.Any())
                {
                    filters.Add(fq => fq.Terms(t => t.Field(f => f.Sizes).Terms(Size)));
                }


                if (catalogId > 0)
                {
                    ISearchResponse<ProductXVM> searchResponse =

                    client.Search<ProductXVM>(s => s
                       .From(Convert.ToInt32((page - 1) * pageSize))
                       .Size(pageSize)
                       .Query(q => q
                           .Bool(b => b
                               .Must(mu => mu
                                   .Match(m => m
                                       .Field(f => f.CatalogID)
                                       .Query(catalogId.ToString())
                                   ),
                                    pr => pr
                                   .Match(mr => mr
                                       .Field("prices.priceCurrencyID")
                                       .Query(priceCurrencyID.ToString())
                                   ),

                                  cv => cv
                                   .Match(cs => cs
                                       .Field(f => f.Pstatus)
                                       .Query("1")
                                   ),

                                  we => we
                                   .Match(qa => qa
                                       .Field(j => j.StoreID)
                                       .Query(storeId.ToString())
                                   )

                               ).MustNot((mu => mu
                                   .Match(m => m
                                       .Field(f => f.TotalQuantity)
                                       .Query("0")
                                   )))
                               .Filter(filters)
                           )
                       )
                   );


                    operationResult.Result = searchResponse.Documents?.ToList();
                    operationResult.TotalItem = searchResponse.HitsMetadata?.Total ?? 0;

                }
                else
                {
                    ISearchResponse<ProductXVM> searchResponse =

                    client.Search<ProductXVM>(s => s
                    .Index("uniqueproducts")
                       .From(Convert.ToInt32((page - 1) * pageSize))
                       .Size(pageSize)
                       .Query(q => q
                           .Bool(b => b
                              .Must(mu => mu
                                   .Match(m => m
                                       .Field("searchItems")
                                       .Query(searchKeyWord)
                                   ),
                                   cr => cr
                                  .Match(mr => mr
                                       .Field("prices.priceCurrencyID")
                                       .Query(priceCurrencyID.ToString())
                                   ),

                                  cv => cv
                                   .Match(cs => cs
                                       .Field(f => f.Pstatus)
                                       .Query("1")
                                   ),

                                  we => we
                                   .Match(qa => qa
                                       .Field(j => j.StoreID)
                                       .Query(storeId.ToString())
                                   )

                               ).MustNot((mu => mu
                                   .Match(m => m
                                       .Field(f => f.TotalQuantity)
                                       .Query("0")
                                   )))
                               .Filter(filters)
                           )
                       )
                   );

                    operationResult.Result = searchResponse.Documents?.ToList();
                    operationResult.TotalItem = searchResponse.HitsMetadata?.Total ?? 0;

                    SetColorId(operationResult, searchKeyWord, storeId);
                }

                if (operationResult.Result != null)
                {
                    foreach (var item in operationResult.Result)
                    {
                        item.PriceCurrencyId = priceCurrencyID;
                        item.LangID = langId;
                    }
                }


                operationResult.PageSize = pageSize;
                operationResult.Page = page;

                cache.Set(cacheKey, operationResult, DateTime.Now.AddMinutes(20));

            }

            return operationResult;
        }

        private void SetColorId(PagedOperationListResult<ProductXVM> operationResult, string searchKeyWord, int storeId)
        {
            var color = colorService.GetColorForSearchText(searchKeyWord, storeId);

            if (color != null && operationResult != null && operationResult.Result != null)
            {
                foreach (var item in operationResult.Result)
                {
                    if (item.ColorVariants.Any(c => c.ColorID == color.ID))
                    {
                        item.ColorID = color?.ID;
                        item.ColorGlobalID = color?.ColorGlobalID;
                    }
                }
            }
        }

        private List<ProductVM> GetAllProductsForCategory(int storeId, int priceCurrencyID, int langId, int catalogId)
        {

            string cacheKey = String.Concat("ProductsAllCat_", storeId, "_price_", priceCurrencyID, "_lang_", langId, "_catalog_", catalogId);

            List<ProductVM> products = cache.Get<List<ProductVM>>(cacheKey);

            if (products == null || 1 == 2)
            {
                products = (from pd in db.ProductDetails
                            join p in db.Products on pd.ProductID equals p.ID
                            join ct in db.ProductInCatalogs on p.ID equals ct.ProductID
                            join prc in db.Prices on p.ID equals prc.ProductID
                            where ct.CatalogID == catalogId
                            && ct.StoreAccountID == storeId
                            && p.StoreAccountID == storeId
                            && pd.LangID == langId
                            && prc.PriceCurrencyID == priceCurrencyID && prc.PStatus == Ecom.Core.Status.Active
                            && p.PStatus == Ecom.Core.Status.Active
                            orderby ct.SortNo
                            select new ProductVM
                            {
                                PriceCurrencyId = priceCurrencyID,
                                ProductID = p.ID,
                                ProductName = pd.ProductName,
                                Gender = p.Gender,
                                ProductDepartmentID = p.ProductDepartmentID,
                                ProductStyleID = p.ProductStyleID,
                                Url = pd.Url,
                                ProductLocalLangID = pd.ID,
                                LangID = pd.LangID,
                                ProductCode = p.ProductCode,
                                SmallName = p.SmallName,
                                CatalogSortNo = ct.SortNo,
                                ColorID = ct.ColorID,
                                ColorGlobalID = ct.Color != null ? ct.Color.ColorGlobalID : 0,
                                SizeVariants = (from szv in db.Stocks where szv.ProductID == p.ID && szv.PStatus == Ecom.Core.Status.Active group szv by new { szv.Size.ID, szv.Size.Name } into g select new SizeVM { SizeID = g.Key.ID, Name = g.Key.Name }).ToList(),
                                Prices = (from c in db.Prices
                                          where c.ProductID == p.ID && c.PStatus == Ecom.Core.Status.Active && c.PriceCurrencyID == priceCurrencyID
                                          select new PriceCreateEditVM
                                          {
                                              CurrencyCode = c.PriceCurrency.CurrencyCode,
                                              CurrencySymbol = c.PriceCurrency.CurrencySymbol,
                                              PriceCurrencyID = c.PriceCurrencyID,
                                              Amount = c.Amount,
                                              AmountOld = c.AmountOld,
                                          }).ToList(),

                                Images = (from i in db.ProductImages where i.ProductID == p.ID && i.PStatus == Ecom.Core.Status.Active select new ProductImageVM { ImagePath = i.ImagePath, ID = i.ID, ColorID = i.ColorID, SortNo = i.SortNo }).ToList(),
                                ColorVariants = (from szv in db.Stocks
                                                 where szv.ProductID == p.ID && szv.PStatus == Ecom.Core.Status.Active
                                                 group szv by new { szv.Color.ID, szv.Color.Name, szv.Color.HexValue, szv.Color.PatternPath } into g
                                                 select new ProductImageVM
                                                 {
                                                     ColorHex = g.Key.HexValue,
                                                     ColorHexPath = g.Key.PatternPath,
                                                     ColorID = g.Key.ID,
                                                     ColorName = g.Key.Name,
                                                     ClassName = g.Key.ID == ct.ColorID ? "active" : ""
                                                     ,
                                                     ImagePath = (from i in db.ProductImages
                                                                  orderby i.SortNo
                                                                  where i.ProductID == p.ID && i.PStatus == Ecom.Core.Status.Active && i.ColorID == g.Key.ID
                                                                  select i.ImagePath).FirstOrDefault()
                                                     ,
                                                     ImagePath2 = (from i in db.ProductImages
                                                                   orderby i.SortNo
                                                                   where i.ProductID == p.ID && i.PStatus == Ecom.Core.Status.Active && i.ColorID == g.Key.ID
                                                                   select i.ImagePath).Skip(1).FirstOrDefault(),

                                                 }).ToList(),

                            }).Take(100).ToList();

                if (products != null)
                {
                    cache.Set(cacheKey, products, DateTime.Now.AddMinutes(30));
                }

            }

            return products;
        }


        public ProductXVM GetProductForIndex(int storeId, long productId)
        {
            ProductXVM result = null;
            if (productId == 0) return result;

            string cacheKey = String.Concat("ProductDetailX_", productId);
            result = cache.Get<ProductXVM>(cacheKey);

            var allLangs = langService.GetlangList(storeId);
            var allCurrencies = currencyService.GetPriceCurrencyList(storeId);


            //TODO:consider to open cache but invalidation is required
            if (result == null || true)
            {
                result = (from p in db.Products
                          where
                          p.StoreAccountID == storeId && p.ID == productId
                          select new ProductXVM
                          {
                              Pstatus = p.PStatus,
                              ProductID = p.ID,
                              ProductName = p.ProductName,
                              Gender = p.Gender,
                              StoreID = p.StoreAccountID ?? 0,
                              Locals = p.ProductDetails.Where(c => c.PStatus == Ecom.Core.Status.Active).Select(c => new ProductLocalVM
                              {
                                  Url = c.Url,
                                  ProductLocalLangID = c.ID,
                                  LangCode = c.Lang.LangCode,
                                  DetailHTML = c.DetailHTML,
                                  LangID = c.LangID,
                                  MetaDescription = c.MetaDescription,
                                  MetaKeywords = c.MetaKeywords,
                                  ProductName = c.ProductName,
                                  Summary = c.Summary,
                                  Title = c.Title

                              }).ToList(),
                              ProductCode = p.ProductCode,
                              SmallName = p.SmallName,
                              ReturnPolicyID = p.ReturnPolicyID,
                              CargoPolicyID = p.CargoPolicyID,
                              BrandID = p.BrandID,
                              ProductDepartmentID = p.ProductDepartmentID,
                              SeasonID = p.SeasonID,
                              DepartmentName = p.ProductDepartment != null ? p.ProductDepartment.Name : null,
                              StyleName = p.ProductStyle != null ? p.ProductStyle.Name : null,
                              ProductStyleID = p.ProductStyleID,
                              Prices = p.Prices.Where(c => c.PStatus == Ecom.Core.Status.Active).Select(c => new PriceVM
                              {
                                  CurrencyCode = c.PriceCurrency.CurrencyCode,
                                  CurrencySymbol = c.PriceCurrency.CurrencySymbol,
                                  Amount = c.Amount,
                                  AmountOld = c.AmountOld,
                                  PriceCurrencyID = c.PriceCurrencyID,
                                  VAT = c.VAT,
                                  ID = c.ID,

                              }).ToList(),
                              AllImages = p.ProductImages.OrderBy(c => c.SortNo).Select(i => new ProductImageVM { ImagePath = i.ImagePath, ID = i.ID, ColorID = i.ColorID, SortNo = i.SortNo }).ToList(),
                          }).FirstOrDefault();

                if (result != null)
                {
                    result.ColorID = result.AllImages.Select(c => c.ColorID).FirstOrDefault();

                    //if(result.Locals!=null)
                    // {
                    //     foreach (var item in result.Locals)
                    //     {
                    //         item.CargoPolicyHTML = GetPolicy(result.CargoPolicyID ?? 0, item.LangID ?? 0, storeId);
                    //         item.ReturnPolicyHTML = GetPolicy(result.ReturnPolicyID ?? 0, item.LangID ?? 0, storeId);
                    //     }
                    // }

                    MapProductImages(result);
                    MapImageVariants(result);

                    SetSizes(result, storeId);
                    SetSearchItems(result, storeId);
                    //SetCurrentImages(result);

                    //result.AllStocksStr = JsonConvert.SerializeObject(result.AllStocks);
                    //result.AllImagesStr = JsonConvert.SerializeObject(result.AllImages);

                    cache.Set(cacheKey, result, DateTime.Now.AddMinutes(20));
                }
            }

            return result;

            //string cacheKey = String.Concat("ProductsForIndex_", storeId, "_price_", priceCurrencyID, "_lang_", langId, "_productID_", productID);

            //List<ProductVM> products = cache.Get<List<ProductVM>>(cacheKey);

            //if (products == null || 1 == 2)
            //{
            //    products = (from pd in db.ProductDetails
            //                join p in db.Products on pd.ProductID equals p.ID
            //                //join ct in db.ProductInCatalogs on p.ID equals ct.ProductID
            //                join prc in db.Prices on p.ID equals prc.ProductID
            //                //where ct.CatalogID == catalogId
            //                //&& ct.StoreAccountID == storeId
            //                //&& 
            //                where
            //                p.StoreAccountID == storeId
            //                && pd.LangID == langId
            //                && prc.PriceCurrencyID == priceCurrencyID && prc.PStatus == Ecom.Core.Status.Active
            //                && p.PStatus == Ecom.Core.Status.Active
            //                //orderby ct.SortNo
            //                select new ProductVM
            //                {
            //                    PriceCurrencyId = priceCurrencyID,
            //                    ProductID = p.ID,
            //                    ProductName = pd.ProductName,
            //                    Gender = p.Gender,
            //                    ProductDepartmentID = p.ProductDepartmentID,
            //                    ProductStyleID = p.ProductStyleID,
            //                    Url = pd.Url,
            //                    ProductLocalLangID = pd.ID,
            //                    LangID = pd.LangID,
            //                    ProductCode = p.ProductCode,
            //                    SmallName = p.SmallName,

            //                    //CatalogSortNo = ct.SortNo,
            //                    //ColorID = ct.ColorID,
            //                    //ColorGlobalID = ct.Color != null ? ct.Color.ColorGlobalID : 0,

            //                    SizeVariants = (from szv in db.Stocks where szv.ProductID == p.ID && szv.PStatus == Ecom.Core.Status.Active group szv by new { szv.Size.ID, szv.Size.Name } into g select new SizeVM { SizeID = g.Key.ID, Name = g.Key.Name }).ToList(),
            //                    Prices = (from c in db.Prices
            //                              where c.ProductID == p.ID && c.PStatus == Ecom.Core.Status.Active //&& c.PriceCurrencyID == priceCurrencyID
            //                              select new PriceCreateEditVM
            //                              {
            //                                  CurrencyCode = c.PriceCurrency.CurrencyCode,
            //                                  CurrencySymbol = c.PriceCurrency.CurrencySymbol,
            //                                  PriceCurrencyID = c.PriceCurrencyID,
            //                                  Amount = c.Amount,
            //                                  AmountOld = c.AmountOld,
            //                              }).ToList(),

            //                    Images = (from i in db.ProductImages where i.ProductID == p.ID && i.PStatus == Ecom.Core.Status.Active select new ProductImageVM { ImagePath = i.ImagePath, ID = i.ID, ColorID = i.ColorID, SortNo = i.SortNo }).ToList(),
            //                    ColorVariants = (from szv in db.Stocks
            //                                     where szv.ProductID == p.ID && szv.PStatus == Ecom.Core.Status.Active
            //                                     group szv by new { szv.Color.ID, szv.Color.Name, szv.Color.HexValue, szv.Color.PatternPath } into g
            //                                     select new ProductImageVM
            //                                     {
            //                                         ColorHex = g.Key.HexValue,
            //                                         ColorHexPath = g.Key.PatternPath,
            //                                         ColorID = g.Key.ID,
            //                                         ColorName = g.Key.Name
            //                                         //ClassName = g.Key.ID == ct.ColorID ? "active" : ""
            //                                         ,
            //                                         ImagePath = (from i in db.ProductImages
            //                                                      orderby i.SortNo
            //                                                      where i.ProductID == p.ID && i.PStatus == Ecom.Core.Status.Active && i.ColorID == g.Key.ID
            //                                                      select i.ImagePath).FirstOrDefault()
            //                                         ,
            //                                         ImagePath2 = (from i in db.ProductImages
            //                                                       orderby i.SortNo
            //                                                       where i.ProductID == p.ID && i.PStatus == Ecom.Core.Status.Active && i.ColorID == g.Key.ID
            //                                                       select i.ImagePath).Skip(1).FirstOrDefault(),

            //                                     }).ToList(),

            //                }).Take(100).ToList();

            //    if (products != null)
            //    {
            //        cache.Set(cacheKey, products, DateTime.Now.AddMinutes(30));
            //    }

            //}

            //return products;
        }


        public List<FilterGroup> GetFilterItems(
         int storeId,
         int priceCurrencyID,
         int langId,
         int catalogId)
        {
            string cacheKey = String.Concat("FilterItems_store_", storeId, "_price_", priceCurrencyID, "_lang_", langId, "_catalog_", catalogId);
            var filterGroups = cache.Get<List<FilterGroup>>(cacheKey);
            if (filterGroups != null && 1 == 2) return filterGroups;

            var sizes = (from pd in db.ProductDetails
                         join p in db.Products on pd.ProductID equals p.ID
                         join ct in db.ProductInCatalogs on p.ID equals ct.ProductID
                         join st in db.Stocks on p.ID equals st.ProductID
                         join sz in db.Sizes on st.SizeID equals sz.ID
                         where ct.CatalogID == catalogId
                         && ct.StoreAccountID == storeId
                         && p.StoreAccountID == storeId
                         && pd.LangID == langId
                         && p.PStatus == Ecom.Core.Status.Active
                         && st.Quantity > 0
                         orderby ct.SortNo
                         group p by new
                         {

                             SizeId = sz.ID,
                             SizeName = sz.Name

                         } into g
                         select new
                         {
                             FilterType = "sz",
                             Id = g.Key.SizeId,
                             Name = g.Key.SizeName,
                         }).ToList();

            var colors = (from pd in db.ProductDetails
                          join p in db.Products on pd.ProductID equals p.ID
                          join ct in db.ProductInCatalogs on p.ID equals ct.ProductID
                          join st in db.Stocks on p.ID equals st.ProductID
                          join cl in db.Colors on st.ColorID equals cl.ID
                          join gl in db.ColorGlobals on cl.ColorGlobalID equals gl.ID
                          where ct.CatalogID == catalogId
                          && ct.StoreAccountID == storeId
                          && p.StoreAccountID == storeId
                          && pd.LangID == langId
                          && p.PStatus == Ecom.Core.Status.Active && cl.PStatus == Core.Status.Active
                          && st.Quantity > 0
                          orderby ct.SortNo
                          group p by new
                          {

                              ColorId = gl.ID,
                              ColorName = gl.Name,
                              Hex = gl.HexValue

                          } into g
                          select new
                          {
                              FilterType = "cl",
                              Id = g.Key.ColorId,
                              Name = g.Key.ColorName,
                              ColorHex = g.Key.Hex
                          }).ToList();


            var styles = (from pd in db.ProductDetails
                          join p in db.Products on pd.ProductID equals p.ID
                          join ct in db.ProductInCatalogs on p.ID equals ct.ProductID
                          join sty in db.ProductStyles on p.ProductStyleID equals sty.ID
                          where ct.CatalogID == catalogId
                          && ct.StoreAccountID == storeId
                          && p.StoreAccountID == storeId
                          && pd.LangID == langId
                          && p.PStatus == Ecom.Core.Status.Active
                          orderby ct.SortNo
                          group p by new
                          {

                              StyleID = sty.ID,
                              StyleName = sty.Name,

                          } into g
                          select new
                          {
                              FilterType = "s",
                              Id = g.Key.StyleID,
                              Name = g.Key.StyleName,
                          }).ToList();


            var departments = (from pd in db.ProductDetails
                               join p in db.Products on pd.ProductID equals p.ID
                               join ct in db.ProductInCatalogs on p.ID equals ct.ProductID
                               join dep in db.ProductDepartments on p.ProductDepartmentID equals dep.ID
                               where ct.CatalogID == catalogId
                               && ct.StoreAccountID == storeId
                               && p.StoreAccountID == storeId
                               && pd.LangID == langId
                               && p.PStatus == Ecom.Core.Status.Active
                               orderby ct.SortNo
                               group p by new
                               {

                                   DepartmentID = dep.ID,
                                   DepartmentName = dep.Name,

                               } into g
                               select new
                               {
                                   FilterType = "d",
                                   Id = g.Key.DepartmentID,
                                   Name = g.Key.DepartmentName,
                               }).ToList();

            filterGroups = new List<FilterGroup>();

            filterGroups.Add(new FilterGroup
            {
                Name = "Grid",
                FilterType = "g",
                Items = new List<FilterItem>
                {
                     new FilterItem{ Id=6, IsSelected=true, Name= "6" },
                      new FilterItem{ Id=12, IsSelected=false, Name= "12" },
                       new FilterItem{ Id=24, IsSelected=false, Name= "24" }
                }
            });

            if (sizes.Any())
            {
                filterGroups.Add(new FilterGroup
                {
                    Name = "Size",
                    FilterType = "sz",
                    Items = sizes?.Select(c => new FilterItem { Id = c.Id, Name = c.Name }).ToList()
                });
            }

            if (colors.Any())
            {
                filterGroups.Add(new FilterGroup
                {
                    Name = "Color",
                    FilterType = "cl",
                    Items = colors?.Select(c => new FilterItem { Id = c.Id, Name = c.Name, ColorHex = c.ColorHex }).ToList()
                });
            }

            if (departments.Any())
            {
                filterGroups.Add(new FilterGroup
                {
                    Name = "Department",
                    FilterType = "d",
                    Items = departments?.Select(c => new FilterItem { Id = c.Id, Name = c.Name }).ToList()
                });
            }

            if (styles.Any())
            {
                filterGroups.Add(new FilterGroup
                {
                    Name = "Style",
                    FilterType = "s",
                    Items = styles?.Select(c => new FilterItem { Id = c.Id, Name = c.Name }).ToList()
                });
            }

            if (filterGroups != null)
            {
                cache.Set(cacheKey, filterGroups, DateTime.Now.AddHours(4));
            }


            return filterGroups;
        }

        private void MapPricesAndImages(PagedOperationListResult<ProductVM> operationResult)
        {
            if (operationResult.Result != null && operationResult.Result.Count > 0)
            {
                Dictionary<int, PriceCreateEditVM> prices = new Dictionary<int, PriceCreateEditVM>();

                foreach (var prd in operationResult.Result)
                {
                    if (prd.Prices == null || prd.Prices.Count == 0)
                    {
                        var pricesOftheSameProduct = operationResult.Result.Where(c => c.ProductID == prd.ProductID && c.Prices != null && c.Prices.Count > 0).Select(c => c.Prices).FirstOrDefault();
                        prd.Prices = pricesOftheSameProduct;
                    }

                    if (prd.Images == null || prd.Images.Count == 0)
                    {
                        var imagesOftheSameProduct = operationResult.Result.Where(c => c.ProductID == prd.ProductID && c.Images != null && c.Images.Count > 0).Select(c => c.Images).FirstOrDefault();
                        prd.Images = imagesOftheSameProduct;

                    }

                }
            }
        }


        private void MapProductImages(ProductXVM product)
        {
            if (product == null || product.AllImages == null) return;

            Dictionary<int, ColorCreateEditVM> colors = new Dictionary<int, ColorCreateEditVM>();

            foreach (var item in product.AllImages)
            {

                if (item.ColorID.HasValue)
                {
                    if (colors.ContainsKey(item.ColorID.Value))
                    {
                        var color = colors[item.ColorID.Value];

                        item.ColorName = color?.Name;
                        item.ColorHexPath = color?.PatternPath;
                        item.ColorHex = color?.HexValue;
                        if (item.ColorID == product.ColorID) item.ClassName = "active";
                    }
                    else
                    {
                        var color = db.Colors.Where(x => x.ID == item.ColorID).Select(c => new ColorCreateEditVM { Name = c.Name, HexValue = c.HexValue, PatternPath = c.PatternPath }).FirstOrDefault();
                        if (color != null)
                        {
                            item.ColorName = color?.Name;
                            item.ColorHexPath = color?.PatternPath;
                            item.ColorHex = color?.HexValue;
                            colors.Add(item.ColorID.Value, color);
                            if (item.ColorID == product.ColorID) item.ClassName = "active";
                        }
                    }

                }

            }
        }
        private void MapImageVariants(ProductXVM product)
        {
            if (product == null || product.AllImages == null || product.AllImages.Count <= 0)
            {
                product.ColorVariants = new List<ProductImageVM>();
                return;
            }

            Dictionary<int, string> colors = new Dictionary<int, string>();

            if (product.AllImages != null && product.AllImages.Count > 0)
            {
                product.ColorVariants =
                  (from c in product.AllImages
                   group c by new
                   {
                       c.ColorID,
                       c.ColorName,
                       c.ColorHex,
                       c.ColorHexPath

                   } into g
                   select new ProductImageVM
                   {
                       ColorID = g.Key.ColorID,
                       ColorName = g.Key.ColorName,
                       ColorHex = g.Key.ColorHex,
                       ColorHexPath = g.Key.ColorHexPath

                   }).ToList();

                foreach (var item in product.ColorVariants)
                {
                    item.ImagePath = product.AllImages.Where(c => c.ColorID == item.ColorID).Select(c => c.ImagePath).FirstOrDefault();
                    item.ImagePath2 = product.AllImages.Where(c => c.ColorID == item.ColorID).Skip(1).Select(c => c.ImagePath).FirstOrDefault();
                    if (item.ColorID != null && item.ColorID == product.ColorID) item.ClassName = "active";
                }
            }

        }

        private void MapProductImages(ProductDTVM product)
        {
            if (product == null || product.AllImages == null) return;

            Dictionary<int, ColorCreateEditVM> colors = new Dictionary<int, ColorCreateEditVM>();

            foreach (var item in product.AllImages)
            {

                if (item.ColorID.HasValue)
                {
                    if (colors.ContainsKey(item.ColorID.Value))
                    {
                        var color = colors[item.ColorID.Value];

                        item.ColorName = color?.Name;
                        item.ColorHexPath = color?.PatternPath;
                        item.ColorHex = color?.HexValue;
                        if (item.ColorID == product.CurrentColorID) item.ClassName = "active";
                    }
                    else
                    {
                        var color = db.Colors.Where(x => x.ID == item.ColorID).Select(c => new ColorCreateEditVM { Name = c.Name, HexValue = c.HexValue, PatternPath = c.PatternPath }).FirstOrDefault();
                        if (color != null)
                        {
                            item.ColorName = color?.Name;
                            item.ColorHexPath = color?.PatternPath;
                            item.ColorHex = color?.HexValue;
                            colors.Add(item.ColorID.Value, color);
                            if (item.ColorID == product.CurrentColorID) item.ClassName = "active";
                        }
                    }

                }

            }
        }
        private void MapImageVariants(ProductDTVM product)
        {
            if (product == null || product.AllImages == null || product.AllImages.Count <= 0)
            {
                product.ColorVariants = new List<ProductImageVM>();
                return;
            }

            Dictionary<int, string> colors = new Dictionary<int, string>();

            if (product.AllImages != null && product.AllImages.Count > 0)
            {
                product.ColorVariants =
                  (from c in product.AllImages
                   group c by new
                   {
                       c.ColorID,
                       c.ColorName,
                       c.ColorHex,
                       c.ColorHexPath

                   } into g
                   select new ProductImageVM
                   {
                       ColorID = g.Key.ColorID,
                       ColorName = g.Key.ColorName,
                       ColorHex = g.Key.ColorHex,
                       ColorHexPath = g.Key.ColorHexPath

                   }).ToList();

                foreach (var item in product.ColorVariants)
                {
                    item.ImagePath = product.AllImages.Where(c => c.ColorID == item.ColorID).Select(c => c.ImagePath).FirstOrDefault();
                    item.ImagePath2 = product.AllImages.Where(c => c.ColorID == item.ColorID).Skip(1).Select(c => c.ImagePath).FirstOrDefault();
                    if (item.ColorID != null && item.ColorID == product.CurrentColorID) item.ClassName = "active";
                }
            }

        }

        private void MapImageVariants(PagedOperationListResult<ProductVM> operationResult)
        {
            if (operationResult.Result != null && operationResult.Result.Count > 0)
            {
                Dictionary<int, string> colors = new Dictionary<int, string>();

                foreach (var prd in operationResult.Result)
                {
                    if (prd.Images != null && prd.Images.Count > 0)
                    {
                        prd.ColorVariants =
                              (from c in prd.Images
                               group c by new
                               {
                                   c.ColorID,
                                   c.ColorName,
                                   c.ColorHex,
                                   c.ColorHexPath

                               } into g
                               select new ProductImageVM
                               {
                                   ColorID = g.Key.ColorID,
                                   ColorName = g.Key.ColorName,
                                   ColorHex = g.Key.ColorHex,
                                   ColorHexPath = g.Key.ColorHexPath

                               }).ToList();

                        foreach (var item in prd.ColorVariants)
                        {
                            item.ImagePath = prd.Images.Where(c => c.ColorID == item.ColorID).Select(c => c.ImagePath).FirstOrDefault();
                            item.ImagePath2 = prd.Images.Where(c => c.ColorID == item.ColorID).Skip(1).Select(c => c.ImagePath).FirstOrDefault();
                            if (item.ColorID != null && item.ColorID == prd.ColorID) item.ClassName = "active";
                        }
                    }
                }
            }
        }

        private void MapProductImages(PagedOperationListResult<ProductVM> operationResult)
        {
            if (operationResult.Result != null && operationResult.Result.Count > 0)
            {
                Dictionary<int, ColorCreateEditVM> colors = new Dictionary<int, ColorCreateEditVM>();

                foreach (var prd in operationResult.Result)
                {
                    if (prd.Images != null && prd.Images.Count > 0)
                    {
                        foreach (var item in prd.Images)
                        {
                            if (item.ColorID.HasValue)
                            {
                                if (colors.ContainsKey(item.ColorID.Value))
                                {
                                    var color = colors[item.ColorID.Value];

                                    item.ColorName = color?.Name;
                                    item.ColorHexPath = color?.PatternPath;
                                    item.ColorHex = color?.HexValue;
                                    if (item.ColorID == prd.ColorID) item.ClassName = "active";

                                }
                                else
                                {
                                    var color = db.Colors.Where(x => x.ID == item.ColorID).Select(c => new ColorCreateEditVM { Name = c.Name, HexValue = c.HexValue, PatternPath = c.PatternPath }).FirstOrDefault();

                                    if (color != null)
                                    {
                                        item.ColorName = color?.Name;
                                        item.ColorHexPath = color?.PatternPath;
                                        item.ColorHex = color?.HexValue;
                                        colors.Add(item.ColorID.Value, color);
                                        if (item.ColorID == prd.ColorID) item.ClassName = "active";
                                    }
                                }

                            }
                        }
                    }

                }
            }
        }

        public bool GetProductForRoute(int storeId, string url, int? langId)
        {
            bool result = false;
            if (String.IsNullOrWhiteSpace(url)) return false;

            string cacheKey = String.Concat("ProductForRoute_", url, "_", storeId, "_", langId ?? 0);
            result = cache.Get<bool>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (result == false)
            {
                var curProductId = 0;
                url = '/' + (url.TrimStart('/'));

                if (langId.HasValue && langId > 0)
                {
                    curProductId = db.ProductDetails.Where(c => c.Url == url && c.StoreAccountID == storeId && c.LangID == langId).Select(c => c.ID).FirstOrDefault();
                }
                else
                {
                    curProductId = db.ProductDetails.Where(c => c.Url == url && c.StoreAccountID == storeId).Select(c => c.ID).FirstOrDefault();
                }

                if (curProductId != 0)
                {
                    result = true;
                    cache.Set(cacheKey, result, DateTime.Now.AddMinutes(60));
                }
            }

            return result;

        }


        public string GetProductLangCodeForRoute(int storeId, string url, int? langId)
        {
            string result = "";
            if (String.IsNullOrWhiteSpace(url)) return null;

            string cacheKey = String.Concat("Product_Lang_ForRoute_", url, "_", storeId, "_", langId ?? 0);
            result = cache.Get<string>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (result == null)
            {
                url = '/' + (url.TrimStart('/'));

                if (langId.HasValue && langId > 0)
                {
                    result = db.ProductDetails.Where(c => c.Url == url && c.StoreAccountID == storeId && c.LangID == langId).Select(c => c.Lang.LangCode).FirstOrDefault();
                }
                else
                {
                    result = db.ProductDetails.Where(c => c.Url == url && c.StoreAccountID == storeId && c.LangID != null).Select(c => c.Lang.LangCode).FirstOrDefault();
                }

                if (result != null)
                {
                    cache.Set(cacheKey, result, DateTime.Now.AddMinutes(60));
                }
            }

            return result;

        }


        public string GetPolicy(int policyID, int langId, int storeId)
        {
            if (policyID == 0 || storeId == 0 || langId == 0) return null;

            string cacheKey = $"PolicyString_By_ID_{policyID}-{storeId}-{langId}";

            var policyStr = cache.Get<string>(cacheKey);

            if (policyStr == null)
            {
                policyStr = db.PolicyLocalLangs.Where(x => x.LangID == langId
                && x.StoreAccountID == storeId && x.LangID == langId && x.PolicyID == policyID
                && x.PStatus == Ecom.Core.Status.Active).OrderBy(x => x.ID).Select(x =>
                x.DetailHTML).FirstOrDefault();

                if (policyStr != null)
                {
                    cache.Set(cacheKey, policyStr, DateTime.Now.AddHours(1));
                }
            }

            return policyStr;
        }

        public ProductDTVM GetProductDetails(int storeId, int? productId, int? langId, int priceCurrencyId, int? firstColorId = null, string url = null)
        {
            ProductDTVM result = null;
            if (String.IsNullOrWhiteSpace(url) && !productId.HasValue) return null;

            string cacheKey = String.Concat("ProductDetail_", productId ?? 0, "_", url, "_", storeId, "_", langId ?? 0);
            result = cache.Get<ProductDTVM>(cacheKey);

            //TODO:consider to open cache but invalidation is required
            if (result == null || true)
            {

                url = !string.IsNullOrWhiteSpace(url) ? '/' + (url.TrimStart('/')) : null;

                var query = (from pd in db.ProductDetails
                             join p in db.Products on pd.ProductID equals p.ID
                             where (pd.ProductID == productId || pd.Url == url)
                             && pd.StoreAccountID == storeId
                             && (langId == null || pd.LangID == langId)
                             && pd.PStatus == Ecom.Core.Status.Active
                             && p.PStatus == Ecom.Core.Status.Active
                             orderby pd.ID descending
                             select new ProductDTVM
                             {
                                 ProductID = p.ID,
                                 ProductName = pd.ProductName,
                                 Gender = p.Gender,
                                 Url = pd.Url,
                                 ProductLocalLangID = pd.ID,
                                 LangID = pd.LangID,
                                 ProductCode = p.ProductCode,
                                 SmallName = p.SmallName,
                                 ReturnPolicyID = p.ReturnPolicyID,
                                 CargoPolicyID = p.CargoPolicyID,
                                 BrandID = p.BrandID,
                                 CanonicalLink = pd.CanonicalLink,
                                 DetailHTML = pd.DetailHTML,
                                 MetaDescription = pd.MetaDescription,
                                 MetaKeywords = pd.MetaKeywords,
                                 ProductDepartmentID = p.ProductDepartmentID,
                                 SeasonID = p.SeasonID,
                                 Title = pd.Title,
                                 Summary = pd.Summary,
                                 ProductStyleID = p.ProductStyleID,
                                 //CatalogSortNo = ct.SortNo,
                                 //ColorID = ct.ColorID,
                                 Prices = p.Prices.Where(c => c.PStatus == Ecom.Core.Status.Active && c.PriceCurrencyID == priceCurrencyId).Select(c => new PriceCreateEditVM
                                 {
                                     CurrencyCode = c.PriceCurrency.CurrencyCode,
                                     CurrencySymbol = c.PriceCurrency.CurrencySymbol,
                                     Amount = c.Amount,
                                     AmountOld = c.AmountOld,
                                 }).ToList(),
                                 AllImages = p.ProductImages.OrderBy(c => c.SortNo).Select(i => new ProductImageVM { ImagePath = i.ImagePath, ID = i.ID, ColorID = i.ColorID, SortNo = i.SortNo }).ToList(),
                             });

                result = query.FirstOrDefault();

                //MapProductImages(operationResult);
                //MapPricesAndImages(operationResult);

                if (result != null)
                {
                    result.CurrentColorID = !firstColorId.HasValue ? result.AllImages.Select(c => c.ColorID).FirstOrDefault() : firstColorId;

                    MapProductImages(result);
                    MapImageVariants(result);

                    SetSizes(result, storeId);
                    SetCurrentImages(result);

                    result.AllStocksStr = JsonConvert.SerializeObject(result.AllStocks);
                    result.AllImagesStr = JsonConvert.SerializeObject(result.AllImages);

                    result.CargoPolicyHTML = GetPolicy(result.CargoPolicyID ?? 0, langId ?? 0, storeId);
                    result.ReturnPolicyHTML = GetPolicy(result.ReturnPolicyID ?? 0, langId ?? 0, storeId);

                    cache.Set(cacheKey, result, DateTime.Now.AddMinutes(60));
                }
            }

            return result;

        }

        //private void SetCurrentImages(ProductXVM result)
        //{
        //    if (result.ColorVariants != null && result.ColorVariants.Any())
        //    {
        //        result.CurrentImages = result.AllImages.Where(c => c.ColorID == result.ColorID).ToList();
        //    }
        //}

        private void SetCurrentImages(ProductDTVM result)
        {
            if (result.ColorVariants != null && result.ColorVariants.Any())
            {
                result.CurrentImages = result.AllImages.Where(c => c.ColorID == result.CurrentColorID).ToList();
            }
        }


        private void SetSearchItems(ProductXVM result, int storeId)
        {
            if (result != null)
            {
                result.SearchItems += ((result.Locals?.Select(x => x.ProductName).Distinct().Aggregate((current, next) => current + "," + next) ?? "") + ", ");
                result.SearchItems += ((result.ColorVariants?.Select(x => x.ColorGlobalName).Distinct().Aggregate((current, next) => current + "," + next) ?? "") + ", ");
                result.SearchItems += ((result.ColorVariants?.Select(x => x.ColorName).Distinct().Aggregate((current, next) => current + "," + next) ?? "") + ", ");
                result.SearchItems += ((result.SizeVariants?.Select(x => x.Name).Distinct().Aggregate((current, next) => current + "," + next) ?? "") + ", ");
                result.SearchItems += ((result.DepartmentName ?? "") + ", ");
                result.SearchItems += ((result.StyleName ?? "") + ", ");
                result.SearchItems = result.SearchItems.Replace(", , ,", ",");
                result.SearchItems = result.SearchItems.Replace(", ,", ",");
                result.SearchItems = result.SearchItems.Replace(",,,,", ",");
                result.SearchItems = result.SearchItems.Replace(",,,", ",");
                result.SearchItems = result.SearchItems.Replace(",,", ",");
            }

        }

        private void SetSizes(ProductXVM result, int storeId)
        {
            if (result.ColorVariants != null && result.ColorVariants.Any())
            {

                var allStocks = db.Stocks.AsNoTracking().Include(c => c.Size).Include(w => w.Color).Where(c => c.ProductID == result.ProductID && c.StoreAccountID == storeId).ToList();

                //TODO:change to Allstocks
                result.AllStocks = allStocks.Where(c => c.ProductID == result.ProductID && c.StoreAccountID == storeId)
               .Select(c => new
                StockVM
               {
                   IsActive = c.Quantity > 0,
                   SizeName = c.Size?.Name,
                   ColorName = c.Color?.Name,
                   SizeID = c.Size?.ID,
                   StockID = c.ID,
                   Barkod = c.Barkod,
                   Quantity = c.Quantity,
                   ColorID = c.Color?.ID
               }).ToList();

                result.Sizes = allStocks.Where(c => c.Quantity > 0 && c.SizeID != null).Select(c => c.SizeID ?? 0).ToList();

                result.SizeVariants = allStocks.Where(c => c.ProductID == result.ProductID && c.SizeID != null && c.StoreAccountID == storeId)
               .Select(c => new
                SizeVM
               {
                   IsActive = c.Quantity > 0,
                   Name = c.Size.Name,
                   SizeID = c.Size.ID,
                   ColorID = c.Color?.ID
               }).ToList();

            }
        }

        private void SetSizes(ProductDTVM result, int storeId)
        {
            if (result.ColorVariants != null && result.ColorVariants.Any())
            {

                var allStocks = db.Stocks.Include(c => c.Size).Include(w => w.Color).Where(c => c.ProductID == result.ProductID && c.StoreAccountID == storeId).ToList();

                //TODO:change to Allstocks
                result.AllStocks = allStocks.Where(c => c.ProductID == result.ProductID && c.StoreAccountID == storeId)
               .Select(c => new
                StockVM
               {
                   IsActive = c.Quantity > 0,
                   SizeName = c.Size?.Name,
                   ColorName = c.Color?.Name,
                   SizeID = c.Size?.ID,
                   StockID = c.ID,
                   Barkod = c.Barkod,
                   Quantity = c.Quantity,
                   ColorID = c.Color?.ID
               }).ToList();


                result.CurrentSizes = allStocks.Where(c => c.ProductID == result.ProductID && c.SizeID != null && c.ColorID == result.CurrentColorID && c.StoreAccountID == storeId)
               .Select(c => new
                SizeVM
               {
                   IsActive = c.Quantity > 0,
                   Name = c.Size.Name,
                   SizeID = c.Size.ID,
                   ColorID = c.Color?.ID
               }).ToList();

            }
        }
    }
}
