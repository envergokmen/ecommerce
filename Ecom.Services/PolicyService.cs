﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Policies;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class PolicyService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public PolicyService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<PolicyCreateEditVM> GetAll(int storeId, PoliciyType? type, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<PolicyCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Policies
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active
                                   && (type == null || p.PolicyType == type)
                                   &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Policies
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active
                                         && (type == null || p.PolicyType == type)
                                          && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new PolicyCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              PolicyType = p.PolicyType,
                                              Translations = p.PolicyLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Policies
                                  where p.PStatus == Status.Active
                                   && (type == null || p.PolicyType == type) &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.Policies.Where(x => x.StoreAccountID == storeId
                && (type == null || x.PolicyType == type)
                && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                    new PolicyCreateEditVM
                    {
                        ID = p.ID,
                        Name = p.Name,
                        PStatus = p.PStatus,
                        PolicyType = p.PolicyType,
                        Translations = p.PolicyLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                    }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<PolicyCreateEditVM> Get(int id, int storeId, PoliciyType? type)
        {
            var result = new OperationResult<PolicyCreateEditVM>();

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new PolicyCreateEditVM();
                SetListItems(result, Status.Active);
                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
                if (type != null)
                {
                    result.Result.PolicyType = type.Value;
                }
                return result;
            }

            result.Result = db.Policies.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new PolicyCreateEditVM
             {
                 ID = p.ID,
                 SortNo = p.SortNo ?? 0,
                 Name = p.Name,
                 DefaultPolicy = p.DefaultPolicy,
                 PolicyType = p.PolicyType,
                 PStatus = p.PStatus,
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                SetListItems(result, result.Result.PStatus);

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        private void SetListItems(OperationResult<PolicyCreateEditVM> result, Status status)
        {

            var allStatuses = serviceUtils.GetStatusValues(1);
            var allIsDefaultValues = serviceUtils.ToSelectListInt<YesNo>(-1);
            var allPolicyTypes = serviceUtils.ToSelectListInt<PoliciyType>(-1);

            result.Result.AllStatuses = allStatuses;
            result.Result.AllIsDefaults = allIsDefaultValues;
            result.Result.AllPolicyTypes = allPolicyTypes;
            result.Result.PStatus = status;
        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var PolicyType = db.Policies.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (PolicyType != null)
            {
                PolicyType.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.PolicyTypeDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Policy MapToDBObject(PolicyCreateEditVM model, Policy dbModel)
        {
            var PolicyType = dbModel ?? new Policy();

            PolicyType.ID = model.ID;

            PolicyType.Name = model.Name;
            PolicyType.PStatus = model.PStatus;
            PolicyType.SortNo = model.SortNo;
            PolicyType.DefaultPolicy = model.DefaultPolicy;
            PolicyType.PolicyType = model.PolicyType;

            PolicyType.StoreAccountID = model.ID == 0 ? model.StoreID : PolicyType.StoreAccountID;
            PolicyType.ModifiedOn = DateTime.Now;
            if (PolicyType.ID != 0) PolicyType.CreatedOn = DateTime.Now;

            return PolicyType;

        }

        private PolicyCreateEditVM MapToVM(Policy dbModel)
        {
            if (dbModel == null) return null;

            var PolicyType = new PolicyCreateEditVM();
            PolicyType.ID = dbModel.ID;
            PolicyType.Name = dbModel.Name;
            PolicyType.PStatus = dbModel.PStatus;
            PolicyType.SortNo = dbModel.SortNo ?? 0;
            PolicyType.DefaultPolicy = dbModel.DefaultPolicy;
            PolicyType.PolicyType = dbModel.PolicyType;

            return PolicyType;
        }

        public OperationResult<PolicyCreateEditVM> Save(PolicyCreateEditVM PolicyType)
        {
            var result = new OperationResult<PolicyCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(PolicyType, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Policy existing = null;

            if (PolicyType.ID != 0)
            {
                existing = db.Policies.Where(x => x.ID == PolicyType.ID).FirstOrDefault();
                existing = MapToDBObject(PolicyType, existing);
            }
            else
            {
                existing = MapToDBObject(PolicyType, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(PolicyType.ID == 0 ? QueueItemType.PolicyTypeCreated : QueueItemType.PolicyTypeUpdated, JsonConvert.SerializeObject(result.Result), PolicyType.StoreID);

            return result;

        }


    }
}
