﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Domains;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class DomainService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public DomainService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }

        public DomainViewModel GetDomainByName(string domainName)
        {
            string cacheKey = $"Domain_By_Name_{domainName}";
            var domainId = cache.Get<Int32>(cacheKey);
            if (domainId == 0)
            {
                domainId = db.Domains.Where(x => x.Name == domainName && x.PStatus == Status.Active).OrderBy(x => x.Priority).Select(x => x.ID).FirstOrDefault();

                if (domainId != 0)
                {
                    cache.Set(cacheKey, domainId, DateTime.Now.AddHours(6));
                }
            }

            return GetDomainById(domainId);
        }

        public DomainViewModel GetDomainByNameAndLang(string domainName, string langCode)
        {
            string cacheKey = $"Domain_By_Name_{domainName}_lang_{langCode}";

            var domainId = cache.Get<Int32>(cacheKey);

            if (domainId == 0)
            {
                domainId = db.Domains.Where(x => x.Name == domainName && x.Lang.LangCode == langCode && x.PStatus == Status.Active).OrderBy(x => x.Priority).Select(x => x.ID).FirstOrDefault();

                if (domainId != 0)
                {
                    cache.Set(cacheKey, domainId, DateTime.Now.AddHours(6));
                }
            }

            return GetDomainById(domainId);
        }

        public DomainViewModel GetDefaultDomain()
        {
            string cacheKey = $"Def_Domain";
            var domainId = cache.Get<Int32>(cacheKey);

            if (domainId == 0)
            {
                domainId = db.Domains.Where(x => x.PStatus == Status.Active).Select(x => x.ID).FirstOrDefault();

                if (domainId != 0)
                {
                    cache.Set(cacheKey, domainId, DateTime.Now.AddHours(6));
                }
            }

            return GetDomainById(domainId);
        }

        public DomainViewModel GetDomainById(int domainId)
        {
            string cacheKey = $"Domain_{domainId}";

            var domain = cache.Get<DomainViewModel>(cacheKey);

            if (domain == null)
            {
                domain = db.Domains
                    .Where(x => x.ID == domainId && x.PStatus == Status.Active)
                    .Select(x => new DomainViewModel
                    {
                        AllowChangeCurrency = x.AllowChangeCurrency,
                        AllowChangeLang = x.AllowChangeLang,
                        DepotID = x.DepotID,
                        StoreAccountID = x.StoreAccountID,
                        DomainBrandName = x.DomainBrandName,
                        DomainSlogan = x.DomainSlogan,
                        DomainType = x.DomainType,
                        PriceCurrencyCode = x.PriceCurrency.CurrencyCode,
                        PriceCurrencySymbol = x.PriceCurrency.CurrencySymbol,
                        PriceCurrencyID = x.PriceCurrencyID ?? 0,
                        LangCode = x.Lang.LangCode,
                        LangID = x.Lang.ID,
                        ID = x.ID,
                        Name = x.Name

                    }).FirstOrDefault();

                if (domain != null)
                {
                    cache.Set(cacheKey, domain, DateTime.Now.AddHours(6));
                }
            }

            return domain;
        }



        public PagedOperationListResult<DomainCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<DomainCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Domains
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Domains
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.Priority
                                          select new DomainCreateEditVM
                                          {
                                              ID = p.ID,
                                              AllowChangeCurrency = p.AllowChangeCurrency,
                                              AllowChangeLang = p.AllowChangeLang,
                                              Priority = p.Priority,
                                              DepotID = p.DepotID,
                                              DomainBrandName = p.DomainBrandName,
                                              LangID = p.LangID,
                                              PriceCurrencyID = p.PriceCurrencyID,

                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              LangCode = p.Lang.LangCode,
                                              CurrencyCode = p.PriceCurrency.CurrencyCode
                                          })
                      .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Domains
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.Domains.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.Priority).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new DomainCreateEditVM
                  {
                      ID = p.ID,
                      AllowChangeCurrency = p.AllowChangeCurrency,
                      AllowChangeLang = p.AllowChangeLang,
                      Priority = p.Priority,
                      DepotID = p.DepotID,
                      DomainBrandName = p.DomainBrandName,
                      LangID = p.LangID,
                      PriceCurrencyID = p.PriceCurrencyID,

                      Name = p.Name,
                      PStatus = p.PStatus,
                      LangCode = p.Lang.LangCode,
                      CurrencyCode = p.PriceCurrency.CurrencyCode
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<DomainCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<DomainCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new DomainCreateEditVM();
                result.Result.AllLangs = allLangs;
                result.Result.AllStatuses = allStatuses;
                result.Result.AllCurrencies = allCurrencies;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Domains.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new DomainCreateEditVM
             {
                 ID = p.ID,
                 AllowChangeCurrency = p.AllowChangeCurrency,
                 AllowChangeLang = p.AllowChangeLang,
                 Priority = p.Priority,
                 DepotID = p.DepotID,
                 DomainBrandName = p.DomainBrandName,
                 LangID = p.LangID,
                 PriceCurrencyID = p.PriceCurrencyID,
                 Name = p.Name,
                 PStatus = p.PStatus,
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllCurrencies = allCurrencies;
                result.Result.AllLangs = allLangs;
                result.Result.AllStatuses = allStatuses;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var domain = db.Domains.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (domain != null)
            {
                domain.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.DomainDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Domain MapToDBObject(DomainCreateEditVM model, Domain dbModel)
        {
            var domain = dbModel ?? new Domain();

            domain.ID = model.ID;

            domain.Name = model.Name;
            domain.PStatus = model.PStatus;
            domain.Priority = model.Priority;
            domain.AllowChangeCurrency = model.AllowChangeCurrency;
            domain.AllowChangeLang = model.AllowChangeLang;
            domain.DepotID = model.DepotID;
            domain.DomainBrandName = model.DomainBrandName;
            domain.LangID = model.LangID ?? 0;
            domain.PriceCurrencyID = model.PriceCurrencyID ?? 0;
            domain.Priority = model.Priority;
            domain.Priority = model.Priority;

            domain.StoreAccountID = model.ID == 0 ? model.StoreID : domain.StoreAccountID;
            domain.ModifiedOn = DateTime.Now;
            if (domain.ID != 0) domain.CreatedOn = DateTime.Now;

            return domain;

        }

        private DomainCreateEditVM MapToVM(Domain dbModel)
        {
            if (dbModel == null) return null;

            var domain = new DomainCreateEditVM();
            domain.ID = dbModel.ID;
            domain.Name = dbModel.Name;
            domain.PStatus = dbModel.PStatus;
            domain.Priority = dbModel.Priority;
            domain.AllowChangeCurrency = dbModel.AllowChangeCurrency;
            domain.AllowChangeLang = dbModel.AllowChangeLang;
            domain.DepotID = dbModel.DepotID;
            domain.DomainBrandName = dbModel.DomainBrandName;
            domain.LangID = dbModel.LangID;
            domain.PriceCurrencyID = dbModel.PriceCurrencyID;
            domain.Priority = dbModel.Priority;
            domain.Priority = dbModel.Priority;

            return domain;
        }

        public OperationResult<DomainCreateEditVM> Save(DomainCreateEditVM domain)
        {
            var result = new OperationResult<DomainCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(domain, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Domain existing = null;

            if (domain.ID != 0)
            {
                existing = db.Domains.Where(x => x.ID == domain.ID).FirstOrDefault();
                existing = MapToDBObject(domain, existing);
            }
            else
            {
                existing = MapToDBObject(domain, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(domain.ID == 0 ? QueueItemType.DomainCreated : QueueItemType.DomainUpdated, JsonConvert.SerializeObject(result.Result), domain.StoreID);

            return result;

        }


    }
}
