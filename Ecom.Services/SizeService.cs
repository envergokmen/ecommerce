﻿using Ecom.Core;
using Ecom.Models;
using Ecom.ViewModels.Sizes;
using System;
using System.Linq;
using Microsoft.Extensions.Localization;
using Ecom.Services.Bus;
using Newtonsoft.Json;

namespace Ecom.Services
{
    public class SizeService
    {

        private readonly EcomContext db;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        public SizeService(
            IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            EcomContext _db,
            RabbitMQService _rabbitMQService
            )
        {
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            this.db = _db;

        }

        public PagedOperationListResult<SizeCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<SizeCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Sizes
                                  where
                                   p.PStatus == Status.Active &&
                                   p.StoreAccountID == storeId &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Sizes
                                          where
                                          p.PStatus == Status.Active &&
                                          p.StoreAccountID == storeId 
                                          && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new SizeCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              SortNo = p.SortNo

                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Sizes
                                  where p.StoreAccountID==storeId && p.PStatus == Status.Active
                                  select p).Count();

                operationResult.Result = db.Sizes.Where(x => x.StoreAccountID == storeId && x.PStatus== Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                 new SizeCreateEditVM
                 {
                     ID = p.ID,
                     Name = p.Name,
                     PStatus = p.PStatus,
                     SortNo = p.SortNo
                 }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<SizeCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<SizeCreateEditVM>();

            result.Result = db.Sizes.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p => new SizeCreateEditVM
            {
                ID = p.ID,
                Name = p.Name,
                PStatus = p.PStatus,
                SortNo = p.SortNo
            }).FirstOrDefault();

            if (result.Result != null)
            {
                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var brand = db.Sizes.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (brand != null)
            {
                brand.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.SizeDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Size MapToDBObject(SizeCreateEditVM model, Size dbModel)
        {
            var brand = dbModel ?? new Size();

            brand.ID = model.ID;
            brand.Name = model.Name;
            brand.PStatus = model.PStatus;
            brand.SortNo = model.SortNo;
            brand.StoreAccountID = model.StoreID;
            //brand.ModifiedById= //current user
            brand.ModifiedOn = DateTime.Now;
            if (brand.ID != 0) brand.CreatedOn = DateTime.Now;

            return brand;

        }

        private SizeCreateEditVM MapToVM(Size dbModel)
        {
            if (dbModel == null) return null;

            var brand = new SizeCreateEditVM();
            brand.ID = dbModel.ID;
            brand.Name = dbModel.Name;
            brand.PStatus = dbModel.PStatus;
            brand.SortNo = dbModel.SortNo;
            brand.StoreAccountID = dbModel.StoreAccountID;
            return brand;

        }

        public OperationResult<SizeCreateEditVM> Save(SizeCreateEditVM brand)
        {
            var result = new OperationResult<SizeCreateEditVM>();

            Size existing = null;

            if (brand.ID != 0)
            {
                existing = db.Sizes.Where(x => x.ID == brand.ID).FirstOrDefault();
                existing = MapToDBObject(brand, existing);
            }
            else
            {
                existing = MapToDBObject(brand, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(brand.ID == 0 ? QueueItemType.SizeCreated : QueueItemType.SizeUpdated, JsonConvert.SerializeObject(result.Result), brand.StoreID);

            return result;

        }


    }
}
