﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Cargoes;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class CargoService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public CargoService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<CargoCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<CargoCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Cargoes
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Cargoes
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new CargoCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              SortNo = p.SortNo,
                                              CargoCompany = p.CargoCompany,
                                              LogoPath=p.LogoPath,
                                              PStatus = p.PStatus
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Cargoes
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.Cargoes.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new CargoCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      SortNo = p.SortNo,
                      CargoCompany = p.CargoCompany,
                      PStatus = p.PStatus,
                      LogoPath = p.LogoPath
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<CargoCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<CargoCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allDomains = db.Domains.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allCargoCompanies = serviceUtils.ToSelectListInt<CargoCompanyType>(-1);

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new CargoCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllCargoCompanies = allCargoCompanies;
                result.Result.AllDomains = allDomains;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Cargoes.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new CargoCreateEditVM
             {
                 ID = p.ID,
                 SortNo = p.SortNo,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 CargoCompany = p.CargoCompany,
                 DomainID = p.DomainID,
                 IsUseWebServis = p.IsUseWebServis,
                 LogoPath = p.LogoPath,
                 Notes = p.Notes,
                 ShipperPhoneNumber = p.ShipperPhoneNumber,
                 TaxNumber = p.TaxNumber,
                 WebServisCustomerNumber = p.WebServisCustomerNumber,
                 WebServisShipperAddress = p.WebServisShipperAddress,
                 WebServisShipperAreaCode = p.WebServisShipperAreaCode,
                 WebServisShipperCityCode = p.WebServisShipperCityCode,
                 WebServisShipperName = p.WebServisShipperName,
                 WebServisUserName = p.WebServisUserName,
                 WebServisUserPassword = p.WebServisUserPassword,

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllCargoCompanies = allCargoCompanies;
                result.Result.AllDomains = allDomains;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Cargo = db.Cargoes.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Cargo != null)
            {
                Cargo.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.CargoDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Cargo MapToDBObject(CargoCreateEditVM model, Cargo dbModel)
        {
            var Cargo = dbModel ?? new Cargo();

            Cargo.ID = model.ID;

            Cargo.Name = model.Name;
            Cargo.PStatus = model.PStatus;
            Cargo.Notes = model.Notes;
            Cargo.TaxNumber = model.TaxNumber;


            Cargo.IsUseWebServis = model.IsUseWebServis;
            Cargo.WebServisUserName = model.WebServisUserName;
            Cargo.WebServisUserPassword = model.WebServisUserPassword;
            Cargo.WebServisCustomerNumber = model.WebServisCustomerNumber;
            Cargo.WebServisShipperName = model.WebServisShipperName;
            Cargo.WebServisShipperAddress = model.WebServisShipperAddress;
            Cargo.ShipperPhoneNumber = model.ShipperPhoneNumber;
            Cargo.WebServisShipperCityCode = model.WebServisShipperCityCode;
            Cargo.WebServisShipperAreaCode = model.WebServisShipperAreaCode;
            Cargo.CargoCompany = model.CargoCompany;
            Cargo.DomainID = model.DomainID;

            Cargo.SortNo = model.SortNo;

            Cargo.LogoPath = !String.IsNullOrWhiteSpace(model.LogoPath) ? model.LogoPath : dbModel?.LogoPath ?? "";
            Cargo.StoreAccountID = model.ID == 0 ? model.StoreID : Cargo.StoreAccountID;
            Cargo.ModifiedOn = DateTime.Now;
            if (Cargo.ID != 0) Cargo.CreatedOn = DateTime.Now;

            return Cargo;

        }

        private CargoCreateEditVM MapToVM(Cargo dbModel)
        {
            if (dbModel == null) return null;

            var Cargo = new CargoCreateEditVM();
            Cargo.ID = dbModel.ID;
            Cargo.Name = dbModel.Name;
            Cargo.PStatus = dbModel.PStatus;
            Cargo.SortNo = dbModel.SortNo;

            Cargo.IsUseWebServis = dbModel.IsUseWebServis;
            Cargo.WebServisUserName = dbModel.WebServisUserName;
            Cargo.WebServisUserPassword = dbModel.WebServisUserPassword;
            Cargo.WebServisCustomerNumber = dbModel.WebServisCustomerNumber;
            Cargo.WebServisShipperName = dbModel.WebServisShipperName;
            Cargo.WebServisShipperAddress = dbModel.WebServisShipperAddress;
            Cargo.ShipperPhoneNumber = dbModel.ShipperPhoneNumber;
            Cargo.WebServisShipperCityCode = dbModel.WebServisShipperCityCode;
            Cargo.WebServisShipperAreaCode = dbModel.WebServisShipperAreaCode;
            Cargo.CargoCompany = dbModel.CargoCompany;
            Cargo.DomainID = dbModel.DomainID;

            Cargo.SortNo = dbModel.SortNo;
            Cargo.LogoPath = dbModel.LogoPath;
            return Cargo;
        }

        public OperationResult<CargoCreateEditVM> Save(CargoCreateEditVM Cargo)
        {
            var result = new OperationResult<CargoCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Cargo, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Cargo existing = null;

            if (Cargo.ID != 0)
            {
                existing = db.Cargoes.Where(x => x.ID == Cargo.ID).FirstOrDefault();
                existing = MapToDBObject(Cargo, existing);
            }
            else
            {
                existing = MapToDBObject(Cargo, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Cargo.ID == 0 ? QueueItemType.CargoCreated : QueueItemType.CargoUpdated, JsonConvert.SerializeObject(result.Result), Cargo.StoreID);

            return result;

        }


    }
}
