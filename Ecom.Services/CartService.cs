﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Catalogs;
using Ecom.ViewModels.Colors;
using Ecom.ViewModels.Pages;
using Ecom.ViewModels.Prices;
using Ecom.ViewModels.Products;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class CartService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;
        private readonly PriceCurrencyService priceCurrencyService;
        private readonly ProductService productService;

        private readonly EcomContext db;

        public CartService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            PriceCurrencyService _priceCurrencyService,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService,
            ProductService _productService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
            this.priceCurrencyService = _priceCurrencyService;
            this.productService = _productService;
        }

        public Cart GetExistingCartItem(int storeId, int? userId, string key, int priceCurrencyId, long stockId)
        {
            return (from p in db.Carts
                    where p.StoreAccountID == storeId && p.PStatus == Status.Active
                    && p.PriceCurrencyID == priceCurrencyId && p.StockID == stockId
                    && (p.UserID == userId || p.Session == key)
                    orderby p.ID
                    select p).FirstOrDefault();
        }

        public CartVM Get(CartVM cartVM, int storeId, int? userId, string key, int priceCurrencyId)
        {
            var currency = priceCurrencyService.GetCurrencyById(priceCurrencyId, storeId);

            cartVM.Products = (from p in db.Carts
                               where p.StoreAccountID == storeId && p.PStatus == Status.Active
                               && p.PriceCurrencyID == priceCurrencyId
                               && (p.UserID == userId || (p.Session == key || p.UserID == null))
                               orderby p.ID
                               select new CartProductVM
                               {
                                   ProductID = p.ProductID ?? 0,
                                   ProductName = p.Name,
                                   Url = p.Url,
                                   ProductCode = p.ProductCode,
                                   PriceCurrencyId = p.PriceCurrencyID ?? 0,
                                   CurrencyCode = currency.CurrencyCode,
                                   CurrencySymbol = currency.CurrencySymbol,
                                   SizeName = p.SizeName,
                                   ColorName = p.ColorName,
                                   StockID = p.StockID,
                                   Barkod = p.Barkod,
                                   Quantity = p.Quantity ?? 1,
                                   Amount = p.Amount,
                                   AmountOld = p.AmountPsf,
                                   ColorID = p.ColorID,
                                   SizeID = p.SizeID,
                                   ID = p.ID,
                                   ImagePath = p.ImagePath,
                                   LangID = p.LangID

                               }).ToList();

            return cartVM;
        }

        public int GetCount(int storeId, int? userId, string key, int priceCurrencyId)
        {
            return (from p in db.Carts
                    where p.StoreAccountID == storeId && p.PStatus == Status.Active && p.PriceCurrencyID == priceCurrencyId
                    && (p.UserID == userId || (p.Session == key || p.UserID == null))
                    select p).Sum(c => c.Quantity) ?? 0;
        }

        public CartVM Remove(CartVM cartVM, int storeId, long id, int? userId, string key, int priceCurrencyId)
        {
            var cart = db.Carts
                 .Where(x => x.ID == id && x.StoreAccountID == storeId && (x.UserID == userId || (x.Session == key && x.UserID == null))).FirstOrDefault();

            if (cart != null)
            {
                db.Carts.Remove(cart);
                db.SaveChanges();

                rabbitMQService.Publish(QueueItemType.CartDeleted, JsonConvert.SerializeObject(new { id, cart.UserID, key, cart.ProductID, cart.StockID, cart.ColorID, cart.SizeID, cart.Quantity }), storeId);

                cartVM = Get(cartVM, storeId, userId, key, priceCurrencyId);
                cartVM.OperationResult.SetSuccessAndClearError();
            }
            else
            {
                cartVM.OperationResult.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return cartVM;

        }

        public void RemoveAll(int storeId, int? userId, string key, int priceCurrencyId)
        {
            var carts = db.Carts.Where(x => x.PriceCurrencyID==priceCurrencyId && x.StoreAccountID == storeId && (x.UserID == userId || (x.Session == key && x.UserID == null))).ToList();

            if (carts != null)
            {
                ArrayList deletedItems = new ArrayList();

                foreach (var item in carts)
                {
                    deletedItems.Add(new { item.ID, item.UserID, key, item.ProductID, item.StockID, item.ColorID, item.SizeID, item.Quantity });
                    db.Carts.Remove(item);
                }

                db.SaveChanges();
                rabbitMQService.Publish(QueueItemType.CartAllDeleteOrder, JsonConvert.SerializeObject(deletedItems), storeId);
            }
          
        }

        public CartVM SetQuantity(CartVM cartVM, int storeId, long id, int quantity, int? userId, string key, int priceCurrencyId)
        {
            
            var cart = db.Carts.Include(c=>c.Stock)
                 .Where(x => x.ID == id && x.StoreAccountID == storeId && (x.UserID == userId || (x.Session == key && x.UserID == null))).FirstOrDefault();

            if (cart != null)
            {
                if(cart.Stock!=null && cart.Stock.Quantity >= quantity)
                {
                    cart.Quantity = quantity;
                    db.SaveChanges();

                    rabbitMQService.Publish(QueueItemType.CartUpdated, JsonConvert.SerializeObject(new { id, cart.UserID, key, cart.ProductID, cart.StockID, cart.ColorID, cart.SizeID, cart.Quantity}), storeId);
                    var currentCart = Get(cartVM, storeId, userId, key, priceCurrencyId);
                }
                else
                {
                    cartVM.OperationResult.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
                }

            }
            else
            {
                cartVM.OperationResult.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return cartVM;
        }

        public CartVM Add(CartVM cartVM, int storeId, int priceCurrencyId, int productId, int? userId, int langId, string key, int? sizeId, int? colorId, int? stockId)
        {
            
            var product = productService.GetProductDetails(storeId, productId: productId, langId: langId, priceCurrencyId: priceCurrencyId);
            var currentCart = Get(cartVM, storeId, userId, key, priceCurrencyId);
            var stock = GetExistingStock(sizeId, colorId, stockId, product);

            if (product != null && stock != null && stock.Quantity > 0)
            {
                Cart cartItem = CreateCartItem(storeId, priceCurrencyId, userId, key, product, stock);

                var existingItem = currentCart.Products.FirstOrDefault(c => c.StockID == stock.StockID);
                var queItemType = QueueItemType.CartCreated;

                if (existingItem != null)
                {

                    queItemType = QueueItemType.CartUpdated;
                    existingItem.Quantity++;

                    if (stock.Quantity < existingItem.Quantity)
                    {
                        cartVM.OperationResult.SetErrorAndMarkAsErrored("you have reached to stock limit for this product");
                        return cartVM;
                    }

                    var cart = GetExistingCartItem(storeId, userId, key, priceCurrencyId, stock.StockID);
                    if (cart != null)
                    {
                        cart.Quantity++;
                    }
                }
                else
                {
                    db.Carts.Add(cartItem);
                }

                db.SaveChanges();
                AddNewProductToCartVM(storeId, priceCurrencyId, langId, currentCart, cartItem, queItemType);

                cartVM.OperationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                rabbitMQService.Publish(queItemType, JsonConvert.SerializeObject(currentCart), storeId);

                return cartVM;
            }
            else
            {
                cartVM.OperationResult.SetErrorAndMarkAsErrored("product cannot be found, this item may have been deleted or out of stock");
                return cartVM;
            }

        }

        private void AddNewProductToCartVM(int storeId, int priceCurrencyId, int langId, CartVM currentCart, Cart cartItem, QueueItemType queItemType)
        {
            if (queItemType == QueueItemType.CartCreated)
            {

                currentCart.Products.Add(new CartProductVM
                {
                    LangID = langId,
                    ProductName = cartItem.Name,
                    ImagePath = cartItem.ImagePath,
                    Amount = cartItem.Amount,
                    AmountOld = cartItem.AmountPsf,
                    Barkod = cartItem.Barkod,
                    ColorID = cartItem.ColorID,
                    SizeID = cartItem.SizeID,
                    SizeName = cartItem.SizeName,
                    Quantity = cartItem.Quantity ?? 1,
                    ColorName = cartItem.ColorName,
                    PriceCurrencyId = cartItem.PriceCurrencyID ?? 0,
                    CurrencySymbol = currentCart?.CurrencySymbol,
                    CurrencyCode = currentCart?.CurrencyCode,
                    ProductCode = cartItem.ProductCode,
                    ProductID = cartItem.ProductID.Value,
                    StockID = cartItem.StockID,
                    ID = cartItem.ID,
                    Url = cartItem.Url,

                });
            }
        }

        private static Cart CreateCartItem(int storeId, int priceCurrencyId, int? userId, string key, ProductDTVM product, StockVM stock)
        {
            if (stock != null && product != null) product.CurrentColorID = stock.ColorID;

            return new Cart
            {
                Amount = product.Amount,
                AmountPsf = product.AmountOld,
                ProductID = product.ProductID,
                PriceCurrencyID = priceCurrencyId,
                Url = product.Url,
                ProductCode = product.ProductCode,
                ImagePath = product.ImagePath,
                UserID = userId,
                Session = userId.HasValue && userId.Value>0 ? null : key,
                StoreAccountID = storeId,
                Name = product.ProductName,
                StockID = stock.StockID,
                Quantity = 1,
                SizeName = stock.SizeName,
                ColorName = stock.ColorName,
                Barkod = stock.Barkod

            };
        }

        private static StockVM GetExistingStock(int? sizeId, int? colorId, int? stockId, ProductDTVM product)
        {
            StockVM stock = null;

            if (product == null || !product.AllStocks.Any()) return stock;

            if (stockId != null)
            {
                stock = product.AllStocks.Where(c => c.StockID == stockId).FirstOrDefault();
            }
            else if (colorId.HasValue && sizeId.HasValue)
            {
                stock = product.AllStocks.Where(c => c.ColorID == colorId && c.SizeID == sizeId).FirstOrDefault();
            }
            else if (sizeId.HasValue)
            {
                stock = product.AllStocks.Where(c => c.ColorID == null && c.SizeID == sizeId).FirstOrDefault();
            }
            else if (colorId.HasValue)
            {
                stock = product.AllStocks.Where(c => c.ColorID == colorId && c.SizeID == null).FirstOrDefault();
            }

            return stock;
        }

    }
}
