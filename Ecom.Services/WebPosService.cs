﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.WebPoses;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class WebPosService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public WebPosService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<WebPosCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10, int? paymentId = null)
        {
            var operationResult = new PagedOperationListResult<WebPosCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            var totalQuery = (from p in db.WebPoses
                              where
                               p.StoreAccountID == storeId && p.PStatus == Status.Active
                              select p);

            var query = (from p in db.WebPoses
                         where p.StoreAccountID == storeId && p.PStatus == Status.Active
                         orderby p.ID
                         select p).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize));

            if (paymentId.HasValue && paymentId > 0)
            {
                totalQuery = totalQuery.Where(x => x.PaymentID == paymentId);
                query = query.Where(x => x.PaymentID == paymentId);
            }


            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {
                totalQuery = totalQuery.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
                query = query.Where(p => p.Name.ToLower().Contains(searchKeyword.ToLower()));
            }

            operationResult.Result = query.Select(p =>
                              new WebPosCreateEditVM
                              {
                                  ID = p.ID,
                                  Name = p.Name,
                                  SortNo = p.SortNo,
                                  PaymentType = p.PaymentType,
                                  PaymentTypeStr = p.PaymentType.ToString(),
                                  LogoPath=p.LogoPath,
                                  PStatus = p.PStatus,
                                  InstallmentsCount = p.WebPosInstallments.Where(c => c.PStatus == Status.Active).Count(),
                                  PaymentID = p.PaymentID
                              }).ToList();

            totalItemCount = totalQuery.Count();

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<WebPosCreateEditVM> Get(int id, int storeId, int typeid)
        {
            var result = new OperationResult<WebPosCreateEditVM>();

            var allPostPaymentTypes = serviceUtils.ToSelectListInt<SanalPosOdemeTuru>(-1);

            var allPayments = db.Payments.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();


            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new WebPosCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllPayments = allPayments;
                result.Result.AllPosPaymentTypes = allPostPaymentTypes;
                result.Result.PaymentID = typeid;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.WebPoses.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new WebPosCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 ApiPassword = p.ApiPassword,
                 ApiUser = p.ApiUser,
                 BankCode = p.BankCode,
                 EntegHesapAdi = p.EntegHesapAdi,
                 EntegHesapKodu = p.EntegHesapKodu,
                 EntegRefKodu = p.EntegRefKodu,
                 IsDefault = p.IsDefault,
                 LogoPath = p.LogoPath,
                 MagazaNo = p.MagazaNo,
                 MountDelayCount = p.MountDelayCount,
                 NormalPostUrl = p.NormalPostUrl,
                 PaymentType = p.PaymentType,
                 PostNetNo = p.PostNetNo,
                 PostUrl = p.PostUrl,
                 ProvType = p.ProvType,
                 SecureKey3D = p.SecureKey3D,
                 TerminalID = p.TerminalID,
                 SortNo = p.SortNo,
                 PStatus = p.PStatus,
                 PaymentID = p.PaymentID,

             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllPayments = allPayments;
                result.Result.AllPosPaymentTypes = allPostPaymentTypes;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var webpos = db.WebPoses.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (webpos != null)
            {
                webpos.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.WebPosDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private WebPos MapToDBObject(WebPosCreateEditVM model, WebPos dbModel)
        {
            var webpos = dbModel ?? new WebPos();

            webpos.ID = model.ID;

            webpos.Name = model.Name;
            webpos.ApiPassword = model.ApiPassword;
            webpos.ApiUser = model.ApiUser;
            webpos.BankCode = model.BankCode;
            webpos.EntegHesapAdi = model.EntegHesapAdi;
            webpos.EntegHesapKodu = model.EntegHesapKodu;
            webpos.EntegRefKodu = model.EntegRefKodu;
            webpos.IsDefault = model.IsDefault;
            webpos.LogoPath = !String.IsNullOrWhiteSpace(model.LogoPath) ? model.LogoPath : dbModel?.LogoPath ?? "";
            webpos.MagazaNo = model.MagazaNo;
            webpos.MountDelayCount = model.MountDelayCount;
            webpos.NormalPostUrl = model.NormalPostUrl;
            webpos.PaymentType = model.PaymentType;
            webpos.PostNetNo = model.PostNetNo;
            webpos.PostUrl = model.PostUrl;
            webpos.ProvType = model.ProvType;
            webpos.SecureKey3D = model.SecureKey3D;
            webpos.TerminalID = model.TerminalID;
            webpos.SortNo = model.SortNo;
            webpos.PStatus = model.PStatus;
            webpos.PaymentID = model.PaymentID;

            webpos.StoreAccountID = model.ID == 0 ? model.StoreID : webpos.StoreAccountID;
            webpos.ModifiedOn = DateTime.Now;
            if (webpos.ID != 0) webpos.CreatedOn = DateTime.Now;

            return webpos;

        }

        private WebPosCreateEditVM MapToVM(WebPos dbModel)
        {
            if (dbModel == null) return null;

            var webpos = new WebPosCreateEditVM();
            webpos.ID = dbModel.ID;

            webpos.Name = dbModel.Name;
            webpos.ApiPassword = dbModel.ApiPassword;
            webpos.ApiUser = dbModel.ApiUser;
            webpos.BankCode = dbModel.BankCode;
            webpos.EntegHesapAdi = dbModel.EntegHesapAdi;
            webpos.EntegHesapKodu = dbModel.EntegHesapKodu;
            webpos.EntegRefKodu = dbModel.EntegRefKodu;
            webpos.IsDefault = dbModel.IsDefault;
            webpos.LogoPath = dbModel.LogoPath;
            webpos.MagazaNo = dbModel.MagazaNo;
            webpos.MountDelayCount = dbModel.MountDelayCount;
            webpos.NormalPostUrl = dbModel.NormalPostUrl;
            webpos.PaymentType = dbModel.PaymentType;
            webpos.PostNetNo = dbModel.PostNetNo;
            webpos.PostUrl = dbModel.PostUrl;
            webpos.ProvType = dbModel.ProvType;
            webpos.SecureKey3D = dbModel.SecureKey3D;
            webpos.TerminalID = dbModel.TerminalID;
            webpos.SortNo = dbModel.SortNo;
            webpos.PStatus = dbModel.PStatus;

            webpos.PaymentID = dbModel.PaymentID;
            webpos.PStatus = dbModel.PStatus;

            return webpos;
        }

        public OperationResult<WebPosCreateEditVM> Save(WebPosCreateEditVM webpos)
        {
            var result = new OperationResult<WebPosCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(webpos, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            WebPos existing = null;

            if (webpos.ID != 0)
            {
                existing = db.WebPoses.Where(x => x.ID == webpos.ID).FirstOrDefault();
                existing = MapToDBObject(webpos, existing);
            }
            else
            {
                existing = MapToDBObject(webpos, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(webpos.ID == 0 ? QueueItemType.WebPosCreated : QueueItemType.WebPosUpdated, JsonConvert.SerializeObject(result.Result), webpos.StoreID);

            return result;

        }


    }
}
