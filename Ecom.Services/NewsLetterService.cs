﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Catalogs;
using Ecom.ViewModels.Colors;
using Ecom.ViewModels.Pages;
using Ecom.ViewModels.Prices;
using Ecom.ViewModels.Products;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class NewsLetterService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public NewsLetterService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            PriceCurrencyService _priceCurrencyService,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService,
            ProductService _productService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }

        public StandartOperationResult Remove(string Email, int storeId, int? userId)
        {
            StandartOperationResult result = new StandartOperationResult();

            var newsletters = db.NewsLetters.Where(x => (x.Email == Email || x.UserID == userId) && x.StoreAccountID == storeId).ToList();

            if (newsletters != null && newsletters.Count > 0)
            {
                foreach (var item in newsletters)
                {
                    db.NewsLetters.Remove(item);
                }

                db.SaveChanges();
                rabbitMQService.Publish(QueueItemType.NewsLetterDeleted, JsonConvert.SerializeObject(new { Email, UserId = userId ?? 0, storeId }), storeId);
                result.SetSuccessAndClearError(sharedDatalocalizer["MailingeEklendi"]);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        public StandartOperationResult Add(string Email, string IP, int? domainId, int? langId, int storeId, int? userId)
        {
            StandartOperationResult result = new StandartOperationResult();

            var isEmail = Utils.isEmail(Email);

            if (!isEmail)
            {
                result.SetAsBadRequest("Please type a valid email address");
                return result;
            }

            var newsletters = db.NewsLetters.Where(x => x.Email == Email && x.LangID == langId && x.DomainID == domainId && x.StoreAccountID == storeId).FirstOrDefault();

            if (newsletters == null)
            {
                var todayDate = DateTime.Now.Date;

                var IPRequestCount = db.NewsLetters.Where(x => x.IP == IP && x.CreatedOn > todayDate).Count();
                if (IPRequestCount < 5)
                {
                    db.NewsLetters.Add(new NewsLetter
                    {
                        DomainID = domainId,
                        StoreAccountID = storeId,
                        UserID = userId,
                        LangID = langId,
                        Email = Email,
                        IP = IP
                    });

                    db.SaveChanges();

                    result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
                    rabbitMQService.Publish(QueueItemType.NewsLetterCreated, JsonConvert.SerializeObject(new { Email, domainId, userId, langId, storeId }), storeId);
                }
                else
                {
                    result.SetAsBadRequest("Too many requests, Please try another time");
                }


            }
            else
            {
                result.SetErrorAndMarkAsErrored(sharedDatalocalizer["EmailListemizdeKaydinizBulunuyor"]);
            }

            return result;
        }

    }
}
