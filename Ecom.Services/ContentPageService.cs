﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.ContentPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class ContentPageService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public ContentPageService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<ContentPageCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<ContentPageCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.ContentPages
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.ContentPages
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.SortNo
                                          select new ContentPageCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              ContentPageType=p.ContentPageType,
                                              Translations = p.ContentPageLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.ContentPages
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.ContentPages.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.SortNo).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new ContentPageCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus,
                      Translations = p.ContentPageLocalLangs.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Count()
                  }

                ).ToList();
            }

           //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<ContentPageCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<ContentPageCreateEditVM>();

            var allContentPageTypes = serviceUtils.ToSelectListInt<ContentPageType>(-1);
            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new ContentPageCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllContentPageTypes = allContentPageTypes;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.ContentPages.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new ContentPageCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 ContentPageType = p.ContentPageType
               
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;
                result.Result.AllContentPageTypes = allContentPageTypes;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var ContentPage = db.ContentPages.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (ContentPage != null)
            {
                ContentPage.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.ContentPageDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private ContentPage MapToDBObject(ContentPageCreateEditVM model, ContentPage dbModel)
        {
            var ContentPage = dbModel ?? new ContentPage();

            ContentPage.ID = model.ID;

            ContentPage.Name = model.Name;
            ContentPage.PStatus = model.PStatus;
            ContentPage.ContentPageType = model.ContentPageType;
            ContentPage.SortNo = model.SortNo; 
            ContentPage.StoreAccountID = model.ID == 0 ? model.StoreID : ContentPage.StoreAccountID;
            ContentPage.ModifiedOn = DateTime.Now;
            if (ContentPage.ID != 0) ContentPage.CreatedOn = DateTime.Now;

            return ContentPage;

        }

        private ContentPageCreateEditVM MapToVM(ContentPage model)
        {
            if (model == null) return null;

            var ContentPage = new ContentPageCreateEditVM();
            ContentPage.ID = model.ID;
            ContentPage.Name = model.Name;
            ContentPage.PStatus = model.PStatus;
            ContentPage.SortNo = model.SortNo;
            ContentPage.ContentPageType = model.ContentPageType;

            return ContentPage;
        }

        public OperationResult<ContentPageCreateEditVM> Save(ContentPageCreateEditVM ContentPage)
        {
            var result = new OperationResult<ContentPageCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(ContentPage, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            ContentPage existing = null;

            if (ContentPage.ID != 0)
            {
                existing = db.ContentPages.Where(x => x.ID == ContentPage.ID).FirstOrDefault();
                existing = MapToDBObject(ContentPage, existing);
            }
            else
            {
                existing = MapToDBObject(ContentPage, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(ContentPage.ID == 0 ? QueueItemType.ContentPageCreated : QueueItemType.ContentPageUpdated, JsonConvert.SerializeObject(result.Result), ContentPage.StoreID);

            return result;

        }


    }
}
