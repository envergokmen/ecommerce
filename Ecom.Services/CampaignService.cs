﻿using Ecom.Core;
using Ecom.Models;
using Ecom.Services.Bus;
using Ecom.ViewModels;
using Ecom.ViewModels.Campaigns;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecom.Services
{
    public class CampaignService
    {
        private readonly IMemoryCache cache;
        private readonly IStringLocalizer<SharedDataResource> sharedDatalocalizer;
        private readonly RabbitMQService rabbitMQService;
        private readonly ServiceUtils serviceUtils;

        private readonly EcomContext db;

        public CampaignService(
            IMemoryCache _cache,
            EcomContext _db,
           IStringLocalizer<SharedDataResource> _sharedDatalocalizer,
            ServiceUtils _serviceUtils,
            RabbitMQService _rabbitMQService
            )
        {
            cache = _cache;
            db = _db;
            serviceUtils = _serviceUtils;
            sharedDatalocalizer = _sharedDatalocalizer;
            rabbitMQService = _rabbitMQService;
        }


        public PagedOperationListResult<CampaignCreateEditVM> GetAll(int storeId, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            var operationResult = new PagedOperationListResult<CampaignCreateEditVM>();
            operationResult.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

            int totalItemCount = 0;

            if (!String.IsNullOrWhiteSpace(searchKeyword))
            {

                totalItemCount = (from p in db.Campaigns
                                  where
                                   p.StoreAccountID == storeId && p.PStatus == Status.Active &&
                                     (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                  select p).Count();

                operationResult.Result = (from p in db.Campaigns
                                          where p.StoreAccountID == storeId && p.PStatus == Status.Active && (p.Name.ToLower().Contains(searchKeyword.ToLower()))
                                          orderby p.Priority
                                          select new CampaignCreateEditVM
                                          {
                                              ID = p.ID,
                                              Name = p.Name,
                                              PStatus = p.PStatus,
                                              BeginDate = p.BeginDate,
                                              EndDate = p.EndDate,
                                              CalculationType = p.CalculationType,
                                              AllowOnDiscountedProducts = p.AllowOnDiscountedProducts,
                                              AllowWithOtherCoupons = p.AllowWithOtherCoupons,
                                              AllowWithOtherCampaigns = p.AllowWithOtherCampaigns,
                                              AllowMultipleUse = p.AllowMultipleUse,
                                              AllowMultipleCouponPerUser = p.AllowMultipleCouponPerUser,
                                              Amount = p.Amount,
                                              AmountLimit = p.AmountLimit,
                                              MaxAmountLimit = p.MaxAmountLimit,
                                              CouponEndDate = p.CouponEndDate,
                                              CouponAutoEndDate = p.CouponAutoEndDate,
                                              DiscountLimit = p.DiscountLimit,
                                              CreationLimit = p.CreationLimit,
                                              DomainId = p.DomainId,
                                              ImagePath = p.ImagePath,
                                              CampaignType = p.CampaignType,
                                              ShowOnCampaigns = p.ShowOnCampaigns,
                                              Priority = p.Priority,
                                              CreditCardLimits = p.CreditCardLimits,
                                              PriceCurrencyID = p.PriceCurrencyID
                                          })
                    .Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).ToList();

            }
            else
            {
                totalItemCount = (from p in db.Campaigns
                                  where p.PStatus == Status.Active &&
                                  p.StoreAccountID == storeId
                                  select p).Count();

                operationResult.Result = db.Campaigns.Where(x => x.StoreAccountID == storeId && x.PStatus == Status.Active).OrderBy(c => c.Priority).Skip(Convert.ToInt32((page - 1) * pageSize)).Take(Convert.ToInt32(pageSize)).Select(p =>
                  new CampaignCreateEditVM
                  {
                      ID = p.ID,
                      Name = p.Name,
                      PStatus = p.PStatus,
                      BeginDate = p.BeginDate,
                      EndDate = p.EndDate,
                      CalculationType = p.CalculationType,
                      AllowOnDiscountedProducts = p.AllowOnDiscountedProducts,
                      AllowWithOtherCoupons = p.AllowWithOtherCoupons,
                      AllowWithOtherCampaigns = p.AllowWithOtherCampaigns,
                      AllowMultipleUse = p.AllowMultipleUse,
                      AllowMultipleCouponPerUser = p.AllowMultipleCouponPerUser,
                      Amount = p.Amount,
                      AmountLimit = p.AmountLimit,
                      MaxAmountLimit = p.MaxAmountLimit,
                      CouponEndDate = p.CouponEndDate,
                      CouponAutoEndDate = p.CouponAutoEndDate,
                      DiscountLimit = p.DiscountLimit,
                      CreationLimit = p.CreationLimit,
                      DomainId = p.DomainId,
                      ImagePath = p.ImagePath,
                      CampaignType = p.CampaignType,
                      ShowOnCampaigns = p.ShowOnCampaigns,
                      Priority = p.Priority,
                      CreditCardLimits = p.CreditCardLimits,
                      PriceCurrencyID = p.PriceCurrencyID
                  }

                ).ToList();
            }

            //operationResult.TotalPage = (totalItemCount % pageSize) == 0 ? (totalItemCount / pageSize) : (totalItemCount / pageSize) + 1;
            operationResult.TotalItem = totalItemCount;
            operationResult.PageSize = pageSize;
            operationResult.SearchKeyword = searchKeyword;
            operationResult.Page = page;

            return operationResult;
        }

        public OperationResult<CampaignCreateEditVM> Get(int id, int storeId)
        {
            var result = new OperationResult<CampaignCreateEditVM>();

            var allCurrencies = db.PriceCurrencies.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allLangs = db.Langs.Where(x => x.PStatus == Status.Active).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allDomains = db.Domains.Where(x => x.PStatus == Status.Active && x.StoreAccountID == storeId).Select(x => new NameValueIntVM
            {
                Name = x.Name,
                Value = x.ID
            }).ToList();

            var allCalculations = serviceUtils.ToSelectList<HesapLamaTuru>(-1);
            var allCampaignTypes = serviceUtils.ToSelectList<KampanyaTuru>(-1);

            var allStatuses = serviceUtils.GetStatusValues(1);

            //for new Item selectlists
            if (id == 0)
            {
                result.Result = new CampaignCreateEditVM();
                result.Result.AllStatuses = allStatuses;
                result.Result.AllCampaignTypes = allCampaignTypes;
                result.Result.AllCalculationTypes = allCalculations;
                result.Result.AllPriceCurrencies = allCurrencies;
                result.Result.AllDomains = allDomains;
                result.Result.PStatus = Status.Active;

                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);

                return result;
            }

            result.Result = db.Campaigns.Where(x => x.ID == id && x.StoreAccountID == storeId).Select(p =>
             new CampaignCreateEditVM
             {
                 ID = p.ID,
                 Name = p.Name,
                 PStatus = p.PStatus,
                 BeginDate = p.BeginDate,
                 EndDate = p.EndDate,
                 CalculationType = p.CalculationType,
                 AllowOnDiscountedProducts = p.AllowOnDiscountedProducts,
                 AllowWithOtherCoupons = p.AllowWithOtherCoupons,
                 AllowWithOtherCampaigns = p.AllowWithOtherCampaigns,
                 AllowMultipleUse = p.AllowMultipleUse,
                 AllowMultipleCouponPerUser = p.AllowMultipleCouponPerUser,
                 Amount = p.Amount,
                 AmountLimit = p.AmountLimit,
                 MaxAmountLimit = p.MaxAmountLimit,
                 CouponEndDate = p.CouponEndDate,
                 CouponAutoEndDate = p.CouponAutoEndDate,
                 DiscountLimit = p.DiscountLimit,
                 CreationLimit = p.CreationLimit,
                 DomainId = p.DomainId,
                 ImagePath = p.ImagePath,
                 CampaignType = p.CampaignType,
                 ShowOnCampaigns = p.ShowOnCampaigns,
                 Priority = p.Priority,
                 CreditCardLimits = p.CreditCardLimits,
                 PriceCurrencyID = p.PriceCurrencyID
             }
            ).FirstOrDefault();

            if (result.Result != null)
            {
                result.Result.AllStatuses = allStatuses;

                result.Result.AllStatuses = allStatuses;
                result.Result.AllCampaignTypes = allCampaignTypes;
                result.Result.AllCalculationTypes = allCalculations;
                result.Result.AllDomains = allDomains;
                result.Result.AllPriceCurrencies = allCurrencies;
                result.Result.PStatus = Status.Active;


                result.SetSuccessAndClearError(sharedDatalocalizer["GeneralSuccessMessage"]);
            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);

            }

            return result;

        }

        public OperationIdResult Delete(int id, int storeId)
        {
            var result = new OperationIdResult();

            var Campaign = db.Campaigns.Where(x => x.ID == id && x.StoreAccountID == storeId).FirstOrDefault();

            if (Campaign != null)
            {
                Campaign.PStatus = Status.Deleted;
                db.SaveChanges();
                result.SetSuccessAndClearError(id, sharedDatalocalizer["GeneralSuccessMessage"]);
                rabbitMQService.Publish(QueueItemType.CampaignDeleted, JsonConvert.SerializeObject(result.Result), storeId);

            }
            else
            {
                result.SetAsNotFound(sharedDatalocalizer["GeneralNotFoundMessage"]);
            }

            return result;

        }

        private Campaign MapToDBObject(CampaignCreateEditVM model, Campaign dbModel)
        {
            var campaign = dbModel ?? new Campaign();

            campaign.ID = model.ID;
            campaign.Name = model.Name;
            campaign.PStatus = model.PStatus;
            campaign.Priority = model.Priority;
            campaign.BeginDate = model.BeginDate;
            campaign.EndDate = model.EndDate;
            campaign.CalculationType = model.CalculationType!=null ? model.CalculationType.Value: HesapLamaTuru.Nakit;
            campaign.AllowOnDiscountedProducts = model.AllowOnDiscountedProducts;
            campaign.AllowWithOtherCoupons = model.AllowWithOtherCoupons;
            campaign.AllowWithOtherCampaigns = model.AllowWithOtherCampaigns;
            campaign.AllowMultipleUse = model.AllowMultipleUse;
            campaign.AllowMultipleCouponPerUser = model.AllowMultipleCouponPerUser;
            campaign.Amount = model.Amount!=null ? model.Amount.Value : 0;
            campaign.AmountLimit = model.AmountLimit!=null ? model.AmountLimit.Value : 0;
            campaign.MaxAmountLimit = model.MaxAmountLimit;
            campaign.CouponEndDate = model.CouponEndDate;
            campaign.CouponAutoEndDate = model.CouponAutoEndDate;
            campaign.DiscountLimit = model.DiscountLimit;
            campaign.CreationLimit = model.CreationLimit;
            campaign.DomainId = model.DomainId;
            campaign.ImagePath = model.ImagePath;

            campaign.ImagePath = !String.IsNullOrWhiteSpace(model.ImagePath) ? model.ImagePath : dbModel?.ImagePath ?? "";

            campaign.CampaignType = model.CampaignType;
            campaign.ShowOnCampaigns = model.ShowOnCampaigns;
            campaign.CreditCardLimits = model.CreditCardLimits;
            campaign.PriceCurrencyID = model.PriceCurrencyID;
            campaign.StoreAccountID = model.ID == 0 ? model.StoreID : campaign.StoreAccountID;
            campaign.ModifiedOn = DateTime.Now;
            if (campaign.ID != 0) campaign.CreatedOn = DateTime.Now;

            return campaign;

        }

        private CampaignCreateEditVM MapToVM(Campaign model)
        {
            if (model == null) return null;

            var campaign = new CampaignCreateEditVM();

            campaign.ID = model.ID;
            campaign.Name = model.Name;
            campaign.PStatus = model.PStatus;
            campaign.Priority = model.Priority;
            campaign.BeginDate = model.BeginDate;
            campaign.EndDate = model.EndDate;
            campaign.CalculationType = model.CalculationType;
            campaign.AllowOnDiscountedProducts = model.AllowOnDiscountedProducts;
            campaign.AllowWithOtherCoupons = model.AllowWithOtherCoupons;
            campaign.AllowWithOtherCampaigns = model.AllowWithOtherCampaigns;
            campaign.AllowMultipleUse = model.AllowMultipleUse;
            campaign.AllowMultipleCouponPerUser = model.AllowMultipleCouponPerUser;
            campaign.Amount = model.Amount;
            campaign.AmountLimit = model.AmountLimit;
            campaign.MaxAmountLimit = model.MaxAmountLimit;
            campaign.CouponEndDate = model.CouponEndDate;
            campaign.CouponAutoEndDate = model.CouponAutoEndDate;
            campaign.DiscountLimit = model.DiscountLimit;
            campaign.CreationLimit = model.CreationLimit;
            campaign.DomainId = model.DomainId;
            campaign.ImagePath = model.ImagePath;
            campaign.CampaignType = model.CampaignType;
            campaign.ShowOnCampaigns = model.ShowOnCampaigns;
            campaign.CreditCardLimits = model.CreditCardLimits;
            campaign.PriceCurrencyID = model.PriceCurrencyID;

            return campaign;
        }

        public OperationResult<CampaignCreateEditVM> Save(CampaignCreateEditVM Campaign)
        {
            var result = new OperationResult<CampaignCreateEditVM>();

            var validationResult = serviceUtils.TryToValidateModel(Campaign, true);

            if (validationResult.Status == false)
            {
                result.SetError(validationResult.ResultDescription, ReturnStatusCode.BadRequest);
                return result;
            }

            Campaign existing = null;

            if (Campaign.ID != 0)
            {
                existing = db.Campaigns.Where(x => x.ID == Campaign.ID).FirstOrDefault();
                existing = MapToDBObject(Campaign, existing);
            }
            else
            {
                existing = MapToDBObject(Campaign, null);
                db.Add(existing);
            }

            db.SaveChanges();
            result.SetSuccessAndClearError(MapToVM(existing), sharedDatalocalizer["GeneralSuccessMessage"]);

            rabbitMQService.Publish(Campaign.ID == 0 ? QueueItemType.CampaignCreated : QueueItemType.CampaignUpdated, JsonConvert.SerializeObject(result.Result), Campaign.StoreID);

            return result;

        }

        public StandartOperationResult GenerateCouponForCampaigns(int? userID, List<Campaign> cpns, int? RefUserID = default(int?), decimal? Amount = default(decimal?), decimal? AmountLimit = default(decimal?))
        {

            StandartOperationResult sonuc = new StandartOperationResult();
            sonuc.SetError("");

            var random = new Random();
            DateTime simdi = DateTime.Now;

            List<string> couponCodes = new List<string>();
            bool giveNewCoupon = false;

            try
            {

                if (cpns != null && cpns.Count() > 0)
                {
                    foreach (Campaign item in cpns)
                    {
                        Coupon cpNew = new Coupon();

                        MapCouponFields(userID, RefUserID, Amount, AmountLimit, random, item, cpNew);

                        List<Coupon> cList = (from c in db.Coupons.AsNoTracking()
                                              where c.CampaignID == item.ID && c.UserID == userID && c.StoreAccountID == item.StoreAccountID
                                              select c).ToList();

                        if (cList.Count > 0 && item.AllowMultipleCouponPerUser)
                        {
                            Coupon couponNotUsed = (from p in cList where p.IsUsed == false && p.EndDate > simdi select p).FirstOrDefault();

                            if (couponNotUsed != null)
                            {
                                giveNewCoupon = false;
                            }
                            else
                            {
                                giveNewCoupon = true;
                            }
                        }

                        if (couponCodes.Contains(cpNew.CouponCode))
                        {
                            sonuc.SetError("Kupon kodlarında tekrar eden değer tespit edildi lütfen tekrar oluşturunuz. ");

                        }
                        else if (cList.Count > 0 && giveNewCoupon == false)
                        {
                            sonuc.SetError("Bu üye daha önce bu kampanyadan kupon kazanmıştır ");
                        }
                        else
                        {

                            couponCodes.Add(cpNew.CouponCode);
                            db.Coupons.Add(cpNew);
                            db.SaveChanges();

                            sonuc.SetSuccessAndClearError(cpNew.ID);

                        }

                    }
                }


            }
            finally
            {
                
            }

            return sonuc;
        }

        private void MapCouponFields(int? userID, int? RefUserID, decimal? Amount, decimal? AmountLimit, Random random, Campaign item, Coupon cpNew)
        {
            cpNew.StoreAccountID = item.StoreAccountID;
            cpNew.CampaignID = item.ID;
            cpNew.CreatedOn = DateTime.Now;
            cpNew.IsUsed = false;
            cpNew.DomainId = item.DomainId;
            cpNew.PStatus = Status.Active;
            cpNew.UserID = userID;


            cpNew.BeginDate = item.BeginDate;
            cpNew.EndDate = item.CouponEndDate;

            if (item.CouponAutoEndDate != null && item.CouponAutoEndDate != 0)
            {
                cpNew.EndDate = DateTime.Now.AddDays(Convert.ToInt32(item.CouponAutoEndDate));
            }

            cpNew.AllowMultipleUse = item.AllowMultipleUse;
            cpNew.AllowOnDiscountedProducts = item.AllowOnDiscountedProducts;
            cpNew.Amount = item.Amount;
            cpNew.AmountLimit = item.AmountLimit;

            if (Amount != null)
            {
                cpNew.Amount = Amount;
            }

            if (AmountLimit != null)
            {
                cpNew.AmountLimit = AmountLimit;
            }

            cpNew.DiscountLimit = item.DiscountLimit;
            cpNew.CreditCardLimits = item.CreditCardLimits;
            cpNew.PriceCurrencyID = item.PriceCurrencyID;
            cpNew.CampaignType = item.CampaignType;
            cpNew.CreationLimit = item.CreationLimit;
            cpNew.CalculationType = item.CalculationType;
            cpNew.MaxAmountLimit = item.MaxAmountLimit;

            cpNew.CouponCode = CreateCouponCode(item.Name.Substring(0, 3), random);
        }

        private string CreateCouponCode(string StartWith, Random random)
        {

            string CouponCode = (String.IsNullOrWhiteSpace(StartWith)) ? "" : Utils.FormatUrl(StartWith.Replace(" ", "")).ToUpper(new System.Globalization.CultureInfo("en"));

            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            var result = new string(
                Enumerable.Repeat(chars, 15)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            CouponCode += result.ToString();

            return CouponCode;
        }

        public StandartOperationResult GenerateCouponForCartCampaign(int storeId, int? domainID, decimal cartTotal, int userID)
        {
            StandartOperationResult result = new StandartOperationResult();
            if (userID > 0) return result;

            DateTime bugun = DateTime.Now;

            string checkedKey = String.Concat("UserCartCampaign", cartTotal, userID);

            if (String.IsNullOrWhiteSpace(cache.Get<string>(checkedKey)))
            {
                List<Campaign> cpns = (from p in db.Campaigns
                                       where
                                       p.StoreAccountID == storeId &&
                                       (p.DomainId == null || p.DomainId == domainID)
                                           && p.BeginDate <= bugun && p.EndDate
                                           > bugun && p.PStatus == Status.Active
                                           && p.CampaignType == KampanyaTuru.AcordingToCartTotal
                                           && p.CreationLimit <= cartTotal
                                       orderby p.CreationLimit descending
                                       select p).Take(1).ToList();

                result = GenerateCouponForCampaigns(userID, cpns);

                cache.Set<string>(checkedKey, "Checked", DateTime.Now.AddMinutes(30));

            }

            return result;
        }


        public StandartOperationResult GenerateCouponAllUsers(int storeId, int? domainID, int userID, KampanyaTuru[] campaignTypes)
        {
            StandartOperationResult result = new StandartOperationResult();
            if (userID <= 0) return result;

            DateTime bugun = DateTime.Now;

            var allCampaignTypes = string.Join('-', campaignTypes);
            string checkedKey = String.Concat("CouponForAll", userID,"-store-", storeId, "-domain-", domainID??0, "-user-", userID,"-campaigns-", allCampaignTypes);

            if (String.IsNullOrWhiteSpace(cache.Get<string>(checkedKey)))
            {
                List<Campaign> cpns = (from p in db.Campaigns
                                       where
                                       p.StoreAccountID == storeId &&
                                       (p.DomainId == null || p.DomainId == domainID)
                                           && p.BeginDate <= bugun && p.EndDate
                                           > bugun && p.PStatus == Status.Active
                                           && campaignTypes.Contains(p.CampaignType)
                                       orderby p.CreationLimit descending
                                       select p).Take(5).ToList();

                result = GenerateCouponForCampaigns(userID, cpns);

                cache.Set<string>(checkedKey, "Checked", DateTime.Now.AddMinutes(30));

            }

            return result;
        }





    }
}
