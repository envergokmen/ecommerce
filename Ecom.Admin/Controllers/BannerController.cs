using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.MvcCore.Extensions;
using Ecom.Services;
using Ecom.ViewModels.Banners;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class BannerController : EcomAdminBaseController
    {
        private readonly BannerService bannerService;
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
       
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public BannerController(IHttpContextAccessor _contextAccessor, IHostingEnvironment env, BannerService _bannerService, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            bannerService = _bannerService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] BannerCreateEditVM banner)
        {
            SetCurUserAndStore(banner);

            if (banner != null && banner.file != null)
            {
                banner.ImagePath = FileUploadHelpler.SaveFilFromHttp(Request.IsLocal(), banner.file, rootPath, Utils.FormatUrl(this.storeName), "Banners");
            }

            var operationResult = bannerService.Save(banner);

            return Json(operationResult);
        }

        public IActionResult Edit(int Id)
        {
            //System.Threading.Thread.Sleep(500);
            
            var operationResult = bannerService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(bannerService.Delete(Id, this.storeAccountID));
        }

        public IActionResult DeleteImage(int Id)
        {
            return Json(bannerService.DeleteImage(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageBanner = 10)
        {
            PagedOperationListResult<BannerCreateEditVM> operationResult = bannerService.GetAll(this.storeAccountID, page, searchKeyword, pageBanner);
            return Json(operationResult);
        }

    }
}
