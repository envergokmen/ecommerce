using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.MvcCore.Extensions;
using Ecom.Services;
using Ecom.ViewModels.Banks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class BankController : EcomAdminBaseController
    {
        private readonly BankService bankService;
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
       
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public BankController(IHttpContextAccessor _contextAccessor, IHostingEnvironment env, BankService _bankService, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            bankService = _bankService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] BankCreateEditVM bank)
        {
            SetCurUserAndStore(bank);

            if (bank != null && bank.file!=null)
            {
                bank.LogoPath = FileUploadHelpler.SaveFilFromHttp(Request.IsLocal(), bank.file, rootPath, Utils.FormatUrl(this.storeName), "Banks");
            }
         
            var operationResult = bankService.Save(bank);

            return Json(operationResult);
        }

        public IActionResult Edit(int Id)
        {
            //System.Threading.Thread.Sleep(500);
            
            var operationResult = bankService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(bankService.Delete(Id, this.storeAccountID));
        }
        public IActionResult DeleteImage(int Id)
        {
            return Json(bankService.DeleteImage(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            PagedOperationListResult<BankCreateEditVM> operationResult = bankService.GetAll(this.storeAccountID, page, searchKeyword, pageSize);
            return Json(operationResult);
        }

    }
}
