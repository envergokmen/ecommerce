﻿using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Stores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace Ecom.Admin.Controllers
{
    public class EcomAdminBaseController : Controller
    {
        public IMemberShipService memberShipService;
        public DomainService domainService;
        public IHttpContextAccessor contextAccessor;
        public EcomAdminBaseController(IMemberShipService _memberShipService, DomainService _domainService, IHttpContextAccessor contextAccessor)
        {
            this.memberShipService = _memberShipService;
            this.domainService = _domainService;
            this.contextAccessor = contextAccessor;
        }

        public UserViewModel user;
        public DomainViewModel domain;

        public string domainName => domain?.Name ?? "NoDomain";
        public string storeName => user?.StoreName ?? $"NoStore-{user.StoreAccountID ?? 0}";
        public int storeAccountID => user?.StoreAccountID ?? 0;
        public string actionName;
        public string controllerName;
        public string curUrl;

        public void SetCurUserAndStore(BaseViewModel baseRequestModel)
        {
            if (baseRequestModel != null)
            {
                baseRequestModel.IP = contextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                baseRequestModel.CurUser = user;
            }
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string langCode = context.RouteData.Values["lang"]?.ToString();

            curUrl = UriHelper.GetDisplayUrl(context.HttpContext.Request);
            //var domainName = new Uri(curUrl).Authority?.Split(':')[0];


            user = memberShipService.TryToSignUser(UserType.StoreManager);

            //domain = user != null ? domainService.GetDomainById(user.DomainId ?? 0) : null;

            if (user == null)
            {
                context.Result = new RedirectResult($"/sign-in/{langCode}");
                return;
            }

            //String.IsNullOrWhiteSpace(langCode) ? domainService.GetDomainByName(domainName) : domainService.GetDomainByNameAndLang(domainName, langCode);

            //if (user!=null)
            //{
            //    domain = new DomainViewModel { ID = user.DomainId ?? 0 };
            //}

            // we must always have a domain in specifed lang
            //if (domain == null)
            //{
            //    context.Result = new NotFoundResult();
            //    return;
            //}

            if (String.IsNullOrWhiteSpace(langCode) && !String.IsNullOrWhiteSpace(domain.LangCode))
            {

                context.Result = new RedirectResult($"{curUrl.TrimEnd('/')}/{domain.LangCode}");
                return;
            }

            actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ActionName;
            controllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ControllerName;
            user = memberShipService.TryToSignUser(UserType.StoreManager);

            base.OnActionExecuting(context);
        }

    }
}
