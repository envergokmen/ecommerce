using System.Collections.Generic;
using Ecom.Auth.Services;
using Ecom.Services;
using Ecom.ViewModels;
using Ecom.ViewModels.Api.Panel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class AdminMenuController : EcomAdminBaseController
    {
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;

        public AdminMenuController(IHttpContextAccessor _contextAccessor, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
        }

        public IEnumerable<AdminMenu> GetAdminMenu(string lang="en")
        {
            List<AdminMenu> adminMenu = new List<AdminMenu>
            {

                 new AdminMenu(
                     _localizer["Siparisler"].Value,
                     1,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["OnayBekleyen"].Value, linkTo: $"/orders/{lang}/Waiting", Id:2) ,
                      new AdminMenu(_localizer["Onaylanan"].Value, linkTo:$"/orders/{lang}/Approved/", Id:3) ,
                      new AdminMenu(_localizer["TeslimEdilen"].Value, linkTo:$"/orders/{lang}/Delivered", Id:4) ,
                      new AdminMenu(_localizer["IptalEdilen"].Value, linkTo:$"/orders/{lang}/Cancelled", Id:5) ,
                      new AdminMenu(_localizer["Tumu"].Value, linkTo: $"/orders/{lang}/All", Id:6)
                  }, isOpen:false
                 ),
                
                  new AdminMenu(_localizer["UrunYonetimi"].Value,
                  7,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["Markalar"].Value, linkTo:$"/brands/{lang}", Id:8, icon:"dragon") ,
                      new AdminMenu(_localizer["Urunler"].Value, linkTo:$"/products/{lang}", Id:9, icon:"gem") ,
                      new AdminMenu(_localizer["Sezonlar"].Value, linkTo:$"/seasons/{lang}", Id:10, icon:"swimming-pool") ,
                      new AdminMenu(_localizer["Stiller"].Value, linkTo:$"/styles/{lang}", Id:10, icon:"dice-d20") ,
                      new AdminMenu(_localizer["Departmanlar"].Value, linkTo:$"/departments/{lang}", Id:10, icon:"cube") ,
                      new AdminMenu(_localizer["Kataloglar"].Value, linkTo:$"/catalogs/{lang}", Id:11, icon:"sitemap") ,
                      new AdminMenu(_localizer["KargoPolitikalari"].Value, linkTo:$"/policies/{lang}/type/cargopolicy", Id:70, icon:"book"),
                      new AdminMenu(_localizer["IadePolitikalari"].Value, linkTo:$"/policies/{lang}/type/returnpolicy", Id:71, icon:"book") }
                  , isOpen:true
                 ),

                 new AdminMenu(_localizer["RenkBedenTanimlari"].Value,
                 13,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["BedenTanimlari"].Value, linkTo:$"/sizes/{lang}", Id:14, icon:"baby") ,
                      new AdminMenu(_localizer["GlobalRenkler"].Value, linkTo:$"/colorglobals/{lang}", Id:15, icon:"camera") ,
                      new AdminMenu(_localizer["Renkler"].Value, linkTo:$"/colors/{lang}", Id:16, icon:"candy-cane")
                  }
                 ),

                  new AdminMenu(_localizer["KampanyaKupon"].Value,
                  17,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["Kampanyalar"].Value, linkTo:$"/campaigns/{lang}", Id:18, icon:"bullhorn") ,
                      new AdminMenu(_localizer["Kuponlar"].Value, linkTo:$"/coupons/{lang}", Id:19, icon:"ticket-alt"),
                  }
                 ),

                  new AdminMenu(_localizer["OdemePos"].Value,
                  20,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["OdemeTipleri"].Value, linkTo:$"/payments/{lang}", Id:21, icon:"money-check-alt") ,
                      new AdminMenu(_localizer["SanalPosTanimlari"].Value, linkTo:$"/webposes/{lang}", Id:23, icon:"money-check"),
                      new AdminMenu(_localizer["SanalPosTaksitTanimlari"].Value, linkTo:$"/webposinstallments/{lang}", Id:75, icon:"money-check"),
                      new AdminMenu(_localizer["BinNumaralari"].Value, linkTo:$"/bins/{lang}", Id:24, icon:"money-check"),
                      new AdminMenu(_localizer["BankaTanimlari"].Value, linkTo:$"/banks/{lang}", Id:22, icon:"building"),
                  }
                 ),


                  new AdminMenu(_localizer["IcerikAyarlari"].Value,
                  25,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["BannerYonetimi"].Value, linkTo:$"/banners/{lang}", Id:26, icon:"object-ungroup") ,
                      new AdminMenu(_localizer["IcerikSayfalari"].Value, linkTo:$"/contentpages/{lang}", Id:27, icon:"newspaper") ,
                      new AdminMenu(_localizer["EmailSablonlari"].Value, linkTo:$"/emailtemplates/{lang}", Id:28, icon:"envelope-open-text"),
                      new AdminMenu(_localizer["Blog"].Value, linkTo:$"/blogs/{lang}", Id:29, icon:"newspaper"),
                      new AdminMenu(_localizer["BlogKategorileri"].Value, linkTo:$"/catalogs/{lang}", Id:30, icon:"sitemap"),
                      new AdminMenu(_localizer["BlogYorumlari"].Value, linkTo:$"/blogcomments/{lang}", Id:31, icon:"comments")
                  }
                 ),

                  new AdminMenu(_localizer["SiparisRaporlari"].Value,
                  32,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["Ozet"].Value, linkTo:$"/reports/order/summary/{lang}", Id:33, icon:"chart-line") ,
                      new AdminMenu(_localizer["BuHafta"].Value, linkTo:$"/reports/order/thisweek/{lang}", Id:34, icon:"chart-line"),
                      new AdminMenu(_localizer["GecenHafta"].Value, linkTo:$"/reports/order/lastweek/{lang}", Id:35, icon:"chart-line"),
                      new AdminMenu(_localizer["Bugun"].Value, linkTo:$"/reports/order/today/{lang}", Id:36, icon:"chart-line"),
                      new AdminMenu(_localizer["Dun"].Value, linkTo:$"/reports/order/yesterday/{lang}", Id:37, icon:"chart-line"),
                      new AdminMenu(_localizer["BuAy"].Value, linkTo:$"/reports/order/thismonth/{lang}", Id:38, icon:"chart-line"),
                      new AdminMenu(_localizer["GecenAy"].Value, linkTo:$"/reports/order/lastmonth/{lang}", Id:39, icon:"chart-line"),
                      new AdminMenu(_localizer["Son15Gun"].Value, linkTo:$"/reports/order/last15/{lang}", Id:40, icon:"chart-line"),
                      new AdminMenu(_localizer["Son30Gun"].Value, linkTo:$"/reports/order/last30/{lang}", Id:41, icon:"chart-line"),
                  }
                 ),

                new AdminMenu(_localizer["Raporlar"].Value,
                  42,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["UyeRaporlari"].Value, linkTo:$"/reports/member/{lang}", Id:43, icon:"chart-line") ,
                      new AdminMenu(_localizer["OylamaFavoriRaporlari"].Value, linkTo:$"/reports/ratefav/{lang}", Id:44, icon:"chart-line"),
                      new AdminMenu(_localizer["SepetRaporlari"].Value, linkTo:$"/reports/cart/{lang}", Id:45, icon:"chart-line"),
                      new AdminMenu(_localizer["ZiyaretciRaporlari"].Value, linkTo:$"/reports/visitors/{lang}", Id:46, icon:"chart-line"),
                      new AdminMenu(_localizer["KampanyaRaporlari"].Value, linkTo:$"/reports/campaigns/{lang}", Id:47, icon:"chart-line"),
                      new AdminMenu(_localizer["KuponRaporlari"].Value, linkTo:$"/reports/coupon/{lang}", Id:48, icon:"chart-line")
                  }
                 ),

                  new AdminMenu(_localizer["Uyeler"].Value,
                  49,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["UyeListesi"].Value, linkTo:$"/members/{lang}", Id:50,  icon:"users") ,
                      new AdminMenu(_localizer["UyeMesajlari"].Value, linkTo:$"/members/messages/{lang}", Id:51,  icon:"comments")
                  }
                 ),

                  //faEthernet, faEnvelope, faDollarSign, faMoneyBillAlt, faGlobeAfrica, faUserCog, faShieldAlt,
                new AdminMenu(_localizer["Ayarlar"].Value,
                  52,
                  new List<AdminMenu>{

                      new AdminMenu(_localizer["HesapAyarlari"].Value, linkTo:$"/storeaccounts/{lang}", Id:68,  icon:"cog"),
                      new AdminMenu(_localizer["DomainAyarlari"].Value, linkTo:$"/domains/{lang}", Id:53,  icon:"ethernet"),
                      new AdminMenu(_localizer["GonderiSaglayicilari"].Value, linkTo:$"/sendingproviders/{lang}", Id:67,  icon:"envelope"),
                      new AdminMenu(_localizer["ParaBirimi"].Value, linkTo:$"/currencies/{lang}", Id:54,  icon:"dollar-sign"),
                      new AdminMenu(_localizer["DilTanimlari"].Value, linkTo:$"/langs/{lang}", Id:55,  icon:"globe-africa"),
                      new AdminMenu(_localizer["Kullanicilar"].Value, linkTo:$"/users/{lang}", Id:56,  icon:"user-cog"),
                      new AdminMenu(_localizer["Yetkiler"].Value, linkTo:$"/roles/{lang}", Id:57,  icon:"shield-alt")
                      //new AdminMenu(_localizer["MagazaTanimlari"].Value, linkTo:$"/shops/{lang}", Id:68),
                  }
                 ),


                new AdminMenu(_localizer["UlkeSehirTanimlari"].Value,
                  58,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["UlkeTanimlari"].Value, linkTo:$"/countries/{lang}", Id:59,icon:"globe-africa"),
                      new AdminMenu(_localizer["SehirTanimlari"].Value, linkTo:$"/cities/{lang}", Id:60,icon:"globe-africa"),
                      new AdminMenu(_localizer["IlceTanimlari"].Value, linkTo:$"/towns/{lang}", Id:61,icon:"globe-africa")
                  }
                 ),


                 new AdminMenu(_localizer["KargoTanimlari"].Value,
                  62,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["KargoTanimlari"].Value, linkTo:$"/cargoes/{lang}", Id:63 ,icon:"truck"),
                      new AdminMenu(_localizer["KargoFiyatTanimlari"].Value, linkTo:$"/cargoprices/{lang}", Id:64 ,icon:"money-bill-alt")
                  }
                 ),


                 new AdminMenu(_localizer["HediyePaketiTanimlari"].Value,
                  65,
                  new List<AdminMenu>{
                      new AdminMenu(_localizer["HediyePaketiTanimlari"].Value, linkTo:$"/giftpackages/{lang}", Id:66, icon:"gift")
                  }
                 ),

                 //   new AdminMenu(_localizer["MagazaTanimlari"].Value,
                 // 67,
                 // new List<AdminMenu>{
                 //     new AdminMenu(_localizer["MagazaTanimlari"].Value, linkTo:$"/ColorGlobals/{lang}", Id:68)
                 // }
                 //),

            };

            return adminMenu;
        }


    }
}
