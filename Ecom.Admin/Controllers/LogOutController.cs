using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class LogOutController : Controller
    {
        IMemberShipService memberShipService;

        public LogOutController(IMemberShipService _memberShipService)
        { 
            this.memberShipService = _memberShipService;
        }

        //[HttpPost]
        public IActionResult Index(string lang)
        {
            memberShipService.LogOut();
            StandartOperationResult logOutResult = new StandartOperationResult(); 
            return Redirect($"/sign-in/{lang}");
        }

        public IActionResult Ajax(string lang)
        {
            memberShipService.LogOut();
            StandartOperationResult logOutResult = new StandartOperationResult();
            logOutResult.SetSuccessAndClearError();
            return Json(logOutResult);
        }


    }
}
