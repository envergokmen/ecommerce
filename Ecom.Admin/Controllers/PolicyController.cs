using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.Policies;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class PolicyController : EcomAdminBaseController
    {
    
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly PolicyService policyService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public PolicyController(PolicyService _policyService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.policyService = _policyService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] PolicyCreateEditVM Policy)
        {
            SetCurUserAndStore(Policy);

            var operationResult = policyService.Save(Policy);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id, PoliciyType? type)
        { 
            var operationResult = policyService.Get(Id, this.storeAccountID, type);
            return Json(operationResult);
        }

        public IActionResult GetName(int Id)
        {
            var operationResult = policyService.Get(Id, this.storeAccountID, null);
            return Content(operationResult.Result.Name);
        }

        public IActionResult Delete(int Id)
        {
            return Json(policyService.Delete(Id, this.storeAccountID));
        }
       
        public IActionResult Index(PoliciyType? type, int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            PagedOperationListResult<PolicyCreateEditVM> operationResult = policyService.GetAll(this.storeAccountID, type, page, searchKeyword, pageSize);
            return Json(operationResult);
        }

    }
}
