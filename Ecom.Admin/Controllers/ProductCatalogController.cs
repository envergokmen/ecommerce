using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.Catalogs;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class ProductCatalogController : EcomAdminBaseController
    {
    
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly CatalogService catalogService;
        private readonly ProductService productService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public ProductCatalogController(ProductService _productService, CatalogService _catalogService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.catalogService = _catalogService;
            this.productService = _productService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

       
        public IActionResult Edit(int Id)
        { 
            var operationResult = catalogService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }


        public IActionResult sort(long catalogId = 0)
        {

            OperationResult<ProductCatalogCreateEditVM> operationResult = new OperationResult<ProductCatalogCreateEditVM>(); //catalogService.GetAll(this.storeAccountID, page, searchKeyword, pageSize);

            operationResult.Result = new ProductCatalogCreateEditVM();

            operationResult.Result.AddedCatalogs = productService.GetCatalogProductsForSort(catalogId, this.storeAccountID).Result;

            operationResult.SetSuccessAndClearError();

            return Json(operationResult);
        }

        public IActionResult UpdateCatalogSort([FromBody] CatalogCatalogOrderVM sortVM)
        {
           
            var operationResult = productService.UpdateCatalogSort(sortVM.CatalogID, this.storeAccountID, sortVM.Items);
             
            return sort(sortVM.CatalogID);
        }


        public IActionResult Index(long productId = 0)
        {

            OperationResult<ProductCatalogCreateEditVM> operationResult = new OperationResult<ProductCatalogCreateEditVM>(); //catalogService.GetAll(this.storeAccountID, page, searchKeyword, pageSize);

            operationResult.Result = new ProductCatalogCreateEditVM();

            operationResult.Result.ProductID = productId;
            operationResult.Result.ProductColors = productService.GetProductForCatalog(productId, this.storeAccountID).Result;
            operationResult.Result.Catalogs = catalogService.GetAllCatalogs(this.storeAccountID, false, CatalogType.Product);
            operationResult.Result.AddedCatalogs = productService.GetProductInCatalog(productId, this.storeAccountID);
            operationResult.SetSuccessAndClearError();

            return Json(operationResult);
        }

        //[HttpPost]
        public IActionResult AddToCatalog([FromBody] AddToCatalogVM addVM)
        {
            SetCurUserAndStore(addVM);

            var operationResult = catalogService.MergeProductWithCatalogs(addVM.ProductID, addVM.StoreID, addVM.CatalogIDs, addVM.ColorIDs);

            if(operationResult.Status)
            {
                return Index(addVM.ProductID);
            }

            return Json(operationResult);
        }

        public IActionResult RemoveFromCatalogForSort(int productInCatalogID, int catalogId)
        {
            var result = catalogService.RemoveFromCatalog(productInCatalogID, this.storeAccountID);
 
            return sort(catalogId);
        }

        public IActionResult MoveToTop(int productInCatalogID, int catalogId)
        {
            var result = catalogService.MoveToTop(productInCatalogID, this.storeAccountID);

            return sort(catalogId);
        }

        public IActionResult RemoveFromCatalog(int productInCatalogID)
        {
            var result = catalogService.RemoveFromCatalog(productInCatalogID, this.storeAccountID);

            if (result.Status)
            {
                return Index(result.Result?.Select(c=>c.ProductID).FirstOrDefault() ?? 0);
            }

            return Json(result);
        }

        public IActionResult RemoveFromAllCatalogs(int productId)
        {
            var result  = catalogService.RemoveFromAllCatalogs(productId, this.storeAccountID);

            if (result.Status)
            {
                return Index(result.Result?.Select(c => c.ProductID).FirstOrDefault() ?? 0);
            }

            return Json(result);
        }



    }
}
