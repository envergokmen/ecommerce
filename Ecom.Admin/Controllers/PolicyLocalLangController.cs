using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.PolicyLocalLangs;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class PolicyLocalLangController : EcomAdminBaseController
    {

        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly PolicyLocalLangService policyLocalLangService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public PolicyLocalLangController(PolicyLocalLangService _policyLocalLangService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.policyLocalLangService = _policyLocalLangService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] PolicyLocalLangCreateEditVM Policy)
        {
            SetCurUserAndStore(Policy);

            var operationResult = policyLocalLangService.Save(Policy);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id, int policyId = -1)
        {
            var operationResult = policyLocalLangService.Get(Id, this.storeAccountID, policyId);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(policyLocalLangService.Delete(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10, int? policyId = null)
        {
            PagedOperationListResult<PolicyLocalLangCreateEditVM> operationResult = policyLocalLangService.GetAll(this.storeAccountID, page, searchKeyword, pageSize, policyId);
            return Json(operationResult);
        }

    }
}
