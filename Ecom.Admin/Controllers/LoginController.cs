using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class LoginController : Controller
    {
        IHttpContextAccessor _contextAccessor;
        IMemberShipService memberShipService;

        public LoginController(IMemberShipService _memberShipService)
        { 
            this.memberShipService = _memberShipService;
        }

        //[HttpPost]
        public IActionResult Index([FromBody] LoginViewModel loginModel)
        {

            if (loginModel != null) loginModel.LoginType = UserType.StoreManager;

            OperationResult<UserViewModel> loginResult = memberShipService.Login(loginModel);
            return Json(loginResult);
        }


    }
}
