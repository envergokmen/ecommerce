using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.EmailTemplates;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class EmailTemplateController : EcomAdminBaseController
    {
    
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly EmailTemplateService emailTemplateService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public EmailTemplateController(EmailTemplateService _emailTemplateService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.emailTemplateService = _emailTemplateService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] EmailTemplateCreateEditVM EmailTemplate)
        {
            SetCurUserAndStore(EmailTemplate);

            var operationResult = emailTemplateService.Save(EmailTemplate);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id)
        { 
            var operationResult = emailTemplateService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }

        public IActionResult GetName(int Id)
        {
            var operationResult = emailTemplateService.Get(Id, this.storeAccountID);
            return Content(operationResult.Result.Name);
        }

        public IActionResult Delete(int Id)
        {
            return Json(emailTemplateService.Delete(Id, this.storeAccountID));
        }
       
        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            PagedOperationListResult<EmailTemplateCreateEditVM> operationResult = emailTemplateService.GetAll(this.storeAccountID, page, searchKeyword, pageSize);
            return Json(operationResult);
        }

    }
}
