using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.MvcCore.Extensions;
using Ecom.Services;
using Ecom.ViewModels.Brands;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class BrandController : EcomAdminBaseController
    {
        private readonly BrandService brandService;
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
       
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public BrandController(IHttpContextAccessor _contextAccessor, IHostingEnvironment env, BrandService _brandService, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            brandService = _brandService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] BrandCreateEditVM brand)
        {
            SetCurUserAndStore(brand);

            if (brand != null && brand.file!=null)
            {
                brand.ImagePath = FileUploadHelpler.SaveFilFromHttp(Request.IsLocal(), brand.file, rootPath, Utils.FormatUrl(this.storeName), "Brands");
            }
         
            var operationResult = brandService.Save(brand);

            return Json(operationResult);
        }

        public IActionResult Edit(int Id)
        {
            //System.Threading.Thread.Sleep(500);
            
            var operationResult = brandService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(brandService.Delete(Id, this.storeAccountID));
        }
        public IActionResult DeleteImage(int Id)
        {
            return Json(brandService.DeleteImage(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            PagedOperationListResult<BrandCreateEditVM> operationResult = brandService.GetAll(this.storeAccountID, page, searchKeyword, pageSize);
            return Json(operationResult);
        }

    }
}
