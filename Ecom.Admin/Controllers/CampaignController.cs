using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.Campaigns;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class CampaignController : EcomAdminBaseController
    {
    
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly CampaignService campaignService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public CampaignController(CampaignService _campaignService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.campaignService = _campaignService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] CampaignCreateEditVM Campaign)
        {
            SetCurUserAndStore(Campaign);

            var operationResult = campaignService.Save(Campaign);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id)
        { 
            var operationResult = campaignService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(campaignService.Delete(Id, this.storeAccountID));
        }
       
        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            PagedOperationListResult<CampaignCreateEditVM> operationResult = campaignService.GetAll(this.storeAccountID, page, searchKeyword, pageSize);
            return Json(operationResult);
        }

    }
}
