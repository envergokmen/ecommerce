using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.MvcCore.Extensions;
using Ecom.Services;
using Ecom.ViewModels.Colors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class ColorController : EcomAdminBaseController
    {
        private readonly ColorService colorService;
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
       
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public ColorController(IHttpContextAccessor _contextAccessor, IHostingEnvironment env, ColorService _colorService, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            colorService = _colorService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] ColorCreateEditVM color)
        {
            SetCurUserAndStore(color);

            if (color != null && color.file != null)
            {
                color.PatternPath = FileUploadHelpler.SaveFilFromHttp(Request.IsLocal(), color.file, rootPath, Utils.FormatUrl(this.storeName), "Colors");
            }

            var operationResult = colorService.Save(color);

            return Json(operationResult);
        }

        public IActionResult Edit(int Id)
        {
            //System.Threading.Thread.Sleep(500);
            
            var operationResult = colorService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(colorService.Delete(Id, this.storeAccountID));
        }

        public IActionResult DeleteImage(int Id)
        {
            return Json(colorService.DeleteImage(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageColor = 10)
        {
            PagedOperationListResult<ColorCreateEditVM> operationResult = colorService.GetAll(this.storeAccountID, page, searchKeyword, pageColor);
            return Json(operationResult);
        }

    }
}
