using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.MvcCore.Extensions;
using Ecom.MvcCore.Product;
using Ecom.Services;
using Ecom.Services.Bus;
using Ecom.ViewModels.Products;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;

namespace Ecom.Admin.Controllers
{
    public class ProductController : EcomAdminBaseController
    {

        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly ProductService productService;
        private readonly ProductImageHelper imageHelper;
        public RabbitMQService busService = new RabbitMQService();

        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public ProductController(RabbitMQService _busService ,ProductImageHelper _imageHelper, ProductService _productService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.productService = _productService;
            _env = env;
            rootPath = _env.ContentRootPath;
            imageHelper = _imageHelper;
            busService = _busService;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] ProductCreateEditVM Product)
        {
            SetCurUserAndStore(Product);


            if (Product != null && Product.files != null)
            {
                Product.Images = imageHelper.ParseProductImages(Product, Product.files);

                foreach (var item in Product.Images)
                {
                    var formattedProductCode = Utils.FormatUrl(Product.ProductCode);

                    item.ImagePath = FileUploadHelpler.SaveFilFromHttp(Request.IsLocal(), item.file, rootPath, Utils.FormatUrl(this.storeName), "Products", formattedProductCode);
                }
            }
            //    Product.Images = productService.ParseProductImages(Product.files);

            //    //foreach (var item in Product.files)
            //    //{
            //    //    Product.ImagePath = FileUploadHelpler.SaveFilFromHttp(Product.file, rootPath, Utils.FormatUrl(this.storeName), "Brands");
            //    //}


            //}

            var operationResult = productService.Save(Product);
            return Json(operationResult);
        }


        public IActionResult Upload([FromForm] UploadProductVM uploadVM)
        {
            SetCurUserAndStore(uploadVM);
            List<string> uploadedProducts = new List<string>();
            if (uploadVM != null && uploadVM.files != null)
            {
                foreach (var item in uploadVM.files)
                {
                    var formattedDate = string.Concat(DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year);
                    var uploaded = FileUploadHelpler.UploadProductFile(item, rootPath, Utils.FormatUrl(this.storeName), "UploadProducts", formattedDate);
                    uploadedProducts.Add( uploaded.Replace(@"\\", @"\") );
                }
            }

            busService.Publish(QueueItemType.ProductUploaded, JsonConvert.SerializeObject(uploadedProducts), uploadVM.StoreID);
            StandartOperationResult operationResult = new StandartOperationResult();
            operationResult.SetSuccessAndClearError(_localizer["Your files has been uploaded, we will process your products soon. Please check in 10 minutes..."]);

            return Json(operationResult);
        }

        public IActionResult SaveImage([FromBody] ProductImageVM image)
        {
            SetCurUserAndStore(image);

            var operationResult = productService.UpdateImage(image);
            return Json(operationResult);
        }

        public IActionResult GetName(int Id)
        {
            var operationResult = productService.Get(Id, this.storeAccountID);
            return Content(operationResult.Result.ProductCode);
        }

        public IActionResult Edit(int Id)
        {
            var operationResult = productService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }

        public IActionResult DeleteImage(int Id)
        {
            return Json(productService.DeleteImage(Id, this.storeAccountID));
        }

        public IActionResult DeleteAllImages(int productId)
        {
            return Json(productService.DeleteAllImages(productId, this.storeAccountID));
        }

        public IActionResult Delete(int Id)
        {
            return Json(productService.Delete(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            PagedOperationListResult<ProductCreateEditVM> operationResult = productService.GetAll(this.storeAccountID, page, searchKeyword, pageSize);
            return Json(operationResult);
        }

    }
}