using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.ContentPageLocalLangs;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class ContentPageLocalLangController : EcomAdminBaseController
    {

        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly ContentPageLocalLangService contentPageLocalLangService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public ContentPageLocalLangController(ContentPageLocalLangService _contentPageLocalLangService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.contentPageLocalLangService = _contentPageLocalLangService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] ContentPageLocalLangCreateEditVM ContentPage)
        {
            SetCurUserAndStore(ContentPage);

            var operationResult = contentPageLocalLangService.Save(ContentPage);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id, int contentPageId = -1)
        {
            var operationResult = contentPageLocalLangService.Get(Id, this.storeAccountID, contentPageId);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(contentPageLocalLangService.Delete(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10, int? contentPageId = null)
        {
            PagedOperationListResult<ContentPageLocalLangCreateEditVM> operationResult = contentPageLocalLangService.GetAll(this.storeAccountID, page, searchKeyword, pageSize, contentPageId);
            return Json(operationResult);
        }

    }
}
