using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.CargoPrices;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class CargoPriceController : EcomAdminBaseController
    {
    
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly CargoPriceService cargopriceService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public CargoPriceController(CargoPriceService _cargopriceService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.cargopriceService = _cargopriceService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] CargoPriceCreateEditVM CargoPrice)
        {
            SetCurUserAndStore(CargoPrice);

            var operationResult = cargopriceService.Save(CargoPrice);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id)
        { 
            var operationResult = cargopriceService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(cargopriceService.Delete(Id, this.storeAccountID));
        }
       
        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            PagedOperationListResult<CargoPriceCreateEditVM> operationResult = cargopriceService.GetAll(this.storeAccountID, page, searchKeyword, pageSize);
            return Json(operationResult);
        }

    }
}
