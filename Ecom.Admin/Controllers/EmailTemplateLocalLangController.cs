using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.EmailTemplateLocalLangs;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class EmailTemplateLocalLangController : EcomAdminBaseController
    {

        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly EmailTemplateLocalLangService emailTemplateLocalLangService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public EmailTemplateLocalLangController(EmailTemplateLocalLangService _emailTemplateLocalLangService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.emailTemplateLocalLangService = _emailTemplateLocalLangService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] EmailTemplateLocalLangCreateEditVM EmailTemplate)
        {
            SetCurUserAndStore(EmailTemplate);

            var operationResult = emailTemplateLocalLangService.Save(EmailTemplate);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id, int emailTemplateId = -1)
        {
            var operationResult = emailTemplateLocalLangService.Get(Id, this.storeAccountID, emailTemplateId);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(emailTemplateLocalLangService.Delete(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10, int? emailTemplateId = null)
        {
            PagedOperationListResult<EmailTemplateLocalLangCreateEditVM> operationResult = emailTemplateLocalLangService.GetAll(this.storeAccountID, page, searchKeyword, pageSize, emailTemplateId);
            return Json(operationResult);
        }

    }
}
