using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.Domains;
using Ecom.ViewModels.Stores;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class StoreAccountController : EcomAdminBaseController
    {
    
        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly StoreAccountService storeAccountService;

        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public StoreAccountController(StoreAccountService _storeAccountService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer; 
            _env = env;
            rootPath = _env.ContentRootPath;
            storeAccountService = _storeAccountService;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] StoreAccountCreateEditVM domain)
        {
            SetCurUserAndStore(domain);

            var operationResult = storeAccountService.Save(domain);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id)
        { 
            var operationResult = storeAccountService.Get(Id);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(storeAccountService.Delete(Id, this.storeAccountID));
        }
       
        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            PagedOperationListResult<StoreAccountCreateEditVM> operationResult = storeAccountService.GetAll(page, searchKeyword, pageSize, this.user.ID);
            return Json(operationResult);
        }

    }
}
