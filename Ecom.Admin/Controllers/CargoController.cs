using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.MvcCore.Extensions;
using Ecom.Services;
using Ecom.ViewModels.Cargoes;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class CargoController : EcomAdminBaseController
    {

        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly CargoService seasonService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public CargoController(CargoService _seasonService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.seasonService = _seasonService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] CargoCreateEditVM Cargo)
        {
            SetCurUserAndStore(Cargo);

            if (Cargo != null && Cargo.file != null)
            {
                Cargo.LogoPath = FileUploadHelpler.SaveFilFromHttp(Request.IsLocal(), Cargo.file, rootPath, Utils.FormatUrl(this.storeName), "Cargoes");
            }

            var operationResult = seasonService.Save(Cargo);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id)
        {
            var operationResult = seasonService.Get(Id, this.storeAccountID);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(seasonService.Delete(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10)
        {
            PagedOperationListResult<CargoCreateEditVM> operationResult = seasonService.GetAll(this.storeAccountID, page, searchKeyword, pageSize);
            return Json(operationResult);
        }

    }
}
