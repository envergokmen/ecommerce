using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.WebPosInstallments;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class WebPosInstallmentController : EcomAdminBaseController
    {

        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly WebPosInstallmentService webPosInstallmentService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public WebPosInstallmentController(WebPosInstallmentService _webPosInstallmentService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.webPosInstallmentService = _webPosInstallmentService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] WebPosInstallmentCreateEditVM Policy)
        {
            SetCurUserAndStore(Policy);

            var operationResult = webPosInstallmentService.Save(Policy);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id, int webposId = -1)
        {
            var operationResult = webPosInstallmentService.Get(Id, this.storeAccountID, webposId);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(webPosInstallmentService.Delete(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10, int? webposId = null)
        {
            PagedOperationListResult<WebPosInstallmentCreateEditVM> operationResult = webPosInstallmentService.GetAll(this.storeAccountID, page, searchKeyword, pageSize, webposId);
            return Json(operationResult);
        }

    }
}
