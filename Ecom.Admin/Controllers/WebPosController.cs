using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.MvcCore.Extensions;
using Ecom.Services;
using Ecom.ViewModels.WebPoses;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class WebPosController : EcomAdminBaseController
    {

        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly WebPosService webposeservice;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public WebPosController(WebPosService _webposeservice, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.webposeservice = _webposeservice;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] WebPosCreateEditVM WebPos)
        {
            SetCurUserAndStore(WebPos);

            if (WebPos != null && WebPos.file != null)
            {
                WebPos.LogoPath = FileUploadHelpler.SaveFilFromHttp(Request.IsLocal(), WebPos.file, rootPath, Utils.FormatUrl(this.storeName), "webposes");
            }

            var operationResult = webposeservice.Save(WebPos);
            return Json(operationResult);
        }

        public IActionResult GetName(int Id)
        {
            var operationResult = webposeservice.Get(Id, this.storeAccountID, -1);
            return Content(operationResult.Result.Name);
        }

        public IActionResult Edit(int Id, int paymentId = -1)
        {
            var operationResult = webposeservice.Get(Id, this.storeAccountID, paymentId);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(webposeservice.Delete(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10, int? paymentId = null)
        {
            PagedOperationListResult<WebPosCreateEditVM> operationResult = webposeservice.GetAll(this.storeAccountID, page, searchKeyword, pageSize, paymentId);
            return Json(operationResult);
        }

    }
}
