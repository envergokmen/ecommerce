using System.Threading.Tasks;
using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Services;
using Ecom.ViewModels.ProductLocalLangs;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Ecom.Admin.Controllers
{
    public class ProductLocalLangController : EcomAdminBaseController
    {

        private readonly IStringLocalizer<SharedAdminLayout> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedlocalizer;
        private readonly IStringLocalizer<SharedDataResource> _sharedDatalocalizer;
        private readonly ProductDetailService productLocalLangService;
        private readonly ServiceUtils _serviceUtils;
        private IHostingEnvironment _env;
        private readonly string rootPath;

        public ProductLocalLangController(ProductDetailService _productLocalLangService, IHttpContextAccessor _contextAccessor, IHostingEnvironment env, IStringLocalizer<SharedDataResource> sharedDatalocalizer, ServiceUtils serviceUtils, IStringLocalizer<SharedResource> sharedlocalizer, IStringLocalizer<SharedAdminLayout> localizer, IMemberShipService _memberShipService, DomainService _domainService) : base(_memberShipService, _domainService, _contextAccessor)
        {
            _localizer = localizer;
            _sharedlocalizer = sharedlocalizer;
            _serviceUtils = serviceUtils;
            _sharedDatalocalizer = sharedDatalocalizer;
            this.productLocalLangService = _productLocalLangService;
            _env = env;
            rootPath = _env.ContentRootPath;
        }

        //[HttpPost]
        public IActionResult CreateOrEdit([FromForm] ProductLocalLangCreateEditVM Product)
        {
            SetCurUserAndStore(Product);

            var operationResult = productLocalLangService.Save(Product);
            return Json(operationResult);
        }

        public IActionResult Edit(int Id, int productId = -1)
        {
            var operationResult = productLocalLangService.Get(Id, this.storeAccountID, productId);
            return Json(operationResult);
        }

        public IActionResult Delete(int Id)
        {
            return Json(productLocalLangService.Delete(Id, this.storeAccountID));
        }

        public IActionResult Index(int page = 1, string searchKeyword = null, int pageSize = 10, int? productId = null)
        {
            PagedOperationListResult<ProductLocalLangCreateEditVM> operationResult = productLocalLangService.GetAll(this.storeAccountID, page, searchKeyword, pageSize, productId);
            return Json(operationResult);
        }

    }
}
