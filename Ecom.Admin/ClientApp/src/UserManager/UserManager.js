class UserManager {

    getLoggedInUser() {

        var user = window.localStorage.getItem("user");
        if (user) {
            return JSON.parse(user);
        }

        return null;
    }
    getLoggedInName() {

        var user = this.getLoggedInUser();
        if (user) {
           return user.Name;
        }
        return null;
    }
    getLoggedInUserName() {

        var user = this.getLoggedInUser();
        if (user) {
           return user.UserName;
        }
        return null;
    }

    setLoggedInUser(user) {

        if (typeof user === 'string' || user instanceof String) {
            window.localStorage.setItem("user", user);
        }
        else {
            window.localStorage.setItem("user", JSON.stringify(user));
        }
    }

    removeLoggedInUser() {
        window.localStorage.removeItem("user")
    }

}
export default UserManager;