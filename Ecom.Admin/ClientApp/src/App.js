﻿import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import LayoutBlank from './components/LayoutBlank';
import Home from './components/Home';
import SignIn from './components/SignIn/SignIn';
import Counter from './components/Counter';
import FetchData from './components/FetchData';
import { library } from '@fortawesome/fontawesome-svg-core';
// https://fontawesome.com/icons?d=gallery&s=solid&m=free
import { faDragon, faGem, faSearch, faAngleDown, faBookmark,  faBook,  faIgloo, faCertificate, faCheckSquare, faCoffee, faEnvelope,
    faSwimmingPool, faSitemap, faTruck,  faBaby, faCamera, faCameraRetro, faCandyCane, faBullhorn, faComments,
    faTicketAlt, faMoneyCheckAlt, faBuilding, faMoneyCheck, faObjectUngroup, faNewspaper, faEnvelopeOpenText,
    faCommentDots, faCog, faEthernet, faDollarSign, faMoneyBillAlt, faGlobeAfrica, faUserCog, faUsers, faCube, faDiceD20,
    faShieldAlt, faChartLine,
    faGift} from '@fortawesome/free-solid-svg-icons';
import Orders from './components/Orders/Orders';
import Brands from './components/Brands/Brands';
import BrandsNew from './components/Brands/BrandsNew';


import Banks from './components/Banks/Banks';
import BanksNew from './components/Banks/BanksNew';

import Domains from './components/Domains/Domains';
import DomainsNew from './components/Domains/DomainsNew';
import {
    Switch
} from 'react-router-dom'
import StoreAccountsNew from './components/StoreAccount/StoreAccountsNew';
import StoreAccounts from './components/StoreAccount/StoreAccounts';
import Sizes from './components/Sizes/Sizes';
import SizesNew from './components/Sizes/SizesNew';
import ColorsNew from './components/Colors/ColorsNew';
import Colors from './components/Colors/Colors';
import ColorGlobalsNew from './components/ColorGlobal/ColorGlobalsNew';
import ColorGlobals from './components/ColorGlobal/ColorGlobals';
import SeasonsNew from './components/Season/SeasonsNew';
import Seasons from './components/Season/Seasons';


import CargoesNew from './components/Cargo/CargoesNew';
import Cargoes from './components/Cargo/Cargoes';

import PoliciesNew from './components/Policy/PoliciesNew';
import Policies from './components/Policy/Policies';

import PolicyLocalLangsNew from './components/PolicyLocalLang/PolicyLocalLangsNew';
import PolicyLocalLangs from './components/PolicyLocalLang/PolicyLocalLangs';


import ProductsNew from './components/Product/ProductsNew';
import ProductsUpload from './components/Product/ProductsUpload';
import Products from './components/Product/Products';

import CatalogsNew from './components/Catalogs/CatalogsNew';
import Catalogs from './components/Catalogs/Catalogs';
import CatalogLocalLangsNew from './components/CatalogLocalLang/CatalogLocalLangsNew';
import CatalogLocalLangs from './components/CatalogLocalLang/CatalogLocalLangs';


import ContentPagesNew from './components/ContentPages/ContentPagesNew';
import ContentPages from './components/ContentPages/ContentPages';
import ContentPageLocalLangsNew from './components/ContentPageLocalLang/ContentPageLocalLangsNew';
import ContentPageLocalLangs from './components/ContentPageLocalLang/ContentPageLocalLangs';

import CampaignsNew from './components/Campaigns/CampaignsNew';
import Campaigns from './components/Campaigns/Campaigns';

import BannersNew from './components/Banners/BannersNew';
import Banners from './components/Banners/Banners';
import CouponsNew from './components/Coupon/CouponsNew';
import Coupons from './components/Coupon/Coupons';

import SendingProviderNew from './components/SendingProvider/SendingProvidersNew';
import SendingProviders from './components/SendingProvider/SendingProviders';

import EmailTemplateNew from './components/EmailTemplates/EmailTemplatesNew';
import EmailTemplates from './components/EmailTemplates/EmailTemplates';

import ProductLocalLangsNew from './components/ProductLocalLang/ProductLocalLangsNew';
import ProductLocalLangs from './components/ProductLocalLang/ProductLocalLangs';
import StocksNew from './components/Stock/StocksNew';
import Stocks from './components/Stock/Stocks';
import EmailTemplateLocalLangs from './components/EmailTemplateLocalLang/EmailTemplateLocalLangs';
import EmailTemplateLocalLangsNew from './components/EmailTemplateLocalLang/EmailTemplateLocalLangsNew';
import CargoPricesNew from './components/CargoPrice/CargoPricesNew';
import CargoPrices from './components/CargoPrice/CargoPrices';

import PricesNew from './components/Price/PricesNew';
import Prices from './components/Price/Prices';
import ProductCatalogs from './components/ProductCatalog/ProductCatalogs';
import PaymentsNew from './components/Payment/PaymentsNew';
import Payments from './components/Payment/Payments';
import WebPosesNew from './components/WebPos/WebPosesNew';
import WebPoses from './components/WebPos/WebPoses';

import WebPosInstallmentsNew from './components/WebPosInstallment/WebPosInstallmentsNew';
import WebPosInstallments from './components/WebPosInstallment/WebPosInstallments';
import SortCatalog from './components/SortCatalog/SortCatalog';
import ProductDepartmentsNew from './components/ProductDepartment/ProductDepartmentNew';
import ProductDepartments from './components/ProductDepartment/ProductDepartments';
import ProductStylesNew from './components/ProductStyle/ProductStyleNew';
import ProductStyles from './components/ProductStyle/ProductStyles';

library.add( 
    faSwimmingPool, faSitemap, faTruck,  faBaby, faCameraRetro, faCandyCane, faBullhorn,
    faTicketAlt, faMoneyCheckAlt, faBuilding, faMoneyCheck, faObjectUngroup, faNewspaper, faEnvelopeOpenText,
    faCommentDots, faCog, faEthernet, faDollarSign, faMoneyBillAlt, faGlobeAfrica, faUserCog, faUsers,
    faShieldAlt, faGift, faCamera, faChartLine, faComments, faCube, faDiceD20, 
    faDragon, faGem, faSearch, faAngleDown, faBook, faBookmark, faEnvelope, faIgloo, faCertificate, faCheckSquare, faCoffee);

export default () => (
    <Switch>
        {/* <FontAwesomeIcon icon="angle-down" /> */}
        <LayoutBlank exact path='/sign-in/:lang?' >
            <Switch>
                <Route path='/sign-in/:lang?' component={SignIn} />
            </Switch>
        </LayoutBlank>

        <Layout>
            <Switch>

                <Route path='/products/:lang/new' component={ProductsNew} />
                <Route path='/products/:lang/edit/:id' component={ProductsNew} />
                <Route path='/products/:lang/upload' component={ProductsUpload} />
                <Route path='/products/:lang?/:page?' component={Products} />

                <Route path='/banks/:lang/new' component={BanksNew} />
                <Route path='/banks/:lang/edit/:id' component={BanksNew} />
                <Route path='/banks/:lang?/:page?' component={Banks} />

                <Route path='/brands/:lang/new' component={BrandsNew} />
                <Route path='/brands/:lang/edit/:id' component={BrandsNew} />
                <Route path='/brands/:lang?/:page?' component={Brands} />

                <Route path='/domains/:lang/new' component={DomainsNew} />
                <Route path='/domains/:lang/edit/:id' component={DomainsNew} />
                <Route path='/domains/:lang?/:page?' component={Domains} />


                <Route path='/prices/:lang/new/product/:productId/stock/:stockId' component={PricesNew} />
                <Route path='/prices/:lang/new/product/:productId' component={PricesNew} />
                <Route path='/prices/:lang/product/:productId' component={Prices} />
                <Route path='/prices/:lang/product/:productId/stock:/stockId' component={Prices} />
                <Route path='/prices/:lang/new' component={PricesNew} />
                <Route path='/prices/:lang/edit/:id/product/:productId' component={PricesNew} />
                <Route path='/prices/:lang/edit/:id' component={PricesNew} />
                <Route path='/prices/:lang?/:page?' component={Prices} />

                 
                <Route path='/productlocallangs/:lang/new/product/:productId' component={ProductLocalLangsNew} />
                <Route path='/productlocallangs/:lang/translations/:productId' component={ProductLocalLangs} />
                <Route path='/productlocallangs/:lang/new' component={ProductLocalLangsNew} />
                <Route path='/productlocallangs/:lang/edit/:id/product/:productId' component={ProductLocalLangsNew} />
                <Route path='/productlocallangs/:lang/edit/:id' component={ProductLocalLangsNew} />
                <Route path='/productlocallangs/:lang?/:page?' component={ProductLocalLangs} />
products
                <Route path='/productcatalogs/:lang/:productId' component={ProductCatalogs} />
                <Route path='/catalogsort/:lang/:catalogId' component={SortCatalog} />

                <Route path='/stocks/:lang/new/product/:productId' component={StocksNew} />
                <Route path='/stocks/:lang/product/:productId' component={Stocks} />
                <Route path='/stocks/:lang/new' component={StocksNew} />
                <Route path='/stocks/:lang/edit/:id/product/:productId' component={StocksNew} />
                <Route path='/stocks/:lang/edit/:id' component={StocksNew} />
                <Route path='/stocks/:lang?/:page?' component={Stocks} />


                <Route path='/webPoses/:lang/new/payment/:paymentId' component={WebPosesNew} />
                <Route path='/webPoses/:lang/payment/:paymentId' component={WebPoses} />
                <Route path='/webPoses/:lang/new' component={WebPosesNew} />
                <Route path='/webPoses/:lang/edit/:id/payment/:paymentId' component={WebPosesNew} />
                <Route path='/webPoses/:lang/edit/:id' component={WebPosesNew} />
                <Route path='/webPoses/:lang?/:page?' component={WebPoses} />

                <Route path='/webPosInstallments/:lang/new/webpos/:webposId' component={WebPosInstallmentsNew} />
                <Route path='/webPosInstallments/:lang/webpos/:webposId' component={WebPosInstallments} />
                <Route path='/webPosInstallments/:lang/new' component={WebPosInstallmentsNew} />
                <Route path='/webPosInstallments/:lang/edit/:id/webpos/:webposId' component={WebPosInstallmentsNew} />
                <Route path='/webPosInstallments/:lang/edit/:id' component={WebPosInstallmentsNew} />
                <Route path='/webPosInstallments/:lang?/:page?' component={WebPosInstallments} />



                <Route path='/policies/:lang/type/:type/new' component={PoliciesNew} />
                <Route path='/policies/:lang/new' component={PoliciesNew} />
                <Route path='/policies/:lang/edit/:id' component={PoliciesNew} />
                <Route path='/policies/:lang/type/:type?/:page?' component={Policies} />

                <Route path='/policylocallangs/:lang/new/policy/:policyId' component={PolicyLocalLangsNew} />
                <Route path='/policylocallangs/:lang/translations/:policyId' component={PolicyLocalLangs} />
                <Route path='/policylocallangs/:lang/new' component={PolicyLocalLangsNew} />
                <Route path='/policylocallangs/:lang/edit/:id/policy/:policyId' component={PolicyLocalLangsNew} />
                <Route path='/policylocallangs/:lang/edit/:id' component={PolicyLocalLangsNew} />
                <Route path='/policylocallangs/:lang?/:page?' component={PolicyLocalLangs} />

                <Route path='/storeaccounts/:lang/new' component={StoreAccountsNew} />
                <Route path='/storeaccounts/:lang/edit/:id' component={StoreAccountsNew} />
                <Route path='/storeaccounts/:lang?/:page?' component={StoreAccounts} />

                <Route path='/cataloglocallangs/:lang/new/catalog/:catalogId' component={CatalogLocalLangsNew} />
                <Route path='/cataloglocallangs/:lang/translations/:catalogId' component={CatalogLocalLangs} />
                <Route path='/cataloglocallangs/:lang/new' component={CatalogLocalLangsNew} />
                <Route path='/cataloglocallangs/:lang/edit/:id' component={CatalogLocalLangsNew} />
                <Route path='/cataloglocallangs/:lang?/:page?' component={CatalogLocalLangs} />

                <Route path='/catalogs/:lang/new' component={CatalogsNew} />
                <Route path='/catalogs/:lang/edit/:id' component={CatalogsNew} />
                <Route path='/catalogs/:lang?/:page?' component={Catalogs} />


                <Route path='/contentpages/:lang/new' component={ContentPagesNew} />
                <Route path='/contentpages/:lang/edit/:id' component={ContentPagesNew} />
                <Route path='/contentpages/:lang?/:page?' component={ContentPages} />

                <Route path='/contentpagelocallangs/:lang/new/contentpage/:contentpageId' component={ContentPageLocalLangsNew} />
                <Route path='/contentpagelocallangs/:lang/translations/:contentpageId' component={ContentPageLocalLangs} />
                <Route path='/contentpagelocallangs/:lang/new' component={ContentPageLocalLangsNew} />
                <Route path='/contentpagelocallangs/:lang/edit/:id/contentpage/:contentpageId' component={ContentPageLocalLangsNew} />
                <Route path='/contentpagelocallangs/:lang/edit/:id' component={ContentPageLocalLangsNew} />
                <Route path='/contentpagelocallangs/:lang?/:page?' component={ContentPageLocalLangs} />


                <Route path='/emailtemplatelocallangs/:lang/new/emailtemplate/:emailtemplateId' component={EmailTemplateLocalLangsNew} />
                <Route path='/emailtemplatelocallangs/:lang/translations/:emailtemplateId' component={EmailTemplateLocalLangs} />
                <Route path='/emailtemplatelocallangs/:lang/new' component={EmailTemplateLocalLangsNew} />
                <Route path='/emailtemplatelocallangs/:lang/edit/:id/emailtemplate/:emailtemplateId' component={EmailTemplateLocalLangsNew} />
                <Route path='/emailtemplatelocallangs/:lang/edit/:id' component={EmailTemplateLocalLangsNew} />
                <Route path='/emailtemplatelocallangs/:lang?/:page?' component={EmailTemplateLocalLangs} />


                <Route path='/campaigns/:lang/new' component={CampaignsNew} />
                <Route path='/campaigns/:lang/edit/:id' component={CampaignsNew} />
                <Route path='/campaigns/:lang?/:page?' component={Campaigns} />

                <Route path='/colors/:lang/new' component={ColorsNew} />
                <Route path='/colors/:lang/edit/:id' component={ColorsNew} />
                <Route path='/colors/:lang?/:page?' component={Colors} />

                <Route path='/colorglobals/:lang/new' component={ColorGlobalsNew} />
                <Route path='/colorglobals/:lang/edit/:id' component={ColorGlobalsNew} />
                <Route path='/colorglobals/:lang?/:page?' component={ColorGlobals} />


                <Route path='/banners/:lang/new' component={BannersNew} />
                <Route path='/banners/:lang/edit/:id' component={BannersNew} />
                <Route path='/banners/:lang?/:page?' component={Banners} />

                <Route path='/seasons/:lang/new' component={SeasonsNew} />
                <Route path='/seasons/:lang/edit/:id' component={SeasonsNew} />
                <Route path='/seasons/:lang?/:page?' component={Seasons} />

                <Route path='/departments/:lang/new' component={ProductDepartmentsNew} />
                <Route path='/departments/:lang/edit/:id' component={ProductDepartmentsNew} />
                <Route path='/departments/:lang?/:page?' component={ProductDepartments} />

                <Route path='/styles/:lang/new' component={ProductStylesNew} />
                <Route path='/styles/:lang/edit/:id' component={ProductStylesNew} />
                <Route path='/styles/:lang?/:page?' component={ProductStyles} />

                <Route path='/payments/:lang/new' component={PaymentsNew} />
                <Route path='/payments/:lang/edit/:id' component={PaymentsNew} />
                <Route path='/payments/:lang?/:page?' component={Payments} />

                <Route path='/cargoprices/:lang/new' component={CargoPricesNew} />
                <Route path='/cargoprices/:lang/edit/:id' component={CargoPricesNew} />
                <Route path='/cargoprices/:lang?/:page?' component={CargoPrices} />


                <Route path='/cargoes/:lang/new' component={CargoesNew} />
                <Route path='/cargoes/:lang/edit/:id' component={CargoesNew} />
                <Route path='/cargoes/:lang?/:page?' component={Cargoes} />

                <Route path='/coupons/:lang/new' component={CouponsNew} />
                <Route path='/coupons/:lang/edit/:id' component={CouponsNew} />
                <Route path='/coupons/:lang?/:page?' component={Coupons} />

                <Route path='/sizes/:lang/new' component={SizesNew} />
                <Route path='/sizes/:lang/edit/:id' component={SizesNew} />
                <Route path='/sizes/:lang?/:page?' component={Sizes} />


                <Route path='/sendingproviders/:lang/new' component={SendingProviderNew} />
                <Route path='/sendingproviders/:lang/edit/:id' component={SendingProviderNew} />
                <Route path='/sendingproviders/:lang?/:page?' component={SendingProviders} />

                <Route path='/emailtemplates/:lang/new' component={EmailTemplateNew} />
                <Route path='/emailtemplates/:lang/edit/:id' component={EmailTemplateNew} />
                <Route path='/emailtemplates/:lang?/:page?' component={EmailTemplates} />


                <Route path='/orders/:lang?' component={Orders} />
                <Route path='/counter' component={Counter} />
                <Route path='/fetchdata/:startDateIndex?' component={FetchData} />

                {/* <Route exact path='/:lang?' component={Home} /> */}

            </Switch>
        </Layout>
    </Switch>
);
