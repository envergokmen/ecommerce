﻿const searchProductType = 'SEARCH_ORDER';
const initialState = { lang: "tr",  isLoading: false, Products: [] };

export const actionCreators = {
   
    searchProduct: payload => async (dispatch, getState) => {
        dispatch({ type: searchProductType, payload });
    }
   
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === searchProductType) {

        return {
            ...state,
            MenuItems: action.payload

        };
    }
 
    return state;
};
