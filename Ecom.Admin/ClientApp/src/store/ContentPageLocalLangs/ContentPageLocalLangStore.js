﻿import Localizer from "../../Localizer/Localizer";
import axios from 'axios';

const searchContentPageType = 'SEARCH_CONTENTPAGELOCALLANG';
const contentPageFormInitialValuesType = 'CONTENTPAGELOCALLANG_FORM_INITIAL';
const formResultType = 'CONTENTPAGELOCALLANG_FORM_RESULT';
const deleteItemType = 'CONTENTPAGELOCALLANG_DELETE_ITEM';
const saveItemType = 'CONTENTPAGELOCALLANG_SAVE_ITEM';
const resetFormResultType = 'CONTENTPAGELOCALLANG_FORM_RESET';
const initialState = { lang: "tr", isLoading: false, contentPages: null, Page: 1, TotalPage: 1, TotalItem: 0, 
SearchKeyword: "", initial :{ PStatus:1} , operationResult:undefined, deleteResult:undefined, deleteImgResult:undefined};

var localizer = new Localizer();

export const actionCreators = {

    SearchContentPageLocalLangs: payload => async (dispatch, getState) => {

        var page = payload.page ? payload.page : 1;
        var searchKeyword = payload.searchKeyword ? payload.searchKeyword : "";
        var append = payload.append ? payload.append : false;
        var contentpageId = payload.contentpageId ? payload.contentpageId : null;

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/ContentPageLocalLang?page=${page}&searchKeyword=${searchKeyword}&contentpageId=${contentpageId}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });


        dispatch({ type: searchContentPageType, payload: { ...dataRes, append } });

        //Set bread crumb searchBox
        dispatch({ type: "SEARCH_INITIAL", payload: { searchAction: "SEARCH_CONTENTPAGELOCALLANG", actionUrl: `/${lang}/contentPage`, searchKeyWord: "" } });

    },
    FormResult: payload => async (dispatch, getState) => {
        dispatch({ type: formResultType, payload });
    },
    DeleteItem: Id => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/ContentPageLocalLang/delete?id=${Id}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });
        dispatch({ type: deleteItemType, payload: dataRes });

    },
    SaveItem: payload => async (dispatch, getState) => {
 
        dispatch({ type: "SET_LOADING", payload: true });

        var that = this;
        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/ContentPageLocalLang/CreateOrEdit`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: saveItemType, payload: response.data });
            })
            .catch((error) => {
                dispatch({ type: formResultType, payload: error.data });
                dispatch({ type: saveItemType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    },
    ResetFormResult: () => async (dispatch, getState) => {

        dispatch({ type: resetFormResultType, undefined });
    },
    GetInitialValues: (id, contentpageId) => async (dispatch, getState) => {

     
        // if(!id || id == 0){

        //     dispatch({ type: contentPageFormInitialValuesType, payload: { PStatus:1} });

        // }else{

            dispatch({ type: "SET_LOADING", payload: true });

            var lang = localizer.GetSelectedLang();
        const url = `${lang}/ContentPageLocalLang/edit/${id}?contentpageId=${contentpageId}`;
    
            const response = await fetch(url, {
                method: 'post',
            });
    
            var dataRes = await response.json();
    
            dispatch({ type: "SET_LOADING", payload: false });
    
            dispatch({ type: contentPageFormInitialValuesType, payload: dataRes.Result });
            
        // }

    
    },
 
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type == searchContentPageType) {

        const { Page, PageSize, TotalPage, TotalItem, SearchKeyword } = action.payload;

        var contentPages = action.payload.append ? [...state.contentPages, ...action.payload.Result] : action.payload.Result;

        return {
            ...state,
            contentPages: contentPages,
            Page: Page,
            PageSize: PageSize,
            TotalPage: TotalPage,
            TotalItem: TotalItem,
            SearchKeyword: SearchKeyword
        };

    }

    if (action.type == deleteItemType) {

        if (action.payload.Status) {
            return {
                ...state,
                contentPages: [...state.contentPages].filter(x => x.ID != action.payload.Result),
                deleteResult: action.payload
            };

        } else {

            return {
                ...state,
                deleteResult: action.payload
            };
        }

    }

    if (action.type == saveItemType) {

        return {
            ...state,
            saveResult: action.payload
        };

    }

    if (action.type == resetFormResultType) {

        return {
            ...state,
            operationResult: undefined,
            deleteResult: undefined,
            initial: undefined,
            saveResult: undefined
        };
    }
  
    if (action.type == formResultType) {

        return {
            ...state,
            operationResult: action.payload

        };
    }

    if (action.type == contentPageFormInitialValuesType) {

        return {
            ...state,
            initial: action.payload
        };
    }

    return state;
};
