﻿import Localizer from "../../Localizer/Localizer";
import axios from 'axios';

const searchColorGlobalType = 'SEARCH_COLORGLOBAL';
const colorglobalFormInitialValuesType = 'COLORGLOBAL_FORM_INITIAL';
const formResultType = 'COLORGLOBAL_FORM_RESULT';
const deleteItemType = 'COLORGLOBAL_DELETE_ITEM';
const saveItemType = 'COLORGLOBAL_SAVE_ITEM';
const resetFormResultType = 'COLORGLOBAL_FORM_RESET';
const initialState = { lang: "tr", isLoading: false, colorglobals: null, Page: 1, TotalPage: 1, TotalItem: 0, 
SearchKeyword: "", initial :{ PStatus:1} , operationResult:undefined, deleteResult:undefined, deleteImgResult:undefined};

var localizer = new Localizer();

export const actionCreators = {

    SearchColorGlobals: payload => async (dispatch, getState) => {

        var page = payload.page ? payload.page : 1;
        var searchKeyword = payload.searchKeyword ? payload.searchKeyword : "";
        var append = payload.append ? payload.append : false;

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/colorglobal?page=${page}&searchKeyword=${searchKeyword}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });


        dispatch({ type: searchColorGlobalType, payload: { ...dataRes, append } });

        //Set bread crumb searchBox
        dispatch({ type: "SEARCH_INITIAL", payload: { searchAction: "SEARCH_COLORGLOBAL", actionUrl: `/${lang}/colorglobal`, searchKeyWord: "" } });

    },
    FormResult: payload => async (dispatch, getState) => {
        dispatch({ type: formResultType, payload });
    },
    DeleteItem: Id => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/colorglobal/delete?id=${Id}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });
        dispatch({ type: deleteItemType, payload: dataRes });

    },
    SaveItem: payload => async (dispatch, getState) => {
 
        dispatch({ type: "SET_LOADING", payload: true });

        var that = this;
        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/ColorGlobal/CreateOrEdit`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: saveItemType, payload: response.data });
            })
            .catch((error) => {
                dispatch({ type: formResultType, payload: error.data });
                dispatch({ type: saveItemType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    },
    ResetFormResult: () => async (dispatch, getState) => {

        dispatch({ type: resetFormResultType, undefined });
    },
    GetInitialValues: (id) => async (dispatch, getState) => {

     
        // if(!id || id == 0){

        //     dispatch({ type: colorglobalFormInitialValuesType, payload: { PStatus:1} });

        // }else{

            dispatch({ type: "SET_LOADING", payload: true });

            var lang = localizer.GetSelectedLang();
            const url = `${lang}/colorglobal/edit/${id}`;
    
            const response = await fetch(url, {
                method: 'post',
            });
    
            var dataRes = await response.json();
    
            dispatch({ type: "SET_LOADING", payload: false });
    
            dispatch({ type: colorglobalFormInitialValuesType, payload: dataRes.Result });
            
        // }

    
    },
 
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type == searchColorGlobalType) {

        const { Page, PageSize, TotalPage, TotalItem, SearchKeyword } = action.payload;

        var colorglobals = action.payload.append ? [...state.colorglobals, ...action.payload.Result] : action.payload.Result;

        return {
            ...state,
            colorglobals: colorglobals,
            Page: Page,
            PageSize: PageSize,
            TotalPage: TotalPage,
            TotalItem: TotalItem,
            SearchKeyword: SearchKeyword
        };

    }

    if (action.type == deleteItemType) {

        if (action.payload.Status) {
            return {
                ...state,
                colorglobals: [...state.colorglobals].filter(x => x.ID != action.payload.Result),
                deleteResult: action.payload
            };

        } else {

            return {
                ...state,
                deleteResult: action.payload
            };
        }

    }

    if (action.type == saveItemType) {

        return {
            ...state,
            saveResult: action.payload
        };

    }

    if (action.type == resetFormResultType) {

        return {
            ...state,
            operationResult: undefined,
            deleteResult: undefined,
            initial: undefined,
            saveResult: undefined
        };
    }
  
    if (action.type == formResultType) {

        return {
            ...state,
            operationResult: action.payload

        };
    }

    if (action.type == colorglobalFormInitialValuesType) {

        return {
            ...state,
            initial: action.payload
        };
    }

    return state;
};
