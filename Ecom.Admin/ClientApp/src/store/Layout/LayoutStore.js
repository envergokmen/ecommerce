﻿import Localizer from "../../Localizer/Localizer";

const toggleMenuType = 'TOGGLE_MENU';
const setActiveMenuType = 'SET_ACTIVE_MENU';
const getMenuItemsType = 'GET_MENU_ITEMS';
const setLoadingType = 'SET_LOADING';

const initialState = { lang: "tr", activeMenu: 7, isLoading: false, MenuItems: [] };

var localizer = new Localizer();

export const actionCreators = {
    
    toggleMenu: payload => async (dispatch, getState) => {
        dispatch({ type: toggleMenuType, payload });
    },
    setLoading: payload => async (dispatch, getState) => {
        dispatch({ type: setLoadingType, payload });
    },
    GetLeftMenu: menuItems => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/adminmenu/getadminmenu?lang=${lang}`;
        const response = await fetch(url, {
            method: 'post',
            // what goes here? What is the "body" for this? content-type header?
          });

          var menuItems=[];
          
          try {
              menuItems = await response.json();

          } catch (error) {
              
          }
      
        dispatch({ type: getMenuItemsType, payload: { menuItems, lang } });
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === getMenuItemsType) {

        return {
            ...state,
            MenuItems: action.payload.menuItems,
            lang: action.payload.lang
        };
    }

    if (action.type === setLoadingType) {

        //console.log(action);

        return {
            ...state,
            IsLoading: action.payload
        };
    }

    if (action.type === toggleMenuType) {
       
        // let selectedMenuItem = state.MenuItems.filter(function (item) {
        //     return item.id == action.payload;
        // });

        let activeMenu = action.payload === state.activeMenu ? 0 : action.payload;

        return {
            ...state,
            activeMenu: activeMenu

        };
    }

    if (action.type === setActiveMenuType) {

        let activeMenu = action.activeMenu === state.activeMenu ? 0 : action.activeMenu;

        return {
            ...state,
            activeMenu: activeMenu
        };
    }

    return state;
};
