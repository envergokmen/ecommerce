import Localizer from "../../Localizer/Localizer";
import UserManager from "../../UserManager/UserManager";

const trySignInType = 'TRY_SIGN_IN';
const clearLoginInfoType = 'CLEAR_LOGIN_INFO';
const initialState = { lang: "tr", LoginErrors: [], Succeed: false };

var localizer = new Localizer();
var userManager = new UserManager();

export const actionCreators = {


    Login: (loginModel) => async (dispatch, getState) => {

        //console.log("loginModel", JSON.stringify(loginModel));
        var lang = localizer.GetSelectedLang();
        const url = `${lang}/Login`;
        const response = await
            fetch(url, {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(loginModel)
            }).then(function (response) {
                return response.json();
            }).then(function (data) {
                return data;
            }).catch(function () {

                return {Status:false, Result:null, ResultDescription:["An error occured please try again later"]};

                //console.log("Error on login please try again later");
            });;

        dispatch({ type: trySignInType, payload: response });

        //console.log(response.Result);
        //set current user 
        if (response && response.Status && response.Result) {
            userManager.setLoggedInUser(response.Result)
        }
    },

    ClearLoginInfo: () => async (dispatch) => {

        dispatch({ type: clearLoginInfoType, payload: null }); 
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === trySignInType) {

        return {
            ...state,
            LoginResult: action.payload,
            lang: localizer.GetSelectedLang()
        };
    }

    if (action.type === clearLoginInfoType) {

        return {
            ...state,
            LoginResult: null,
            lang: localizer.GetSelectedLang()
        };
    }

    return state;
};
