﻿import Localizer from "../../Localizer/Localizer";
import axios from 'axios';

const toggleProductType = 'TOGGLE_PRODUCTCATALOG';
const toggleCatalogType = 'TOGGLE_PRODUCTCATALOG_CAT';
const searchCatalogType = 'SEARCH_PRODUCTCATALOG';
const stockFormInitialValuesType = 'PRODUCTCATALOG_FORM_INITIAL';
const formResultType = 'PRODUCTCATALOG_FORM_RESULT';
const deleteItemType = 'PRODUCTCATALOG_DELETE_ITEM';
const addToCatalogType = 'PRODUCTCATALOG_SAVE_ITEM';
const resetFormResultType = 'PRODUCTCATALOG_FORM_RESET';
const initialState = {
    lang: "tr", isLoading: false, products: null, Page: 1, TotalPage: 1, TotalItem: 0,
    SearchKeyword: "", initial: { PStatus: 1 }, operationResult: undefined, deleteResult: undefined, deleteImgResult: undefined
};

var localizer = new Localizer();

export const actionCreators = {

    SearchProductCatalogs: payload => async (dispatch, getState) => {

        var append = payload.append ? payload.append : false;
        var productId = payload.productId ? payload.productId : null;

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/ProductCatalog/Index?productId=${productId}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });


        dispatch({ type: searchCatalogType, payload: { ...dataRes } });

        //Set bread crumb searchBox
        dispatch({ type: "SEARCH_INITIAL", payload: { searchAction: "SEARCH_PRODUCTCATALOG", actionUrl: `/${lang}/productcatalog`, searchKeyWord: "" } });

    },
    
    DeleteFromCatalog: productInCatalogId => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/ProductCatalog/RemoveFromCatalog?productInCatalogID=${productInCatalogId}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        
        dispatch({ type: searchCatalogType, payload: dataRes });
        dispatch({ type: "SET_LOADING", payload: false });

    },

    
    DeleteFromAllCatalogs: productId => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/ProductCatalog/RemoveFromAllCatalogs?productId=${productId}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();
        
        dispatch({ type: searchCatalogType, payload: dataRes });
        dispatch({ type: "SET_LOADING", payload: false });

    },

    ToggleProduct: colorId => async (dispatch, getState) => {

        dispatch({ type: toggleProductType, payload: colorId });

    },

    ToggleCatalog: catalogId => async (dispatch, getState) => {

        dispatch({ type: toggleCatalogType, payload: catalogId });

    },
    FormResult: payload => async (dispatch, getState) => {
        dispatch({ type: formResultType, payload });
    },
    ResetFormResult: () => async (dispatch, getState) => {

        dispatch({ type: resetFormResultType, undefined });
    },
    AddToCatalog: payload => async (dispatch, getState) => {

        dispatch({ type: "SET_LOADING", payload: true });
        var productID = payload.ProductID;

        var that = this;
        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/ProductCatalog/AddToCatalog`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: searchCatalogType, payload: response.data });
            })
            .catch((error) => {
                dispatch({ type: formResultType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type == searchCatalogType) {

        var result = action.payload.Result;

        return {
            ...state,
            ProductID: result.ProductID,
            ProductColors: result.ProductColors,
            Catalogs: result.Catalogs,
            AddedCatalogs: result.AddedCatalogs
        };

    }

    if (action.type == toggleProductType) {

        var productColor = [...state.ProductColors].filter(x => x.ColorID == action.payload)[0];
        productColor.IsChecked = productColor.IsChecked == undefined ? true : !productColor.IsChecked;

        return {
            ...state,
            ProductColors: [...state.ProductColors]
        };

    }

    if (action.type == toggleCatalogType) {

        var catalog = [...state.Catalogs].filter(x => x.ID == action.payload)[0];
        catalog.IsChecked = catalog.IsChecked == undefined ? true : !catalog.IsChecked;

        return {
            ...state,
            Catalogs: [...state.Catalogs]
        };

    }

    if (action.type == resetFormResultType) {

        return {
            ...state,
            operationResult: undefined,
            deleteResult: undefined,
            initial: undefined,
            saveResult: undefined
        };
    }

    if (action.type == formResultType) {

        return {
            ...state,
            operationResult: action.payload

        };
    }

    if (action.type == stockFormInitialValuesType) {

        return {
            ...state,
            initial: action.payload
        };
    }

    return state;
};
