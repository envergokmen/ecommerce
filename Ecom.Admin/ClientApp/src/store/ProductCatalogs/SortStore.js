﻿import Localizer from "../../Localizer/Localizer";
import axios from 'axios';

const changeOrderTempType = 'CHANCE_ORDER_SORT_TEMP';
const searchCatalogType = 'SEARCH_SORT';
const formResultType = 'SORT_FORM_RESULT';
const resetFormResultType = 'SORT_FORM_RESET';
const initialState = {
    lang: "tr", isLoading: false, products: null, Page: 1, TotalPage: 1, TotalItem: 0,
    SearchKeyword: "", initial: { PStatus: 1 }, operationResult: undefined, deleteResult: undefined, deleteImgResult: undefined,
    items: ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6']
};

var localizer = new Localizer();

export const actionCreators = {

    SearchProductCatalogs: payload => async (dispatch, getState) => {

        var append = payload.append ? payload.append : false;
        var catalogId = payload.catalogId ? payload.catalogId : null;

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/ProductCatalog/sort?catalogId=${catalogId}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });


        dispatch({ type: searchCatalogType, payload: { ...dataRes } });

        //Set bread crumb searchBox
        dispatch({ type: "SEARCH_INITIAL", payload: { searchAction: "SEARCH_SORT", actionUrl: `/${lang}/catalogsort`, searchKeyWord: "" } });

    },
    
    DeleteFromCatalogSort : (productInCatalogId, catalogId) => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/ProductCatalog/RemoveFromCatalogForSort?productInCatalogID=${productInCatalogId}&catalogId=${catalogId}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        
        dispatch({ type: searchCatalogType, payload: dataRes });
        dispatch({ type: "SET_LOADING", payload: false });

    },

    MoveToTop: (productInCatalogId, catalogId) => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/ProductCatalog/MoveToTop?productInCatalogID=${productInCatalogId}&catalogId=${catalogId}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: searchCatalogType, payload: dataRes });
        dispatch({ type: "SET_LOADING", payload: false });

    },

    FormResult: payload => async (dispatch, getState) => {
        dispatch({ type: formResultType, payload });
    },

    ResetFormResult: () => async (dispatch, getState) => {

        dispatch({ type: resetFormResultType, undefined });
    }
    ,

    UpdateCatalogSort: payload => async (dispatch, getState) => {

        dispatch({ type: changeOrderTempType, payload: payload.Items });

        dispatch({ type: "SET_LOADING", payload: true });

        var that = this;

        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/ProductCatalog/UpdateCatalogSort`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: searchCatalogType, payload: response.data });
            })
            .catch((error) => {
                dispatch({ type: formResultType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    }


};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type == searchCatalogType) {

        var result = action.payload.Result;

        return {
            ...state,
            ProductID: result.ProductID,
            AddedCatalogs: result.AddedCatalogs
        };

    }


    if (action.type == changeOrderTempType) {

        return {
            ...state,
            AddedCatalogs: action.payload
        };
    }

    if (action.type == resetFormResultType) {

        return {
            ...state,
            operationResult: undefined,
            deleteResult: undefined,
            initial: undefined,
            saveResult: undefined
        };
    }

    if (action.type == formResultType) {

        return {
            ...state,
            operationResult: action.payload

        };
    }


    return state;
};
