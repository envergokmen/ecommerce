﻿import Localizer from "../../Localizer/Localizer";

const searchType = 'SEARCH_GENERAL';
const searchSetInitialType = 'SEARCH_INITIAL';
const initialState = { lang: "tr", actionUrl: "/", searchKeyword: "", searchAction: "" };

var localizer = new Localizer();

export const actionCreators = {

    SearchGeneral: payload => async (dispatch, getState) => {

        var page = payload.page ? payload.page : 1;
        var searchKeyword = payload.searchKeyword ? payload.searchKeyword : "";
        var actionUrl = payload.actionUrl ? payload.actionUrl : "";
        var append = payload.append ? payload.append : false;

        const url = `${actionUrl}?page=${page}&searchKeyword=${searchKeyword}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: payload.searchAction, payload: { ...dataRes, append } });
    },

    SetInitialValues: (payload) => async (dispatch, getState) => {

        dispatch({ type: searchSetInitialType, payload: payload });
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === searchType) {

        return {
            ...state,
        };

    }

    //all sub components calls this on load 
    if (action.type === searchSetInitialType) {

        return {
            ...state,
            searchKeyword: action.payload.searchKeyword,
            actionUrl: action.payload.actionUrl,
            searchAction: action.payload.searchAction
        };
    }

    return state;
};
