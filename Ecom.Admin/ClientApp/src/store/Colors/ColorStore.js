﻿import Localizer from "../../Localizer/Localizer";
import axios from 'axios';

const searchColorType = 'SEARCH_COLOR';
const colorFormInitialValuesType = 'COLOR_FORM_INITIAL';
const formResultType = 'COLOR_FORM_RESULT';
const deleteItemType = 'COLOR_DELETE_ITEM';
const saveItemType = 'COLOR_SAVE_ITEM';
const deleteImageType = 'COLOR_FORM_DELETE_IMG';
const resetFormResultType = 'COLOR_FORM_RESET';
const initialState = { lang: "tr", isLoading: false, colors: null, Page: 1, TotalPage: 1, TotalItem: 0, 
SearchKeyword: "", initial :{ PStatus:1} , operationResult:undefined, deleteResult:undefined, deleteImgResult:undefined};

var localizer = new Localizer();

export const actionCreators = {

    SearchColors: payload => async (dispatch, getState) => {

        var page = payload.page ? payload.page : 1;
        var searchKeyword = payload.searchKeyword ? payload.searchKeyword : "";
        var append = payload.append ? payload.append : false;

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/color?page=${page}&searchKeyword=${searchKeyword}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });


        dispatch({ type: searchColorType, payload: { ...dataRes, append } });

        //Set bread crumb searchBox
        dispatch({ type: "SEARCH_INITIAL", payload: { searchAction: "SEARCH_COLOR", actionUrl: `/${lang}/color`, searchKeyWord: "" } });

    },
    FormResult: payload => async (dispatch, getState) => {
        dispatch({ type: formResultType, payload });
    },
    DeleteItem: Id => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/color/delete?id=${Id}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });
        dispatch({ type: deleteItemType, payload: dataRes });

    },
    SaveItem: payload => async (dispatch, getState) => {
 
        dispatch({ type: "SET_LOADING", payload: true });

        var that = this;
        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/Color/CreateOrEdit`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: saveItemType, payload: response.data });
            })
            .catch((error) => {
                dispatch({ type: formResultType, payload: error.data });
                dispatch({ type: saveItemType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    },
    ResetFormResult: () => async (dispatch, getState) => {

        dispatch({ type: resetFormResultType, undefined });
    },
    GetInitialValues: (id) => async (dispatch, getState) => {

     
        // if(!id || id == 0){

        //     dispatch({ type: colorFormInitialValuesType, payload: { PStatus:1} });

        // }else{

            dispatch({ type: "SET_LOADING", payload: true });

            var lang = localizer.GetSelectedLang();
            const url = `${lang}/color/edit/${id}`;
    
            const response = await fetch(url, {
                method: 'post',
            });
    
            var dataRes = await response.json();
    
            dispatch({ type: "SET_LOADING", payload: false });
    
            dispatch({ type: colorFormInitialValuesType, payload: dataRes.Result });
            
        // }

    
    }, 
    DeleteImage: (id) => async (dispatch, getState) => {

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/color/deleteImage/${id}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });

        dispatch({ type: deleteImageType, payload: dataRes });
    }
 
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type == searchColorType) {

        const { Page, PageSize, TotalPage, TotalItem, SearchKeyword } = action.payload;

        var colors = action.payload.append ? [...state.colors, ...action.payload.Result] : action.payload.Result;

        return {
            ...state,
            colors: colors,
            Page: Page,
            PageSize: PageSize,
            TotalPage: TotalPage,
            TotalItem: TotalItem,
            SearchKeyword: SearchKeyword
        };

    }

    if (action.type == deleteItemType) {

        if (action.payload.Status) {
            return {
                ...state,
                colors: [...state.colors].filter(x => x.ID != action.payload.Result),
                deleteResult: action.payload
            };

        } else {

            return {
                ...state,
                deleteResult: action.payload
            };
        }

    }

    if (action.type == saveItemType) {

        return {
            ...state,
            saveResult: action.payload
        };

    }
    
    if (action.type === deleteImageType) {

        
        return {
            ...state,
            initial: { ...state.initial, PatternPath: "" },
            operationResult: undefined,
            deleteImgResult: action.payload
        };
    }

    if (action.type == resetFormResultType) {

        return {
            ...state,
            operationResult: undefined,
            deleteResult: undefined,
            initial: undefined,
            saveResult: undefined
        };
    }
  
    if (action.type == formResultType) {

        return {
            ...state,
            operationResult: action.payload

        };
    }

    if (action.type == colorFormInitialValuesType) {

        return {
            ...state,
            initial: action.payload
        };
    }

    return state;
};
