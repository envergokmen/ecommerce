﻿import Localizer from "../../Localizer/Localizer";
import axios from 'axios';

const searchPolicyType = 'SEARCH_POLICYLOCALLANG';
const policyFormInitialValuesType = 'POLICYLOCALLANG_FORM_INITIAL';
const formResultType = 'POLICYLOCALLANG_FORM_RESULT';
const deleteItemType = 'POLICYLOCALLANG_DELETE_ITEM';
const saveItemType = 'POLICYLOCALLANG_SAVE_ITEM';
const resetFormResultType = 'POLICYLOCALLANG_FORM_RESET';
const initialState = { lang: "tr", isLoading: false, policies: null, Page: 1, TotalPage: 1, TotalItem: 0, 
SearchKeyword: "", initial :{ PStatus:1} , operationResult:undefined, deleteResult:undefined, deleteImgResult:undefined};

var localizer = new Localizer();

export const actionCreators = {

    SearchPolicyLocalLangs: payload => async (dispatch, getState) => {

        var page = payload.page ? payload.page : 1;
        var searchKeyword = payload.searchKeyword ? payload.searchKeyword : "";
        var append = payload.append ? payload.append : false;
        var policyId = payload.policyId ? payload.policyId : null;

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/PolicyLocalLang?page=${page}&searchKeyword=${searchKeyword}&policyId=${policyId}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });


        dispatch({ type: searchPolicyType, payload: { ...dataRes, append } });

        //Set bread crumb searchBox
        dispatch({ type: "SEARCH_INITIAL", payload: { searchAction: "SEARCH_POLICYLOCALLANG", actionUrl: `/${lang}/policy`, searchKeyWord: "" } });

    },
    FormResult: payload => async (dispatch, getState) => {
        dispatch({ type: formResultType, payload });
    },
    DeleteItem: Id => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/PolicyLocalLang/delete?id=${Id}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });
        dispatch({ type: deleteItemType, payload: dataRes });

    },
    SaveItem: payload => async (dispatch, getState) => {
 
        dispatch({ type: "SET_LOADING", payload: true });

        var that = this;
        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/PolicyLocalLang/CreateOrEdit`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: saveItemType, payload: response.data });
            })
            .catch((error) => {
                dispatch({ type: formResultType, payload: error.data });
                dispatch({ type: saveItemType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    },
    ResetFormResult: () => async (dispatch, getState) => {

        dispatch({ type: resetFormResultType, undefined });
    },
    GetInitialValues: (id, policyId) => async (dispatch, getState) => {

     
        // if(!id || id == 0){

        //     dispatch({ type: policyFormInitialValuesType, payload: { PStatus:1} });

        // }else{

            dispatch({ type: "SET_LOADING", payload: true });

            var lang = localizer.GetSelectedLang();
        const url = `${lang}/PolicyLocalLang/edit/${id}?policyId=${policyId}`;
    
            const response = await fetch(url, {
                method: 'post',
            });
    
            var dataRes = await response.json();
    
            dispatch({ type: "SET_LOADING", payload: false });
    
            dispatch({ type: policyFormInitialValuesType, payload: dataRes.Result });
            
        // }

    
    },
 
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type == searchPolicyType) {

        const { Page, PageSize, TotalPage, TotalItem, SearchKeyword } = action.payload;

        var policies = action.payload.append ? [...state.policies, ...action.payload.Result] : action.payload.Result;

        return {
            ...state,
            policies: policies,
            Page: Page,
            PageSize: PageSize,
            TotalPage: TotalPage,
            TotalItem: TotalItem,
            SearchKeyword: SearchKeyword
        };

    }

    if (action.type == deleteItemType) {

        if (action.payload.Status) {
            return {
                ...state,
                policies: [...state.policies].filter(x => x.ID != action.payload.Result),
                deleteResult: action.payload
            };

        } else {

            return {
                ...state,
                deleteResult: action.payload
            };
        }

    }

    if (action.type == saveItemType) {

        return {
            ...state,
            saveResult: action.payload
        };

    }

    if (action.type == resetFormResultType) {

        return {
            ...state,
            operationResult: undefined,
            deleteResult: undefined,
            initial: undefined,
            saveResult: undefined
        };
    }
  
    if (action.type == formResultType) {

        return {
            ...state,
            operationResult: action.payload

        };
    }

    if (action.type == policyFormInitialValuesType) {

        return {
            ...state,
            initial: action.payload
        };
    }

    return state;
};
