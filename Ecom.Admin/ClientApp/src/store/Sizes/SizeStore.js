﻿import Localizer from "../../Localizer/Localizer";
import axios from 'axios';

const searchSizeType = 'SEARCH_SIZE';
const sizeFormInitialValuesType = 'SIZE_FORM_INITIAL';
const formResultType = 'SIZE_FORM_RESULT';
const deleteImageType = 'SIZE_FORM_DELETE_IMG';
const deleteItemType = 'SIZE_DELETE_ITEM';
const saveItemType = 'SIZE_SAVE_ITEM';
const resetFormResultType = 'SIZE_FORM_RESET';
const initialState = { lang: "tr", isLoading: false, sizes: null, Page: 1, TotalPage: 1, TotalItem: 0, 
SearchKeyword: "", initial :{ PStatus:1, ImagePath: ""} , operationResult:undefined, deleteResult:undefined, deleteImgResult:undefined};

var localizer = new Localizer();

export const actionCreators = {

    SearchSizes: payload => async (dispatch, getState) => {

        var page = payload.page ? payload.page : 1;
        var searchKeyword = payload.searchKeyword ? payload.searchKeyword : "";
        var append = payload.append ? payload.append : false;

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/size?page=${page}&searchKeyword=${searchKeyword}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });


        dispatch({ type: searchSizeType, payload: { ...dataRes, append } });

        //Set bread crumb searchBox
        dispatch({ type: "SEARCH_INITIAL", payload: { searchAction: "SEARCH_SIZE", actionUrl: `/${lang}/size`, searchKeyWord: "" } });

    },
    FormResult: payload => async (dispatch, getState) => {
        dispatch({ type: formResultType, payload });
    },
    DeleteItem: Id => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/size/delete?id=${Id}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });
        dispatch({ type: deleteItemType, payload: dataRes });

    },
    SaveItem: payload => async (dispatch, getState) => {

        dispatch({ type: "SET_LOADING", payload: true });
        var that = this;
        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/Size/CreateOrEdit`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {

                dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: saveItemType, payload: response.data });

                console.warn('response', response);
            })
            .catch((error) => {
                console.warn('Not good man :(', error);
                dispatch({ type: formResultType, payload: error.data });
                dispatch({ type: saveItemType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    },
    ResetFormResult: () => async (dispatch, getState) => {

        //console.log("reset form values");
        dispatch({ type: resetFormResultType, undefined });
    },
    GetInitialValues: (id) => async (dispatch, getState) => {

     
        if(!id || id == 0){

            dispatch({ type: sizeFormInitialValuesType, payload: {ImagePath:"", PStatus:1} });

        }else{
            dispatch({ type: "SET_LOADING", payload: true });

            var lang = localizer.GetSelectedLang();
            const url = `${lang}/size/edit/${id}`;
    
            const response = await fetch(url, {
                method: 'post',
            });
    
            var dataRes = await response.json();
    
            dispatch({ type: "SET_LOADING", payload: false });
    
            dispatch({ type: sizeFormInitialValuesType, payload: dataRes.Result });
        }

    
    },

    DeleteImage: (id) => async (dispatch, getState) => {

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/size/deleteImage/${id}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });

        dispatch({ type: deleteImageType, payload: dataRes });
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === searchSizeType) {

        const { Page, PageSize, TotalPage, TotalItem, SearchKeyword } = action.payload;

        var sizes = action.payload.append ? [...state.sizes, ...action.payload.Result] : action.payload.Result;

        return {
            ...state,
            sizes: sizes,
            Page: Page,
            PageSize: PageSize,
            TotalPage: TotalPage,
            TotalItem: TotalItem,
            SearchKeyword: SearchKeyword
        };

    }

    if (action.type === deleteItemType) {

        if (action.payload.Status) {
            return {
                ...state,
                sizes: [...state.sizes].filter(x => x.ID != action.payload.Result),
                deleteResult: action.payload
            };

        } else {

            return {
                ...state,
                deleteResult: action.payload
            };
        }

    }

    if (action.type === saveItemType) {

        return {
            ...state,
            saveResult: action.payload
        };

    }

    if (action.type === resetFormResultType) {

        return {
            ...state,
            operationResult: undefined,
            deleteResult: undefined,
            initial: undefined,
            saveResult: undefined
        };
    }

    
    if (action.type === deleteImageType) {

        
        return {
            ...state,
            initial: { ...state.initial, ImagePath: "" },
            operationResult: undefined,
            deleteImgResult: action.payload
        };
    }

    if (action.type === formResultType) {

        return {
            ...state,
            operationResult: action.payload

        };
    }

    if (action.type === sizeFormInitialValuesType) {

        return {
            ...state,
            initial: action.payload
        };
    }

    return state;
};
