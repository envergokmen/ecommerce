﻿import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import * as Counter from './Counter';
import * as WeatherForecasts from './WeatherForecasts';
import * as LayoutStore from './Layout/LayoutStore';
import * as OrderStore from './Orders/OrderStore';
import * as BrandStore from './Brands/BrandStore';
import * as BrandFormStore from './Brands/BrandFormStore';

import * as BankStore from './Banks/BankStore';
import * as BankFormStore from './Banks/BankFormStore';

import * as DomainStore from './Domains/DomainStore';
import * as DomainFormStore from './Domains/DomainFormStore';

import * as StoreAccountStore from './StoreAccounts/StoreAccountStore';
import * as StoreAccountFormStore from './StoreAccounts/StoreAccountFormStore';

import * as SizeStore from './Sizes/SizeStore';
import * as SizeFormStore from './Sizes/SizeFormStore';

import * as SeasonStore from './Seasons/SeasonStore';
import * as SeasonFormStore from './Seasons/SeasonFormStore';

import * as ProductDepartmentStore from './ProductDepartments/ProductDepartmentStore';
import * as ProductDepartmentFormStore from './ProductDepartments/ProductDepartmentFormStore';


import * as ProductStyleStore from './ProductStyles/ProductStyleStore';
import * as ProductStyleFormStore from './ProductStyles/ProductStyleFormStore';

import * as PaymentStore from './Payments/PaymentStore';
import * as PaymentFormStore from './Payments/PaymentFormStore';

import * as CargoStore from './Cargoes/CargoStore';
import * as CargoFormStore from './Cargoes/CargoFormStore';


import * as CargoPriceStore from './CargoPrices/CargoPriceStore';
import * as CargoPriceFormStore from './CargoPrices/CargoPriceFormStore';


import * as WebPosStore from './WebPoses/WebPosStore';
import * as WebPosFormStore from './WebPoses/WebPosFormStore';

import * as PolicyStore from './Policies/PolicyStore';
import * as PolicyFormStore from './Policies/PolicyFormStore';

import * as PolicyLocalLangStore from './PolicyLocalLangs/PolicyLocalLangStore';
import * as PolicyLocalLangFormStore from './PolicyLocalLangs/PolicyLocalLangFormStore';


import * as StockStore from './Stocks/StockStore';
import * as StockFormStore from './Stocks/StockFormStore';


import * as WebPosInstallmentStore from './WebPosInstallments/WebPosInstallmentStore';
import * as WebPosInstallmentFormStore from './WebPosInstallments/WebPosInstallmentFormStore';

import * as ProductLocalLangStore from './ProductLocalLangs/ProductLocalLangStore';
import * as ProductLocalLangFormStore from './ProductLocalLangs/ProductLocalLangFormStore';

import * as ProductStore from './Products/ProductStore';
import * as ProductFormStore from './Products/ProductFormStore';


import * as SendingProviderStore from './SendingProviders/SendingProviderStore';
import * as SendingProviderFormStore from './SendingProviders/SendingProviderFormStore';

import * as EmailTemplateStore from './EmailTemplates/EmailTemplateStore';
import * as EmailTemplateFormStore from './EmailTemplates/EmailTemplateFormStore';

import * as CouponStore from './Coupons/CouponStore';
import * as CouponFormStore from './Coupons/CouponFormStore';

import * as CatalogStore from './Catalogs/CatalogStore';
import * as CatalogFormStore from './Catalogs/CatalogFormStore';

import * as CatalogLocalLangStore from './CatalogLocalLangs/CatalogLocalLangStore';
import * as CatalogLocalLangFormStore from './CatalogLocalLangs/CatalogLocalLangFormStore';


import * as ContentPageStore from './ContentPages/ContentPageStore';
import * as ContentPageFormStore from './ContentPages/ContentPageFormStore';

import * as ContentPageLocalLangStore from './ContentPageLocalLangs/ContentPageLocalLangStore';
import * as ContentPageLocalLangFormStore from './ContentPageLocalLangs/ContentPageLocalLangFormStore';

import * as EmailTemplateLocalLangStore from './EmailTemplateLocalLangs/EmailTemplateLocalLangStore';
import * as EmailTemplateLocalLangFormStore from './EmailTemplateLocalLangs/EmailTemplateLocalLangFormStore';


import * as ProductCatalogStore from './ProductCatalogs/ProductCatalogStore';
import * as SortStore from './ProductCatalogs/SortStore';

import * as ColorStore from './Colors/ColorStore';
import * as ColorFormStore from './Colors/ColorFormStore';


import * as PriceStore from './Prices/PriceStore';
import * as PriceFormStore from './Prices/PriceFormStore';

import * as BannerStore from './Banners/BannerStore';
import * as BannerFormStore from './Banners/BannerFormStore';

import * as ColorGlobalStore from './ColorGlobals/ColorGlobalStore';
import * as ColorGlobalFormStore from './ColorGlobals/ColorGlobalFormStore';


import * as CampaignStore from './Campaigns/CampaignStore';
import * as CampaignFormStore from './Campaigns/CampaignFormStore';

import * as SignInStore from './SignIn/SignInStore';
import * as BreadCrumbStore from './BreadCrumb/BreadCrumbStore';
import { reducer as formReducer } from 'redux-form'

// import LeftNav from '../components/LeftNav';

export default function configureStore(history, initialState) {


    const reducers = {
        counter: Counter.reducer,
        weatherForecasts: WeatherForecasts.reducer,
        layout: LayoutStore.reducer,
        breadCrumb: BreadCrumbStore.reducer,

        //Products
        catalogs: CatalogStore.reducer,
        catalogForms: CatalogFormStore.reducer,
        catalogLocalLangs: CatalogLocalLangStore.reducer,
        catalogLocalLangForms: CatalogLocalLangFormStore.reducer,

        products: ProductStore.reducer,
        productForms: ProductFormStore.reducer,
        productLocalLangs: ProductLocalLangStore.reducer,
        productLocalLangForms: ProductLocalLangFormStore.reducer,

        prices: PriceStore.reducer,
        priceForms: PriceFormStore.reducer,

        brands: BrandStore.reducer,
        brandForms: BrandFormStore.reducer,


        banks: BankStore.reducer,
        bankForms: BankFormStore.reducer,

        seasons: SeasonStore.reducer,
        seasonForms: SeasonFormStore.reducer,

        productDepartments: ProductDepartmentStore.reducer,
        productDepartmentForms: ProductDepartmentFormStore.reducer,

        productStyles: ProductStyleStore.reducer,
        productStyleForms: ProductStyleFormStore.reducer,

        payments: PaymentStore.reducer,
        paymentForms: PaymentFormStore.reducer,

        productCatalogs: ProductCatalogStore.reducer,
        sorts: SortStore.reducer,

        stocks: StockStore.reducer,
        stockForms: StockFormStore.reducer,
        sizes: SizeStore.reducer,
        sizeForms: SizeFormStore.reducer,
        colors: ColorStore.reducer,
        colorForms: ColorGlobalFormStore.reducer,
        colorglobals: ColorGlobalStore.reducer,
        colorglobalForms: ColorFormStore.reducer,

        policies: PolicyStore.reducer,
        policyForms: PolicyFormStore.reducer,
        policyLocalLangs: PolicyLocalLangStore.reducer,
        policyLocalLangForms: PolicyLocalLangFormStore.reducer,

        //Contents
        banners: BannerStore.reducer,
        bannerForms: BannerFormStore.reducer,

        contentPages: ContentPageStore.reducer,
        contentPageForms: ContentPageFormStore.reducer,
        contentPageLocalLangs: ContentPageLocalLangStore.reducer,
        contentPageLocalLangForms: ContentPageLocalLangFormStore.reducer,

        emailTemplates: EmailTemplateStore.reducer,
        emailTemplateForms: EmailTemplateFormStore.reducer,


        emailTemplateLocalLangs: EmailTemplateLocalLangStore.reducer,
        emailTemplateLocalLangForms: EmailTemplateLocalLangFormStore.reducer,

        //Order and campaigns
        orders: OrderStore.reducer,
        campaigns: CampaignStore.reducer,
        campaignForms: CampaignFormStore.reducer,
        coupons: CouponStore.reducer,
        couponForms: CouponFormStore.reducer,

        cargoes: CargoStore.reducer,
        cargoForms: CargoFormStore.reducer,

        cargoPrices: CargoPriceStore.reducer,
        cargoPriceForms: CargoPriceFormStore.reducer,


        webPoses: WebPosStore.reducer,
        webPosForms: WebPosFormStore.reducer,

        webPosInstallments: WebPosInstallmentStore.reducer,
        webPosInstallmentForms: WebPosInstallmentFormStore.reducer,
        //Settings
        domains: DomainStore.reducer,
        domainForms: DomainFormStore.reducer,
        sendingProviders: SendingProviderStore.reducer,
        sendingProviderForms: SendingProviderFormStore.reducer,

        storeAccounts: StoreAccountStore.reducer,
        storeAccountForms: StoreAccountFormStore.reducer,

        form: formReducer,

        //Authentication
        signin: SignInStore.reducer
    };

    const middleware = [
        thunk,
        routerMiddleware(history)
    ];

    // In development, use the browser's Redux dev tools extension if installed
    const enhancers = [];
    const isDevelopment = process.env.NODE_ENV === 'development';
    if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
        enhancers.push(window.devToolsExtension());
    }

    const rootReducer = combineReducers({
        ...reducers,
        routing: routerReducer
    });

    return createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(...middleware), ...enhancers)
    );
}
