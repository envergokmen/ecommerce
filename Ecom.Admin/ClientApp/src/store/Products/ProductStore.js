﻿import Localizer from "../../Localizer/Localizer";
import axios from 'axios';

const searchProductType = 'SEARCH_PRODUCTS';
const productFormInitialValuesType = 'PRODUCTS_FORM_INITIAL';
const deleteImageType = "PRODUCT_DELETE_IMG";
const deleteAllImageType="PRODUCT_DELETE_ALL_IMG";
const formResultType = 'PRODUCTS_FORM_RESULT';
const deleteItemType = 'PRODUCTS_DELETE_ITEM';
const saveItemType = 'PRODUCTS_SAVE_ITEM';
const uploadType = 'PRODUCTS_UPLOAD';
const saveImageType = 'PRODUCTS_SAVE_IMAGE';
const resetFormResultType = 'PRODUCTS_FORM_RESET';
const initialState = {
    lang: "tr", isLoading: false, products: null, Page: 1, TotalPage: 1, TotalItem: 0,
    SearchKeyword: "", initial: { PStatus: 1 }, operationResult: undefined, deleteResult: undefined, deleteImgResult: undefined
};

var localizer = new Localizer();

export const actionCreators = {

    SearchProducts: payload => async (dispatch, getState) => {

        var page = payload.page ? payload.page : 1;
        var searchKeyword = payload.searchKeyword ? payload.searchKeyword : "";
        var append = payload.append ? payload.append : false;

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/product?page=${page}&searchKeyword=${searchKeyword}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });


        dispatch({ type: searchProductType, payload: { ...dataRes, append } });

        //Set bread crumb searchBox
        dispatch({ type: "SEARCH_INITIAL", payload: { searchAction: "SEARCH_PRODUCTS", actionUrl: `/${lang}/product`, searchKeyWord: "" } });

    },
    FormResult: payload => async (dispatch, getState) => {
        dispatch({ type: formResultType, payload });
    },
    DeleteItem: Id => async (dispatch, getState) => {

        var lang = localizer.GetSelectedLang();
        const url = `/${lang}/product/delete?id=${Id}`;
        dispatch({ type: "SET_LOADING", payload: true });

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });
        dispatch({ type: deleteItemType, payload: dataRes });

    },
    SaveItem: payload => async (dispatch, getState) => {

        dispatch({ type: "SET_LOADING", payload: true });

        var that = this;
        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/Product/CreateOrEdit`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: saveItemType, payload: response.data });
            })
            .catch((error) => {
                dispatch({ type: formResultType, payload: error.data });
                dispatch({ type: saveItemType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    },
    Upload: payload => async (dispatch, getState) => {

        dispatch({ type: "SET_LOADING", payload: true });

        var that = this;
        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/Product/Upload`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: uploadType, payload: response.data });
            })
            .catch((error) => {
                dispatch({ type: formResultType, payload: error.data });
                dispatch({ type: uploadType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    },
    SaveImage: payload => async (dispatch, getState) => {

        dispatch({ type: "SET_LOADING", payload: true });

        var that = this;
        axios({
            method: 'post',
            url: `/${localizer.GetSelectedLang()}/Product/SaveImage`,
            data: payload,
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                //dispatch({ type: formResultType, payload: response.data });
                dispatch({ type: saveImageType, payload: response.data });
            })
            .catch((error) => {
                //dispatch({ type: formResultType, payload: error.data });
                dispatch({ type: saveImageType, payload: error.data });

            }).finally(() => {
                dispatch({ type: "SET_LOADING", payload: false });
            });

    },
    ResetFormResult: () => async (dispatch, getState) => {

        dispatch({ type: resetFormResultType, undefined });
    },
    GetInitialValues: (id) => async (dispatch, getState) => {


        // if(!id || id == 0){

        //     dispatch({ type: productFormInitialValuesType, payload: { PStatus:1} });

        // }else{

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/product/edit/${id}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });

        dispatch({ type: productFormInitialValuesType, payload: dataRes.Result });

        // }


    },
    DeleteImage: (id) => async (dispatch, getState) => {

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/product/deleteImage/${id}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });

        dispatch({ type: deleteImageType, payload: dataRes });
    }
    ,
    DeleteAllImages : (productId) => async (dispatch, getState) => {

        dispatch({ type: "SET_LOADING", payload: true });

        var lang = localizer.GetSelectedLang();
        const url = `${lang}/product/deleteallImages?productId=${productId}`;

        const response = await fetch(url, {
            method: 'post',
        });

        var dataRes = await response.json();

        dispatch({ type: "SET_LOADING", payload: false });

        dispatch({ type: deleteAllImageType, payload: dataRes });
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type == searchProductType) {

        const { Page, PageSize, TotalPage, TotalItem, SearchKeyword } = action.payload;

        var products = action.payload.append ? [...state.products, ...action.payload.Result] : action.payload.Result;

        return {
            ...state,
            products: products,
            Page: Page,
            PageSize: PageSize,
            TotalPage: TotalPage,
            TotalItem: TotalItem,
            SearchKeyword: SearchKeyword
        };

    }


    if (action.type === saveImageType) {
 
        return {
            ...state,
            modal : false,
            date:Date.now(),
            initial: {...state.initial,  Images :  action.payload.Result} ,
            operationResult: undefined,
            saveImgResult: action.payload
        };
    }

    if (action.type === deleteImageType) {
 
        console.log(state);

        return {
            ...state,
            date:Date.now(),
            initial: {...state.initial,  Images : [...state.initial.Images].filter(x => x.ID != action.payload.Result) } ,
            operationResult: undefined,
            deleteImgResult: action.payload
        };
    }

    
    if (action.type === deleteAllImageType) {
  
        return {
            ...state,
            date:Date.now(),
            initial: {...state.initial,  Images : [] } ,
            operationResult: undefined,
            deleteImgResult: action.payload
        };
    }

    if (action.type == deleteItemType) {

        if (action.payload.Status) {
            return {
                ...state,
                products: [...state.products].filter(x => x.ID != action.payload.Result),
                deleteResult: action.payload
            };

        } else {

            return {
                ...state,
                deleteResult: action.payload
            };
        }

    }

    if (action.type == saveItemType) {

        return {
            ...state,
            saveResult: action.payload
        };

    }

    if (action.type == resetFormResultType) {

        return {
            ...state,
            operationResult: undefined,
            deleteResult: undefined,
            initial: undefined,
            saveResult: undefined
        };
    }

    if (action.type == formResultType) {

        return {
            ...state,
            operationResult: action.payload

        };
    }

    if (action.type == productFormInitialValuesType) {

        return {
            ...state,
            initial: action.payload
        };
    }

    return state;
};
