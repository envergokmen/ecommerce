import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Layout/LayoutStore';
import NavItem from './NavItem';
import UserManager from "../../UserManager/UserManager";
import './LeftNav.css';
import Localizer from "../../Localizer/Localizer";


class LeftNav extends Component {
    componentWillMount() {


        var userManager = new UserManager();
        var localizer = new Localizer();

        var curUser = userManager.getLoggedInUser();
        // console.log("leftnav", curUser);
        if (!curUser) {
            window.location.href = "/sign-in/"+ localizer.GetSelectedLang();
        }

        if (curUser != null && curUser != undefined) {
            this.props.GetLeftMenu();
        }

    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        var activeMenu = this.props.activeMenu;
        return (
            <ul className="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                {this.props.MenuItems.map(({ Id, Name, LinkTo, Title, SubItems }) => <NavItem lang={this.props.lang} activeMenu={activeMenu} setActiveItem={() => this.handleClick(Id)} key={Name} Id={Id} Name={Name} LinkTo={LinkTo} Title={Title} SubItems={SubItems} />)}

            </ul>
        );
    }

    handleClick = (Id) => {
        this.props.toggleMenu(Id);
    }
}

export default connect(
    state => state.layout,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(LeftNav);