import React, { Component } from 'react'; 
import { connect } from 'react-redux'; 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';  
import { Link } from "react-router-dom";

class NavItem extends Component {
    render() {
        var classname = this.props.activeMenu === this.props.Id ? "active" : "inactive";

        return (

            <li data-id={this.props.Id}>
                <a href="javascript:;" className={classname} onClick={this.props.setActiveItem} title={this.props.Title}>
                    {this.props.Name} <FontAwesomeIcon icon="angle-down" />
                    <span className="selected"></span>
                </a>
                <ul className="sub-menu">
                    {this.props.SubItems.map(({ Icon, Id, Name, LinkTo, Title }) => <li data-id={Id} key={Name}><Link to={LinkTo}><FontAwesomeIcon icon={Icon ? Icon :"bookmark"} />{Name} </Link></li>)}
                </ul>
            </li>)
    }
}

export default connect()(NavItem);