import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Sizes/SizeStore';
import SizeForm from './SizeForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class SizesNew extends Component {
    componentWillMount() {
 
        const { match: { params } } = this.props;
        const { lang , id} = params;
   
        if (id) {
            this.props.GetInitialValues(id);
        } else {
            this.props.GetInitialValues(0);
        }

        this.breadCrumbNav = 
        [{ link: `/products/${lang}`, text: localizer.get("ProductManagement")},
           {link: `/sizes/${lang}`, text: localizer.get("Sizes")},
           {link: "", text: localizer.getWithFormat("NewField","$Size")}]

    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <SizeForm 
                SaveItem={this.props.SaveItem} 
                DeleteImage={this.props.DeleteImage}
                push={this.props.history.push} 
                deleteImgResult={this.props.deleteImgResult} 
                operationResult={this.props.operationResult} 
                FormResult={this.props.FormResult} 
                formSuccess={"test başarılı mesaji"} 
                formErrors={"test hata mesaji"} 
                header={localizer.getWithFormat("NewField", "$Size")}
                  id={this.props.initial ? this.props.initial.ID : 0}  
                  initialValues={this.props.initial} onSubmit={this.submit}></SizeForm>
            </div>
        );
    }
}

export default connect(
    state => state.sizes,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(SizesNew);