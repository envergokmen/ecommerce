import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Sizes/SizeStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$Size")
  }

  if (!values.SortNo && values.SortNo != "0") {
    errors.SortNo = localizer.getWithFormat("RequiredField", "$SortNo")
  }

  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

let SizeForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/sizes/${localizer.GetSelectedLang()}`);
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);


    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {
      let formData = new FormData();
      formData.append('Name', e.target.elements.Name.value);
      formData.append('SortNo', e.target.elements.SortNo.value);
      formData.append('PStatus',  e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('ID', e.target.elements.ID.value);
      props.SaveItem(formData);
    }
  }

  return (
    <form onSubmit={onFormSubmit.bind(this)} className="FormStandard" method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}
      {props.deleteImgResult && <FormResultMessage operationResult={props.deleteImgResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="Name" type="text" component={FormTextField} label={localizer.get("Size")} />
              <Field name="SortNo" type="number" component={FormTextField} label={localizer.get("SortNo")} />
              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />
 
            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var sizeFormComp = connect(
  state => state.sizeForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(SizeForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditSizeForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(sizeFormComp);
