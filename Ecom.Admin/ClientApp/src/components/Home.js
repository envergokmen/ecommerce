import React from 'react';
import { connect } from 'react-redux';
import BreadCrumb from './BreadCrumb';
import {Localizer, Localize} from "../Localizer/Localizer";
import sharedTranslations from "../Localizer/Translations/_Shared";
import navTranslations from "../Localizer/Translations/Navigation";
import UserManager from '../UserManager/UserManager';
import { Link } from "react-router-dom";

var localizer = new Localizer([sharedTranslations, navTranslations]);
var userManager = new UserManager();
 
const Home = props => (

  <div>
    <BreadCrumb navs={[{ link: ``, text: localizer.get("Management") }]}></BreadCrumb>
    <div id="MainListArea">
      <div>
        <h1>
         <Localize localizer={localizer}>Hello</Localize>, {userManager.getLoggedInName()} !</h1>

         <Link to={`/brands/${localizer.GetSelectedLang()}/new`} className="btn btn-primary">{localizer.getWithFormat("NewField", "$Brand")}</Link>
         <br></br>      <br></br>
          <Link to={`/brands/${localizer.GetSelectedLang()}`} className="btn btn-primary">{localizer.get("Brands")}</Link>
         <br></br>      <br></br>
         <Link to={`/products/${localizer.GetSelectedLang()}/new`} className="btn btn-primary">{localizer.getWithFormat("Products")}</Link>

      </div>
    </div>
  </div>



);

export default connect()(Home);
