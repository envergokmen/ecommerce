import React, { Component } from 'react';

class LayoutBlank extends Component {

    render() {

        return (
            <div>
                <div id="MainArea">
                    {this.props.children}
                </div>
                {/* <footer>
                    <div className="row">
                        <div className="col-sm-12">
                            All rigsht reserved (c)
                        </div>
                    </div>
                </footer> */}
            </div>
        )

    }
}

export default LayoutBlank;