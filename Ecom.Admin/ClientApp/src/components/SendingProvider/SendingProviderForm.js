import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/SendingProviders/SendingProviderStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions, FormCheckBoxField } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$SendingProvider")
  }

  // if (values.PStatus === undefined || values.PStatus === null || values.PStatus === "") {
  //   errors.PStatus = localizer.getWithFormat("RequiredField", "$Status")
  // }

  if (values.PriceCurrencyID === undefined || values.PriceCurrencyID === null || values.PriceCurrencyID === "") {
    errors.PriceCurrencyID = localizer.getWithFormat("RequiredField", "$Currency")
  }

  if (values.LangID === undefined || values.LangID === null || values.LangID === "") {
    errors.LangID = localizer.getWithFormat("RequiredField", "$Language")
  }
  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let SendingProviderForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/sendingProviders/${localizer.GetSelectedLang()}`);
  }

  //const fileInput = React.createRef();

  const handleFile = function () {
  }


  const handleProviderType = (e) => {

    //alert("provider type:" + e.target.value);
    props.SetProviderType(e.target.value);
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {


      let formData = new FormData();
      formData.append('Name', e.target.elements.Name.value);
      formData.append('SenderName', e.target.elements.SenderName.value);
      formData.append('UserName', e.target.elements.UserName.value);
      formData.append('Password', e.target.elements.Password.value);
      formData.append('ProviderType', e.target.elements.ProviderType.value);

      if (e.target.elements.ApiEndPoint) {
        formData.append('ApiEndPoint', e.target.elements.ApiEndPoint.value);
      }

      if (e.target.elements.SmptPort) {
        formData.append('SmptPort', e.target.elements.SmptPort.value);
        formData.append('SmptHost', e.target.elements.SmptHost.value);
        formData.append('EnableSsl', e.target.elements.EnableSsl.checked ? "true" : "false");
      }

      if (e.target.elements.EuEmailColumnName) {
        formData.append('EuEmailColumnName', e.target.elements.EuEmailColumnName.value);
        formData.append('EuNameColumnName', e.target.elements.EuNameColumnName.value);
        formData.append('EuroMessageFromName', e.target.elements.EuroMessageFromName.value);
        formData.append('EuroMessageFromAddress', e.target.elements.EuroMessageFromAddress.value);
        formData.append('EuroMessageReplyAddress', e.target.elements.EuroMessageReplyAddress.value);
        formData.append('EuroMessageApiUser', e.target.elements.EuroMessageApiUser.value);
        formData.append('EuroMessageApiPassword', e.target.elements.EuroMessageApiPassword.value);
      }

      if (e.target.elements.MailApiKey) {
        formData.append('MailApiKey', e.target.elements.MailApiKey.value);
        formData.append('MailTransCode', e.target.elements.MailTransCode.value);
      }

      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('ID', e.target.elements.ID.value);

      props.SaveItem(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="Name" type="text" component={FormTextField} label={localizer.get("SendingProvider")} />
              <Field name="SenderName" type="text" component={FormTextField} label={localizer.get("SenderName")} />

              {props.initialValues && props.initialValues.AllSendingProviderTypes &&
                <Field name="ProviderType" label={localizer.get("ProviderType")} type="select"
                  component={FormSelectField}
                  onChange={handleProviderType}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$ProviderType')}
                  optionFnc={props.initialValues.AllSendingProviderTypes.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }

              <hr />



              {props.initialValues && props.initialValues.ProviderType != "0" &&
                <div className="SubFormGroup">
                  <Field name="ApiEndPoint" type="text" component={FormTextField} label={localizer.get("ApiEndPoint")} />
                </div>
              }

              {props.initialValues && props.initialValues.ProviderType == "0" &&
                <div className="SubFormGroup">
                  <Field name="SmptHost" type="text" component={FormTextField} label={localizer.get("SmptHost")} />
                  <Field name="SmptPort" type="text" component={FormTextField} label={localizer.get("SmptPort")} />
                  <Field id="EnableSsl" name="EnableSsl" type="checkbox" component={FormCheckBoxField} label={localizer.get("EnableSsl")} />
                </div>
              }

              <Field name="UserName" type="text" component={FormTextField} label={localizer.get("UserName")} />
              <Field name="Password" type="text" component={FormTextField} label={localizer.get("Password")} />

            

              {props.initialValues && props.initialValues.ProviderType == "1" &&
                <div className="SubFormGroup">
                  <Field name="EuNameColumnName" type="text" component={FormTextField} label={localizer.get("EuNameColumnName")} />
                  <Field name="EuEmailColumnName" type="text" component={FormTextField} label={localizer.get("EuEmailColumnName")} />
                  <Field name="EuroMessageFromName" type="text" component={FormTextField} label={localizer.get("EuroMessageFromName")} />
                  <Field name="EuroMessageFromAddress" type="text" component={FormTextField} label={localizer.get("EuroMessageFromAddress")} />
                  <Field name="EuroMessageReplyAddress" type="text" component={FormTextField} label={localizer.get("EuroMessageReplyAddress")} />
                  <Field name="EuroMessageApiUser" type="text" component={FormTextField} label={localizer.get("EuroMessageApiUser")} />
                  <Field name="EuroMessageApiPassword" type="text" component={FormTextField} label={localizer.get("EuroMessageApiPassword")} />
                </div>
              }


              {props.initialValues && props.initialValues.ProviderType == "2" &&
                <div className="SubFormGroup">
               
                  <Field name="MailApiKey" type="text" component={FormTextField} label={localizer.get("MailApiKey")} />
                  <Field name="MailTransCode" type="text" component={FormTextField} label={localizer.get("MailTransCode")} />
                </div>
              }

              <hr />
              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var sendingProviderFormComp = connect(
  state => state.sendingProviderForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(SendingProviderForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditSendingProviderForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(sendingProviderFormComp);
