import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Prices/PriceStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';
//import CKEditor from '@ckeditor/ckeditor5-react';
//import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
    const errors = {}
    if (!values.Amount) {
        errors.Amount = localizer.getWithFormat("RequiredField", "$Name")
    }

    if (!values.PriceCurrencyID || values.PriceCurrencyID == undefined || values.PriceCurrencyID == null || values.PriceCurrencyID === "" || values.PriceCurrencyID == "-1") {
        errors.PriceCurrencyID = localizer.getWithFormat("RequiredField", "$Currency")
    }

    // if (!values.ProductID || values.ProductID == undefined || values.ProductID == null || values.ProductID == "" || values.ProductID == "-1") {
    //   errors.ProductID = localizer.getWithFormat("RequiredField", "$Product")
    // }

    return errors;

}

const warn = values => {
    const warnings = {}
    return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let PriceForm = props => {

    const { handleSubmit, pristine, reset, submitting } = props;

    if (props && props.operationResult && props.operationResult.Status) {

        if (!props.productId) {
            props.push(`/prices/${localizer.GetSelectedLang()}`);
        } else {

            if (!props.stockId) {
                props.push(`/prices/${localizer.GetSelectedLang()}/product/${props.productId}`);

            } else {
                props.push(`/prices/${localizer.GetSelectedLang()}/product/${props.productId}/stock/${props.stockId}`);

            }
        }

    }

    //const fileInput = React.createRef();

    const handleFile = function () {
    }
     
    const handleImageDelete = (e) => {
        e.preventDefault();
        props.DeleteImage(props.id);
    }

    const onFormSubmit = (e) => {

        e.preventDefault();

        var validateResult = handleSubmit(e);

        console.log(validateResult);

        if (validateResult != null || validateResult != undefined) {
            return;
        }
        else {

            let formData = new FormData();

            formData.append('Barkod', e.target.elements.Barkod.value);
            formData.append('Amount', e.target.elements.Amount.value);
            formData.append('AmountOld', e.target.elements.AmountOld.value);
            formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
            formData.append('ProductID', e.target.elements.ProductID.value);
            formData.append('ID', e.target.elements.ID.value);
            formData.append('PriceCurrencyID', e.target.elements.PriceCurrencyID.value);
            formData.append('DomainID', e.target.elements.DomainID.value);
            formData.append('StockID', e.target.elements.StockID.value);
            formData.append('VAT', e.target.elements.VAT.value);
            formData.append('ProductCode', e.target.elements.ProductCode.value);

            props.SaveItem(formData);

        }
    }

    return (

        <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

            <div id="FormHeader">
                <h1>{props.header}</h1>
            </div>

            {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

            {(!props.operationResult || !props.operationResult.Status) &&
                <div>
                    <div className="row">
                        <div id="FormBody" className="col-sm-8">

                            <Field name="ID" type="hidden" component={FormHiddenField} />
                            <Field name="ProductID" type="hidden" component={FormHiddenField} />
                            <Field name="StockID" type="hidden" component={FormHiddenField} />
                            <Field name="ProductCode" type="hidden" component={FormHiddenField} />
                            <Field name="Barkod" type="hidden" component={FormHiddenField} />

                            <Field name="Amount" type="number" step="any" component={FormTextField} label={localizer.get("Amount")} />
                            <Field name="AmountOld" type="number" step="any" component={FormTextField} label={localizer.get("AmountOld")} />
                            <Field name="VAT" type="number" step="any" component={FormTextField} label={localizer.get("VAT")} />

                            {props.initialValues && props.initialValues.AllCurrencies &&
                                <Field name="PriceCurrencyID" label={localizer.get("Currency")} type="select"
                                    component={FormSelectField}
                                    selectText={localizer.getWithFormat('PleaseSelectNewField', '$Currency')}
                                    optionFnc={props.initialValues.AllCurrencies.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                        }
                         
                        {props.initialValues && props.initialValues.AllDomains &&
                            <Field name="DomainID" label={localizer.get("Domain")} type="select"
                                component={FormSelectField}
                                selectText={localizer.getWithFormat('PleaseSelectNewField', '$Domain')}
                                optionFnc={props.initialValues.AllDomains.map(status => (
                                    <option value={status.Value} key={status.Value}>
                                        {status.Name}
                                    </option>
                                ))} />
                        }

                           
                            <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />


                        </div>
                    </div>

                    <FormActions
                        submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
                    </FormActions>
                </div>
            }

        </form>
    )
}

var productFormComp = connect(
    state => state.priceForms,
    dispatch => bindActionCreators(actionCreators, dispatch),
)(PriceForm);

export default reduxForm({
    enableReinitialize: true,
    touched: false,
    form: 'EditPriceForm', // a unique identifier for this form
    validate, // <--- validation function given to redux-form
    warn // <--- warning function given to redux-form
})(productFormComp);
