import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Prices/PriceStore';
import PriceForm from './PriceForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class CargoproductsNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id, productId, stockId } = params;

        this.productId = productId;
        this.stockId = stockId;

        this.props.GetInitialValues(id ? id : 0, productId == undefined ? null : productId, stockId == undefined ? null : stockId );
         
        this.breadCrumbNav =
        [{ link: ``, text: localizer.get("Settings") },
        { link: `/products/${localizer.GetSelectedLang()}/type`, text: localizer.get("Products")}, 
        { link: `/prices/${localizer.GetSelectedLang()}/`, text: localizer.get("Prices")},
        { link: "", text: localizer.getWithFormat("NewField", "$Product") }];
    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        var productId = this.productId;
        var stockId = this.stockId;

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <PriceForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    productId={productId}
                    stockId={stockId}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$Price")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></PriceForm>
            </div>
        );
    }
}

export default connect(
    state => state.prices,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(CargoproductsNew);