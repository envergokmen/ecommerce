import React from 'react';



//ref doesn't work child comp.
// const FormFileInput = ({
//     ref,
//     label,
//     type,
//     name,
//     onchange,
//     meta: { touched, error, warning }
// }) => (

//         <div className={error ? "form-group row FormErrorRow" : "form-group row"}>
//             <label htmlFor="firstName" className="col-sm-2 col-form-label">{label}</label>
//             <div className="col-sm-10">
//                 {props.children}
//                 {/* <input name={name} onChange={onchange} ref={ref} placeholder={label} type={type} className="form-control" /> */}
//                 {touched &&
//                     ((error && <span className="FormFieldError">{error}</span>) ||
//                         (warning && <span className="FormFieldWarning">{warning}</span>))}
//             </div>
//         </div>
//     )


const FormResultMessage = ({
    operationResult
}) => (
    
        <div className={operationResult && operationResult.Status ? "alert alert-success" : operationResult && !operationResult.Status ? "alert alert-danger" :"hidden"}>
            {operationResult && operationResult.ResultDescription && operationResult.ResultDescription.map(function(item, i){ 
            return <div key={i}>{item}</div>
            })}
        </div>
    );


const FormSelectField = ({
    input,
    label,
    type,
    selectText,
    desc,
    meta: { touched, error, warning },
    optionFnc,
}) => (

        <div className={touched && error ? "form-group row FormErrorRow" : "form-group row"}>
            <label htmlFor="firstName" className="col-sm-3 col-form-label">{label}</label>
            <div className="col-sm-9">
                <select {...input} placeholder={label} type={type} className="form-control" >
                    {selectText && <option value="">{selectText}</option> }
                    {optionFnc}
                </select>
                {touched &&
                    ((error && <span className="FormFieldError">{error}</span>) ||
                        (warning && <span className="FormFieldWarning">{warning}</span>))}
            </div>
        </div>
    );

const FormTextField = ({
    input,
    label,
    type,
    info,
    desc,
    meta: { touched, error, warning },
}) => (

        <div className={touched && error ? "form-group row FormErrorRow" : "form-group row"}>
            <label htmlFor="firstName" className="col-sm-3 col-form-label">{label}</label>
            <div className="col-sm-9">
                <input {...input} placeholder={label} type={type} className="form-control" />
                {desc && <span className="desc">{desc}</span>}
                {info && <span className="formFieldInfo">{info}</span>}
                {touched &&
                    ((error && <span className="FormFieldError">{error}</span>) ||
                        (warning && <span className="FormFieldWarning">{warning}</span>))}
            </div>
        </div>
    )


    const FormCheckBoxField = ({
        input,
        label,
        type,
        name,
        desc,
        id,
        meta: { touched, error, warning }
    }) => (
    
            <div className={touched && error ? "form-group row FormErrorRow" : "form-group row"}>
                <label htmlFor="firstName" className="col-sm-3 col-form-label">{label}</label>
                <div className="col-sm-9">

                    <input {...input} className="tgl tgl-light" id={id} type={type} /> 
                    <label className="tgl-btn" htmlFor={id}></label>
                    {desc && <span className="desc">{desc}</span>}
                    {/* <input {...input} placeholder={label} type={type} className="form-control" /> */}
                    {touched &&
                        ((error && <span className="FormFieldError">{error}</span>) ||
                            (warning && <span className="FormFieldWarning">{warning}</span>))}
                </div>
            </div>
        )

    const FormStatusField = ({
        input,
        label,
        type,
        desc,
        meta: { touched, error, warning }
    }) => (
    
            <div className={touched && error ? "form-group row FormErrorRow" : "form-group row"}>
                <label htmlFor="firstName" className="col-sm-3 col-form-label">{label}</label>
                <div className="col-sm-9">

                    <input {...input} className="tgl tgl-light" id="PStatus1" type={type} /> 
                    <label className="tgl-btn" htmlFor="PStatus1"></label>
                    {desc && <span className="desc">{desc}</span>}
                    {/* <input {...input} placeholder={label} type={type} className="form-control" /> */}
                    {touched &&
                        ((error && <span className="FormFieldError">{error}</span>) ||
                            (warning && <span className="FormFieldWarning">{warning}</span>))}
                </div>
            </div>
        )

    const FormHiddenField = ({
        input,
        type="hidden"
    }) => (
        <input {...input} type={type} className="form-control" />
        )

const FormActions = ({
    submitting,
    pristine,
    reset,
    saveText,
    resetText

}) => (

        <div id="FormActions" className="row">
            <div className="col-sm-8">
                <div className="form-group row">
                    <label htmlFor="email" className="col-sm-3 col-form-label"></label>
                    <div className="col-sm-9">
                        <button id="SaveButton" type="submit" className="btn btn-success" disabled={submitting}>{saveText}</button>
                        <button id="ResetButton" type="button" className="btn btn-secondary" disabled={pristine || submitting} onClick={reset}>{resetText}</button>
                    </div>
                </div>
            </div>
        </div>

    )


export {FormCheckBoxField, FormStatusField, FormHiddenField, FormResultMessage, FormSelectField, FormTextField, FormActions }


