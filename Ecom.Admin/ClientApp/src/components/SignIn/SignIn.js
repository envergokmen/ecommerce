import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/SignIn/SignInStore';
import './SignIn.css';
import sharedTranslations from '../../Localizer/Translations/_Shared';
import Localizer from "../../Localizer/Localizer";
var localizer = new Localizer(sharedTranslations, '', 'en');
class SignIn extends Component {

  constructor(props) {

    super(props);
    this.state = { RememberMe: true, isLoggedIn: false };
    //console.log("cons login result", this.props.LoginResult);
    // this.props.LoginResult = null;

  }
  componentWillMount() {

    //console.log("Will mounth LoginResult", this.props.LoginResult);
  }

  componentDidMount() {

    //console.log("SıgnIn componentDidMount");
    //console.log("LoginResult did mounth props", this.props.LoginResult);
  }

  render() {
 
    if (this.props.LoginResult && this.props.LoginResult.Status) {
      setTimeout(() => {
        this.clearLogin();
        this.props.history.push(`/${localizer.GetSelectedLang()}`)
      }, 10);
    }

    return (
      <div id="LoginRegisterCon" className="container">
        {((this.props.LoginResult == null || !this.props.LoginResult.Status) &&
          <h1>{localizer.get('SignIn').toLocaleUpperCase(localizer.GetSelectedLang())}</h1>
        )}
        {this.props.LoginResult &&
          ((!this.props.LoginResult.Status &&
            <div className="alert alert-danger" id="LoginResult">
              {this.props.LoginResult && this.props.LoginResult.ResultDescription && this.props.LoginResult.ResultDescription.map((item, i) => <p key={i}>{item}</p>)}
            </div>
          ))}
        {this.props.LoginResult &&
          ((this.props.LoginResult.Status &&
            <div className="alert alert-success" id="LoginResult">
              {this.props.LoginResult.ResultDescription.map((item, i) => <span key={i}>{item}</span>)}
              <div className="spinner-grow spinner-grow-sm text-success float-right" role="Status">
                <span className="sr-only">Loading...</span>
              </div>
            </div>
          ))}

        {((this.props.LoginResult == null || !this.props.LoginResult.Status) &&

          <form action="/Login" id="LoginRegisterForm" method="post" onSubmit={this.handleSubmit.bind(this)}>

            <input className="block-level"
              id="UserName" name="UserName"
              placeholder="Username / Email"
              maxLength="90"
              required="required" type="text" />

            <input className="block-level"
              id="PassWord"
              name="PassWord"
              placeholder="Password"
              maxLength="30"
              required="required" type="password" />

            <div className="form-check">
              <input
                checked={this.state.RememberMe}
                onChange={(e) => {
                  this.handleChange({
                    target: {
                      name: e.target.name,
                      value: e.target.checked,
                    }
                  })
                }}
                type="checkbox" className="form-check-input" id="RememberMe" name="RememberMe" />
              <label className="form-check-label" htmlFor="RememberMe">{localizer.get('RememberMe')}</label>
            </div>

            <button type="submit" id="LoginButton">{localizer.get('Login').toLocaleUpperCase(localizer.GetSelectedLang())}</button>
            <p className="ResetPassword">
              {localizer.get('HavingTrouble')} <a href="/PasswordReset">{localizer.get('ResetYourPassword')}</a>
            </p>
          </form>
        )}

      </div>

    )
  }

  clearLogin() {
    this.props.ClearLoginInfo();
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleSubmit(event) {

    event.preventDefault();
    var rememberMe = !this.state.RememberMe ? false : this.state.RememberMe;
    var loginModel = {
      'UserName': event.target.UserName.value,
      'PassWord': event.target.PassWord.value,
      'RememberMe': rememberMe
    };

    this.props.Login(
      loginModel
    );
  }

}

export default connect(
  state => state.signin,
  dispatch => bindActionCreators(actionCreators, dispatch),

)(SignIn);