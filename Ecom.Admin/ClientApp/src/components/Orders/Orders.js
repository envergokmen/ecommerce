import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Orders/OrderStore';

class Orders extends Component {
    componentWillMount() {
        //get products
    }

    componentWillReceiveProps(nextProps) {
        // This method runs when incoming props (e.g., route params) change
        //const startDateIndex = parseInt(nextProps.match.params.startDateIndex, 10) || 0;
        //this.props.requestWeatherForecasts(startDateIndex);
    }

    render() { 
        return (
            <h1>Orders</h1>
        );
    }

}
 
export default connect( 
    state => state.orders,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(Orders);