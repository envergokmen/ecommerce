import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/WebPoses/WebPosStore';
import { Link } from "react-router-dom";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';
import { Localize, Localizer } from "../../Localizer/Localizer";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {default as queryString } from 'qs';
import { Router } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class WebPoses extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            breadCrumbNav: []
        };

        this.toggleModal = this.toggleModal.bind(this);
        this.NextPage = this.NextPage.bind(this);
        this.delete = this.delete.bind(this);
    }


    NextPage(event) {

        event.preventDefault();
        event.stopPropagation();
        this.props.SearchCargopayments({ append: true, page: this.props.Page + 1, searchKeyword: this.props.searchKeyword });
        this.props.history.push(`/payments/${localizer.GetSelectedLang()}/${this.props.Page + 1}${this.searchKeyword}`);
    }

    delete(event) {

        event.preventDefault();
        event.stopPropagation();
        this.props.DeleteItem(this.state.IdToDelete);
        this.toggleModal(event);
    }

    toggleModal(event = null) {
        event.preventDefault();
        event.stopPropagation();

        const { id, name } = event.target;

        this.setState(prevState => ({
            modal: !prevState.modal,
            IdToDelete: id,
            name: name
        }));

    }
    componentWillMount() {

        const { match: { params } } = this.props;
        this.lang = params.lang;
        this.paymentId = params.paymentId;

        this.page = params.page ? params.page : 1;
        this.searchKeywordQs = (queryString.parse(this.props.location.search).searchKeyword) ? `?searchKeyword=${queryString.parse(this.props.location.search).searchKeyword}` : '';
        this.searchKeyword = (queryString.parse(this.props.location.search).searchKeyword) ? queryString.parse(this.props.location.search).searchKeyword : '';
        this.newLink = !this.paymentId ? `/webposes/${localizer.GetSelectedLang()}/new` : `/webposes/${localizer.GetSelectedLang()}/new/payment/${this.paymentId}`;

        //alert(this.typeId);

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: `/payments/${localizer.GetSelectedLang()}`, text: localizer.get("Payments") },
            { link: "", text: localizer.get("WebPoses") }];

        this.setState({ breadCrumbNav: this.breadCrumbNav });

        var that = this;

        fetch(`/${localizer.GetSelectedLang()}/payment/GetName/${this.paymentId}`).then(function (response) {
            return response.text().then(function (text) {
                that.breadCrumbNav.push({ link: "", text: text });
                that.setState({ breadCrumbNav: that.breadCrumbNav });
            });
        });


    }


    componentDidMount() {

        this.props.SearchWebPoses({ page: this.page, searchKeyword: this.searchKeyword, paymentId: this.paymentId });
        this.props.ResetFormResult();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps && nextProps.deleteResult && !nextProps.deleteResult.Status) {
            toast.error(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.deleteResult && nextProps.deleteResult.Status) {
            toast.success(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }

        //save result from prev from submit
        if (nextProps && nextProps.saveResult && !nextProps.saveResult.Status) {
            toast.error(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.saveResult && nextProps.saveResult.Status) {
            toast.success(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }
    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.state.breadCrumbNav}></BreadCrumb>


                <div id="MainListArea">

                    <Link to={this.newLink} className="btn btn-primary">{localizer.getWithFormat("NewField", "$WebPos")}</Link>

                    {(!this.props.payments || this.props.payments.length == 0) &&
                        <div className="NoListItem">
                            <p>{localizer.getWithFormat("NoListItem", "$WebPos")}</p>
                            <Link to={this.newLink} className="btn btn-primary">{localizer.getWithFormat("NewField", "$WebPos")}</Link>
                        </div>
                    }

                    {this.props.payments &&
                        <table className="table table-striped ListingTable">

                            <thead className="thead-light">
                                <tr>
                                    <th>ID</th>
                                <th className="percent20">{localizer.get('Logo')}</th>
                                <th className="percent20">{localizer.get('Name')}</th>
                                    <th className="percent15">{localizer.get('Provider')}</th>
                                    <th className="percent15">{localizer.get('Installments')}</th>  
                                    <th>{localizer.get('Status')}</th>
                                    <th className="Operations">{localizer.get('Operations')}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.payments.map((item, index) =>
                                    <tr key={item.ID} className={item.PStatus == 1 ? '' : 'text-muted'}>
                                        <td className="Id">{item.ID}</td>
                                        <td>
                                            {item.LogoPath && item.LogoPath != "" && <img style={{ maxWidth: 50 }} src={item.LogoPath} alt=""></img>}
                                        </td>
                                        <td>{item.Name}</td>
                                        <td>{item.PaymentTypeStr}</td>
                                        <td>
                                            <Link to={`/webposinstallments/${localizer.GetSelectedLang()}/webpos/${item.ID}`}>({item.InstallmentsCount}) {localizer.get("Installments")} </Link>
                                            {item.InstallmentsCount == 0 &&
                                                <span className="WarningIcon" title={localizer.get('AddAtLeastOneInstallment')}>
                                                    {localizer.get('Warning')}
                                                </span>
                                            }
                                        </td>
                                       
                                        <td className="PStatus">
                                            <span className={item.PStatus == "1" ? "Status Active" : "Status Passive"}></span>
                                            <span className="StatusText">
                                                {item.PStatus == "1" && localizer.get("Active")}
                                                {item.PStatus != "1" && localizer.get("Passive")}
                                            </span>

                                        </td>
                                        <td className="Operations">

                                            <Link to={`/webposes/${localizer.GetSelectedLang()}/edit/${item.ID}/payment/${item.PaymentID}`} className="btn btn-sm btn-warning"><Localize localizer={localizer}>Edit</Localize></Link>
                                            <button id={item.ID} name={item.Name} onClick={(event) => this.toggleModal(event)} className="btn btn-sm btn-danger"><Localize localizer={localizer}>Delete</Localize></button>

                                        </td>
                                    </tr>
                                )}


                            </tbody>

                        </table>
                    }

                    {this.props.payments &&
                        this.props.TotalPage > this.props.Page &&
                        <div className="text-center">
                            <button onClick={this.NextPage} className="btn btn-success LoadMore">Load More</button>
                        </div>
                    }

                    <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={this.props.className}>
                        <ModalHeader toggle={this.toggleModal}>
                            {localizer.get("ConfirmDelete")}
                        </ModalHeader>
                        <ModalBody>
                            <b>{this.state.name}</b> {' '}
                            {localizer.get('WillBeDeleted')}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={this.delete}>{localizer.get('Confirm')}</Button>{' '}
                            <Button color="secondary" onClick={this.toggleModal}>{localizer.get('Cancel')}</Button>
                        </ModalFooter>
                    </Modal>

                    <ToastContainer />
                </div>
            </div >
        );
    }

}

export default connect(
    state => state.webPoses,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(WebPoses);