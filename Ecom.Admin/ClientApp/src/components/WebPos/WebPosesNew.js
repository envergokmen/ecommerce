import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/WebPoses/WebPosStore';
import WebPosForm from './WebPosForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class WebPosesNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id, paymentId } = params;

        this.paymentId = paymentId;
        this.props.GetInitialValues(id ? id : 0, paymentId == undefined ? null: paymentId);
         
        this.breadCrumbNav =
        [{ link: ``, text: localizer.get("Settings") },
        { link: `/payments/${localizer.GetSelectedLang()}/type`, text: localizer.get("Payments")}, 
        { link: `/webposes/${localizer.GetSelectedLang()}/`, text: localizer.get("WebPoses")},
        { link: "", text: localizer.getWithFormat("NewField", "$WebPos") }];
    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        var paymentId = this.paymentId;

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <WebPosForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    paymentId={paymentId}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$WebPosTranslation")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></WebPosForm>
            </div>
        );
    }
}

export default connect(
    state => state.webPoses,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(WebPosesNew);