import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/WebPoses/WebPosStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';
//import CKEditor from '@ckeditor/ckeditor5-react';
//import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
    const errors = {}
    if (!values.Name) {
        errors.Name = localizer.getWithFormat("RequiredField", "$Name")
    }

    if (values.PaymentID == undefined || values.PaymentID == null || values.PaymentID == "" || values.PaymentID == "-1") {
        errors.PaymentID = localizer.getWithFormat("RequiredField", "$Payment")
    }

    if (values.PaymentType == undefined || values.PaymentType == null || values.PaymentType == "" || values.PaymentType == "-1") {
        errors.PaymentType = localizer.getWithFormat("RequiredField", "$Provider")
    }

    return errors;

}

const warn = values => {
    const warnings = {}
    return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let WebPosForm = props => {

    const { handleSubmit, pristine, reset, submitting } = props;

    if (props && props.operationResult && props.operationResult.Status) {

        if (!props.paymentId) {
            props.push(`/webposes/${localizer.GetSelectedLang()}`);
        } else {
            props.push(`/webposes/${localizer.GetSelectedLang()}/payment/${props.paymentId}`);
        }

    }

    const fileInput = React.createRef();

    const handleFile = function () {
    }

    const handleImageDelete = (e) => {
        e.preventDefault();
        props.DeleteImage(props.id);
    }

    const onFormSubmit = (e) => {

        e.preventDefault();

        var validateResult = handleSubmit(e);

        if (validateResult != null || validateResult != undefined) {
            return;
        }
        else {


            let formData = new FormData();
            formData.append('Name', e.target.elements.Name.value);
            formData.append('BankCode', e.target.elements.BankCode.value);
            formData.append('PostUrl', e.target.elements.PostUrl.value);
            formData.append('NormalPostUrl', e.target.elements.NormalPostUrl.value);
            formData.append('MagazaNo', e.target.elements.MagazaNo.value);
            formData.append('SecureKey3D', e.target.elements.SecureKey3D.value);
            formData.append('ApiUser', e.target.elements.ApiUser.value);
            formData.append('ApiPassword', e.target.elements.ApiPassword.value);
            formData.append('ProvType', e.target.elements.ProvType.value);
            formData.append('PostNetNo', e.target.elements.PostNetNo.value);
            formData.append('PaymentType', e.target.elements.PaymentType.value);
            formData.append('SortNo', e.target.elements.SortNo.value);
            formData.append('MountDelayCount', e.target.elements.MountDelayCount.value);
            formData.append('EntegHesapKodu', e.target.elements.EntegHesapKodu.value);
            formData.append('EntegHesapAdi', e.target.elements.EntegHesapAdi.value);
            formData.append('EntegRefKodu', e.target.elements.EntegRefKodu.value);
            formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
            formData.append('PaymentID', e.target.elements.PaymentID.value);
            formData.append('ID', e.target.elements.ID.value);

            formData.append('file', fileInput.current.files[0]);

            props.SaveItem(formData);

        }
    }

    return (

        <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

            <div id="FormHeader">
                <h1>{props.header}</h1>
            </div>

            {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

            {(!props.operationResult || !props.operationResult.Status) &&
                <div>
                    <div className="row">
                        <div id="FormBody" className="col-sm-8">

                            <Field name="ID" type="hidden" component={FormHiddenField} />
                            <Field name="Name" type="text" component={FormTextField} label={localizer.get("Name")} />
                            <Field name="BankCode" type="text" component={FormTextField} label={localizer.get("BankCode")} />
                            <Field name="PostUrl" type="text" component={FormTextField} label={localizer.get("PostUrl")} />
                            <Field name="NormalPostUrl" type="text" component={FormTextField} label={localizer.get("NormalPostUrl")} />
                            <Field name="MagazaNo" type="text" component={FormTextField} label={localizer.get("StoreNo")} />
                            <Field name="SecureKey3D" type="text" component={FormTextField} label={localizer.get("SecureKey3D")} />
                            <Field name="ApiUser" type="text" component={FormTextField} label={localizer.get("ApiUser")} />
                            <Field name="ApiPassword" type="text" component={FormTextField} label={localizer.get("ApiPassword")} />
                            <Field name="ProvType" type="text" component={FormTextField} label={localizer.get("ProvType")} />
                            <Field name="PostNetNo" type="text" component={FormTextField} label={localizer.get("PostNetNo")} />
                            <Field name="SortNo" type="number" component={FormTextField} label={localizer.get("SortNo")} />
                            <Field name="MountDelayCount" type="text" component={FormTextField} label={localizer.get("MountDelayCount")} />
                            <Field name="EntegHesapKodu" type="text" component={FormTextField} label={localizer.get("EntegHesapKodu")} />
                            <Field name="EntegHesapAdi" type="text" component={FormTextField} label={localizer.get("EntegHesapAdi")} />
                            <Field name="EntegRefKodu" type="text" component={FormTextField} label={localizer.get("EntegRefKodu")} />

                            {props.initialValues && props.initialValues.AllPayments &&
                                <Field name="PaymentID" label={localizer.get("Payment")} type="select"
                                    component={FormSelectField}
                                    selectText={localizer.getWithFormat('PleaseSelectNewField', '$Payment')}
                                    optionFnc={props.initialValues.AllPayments.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }

                            {props.initialValues && props.initialValues.AllPosPaymentTypes &&
                                <Field name="PaymentType" label={localizer.get("PaymentProvider")} type="select"
                                    component={FormSelectField}
                                    selectText={localizer.getWithFormat('PleaseSelectNewField', '$ProviderType')}
                                    optionFnc={props.initialValues.AllPosPaymentTypes.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }


                        <div className="form-group row">
                            <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get("Logo")}</label>
                            <div className="col-sm-9">
                                <input name="ImagePath" onChange={handleFile} ref={fileInput} type="file" className="form-control" />
                            </div>
                        </div>


                            <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />


                        </div>
                    </div>

                    <FormActions
                        submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
                    </FormActions>
                </div>
            }

        </form>
    )
}

var webPosFormComp = connect(
    state => state.webPosForms,
    dispatch => bindActionCreators(actionCreators, dispatch),
)(WebPosForm);

export default reduxForm({
    enableReinitialize: true,
    touched: false,
    form: 'EditWebPosForm', // a unique identifier for this form
    validate, // <--- validation function given to redux-form
    warn // <--- warning function given to redux-form
})(webPosFormComp);
