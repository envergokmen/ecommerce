import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Policies/PolicyStore';
import PolicyForm from './PolicyForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class PoliciesNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id, type } = params;
        this.type = type;
         
        if (id) {
            this.props.GetInitialValues(id, type);
        } else {
            this.props.GetInitialValues(0, type);
        }

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: `/policies/${lang}/type/${type}`, text: localizer.get("Policies") },
            { link: "", text: localizer.getWithFormat("NewField", "$Policy") }]

    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {
        var type = this.type;
         
        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <PolicyForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    type={type}
                    header={localizer.getWithFormat("NewField", "$Policy")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></PolicyForm>
            </div>
        );
    }
}

export default connect(
    state => state.policies,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(PoliciesNew);
