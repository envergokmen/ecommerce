import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/CatalogLocalLangs/CatalogLocalLangStore';
import CatalogLocalLangForm from './CatalogLocalLangForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class CargocatalogsNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id, catalogId } = params;

        this.catalogId = catalogId;
        this.props.GetInitialValues(id ? id : 0, catalogId == undefined ? null: catalogId);
         
        this.breadCrumbNav =
        [{ link: ``, text: localizer.get("Settings") },
        { link: `/catalogs/${localizer.GetSelectedLang()}/type`, text: localizer.get("Catalogs")}, 
        { link: `/cataloglocallangs/${localizer.GetSelectedLang()}/`, text: localizer.get("CatalogTranslations")},
        { link: "", text: localizer.getWithFormat("NewField", "$Catalog") }];
    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        var catalogId = this.catalogId;

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <CatalogLocalLangForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    catalogId={catalogId}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$CatalogTranslation")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></CatalogLocalLangForm>
            </div>
        );
    }
}

export default connect(
    state => state.catalogLocalLangs,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(CargocatalogsNew);