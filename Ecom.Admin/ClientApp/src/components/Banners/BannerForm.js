import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Banners/BannerStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$Banner")
  }

  if (values.BannerShowPlace === undefined || values.BannerShowPlace === null || values.BannerShowPlace === "") {
    errors.BannerShowPlace = localizer.getWithFormat("RequiredField", "$BannerShowPlace")
  }

  if (values.Link === undefined || values.Link === null || values.Link === "") {
    errors.Link = localizer.getWithFormat("RequiredField", "$Link")
  }

  // if (values.LangID === undefined || values.LangID === null || values.LangID === "") {
  //   errors.LangID = localizer.getWithFormat("RequiredField", "$Language")
  // }
  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let BannerForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/banners/${localizer.GetSelectedLang()}`);
  }

  const fileInput = React.createRef();

  const handleFile = function () {
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {

      let formData = new FormData();
      formData.append('Name', e.target.elements.Name.value);
      // formData.append('ImagePath', e.target.elements.ImagePath.value);
      formData.append('BannerShowPlace', e.target.elements.BannerShowPlace.value);
      formData.append('LangID', e.target.elements.LangID.value);
      formData.append('DomainID', e.target.elements.DomainID.value);
      formData.append('Link', e.target.elements.Link.value);
      formData.append('SortNo', e.target.elements.SortNo.value);
      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('ID', e.target.elements.ID.value);

      formData.append('file', fileInput.current.files[0]);

      props.SaveItem(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}
      {props.deleteImgResult && <FormResultMessage operationResult={props.deleteImgResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="Name" type="text" component={FormTextField} label={localizer.get("Name")} />
              <Field name="SortNo" type="number" component={FormTextField} label={localizer.get("SortNo")} />
             
              <Field name="Link" type="text" component={FormTextField} label={localizer.get("Link")} />

              {props.initialValues && props.initialValues.AllBannerPlaces &&
                <Field name="BannerShowPlace" label={localizer.get("BannerShowPlace")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$BannerShowPlace')}
                  optionFnc={props.initialValues.AllBannerPlaces.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


              <div className="form-group row">
                <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get("ImagePath")}</label>
                <div className="col-sm-9">
                  <input name="ImagePath" onChange={handleFile} ref={fileInput} type="file" className="form-control" />
                </div>
              </div>



              {props.initialValues && props.initialValues.AllLangs &&
                <Field name="LangID" label={localizer.get("Language")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Language')}
                  optionFnc={props.initialValues.AllLangs.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


            {props.initialValues && props.initialValues.AllDomains &&
                <Field name="DomainID" label={localizer.get("Domain")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Domain')}
                  optionFnc={props.initialValues.AllDomains.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }

              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />

              {props.initialValues && props.initialValues.ImagePath &&
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">{localizer.get("Logo")}</label>
                  <div className="col-sm-9">
                    <img style={{ maxWidth: 100 }} src={props.initialValues.ImagePath} alt=""></img>
                    <div style={{ marginTop: 10 }}>
                      <a href="javascript:;" onClick={handleImageDelete} className="btn btn-danger btn-sm">{localizer.get("Delete")}</a>
                    </div>
                  </div>
                </div>
              }

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var bannerFormComp = connect(
  state => state.bannerForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(BannerForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditBannerForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(bannerFormComp);
