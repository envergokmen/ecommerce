import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Banners/BannerStore';
import BannerForm from './BannerForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class BannersNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id } = params;

        if (id) {
            this.props.GetInitialValues(id);
        } else {
            this.props.GetInitialValues(0);
        }

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: `/banners/${lang}`, text: localizer.get("Banners") },
            { link: "", text: localizer.getWithFormat("NewField", "$Banner") }]

    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <BannerForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    operationResult={this.props.operationResult}
                    deleteImgResult={this.props.deleteImgResult} 
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$Banner")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></BannerForm>
            </div>
        );
    }
}

export default connect(
    state => state.banners,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(BannersNew);