import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/ProductCatalogs/SortStore';
import { Link } from "react-router-dom";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';
import { Localize, Localizer } from "../../Localizer/Localizer";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {default as queryString } from 'qs';
import { Router } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import arrayMove from 'array-move';

var localizer = new Localizer([sharedTranslations, navTranslations]);

const SortableItem = SortableElement(({ value, deleteOp, moveToTop }) =>
    <li className="SortTableItem">
        {value.ProductCode}
        <img src={value.ImagePath} alt="" />
        {value.ProductName}
        <b> ({value.ColorName})</b>
        <button onClick={deleteOp} value={value.ID} className="btn btn-sm btn-danger">{localizer.get("Delete")}</button>
        <button onClick={moveToTop} value={value.ID} className="btn btn-sm btn-secondary">{localizer.get("ToTop")}</button>
    </li>);

const SortableList = SortableContainer(({ items, deleteOp, moveToTop }) => {
    return (
        <ul id="Sortable">
            {items.map((value, index) => (
                <SortableItem
                    key={`item-${index}`}
                    index={index}
                    value={value}
                    deleteOp={deleteOp}
                    moveToTop={moveToTop}
                />
            ))}
        </ul>
    );
});




class SortCatalog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modal: false,
            breadCrumbNav: [],
            items: ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6']
        };

        this.deleteFromCatalog = this.deleteFromCatalog.bind(this);
        this.moveToTop = this.moveToTop.bind(this);
    }

    onSortEnd = ({ oldIndex, newIndex }) => {


        var payload = {
            Items: arrayMove(this.props.AddedCatalogs, oldIndex, newIndex), CatalogId: this.catalogId
        };

        this.props.UpdateCatalogSort(payload);

    }

    moveToTop(e) {

        this.props.MoveToTop(e.target.value, this.state.catalogId);
    }

    deleteFromCatalog(e) {

        this.props.DeleteFromCatalogSort(e.target.value, this.state.catalogId);
    }

    togglePrd(colorId) {
        this.props.ToggleProduct(colorId);
    }

    addSelected() {

        var selectedColors = this.props.ProductColors.filter(x => x.IsChecked == true).map(function (v) {
            return v.ColorID;
        });;

        var selectedCatalogs = this.props.Catalogs.filter(x => x.IsChecked == true).map(function (v) {
            return v.ID;
        });;;

        var productID = this.props.ProductID;

        //console.log(selectedColors, selectedCatalogs, productID);
        var requestModel = { ProductID: productID, CatalogIDs: selectedCatalogs, ColorIDs: selectedColors };

        this.props.AddToCatalog(requestModel);
    }


    toggleCat(event) {

        var catalogId = event.target.value;
        this.props.ToggleCatalog(catalogId);
    }

    componentWillMount() {

        const { match: { params } } = this.props;
        this.lang = params.lang;
        this.catalogId = params.catalogId;

        this.page = params.page ? params.page : 1;
        this.searchKeywordQs = (queryString.parse(this.props.location.search).searchKeyword) ? `?searchKeyword=${queryString.parse(this.props.location.search).searchKeyword}` : '';
        this.searchKeyword = (queryString.parse(this.props.location.search).searchKeyword) ? queryString.parse(this.props.location.search).searchKeyword : '';
        this.newLink = !this.catalogId ? `/productcatalogs/${localizer.GetSelectedLang()}/new` : `/productcatalogs/${localizer.GetSelectedLang()}/new/product/${this.catalogId}`;

        //alert(this.typeId);
        var breadCrumb = [
            { link: `/products/${localizer.GetSelectedLang()}`, text: localizer.get("Products") },
            { link: `/catalogs/${localizer.GetSelectedLang()}`, text: localizer.get("Catalogs") }];

        this.breadCrumbNav = breadCrumb;

        this.setState({ breadCrumbNav: this.breadCrumbNav, catalogId: this.catalogId });

        var that = this;

        if ((this.state.breadCrumbNav.length == 2 || this.state.breadCrumbNav.length == 0) && !this.breadCrumbName) {

            that.setState({ breadCrumbName: "..." });

            fetch(`/${localizer.GetSelectedLang()}/catalog/GetName/${this.catalogId}`).then(function (response) {
                return response.text().then(function (text) {
                    that.breadCrumbNav.push({ link: "", text: text });
                    that.setState({ breadCrumbNav: that.breadCrumbNav, breadCrumbName: text });
                });
            });
        }


    }


    componentDidMount() {

        this.props.SearchProductCatalogs({ page: this.page, searchKeyword: this.searchKeyword, catalogId: this.catalogId });
        this.props.ResetFormResult();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps && nextProps.deleteResult && !nextProps.deleteResult.Status) {
            toast.error(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.deleteResult && nextProps.deleteResult.Status) {
            toast.success(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }

        //save result from prev from submit
        if (nextProps && nextProps.saveResult && !nextProps.saveResult.Status) {
            toast.error(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.saveResult && nextProps.saveResult.Status) {
            toast.success(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }

        var that = this;

        var breadCrumb = [
            { link: `/products/${localizer.GetSelectedLang()}`, text: localizer.get("Products") },
            { link: `/catalogs/${localizer.GetSelectedLang()}`, text: localizer.get("Catalogs") }];


        that.breadCrumbNav = breadCrumb;

        if (this.state.breadCrumbNav.length == 2 && !this.state.breadCrumbName) {

            that.setState({  breadCrumbName: "..." });

            fetch(`/${localizer.GetSelectedLang()}/catalog/GetName/${this.catalogId}`).then(function (response) {
                return response.text().then(function (text) {

                    if (that.state.breadCrumbNav.length == 2) {

                        that.breadCrumbNav.push({ link: "", text: text });
                        that.setState({ breadCrumbNav: that.breadCrumbNav, breadCrumbName: text });
                    }

                });
            });

        }


    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.state.breadCrumbNav}></BreadCrumb>

                <div id="MainListArea" className="ProductInCatalogs Sort">

                    <div className="row">

                        <div id="CatalogSort" className="col-sm-24">
                            {this.props.AddedCatalogs &&
                                <SortableList axis="xy" items={this.props.AddedCatalogs} catalogId={this.catalogId} onSortEnd={this.onSortEnd} moveToTop={this.moveToTop} deleteOp={this.deleteFromCatalog} />}

                        </div>

                    </div>

                </div>
            </div>
        );
    }

}

export default connect(
    state => state.sorts,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(SortCatalog);