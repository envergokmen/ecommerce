import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Colors/ColorStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$Color")
  }

  if (!values.ColorGlobalID || values.ColorGlobalID === undefined || values.ColorGlobalID === null || values.ColorGlobalID === "") {
    errors.ColorGlobalID = localizer.getWithFormat("RequiredField", "$ColorGlobal")
  }

  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let ColorForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/colors/${localizer.GetSelectedLang()}`);
  }

  const fileInput = React.createRef();

  const handleFile = function () {
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {

      let formData = new FormData();
      formData.append('Name', e.target.elements.Name.value);
      formData.append('ColorDescription', e.target.elements.ColorDescription.value);
      formData.append('ColorCode', e.target.elements.ColorCode.value);
      // formData.append('PatternPath', e.target.elements.PatternPath.value);
      formData.append('ColorGlobalID', e.target.elements.ColorGlobalID.value);
      formData.append('HexValue', e.target.elements.HexValue.value);
      formData.append('SortNo', e.target.elements.SortNo.value);
      formData.append('SortNo', e.target.elements.SortNo.value);
      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('ID', e.target.elements.ID.value);

      formData.append('file', fileInput.current.files[0]);

      props.SaveItem(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}
      {props.deleteImgResult && <FormResultMessage operationResult={props.deleteImgResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="Name" type="text" component={FormTextField} label={localizer.get("Color")} />
              <Field name="SortNo" type="number" component={FormTextField} label={localizer.get("SortNo")} />
              <Field name="ColorDescription" type="text" component={FormTextField} label={localizer.get("ColorDescription")} />
              <Field name="ColorCode" type="text" component={FormTextField} label={localizer.get("ColorCode")} />
              {/* <Field name="PatternPath" type="text" component={FormTextField} label={localizer.get("PatternPath")} /> */}
              <Field name="HexValue" type="text" component={FormTextField} label={localizer.get("HexValue")} />

              {props.initialValues && props.initialValues.AllColorGlobals &&
                <Field name="ColorGlobalID" label={localizer.get("ColorGlobal")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$ColorGlobal')}
                  optionFnc={props.initialValues.AllColorGlobals.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


              <div className="form-group row">
                <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get("PatternPath")}</label>
                <div className="col-sm-9">
                  <input name="PatternPath" onChange={handleFile} ref={fileInput} type="file" className="form-control" />
                </div>
              </div>


              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />

              {props.initialValues && props.initialValues.PatternPath &&
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">{localizer.get("Logo")}</label>
                  <div className="col-sm-9">
                    <img style={{ maxWidth: 100 }} src={props.initialValues.PatternPath} alt=""></img>
                    <div style={{ marginTop: 10 }}>
                      <a href="javascript:;" onClick={handleImageDelete} className="btn btn-danger btn-sm">{localizer.get("Delete")}</a>
                    </div>
                  </div>
                </div>
              }

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var colorFormComp = connect(
  state => state.colorForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(ColorForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditColorForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(colorFormComp);
