import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/ProductCatalogs/ProductCatalogStore';
import { Link } from "react-router-dom";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';
import { Localize, Localizer } from "../../Localizer/Localizer";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {default as queryString } from 'qs';
import { Router } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class ProductCatalogs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            breadCrumbNav: []
        };

        this.deleteFromCatalog = this.deleteFromCatalog.bind(this);
        this.deleteFromAllCatalogs = this.deleteFromAllCatalogs.bind(this);
        this.togglePrd = this.togglePrd.bind(this);
        this.toggleCat = this.toggleCat.bind(this);
        this.addSelected = this.addSelected.bind(this);
    }


    deleteFromCatalog(productInCatalogId) {

        this.props.DeleteFromCatalog(productInCatalogId);
    }

    deleteFromAllCatalogs() {

        this.props.DeleteFromAllCatalogs(this.props.ProductID);
    }

    togglePrd(colorId) {
        this.props.ToggleProduct(colorId);
    }

    addSelected() {

        var selectedColors = this.props.ProductColors.filter(x => x.IsChecked == true).map(function(v){
            return v.ColorID;
        });;

        var selectedCatalogs = this.props.Catalogs.filter(x => x.IsChecked == true).map(function(v){
            return v.ID;
        });;;

        var productID = this.props.ProductID;

        //console.log(selectedColors, selectedCatalogs, productID);
        var requestModel = {ProductID:productID, CatalogIDs:selectedCatalogs, ColorIDs: selectedColors  };

        this.props.AddToCatalog(requestModel);
    }


    toggleCat(event) {

        var catalogId= event.target.value;
        this.props.ToggleCatalog(catalogId);
    }
 
    componentWillMount() {

        const { match: { params } } = this.props;
        this.lang = params.lang;
        this.productId = params.productId;

        this.page = params.page ? params.page : 1;
        this.searchKeywordQs = (queryString.parse(this.props.location.search).searchKeyword) ? `?searchKeyword=${queryString.parse(this.props.location.search).searchKeyword}` : '';
        this.searchKeyword = (queryString.parse(this.props.location.search).searchKeyword) ? queryString.parse(this.props.location.search).searchKeyword : '';
        this.newLink = !this.productId ? `/productcatalogs/${localizer.GetSelectedLang()}/new` : `/productcatalogs/${localizer.GetSelectedLang()}/new/product/${this.productId}`;

        //alert(this.typeId);

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: `/products/${localizer.GetSelectedLang()}`, text: localizer.get("Products") },
            { link: `/catalogs/${localizer.GetSelectedLang()}`, text: localizer.get("Catalogs") },
            { link: "", text: localizer.get("ProductCatalogs") }];

        this.setState({ breadCrumbNav: this.breadCrumbNav });

        var that = this;

        fetch(`/${localizer.GetSelectedLang()}/product/GetName/${this.productId}`).then(function (response) {
            return response.text().then(function (text) {
                that.breadCrumbNav.push({ link: "", text: text });
                that.setState({ breadCrumbNav: that.breadCrumbNav });
            });
        });


    }


    componentDidMount() {

        this.props.SearchProductCatalogs({ page: this.page, searchKeyword: this.searchKeyword, productId: this.productId });
        this.props.ResetFormResult();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps && nextProps.deleteResult && !nextProps.deleteResult.Status) {
            toast.error(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.deleteResult && nextProps.deleteResult.Status) {
            toast.success(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }

        //save result from prev from submit
        if (nextProps && nextProps.saveResult && !nextProps.saveResult.Status) {
            toast.error(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.saveResult && nextProps.saveResult.Status) {
            toast.success(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }
    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.state.breadCrumbNav}></BreadCrumb>

                <div id="MainListArea" className="ProductInCatalogs">

                    <div className="row">
                        <div id="ProductColors" className="col-sm-4">
                            <h4>{localizer.get("PRODUCTCOLORS")}</h4>
                            <div>
                                {this.props && this.props.ProductColors &&

                                    this.props.ProductColors.map((item, index) =>

                                        <div className={`PrdColorItem ${item.IsChecked ? "checked" : ""}`} onClick={() => this.togglePrd(item.ColorID)} key={index}>
                                            <div>{item.ColorName}</div>
                                            <img src={item.ImagePath} alt=""></img>
                                        </div>
                                    )

                                }
                            </div>
                        </div>

                        <div id="ProductCatalogs" className="col-sm-3">
                        <h4>{localizer.get("CATALOGS")}</h4>
                            <div>
                                {this.props && this.props.Catalogs &&

                                    this.props.Catalogs.map((item, index) =>
                                        <div className={`CatalogItem ${item.IsChecked ? "checked" : ""}`} key={index}>
                                            <div className="form-check">
                                                <input 
                                                checked={item.IsChecked}
                                                onChange={this.toggleCat} 
                                                value={item.ID} 
                                                type="checkbox" 
                                                className="form-check-input" 
                                                id={`check${index}`} ></input>
                                                <label className="form-check-label" htmlFor={`check${index}`}>{item.Name}</label>
                                            </div>
                                        </div>
                                    )

                                }</div>
                        </div>

                        <div id="AddToCatalogs" className="col-sm-1 text-center">
                            <h4 className="text-center">&#x2192;</h4>
                            <button onClick={this.addSelected.bind(this)} className="btn btn-primary" id="AddSelected">{localizer.get("Add")} &#x2192; </button>
                        </div>

                        <div id="AddedCatalogs" className="col-sm-4">
                            <h4>{localizer.get('ADDEDCATALOGS')} 
                            {this.props.AddedCatalogs && this.props.AddedCatalogs.length>0 && <button onClick={this.deleteFromAllCatalogs} className="btn btn-sm btn-danger">{localizer.get("Delete All")}</button>}
                            </h4>

                            <div>
                                {this.props && this.props.AddedCatalogs &&

                                    this.props.AddedCatalogs.map((item, index) =>
                                        <div className="AddedCatalogItem" key={index}>
                                           {item.CatalogName}<b> ({item.ColorName})</b>
                                           <button onClick={()=> this.deleteFromCatalog(item.ID)}  className="btn btn-sm btn-danger">{localizer.get("Delete")}</button>
                                        </div>
                                    )

                                }</div>

                        </div>

                    </div>

                </div>
            </div>
        );
    }

}

export default connect(
    state => state.productCatalogs,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(ProductCatalogs);