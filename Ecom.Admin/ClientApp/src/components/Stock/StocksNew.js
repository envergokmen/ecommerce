import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Stocks/StockStore';
import StockForm from './StockForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class StocksNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id, productId } = params;

        this.productId = productId;
        this.props.GetInitialValues(id ? id : 0, productId == undefined ? null: productId);
         
        this.breadCrumbNav =
        [{ link: ``, text: localizer.get("Settings") },
        { link: `/products/${localizer.GetSelectedLang()}/type`, text: localizer.get("Products")}, 
        { link: `/stocks/${localizer.GetSelectedLang()}/`, text: localizer.get("Stocks")},
        { link: "", text: localizer.getWithFormat("NewField", "$Stock") }];
    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        var productId = this.productId;

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <StockForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    productId={productId}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$PolicyTranslation")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></StockForm>
            </div>
        );
    }
}

export default connect(
    state => state.stocks,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(StocksNew);