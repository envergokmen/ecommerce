import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Stocks/StockStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';
//import CKEditor from '@ckeditor/ckeditor5-react';
//import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
    const errors = {}
    if (!values.Barkod) {
        errors.Barkod = localizer.getWithFormat("RequiredField", "$Barcode")
    }
 
    //if (values.ProductID == undefined || values.ProductID == null || values.ProductID == "" || values.ProductID == "-1") {
    //    errors.ProductID = localizer.getWithFormat("RequiredField", "$Product")
    //}

    return errors;

}

const warn = values => {
    const warnings = {}
    return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let StockForm = props => {

    const { handleSubmit, pristine, reset, submitting } = props;

    if (props && props.operationResult && props.operationResult.Status) {

        if (!props.productId) {
            props.push(`/stocks/${localizer.GetSelectedLang()}`);
        } else {
            props.push(`/stocks/${localizer.GetSelectedLang()}/product/${props.productId}`);
        }

    }

    const handleFile = function () {
    }
     
    const handleImageDelete = (e) => {
        e.preventDefault();
        props.DeleteImage(props.id);
    }

    const onFormSubmit = (e) => {

        e.preventDefault();

        var validateResult = handleSubmit(e);

        if (validateResult != null || validateResult != undefined) {
            return;
        }
        else {


            let formData = new FormData();
            formData.append('Barkod', e.target.elements.Barkod.value);
            formData.append('YapCode', e.target.elements.YapCode.value);
            formData.append('ColorID', e.target.elements.ColorID.value);
            formData.append('SizeID', e.target.elements.SizeID.value);
            formData.append('Quantity', e.target.elements.Quantity.value);
            formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
            formData.append('ProductID', e.target.elements.ProductID.value);
            formData.append('ID', e.target.elements.ID.value);

            props.SaveItem(formData);
        }

    }

    return (

        <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

            <div id="FormHeader">
                <h1>{props.header}</h1>
            </div>

            {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

            {(!props.operationResult || !props.operationResult.Status) &&
                <div>
                    <div className="row">
                        <div id="FormBody" className="col-sm-8">

                            <Field name="ID" type="hidden" component={FormHiddenField} />
                            <Field name="ProductID" type="hidden" component={FormHiddenField} />

                            <Field name="Barkod" type="text" component={FormTextField} label={localizer.get("Barkod")} />
                            <Field name="YapCode" type="text" component={FormTextField} label={localizer.get("YapCode")} />
                            <Field name="Quantity" type="number" component={FormTextField} label={localizer.get("Quantity")} />

                            {props.initialValues && props.initialValues.AllColors &&
                                <Field name="ColorID" label={localizer.get("Color")} type="select"
                                    component={FormSelectField}
                                    selectText={localizer.getWithFormat('PleaseSelectNewField', '$Color')}
                                    optionFnc={props.initialValues.AllColors.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }

                            {props.initialValues && props.initialValues.AllSizes &&
                                <Field name="SizeID" label={localizer.get("Size")} type="select"
                                    component={FormSelectField}
                                    selectText={localizer.getWithFormat('PleaseSelectNewField', '$Size')}
                                    optionFnc={props.initialValues.AllSizes.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }

                            <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />


                        </div>
                    </div>

                    <FormActions
                        submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
                    </FormActions>
                </div>
            }

        </form>
    )
}

var stockFormComp = connect(
    state => state.stockForms,
    dispatch => bindActionCreators(actionCreators, dispatch),
)(StockForm);

export default reduxForm({
    enableReinitialize: true,
    touched: false,
    form: 'EditStockForm', // a unique identifier for this form
    validate, // <--- validation function given to redux-form
    warn // <--- warning function given to redux-form
})(stockFormComp);
