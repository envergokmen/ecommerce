import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/EmailTemplateLocalLangs/EmailTemplateLocalLangStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$Name")
  }

  if (!values.LangID || values.LangID == undefined || values.LangID == null || values.LangID === ""|| values.LangID == "-1") {
    errors.LangID = localizer.getWithFormat("RequiredField", "$Language")
  }

  if (!values.EmailTemplateID || values.EmailTemplateID == undefined || values.EmailTemplateID == null || values.EmailTemplateID == "" || values.EmailTemplateID == "-1") {
    errors.EmailTemplateID = localizer.getWithFormat("RequiredField", "$EmailTemplate")
  }
  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let EmailTemplateLocalLangForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {

    if (!props.emailtemplateId) {
      props.push(`/emailtemplatelocallangs/${localizer.GetSelectedLang()}`);
    } else {
      props.push(`/emailtemplatelocallangs/${localizer.GetSelectedLang()}/translations/${props.emailtemplateId}`);
    }

  }

  //const fileInput = React.createRef();

  const handleFile = function () {
  }

  var EditorData=props && props.initialValues ? props.initialValues.Description : "";

  const handleEditorChange = (data) => {
    EditorData = data;
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {


      let formData = new FormData();
      
      formData.append('Name', e.target.elements.Name.value);
      formData.append('LangID', e.target.elements.LangID.value);
      formData.append('DescriptionHTML', EditorData);
      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('EmailTemplateID', e.target.elements.EmailTemplateID.value);
      formData.append('ID', e.target.elements.ID.value);
     
      props.SaveItem(formData);

    }
  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="Name" type="text" component={FormTextField} label={localizer.get("Name")} />
            
              <div className={"form-group row CKEdit"}>
                <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get('Content')}</label>
                <div className="col-sm-9">
                  <CKEditor
                    editor={ClassicEditor}
                    data={props && props.initialValues && props.initialValues.DetailHTML ? props.initialValues.DetailHTML : ""}
                    onInit={editor => {
                      // You can store the "editor" and use when it is needed.
                      console.log('Editor is ready to use!', editor);
                    }}
                    onChange={(event, editor) => {
                      const data = editor.getData();
                      handleEditorChange(data);
                      // console.log( { event, editor, data } );
                    }}
                    onBlur={editor => {
                      //console.log( 'Blur.', editor );
                    }}
                    onFocus={editor => {
                      //console.log( 'Focus.', editor );
                    }}
                  ></CKEditor>
                </div>
              </div>

              {props.initialValues && props.initialValues.AllLangs &&
                <Field name="LangID" label={localizer.get("Language")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Language')}
                  optionFnc={props.initialValues.AllLangs.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }

              {props.initialValues && props.initialValues.AllEmailTemplates &&
                <Field name="EmailTemplateID" label={localizer.get("EmailTemplate")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$EmailTemplate')}
                  optionFnc={props.initialValues.AllEmailTemplates.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }

              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />


            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var emailTemplateFormComp = connect(
  state => state.emailTemplateLocalLangForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(EmailTemplateLocalLangForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditEmailTemplateLocalLangForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(emailTemplateFormComp);
