import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/EmailTemplateLocalLangs/EmailTemplateLocalLangStore';
import EmailTemplateLocalLangForm from './EmailTemplateLocalLangForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class EmailTemplatesNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id, emailtemplateId } = params;

        this.emailtemplateId = emailtemplateId;
        this.props.GetInitialValues(id ? id : 0, emailtemplateId == undefined ? null: emailtemplateId);
         
        this.breadCrumbNav =
        [{ link: ``, text: localizer.get("Settings") },
        { link: `/emailtemplates/${localizer.GetSelectedLang()}/type`, text: localizer.get("EmailTemplates")}, 
        { link: `/emailtemplatelocallangs/${localizer.GetSelectedLang()}/`, text: localizer.get("EmailTemplateTranslations")},
        { link: "", text: localizer.getWithFormat("NewField", "$EmailTemplate") }];
    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        var emailtemplateId = this.emailtemplateId;

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <EmailTemplateLocalLangForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    emailtemplateId={emailtemplateId}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$EmailTemplateTranslation")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></EmailTemplateLocalLangForm>
            </div>
        );
    }
}

export default connect(
    state => state.emailTemplateLocalLangs,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(EmailTemplatesNew);