import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Banks/BankStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$Bank")
  }

  if (!values.SortNo && values.SortNo != "0") {
    errors.SortNo = localizer.getWithFormat("RequiredField", "$SortNo")
  }

    if (!values.BankCode || values.BankCode == "") {
        errors.BankCode = localizer.getWithFormat("RequiredField", "$BankCode")
    }

    if (!values.PaymentID || values.PaymentID == "0") {
        errors.PaymentID = localizer.getWithFormat("RequiredField", "$Payment")
    }

  // if (values.PStatus === undefined || values.PStatus === null || values.PStatus === "") {
  //   errors.PStatus = localizer.getWithFormat("RequiredField", "$Status")
  // }

  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let BankForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/banks/${localizer.GetSelectedLang()}`);
  }

  const fileInput = React.createRef();

  const handleFile = function () {
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);


    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {
      let formData = new FormData();
      formData.append('Name', e.target.elements.Name.value);
      formData.append('SortNo', e.target.elements.SortNo.value);
      formData.append('PStatus',  e.target.elements.PStatus.checked ? "1" : "0");
        formData.append('ID', e.target.elements.ID.value);

        formData.append('CorporateName', e.target.elements.CorporateName.value);
        formData.append('CityName', e.target.elements.CityName.value);
        formData.append('DepartmentAndNo', e.target.elements.DepartmentAndNo.value);
        formData.append('AccountNo', e.target.elements.AccountNo.value);
        formData.append('SortCode', e.target.elements.SortCode.value);
        formData.append('IBAN', e.target.elements.IBAN.value);
        formData.append('PaymentID', e.target.elements.PaymentID.value);
        formData.append('BankCode', e.target.elements.BankCode.value);

      

      formData.append('file', fileInput.current.files[0]);
      props.SaveItem(formData);
    }
  }

  return (
    <form onSubmit={onFormSubmit.bind(this)} className="FormStandard" method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}
      {props.deleteImgResult && <FormResultMessage operationResult={props.deleteImgResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="Name" type="text" component={FormTextField} label={localizer.get("Bank")} />
              <Field name="SortNo" type="number" component={FormTextField} label={localizer.get("SortNo")} />

                      <Field name="CorporateName" type="text" component={FormTextField} label={localizer.get("CorporateName")} />
                      <Field name="CityName" type="text" component={FormTextField} label={localizer.get("CityName")} />
                      <Field name="DepartmentAndNo" type="text" component={FormTextField} label={localizer.get("DepartmentAndNo")} />
                      <Field name="AccountNo" type="text" component={FormTextField} label={localizer.get("AccountNo")} />
                      <Field name="SortCode" type="text" component={FormTextField} label={localizer.get("SortCode")} />
                      <Field name="IBAN" type="text" component={FormTextField} label={localizer.get("IBAN")} />
                      <Field name="BankCode" type="text" component={FormTextField} label={localizer.get("BankCode")} />

                      {props.initialValues && props.initialValues.AllPayments &&
                          <Field name="PaymentID" label={localizer.get("Payment")} type="select"
                              component={FormSelectField}
                              selectText={localizer.getWithFormat('PleaseSelectNewField', '$Payment')}
                              optionFnc={props.initialValues.AllPayments.map(status => (
                                  <option value={status.Value} key={status.Value}>
                                      {status.Name}
                                  </option>
                              ))} />
                      }

              <div className="form-group row">
                <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get("Logo")}</label>
                <div className="col-sm-9">
                  <input name="LogoPath" onChange={handleFile} ref={fileInput} type="file" className="form-control" />
                </div>
              </div>


 
              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />



{/* 
              <Field name="PStatus" label={localizer.get("Status")} type="select"
                component={FormSelectField}
                selectText={localizer.getWithFormat('PleaseSelectNewField', '$Status')}
                optionFnc={statuses.map(status => (
                  <option value={status.value} key={status.value}>
                    {status.name}
                  </option>
                ))} >

              </Field> */}

                      {props.initialValues && props.initialValues.LogoPath && props.initialValues.LogoPath != "" &&
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">{localizer.get("Logo")}</label>
                  <div className="col-sm-9">
                    <img style={{ maxWidth: 100 }} src={props.initialValues.LogoPath} alt=""></img>
                    <div style={{ marginTop: 10 }}>
                      <a href="javascript:;" onClick={handleImageDelete} className="btn btn-danger btn-sm">{localizer.get("Delete")}</a>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var bankFormComp = connect(
  state => state.bankForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(BankForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditBankForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(bankFormComp);
