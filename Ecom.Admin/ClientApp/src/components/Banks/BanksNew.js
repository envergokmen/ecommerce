import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Banks/BankStore';
import BankForm from './BankForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class BanksNew extends Component {
    componentWillMount() {
 
        const { match: { params } } = this.props;
        const { lang , id} = params;
   
        this.props.GetInitialValues(id ? id : 0);

        this.breadCrumbNav = 
        [{ link: ``, text: localizer.get("Settings")},
           {link: `/banks/${lang}`, text: localizer.get("Banks")},
           {link: "", text: localizer.getWithFormat("NewField","$Bank")}]

    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <BankForm 
                SaveItem={this.props.SaveItem} 
                DeleteImage={this.props.DeleteImage}
                push={this.props.history.push} 
                deleteImgResult={this.props.deleteImgResult} 
                operationResult={this.props.operationResult} 
                FormResult={this.props.FormResult} 
                formSuccess={"test başarılı mesaji"} 
                formErrors={"test hata mesaji"} 
                header={localizer.getWithFormat("NewField", "$Bank")}
                  id={this.props.initial ? this.props.initial.ID : 0}  
                  initialValues={this.props.initial} onSubmit={this.submit}></BankForm>
            </div>
        );
    }
}

export default connect(
    state => state.banks,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(BanksNew);