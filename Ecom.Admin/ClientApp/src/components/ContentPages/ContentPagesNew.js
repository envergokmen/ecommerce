import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/ContentPages/ContentPageStore';
import ContentPageForm from './ContentPageForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class ContentPagesNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id } = params;

        if (id) {
            this.props.GetInitialValues(id);
        } else {
            this.props.GetInitialValues(0);
        }

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: `/contentpages/${lang}`, text: localizer.get("ContentPages") },
            { link: "", text: localizer.getWithFormat("NewField", "$ContentPage") }]

    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <ContentPageForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$ContentPage")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></ContentPageForm>
            </div>
        );
    }
}

export default connect(
    state => state.contentPages,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(ContentPagesNew);