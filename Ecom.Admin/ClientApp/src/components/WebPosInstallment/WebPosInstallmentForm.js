import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/WebPosInstallments/WebPosInstallmentStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions, FormCheckBoxField } from '../Forms/FormFields';
//import CKEditor from '@ckeditor/ckeditor5-react';
//import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
    const errors = {}
    if (!values.Barkod) {
        errors.Barkod = localizer.getWithFormat("RequiredField", "$Barcode")
    }

    //if (values.WebPosID == undefined || values.WebPosID == null || values.WebPosID == "" || values.WebPosID == "-1") {
    //    errors.WebPosID = localizer.getWithFormat("RequiredField", "$WebPos")
    //}

    return errors;

}

const warn = values => {
    const warnings = {}
    return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let WebPosInstallmentForm = props => {

    const { handleSubmit, pristine, reset, submitting } = props;

    if (props && props.operationResult && props.operationResult.Status) {

        if (!props.webposId) {
            props.push(`/webposinstallments/${localizer.GetSelectedLang()}`);
        } else {
            props.push(`/webposinstallments/${localizer.GetSelectedLang()}/webpos/${props.webposId}`);
        }

    }

    const handleFile = function () {
    }

    const handleImageDelete = (e) => {
        e.preventDefault();
        props.DeleteImage(props.id);
    }

    const onFormSubmit = (e) => {

        e.preventDefault();

        var validateResult = handleSubmit(e);

        if (validateResult != null || validateResult != undefined) {
            return;
        }
        else {


            let formData = new FormData();

            formData.append('Quantity', e.target.elements.Quantity.value);
            formData.append('Rate', e.target.elements.Rate.value);


            formData.append('Title', e.target.elements.Title.value);

            //formData.append('DelayedMouth', e.target.elements.DelayedMouth.value);
            formData.append('DownLimit', e.target.elements.DownLimit.value);
            formData.append('UpLimit', e.target.elements.UpLimit.value);
            formData.append('ExtraAmount', e.target.elements.ExtraAmount.value);

            formData.append('IsDefault', e.target.elements.IsDefault.value);
            //formData.append('PlusInstallmentLimit', e.target.elements.PlusInstallmentLimit.value);
            //formData.append('PlusIntallmentAmount', e.target.elements.PlusIntallmentAmount.value);

            formData.append('SortNo', e.target.elements.SortNo.value);

            formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
            formData.append('WebPosID', e.target.elements.WebPosID.value);
            formData.append('ID', e.target.elements.ID.value);

            props.SaveItem(formData);
        }

    }

    return (

        <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

            <div id="FormHeader">
                <h1>{props.header}</h1>
            </div>

            {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

            {(!props.operationResult || !props.operationResult.Status) &&
                <div>
                    <div className="row">
                        <div id="FormBody" className="col-sm-8">

                            <Field name="ID" type="hidden" component={FormHiddenField} />

                            <Field name="Quantity" type="text" component={FormTextField} label={localizer.get("Quantity")} />
                            <Field name="Rate" type="text" component={FormTextField} label={localizer.get("CommissionRate")} />
                            <Field name="SortNo" type="number" component={FormTextField} label={localizer.get("SortNo")} />
                            <Field name="Title" type="text" component={FormTextField} label={localizer.get("Title")} />

                            <Field name="UpLimit" type="number" component={FormTextField} label={localizer.get("UpLimit")} />
                            <Field name="DownLimit" type="number" component={FormTextField} label={localizer.get("DownLimit")} />
                            <Field name="ExtraAmount" type="number" component={FormTextField} label={localizer.get("ExtraAmount")} />

                            {props.initialValues && props.initialValues.AllWebPoses &&
                                <Field name="WebPosID" label={localizer.get("PaymentProvider")} type="select"
                                    component={FormSelectField}
                                    selectText={localizer.getWithFormat('PleaseSelectNewField', '$PaymentProvider')}
                                    optionFnc={props.initialValues.AllWebPoses.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }


                        <Field name="IsDefault" id="IsDefault" type="checkbox" component={FormCheckBoxField} label={localizer.get("IsDefault")} />
                            <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />


                        </div>
                    </div>

                    <FormActions
                        submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
                    </FormActions>
                </div>
            }

        </form>
    )
}

var webPosInstallmentFormComp = connect(
    state => state.webPosInstallmentForms,
    dispatch => bindActionCreators(actionCreators, dispatch),
)(WebPosInstallmentForm);

export default reduxForm({
    enableReinitialize: true,
    touched: false,
    form: 'EditWebPosInstallmentForm', // a unique identifier for this form
    validate, // <--- validation function given to redux-form
    warn // <--- warning function given to redux-form
})(webPosInstallmentFormComp);
