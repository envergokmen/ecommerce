import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/WebPosInstallments/WebPosInstallmentStore';
import { Link } from "react-router-dom";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';
import { Localize, Localizer } from "../../Localizer/Localizer";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {default as queryString } from 'qs';
import { Router } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class WebPosInstallments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            breadCrumbNav: []
        };

        this.toggleModal = this.toggleModal.bind(this);
        this.NextPage = this.NextPage.bind(this);
        this.delete = this.delete.bind(this);
    }


    NextPage(event) {

        event.preventDefault();
        event.stopPropagation();
        this.props.SearchCargowebposs({ append: true, page: this.props.Page + 1, searchKeyword: this.props.searchKeyword });
        this.props.history.push(`/webposs/${localizer.GetSelectedLang()}/${this.props.Page + 1}${this.searchKeyword}`);
    }

    delete(event) {

        event.preventDefault();
        event.stopPropagation();
        this.props.DeleteItem(this.state.IdToDelete);
        this.toggleModal(event);
    }

    toggleModal(event = null) {
        event.preventDefault();
        event.stopPropagation();

        const { id, name } = event.target;

        this.setState(prevState => ({
            modal: !prevState.modal,
            IdToDelete: id,
            name: name
        }));

    }
    componentWillMount() {

        const { match: { params } } = this.props;
        this.lang = params.lang;
        this.webposId = params.webposId;

        this.page = params.page ? params.page : 1;
        this.searchKeywordQs = (queryString.parse(this.props.location.search).searchKeyword) ? `?searchKeyword=${queryString.parse(this.props.location.search).searchKeyword}` : '';
        this.searchKeyword = (queryString.parse(this.props.location.search).searchKeyword) ? queryString.parse(this.props.location.search).searchKeyword : '';
        this.newLink = !this.webposId ? `/webposinstallments/${localizer.GetSelectedLang()}/new` : `/webposinstallments/${localizer.GetSelectedLang()}/new/webpos/${this.webposId}`;

        //alert(this.typeId);

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: `/webposes/${localizer.GetSelectedLang()}`, text: localizer.get("WebPoses") },
            { link: "", text: localizer.get("WebPosInstallments") }];

        this.setState({ breadCrumbNav: this.breadCrumbNav });

        var that = this;

        fetch(`/${localizer.GetSelectedLang()}/webpos/GetName/${this.webposId}`).then(function (response) {
            return response.text().then(function (text) {
                that.breadCrumbNav.push({ link: "", text: text });
                that.setState({ breadCrumbNav: that.breadCrumbNav });
            });
        });


    }


    componentDidMount() {

        this.props.SearchWebPosInstallments({ page: this.page, searchKeyword: this.searchKeyword, webposId: this.webposId });
        this.props.ResetFormResult();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps && nextProps.deleteResult && !nextProps.deleteResult.Status) {
            toast.error(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.deleteResult && nextProps.deleteResult.Status) {
            toast.success(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }

        //save result from prev from submit
        if (nextProps && nextProps.saveResult && !nextProps.saveResult.Status) {
            toast.error(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.saveResult && nextProps.saveResult.Status) {
            toast.success(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }
    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.state.breadCrumbNav}></BreadCrumb>


                <div id="MainListArea">

                    <Link to={this.newLink} className="btn btn-primary">{localizer.getWithFormat("NewField", "$WebPosInstallment")}</Link>

                    {(!this.props.webposs || this.props.webposs.length == 0) &&
                        <div className="NoListItem">
                            <p>{localizer.getWithFormat("NoListItem", "$WebPosInstallment")}</p>
                            <Link to={this.newLink} className="btn btn-primary">{localizer.getWithFormat("NewField", "$WebPosInstallment")}</Link>
                        </div>
                    }

                    {this.props.webposs &&
                        <table className="table table-striped ListingTable">

                            <thead className="thead-light">
                                <tr>
                                    <th>ID</th>
                                    <th className="percent20">{localizer.get('Quantity')}</th>
                                    <th className="percent15">{localizer.get('Comission')}</th>
                                    <th className="percent15">{localizer.get('IsDefault')}</th>
                                    <th>{localizer.get('Status')}</th>
                                    <th className="Operations">{localizer.get('Operations')}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.webposs.map((item, index) =>
                                    <tr key={item.ID} className={item.PStatus == 1 ? '' : 'text-muted'}>
                                        <td className="Id">{item.ID}</td>
                                        <td>{item.Quantity}</td>
                                        <td>{item.Rate}</td>
                                        <td>{item.IsDefault ? localizer.get('Yes') : localizer.get('No')}</td>

                                        <td className="PStatus">
                                            <span className={item.PStatus == "1" ? "Status Active" : "Status Passive"}></span>
                                            <span className="StatusText">
                                                {item.PStatus == "1" && localizer.get("Active")}
                                                {item.PStatus != "1" && localizer.get("Passive")}
                                            </span>

                                        </td>
                                        <td className="Operations">

                                            <Link to={`/webposinstallments/${localizer.GetSelectedLang()}/edit/${item.ID}/webpos/${item.WebPosID}`} className="btn btn-sm btn-warning"><Localize localizer={localizer}>Edit</Localize></Link>
                                            <button id={item.ID} name={item.Name} onClick={(event) => this.toggleModal(event)} className="btn btn-sm btn-danger"><Localize localizer={localizer}>Delete</Localize></button>

                                        </td>
                                    </tr>
                                )}


                            </tbody>

                        </table>
                    }

                    {this.props.webposs &&
                        this.props.TotalPage > this.props.Page &&
                        <div className="text-center">
                            <button onClick={this.NextPage} className="btn btn-success LoadMore">Load More</button>
                        </div>
                    }

                    <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={this.props.className}>
                        <ModalHeader toggle={this.toggleModal}>
                            {localizer.get("ConfirmDelete")}
                        </ModalHeader>
                        <ModalBody>
                            <b>{this.state.name}</b> {' '}
                            {localizer.get('WillBeDeleted')}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={this.delete}>{localizer.get('Confirm')}</Button>{' '}
                            <Button color="secondary" onClick={this.toggleModal}>{localizer.get('Cancel')}</Button>
                        </ModalFooter>
                    </Modal>

                    <ToastContainer />
                </div>
            </div >
        );
    }

}

export default connect(
    state => state.webPosInstallments,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(WebPosInstallments);