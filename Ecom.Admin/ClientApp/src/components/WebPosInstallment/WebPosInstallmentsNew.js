import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/WebPosInstallments/WebPosInstallmentStore';
import WebPosInstallmentForm from './WebPosInstallmentForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class WebPosInstallmentsNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id, webposId } = params;

        this.webposId = webposId;
        this.props.GetInitialValues(id ? id : 0, webposId == undefined ? null: webposId);
         
        this.breadCrumbNav =
        [{ link: ``, text: localizer.get("Settings") },
        { link: `/webposs/${localizer.GetSelectedLang()}/type`, text: localizer.get("WebPoses")}, 
        { link: `/webposinstallments/${localizer.GetSelectedLang()}/`, text: localizer.get("WebPosInstallments")},
        { link: "", text: localizer.getWithFormat("NewField", "$WebPosInstallment") }];
    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        var webposId = this.webposId;

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <WebPosInstallmentForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    webposId={webposId}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$PolicyTranslation")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></WebPosInstallmentForm>
            </div>
        );
    }
}

export default connect(
    state => state.webPosInstallments,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(WebPosInstallmentsNew);