import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Domains/DomainStore';
import DomainForm from './DomainForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class DomainsNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id } = params;

        if (id) {
            this.props.GetInitialValues(id);
        } else {
            this.props.GetInitialValues(0);
        }

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: `/domains/${lang}`, text: localizer.get("Domains") },
            { link: "", text: localizer.getWithFormat("NewField", "$Domain") }]

    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <DomainForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$Domain")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></DomainForm>
            </div>
        );
    }
}

export default connect(
    state => state.domains,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(DomainsNew);