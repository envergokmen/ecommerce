import React, { useState } from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Products/ProductStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);


const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$Product")
  }

  if (values.ProductName === undefined || values.ProductName === null || values.ProductName === "") {
    errors.ProductName = localizer.getWithFormat("RequiredField", "$ProductName")
  }

  if (values.ProductCode === undefined || values.ProductCode === null || values.ProductCode === "") {
    errors.ProductCode = localizer.getWithFormat("RequiredField", "$ProductCode")
  }

  // if (values.PriceCurrencyID === undefined || values.PriceCurrencyID === null || values.PriceCurrencyID === "") {
  //   errors.PriceCurrencyID = localizer.getWithFormat("RequiredField", "$Currency")
  // }

  // if (values.LangID === undefined || values.LangID === null || values.LangID === "") {
  //   errors.LangID = localizer.getWithFormat("RequiredField", "$Language")
  // }

  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let ProductForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;
  var [modal, ImageIdToEdit, name] = useState(true, 0, "");
  //this.modal=modal;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/products/${localizer.GetSelectedLang()}`);
  }

  const fileInput = React.createRef();

  const handleFile = function () {
  }

 
  const handleImageDelete = (e) => {
    e.preventDefault();
    var imageId = Number(e.target.attributes.getNamedItem('id').value);
    props.DeleteImage(imageId);
  }

  const handleImageDeleteAll = (e) => {
    e.preventDefault();
    props.DeleteAllImages(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {


      let formData = new FormData();
      formData.append('ProductName', e.target.elements.ProductName.value);
      formData.append('SmallName', e.target.elements.SmallName.value);
      formData.append('ProductCode', e.target.elements.ProductCode.value);
      formData.append('BrandID', e.target.elements.BrandID.value);
      formData.append('SeasonID', e.target.elements.SeasonID.value);
      formData.append('ProductDepartmentID', e.target.elements.ProductDepartmentID.value);
      formData.append('ProductStyleID', e.target.elements.ProductStyleID.value);
      formData.append('Gender', e.target.elements.Gender.value);
      formData.append('ReturnPolicyID', e.target.elements.ReturnPolicyID.value);
      formData.append('CargoPolicyID', e.target.elements.CargoPolicyID.value);

      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('ID', e.target.elements.ID.value);

      //formData.append(`files`, fileInput.current.files);

      if (fileInput.current.files) {

        for (let i = 0; i < fileInput.current.files.length; i++) {
          formData.append(`files`, fileInput.current.files[i]);
        }
      }

      props.SaveItem(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="ProductName" type="text" component={FormTextField} label={localizer.get("ProductName")} />
              <Field name="SmallName" type="text" component={FormTextField} label={localizer.get("SmallName")} />
              <Field name="ProductCode" type="text" component={FormTextField} label={localizer.get("ProductCode")} />


              {props.initialValues && props.initialValues.AllBrands &&
                <Field name="BrandID" label={localizer.get("Brand")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Brand')}
                  optionFnc={props.initialValues.AllBrands.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


              {props.initialValues && props.initialValues.AllProductDepartments &&
                <Field name="ProductDepartmentID" label={localizer.get("ProductDepartment")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Department')}
                  optionFnc={props.initialValues.AllProductDepartments.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


              {props.initialValues && props.initialValues.AllProductStyles &&
                <Field name="ProductStyleID" label={localizer.get("ProductStyle")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Style')}
                  optionFnc={props.initialValues.AllProductStyles.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


              {props.initialValues && props.initialValues.AllSeasons &&
                <Field name="SeasonID" label={localizer.get("Season")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Season')}
                  optionFnc={props.initialValues.AllSeasons.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


              {props.initialValues && props.initialValues.AllReturnPolicies &&
                <Field name="ReturnPolicyID" label={localizer.get("ReturnPolicy")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$ReturnPolicy')}
                  optionFnc={props.initialValues.AllReturnPolicies.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


              {props.initialValues && props.initialValues.AllCargoPolicies &&
                <Field name="CargoPolicyID" label={localizer.get("CargoPolicy")} type="select"
                  component={FormSelectField} selectText={localizer.getWithFormat('PleaseSelectNewField', '$CargoPolicyID')}
                  optionFnc={props.initialValues.AllCargoPolicies.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


              {props.initialValues && props.initialValues.AllGenders &&
                <Field name="Gender" label={localizer.get("Gender")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Gender')}
                  optionFnc={props.initialValues.AllGenders.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }
              <input type="hidden" value={props.date}></input>
              {props.initialValues && props.initialValues.Images && props.initialValues.Images.length > 0 &&
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">{localizer.get("Images")}</label>
                  <div className="col-sm-9">
                    <div className="clearfix">
                      {props.initialValues.Images.map(image => (
                        <div className="PrdImgContainer" key={image.ID}>
                          <img style={{ maxWidth: 100 }} src={image.ImagePath} alt=""></img>
                          {localizer.get('Angle')} : {image.Angle} <br />
                          {localizer.get('Color')}: {image.ColorName} <br />
                          {localizer.get('SortNo')}: {image.SortNo}
                          <div className="ImageActions">
                            <a href="javascript:;" id={image.ID} onClick={handleImageDelete} className="btn btn-danger btn-sm">{localizer.get("Delete")}</a>
                            <a href="javascript:;" id={image.ID} onClick={(event) => props.toggleModal(event, image)} className="btn btn-warning btn-sm">{localizer.get("Edit")}</a>
                          </div>
                        </div>
                      ))}
                    </div>
                    <div className="clearfix">
                      <a href="javascript:;" onClick={handleImageDeleteAll} className="btn btn-danger">{localizer.get("DeleteAll")}</a>
                    </div>
                  </div>
                </div>
              }


              <div className="form-group row">
                <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get("ImagePath")}</label>
                <div className="col-sm-9">
                  <div className="alert alert-warning">
                  {localizer.get('PleaseUseImageFormat')} : <br /><br />
                  
                    {/* <b>PRODUCTCODE</b>_<b>GLOBALCOLOR</b>_<b>SUBCOLOR</b>_<b>ANGLE</b>.jpg
                     <br/>  <br/>
                    example 1: <b>PRD892457</b>_<b>RED</b>_<b>ROSE</b>_<b>1</b>.jpg  <br/>
                    example 2: <b>PRD892478</b>_<b>BLUE</b>_<b>SKYBLUE</b>_<b>2</b>.jpg
  */}
                    <b>{localizer.get('GLOBALCOLOR')}</b>_<b>{localizer.get('SUBCOLOR')}</b>_<b>{localizer.get('ANGLE')}</b>.jpg
                    <br />  <br />
                    {localizer.get('Example')} 1: <b>RED</b>_<b>ROSE</b>_<b>1</b>.jpg  <br />
                    {localizer.get('Example')} 2: <b>BLUE</b>_<b>SKYBLUE</b>_<b>2</b>.jpg

                  </div>
                  <input name="ImagePath" onChange={handleFile} multiple ref={fileInput} type="file" className="form-control" />
                </div>
              </div>




              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>

 

        </div>
      }

    </form>
  )
}

var productFormComp = connect(
  state => state.productForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(ProductForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditProductForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(productFormComp);
