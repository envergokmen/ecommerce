import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Products/ProductStore';
import ProductUploadForm from './ProductUploadForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class ProductsUpload extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggleModal = this.toggleModal.bind(this);
        this.saveImage = this.saveImage.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        // this.NextPage = this.NextPage.bind(this);
        // this.delete = this.delete.bind(this);
    }

    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id } = params;

        if (id) {
            this.props.GetInitialValues(id);
        } else {
            this.props.GetInitialValues(0);
        }

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: `/products/${lang}`, text: localizer.get("Products") },
            { link: "", text: localizer.getWithFormat("NewField", "$Product") }]

    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    toggleModal(event = null, item = null) {
        event.preventDefault();
        event.stopPropagation();

        const { id, name } = event.target;
        if (item != null) {

            this.setState(prevState => ({
                modal: !prevState.modal,
                IdToDelete: id,
                name: name,
                item: item,
                ImageID: item.ID,
                Angle: item.Angle,
                ColorID: item.ColorID ? item.ColorID : "",
                SortNo: item.SortNo
            }));

        } else {

            this.setState(prevState => ({
                modal: !prevState.modal,
                IdToDelete: 0,

            }));

        }

    }

    saveImage(event) {

        event.preventDefault();
        event.stopPropagation();

        this.props.SaveImage({ Id: this.state.ImageID, Angle: this.state.Angle, ColorID: this.state.ColorID, SortNo: this.state.SortNo })
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

        if(nextProps.modal!=undefined)
        {
            this.setState({modal:nextProps.modal});
        }
    }
    submit = values => {

    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <ProductUploadForm
                    Upload={this.props.Upload}
                    push={this.props.history.push}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$Product")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></ProductUploadForm>
            </div>
        );
    }
}

export default connect(
    state => state.products,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(ProductsUpload);