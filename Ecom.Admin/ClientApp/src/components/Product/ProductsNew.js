import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Products/ProductStore';
import ProductForm from './ProductForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class ProductsNew extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggleModal = this.toggleModal.bind(this);
        this.saveImage = this.saveImage.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        // this.NextPage = this.NextPage.bind(this);
        // this.delete = this.delete.bind(this);
    }

    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id } = params;

        if (id) {
            this.props.GetInitialValues(id);
        } else {
            this.props.GetInitialValues(0);
        }

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: `/products/${lang}`, text: localizer.get("Products") },
            { link: "", text: localizer.getWithFormat("NewField", "$Product") }]

    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    toggleModal(event = null, item = null) {
        event.preventDefault();
        event.stopPropagation();

        const { id, name } = event.target;
        if (item != null) {

            this.setState(prevState => ({
                modal: !prevState.modal,
                IdToDelete: id,
                name: name,
                item: item,
                ImageID: item.ID,
                Angle: item.Angle,
                ColorID: item.ColorID ? item.ColorID : "",
                SortNo: item.SortNo
            }));

        } else {

            this.setState(prevState => ({
                modal: !prevState.modal,
                IdToDelete: 0,

            }));

        }

    }

    saveImage(event) {

        event.preventDefault();
        event.stopPropagation();

        this.props.SaveImage({ Id: this.state.ImageID, Angle: this.state.Angle, ColorID: this.state.ColorID, SortNo: this.state.SortNo })
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

        if(nextProps.modal!=undefined)
        {
            this.setState({modal:nextProps.modal});
        }
    }
    submit = values => {

    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <ProductForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    toggleModal={this.toggleModal}
                    header={localizer.getWithFormat("NewField", "$Product")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    date={this.props.date}
                    initialValues={this.props.initial} onSubmit={this.submit}></ProductForm>

                {this.props.initial && this.props.initial.AllColors &&
                
                    <Modal isOpen={this.state.modal} toggle={this.toggleModal} className="ImageEditModal">
                     <form onSubmit={this.saveImage} action="/" method="post">
                        <ModalHeader toggle={this.toggleModal}>
                            {localizer.get("SaveImage")}
                        </ModalHeader>
                        <ModalBody>
                            {this.state.item &&
                                <div>
                                    <div className="text-center">
                                        <img src={this.state.item.ImagePath} style={{ maxWidth: 100 + "%" }} alt=""></img>

                                    </div>

                                    <div className="row">
                                        <div className="col-sm-3">
                                            
                                            {localizer.get('Angle')}
                                     </div>
                                        <div className="col-sm-9">
                                            <input onChange={this.handleInputChange} name="Angle" className="form-control" value={this.state.Angle} type="number" placeholder={localizer.get("Angle")} />
                                        </div>

                                    </div>

                                    <div className="row">
                                        <div className="col-sm-3">
                                        
                                        {localizer.get('SortNo')}
                                            
                                     </div>

                                        <div className="col-sm-9">
                                            <input onChange={this.handleInputChange} name="SortNo" className="form-control" value={this.state.SortNo} type="number" placeholder={localizer.get("SortNo")} />
                                        </div>

                                    </div>

                                    <div className="row">
                                        <div className="col-sm-3">

                                        {localizer.get('Color')}
                                            
                                     </div>

                                        <div className="col-sm-9">

                                        <select name="ColorID" className="form-control" value={this.state.ColorID} onChange={this.handleInputChange} >
                                            <option>{localizer.getWithFormat('PleaseSelectNewField', '$Color')}</option>
                                                {this.props.initial.AllColors.map((e, key) => {
                                                    return <option key={key} value={e.Value}>{e.Name}</option>;
                                                })}
                                            </select>

                                        </div>

                                    </div>



                                </div>
                            }
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" type="submit" onClick={this.saveImage}>{localizer.get('Save')}</Button>{' '}
                            <Button color="secondary" onClick={this.toggleModal}>{localizer.get('Cancel')}</Button>
                        </ModalFooter>
                        </form>
                    </Modal>
             
                }

            </div>
        );
    }
}

export default connect(
    state => state.products,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(ProductsNew);