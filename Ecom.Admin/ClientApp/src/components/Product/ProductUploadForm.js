import React, { useState } from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Products/ProductStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);


const validate = values => {
  const errors = {}
  return errors;
}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let ProductUploadForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;
  var [modal, ImageIdToEdit, name] = useState(true, 0, "");
   
  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/products/${localizer.GetSelectedLang()}`);
  }

  const fileInput = React.createRef();

  const handleFile = function () {
  }


  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {


      let formData = new FormData();

      if (fileInput.current.files) {

        for (let i = 0; i < fileInput.current.files.length; i++) {
          formData.append(`files`, fileInput.current.files[i]);
        }
      }

      props.Upload(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">
      
              <div className="form-group row">
                <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get("FilePath")}</label>
                <div className="col-sm-9">
                  <input name="UploadFile" onChange={handleFile} multiple ref={fileInput} type="file" className="form-control" />
                </div>
              </div>

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>

        </div>
      }

    </form>
  )
}

var productUploadFormComp = connect(
  state => state.productForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(ProductUploadForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditProductForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(ProductUploadForm);
