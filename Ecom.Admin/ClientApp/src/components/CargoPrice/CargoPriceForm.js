import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/CargoPrices/CargoPriceStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  
  if (values.Amount === undefined || values.Amount === null || values.Amount === "") {
    errors.Amount = localizer.getWithFormat("RequiredField", "$Amount")
  }

  if (!values.PriceCurrencyID || values.PriceCurrencyID === undefined || values.PriceCurrencyID === null || values.PriceCurrencyID === "") {
    errors.PriceCurrencyID = localizer.getWithFormat("RequiredField", "$Currency")
  }

  if (!values.CargoID || values.CargoID === undefined || values.CargoID === null || values.CargoID === "") {
    errors.CargoID = localizer.getWithFormat("RequiredField", "$Cargo")
  }

  if (!values.CountryID || values.CountryID === undefined || values.CountryID === null || values.CountryID === "") {
    errors.CountryID = localizer.getWithFormat("RequiredField", "$Country")
  }

  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let CargoPriceForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/cargoprices/${localizer.GetSelectedLang()}`);
  }

  //const fileInput = React.createRef();

  const handleFile = function () {
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {

      
      let formData = new FormData();
        formData.append('Amount', e.target.elements.Amount.value);
        formData.append('FreeShippingLimit', e.target.FreeShippingLimit.value);
        formData.append('CargoID', e.target.CargoID.value);
        formData.append('DomainID', e.target.DomainID.value);
        formData.append('PriceCurrencyID', e.target.PriceCurrencyID.value);
        formData.append('CountryID', e.target.CountryID.value); 

      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('ID', e.target.elements.ID.value);
         
      props.SaveItem(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

                      <Field name="ID" type="hidden" component={FormHiddenField} />

                      <Field name="Amount" type="number" step="any" component={FormTextField} label={localizer.get("Amount")} />
                      <Field name="FreeShippingLimit" type="number" component={FormTextField} label={localizer.get("FreeShippingLimit")} />

                      {props.initialValues && props.initialValues.AllCountries &&
                          <Field name="CountryID" label={localizer.get("Country")} type="select"
                              component={FormSelectField}
                              selectText={localizer.getWithFormat('PleaseSelectNewField', '$Country')}
                          optionFnc={props.initialValues.AllCountries.map(status => (
                                  <option value={status.Value} key={status.Value}>
                                      {status.Name}
                                  </option>
                              ))} />
                      }


                      {props.initialValues && props.initialValues.AllDomains &&
                          <Field name="DomainID" label={localizer.get("Domain")} type="select"
                              component={FormSelectField}
                              selectText={localizer.getWithFormat('PleaseSelectNewField', '$Domain')}
                          optionFnc={props.initialValues.AllDomains.map(status => (
                                  <option value={status.Value} key={status.Value}>
                                      {status.Name}
                                  </option>
                              ))} />
                      }

                      {props.initialValues && props.initialValues.AllCargoes &&
                          <Field name="CargoID" label={localizer.get("Cargo")} type="select"
                              component={FormSelectField}
                              selectText={localizer.getWithFormat('PleaseSelectNewField', '$Cargo')}
                          optionFnc={props.initialValues.AllCargoes.map(status => (
                                  <option value={status.Value} key={status.Value}>
                                      {status.Name}
                                  </option>
                              ))} />
                      }

                      {props.initialValues && props.initialValues.AllCurrencies &&
                          <Field name="PriceCurrencyID" label={localizer.get("Currency")} type="select"
                              component={FormSelectField}
                              selectText={localizer.getWithFormat('PleaseSelectNewField', '$Currency')}
                              optionFnc={props.initialValues.AllCurrencies.map(status => (
                                  <option value={status.Value} key={status.Value}>
                                      {status.Name}
                                  </option>
                              ))} />
                      }


              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var cargopriceFormComp = connect(
  state => state.cargoPriceForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(CargoPriceForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditCargoPriceForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(cargopriceFormComp);
