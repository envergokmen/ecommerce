import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Coupons/CouponStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormCheckBoxField, FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
    const errors = {}



    if (!values.CouponCode || values.CouponCode == "") {
        errors.Name = localizer.getWithFormat("RequiredField", "$CouponCode")
    }

    if (values.CalculationType === undefined || values.CalculationType === null || values.CalculationType === "") {
        errors.CalculationType = localizer.getWithFormat("RequiredField", "$CalculationType");
    }


    if (values.CampaignType === undefined || values.CampaignType === null || values.CampaignType === "") {
        errors.CampaignType = localizer.getWithFormat("RequiredField", "$CampaignType");
    }

    if (values.CalculationType === "0" && (!values.PriceCurrencyID || values.PriceCurrencyID == "")) {
        errors.PriceCurrencyID = localizer.getWithFormat("RequiredField", "$Currency");
    }


    if (values.Amount === undefined || values.Amount === null || values.Amount === "0") {
        errors.Amount = localizer.getWithFormat("RequiredField", "$Amount");
    }

    if (!values.AmountLimit === undefined || values.AmountLimit === null || values.AmountLimit === "") {
        errors.Amount = localizer.getWithFormat("RequiredField", "$AmountLimit");
    }


    return errors;

}

const warn = values => {
    const warnings = {}
    return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let CouponForm = props => {

    const { handleSubmit, pristine, reset, submitting } = props;

    if (props && props.operationResult && props.operationResult.Status) {
        props.push(`/coupons/${localizer.GetSelectedLang()}`);
    }

    //const fileInput = React.createRef();

    const handleFile = function () {
    }

    const handleImageDelete = (e) => {
        e.preventDefault();
        props.DeleteImage(props.id);
    }

    const onFormSubmit = (e) => {

        e.preventDefault();

        var validateResult = handleSubmit(e);

        if (validateResult != null || validateResult != undefined) {
            return;
        }
        else {



            let formData = new FormData();

            formData.append('CouponCode', e.target.elements.CouponCode.value);
            formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
            formData.append('ID', e.target.elements.ID.value);
            formData.append('BeginDate', e.target.elements.BeginDate.value);
            formData.append('EndDate', e.target.elements.EndDate.value);
            formData.append('CalculationType', e.target.elements.CalculationType.value);
            formData.append('CampaignType', e.target.elements.CampaignType.value);
            formData.append('AllowOnDiscountedProducts', e.target.elements.AllowOnDiscountedProducts.checked ? "true" : "false");
            formData.append('AllowMultipleUse', e.target.elements.AllowMultipleUse.checked ? "true" : "false");
            formData.append('Amount', e.target.elements.Amount.value);
            formData.append('AmountLimit', e.target.elements.AmountLimit.value);
            formData.append('MaxAmountLimit', e.target.elements.MaxAmountLimit.value);
            formData.append('DiscountLimit', e.target.elements.DiscountLimit.value);
            formData.append('DomainId', e.target.elements.DomainId.value);
            formData.append('PriceCurrencyID', e.target.elements.PriceCurrencyID.value);
            formData.append('CampaignID', e.target.elements.CampaignID.value);
            formData.append('CreationLimit', e.target.elements.CreationLimit.value);

            props.SaveItem(formData);
        }

    }

    return (

        <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

            <div id="FormHeader">
                <h1>{props.header}</h1>
            </div>

            {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

            {(!props.operationResult || !props.operationResult.Status) &&
                <div>
                    <div className="row">
                        <div id="FormBody" className="col-sm-8">


                            <Field name="ID" type="hidden" component={FormHiddenField} />
                       

                            <Field name="CouponCode" type="text" component={FormTextField} label={localizer.get("CouponCode")} />

                            {props.initialValues && props.initialValues.AllCampaignTypes &&
                                <Field name="CampaignType" label={localizer.get("CampaignType")} type="select"
                                    component={FormSelectField}
                                    selectText={localizer.getWithFormat('PleaseSelectNewField', '$CampaignType')}
                                    optionFnc={props.initialValues.AllCampaignTypes.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }


                            {props.initialValues && props.initialValues.AllCalculationTypes &&
                                <Field name="CalculationType" label={localizer.get("CalculationType")} type="select"
                                    component={FormSelectField}
                                    selectText={localizer.getWithFormat('PleaseSelectNewField', '$CalculationType')}
                                    optionFnc={props.initialValues.AllCalculationTypes.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }

                        <Field id="AllowOnDiscountedProducts" desc={localizer.get("AllowOnDiscountedProductsDesc")} name="AllowOnDiscountedProducts" type="checkbox" component={FormCheckBoxField} label={localizer.get("AllowOnDiscountedProducts")} />

                        <Field id="AllowMultipleUse" desc={localizer.get("AllowMultipleUseDesc")} name="AllowMultipleUse" type="checkbox" component={FormCheckBoxField} label={localizer.get("AllowMultipleUse")} />
                        <Field name="Amount" desc={localizer.get("AmountDesc")} type="number" component={FormTextField} label={localizer.get("Amount")} />
                        <Field name="AmountLimit" desc={localizer.get("AmountLimitDesc")} type="number" component={FormTextField} label={localizer.get("AmountLimit")} />
                        <Field name="MaxAmountLimit" desc={localizer.get("MaxAmountLimitDesc")} type="number" component={FormTextField} label={localizer.get("MaxAmountLimit")} />

                        <Field name="DiscountLimit" desc={localizer.get("DiscountLimitDesc")} type="number" component={FormTextField} label={localizer.get("DiscountLimit")} />
                        <Field name="CreationLimit" desc={localizer.get("CreationLimitDesc")} type="number" component={FormTextField} label={localizer.get("CreationLimit")} />
                         

                            <Field name="BeginDate" type="datetime-local" component={FormTextField} label={localizer.get("BeginDate")} />
                            <Field name="EndDate" type="datetime-local" component={FormTextField} label={localizer.get("EndDate")} />

                            {props.initialValues && props.initialValues.AllCampaigns &&
                                <Field name="CampaignID" label={localizer.get("Campaign")} type="select"
                                    component={FormSelectField}
                                    selectText={localizer.getWithFormat('PleaseSelectNewField', '$Campaign')}
                                    optionFnc={props.initialValues.AllCampaigns.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }


                            {props.initialValues && props.initialValues.AllDomains &&
                                <Field name="DomainId" label={localizer.get("Domain")} type="select"
                                    component={FormSelectField}
                            selectText={localizer.getWithFormat('AnyField', '$Domain')}
                                    optionFnc={props.initialValues.AllDomains.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }

                            {props.initialValues && props.initialValues.AllPriceCurrencies &&
                                <Field name="PriceCurrencyID" label={localizer.get("Currency")} type="select"
                                    component={FormSelectField}
                            selectText={localizer.getWithFormat('AnyField', '$Currency')}
                                    optionFnc={props.initialValues.AllPriceCurrencies.map(status => (
                                        <option value={status.Value} key={status.Value}>
                                            {status.Name}
                                        </option>
                                    ))} />
                            }

                            <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />


                        </div>
                    </div>

                    <FormActions
                        submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
                    </FormActions>
                </div>
            }

        </form>
    )
}

var couponFormComp = connect(
    state => state.couponForms,
    dispatch => bindActionCreators(actionCreators, dispatch),
)(CouponForm);

export default reduxForm({
    enableReinitialize: true,
    touched: false,
    form: 'EditCouponForm', // a unique identifier for this form
    validate, // <--- validation function given to redux-form
    warn // <--- warning function given to redux-form
})(couponFormComp);
