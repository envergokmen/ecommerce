import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/PolicyLocalLangs/PolicyLocalLangStore';
import PolicyLocalLangForm from './PolicyLocalLangForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class CargopoliciesNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id, policyId } = params;

        this.policyId = policyId;
        this.props.GetInitialValues(id ? id : 0, policyId == undefined ? null: policyId);
         
        this.breadCrumbNav =
        [{ link: ``, text: localizer.get("Settings") },
        { link: `/policies/${localizer.GetSelectedLang()}/type`, text: localizer.get("Policies")}, 
        { link: `/policylocallangs/${localizer.GetSelectedLang()}/`, text: localizer.get("PolicyTranslations")},
        { link: "", text: localizer.getWithFormat("NewField", "$Policy") }];
    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        var policyId = this.policyId;

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <PolicyLocalLangForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    policyId={policyId}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$PolicyTranslation")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></PolicyLocalLangForm>
            </div>
        );
    }
}

export default connect(
    state => state.policyLocalLangs,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(CargopoliciesNew);