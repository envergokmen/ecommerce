import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/BreadCrumb/BreadCrumbStore';
import { Link } from "react-router-dom";
import { Button } from 'reactstrap';
import sharedTranslations from '../Localizer/Translations/_Shared';
import Localizer from "../Localizer/Localizer";
import UserManager from "../UserManager/UserManager";
import { withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

var localizer = new Localizer(sharedTranslations, "", 'en');
var userManager = new UserManager();

class BreadCrumb extends Component {

    constructor() {
        super();
        this.onHandleChange = this.onHandleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = { searchKeyword: "" }
    }

    componentWillMount() {

        var curUser = userManager.getLoggedInUser();
        console.log(curUser);

        if (!userManager.getLoggedInUser()) {
            this.props.history.push(`/sign-in/${localizer.GetSelectedLang()}`)
        }

        if(this.props && this.props.navs && this.props.navs.length>0)
        {
            document.title=  this.props.navs[this.props.navs.length-1].text;
        }
         
        
    }

    onHandleChange(e) {
        this.setState({
            searchKeyword: e.target.value
        })
    }

    handleSubmit(e) {

        e.preventDefault();
        e.stopPropagation();

        this.props.SearchGeneral(
            {
                searchAction: this.props.searchAction,
                actionUrl: this.props.actionUrl,
                searchKeyword: this.state.searchKeyword
            });
    }

    changeLang = (lang) => {

        localizer.SetActiveLag(lang);
        var urlToReplace = localizer.getReplaceUrlForLang(lang);

        if (urlToReplace) {
            window.location.href = urlToReplace;
        }
    }

    logOut = () => {

        userManager.removeLoggedInUser();
        var lang = localizer.GetSelectedLang();
        var that=this;

        const response = fetch(`/${lang}/LogOut/Ajax`, {
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
           
            if (data && data.Status) {
                that.props.history.push(`/sign-in/${lang}`);
            }
            return data;

        });

    }

    renderLink(link, text, index) {

        if (link === "") {
            return <span key={"bread" + index}>{text}</span>
        } else {
            return (<Link key={"bread" + index} to={link}>{text}</Link>)
        }
    }

    render() {

        var lang = localizer.GetSelectedLang();
        var trColorName = lang === "tr" ? "primary" : "light";
        var enColorName = lang === "en" ? "primary" : "light";

        var user = userManager.getLoggedInUser();
        var userComp = (<span key="LogOutLink"></span>)
        if (user) {

            userComp = <div key={user.UserName} id="UserContainer" className="float-right">
                <span className="text-black-50">{localizer.get("Welcome")}</span>
                <span>{user.Name}</span>

                <a key="LogoutLink" onClick={() => this.logOut()} href="javascript:;" id="LogOutLink" className="btn btn-sm btn-light">{localizer.get("Logout")}</a>
            </div>
        }

        return (<div id="BreadCrumb">
            <div id="BreadCrumbLeftCon">
                {this.props.navs.map(({ link, text }, index) =>
                    this.renderLink(link, text, index)
                )}
            </div>
            <Button color={trColorName} onClick={() => this.changeLang("tr")} size="sm" className={`lang-btn float-right`}>TR</Button>
            <Button color={enColorName} onClick={() => this.changeLang("en")} size="sm" className={`lang-btn float-right`}>EN</Button>
            {userComp}

            <div id="SearchBox">
                <form searchaction={this.props.searchAction} action={this.props.actionUrl} onSubmit={this.handleSubmit}>
                    <input onChange={this.onHandleChange} type="text" className="form-control" name="searchKeyword" value={this.state.searchKeyword}></input>
                    <button id="SubmitSearch">
                        <FontAwesomeIcon icon="search" />
                    </button>
                </form>
            </div>

        </div>
        )
    };
}

// export default withRouter(connect()(BreadCrumb));


export default withRouter(connect(
    //mapDispatchToProps,
    state => state.breadCrumb,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(BreadCrumb));