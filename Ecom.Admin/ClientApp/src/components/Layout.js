import React, { Component } from 'react';
import LeftNav from './LeftNav/LeftNav';
import Localizer from "../Localizer/Localizer";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/Layout/LayoutStore';

var localizer = new Localizer();

class Layout extends Component {

    render() {

        return (
            <div>
                <div id="MainContainer">
                {this.props.IsLoading &&
                <div id="Loader" className="spinner-border text-dark" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
                }
                    <div id="LeftNavContainer">
                        <LeftNav />
                    </div>
                    <div id="MainAreaContainer">
                        <div id="MainArea">
                            {this.props.children}
                        </div>
                    </div>
                </div>
                <footer>
                    <div className="row">
                        <div className="col-sm-12">
                            All rigsht reserved (c)
                        </div>
                    </div>
                </footer>
            </div>
        )

    }
}
 
export default connect( 
    state => state.layout,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(Layout);