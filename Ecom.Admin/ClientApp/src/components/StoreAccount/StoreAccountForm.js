import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/StoreAccounts/StoreAccountStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import storeAccountTranslations from "../../Localizer/Translations/StoreAccounts";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations, storeAccountTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$StoreAccount")
  }

  // if (values.PStatus === undefined || values.PStatus === null || values.PStatus === "") {
  //   errors.PStatus = localizer.getWithFormat("RequiredField", "$Status")
  // }

  if (values.PriceCurrencyID === undefined || values.PriceCurrencyID === null || values.PriceCurrencyID === "") {
    errors.PriceCurrencyID = localizer.getWithFormat("RequiredField", "$Currency")
  }

  if (values.LangID === undefined || values.LangID === null || values.LangID === "") {
    errors.LangID = localizer.getWithFormat("RequiredField", "$Language")
  }
  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let StoreAccountForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/storeAccounts/${localizer.GetSelectedLang()}`);
  }

  //const fileInput = React.createRef();

  const handleFile = function () {
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {
    
    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {

      
      let formData = new FormData();

      
      formData.append('StoreName', e.target.elements.StoreName.value);
      formData.append('CompanyName', e.target.elements.CompanyName.value);
      formData.append('CompanyEmail', e.target.elements.CompanyEmail.value);
      formData.append('CompanyPhone', e.target.elements.CompanyPhone.value);
      formData.append('CompanyAddress', e.target.elements.CompanyAddress.value);
      formData.append('CompanyTaxInfo', e.target.elements.CompanyTaxInfo.value);
      formData.append('CompanyTaxInfo2', e.target.elements.CompanyTaxInfo2.value);

      // formData.append('Priority', e.target.elements.Priority.value);
      formData.append('PStatus', '1'); //e.target.elements.PStatus.checked ? "1" : "0"
      // formData.append('LangID', e.target.elements.LangID.value);
      // formData.append('PriceCurrencyID', e.target.elements.PriceCurrencyID.value);
      formData.append('ID', e.target.elements.ID.value);

      props.SaveItem(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              
              <Field name="CompanyName" type="text" component={FormTextField} label={localizer.get("CompanyName")} />
              <Field name="StoreName" type="text" component={FormTextField} label={localizer.get("StoreName")} />
              <Field name="CompanyEmail" type="text" component={FormTextField} label={localizer.get("CompanyEmail")} />
              <Field name="CompanyPhone" type="text" component={FormTextField} label={localizer.get("CompanyPhone")} />
              <Field name="CompanyAddress" type="text" component={FormTextField} label={localizer.get("CompanyAddress")} />
              <Field name="CompanyTaxInfo" type="text" component={FormTextField} label={localizer.get("CompanyTaxInfo")} />
              <Field name="CompanyTaxInfo2" type="text" component={FormTextField} label={localizer.get("CompanyTaxInfo2")} />
 
              {/* {props.initialValues && props.initialValues.AllLangs &&
                <Field name="CountryID" label={localizer.get("Language")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Language')}
                  optionFnc={props.initialValues.AllLangs.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              } */}

              {/* {props.initialValues && props.initialValues.AllCurrencies &&
                <Field name="PriceCurrencyID" label={localizer.get("Currency")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Currency')}
                  optionFnc={props.initialValues.AllCurrencies.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              } */}

              {/* <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} /> */}

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var storeAccountFormComp = connect(
  state => state.storeAccountForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(StoreAccountForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditStoreAccountForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(storeAccountFormComp);
