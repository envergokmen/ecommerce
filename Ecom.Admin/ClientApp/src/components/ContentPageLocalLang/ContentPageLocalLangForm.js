import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/ContentPageLocalLangs/ContentPageLocalLangStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$Name")
  }

  if (!values.LangID || values.LangID == undefined || values.LangID == null || values.LangID === ""|| values.LangID == "-1") {
    errors.LangID = localizer.getWithFormat("RequiredField", "$Language")
  }

  if (!values.ContentPageID || values.ContentPageID == undefined || values.ContentPageID == null || values.ContentPageID == "" || values.ContentPageID == "-1") {
    errors.ContentPageID = localizer.getWithFormat("RequiredField", "$ContentPage")
  }
  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let ContentPageLocalLangForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {

    if (!props.contentpageId) {
      props.push(`/contentpagelocallangs/${localizer.GetSelectedLang()}`);
    } else {
      props.push(`/contentpagelocallangs/${localizer.GetSelectedLang()}/translations/${props.contentpageId}`);
    }

  }

  //const fileInput = React.createRef();

  const handleFile = function () {
  }

  var EditorData=props && props.initialValues ? props.initialValues.Description : "";

  const handleEditorChange = (data) => {
    EditorData = data;
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {


      let formData = new FormData();
      
      formData.append('Name', e.target.elements.Name.value);
      formData.append('LangID', e.target.elements.LangID.value);
      formData.append('DetailHTML', EditorData);
      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('ContentPageID', e.target.elements.ContentPageID.value);
      formData.append('ID', e.target.elements.ID.value);
      formData.append('Title', e.target.elements.Title.value);
      formData.append('Summary', e.target.elements.Summary.value);
      formData.append('MetaDescription', e.target.elements.MetaDescription.value);
      formData.append('MetaKeywords', e.target.elements.MetaKeywords.value);
      formData.append('CanonicalLink', e.target.elements.CanonicalLink.value);
      formData.append('Url', e.target.elements.Url.value);
      //formData.append('WebImageUrl', e.target.elements.WebImageUrl.value);
      //formData.append('MenuImageUrl', e.target.elements.MenuImageUrl.value);
      //formData.append('MobileImageUrl', e.target.elements.MenuImageUrl.value);

      props.SaveItem(formData);

    }
  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="Name" type="text" component={FormTextField} label={localizer.get("Name")} />
              <Field name="Title" type="text" component={FormTextField} label={localizer.get("Title")} />
              <Field name="Summary" type="text" component={FormTextField} label={localizer.get("Summary")} />
              <Field name="MetaDescription" type="text" component={FormTextField} label={localizer.get("MetaDescription")} />
              <Field name="MetaKeywords" type="text" component={FormTextField} label={localizer.get("MetaKeywords")} />
              <Field name="CanonicalLink" type="text" component={FormTextField} label={localizer.get("CanonicalLink")} />
              <Field name="Url" type="text" component={FormTextField} label={localizer.get("Url")} />

              <div className={"form-group row CKEdit"}>
                <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get('Content')}</label>
                <div className="col-sm-9">
                  <CKEditor
                    editor={ClassicEditor}
                    data={props && props.initialValues && props.initialValues.DetailHTML ? props.initialValues.DetailHTML : ""}
                    onInit={editor => {
                      // You can store the "editor" and use when it is needed.
                      console.log('Editor is ready to use!', editor);
                    }}
                    onChange={(event, editor) => {
                      const data = editor.getData();
                      handleEditorChange(data);
                      // console.log( { event, editor, data } );
                    }}
                    onBlur={editor => {
                      //console.log( 'Blur.', editor );
                    }}
                    onFocus={editor => {
                      //console.log( 'Focus.', editor );
                    }}
                  ></CKEditor>
                </div>
              </div>

              {props.initialValues && props.initialValues.AllLangs &&
                <Field name="LangID" label={localizer.get("Language")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Language')}
                  optionFnc={props.initialValues.AllLangs.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }


              {props.initialValues && props.initialValues.AllContentPages &&
                <Field name="ContentPageID" label={localizer.get("ContentPage")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$ContentPage')}
                  optionFnc={props.initialValues.AllContentPages.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }

              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />


            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var contentPageFormComp = connect(
  state => state.contentPageLocalLangForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(ContentPageLocalLangForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditContentPageLocalLangForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(contentPageFormComp);
