import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/ContentPageLocalLangs/ContentPageLocalLangStore';
import ContentPageLocalLangForm from './ContentPageLocalLangForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class ContentPagesNew extends Component {
    componentWillMount() {

        const { match: { params } } = this.props;
        const { lang, id, contentpageId } = params;

        this.contentpageId = contentpageId;
        this.props.GetInitialValues(id ? id : 0, contentpageId == undefined ? null: contentpageId);
         
        this.breadCrumbNav =
        [{ link: ``, text: localizer.get("Settings") },
        { link: `/contentpages/${localizer.GetSelectedLang()}/type`, text: localizer.get("ContentPages")}, 
        { link: `/contentpagelocallangs/${localizer.GetSelectedLang()}/`, text: localizer.get("ContentPageTranslations")},
        { link: "", text: localizer.getWithFormat("NewField", "$ContentPage") }];
    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        var contentpageId = this.contentpageId;

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <ContentPageLocalLangForm
                    SaveItem={this.props.SaveItem}
                    push={this.props.history.push}
                    contentpageId={contentpageId}
                    operationResult={this.props.operationResult}
                    FormResult={this.props.FormResult}
                    header={localizer.getWithFormat("NewField", "$ContentPageTranslation")}
                    id={this.props.initial ? this.props.initial.ID : 0}
                    initialValues={this.props.initial} onSubmit={this.submit}></ContentPageLocalLangForm>
            </div>
        );
    }
}

export default connect(
    state => state.contentPageLocalLangs,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(ContentPagesNew);