import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Cargoes/CargoStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormCheckBoxField, FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$Name")
  }
 
  if (!values.CargoCompany || values.CargoCompany === undefined || values.CargoCompany === null || values.CargoCompany === "") {
    errors.CargoCompany = localizer.getWithFormat("RequiredField", "$CargoCompany")
  }

  // if (values.LangID === undefined || values.LangID === null || values.LangID === "") {
  //   errors.LangID = localizer.getWithFormat("RequiredField", "$Language")
  // }
  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let CargoForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/cargoes/${localizer.GetSelectedLang()}`);
  }

  const fileInput = React.createRef();

  const handleFile = function () {
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {


      let formData = new FormData();
      formData.append('Name', e.target.elements.Name.value);
      formData.append('SortNo', e.target.elements.SortNo.value);
      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");

      formData.append('IsUseWebServis', e.target.elements.IsUseWebServis.checked ? "true" : "false");

      formData.append('Notes', e.target.elements.Notes.value);
      formData.append('WebServisUserName', e.target.elements.WebServisUserName.value);
      formData.append('WebServisUserPassword', e.target.elements.WebServisUserPassword.value);
      formData.append('WebServisUserPassword', e.target.elements.WebServisUserPassword.value);
      formData.append('WebServisCustomerNumber', e.target.elements.WebServisCustomerNumber.value);
      formData.append('WebServisShipperName', e.target.elements.WebServisShipperName.value);
      formData.append('WebServisShipperAddress', e.target.elements.WebServisShipperAddress.value);
      formData.append('ShipperPhoneNumber', e.target.elements.ShipperPhoneNumber.value);
      formData.append('WebServisShipperCityCode', e.target.elements.WebServisShipperCityCode.value);
      formData.append('WebServisShipperAreaCode', e.target.elements.WebServisShipperAreaCode.value);
      formData.append('CargoCompany', e.target.elements.CargoCompany.value);
      formData.append('DomainID', e.target.elements.DomainID.value);
      formData.append('file', fileInput.current.files[0]);
      formData.append('ID', e.target.elements.ID.value);

      props.SaveItem(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="Name" type="text" component={FormTextField} label={localizer.get("Cargo")} />
              <Field name="SortNo" type="number" component={FormTextField} label={localizer.get("SortNo")} />

              <Field id="IsUseWebServis" name="IsUseWebServis" type="checkbox" component={FormCheckBoxField} label={localizer.get("IsUseWebServis")} />
              <Field name="Notes" type="text" component={FormTextField} label={localizer.get("Notes")} />

              <Field name="WebServisUserName" type="text" component={FormTextField} label={localizer.get("WebServisUserName")} />
              <Field name="WebServisUserPassword" type="text" component={FormTextField} label={localizer.get("WebServisUserPassword")} />
              <Field name="WebServisCustomerNumber" type="text" component={FormTextField} label={localizer.get("WebServisCustomerNumber")} />
              <Field name="WebServisShipperName" type="text" component={FormTextField} label={localizer.get("WebServisShipperName")} />
              <Field name="WebServisShipperAddress" type="text" component={FormTextField} label={localizer.get("WebServisShipperAddress")} />
              <Field name="ShipperPhoneNumber" type="text" component={FormTextField} label={localizer.get("ShipperPhoneNumber")} />
              <Field name="WebServisShipperCityCode" type="text" component={FormTextField} label={localizer.get("WebServisShipperCityCode")} />
              <Field name="WebServisShipperAreaCode" type="text" component={FormTextField} label={localizer.get("WebServisShipperAreaCode")} />


              {props.initialValues && props.initialValues.AllDomains &&
                <Field name="DomainID" label={localizer.get("Domain")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Domain')}
                  optionFnc={props.initialValues.AllDomains.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }

              {props.initialValues && props.initialValues.AllCargoCompanies &&
                <Field name="CargoCompany" label={localizer.get("CargoCompany")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$CargoCompany')}
                  optionFnc={props.initialValues.AllCargoCompanies.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }

                      <div className="form-group row">
                          <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get("Logo")}</label>
                          <div className="col-sm-9">
                              <input name="ImagePath" onChange={handleFile} ref={fileInput} type="file" className="form-control" />
                          </div>
                      </div>

              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var cargoFormComp = connect(
  state => state.cargoForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(CargoForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditCargoForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(cargoFormComp);
