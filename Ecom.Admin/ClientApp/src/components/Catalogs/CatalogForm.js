import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Catalogs/CatalogStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormCheckBoxField, FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}
  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$Catalog")
  }

  // if (values.PStatus === undefined || values.PStatus === null || values.PStatus === "") {
  //   errors.PStatus = localizer.getWithFormat("RequiredField", "$Status")
  // }

  if (values.PriceCurrencyID === undefined || values.PriceCurrencyID === null || values.PriceCurrencyID === "") {
    errors.PriceCurrencyID = localizer.getWithFormat("RequiredField", "$Currency")
  }

  if (values.LangID === undefined || values.LangID === null || values.LangID === "") {
    errors.LangID = localizer.getWithFormat("RequiredField", "$Language")
  }
  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let CatalogForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/catalogs/${localizer.GetSelectedLang()}`);
  }

  //const fileInput = React.createRef();

  const handleFile = function () {
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {

      
      //console.log(e.target.elements.ShowInHeader.checked , e.target.elements.ShowInFooter.checked );

      let formData = new FormData();
      
      formData.append('Name', e.target.elements.Name.value);
      formData.append('SortNo', e.target.elements.SortNo.value);
      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('ID', e.target.elements.ID.value);
      formData.append('ShowInFooter', e.target.elements.ShowInFooter.checked ? "true" : "false");
      formData.append('ShowInHeader', e.target.elements.ShowInHeader.checked ? "true" : "false");
      formData.append('ParentID', e.target.elements.ParentID.value);
      formData.append('CatalogType', e.target.elements.CatalogType.value);
 
    // WebImageUrl
    // MenuImageUrl
    // MobileImageUrl

      props.SaveItem(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />

              <Field name="Name" type="text" component={FormTextField} label={localizer.get("Name")} />


              {props.initialValues && props.initialValues.AllCatalogs &&
                <Field name="ParentID" label={localizer.get("ParentCatalog")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$ParentCatalog')}
                  optionFnc={props.initialValues.AllCatalogs.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }
 
              {props.initialValues && props.initialValues.AllCatalogTypes &&
                <Field name="CatalogType" label={localizer.get("CatalogType")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$CatalogType')}
                  optionFnc={props.initialValues.AllCatalogTypes.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }
              
              <Field name="SortNo" type="number" component={FormTextField} label={localizer.get("SortNo")} />
              <Field id="ShowInHeader"  name="ShowInHeader" type="checkbox" component={FormCheckBoxField} label={localizer.get("ShowInHeader")} />
              <Field id="ShowInFooter" name="ShowInFooter" type="checkbox" component={FormCheckBoxField} label={localizer.get("ShowInFooter")} />

              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var catalogFormComp = connect(
  state => state.catalogForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(CatalogForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditCatalogForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(catalogFormComp);
