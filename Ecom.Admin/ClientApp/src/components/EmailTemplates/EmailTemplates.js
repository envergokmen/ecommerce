import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/EmailTemplates/EmailTemplateStore';
import { Link } from "react-router-dom";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';
import { Localize, Localizer } from "../../Localizer/Localizer";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {default as queryString } from 'qs';
import { Router } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class EmailTemplates extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggleModal = this.toggleModal.bind(this);
        this.NextPage = this.NextPage.bind(this);
        this.delete = this.delete.bind(this);
    }


    NextPage(event) {

        event.preventDefault();
        event.stopPropagation();
        this.props.SearchEmailTemplates({ append: true, page: this.props.Page + 1, searchKeyword: this.props.searchKeyword });
        this.props.history.push(`/emailTemplates/${localizer.GetSelectedLang()}/${this.props.Page + 1}${this.searchKeyword}`);
    }

    delete(event) {

        event.preventDefault();
        event.stopPropagation();
        this.props.DeleteItem(this.state.IdToDelete);
        this.toggleModal(event);
    }

    toggleModal(event = null) {
        event.preventDefault();
        event.stopPropagation();

        const { id, name } = event.target;

        this.setState(prevState => ({
            modal: !prevState.modal,
            IdToDelete: id,
            name: name
        }));

    }
    componentWillMount() {

        const { match: { params } } = this.props;
        this.lang = params.lang;
        this.page = params.page ? params.page : 1;
        this.searchKeywordQs = (queryString.parse(this.props.location.search).searchKeyword) ? `?searchKeyword=${queryString.parse(this.props.location.search).searchKeyword}` : '';
        this.searchKeyword = (queryString.parse(this.props.location.search).searchKeyword) ? queryString.parse(this.props.location.search).searchKeyword : '';

        this.breadCrumbNav =
            [{ link: ``, text: localizer.get("Settings") },
            { link: "", text: localizer.get("EmailTemplates") }];
     
    }


    componentDidMount() {

        this.props.SearchEmailTemplates({ page: this.page, searchKeyword: this.searchKeyword });
        this.props.ResetFormResult();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps && nextProps.deleteResult && !nextProps.deleteResult.Status) {
            toast.error(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.deleteResult && nextProps.deleteResult.Status) {
            toast.success(nextProps.deleteResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }

        //save result from prev from submit
        if (nextProps && nextProps.saveResult && !nextProps.saveResult.Status) {
            toast.error(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        } else if (nextProps && nextProps.saveResult && nextProps.saveResult.Status) {
            toast.success(nextProps.saveResult.ResultDescription.join(' '));
            this.props.ResetFormResult();
        }
    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>


                <div id="MainListArea">

                    <Link to={`/emailtemplates/${localizer.GetSelectedLang()}/new`} className="btn btn-primary">{localizer.getWithFormat("NewField", "$EmailTemplate")}</Link>

                    {(!this.props.emailTemplates || this.props.emailTemplates.length==0) && 
                    <div className="NoListItem">
                      <p>{localizer.getWithFormat("NoListItem", "$EmailTemplate")}</p>
                         <Link to={`/emailtemplates/${localizer.GetSelectedLang()}/new`} className="btn btn-primary">{localizer.getWithFormat("NewField", "$EmailTemplate")}</Link>
                    </div>
                    }

                    {this.props.emailTemplates &&
                        <table className="table table-striped ListingTable">

                            <thead className="thead-light">
                                <tr>
                                    <th>ID</th>
                                    <th className="percent25">{localizer.get('Name')}</th>
                                    <th className="percent25">{localizer.get('Translations')}</th>
                                    <th className="percent25">{localizer.get('Type')}</th>
                                    <th>{localizer.get('Status')}</th>
                                    <th className="Operations">{localizer.get('Operations')}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.emailTemplates.map((item, index) =>
                                    <tr key={item.ID} className={item.PStatus == 1 ? '' : 'text-muted'}>
                                        <td className="Id">{item.ID}</td>
                                        <td>{item.Name}</td>
                                        <td>

<Link to={`/emailtemplatelocallangs/${localizer.GetSelectedLang()}/translations/${item.ID}`}>({item.Translations}) {localizer.get("Translation")} </Link>
{item.Translations == 0 &&
    <span className="WarningIcon" title={localizer.get('AddAtLeastOneTranslation')}>
        {localizer.get('Warning')}
    </span>
}
</td>
                                        <td>{item.MailTypeText}</td>

                                        <td className="PStatus">
                                            <span className={item.PStatus=="1" ?"Status Active":"Status Passive"}></span>
                                            <span className="StatusText">
                                            {item.PStatus=="1" && localizer.get("Active")}
                                            {item.PStatus!="1" && localizer.get("Passive")}
                                            </span>
                                            
                                        </td>
                                        <td className="Operations">

                                            <Link to={`/emailtemplatelocallangs/${localizer.GetSelectedLang()}/translations/${item.ID}`} className="btn btn-sm btn-primary">{localizer.get("Translations")}</Link>

                                            <Link to={`/emailTemplates/${localizer.GetSelectedLang()}/edit/${item.ID}`} className="btn btn-sm btn-warning"><Localize localizer={localizer}>Edit</Localize></Link>
                                            <button id={item.ID} name={item.Name} onClick={(event) => this.toggleModal(event)} className="btn btn-sm btn-danger"><Localize localizer={localizer}>Delete</Localize></button>

                                        </td>
                                    </tr>
                                )}


                            </tbody>

                        </table>
                    }

                    {this.props.emailTemplates &&
                        this.props.TotalPage > this.props.Page &&
                        <div className="text-center">
                            <button onClick={this.NextPage} className="btn btn-success LoadMore">Load More</button>
                        </div>
                    }

                    <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={this.props.className}>
                        <ModalHeader toggle={this.toggleModal}>
                            {localizer.get("ConfirmDelete")}
                        </ModalHeader>
                        <ModalBody>
                            <b>{this.state.name}</b> {' '}
                            {localizer.get('WillBeDeleted')}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={this.delete}>{localizer.get('Confirm')}</Button>{' '}
                            <Button color="secondary" onClick={this.toggleModal}>{localizer.get('Cancel')}</Button>
                        </ModalFooter>
                    </Modal>

                    <ToastContainer />
                </div>
            </div >
        );
    }

}

export default connect(
    state => state.emailTemplates,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(EmailTemplates);