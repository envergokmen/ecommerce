import React from 'react'
import { Field, reduxForm, initialize } from 'redux-form'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/EmailTemplates/EmailTemplateStore';
import { Localizer } from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import { FormStatusField, FormResultMessage, FormHiddenField, FormSelectField, FormTextField, FormActions } from '../Forms/FormFields';
//import CKEditor from '@ckeditor/ckeditor5-react';
//import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

var localizer = new Localizer([sharedTranslations]);

const validate = values => {
  const errors = {}

  if (!values.Name) {
    errors.Name = localizer.getWithFormat("RequiredField", "$EmailTemplate")
  }

  if (values.EmailProviderID == undefined || values.EmailProviderID == null || values.EmailProviderID == "" || values.EmailProviderID == "0") {
    errors.EmailProviderID = localizer.getWithFormat("RequiredField", "$EmailProvider")
  }
  if (values.MailType === undefined || values.MailType === null || values.MailType === "") {
    errors.MailType = localizer.getWithFormat("RequiredField", "$EmailType")
  }

  if (values.DomainID == undefined || values.DomainID === null || values.DomainID == "" || values.DomainID == "0") {
    errors.DomainID = localizer.getWithFormat("RequiredField", "$Domain")
  }

  return errors;

}

const warn = values => {
  const warnings = {}
  return warnings
}

const statuses = [{ name: localizer.get('Active'), value: 1 }, { name: localizer.get('Passive'), value: 0 }]

let EmailTemplateForm = props => {

  const { handleSubmit, pristine, reset, submitting } = props;

  if (props && props.operationResult && props.operationResult.Status) {
    props.push(`/emailTemplates/${localizer.GetSelectedLang()}`);
  }

  var EditorData="";

  //const fileInput = React.createRef();

  const handleFile = function () {
  }

  const handleEditorChange = (data) => {
   var EditorData=data;
   console.log("editordata",data);
  }

  const handleImageDelete = (e) => {
    e.preventDefault();
    props.DeleteImage(props.id);
  }

  const onFormSubmit = (e) => {

    e.preventDefault();

    var validateResult = handleSubmit(e);

    if (validateResult != null || validateResult != undefined) {
      return;
    }
    else {

      
      let formData = new FormData();
      formData.append('Name', e.target.elements.Name.value);
      formData.append('DomainID', e.target.elements.DomainID.value);
      formData.append('EmailProviderID', e.target.elements.EmailProviderID.value);
      formData.append('MailType', e.target.elements.MailType.value);
      formData.append('PStatus', e.target.elements.PStatus.checked ? "1" : "0");
      formData.append('ID', e.target.elements.ID.value);

      props.SaveItem(formData);
    }

  }

  return (

    <form onSubmit={onFormSubmit.bind(this)} className={props.initialValues && props.initialValues.AllStatuses ? "FormStandard" : "FormStandard hidden"} method="POST" encType='multipart/form-data'>

      <div id="FormHeader">
        <h1>{props.header}</h1>
      </div>

      {props.operationResult && <FormResultMessage operationResult={props.operationResult}></FormResultMessage>}

      {(!props.operationResult || !props.operationResult.Status) &&
        <div>
          <div className="row">
            <div id="FormBody" className="col-sm-8">

              <Field name="ID" type="hidden" component={FormHiddenField} />
              <Field name="Name" type="text" component={FormTextField} label={localizer.get("Name")} />

              {props.initialValues && props.initialValues.AllDomains &&
                <Field name="DomainID" label={localizer.get("Domain")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$Domain')}
                  optionFnc={props.initialValues.AllDomains.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }

              {props.initialValues && props.initialValues.AllMailTypes &&
                <Field name="MailType" label={localizer.get("EmailType")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$EmailType')}
                  optionFnc={props.initialValues.AllMailTypes.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }
 
              {props.initialValues && props.initialValues.AllProviders &&
                <Field name="EmailProviderID" label={localizer.get("SendingProvider")} type="select"
                  component={FormSelectField}
                  selectText={localizer.getWithFormat('PleaseSelectNewField', '$SendingProvider')}
                  optionFnc={props.initialValues.AllProviders.map(status => (
                    <option value={status.Value} key={status.Value}>
                      {status.Name}
                    </option>
                  ))} />
              }



      {/* <div className={"form-group row CKEdit"}>
            <label htmlFor="firstName" className="col-sm-3 col-form-label">{localizer.get('Content')}</label>
            <div className="col-sm-9">
            <CKEditor
                    editor={ ClassicEditor }
                    data="<p>Hello from CKEditor 5!</p>"
                    onInit={ editor => {
                        // You can store the "editor" and use when it is needed.
                        console.log( 'Editor is ready to use!', editor );
                    } }
                    onChange={ ( event, editor ) => {
                        const data = editor.getData();
                        handleEditorChange(data);
                       // console.log( { event, editor, data } );
                    } }
                    onBlur={ editor => {
                        //console.log( 'Blur.', editor );
                    } }
                    onFocus={ editor => {
                        //console.log( 'Focus.', editor );
                    } }
                ></CKEditor>
            </div>
        </div> */}

               

              <Field name="PStatus" type="checkbox" component={FormStatusField} label={localizer.get("Status")} />

            </div>
          </div>

          <FormActions
            submitting={submitting} pristine={pristine} reset={reset} saveText={localizer.get("SaveChanges")} resetText={localizer.get("ResetChanges")}>
          </FormActions>
        </div>
      }

    </form>
  )
}

var emailTemplateFormComp = connect(
  state => state.emailTemplateForms,
  dispatch => bindActionCreators(actionCreators, dispatch),
)(EmailTemplateForm);

export default reduxForm({
  enableReinitialize: true,
  touched: false,
  form: 'EditEmailTemplateForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(emailTemplateFormComp);
