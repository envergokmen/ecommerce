import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/Brands/BrandStore';
import BrandForm from './BrandForm';
import Localizer from "../../Localizer/Localizer";
import sharedTranslations from "../../Localizer/Translations/_Shared";
import navTranslations from "../../Localizer/Translations/Navigation";
import BreadCrumb from '../BreadCrumb';

var localizer = new Localizer([sharedTranslations, navTranslations]);

class BrandsNew extends Component {
    componentWillMount() {
 
        const { match: { params } } = this.props;
        const { lang , id} = params;
   
        if (id) {
            this.props.GetInitialValues(id);
        } else {
            this.props.GetInitialValues(0);
        }

        this.breadCrumbNav = 
        [{ link: `/products/${lang}`, text: localizer.get("ProductManagement")},
           {link: `/brands/${lang}`, text: localizer.get("Brands")},
           {link: "", text: localizer.getWithFormat("NewField","$Brand")}]

    }

    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
    }
    submit = values => {

    }

    render() {

        return (
            <div>
                <BreadCrumb navs={this.breadCrumbNav}></BreadCrumb>
                <BrandForm 
                SaveItem={this.props.SaveItem} 
                DeleteImage={this.props.DeleteImage}
                push={this.props.history.push} 
                deleteImgResult={this.props.deleteImgResult} 
                operationResult={this.props.operationResult} 
                FormResult={this.props.FormResult} 
                formSuccess={"test başarılı mesaji"} 
                formErrors={"test hata mesaji"} 
                header={localizer.getWithFormat("NewField", "$Brand")}
                  id={this.props.initial ? this.props.initial.ID : 0}  
                  initialValues={this.props.initial} onSubmit={this.submit}></BrandForm>
            </div>
        );
    }
}

export default connect(
    state => state.brands,
    dispatch => bindActionCreators(actionCreators, dispatch),

)(BrandsNew);