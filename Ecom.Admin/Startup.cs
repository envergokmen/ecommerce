using Ecom.Auth.Services;
using Ecom.Core;
using Ecom.Models;
using Ecom.MvcCore;
using Ecom.MvcCore.Product;
using Ecom.Services;
using Ecom.Services.Bus;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using System;
using System.Globalization;
using System.Reflection;

namespace Ecom.Admin
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true; //CheckConsentNeeded set to true, non-essential cookies aren't sent to the browser.
                options.MinimumSameSitePolicy = SameSiteMode.None;

            });

            services.AddEntityFrameworkNpgsql()
               .AddDbContext<EcomContext>()
               .BuildServiceProvider();

            services.AddDbContext<EcomContext>(options =>
            options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"), o => o.MigrationsAssembly("Ecom.Database")));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<RabbitMQService, RabbitMQService>();
            services.AddScoped<UserService, UserService>();
            services.AddScoped<DomainService, DomainService>();
            services.AddScoped<BrandService, BrandService>();
            services.AddScoped<ServiceUtils, ServiceUtils>();
            services.AddScoped<StoreAccountService, StoreAccountService>();
            services.AddScoped<SizeService, SizeService>();
            services.AddScoped<ColorService, ColorService>();
            services.AddScoped<ColorGlobalService, ColorGlobalService>();
            services.AddScoped<SeasonService, SeasonService>();
            services.AddScoped<CatalogService, CatalogService>();
            services.AddScoped<BannerService, BannerService>();
            services.AddScoped<CampaignService, CampaignService>();
            services.AddScoped<CouponService, CouponService>();
            services.AddScoped<EmailTemplateService, EmailTemplateService>();
            services.AddScoped<SendingProviderService, SendingProviderService>();
            services.AddScoped<ProductService, ProductService>();
            services.AddScoped<ProductDetailService, ProductDetailService>();
            services.AddScoped<PolicyLocalLangService, PolicyLocalLangService>();
            services.AddScoped<CatalogLocalLangService, CatalogLocalLangService>();
            services.AddScoped<PolicyService, PolicyService>();
            services.AddScoped<StockService, StockService>();
            services.AddScoped<ContentPageService, ContentPageService>();
            services.AddScoped<ContentPageLocalLangService, ContentPageLocalLangService>();
            services.AddScoped<EmailTemplateLocalLangService, EmailTemplateLocalLangService>();
            services.AddScoped<CargoService, CargoService>();
            services.AddScoped<CargoPriceService, CargoPriceService>();
            services.AddScoped<PriceCurrencyService, PriceCurrencyService>();
            services.AddScoped<PriceService, PriceService>();
            services.AddScoped<PaymentService, PaymentService>();
            services.AddScoped<WebPosService, WebPosService>();
            services.AddScoped<WebPosInstallmentService, WebPosInstallmentService>();
            services.AddScoped<BankService, BankService>();
            services.AddScoped<ProductImageHelper, ProductImageHelper>();
            services.AddScoped<BankService, BankService>();
            services.AddScoped<ProductDepartmentService, ProductDepartmentService>();
            services.AddScoped<ProductStyleService, ProductStyleService>();
            services.AddScoped<LangService, LangService>();
            services.AddScoped<NewsLetterService, NewsLetterService>();

            services.AddScoped<IMemberShipService, MemberShipService>();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(60);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });

            services.AddDistributedMemoryCache();

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
               .AddMvcLocalization()
               .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddJsonOptions(o =>
                {
                    if (o.SerializerSettings.ContractResolver != null)
                    {
                        var castedResolver = o.SerializerSettings.ContractResolver
                            as DefaultContractResolver;
                        castedResolver.NamingStrategy = null;
                    }
                })
              // .AddDataAnnotationsLocalization();
              .AddDataAnnotationsLocalization(options =>
              {
                  options.DataAnnotationLocalizerProvider = (type, factory) =>
                  {
                      var assemblyName = new AssemblyName(typeof(SharedDataResource).GetTypeInfo().Assembly.FullName);
                      return factory.Create("SharedDataResource", assemblyName.Name);
                  };
              });

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            services.Configure<RouteOptions>(options =>
            {
                options.ConstraintMap.Add("lang", typeof(IsLangCodeConstrait));
            });

            var sp = services.BuildServiceProvider();
            var domainService = sp.GetService<DomainService>();
            var catalogService = sp.GetService<CatalogService>();
            var productService = sp.GetService<ProductService>();
            var langService = sp.GetService<LangService>();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("tr-TR"),
                    new CultureInfo("en-US"),
                };

                //options.DefaultRequestCulture = new RequestCulture(culture: "tr-TR", uiCulture: "tr-TR");

                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;

                options.RequestCultureProviders = new[]{ new RouteDataRequestCultureProvider{
                   IndexOfCulture=1,
                   IndexofUICulture=1,
                   defaultCulture=null,
                   domainService =domainService,
                   catalogService = catalogService,
                   productService = productService,
                   langService = langService
                }};

            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);

            app.UseDeveloperExceptionPage();


            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Error");
            //    app.UseHsts();
            //}

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            HttpHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "PagesWithLangCode",
                  template: "{lang}/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" },
                  constraints: new { lang = new IsLangCodeConstrait() }
                  );

                //routes.MapRoute(
                //    name: "default",
                //    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
