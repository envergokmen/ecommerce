﻿using Ecom.Core;
using Ecom.ViewModels.Products;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Ecom.Admin
{
    public class FileUploadHelpler
    {
        public static string SaveFilFromHttp(bool isLocal, IFormFile file, string rootPath, string domainName, string contentType, string subFolder="")
        {

            var newFullPath = "";
            var newImagePath = "";
            if (file != null && file.Length > 0)
            {
                var fileExt = Path.GetExtension(file.FileName);
                if(new string[] { ".JPG", ".PNG", ".jpg", ".jpeg", ".png", ".bmp",".xls","xlsx",".csv"}.Contains(fileExt)==false)
                {
                    throw new Exception("file name is not acceptable");
                }

                var curImageGuid = Guid.NewGuid().ToString();

                var formattedDomainName = Utils.FormatUrl(domainName);
                var tempFolder = Path.Combine(rootPath, "ContentImages", formattedDomainName, "Temp");
                var tempfilePath = Path.Combine(tempFolder, curImageGuid + Path.GetExtension(Path.GetTempFileName()));
                var tempDirectory = new DirectoryInfo(tempFolder);

                if (!tempDirectory.Exists) Directory.CreateDirectory(tempDirectory.FullName);

                using (var stream = new FileStream(tempfilePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                var newPathFoler = isLocal ? Path.Combine(rootPath, "ClientApp", "public", "ContentImages", formattedDomainName, contentType, subFolder): Path.Combine(rootPath, "ClientApp", "build", "ContentImages", formattedDomainName, contentType, subFolder);

                newFullPath = Path.Combine(newPathFoler, curImageGuid.ToString() + fileExt);

                var newPathDirectory = new DirectoryInfo(newPathFoler);
                if (!newPathDirectory.Exists) Directory.CreateDirectory(newPathDirectory.FullName);

                newImagePath = String.Concat("/ContentImages/", formattedDomainName, "/", contentType, (subFolder!="" ? "/"+subFolder:""), "/", curImageGuid + fileExt);

                int i = 1;
                while (File.Exists(Path.Combine(newFullPath)))
                {
                    curImageGuid = Guid.NewGuid().ToString();
                    newImagePath = String.Concat("/ContentImages/", formattedDomainName, "/", contentType, (subFolder != "" ? "/" + subFolder : ""), "/", curImageGuid + fileExt);
                    newFullPath = Path.Combine(newPathFoler, curImageGuid + fileExt);

                    i++;
                }

                File.Copy(tempfilePath, newFullPath);

                try
                {
                    File.Delete(tempfilePath);
                }
                catch { }

            }

            return newImagePath;

        }
         
        public static string UploadProductFile(IFormFile file, string rootPath, string domainName, string contentType="ProductUploads", string subFolder = "")
        {

            var newFullPath = "";
            var newImagePath = "";
            if (file != null && file.Length > 0)
            {
                var fileExt = Path.GetExtension(file.FileName);
                if (new string[] { ".xls", ".json", ".ods", ".sxc",".xlsx" }.Contains(fileExt) == false)
                {
                    throw new Exception("file name is not acceptable");
                }

                var curImageGuid = Guid.NewGuid().ToString();

                var formattedDomainName = Utils.FormatUrl(domainName);
                var tempFolder = Path.Combine(rootPath, "Uploads", formattedDomainName, "Temp");
                var tempfilePath = Path.Combine(tempFolder, curImageGuid + Path.GetExtension(Path.GetTempFileName()));
                var tempDirectory = new DirectoryInfo(tempFolder);

                if (!tempDirectory.Exists) Directory.CreateDirectory(tempDirectory.FullName);

                using (var stream = new FileStream(tempfilePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                var newPathFoler = Path.Combine(rootPath, "ClientApp", "public", "Uploads", formattedDomainName, contentType, subFolder);

                newFullPath = Path.Combine(newPathFoler, curImageGuid.ToString() + fileExt);

                var newPathDirectory = new DirectoryInfo(newPathFoler);
                if (!newPathDirectory.Exists) Directory.CreateDirectory(newPathDirectory.FullName);

                newImagePath = String.Concat("/Uploads/", formattedDomainName, "/", contentType, (subFolder != "" ? "/" + subFolder : ""), "/", curImageGuid + fileExt);

                int i = 1;
                while (File.Exists(Path.Combine(newFullPath)))
                {
                    curImageGuid = Guid.NewGuid().ToString();
                    newImagePath = String.Concat("/Uploads/", formattedDomainName, "/", contentType, (subFolder != "" ? "/" + subFolder : ""), "/", curImageGuid + fileExt);
                    newFullPath = Path.Combine(newPathFoler, curImageGuid + fileExt);

                    i++;
                }

                File.Copy(tempfilePath, newFullPath);

                try
                {
                    File.Delete(tempfilePath);
                }
                catch { }

            }

            return newFullPath;

        }

    }
}
